package com.nomad.mopic.code.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.nomad.mopic.code.mapper.CommonCodeMapper;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.vo.CommonCodeVO;

@Service
public class CommonCodeService implements CommonCodeMapper {
	@Resource(name = "commonCodeMapper")
    private CommonCodeMapper commonCodeMapper;

	@Override
	public List<CommonCodeVO> selectCmnCdList(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.selectCmnCdList(commonCodeVO);
	}

	@Override
	public CommonCodeVO selectCmnCdDetail(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.selectCmnCdDetail(commonCodeVO);
	}

	@Override
	public boolean insertCmnCd(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.insertCmnCd(commonCodeVO);
	}

	@Override
	public boolean updateCmnCd(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.updateCmnCd(commonCodeVO);
	}

	@Override
	public List<CommonCodeVO> selectCmnGrpList(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.selectCmnGrpList(commonCodeVO);
	}

	@Override
	public boolean insertCmnGrp(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.insertCmnGrp(commonCodeVO);
	}

	@Override
	public boolean updateCmnGrp(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.updateCmnGrp(commonCodeVO);
	}

	@Override
	public List<CommonCodeVO> selectCmnCdGrpDetailList(CommonCodeVO commonCodeVO) {
		return commonCodeMapper.selectCmnCdGrpDetailList(commonCodeVO);
	}

	public Map<String, Object> selectCmnCdGrpDetailList(Map<Object, Object> inputMap) throws Exception {
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		List<CommonCodeVO> cmnCdGrpList	= null;
		Map<String, Object> map			= new HashMap<String, Object>();
		Map<String, Object> listMap		= new HashMap<String, Object>();

		int totalCnt					= 0;
		String mainMenu					= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")	, "");
		String subMenu					= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")	, "");
		String grpCdList				= StringUtils.defaultIfEmpty((String) inputMap.get("grpCdList")	, "");
		String detailYn					= StringUtils.defaultIfEmpty((String) inputMap.get("detailYn")	, "N");
		String[] grpCdListArr			= grpCdList.split(",");
		
		if(StringUtils.isEmpty(grpCdList))
			throw new Exception(StaticObject.Message.DATA_SEARCH_FAILED);
		
		grpCdList = "";
		for(int i = 0; i < grpCdListArr.length; i++) {
			String grpCd = grpCdListArr[i].trim();
			grpCdList += StringUtils.isEmpty(grpCdList) ? "'" + grpCd + "'" : ",'" + grpCd + "'";
		}
		
		commonCodeVO.setGrpCdList(grpCdList);
		commonCodeVO.setDetailYn(detailYn);
		commonCodeVO.setDelYn("N");
		
		cmnCdGrpList = this.selectCmnCdGrpDetailList(commonCodeVO);
		totalCnt += cmnCdGrpList.size();
		
		if(cmnCdGrpList != null) {
			for(int i = 0; i < grpCdListArr.length; i++) {
				String grpCd = grpCdListArr[i].trim();
				List<CommonCodeVO> tmpList = new ArrayList<CommonCodeVO>();

				for(CommonCodeVO vo : cmnCdGrpList) {
					if((grpCd).equals(vo.getGrpCd()))
						tmpList.add(vo);
				}
				
				listMap.put(grpCd, tmpList);
			}
		}
		
		map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		map.put("mainMenu"		, mainMenu);
		map.put("subMenu"		, subMenu);
		map.put("totalCnt"		, totalCnt);
		map.put("data"			, listMap);
		
		return map;
	}
}
