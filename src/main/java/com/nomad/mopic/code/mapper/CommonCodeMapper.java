package com.nomad.mopic.code.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.CommonCodeVO;

@Repository(value = "commonCodeMapper")
public interface CommonCodeMapper {
	public List<CommonCodeVO> selectCmnCdList(CommonCodeVO commonCodeVO);
	public CommonCodeVO selectCmnCdDetail(CommonCodeVO commonCodeVO);
	public boolean insertCmnCd(CommonCodeVO commonCodeVO);
	public boolean updateCmnCd(CommonCodeVO commonCodeVO);
	public List<CommonCodeVO> selectCmnGrpList(CommonCodeVO commonCodeVO);
	public boolean insertCmnGrp(CommonCodeVO commonCodeVO);
	public boolean updateCmnGrp(CommonCodeVO commonCodeVO);
	public List<CommonCodeVO> selectCmnCdGrpDetailList(CommonCodeVO commonCodeVO);
}
