package com.nomad.mopic.code.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.vo.CommonCodeVO;

@Controller
public class CommonCodeController {
	private static final Logger logger = LoggerFactory.getLogger(CommonCodeController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	public CommonCodeService commonCodeService;
	
	/**
	 * 공통코드 목록
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectCmnCdList", method = RequestMethod.POST)
	public View selectCmnCdList(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		List<CommonCodeVO> cmnCdList	= null;

		int totalCnt		= 0;
		int pageNumber		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("pageNumber")	, "1"));
		int rowCount		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("rowCount")		, "9999999"));
		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")						, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")						, "");
		
		String grpCd		= StringUtils.defaultIfEmpty((String) inputMap.get("grpCd")							, "0");
		String cd			= StringUtils.defaultIfEmpty((String) inputMap.get("cd")							, "0");
		String grpName		= StringUtils.defaultIfEmpty((String) inputMap.get("grpName")						, "");
		String cdName		= StringUtils.defaultIfEmpty((String) inputMap.get("cdName")						, "");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")							, "A");
		String detailYn		= StringUtils.defaultIfEmpty((String) inputMap.get("detailYn")						, "N");
		//System.out.println("=================================grpCd : " + grpCd);
		try {
			commonCodeVO.setGrpCd(grpCd);
			commonCodeVO.setCd(cd);
			commonCodeVO.setGrpName(grpName);
			commonCodeVO.setCdName(cdName);
			commonCodeVO.setDelYn(delYn);
			commonCodeVO.setDetailYn(detailYn);
			commonCodeVO.setPageNumber(pageNumber);
			commonCodeVO.setRowCount(rowCount);
			
			cmnCdList	= commonCodeService.selectCmnCdList(commonCodeVO);
			totalCnt	= (Integer) (cmnCdList.size() > 0 ? cmnCdList.get(0).getTotCnt() : 0);
				
			/*
			if(logger.isDebugEnabled()) {
				for(CommonCodeVO vo : cmnCdList) {
					logger.debug("↓========================================↓");
					logger.debug(vo.getCdName());
					logger.debug("↑========================================↑");
				}
			}
			*/
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, cmnCdList);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드 상세
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectCmnCdDetail", method = RequestMethod.POST)
	public View selectCmnCdDetail(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map		= new HashMap<String, Object>();
		CommonCodeVO commonCodeVO	= new CommonCodeVO();

		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")	, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")	, "");
		
		String grpCd		= StringUtils.defaultIfEmpty((String) inputMap.get("grpCd")		, "0");
		String cd			= StringUtils.defaultIfEmpty((String) inputMap.get("cd")		, "0");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")		, "A");
		String detailYn		= StringUtils.defaultIfEmpty((String) inputMap.get("detailYn")	, "N");
		
		try {
			commonCodeVO.setGrpCd(grpCd);
			commonCodeVO.setCd(cd);
			commonCodeVO.setDelYn(delYn);
			commonCodeVO.setDetailYn(detailYn);
			
			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("data"			, commonCodeVO);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드 추가
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/insertCmnCd", method = RequestMethod.POST)
	public View insertCmnCd(@RequestBody Map<Object, Object> inputMap, HttpServletRequest request, ModelMap modelMap) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		Map<String, Object> map			= new HashMap<String, Object>();

		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")				, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")				, "");
		
		String grpCd		= StringUtils.defaultIfEmpty((String) inputMap.get("grpCd")					, "0");
		String cdName		= StringUtils.defaultIfEmpty((String) inputMap.get("cdName")				, "");
		String cdDesc		= StringUtils.defaultIfEmpty((String) inputMap.get("cdDesc")				, "");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")					, "N");
		String ref1			= StringUtils.defaultIfEmpty((String) inputMap.get("ref1")					, "");
		String ref2			= StringUtils.defaultIfEmpty((String) inputMap.get("ref2")					, "");
		String ref3			= StringUtils.defaultIfEmpty((String) inputMap.get("ref3")					, "");
		String ref4			= StringUtils.defaultIfEmpty((String) inputMap.get("ref4")					, "");
		String ref5			= StringUtils.defaultIfEmpty((String) inputMap.get("ref5")					, "");
		String regId		= sessionMap.get("userId");
		
		try {
			// 권한체크(추가예정)

			commonCodeVO.setGrpCd(grpCd);
			commonCodeVO.setCdName(cdName);
			commonCodeVO.setCdDesc(cdDesc);
			commonCodeVO.setDelYn(delYn);
			commonCodeVO.setRef1(ref1);
			commonCodeVO.setRef2(ref2);
			commonCodeVO.setRef3(ref3);
			commonCodeVO.setRef4(ref4);
			commonCodeVO.setRef5(ref5);
			commonCodeVO.setRegId(regId);
			commonCodeVO.setUpdId(regId);
			
			commonCodeService.insertCmnCd(commonCodeVO);
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_INSERT_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드 업데이트
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/updateCmnCd", method = RequestMethod.POST)
	public View updateCmnCd(@RequestBody Map<Object, Object> inputMap, HttpServletRequest request, ModelMap modelMap) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		Map<String, Object> map			= new HashMap<String, Object>();

		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")				, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")				, "");
		
		int sort			= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sort")	, "0"));
		String cd			= StringUtils.defaultIfEmpty((String) inputMap.get("cd")					, "0");
		String cdName		= StringUtils.defaultIfEmpty((String) inputMap.get("cdName")				, "");
		String cdDesc		= StringUtils.defaultIfEmpty((String) inputMap.get("cdDesc")				, "");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")					, "");
		String ref1			= StringUtils.defaultIfEmpty((String) inputMap.get("ref1")					, "");
		String ref2			= StringUtils.defaultIfEmpty((String) inputMap.get("ref2")					, "");
		String ref3			= StringUtils.defaultIfEmpty((String) inputMap.get("ref3")					, "");
		String ref4			= StringUtils.defaultIfEmpty((String) inputMap.get("ref4")					, "");
		String ref5			= StringUtils.defaultIfEmpty((String) inputMap.get("ref5")					, "");
		String regId		= sessionMap.get("userId");
		
		try {
			// 권한체크(추가예정)

			commonCodeVO.setCd(cd);
			commonCodeVO.setUpdId(regId);
			
			if(sort > 0)
				commonCodeVO.setSort(sort);
			if(!StringUtils.isEmpty(cdName))
				commonCodeVO.setCdName(cdName);
			if(!StringUtils.isEmpty(cdDesc))
				commonCodeVO.setCdDesc(cdDesc);
			if(!StringUtils.isEmpty(delYn))
				commonCodeVO.setDelYn(delYn);
			if(!StringUtils.isEmpty(ref1))
				commonCodeVO.setRef1(ref1);
			if(!StringUtils.isEmpty(ref2))
				commonCodeVO.setRef2(ref2);
			if(!StringUtils.isEmpty(ref3))
				commonCodeVO.setRef3(ref3);
			if(!StringUtils.isEmpty(ref4))
				commonCodeVO.setRef4(ref4);
			if(!StringUtils.isEmpty(ref5))
				commonCodeVO.setRef5(ref5);
			
			commonCodeService.updateCmnCd(commonCodeVO);
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_INSERT_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드그룹 목록
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectCmnCdGrpList", method = RequestMethod.POST)
	public View selectCmnCdGrpList(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		List<CommonCodeVO> cmnCdGrpList	= null;
		Map<String, Object> map			= new HashMap<String, Object>();

		int totalCnt		= 0;
		int pageNumber		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("pageNumber")	, "1"));
		int rowCount		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("rowCount")		, "9999999"));
		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")						, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")						, "");
		
		String grpCd		= StringUtils.defaultIfEmpty((String) inputMap.get("grpCd")							, "0");
		String grpName		= StringUtils.defaultIfEmpty((String) inputMap.get("grpName")						, "");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")							, "A");
		
		try {
			commonCodeVO.setGrpCd(grpCd);
			commonCodeVO.setGrpName(grpName);
			commonCodeVO.setDelYn(delYn);
			commonCodeVO.setPageNumber(pageNumber);
			commonCodeVO.setRowCount(rowCount);
			
			cmnCdGrpList = commonCodeService.selectCmnGrpList(commonCodeVO);
			totalCnt	= (Integer) (cmnCdGrpList.size() > 0 ? cmnCdGrpList.get(0).getTotCnt() : 0);
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, cmnCdGrpList);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드그룹 목록
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectCmnCdGrpDetailList", method = RequestMethod.POST)
	public View selectCmnCdGrpDetailList(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map			= new HashMap<String, Object>();
		
		try {
			map = commonCodeService.selectCmnCdGrpDetailList(inputMap);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드그룹 추가
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/insertCmnGrp", method = RequestMethod.POST)
	public View insertCmnGrp(@RequestBody Map<Object, Object> inputMap, HttpServletRequest request, ModelMap modelMap) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		Map<String, Object> map			= new HashMap<String, Object>();

		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")				, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")				, "");
		
		String grpName		= StringUtils.defaultIfEmpty((String) inputMap.get("grpName")				, "");
		String grpDesc		= StringUtils.defaultIfEmpty((String) inputMap.get("grpDesc")				, "");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")					, "N");
		String ref1			= StringUtils.defaultIfEmpty((String) inputMap.get("ref1")					, "");
		String ref2			= StringUtils.defaultIfEmpty((String) inputMap.get("ref2")					, "");
		String ref3			= StringUtils.defaultIfEmpty((String) inputMap.get("ref3")					, "");
		String ref4			= StringUtils.defaultIfEmpty((String) inputMap.get("ref4")					, "");
		String ref5			= StringUtils.defaultIfEmpty((String) inputMap.get("ref5")					, "");
		String regId		= sessionMap.get("userId");
		
		try {
			// 권한체크(추가예정)
			
			commonCodeVO.setGrpName(grpName);
			commonCodeVO.setGrpDesc(grpDesc);
			commonCodeVO.setDelYn(delYn);
			commonCodeVO.setRef1(ref1);
			commonCodeVO.setRef2(ref2);
			commonCodeVO.setRef3(ref3);
			commonCodeVO.setRef4(ref4);
			commonCodeVO.setRef5(ref5);
			commonCodeVO.setRegId(regId);
			commonCodeVO.setUpdId(regId);
			
			commonCodeService.insertCmnGrp(commonCodeVO);
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_INSERT_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 공통코드그룹 업데이트
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/updateCmnGrp", method = RequestMethod.POST)
	public View updateCmnGrp(@RequestBody Map<Object, Object> inputMap, HttpServletRequest request, ModelMap modelMap) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		CommonCodeVO commonCodeVO		= new CommonCodeVO();
		Map<String, Object> map			= new HashMap<String, Object>();

		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")				, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")				, "");
		
		int sort			= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sort")	, "0"));
		String grpCd		= StringUtils.defaultIfEmpty((String) inputMap.get("grpCd")					, "");
		String grpName		= StringUtils.defaultIfEmpty((String) inputMap.get("grpName")				, "");
		String grpDesc		= StringUtils.defaultIfEmpty((String) inputMap.get("grpDesc")				, "");
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")					, "");
		String ref1			= StringUtils.defaultIfEmpty((String) inputMap.get("ref1")					, "");
		String ref2			= StringUtils.defaultIfEmpty((String) inputMap.get("ref2")					, "");
		String ref3			= StringUtils.defaultIfEmpty((String) inputMap.get("ref3")					, "");
		String ref4			= StringUtils.defaultIfEmpty((String) inputMap.get("ref4")					, "");
		String ref5			= StringUtils.defaultIfEmpty((String) inputMap.get("ref5")					, "");
		String regId		= sessionMap.get("userId");
		
		try {
			// 권한체크(추가예정)

			if(StringUtils.isEmpty(grpCd)) throw new Exception(StaticObject.Message.DATA_MODIFY_FAILED);
			
			commonCodeVO.setGrpCd(grpCd);
			commonCodeVO.setUpdId(regId);
			
			if(sort > 0)
				commonCodeVO.setSort(sort);
			if(!StringUtils.isEmpty(grpName))
				commonCodeVO.setGrpName(grpName);
			if(!StringUtils.isEmpty(grpDesc))
				commonCodeVO.setGrpDesc(grpDesc);
			if(!StringUtils.isEmpty(delYn))
				commonCodeVO.setDelYn(delYn);
			if(!StringUtils.isEmpty(ref1))
				commonCodeVO.setRef1(ref1);
			if(!StringUtils.isEmpty(ref2))
				commonCodeVO.setRef2(ref2);
			if(!StringUtils.isEmpty(ref3))
				commonCodeVO.setRef3(ref3);
			if(!StringUtils.isEmpty(ref4))
				commonCodeVO.setRef4(ref4);
			if(!StringUtils.isEmpty(ref5))
				commonCodeVO.setRef5(ref5);
			
			commonCodeService.updateCmnGrp(commonCodeVO);
	
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_INSERT_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
}
