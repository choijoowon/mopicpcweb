package com.nomad.mopic.saving.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.SavingVO;

@Repository(value = "savingMapper")
public interface SavingMapper {
	public void insertSavingData(SavingVO vo);
	public void insertSavingChargeData(SavingVO vo);
	public ArrayList<Map<String, Object>> selectSavingListCnt(SavingVO savingVO);
	public List<SavingVO> selectSavingList(SavingVO savingVO);
	public Map<String, Object> selectMySavingDetail(SavingVO savingVO);
	public SavingVO selectSavingDetail(SavingVO savingVO);
	public List<?> selectMySavingList(Map<Object, Object> map);
	public List<CommonCodeVO> selectSavingBankList(Map<String, String> paramMap);
	public void deleteSavingData(SavingVO savingVO);
	public void deleteSavingChargeData(SavingVO savingVO);
	public void updateSavingUseYn(SavingVO vo);
}