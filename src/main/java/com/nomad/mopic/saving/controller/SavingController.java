package com.nomad.mopic.saving.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.saving.service.SavingService;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.SavingVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class SavingController {
	private static final Logger logger = LoggerFactory.getLogger(SavingController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	public SavingService savingService;
	
	@Autowired
	public CommonService commonService;

	/**
	 * 예금 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saving/deposit")
	public String deposit(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);
	
		return "savings-page/savings-page";
	}
	
	/**
	 * 적금 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saving/saving")
	public String saving(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);
		
		return "savings-page/savings-page";
	}
	
	/**
	 * MMDA 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saving/mmda")
	public String mmda(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);
		
		return "savings-page/savings-page";
	}
	
	/**
	 * 주택청약 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/saving/house")
	public String house(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);
		
		return "savings-page/savings-page";
	}

	/**
	 * 예금 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/saving/deposit/detail", method = RequestMethod.GET)
	public String depositDetail(HttpServletRequest request, SavingVO vo, Locale locale, Model model) {
		SavingVO savingVO = new SavingVO();
		String acCode		= request.getParameter("AC_CODE");
		
		try {
			vo.setAC_CODE(acCode);
			savingVO = savingService.selectSavingDetail(vo);
			
			model.addAttribute("data"		, savingVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "savings-page/deposit-detail-pop";
	}

	/**
	 * 적금 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/saving/saving/detail", method = RequestMethod.GET)
	public String savingDetail(HttpServletRequest request, SavingVO vo, Locale locale, Model model) {
		SavingVO savingVO = new SavingVO();
		String acCode		= request.getParameter("AC_CODE");
		
		try {
			vo.setAC_CODE(acCode);
			savingVO = savingService.selectSavingDetail(vo);
			
			model.addAttribute("data"		, savingVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "savings-page/saving-detail-pop";
	}

	/**
	 * mmda 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/saving/mmda/detail", method = RequestMethod.GET)
	public String mmdaDetail(HttpServletRequest request, SavingVO vo, Locale locale, Model model) {
		SavingVO savingVO = new SavingVO();
		String acCode		= request.getParameter("AC_CODE");
		
		try {
			vo.setAC_CODE(acCode);
			savingVO = savingService.selectSavingDetail(vo);
			
			model.addAttribute("data"		, savingVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "savings-page/mmda-detail-pop";
	}

	/**
	 * 청약 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/saving/house/detail", method = RequestMethod.GET)
	public String houseDetail(HttpServletRequest request, SavingVO vo, Locale locale, Model model) {
		SavingVO savingVO = new SavingVO();
		String acCode		= request.getParameter("AC_CODE");
		
		try {
			vo.setAC_CODE(acCode);
			savingVO = savingService.selectSavingDetail(vo);
			
			model.addAttribute("data"		, savingVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "savings-page/house-detail-pop";
	}

	
	
	
	/*****************************************************************************************************************************************/
	/* 이하 API 영역 
	/*****************************************************************************************************************************************/

	/**
	 * 예적금 리스트 가져오기
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/scrapSavingList", method = RequestMethod.GET)
	public View scrapSavingList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();
		
		map = savingService.scrapSavingList();
		
		modelMap.put("result", map);
		
		return jsonView;
	}

	/**
	 * 예적금 수수료 리스트 가져오기
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/scrapSavingChargeList", method = RequestMethod.GET)
	public View scrapSavingChargeList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();

		map = savingService.scrapSavingChargeList();
		
		modelMap.put("result", map);
		
		return jsonView;
	}
	
	/**
	 * 2시 정각마다 실행
	 */
	public void scrapSavingChargeListBatch() {
		Map<String, Object> map	= new HashMap<String, Object>();
		
		map = savingService.scrapSavingChargeList();
	}

	/**
	 * 예금 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/selectDepositList", method = RequestMethod.POST)
	public View selectSavingList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap   		= CommonUtil.getSession(request);
		SavingVO savingVO						= new SavingVO();
		
		savingVO.setDS_CODE("A000101");
		savingVO.setUserId(sessionMap.get("userId"));
		
		savingService.selectSavingList(inputMap, modelMap, savingVO);
		
		return jsonView;
	}

	/**
	 * 적금 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/selectSavingList", method = RequestMethod.POST)
	public View selectInstallmentSavingList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap   		= CommonUtil.getSession(request);
		SavingVO savingVO						= new SavingVO();
		
		savingVO.setDS_CODE("A000102");
		savingVO.setUserId(sessionMap.get("userId"));
		
		savingService.selectSavingList(inputMap, modelMap, savingVO);
		
		return jsonView;
	}

	/**
	 * MMDA 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/selectMmdaList", method = RequestMethod.POST)
	public View selectMmdaList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap   		= CommonUtil.getSession(request);
		SavingVO savingVO						= new SavingVO();
		
		savingVO.setDS_CODE("A000103");
		savingVO.setUserId(sessionMap.get("userId"));
		
		savingService.selectSavingList(inputMap, modelMap, savingVO);
		
		return jsonView;
	}

	/**
	 * 주택청약 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/selectHouseList", method = RequestMethod.POST)
	public View selectApplList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap   		= CommonUtil.getSession(request);
		SavingVO savingVO						= new SavingVO();
		
		savingVO.setDS_CODE("A000104");
		savingVO.setUserId(sessionMap.get("userId"));
		
		savingService.selectSavingList(inputMap, modelMap, savingVO);
		
		return jsonView;
	}

	/**
	 * 예/적금 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectSavingDetail", method = RequestMethod.POST)
	public View selectSavingDetail(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		SavingVO savingVO				= new SavingVO();
		String acCode					= StringUtils.defaultIfEmpty((String) inputMap.get("acCode"), "");
		
		try {
			if(StringUtils.isEmpty(acCode))
				throw new Exception(StaticObject.Message.DATA_SEARCH_FAILED);

			savingVO.setUserId(sessionMap.get("userId"));
			savingVO.setAC_CODE(acCode);
			
			savingVO = savingService.selectSavingDetail(savingVO);
			CommonUtil.makeProductResponseMap("200", inputMap, savingVO, null, modelMap);
		} catch(Exception e) {
			modelMap.put("resultMessage", e.getLocalizedMessage());
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		return jsonView;
	}

	/**
	 * 예/적금 금융기관목록(팝업)
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/selectSavingBankList", method = RequestMethod.POST)
	public View selectSavingBankList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		List<CommonCodeVO> bankList		= null;
		Map<String, Object> map			= new HashMap<String, Object>();
		Map<String, String> paramMap	= new HashMap<String, String>();
		
		int totalCnt	= 0;
		String category	= StringUtils.defaultIfEmpty((String) inputMap.get("category"), "");
		
		try {
			paramMap.put("category", category);
			
			bankList	= savingService.selectSavingBankList(paramMap);
			
			totalCnt	= bankList != null ? bankList.size() : 0;

			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, bankList);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}

	/**
	 * 예/적금 상품목록(팝업)
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/filterProductList", method = RequestMethod.POST)
	public View selectSavingProductList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		Map<String, Object> map			= new HashMap<String, Object>();
		SavingVO savingVO				= new SavingVO();
		List<SavingVO> productList		= null;
		int totalCnt					= 0;
		
		String dsCode			= StringUtils.defaultIfEmpty((String) inputMap.get("dsCode")		, "");
		String dsBankingCode	= StringUtils.defaultIfEmpty((String) inputMap.get("dsBankingCode")	, "");
		
		try {
			if(!StringUtils.isEmpty(dsCode))
				savingVO.setDS_CODE(dsCode);
			if(!StringUtils.isEmpty(dsBankingCode))
				savingVO.setDS_BANKING(dsBankingCode);
			savingVO.setPageNumber(1);
			savingVO.setRowCount(99999);
			savingVO.setUserId(sessionMap.get("userId"));
			
			productList = savingService.selectSavingList(savingVO);
			totalCnt = productList != null ? productList.size() : 0;

			map.put("data"			, productList);
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, totalCnt);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}

	/**
	 * 예/적금 상품등록(보류)
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/saving/insertMySavingDetail", method = RequestMethod.POST)
	public View insertMySavingDetail(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		Map<String, String> paramMap	= new HashMap<String, String>();
		Map<String, Object> map			= new HashMap<String, Object>();
		SavingVO savingVO				= new SavingVO();
		int totalCnt					= 0;
		//, #{product_type}
		
		int prodType		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("prodType")	, "0"));
		String user_id		= sessionMap.get("user_id");
		String prod_code	= StringUtils.defaultIfEmpty((String) inputMap.get("prod_code")					, "");
		String product_type	= StringUtils.defaultIfEmpty((String) inputMap.get("product_type")				, "");
		String amount		= StringUtils.defaultIfEmpty((String) inputMap.get("amount")					, "");
		String term_cd		= StringUtils.defaultIfEmpty((String) inputMap.get("term_cd")					, "");
		String interest		= StringUtils.defaultIfEmpty((String) inputMap.get("interest")					, "");
		
		try {
			paramMap.put("user_id"		, user_id);
			paramMap.put("product_type"	, product_type);
			paramMap.put("prod_code"	, prod_code);
			paramMap.put("amount"		, amount);
			paramMap.put("term_cd"		, term_cd);
			paramMap.put("interest"		, interest);
			
			//savingService.insertMySavingDetail(paramMap);

			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, totalCnt);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}
	
	
}
