package com.nomad.mopic.saving.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.saving.mapper.SavingMapper;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.SavingVO;

@Service
public class SavingService implements SavingMapper {
	@Resource(name = "savingMapper")
    private SavingMapper savingMapper;
	
	@Value("#{periodcode}")
	private Properties periodCode;

	@Value("#{savingtarget}")
	private Properties savingTarget;

	public Map<String, Object> scrapSavingList() {
		Map<String, Object> map	= new HashMap<String, Object>();
		String url		= StaticObject.Url.URL_TOUCHBANK_SERVER;
		String param	= "/api/Account.do";
		String result	= "";
		int scrapCnt	= 0;
		int scrapLimit	= 5;
		
		try {
			for(int i = 0; i < scrapLimit; i++) {
				result = CommonUtil.urlConnect(url, param, "UTF-8", null);
				
				ObjectMapper om = new ObjectMapper();
				JsonNode node = om.readTree(result);
				ArrayList<SavingVO> savingList	= om.readValue(node.get("result").get("list").toString(), new TypeReference<ArrayList<SavingVO>>(){});
				
				if(savingList.size() > 0) {
					SavingVO savingVO = new SavingVO();
					this.deleteSavingData(savingVO);
					
					for(SavingVO vo : savingList) {
						if(("Y").equals(vo.getDEL_YN())) {
							SavingVO tmpVO = new SavingVO();
							tmpVO.setAC_CODE(vo.getAC_CODE());
							tmpVO.setUSE_YN("N");
							this.updateSavingUseYn(tmpVO);
						} else {
							this.insertSavingData(vo);
						}
						
						scrapCnt++;
					}
					
					break;
				}
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, scrapCnt);
			map.put("data"			, "");
		} catch(Exception e) {
			e.printStackTrace();
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return map;
	}
	
	public Map<String, Object> scrapSavingChargeList() {
		Map<String, Object> map	= new HashMap<String, Object>();
		String url		= StaticObject.Url.URL_TOUCHBANK_SERVER;
		String param	= "/api/Account_charge.do";
		String result	= "";
		int scrapCnt	= 0;
		int scrapLimit	= 5;
		
		try {
			for(int i = 0; i < scrapLimit; i++) {
				result = CommonUtil.urlConnect(url, param, "UTF-8", null);
				
				ObjectMapper om = new ObjectMapper();
				JsonNode node = om.readTree(result);
				ArrayList<SavingVO> savingList	= om.readValue(node.get("result").get("list").toString(), new TypeReference<ArrayList<SavingVO>>(){});
				
				if(savingList.size() > 0) {
					SavingVO savingVO = new SavingVO();
					this.deleteSavingChargeData(savingVO);
					
					for(SavingVO vo : savingList) {
						this.insertSavingChargeData(vo);
						scrapCnt++;
					}
					
					break;
				}
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, scrapCnt);
			map.put("data"			, "");
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return map;
	}
	
	public void updateSavingUseYn(SavingVO vo) {
		savingMapper.updateSavingUseYn(vo);
	}
	
	public void insertSavingData(SavingVO vo) {
		savingMapper.insertSavingData(vo);
	}

	public void insertSavingChargeData(SavingVO vo) {
		savingMapper.insertSavingChargeData(vo);
	}

	public ArrayList<Map<String, Object>> selectSavingListCnt(SavingVO savingVO) {
		return savingMapper.selectSavingListCnt(savingVO);
	}

	public List<SavingVO> selectSavingList(SavingVO savingVO) {
		return savingMapper.selectSavingList(savingVO);
	}

	public Map<String, Object> selectMySavingDetail(SavingVO savingVO) {
		return savingMapper.selectMySavingDetail(savingVO);
	}

	public List<?> selectMySavingList(Map<Object, Object> map) {
		return savingMapper.selectMySavingList(map);
	}

	public void selectSavingList(Map<Object, Object> inputMap, ModelMap modelMap, SavingVO savingVO) {
		ArrayList<Map<String, Object>> cntList	= new ArrayList<Map<String, Object>>();
		List<SavingVO> savingList				= null;
		
		int pageNum			= 0;
		int total_count		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("total_count")	, "1"));
		int rowCount		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("rowCount")		, "15"));
		pageNum 			= (int) Math.floor(total_count/rowCount) + 1;
		
		int period			= 0;
		int amount			= 0;
		int standardTax		= 0;
		String amountStr	= StringUtils.defaultIfEmpty((String) inputMap.get("amount")		, "0");
		String searchOption	= StringUtils.defaultIfEmpty((String) inputMap.get("searchOption")	, "NORMAL");
		String dsFnCode		= StringUtils.defaultIfEmpty((String) inputMap.get("filter")		, "A");
		String searchKey	= StringUtils.defaultIfEmpty((String) inputMap.get("id")			, "");
		String searchOrder	= StringUtils.defaultIfEmpty((String) inputMap.get("sorting")		, "RATE");
		String periodCd		= StringUtils.defaultIfEmpty((String) inputMap.get("periodCd")		, "");
		String periodcd		= StringUtils.defaultIfEmpty((String) inputMap.get("periodcd")		, "");
		String afterTax		= StringUtils.defaultIfEmpty((String) inputMap.get("afterTax")		, "0");
		String aftertax		= StringUtils.defaultIfEmpty((String) inputMap.get("aftertax")		, "0");
		
		String dsLocal		= StringUtils.defaultIfEmpty((String) inputMap.get("DS_LOCAL")		, "");
		String dsTarget		= StringUtils.defaultIfEmpty((String) inputMap.get("DS_TARGET")		, "");
		
		if(StringUtils.isEmpty(periodCd)) {
			periodCd		= periodcd;
			searchOption	= "COMPARE";
			afterTax		= aftertax;
		}
		
		period	= this.getSavingMonth(periodCd);

		try {
			/*
			if(("COMPARE").equals(searchOption)) {
				savingVO.setSearchKey(searchKey);
				Map<String, Object> myProd	= this.selectMySavingDetail(savingVO);
				
				savingVO.setAC_CODE(myProd.get("prodCode").toString());
				
				amount		= Integer.parseInt(myProd.get("amount").toString());
				standardTax	= Integer.parseInt(myProd.get("afterTax").toString());
			} else {
				amountStr	= CommonUtil.removeComma(amountStr);
				amount		= Integer.parseInt(amountStr);
			}
			*/
			standardTax	= Integer.parseInt(afterTax);
			amountStr	= CommonUtil.removeComma(amountStr);
			amount		= Integer.parseInt(amountStr) * 10000;
			
			//if(amount % 5000 > 0) throw new Exception(StaticObject.Message.AMOUNT_UNIT_5000);
			
			if(!StringUtils.isEmpty(searchKey))
				savingVO.setSearchKey(searchKey);
			savingVO.setSearchOption(searchOption);
			savingVO.setAmount(amount);
			savingVO.setPeriod(period);
			savingVO.setDS_FN_CODE(dsFnCode);
			savingVO.setPageNumber(pageNum);
			savingVO.setRowCount(rowCount);
			savingVO.setSearchOrder(searchOrder);
			savingVO.setPeriodCd(periodCd);
			savingVO.setDS_LOCAL(dsLocal);
			savingVO.setDS_TARGET(dsTarget);
			
			cntList = this.selectSavingListCnt(savingVO);
			
			savingList = this.selectSavingList(savingVO);
			
			// 그룹설정
			inputMap.put("best_yn","N");
			for(SavingVO data : savingList) {
				// best 설정
				if(total_count == 0) {
					String dsPreTax = StringUtils.defaultIfEmpty(data.getDS_PRE_TAX(), "0");
					String baseRate = StringUtils.defaultIfEmpty(data.getBASE_RATE(), "0");
					if(("1").equals(data.getRnum()) && Double.parseDouble(dsPreTax) <= Double.parseDouble(baseRate)) {
						inputMap.put("best_yn","Y");
					}
				}
				
				// 가입대상설정
				data.setDS_TARGET_NM(this.replaceTarget(data.getDS_TARGET()));
				
				// 그룹설정
				if(standardTax > 0) {
					if(standardTax > data.getAfterTax())
						data.setAC_GROUP("HIGH");
					else
						data.setAC_GROUP("LOW");
				} else {
					data.setAC_GROUP("");
				}
			}
			
			CommonUtil.makeProductResponseMap("200", inputMap, savingList, cntList, modelMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public SavingVO selectSavingDetail(SavingVO savingVO) {
		SavingVO returnVO = savingMapper.selectSavingDetail(savingVO);
		returnVO.setDS_TARGET_NM(this.replaceTarget(returnVO.getDS_TARGET()));
		
		return returnVO;
	}
	
	public int getSavingMonth(String period) {
		if(StringUtils.isEmpty(period))
			return 12;
		
		int savingMonth = 0;
		savingMonth = Integer.parseInt(StringUtils.defaultIfEmpty((String) periodCode.get(period), "0"));
		
		return savingMonth;
	}

	public List<CommonCodeVO> selectSavingBankList(Map<String, String> paramMap) {
		return savingMapper.selectSavingBankList(paramMap);
	}

	public void deleteSavingData(SavingVO savingVO) {
		savingMapper.deleteSavingData(savingVO);
	}

	public void deleteSavingChargeData(SavingVO savingVO) {
		savingMapper.deleteSavingChargeData(savingVO);
	}

	public String replaceTarget(String targetCode) {
		if(StringUtils.isEmpty(targetCode))
			return "";
		
		String returnString = "";
		
		if(targetCode.indexOf(",") > 0) {
			String[] targetArr = targetCode.split(",");
			
			for(int i = 0; i < targetArr.length; i++) {
				returnString += ("").equals(returnString) ? savingTarget.getProperty(targetArr[i].trim()) : ", " + savingTarget.getProperty(targetArr[i].trim());
			}
		} else {
			returnString = savingTarget.getProperty(targetCode.trim());
		}
		
		return returnString;
	}
}