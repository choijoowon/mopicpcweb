package com.nomad.mopic.main.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.main.service.MainService;
import com.nomad.mopic.vo.InsureVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MainController {
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	public MainService mainService;
	
	@Autowired
	public CommonService commonService;
	
	/**
	 * index page
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response, Locale locale, ModelMap model) throws Exception{
		
		commonService.setReactSearchQuery(request, model);
		model.addAttribute("requestUrl", request.getRequestURI());		
		
		model.addAttribute("today_product",mainService.selectTodayProductList(null));
		model.addAttribute("new_product",mainService.selectNewProductList(null));
		
		response.setHeader("Cache-Control","");
		response.setHeader("Pragma","");
		response.setDateHeader("Expires",0);
		if (request.getProtocol().equals("HTTP/1.1"))
		        response.setHeader("Cache-Control", "");

		return "main-page/main-page";
	}
	
	/**
	 * main Today Product Page
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/main/bottom", method = RequestMethod.GET)
	public String mainTodayProductPage(HttpServletRequest request, Locale locale, ModelMap model) throws Exception{
		
		model.addAttribute("today_product",mainService.selectTodayProductList(null));
		model.addAttribute("new_product",mainService.selectNewProductList(null));
		
		return "main-page/main-bottom-page";
	}
			
	@RequestMapping(value = "/yellopass/redirect")
	public String test(Locale locale, Model model) {
		
		return "popupRedirect";
	}
	
	/**
	 * 수수료 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/etc/selectAccountCharge", method = RequestMethod.POST)
	public View selectAccountCharge(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, Locale locale, Model model) {
		
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		
		try {
			
			returnMap.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			returnMap.put("data"		, mainService.selectChargeSavings(inputMap));
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	/**
	 * 신상품 목록 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/main/selectNewProduct", method = RequestMethod.POST)
	public View selectNewProduct(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, Locale locale, Model model) {
		
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		
		try {
			
			returnMap.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			returnMap.put("data"		, null);
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	/**
	 * 신상품 목록 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/main/selectRecommendProduct", method = RequestMethod.POST)
	public View selectRecommendProduct(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, Locale locale, Model model) {
		
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		
		try {
			
			returnMap.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			returnMap.put("data"		, null);
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	@RequestMapping(value = "/favicon.ico", method = RequestMethod.GET)
	public String favicon(HttpServletRequest request, Locale locale, ModelMap model) throws Exception{
		
		return "forward:/resources/images/favicon.ico";
	}
	
	@RequestMapping(value = "/main/whatismopic")
	public String whatismopic(Locale locale, Model model) {
		
		return "main-page/whatismopic";
	}
	
	@RequestMapping(value = "/main/serviceTerm")
	public String serviceTerm(Locale locale, Model model) {
		
		return "main-page/serviceTerm";
	}
	
	@RequestMapping(value = "/main/privacy")
	public String serviceContract(Locale locale, Model model) {
		
		return "main-page/privacy";
	}
	
	/**
	 * index page
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/checkmem", method = RequestMethod.GET)
	public String checkmem(HttpServletRequest request, Locale locale, ModelMap model) throws Exception{
		
		return "main-page/vm_memory";
	}
}
