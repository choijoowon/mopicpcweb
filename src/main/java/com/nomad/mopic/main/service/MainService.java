package com.nomad.mopic.main.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nomad.mopic.main.mapper.MainMapper;

@Service
public class MainService implements MainMapper {
	@Resource(name = "mainMapper")
    private MainMapper mainMapper;
	
	@Override
	public String selectTestDate() {
		return mainMapper.selectTestDate();
	}
	
	@Override
	public List selectTodayProductList(Map searchQuery) {
		return mainMapper.selectTodayProductList(searchQuery);
	}
	
	@Override
	public List selectNewProductList(Map searchQuery) {
		return mainMapper.selectNewProductList(searchQuery);
	}
	
	@Override
	public List selectChargeSavings(Map searchQuery) {
		return mainMapper.selectChargeSavings(searchQuery);
	}
	
	@Override
	public List selectChargeBankList(Map searchQuery) {
		return mainMapper.selectChargeBankList(searchQuery);
	}
}