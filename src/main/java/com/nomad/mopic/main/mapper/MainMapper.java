package com.nomad.mopic.main.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository(value = "mainMapper")
public interface MainMapper {

	public String selectTestDate();
	
	public List selectTodayProductList(Map searchQuery);
	
	public List selectNewProductList(Map searchQuery);

	public List selectChargeSavings(Map searchQuery);
	
	public List selectChargeBankList(Map searchQuery);

}