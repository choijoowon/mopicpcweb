package com.nomad.mopic.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.log.service.LogService;
import com.nomad.mopic.main.service.MainService;
import com.nomad.mopic.mypage.service.MyPageService;

public class DefaultInfoInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(DefaultInfoInterceptor.class);
	
	@Autowired
	CryptoUtil cryptoUtil;

	@Autowired
	private LogService logService;
	
	@Autowired
	public MyPageService mypageService;
	
	@Autowired
	public MainService mainService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		Map<String, String> map			= new HashMap<String, String>();
		HttpSession session				= request.getSession();
		
		// member info
		String userId			= StringUtils.defaultIfEmpty((String) session.getAttribute("userId")		, "");
		String user_level		= StringUtils.defaultIfEmpty((String) session.getAttribute("user_level")	, "");
		String access_token		= StringUtils.defaultIfEmpty((String) session.getAttribute("access_token")	, "");
		String isAdmin			= ("super").equals(cryptoUtil.dec(user_level)) ? "Y" : "N";
		
		if(!StringUtils.isEmpty(userId)) {
			map.put("userId"	, userId);
			map.put("isLogin"	, "Y");
			map.put("access_token"	, access_token);
			
		} else {
			map.put("userId"	, "");
			map.put("isLogin"	, "N");
			map.put("access_token"	, "");
		}
		
		map.put("isAdmin"	, isAdmin);
		map.put("requestUrl", request.getRequestURI());
		
		request.setAttribute("session", map);
		
	    return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		String requestUri		= request.getRequestURI();
		
		if(requestUri.indexOf(".") == -1) {
			Map<String, String> map			= new HashMap<String, String>();
			Map<String, Object> productMap	= new HashMap<String, Object>();
			HttpSession session				= request.getSession();
			
			String userId					= StringUtils.defaultIfEmpty((String) session.getAttribute("userId"), "");
			
			map.put("uri", requestUri);
			map.put("userId", userId);
			
			if(requestUri.indexOf("/admin/") > -1) {
				map.put("page_group", "ADMIN");
			} else if(requestUri.indexOf("/mopic/api/") > -1) {
				map.put("page_group", "API");
			} else {
				map.put("page_group", "FRONT");
				
				// my product info
				if(request.getAttribute("myProduct") == null) {
					if(!StringUtils.isEmpty(userId)) {
						productMap = mypageService.selectMyProductListCnt(request);
					}
					request.setAttribute("myProduct", productMap);
				}
				
				// calc
				request.setAttribute("charageCompanyOpts", mainService.selectChargeBankList(null));
			}
			
			logService.insertVisitLog(map);
		}
	}
}
