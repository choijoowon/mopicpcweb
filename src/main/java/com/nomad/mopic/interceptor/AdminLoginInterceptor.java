package com.nomad.mopic.interceptor;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.common.StaticObject;

public class AdminLoginInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger		= LoggerFactory.getLogger(AdminLoginInterceptor.class);
	
	@Autowired
	CryptoUtil cryptoUtil;
	
	private static final String LOGIN_KEY	= StaticObject.Key.KEY_QUALIFICATION;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession(true);
		String user_level	= cryptoUtil.dec(StringUtils.defaultIfEmpty((String) session.getAttribute(LOGIN_KEY), ""));
		
		if(("super").equals(user_level)){
			CommonUtil.deleteSession(request, response);
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		ModelMap modelMap	= modelAndView.getModelMap();
		String resultCode	= modelMap.get("resultCode").toString();
		String userId		= StringUtils.defaultIfEmpty((String) modelMap.get("userId")		, "");
		String redirect		= StringUtils.defaultIfEmpty((String) modelMap.get("redirectUrl")	, "");
		
		if((StaticObject.Code.CODE_SUCCESS).equals(resultCode)) {
			modelMap.addAttribute("userId", userId);
			modelMap.addAttribute("user_level", cryptoUtil.enc("super"));
			
			CommonUtil.createSession(request, response, modelMap);
			// 저장된 url로 이동(없으면 인덱스)
			String redirectUrl = StringUtils.isEmpty(redirect) ? "/admin/index" : redirect;
			
			response.sendRedirect(redirectUrl);
		} else {
			response.setContentType("text/html; charset=UTF-8"); 
			PrintWriter out = response.getWriter();  
            out.println("<script>alert('" + StaticObject.Message.MEMBER_LOGIN_FAILED + "'); history.go(-1);</script>");
            out.flush(); 
		}
	}
}
