package com.nomad.mopic.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.common.StaticObject;

public class AdminAuthInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(AdminAuthInterceptor.class);
	
	@Autowired
	CryptoUtil cryptoUtil;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession(true);   
		String user_level	= cryptoUtil.dec((String) session.getAttribute(StaticObject.Key.KEY_QUALIFICATION));
		String uri			= request.getRequestURI();
		String param		= request.getQueryString();

		if(param == null || param.equals("null")) {
			param = "";
		} else {
			param = "?" + param;
		}
		
		if(!("super").equals(user_level)){
			// 옐로우 로그인 페이지로 이동
			response.sendRedirect("/admin/login?redirectUrl=" + uri + param);
			
			return false;
		}
		
		return true;
	}
}
