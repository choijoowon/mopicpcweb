package com.nomad.mopic.interceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession();

		if(session.getAttribute(StaticObject.Key.KEY_LOGIN) != null) {
			CommonUtil.deleteSession(request, response);
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		ModelMap modelMap		= modelAndView.getModelMap();
		String resultCode		= modelMap.get("resultCode").toString();
		String userId			= StringUtils.defaultIfEmpty((String) modelMap.get("userId")		, "");
		String redirect			= StringUtils.defaultIfEmpty((String) modelMap.get("redirectUrl")	, "");
		String access_token			= StringUtils.defaultIfEmpty((String) modelMap.get("access_token")	, "");

		if((StaticObject.Code.CODE_SUCCESS).equals(resultCode)) {
			String redirectUrl = StringUtils.isEmpty(redirect) ? "/" : redirect;
			modelMap.addAttribute("userId", userId);
			modelMap.addAttribute("user_level", "");
			modelMap.addAttribute("redirectUrl", redirectUrl);
			modelMap.addAttribute("access_token", access_token);
			
			CommonUtil.createSession(request, response, modelMap);
			// 저장된 url로 이동(없으면 인덱스)
		}
	}
}