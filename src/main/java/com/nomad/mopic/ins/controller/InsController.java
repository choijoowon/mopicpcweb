package com.nomad.mopic.ins.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;

import com.nomad.mopic.card.service.CardService;
import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.ins.service.InsService;
import com.nomad.mopic.loan.service.LoanService;
import com.nomad.mopic.saving.service.SavingService;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.InsureVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class InsController {
	private static final Logger logger = LoggerFactory.getLogger(InsController.class);
	
	@Autowired
	public InsService insService;
	
	@Autowired
	public SavingService savingService;

	@Autowired
	public LoanService loanService;
	
	@Autowired
	public CardService cardService;
	
	@Autowired
	public View jsonView;
	
	@Autowired
	public CommonCodeService commonCodeService;
	
	@Autowired
	public CommonService commonService;
	
	/**
	 * 실손보험 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/shilson")
	public String shilson(HttpServletRequest request, InsureVO vo, Locale locale, ModelMap model)  throws Exception{
		
//		Map<String, String> sessionMap    = CommonUtil.getSession(request);
//		Map<Object, Object> inputMap = new HashMap<Object, Object>();
//		
//		if (StringUtils.isEmpty(vo.getSex()))
//			return "insure-page/insure-page";
//		
//		inputMap.put("age",vo.getAge());
//		inputMap.put("birthConvertToAge",vo.getBirthConvertToAge());
//		inputMap.put("sex",vo.getSex());
//		inputMap.put("total_count","0");
//		inputMap.put("dambo1",vo.getDambo1());
//		
//		CommonCodeVO commonCodeVO = new CommonCodeVO();
//		
//		try{
//			commonCodeVO.setGrpCd("3100"); //실손보험 sorting기준 , 설정파일등에 추가 필요
//			commonCodeVO.setCd("3100001");
//			commonCodeVO.setDelYn("N");
//			commonCodeVO.setDetailYn("Y");
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("orderby", commonCodeVO.getRef1());
//			
//			commonCodeVO = new CommonCodeVO();
//			commonCodeVO.setGrpCd("3102"); //선택담보 명칭
//			commonCodeVO.setCd((String) inputMap.get("dambo1"));
//			commonCodeVO.setDelYn("N");
//			commonCodeVO.setDetailYn("N");
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("dambo1_name", commonCodeVO.getCdName());
//			
//		}catch(Exception e){
//			logger.error("[/mopic/api/ins/getShilsonList]" ,e);
//			inputMap.put("orderby", "");
//		}
//		
//		inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
//		
//		inputMap.put("join_type_cd", ((String)inputMap.get("dambo1")).substring(8));
//		inputMap.put("dambo1", ((String)inputMap.get("dambo1")).substring(0,8));
//				    
//		List rList = insService.selectShilsonList(inputMap);
//		List rStatList = insService.selectShilsonCountByFilter(inputMap);
//		
//		inputMap.put("dambo1",vo.getDambo1());
//				

//		Map<Object, Object> inputMap = new HashMap<Object, Object>();
//		inputMap.put("grpCdList","1234,3102");
//		model.put("optCodeMap"	, CommonUtil.getReactJsonString(commonCodeService.selectCmnCdGrpDetailList(inputMap).get("data")));	
		commonService.setReactSearchQuery(request, model);
		
		return "insure-page/insure-page";
	}
	
	/**
	 * 생명보험 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/life")
	public String life(HttpServletRequest request, InsureVO vo, Locale locale, ModelMap model)   throws Exception{
		
//		Map<String, String> sessionMap    = CommonUtil.getSession(request);
//		Map<Object, Object> inputMap = new HashMap<Object, Object>();
//		
//		if (StringUtils.isEmpty(vo.getSex()))
//			return "insure-page/insure-page";
//		
//		inputMap.put("age",vo.getAge());
//		inputMap.put("sex",vo.getSex());
//		inputMap.put("total_count","0");
//		
//		CommonCodeVO commonCodeVO = new CommonCodeVO();
//		
//		try{
//			commonCodeVO.setGrpCd("3200"); //실손보험 sorting기준 , 설정파일등에 추가 필요
//			commonCodeVO.setCd("3200001");
//			commonCodeVO.setDelYn("N");
//			commonCodeVO.setDetailYn("Y");
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("orderby", commonCodeVO.getRef1());
//		}catch(Exception e){
//			inputMap.put("orderby", "");
//		}
//		
//		inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
//		
//		List rList = insService.selectLifeList(inputMap);
//		List rStatList = insService.selectLifeCountByFilter(inputMap);
//				
//		CommonUtil.makeProductResponseMap("200", getJsonString(inputMap), getJsonString(rList), getJsonString(rStatList), model);
		commonService.setReactSearchQuery(request, model);
		
		return "insure-page/insure-page";
	}
	
	/**
	 * 연금/저축 보험 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/annuity")
	public String annunity(HttpServletRequest request, InsureVO vo, Locale locale, ModelMap model)   throws Exception{
		
//		Map<String, String> sessionMap    = CommonUtil.getSession(request);
//		Map<Object, Object> inputMap = new HashMap<Object, Object>();
//		
//		if (StringUtils.isEmpty(vo.getSex()))
//			return "insure-page/insure-page";
//		
//		inputMap.put("age",vo.getAge());
//		inputMap.put("sex",vo.getSex());
//		inputMap.put("total_count","0");
//		
//		CommonCodeVO commonCodeVO = new CommonCodeVO();
//		
//		try{
//			commonCodeVO.setGrpCd("3300"); //실손보험 sorting기준 , 설정파일등에 추가 필요
//			commonCodeVO.setCd("3300001");
//			commonCodeVO.setDelYn("N");
//			commonCodeVO.setDetailYn("Y");
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("orderby", commonCodeVO.getRef1());
//		}catch(Exception e){
//			inputMap.put("orderby", "");
//		}
//		
//		inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
//		
//		List rList = insService.selectAnnuityList(inputMap);
//		List rStatList = insService.selectAnnuityCountByFilter(inputMap);
//				
//		CommonUtil.makeProductResponseMap("200", getJsonString(inputMap), getJsonString(rList), getJsonString(rStatList), model);
		commonService.setReactSearchQuery(request, model);
		
		return "insure-page/insure-page";
	}
	
	/**
	 * 자동차보험 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/car")
	public String car(HttpServletRequest request, InsureVO vo, Locale locale, ModelMap model) throws Exception {
		
				
//		Map<String, String> sessionMap    = CommonUtil.getSession(request);
//		Map<Object, Object> inputMap = new HashMap<Object, Object>();
//		
//		if (StringUtils.isEmpty(vo.getSex()))
//			return "insure-page/insure-page";
//		
//		inputMap.put("age",vo.getAge());
//		inputMap.put("sex",vo.getSex());
//		inputMap.put("dc_grade",vo.getDc_grade());
//		inputMap.put("car_type",vo.getCar_type());
//		inputMap.put("age_contract",vo.getAge_contract());
//		inputMap.put("driver",vo.getDriver());
//		inputMap.put("dambo",vo.getDambo());
//		inputMap.put("total_count","0");
//		
//		CommonCodeVO commonCodeVO = new CommonCodeVO();
//		
//		try{
//			commonCodeVO.setGrpCd("3400"); //실손보험 sorting기준 , 설정파일등에 추가 필요
//			commonCodeVO.setCd("3400001");
//			commonCodeVO.setDelYn("N");
//			commonCodeVO.setDetailYn("Y");
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("orderby", commonCodeVO.getRef1());
//		}catch(Exception e){
//			inputMap.put("orderby", "");
//		}
//		
//		inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
//		
//		List rList = insService.selectCarList(inputMap);
//		List rStatList = insService.selectCarCountByFilter(inputMap);
//		
////		inputMap.put("age","");
//				
//		CommonUtil.makeProductResponseMap("200", getJsonString(inputMap), getJsonString(rList), getJsonString(rStatList), model);
		commonService.setReactSearchQuery(request, model);
		
		return "insure-page/insure-page";
	}
	
	/**
	 * 실손보험 리스트 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/selectShilsonList")
	public View selectShilsonList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		int dambo1				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("dambo1")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		Map<Object, Object> inputMapClone = new HashMap<Object, Object>();
		inputMapClone.putAll(inputMap);
		
		try {
			
			try{
				commonCodeVO.setGrpCd("3100"); //실손보험 sorting기준 , 설정파일등에 추가 필요
				commonCodeVO.setCd(sorting==0?"3100001":Integer.toString(sorting));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("Y");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("orderby", commonCodeVO.getRef1());
				
				commonCodeVO = new CommonCodeVO();
				commonCodeVO.setGrpCd("3102"); //선택담보 명칭
				commonCodeVO.setCd((String) inputMap.get("dambo1"));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("N");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("dambo1_name", commonCodeVO.getCdName());
				
			}catch(Exception e){
				logger.error("[/mopic/api/ins/getShilsonList]" ,e);
				inputMap.put("orderby", "");
			}
			
			//System.out.println("commonCodeVO.getRef1():::"+commonCodeVO.getRef1());
			
//			commonCodeVO.setGrpCd(1102); //선택담보(종합/상해/질병)
//			commonCodeVO.setCd(dambo1);
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("dambo1_text", commonCodeVO.getCdName());
			
//			map.put("resultCode", "200");
//			map.put("productData", insService.selectShilsonList(inputMap));
			
			//dambo1로 넘오는 조건값을 (종합/상해/질병)과 (표준/선택)으로 분리
			inputMap.put("join_type_cd", ((String)inputMap.get("dambo1")).substring(8));
			inputMap.put("dambo1", ((String)inputMap.get("dambo1")).substring(0,8));
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크) 
			
			List rList = insService.selectShilsonList(inputMap);
			
			CommonUtil.makeProductResponseMap("200", inputMapClone, rList, insService.selectShilsonCountByFilter(inputMap), modelMap);
			
			CommonUtil.setBestYN(inputMapClone, modelMap, (String)inputMap.get("amount")+"", ((HashMap)rList.get(0)).get("insurance_fee")+"", "Y");
			
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/ins/getShilsonList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, new ArrayList(), null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}
	
	/**
	 * 실손보험 상세보기 팝업
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/shilson/detail")
	public String shilsonDetail(HttpServletRequest request, InsureVO vo, Locale locale, Model model) {
		
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		try {
			
			vo.setUserId(sessionMap.get("userId"));
			
			List<HashMap> list = insService.selectShilsonDetail(vo);
			
			model.addAttribute("data",list);
			model.addAttribute("insurance_sum",list.get(0).get("insurance_sum"));
			model.addAttribute("prod_name",list.get(0).get("prod_name"));
			model.addAttribute("prod_code",list.get(0).get("prod_code"));
			model.addAttribute("interest_yn",list.get(0).get("interest_yn"));
			
			model.addAttribute("homepage",list.get(0).get("homepage"));
			model.addAttribute("bi",vo.getBi());
			model.addAttribute("bigo",list.get(0).get("bigo"));
		
		} catch(Exception e) {
			
			logger.error("[/ins/shilson/detail]" ,e);
		}
		
		return "insure-page/shilson-detail-pop";
	}
	
	/**
	 * 실손보험 상세 조회
	 * @param locale
	 * @param model
	 * @return
	 */
//	@RequestMapping(value = "/mopic/api/ins/selectShilsonDetail")
//	public View selectShilsonDetail(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
//		
//		try {
//			
//			CommonUtil.makeProductResponseMap("200", inputMap, insService.selectShilsonDetail(inputMap), null, modelMap);
//			
//		} catch(Exception e) {
//			
//			logger.error("[/mopic/api/ins/selectShilsonDetail]" ,e);
//			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
//		}
//		
//		return jsonView;
//	}
	
	/**
	 * 정기보험 리스트 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/selectLifeList")
	public View selectLifeList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		int dambo1				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("dambo1")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		try {
			
			try{
				commonCodeVO.setGrpCd("3200"); //실손보험 sorting기준 , 설정파일등에 추가 필요
				commonCodeVO.setCd(sorting==0?"3200001":Integer.toString(sorting));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("Y");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("orderby", commonCodeVO.getRef1());
			}catch(Exception e){
				inputMap.put("orderby", "");
			}
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
			List rList = insService.selectLifeList(inputMap);
			CommonUtil.makeProductResponseMap("200", inputMap, rList, insService.selectLifeCountByFilter(inputMap), modelMap);
			
			CommonUtil.setBestYN(inputMap, modelMap, (String)inputMap.get("price_index")+"", ((HashMap)rList.get(0)).get("price_index")+"", "Y");
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/ins/getShilsonList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}
	
	/**
	 * 정기보험 상세보기 팝업
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/life/detail")
	public String lifeDetail(HttpServletRequest request, InsureVO vo, Locale locale, Model model) {
		
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		try {
			vo.setUserId(sessionMap.get("userId"));
			model.addAttribute("data",insService.selectLifeDetail(vo));
			
			model.addAttribute("bi",vo.getBi());
		
		} catch(Exception e) {
			
			logger.error("[/ins/life/detail]" ,e);
		}
		
		return "insure-page/life-detail-pop";
	}
	
	/**
	 * 연금/저축 보험 리스트 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/selectAnnuityList")
	public View selectAnnuityList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		int dambo1				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("dambo1")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		try {
			
			try{
				commonCodeVO.setGrpCd("3300"); //실손보험 sorting기준 , 설정파일등에 추가 필요sa
				commonCodeVO.setCd(Integer.toString(sorting));
				commonCodeVO.setCd(sorting==0?"3300001":Integer.toString(sorting));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("Y");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("orderby", commonCodeVO.getRef1());
			}catch(Exception e){
				inputMap.put("orderby", "");
			}
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
			List rList = insService.selectAnnuityList(inputMap);
			
			CommonUtil.makeProductResponseMap("200", inputMap, rList, insService.selectAnnuityCountByFilter(inputMap), modelMap);
			
			CommonUtil.setBestYN(inputMap, modelMap, (String)inputMap.get("saved_rate_10")+"", ((HashMap)rList.get(0)).get("saved_rate_10")+"", "N");
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/ins/selectAnnuityList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}
	
	/**
	 * 연금저축보험 상세보기 팝업
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/annuity/detail")
	public String annuityDetail(HttpServletRequest request, InsureVO vo, Locale locale, Model model) {
		
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		try {
			vo.setUserId(sessionMap.get("userId"));
			model.addAttribute("data",insService.selectAnnuityDetail(vo));
			
			model.addAttribute("bi",vo.getBi());
		
		} catch(Exception e) {
			
			logger.error("[/ins/annuity/detail]" ,e);
		}
		
		return "insure-page/annuity-detail-pop";
	}
	
	/**
	 * 연금저축보험 상세보기 팝업
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ins/annuity/refund")
	public String annuityRefund(InsureVO vo, Locale locale, Model model) {
		
		try {
		
			model.addAttribute("data",insService.selectAnnuityRefund(vo));
		
		} catch(Exception e) {
			
			logger.error("[/ins/annuity/refund]" ,e);
		}
		
		return "insure-page/annuity-refund-pop";
	}
	
	/**
	 * 연금/저축 보험 상세 조회
	 * @param locale
	 * @param model
	 * @return
	 */
//	@RequestMapping(value = "/mopic/api/ins/selectAnnuityDetail")
//	public View selectAnnuityDetail(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
//		
//		try {
//			
//			CommonUtil.makeProductResponseMap("200", inputMap, insService.selectAnnuityDetail(inputMap), null, modelMap);
//			
//		} catch(Exception e) {
//			
//			logger.error("[/mopic/api/ins/selectLifeDetail]" ,e);
//			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
//		}
//		
//		return jsonView;
//	}
	
	/**
	 * 연금/저축 보험 해지환급금  조회
	 * @param locale
	 * @param model
	 * @return
	 */
//	@RequestMapping(value = "/mopic/api/ins/selectAnnuityRefund")
//	public View selectAnnuityRefund(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
//		
//		try {
//			
//			CommonUtil.makeProductResponseMap("200", inputMap, insService.selectAnnuityRefund(inputMap), null, modelMap);
//			
//		} catch(Exception e) {
//			
//			logger.error("[/mopic/api/ins/selectAnnuityRefund]" ,e);
//			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
//		}
//		
//		return jsonView;
//	}
	
	/**
	 * 자동차보험 리스트 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/selectCarList")
	public View selectCarList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		try {
			
			try{
				commonCodeVO.setGrpCd("3400"); //자동차보험 sorting기준 , 설정파일등에 추가 필요
				commonCodeVO.setCd(sorting==0?"3400001":Integer.toString(sorting));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("Y");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("orderby", commonCodeVO.getRef1());
			}catch(Exception e){
				inputMap.put("orderby", "");
			}

			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
			List rList = insService.selectCarList(inputMap);
			
			CommonUtil.makeProductResponseMap("200", inputMap, rList, insService.selectCarCountByFilter(inputMap), modelMap);
			
			CommonUtil.setBestYN(inputMap, modelMap, (String)inputMap.get("amount")+"", ((HashMap)rList.get(0)).get("insurance_fee")+"", "Y");
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/ins/getShilsonList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}
	
	
	
	/**
	 * 가입상품 등록하기 팝업에서 사용하는 회사 목록 조회용 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/selectAllCompanyListByInsType")
	public View selectJoinCompanyList(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		
		try {
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data", insService.selectAllCompanyListByInsType(inputMap));
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/selectJoinCompanyList]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 실손보험 가입상품등록 팝업에서 상품명으로 조회하는 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/filterProductByName")
	public View filterShilsonByName(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		List rlist = null;
		
		try {
			
			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception("403");
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			
			String menu_cd = (String)inputMap.get("menu_cd");
			
			if ( "3100".equals(menu_cd) ) {
				
				inputMap.put("join_type_cd", ((String)inputMap.get("dambo1")).substring(8));
				inputMap.put("dambo1", ((String)inputMap.get("dambo1")).substring(0,8));
				
				rlist = insService.filterShilsonByName(inputMap);
			}else if ( "3200".equals(menu_cd) ) {
				rlist = insService.filterLifeByName(inputMap);
			}else if ( "3300".equals(menu_cd) ) {
				rlist = insService.filterAnnuityByName(inputMap);
			}else if ( "3400".equals(menu_cd) ) {
				rlist = insService.filterCarByName(inputMap);
			}
			
			map.put("data", rlist);
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/filterProductByName]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 내가 가입한 실손보험  조회 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/ins/selectMyProductList")
	public View selectMyShilsonList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		List rlist = null;
		
		try {
			
			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception("403");
			
			inputMap.put("user_id"	, sessionMap.get("userId"));//login id 셋팅
			inputMap.put("gubun"	,StaticObject.Code.CODE_MY_JOIN_PRODUCT);
			
			String menu_cd = (String)inputMap.get("menu_cd");
			
			if(("1100").equals(menu_cd) || ("1200").equals(menu_cd) || ("1300").equals(menu_cd) || ("1400").equals(menu_cd)) {
				/*
				System.out.println("=================================ds_code : " + (String)inputMap.get("ds_code"));
				System.out.println("=================================DS_CODE : " + (String)inputMap.get("DS_CODE"));
				System.out.println("=================================term_code : " + (String)inputMap.get("term_code"));
				System.out.println("=================================inputMap : " + inputMap);
				String term_code =  StringUtils.defaultIfEmpty((String) inputMap.get("term_code"), "");
				int term = savingService.getSavingMonth(term_code);
				inputMap.put("period"	, term);
				*/
				inputMap.put("menu_cd"	, menu_cd);
				rlist = savingService.selectMySavingList(inputMap);
			} else if(("2100").equals(menu_cd) || ("2200").equals(menu_cd) || ("2300").equals(menu_cd)) {
				inputMap.put("menu_cd", menu_cd);
				rlist = loanService.selectMyLoanList(inputMap);
			} else if ( "3100".equals(menu_cd) )
				rlist = insService.selectMyShilsonList(inputMap);
			else if ( "3200".equals(menu_cd) )
				rlist = insService.selectMyLifeList(inputMap);
			else if ( "3300".equals(menu_cd) )
				rlist = insService.selectMyAnnuityList(inputMap);
			else if ( "3400".equals(menu_cd) )
				rlist = insService.selectMyCarList(inputMap);
			else if ( "4100".equals(menu_cd) || "4200".equals(menu_cd))
				rlist = cardService.selectMyProductList(inputMap);

			
			map.put("data", rlist);
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/selectMyShilsonList]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	
	/**
	 * 가입상품 등록용 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/common/insertMyProduct")
	public View insertMyProduct(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		//int gubun				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("gubun")	, "30000001"));
		
		Map<String, Object> map			= new HashMap<String, Object>();
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		List<?> rlist					= null;
		String product_type				= StringUtils.defaultIfEmpty((String) inputMap.get("product_type")	, "");
		String term						= StringUtils.defaultIfEmpty((String) inputMap.get("term")	, "");
		
		//System.out.println("request input map ::: " + inputMap.toString());
		
		try {

			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception();
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			inputMap.put("gubun" ,StaticObject.Code.CODE_MY_JOIN_PRODUCT);
			
			if(!StringUtils.isEmpty(product_type))
				inputMap.put("menu_cd", product_type.substring(0, 4));
			if(!StringUtils.isEmpty(term))
				inputMap.put("term", 12);
			
			insService.insertMyProduct(inputMap);
			
			if(product_type.startsWith("1100") || product_type.startsWith("1200") || product_type.startsWith("1300") || product_type.startsWith("1400"))
				rlist = savingService.selectMySavingList(inputMap);
			else if(product_type.startsWith("2100") || product_type.startsWith("2200") || product_type.startsWith("2300"))
				rlist = loanService.selectMyLoanList(inputMap);
			else if ( product_type.startsWith("3100") )
				rlist = insService.selectMyShilsonList(inputMap);
			else if ( product_type.startsWith("3200") )
				rlist = insService.selectMyLifeList(inputMap);
			else if ( product_type.startsWith("3300") )
				rlist = insService.selectMyAnnuityList(inputMap);
			else if ( product_type.startsWith("3400") )	
				rlist = insService.selectMyCarList(inputMap);
			else if ( product_type.startsWith("4100") || product_type.startsWith("4200") )
				rlist = cardService.selectMyProductList(inputMap);
			
			map.put("resultCode", StaticObject.Code.CODE_SUCCESS);
			map.put("data"		, rlist.get(0));
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/insertMyProduct]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 관심상품 등록용 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/common/insertMyInterest")
	public View insertMyInterest(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		List rlist = null;
		try {
			
			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception("403");
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			
			inputMap.put("gubun",StaticObject.Code.CODE_MY_INTEREST_PRODUCT);
			
			insService.insertMyInterest(inputMap);
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data", inputMap);
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/insertMyProduct]" ,e);
			map.put("resultCode"	, e.getMessage());
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 가입상품 삭제 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/common/deleteMyProduct")
	public View deleteMyProduct(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		try {
			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception("403");
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data", insService.deleteMyProduct(inputMap));
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/deleteMyProduct]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 관심상품 삭제 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/common/deleteMyInterest")
	public View deleteMyInterest(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		try {
			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception("403");
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data", insService.deleteMyInterest(inputMap));
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/ins/deleteMyInterest]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	
}