package com.nomad.mopic.ins.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.InsureVO;

@Repository(value = "insMapper")
public interface InsMapper {
	
	public List selectShilsonList(Map searchQuery);
	public List selectShilsonCountByFilter(Map searchQuery);
	public List selectShilsonDetail(InsureVO vo);
	
	public List selectLifeList(Map searchQuery);
	public List selectLifeCountByFilter(Map searchQuery);
	public Map selectLifeDetail(InsureVO vo);
	
	public List selectAnnuityList(Map searchQuery);
	public List selectAnnuityCountByFilter(Map searchQuery);
	public Map selectAnnuityDetail(InsureVO vo);
	public List selectAnnuityRefund(InsureVO vo);
	
	public List selectCarList(Map searchQuery);
	public List selectCarCountByFilter(Map searchQuery);
	
	public List selectAllCompanyListByInsType(Map searchQuery);
	
	//내 가입 상품 등록
	public Integer insertMyProduct(Map map);
	//내 관심상품 등록
	public Integer insertMyInterest(Map map);
	
	
	//내 가입 상품 삭제
	public Integer deleteMyProduct(Map map);	
	//내 관심 상품 삭제
	public Integer deleteMyInterest(Map map);
	
	//실손 상품명으로 상품목록 조회
	public List filterShilsonByName(Map searchQuery);
	//정기 상품명으로 상품목록 조회
	public List filterLifeByName(Map searchQuery);
	//연금저축 상품명으로 상품목록 조회
	public List filterAnnuityByName(Map searchQuery);
	//자동차 상품명으로 상품목록 조회
	public List filterCarByName(Map searchQuery);	
	
	//내 실손보험 목록 조회
	public List selectMyShilsonList(Map searchQuery);
	//내 정기보험 목록 조회
	public List selectMyLifeList(Map searchQuery);
	//내 연금저축보험 목록 조회
	public List selectMyAnnuityList(Map searchQuery);
	//내 자동차보험 목록 조회
	public List selectMyCarList(Map searchQuery);
	
	

}