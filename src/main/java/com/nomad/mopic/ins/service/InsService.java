package com.nomad.mopic.ins.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nomad.mopic.ins.mapper.InsMapper;
import com.nomad.mopic.vo.InsureVO;

@Service
public class InsService implements InsMapper {
	@Resource(name = "insMapper")
    private InsMapper insMapper;
	
	@Override
	public List selectShilsonList(Map searchQuery) {
		return insMapper.selectShilsonList(searchQuery);
	}
	
	@Override
	public List selectShilsonCountByFilter(Map searchQuery) {
		return insMapper.selectShilsonCountByFilter(searchQuery);
	}
	
	@Override
	public List selectShilsonDetail(InsureVO vo) {
		return insMapper.selectShilsonDetail(vo);
	}
	
	@Override
	public List selectLifeList(Map searchQuery) {
		return insMapper.selectLifeList(searchQuery);
	}
	
	@Override
	public List selectAnnuityCountByFilter(Map searchQuery) {
		return insMapper.selectAnnuityCountByFilter(searchQuery);
	}
	
	@Override
	public Map selectLifeDetail(InsureVO vo) {
		return insMapper.selectLifeDetail(vo);
	}
	
	@Override
	public List selectAnnuityList(Map searchQuery) {
		return insMapper.selectAnnuityList(searchQuery);
	}
	
	@Override
	public List selectLifeCountByFilter(Map searchQuery) {
		return insMapper.selectLifeCountByFilter(searchQuery);
	}
	
	@Override
	public Map selectAnnuityDetail(InsureVO vo) {
		return insMapper.selectAnnuityDetail(vo);
	}
	
	@Override
	public List selectAnnuityRefund(InsureVO vo) {
		// TODO Auto-generated method stub
		return insMapper.selectAnnuityRefund(vo);
	}
	
	@Override
	public List selectCarList(Map searchQuery) {
		return insMapper.selectCarList(searchQuery);
	}
	
	@Override
	public List selectCarCountByFilter(Map searchQuery) {
		return insMapper.selectCarCountByFilter(searchQuery);
	}
	
	@Override
	public List selectAllCompanyListByInsType(Map searchQuery) {
		return insMapper.selectAllCompanyListByInsType(searchQuery);
	}
	
	@Override
	public Integer insertMyProduct(Map map) {
		return insMapper.insertMyProduct(map);
	}
	
	@Override
	public Integer insertMyInterest(Map map) {
		return insMapper.insertMyInterest(map);
	}
	
	@Override
	public Integer deleteMyProduct(Map map) {
		return insMapper.deleteMyProduct(map);
	}
	
	@Override
	public Integer deleteMyInterest(Map map) {
		return insMapper.deleteMyInterest(map);
	}
		
	@Override
	public List filterShilsonByName(Map searchQuery) {
		return insMapper.filterShilsonByName(searchQuery);
	}
	
	@Override
	public List filterLifeByName(Map searchQuery) {
		return insMapper.filterLifeByName(searchQuery);
	}
	
	@Override
	public List filterAnnuityByName(Map searchQuery) {
		return insMapper.filterAnnuityByName(searchQuery);
	}
	
	@Override
	public List filterCarByName(Map searchQuery) {
		return insMapper.filterCarByName(searchQuery);
	}
	
	@Override
	public List selectMyShilsonList(Map searchQuery) {
		return insMapper.selectMyShilsonList(searchQuery);
	}
	
	@Override
	public List selectMyLifeList(Map searchQuery) {
		return insMapper.selectMyLifeList(searchQuery);
	}
	
	@Override
	public List selectMyAnnuityList(Map searchQuery) {
		return insMapper.selectMyAnnuityList(searchQuery);
	}
	
	@Override
	public List selectMyCarList(Map searchQuery) {
		return insMapper.selectMyCarList(searchQuery);
	}
}