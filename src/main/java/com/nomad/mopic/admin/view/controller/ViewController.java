package com.nomad.mopic.admin.view.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.board.service.BoardService;
import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.vo.CommonCodeVO;

@Controller
public class ViewController {
	private static final Logger logger = LoggerFactory.getLogger(ViewController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	CryptoUtil cryptoUtil;
	
	@Autowired
	BoardService boardService;
	
	@Autowired
	CommonCodeService commonCodeService;

	/**
	 * 메인
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/index", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response, Model model) {
		//model.addAttribute("serverTime", testDate);
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		
		model.addAttribute("requestUri", request.getRequestURI());
		
		
		
		return "admin/index";
	}

	/**
	 * 로그인
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
		String redirectUrl = StringUtils.defaultIfEmpty(request.getParameter("redirectUrl"), "");
		
		model.addAttribute("requestUri", request.getRequestURI());
		model.addAttribute("redirectUrl", redirectUrl);
		//System.out.println(cryptoUtil.enc("aaaa1111"));
		return "admin/login/login";
	}

	/**
	 * 로그아웃
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		CommonUtil.deleteSession(request, response);
		
		return "redirect:/admin/login";
	}

	/**
	 * 퍼블진행상황(개발 완료 후 삭제)
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/pub", method = RequestMethod.GET)
	public String index2(HttpServletRequest request, Model model) {
		//model.addAttribute("serverTime", testDate);

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/admin_page_list";
	}
	
	/**
	 * 공지사항 목록
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/noticeList", method = RequestMethod.GET)
	public String noticeList(HttpServletRequest request, Model model) {
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000001";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);

		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/notice/noticeList";
	}
	
	/**
	 * 공지사항 글쓰기
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/noticeWrite", method = RequestMethod.GET)
	public String noticeWrite(HttpServletRequest request, Model model) {
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000001";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/notice/noticeWrite";
	}
	
	/**
	 * 공지사항 글수정
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/noticeModify", method = RequestMethod.GET)
	public String noticeModify(HttpServletRequest request, Model model) {
		int bltnCd	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("bltnCd"), "0"));
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000001";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnCd"		, bltnCd);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/notice/noticeWrite";
	}
	
	/**
	 * faq 목록
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/faqList", method = RequestMethod.GET)
	public String faqList(HttpServletRequest request, Model model) {
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000002";		// faq
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);

		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/notice/noticeList";
	}
	
	/**
	 * faq 글쓰기
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/faqWrite", method = RequestMethod.GET)
	public String faqWrite(HttpServletRequest request, Model model) {
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		CommonCodeVO faqCategoryVO	= new CommonCodeVO();
		List<CommonCodeVO> cList	= null;
		
		String bltnType = "1000002";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		// faq 카테고리
		faqCategoryVO.setGrpCd("1024");
		faqCategoryVO.setPageNumber(1);
		faqCategoryVO.setRowCount(9999999);
		faqCategoryVO.setDelYn("N");
		faqCategoryVO.setDetailYn("N");
		
		cList = commonCodeService.selectCmnCdList(faqCategoryVO);
		
		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		model.addAttribute("category"	, cList);
		
		return "admin/notice/noticeWrite";
	}
	
	/**
	 * faq 글수정
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/faqModify", method = RequestMethod.GET)
	public String faqModify(HttpServletRequest request, Model model) {
		int bltnCd	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("bltnCd"), "0"));
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO faqCategoryVO	= new CommonCodeVO();
		List<CommonCodeVO> cList	= null;
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000002";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		// faq 카테고리
		faqCategoryVO.setGrpCd("1024");
		faqCategoryVO.setPageNumber(1);
		faqCategoryVO.setRowCount(9999999);
		faqCategoryVO.setDelYn("N");
		faqCategoryVO.setDetailYn("N");
		
		cList = commonCodeService.selectCmnCdList(faqCategoryVO);
		
		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnCd"		, bltnCd);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		model.addAttribute("category"	, cList);
		
		return "admin/notice/noticeWrite";
	}
	
	/**
	 * 공통코드 리스트
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/commonCodeList", method = RequestMethod.GET)
	public String commonCodeList(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/code/commonCodeList";
	}
	
	/**
	 * 상품목록조회
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/productList", method = RequestMethod.GET)
	public String productList(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/product/productList";
	}
	
	/**
	 * 상품등록
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/productAdd", method = RequestMethod.GET)
	public String productAdd(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/product/productAdd";
	}
	
	/**
	 * 이벤트목록조회
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/eventList", method = RequestMethod.GET)
	public String eventList(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/event/eventList";
	}
	
	/**
	 * 이벤트등록
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/eventAdd", method = RequestMethod.GET)
	public String eventAdd(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/event/eventAdd";
	}
	
	/**
	 * 메뉴관리
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/menuManage", method = RequestMethod.GET)
	public String menuManage(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/menu/menuManage";
	}
	
	/**
	 * 배경이미지관리
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/background", method = RequestMethod.GET)
	public String background(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/background/background";
	}
	
	/**
	 * 1:1문의내역
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/inquireList", method = RequestMethod.GET)
	public String inquireList(HttpServletRequest request, Model model) {
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000003";		//  1:1문의
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);

		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/inquire/inquireList";
	}
	
	/**
	 * 1:1문의 글쓰기
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/inquireWrite", method = RequestMethod.GET)
	public String inquireWrite(HttpServletRequest request, Model model) {
		int upperBltnCd	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("upperBltnCd"), "0"));
		int pageNum		= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000003";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("upperBltnCd", upperBltnCd);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/inquire/inquireWrite";
	}
	
	/**
	 * 1:1문의 글수정
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/inquireModify", method = RequestMethod.GET)
	public String inquireModify(HttpServletRequest request, Model model) {
		int upperBltnCd	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("upperBltnCd"), "0"));
		int pageNum		= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		String bltnType = "1000003";		// 공지사항
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("upperBltnCd", upperBltnCd);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "admin/inquire/inquireWrite";
	}
	
	/**
	 * 통계
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/statisticBoard", method = RequestMethod.GET)
	public String statisticBoard(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/statistic/statisticBoard";
	}
	
	/**
	 * 방문현황
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/visitorBoard", method = RequestMethod.GET)
	public String visitorBoard(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/statistic/visitorBoard";
	}
	
	/**
	 * 방문현황
	 * 
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/admin/memberBoard", method = RequestMethod.GET)
	public String memberBoard(HttpServletRequest request, Model model) {

		model.addAttribute("requestUri", request.getRequestURI());
		
		return "admin/statistic/memberBoard";
	}
	
	
}