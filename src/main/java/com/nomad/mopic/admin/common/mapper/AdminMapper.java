package com.nomad.mopic.admin.common.mapper;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.MemberVO;

@Repository(value = "adminMapper")
public interface AdminMapper {
	MemberVO getAdminUserInfo(MemberVO paramVO);

}
