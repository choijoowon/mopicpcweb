package com.nomad.mopic.admin.common.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nomad.mopic.admin.common.mapper.AdminMapper;
import com.nomad.mopic.vo.MemberVO;

@Service
public class AdminService {
	@Resource(name = "adminMapper")
    private AdminMapper adminMapper;

	public MemberVO getAdminUserInfo(MemberVO paramVO) {
		return adminMapper.getAdminUserInfo(paramVO);
		
		
	}

}
