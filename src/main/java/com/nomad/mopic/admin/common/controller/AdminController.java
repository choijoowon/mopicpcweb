package com.nomad.mopic.admin.common.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.admin.common.service.AdminService;
import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.vo.MemberVO;

@Controller
public class AdminController {
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	CryptoUtil cryptoUtil;

	@Autowired
	public View jsonView;
	
	@Autowired
	public AdminService adminService;

	/**
	 * 로그인
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/doAdminLogin", method = RequestMethod.POST)
	public String index(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		//ModelMap modelMap	= new ModelMap();
		Map<String, Object> map	= new HashMap<String, Object>();
		MemberVO paramVO	= new MemberVO();
		MemberVO memberVO	= new MemberVO();
		String userId		= StringUtils.defaultIfEmpty(request.getParameter("userId")					, "");
		String userPwd		= cryptoUtil.enc(StringUtils.defaultIfEmpty(request.getParameter("userPwd")	, ""));
		String redirectUrl	= StringUtils.defaultIfEmpty(request.getParameter("redirectUrl")			, "");
		
		try {
			paramVO.setUserId(userId);
			paramVO.setUserPwd(userPwd);
			
			memberVO	= adminService.getAdminUserInfo(paramVO);
			
			if(memberVO == null)
				throw new Exception("1");

			modelMap.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			modelMap.addAttribute("userId"		, userId);
			modelMap.addAttribute("redirectUrl"	, redirectUrl);
		} catch(Exception e) {
			modelMap.addAttribute("resultCode"	, StaticObject.Code.CODE_FAILED);
			modelMap.addAttribute("resultMessage"	, e.getLocalizedMessage());
		}

		return "null";
	}

}
