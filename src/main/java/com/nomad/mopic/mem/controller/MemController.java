package com.nomad.mopic.mem.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.mem.service.MemService;
import com.nomad.mopic.vo.MemberVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MemController {
	private static final Logger logger = LoggerFactory.getLogger(MemController.class);
	
	@Autowired
	public View jsonView;
	
	@Autowired
	public MemService memService;
	
	/**
	 * 로그인
	 * 
	 * @param request
	 * @param response
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/member/doLogin", method = RequestMethod.POST)
	public View login(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		HashMap<String, String> userInfoMap = null;
		MemberVO memberVO					= new MemberVO();
		JSONObject jsonParam				= null;
		String redirectUrl					= StringUtils.defaultIfEmpty((String) inputMap.get("redirectUrl"), "");
		String access_token					= StringUtils.defaultIfEmpty((String) inputMap.get("access_token"), "");
		String url							= StaticObject.Url.URL_YELLO_PASS_DEV_PROFILE;
		String param						= "";
		String result						= "";

		try {
			jsonParam = new JSONObject();
			jsonParam.put("access_token", access_token);
			
			//System.out.println("access_token::::"+access_token);
			
			String output = jsonParam.toString();
			
			//System.out.println("output::::"+output);
			
			result = CommonUtil.urlConnect(url, param, "UTF-8", output);
			
			//System.out.println("result::::"+result);
			
			ObjectMapper om	= new ObjectMapper();
			JsonNode node	= om.readTree(result);
			
			userInfoMap = om.readValue(result, new TypeReference<HashMap<String, Object>>(){});
			userInfoMap.put("resultCode", "200");
			
			// login success
			if ("100".equals(userInfoMap.get("code"))) {
				memberVO = memService.selectUserInfo(userInfoMap);
				
				if (memberVO == null) {
					modelMap.addAttribute("is_first","1");
					memService.insertUserInfo(userInfoMap);
					
					request.getSession().setAttribute("is_first" , "Y");
				} else {
					modelMap.addAttribute("is_first","0");
					memService.updateLoginInfo(userInfoMap);
				}
				//for test2 lines
				//modelMap.addAttribute("is_first","1");
				//request.getSession().setAttribute("is_first" , "Y");
				//
				
				modelMap.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
				modelMap.addAttribute("userId"		, (String) userInfoMap.get("user_id"));
				modelMap.addAttribute("access_token" , access_token);
				modelMap.addAttribute("redirectUrl"	, redirectUrl);
			} else {
				modelMap.put("resultCode"	, StaticObject.Code.CODE_FAILED);
				modelMap.put("resultMessage", userInfoMap.get("desc"));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		return jsonView;
	}

	/**
	 * 로그아웃
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/member/logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		String redirectUrl = StringUtils.defaultIfEmpty(request.getParameter("redirectUrl"), "/");
		
		CommonUtil.deleteSession(request, response);
		
		return "redirect:" + redirectUrl;
	}
	
	/**
	 * 로그인페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/member/loginPage", method = RequestMethod.GET)
	public String loginPage(HttpServletRequest request, Locale locale, Model model) {
		String redirectUrl = StringUtils.defaultIfEmpty(request.getParameter("redirectUrl"), "");
		
		model.addAttribute("redirectUrl", redirectUrl);
		
		return "mem/loginPage";
	}
	
	
	/*****************************************************************************************************************************************/
	/** 이하 API 영역 
	/*****************************************************************************************************************************************/
	
	/**
	 * 사용자 정보조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectUserInfo", method = RequestMethod.POST)
	public View selectUserInfo(HttpServletRequest request, Locale locale, Model model) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		Map<String, String> map			= new HashMap<String, String>();
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		MemberVO memberVO				= new MemberVO();
		String userId					= StringUtils.defaultIfEmpty(sessionMap.get("userId"), "");
		
		try {
			if(StringUtils.isEmpty(userId)) 
				throw new Exception();
			
			map.put("user_id", sessionMap.get("userId"));
			
			memberVO = memService.selectUserInfo(map);
			
			returnMap.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			returnMap.put("data"		, memberVO);
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	/**
	 * 사용자 정보수정
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/updateUserInfo", method = RequestMethod.POST)
	public View updateUserInfo(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap model) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		Map<String, String> map			= new HashMap<String, String>();
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		MemberVO memberVO				= new MemberVO();
		
		int income						= 0;
		String userId					= StringUtils.defaultIfEmpty(sessionMap.get("userId")							, "");
		String email					= StringUtils.defaultIfEmpty((String) inputMap.get("email")						, "");
		String incomeStr				= StringUtils.defaultIfEmpty((String) inputMap.get("income")					, "");
		String married					= StringUtils.defaultIfEmpty((String) inputMap.get("married")					, "");
		String hasChild					= StringUtils.defaultIfEmpty((String) inputMap.get("hasChild")					, "");
		String occupation				= StringUtils.defaultIfEmpty((String) inputMap.get("occupation")				, "");
		String dob						= StringUtils.defaultIfEmpty((String) inputMap.get("dob")						, "");
		String sex						= StringUtils.defaultIfEmpty((String) inputMap.get("sex")						, "");
		String productInterest			= StringUtils.defaultIfEmpty((String) inputMap.get("productInterest")			, "");
		String homeCity					= StringUtils.defaultIfEmpty((String) inputMap.get("homeCity")					, "");
		String homeTown					= StringUtils.defaultIfEmpty((String) inputMap.get("homeTown")					, "");
		int bankCode					= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bankCode")	, "0"));
		
		if(incomeStr.indexOf(",") > -1) {
			incomeStr = incomeStr.replaceAll(",", "");
		}
		if ( !"".equals(incomeStr) )
			income = Integer.parseInt(incomeStr);
		
		try {
			if(StringUtils.isEmpty(userId)) 
				throw new Exception();
			
			memberVO.setUserId(userId);
			if(!StringUtils.isEmpty(email))
				memberVO.setEmail(email);
			if(income != 0)
				memberVO.setIncome(income);
			if(!StringUtils.isEmpty(married))
				memberVO.setMarried(married);
			if(!StringUtils.isEmpty(hasChild))
				memberVO.setHasChild(hasChild);
			if(!StringUtils.isEmpty(occupation))
				memberVO.setOccupation(occupation);
			if(!StringUtils.isEmpty(dob))
				memberVO.setDob(dob);
			if(!StringUtils.isEmpty(sex))
				memberVO.setSex(sex);
			if(!StringUtils.isEmpty(productInterest))
				memberVO.setProductInterest(productInterest);
			if(!StringUtils.isEmpty(homeCity))
				memberVO.setHomeCity(homeCity);
			if(!StringUtils.isEmpty(homeTown))
				memberVO.setHomeTown(homeTown);
			if(bankCode != 0)
				memberVO.setBankCode(bankCode);
			
			memService.updateUserInfo(memberVO);
			
			returnMap.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			returnMap.put("data"		, memberVO);
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	/**
	 * 사용자 로그인정보업데이트
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/updateLoginInfo", method = RequestMethod.POST)
	public View updateLoginInfo(HttpServletRequest request, ModelMap model) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		Map<String, String> map			= new HashMap<String, String>();
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		String userId					= StringUtils.defaultIfEmpty(sessionMap.get("userId"), "");
		
		try {
			if(StringUtils.isEmpty(userId)) 
				throw new Exception();
			
			map.put("user_id", userId);
			
			memService.updateLoginInfo(map);
			
			returnMap.put("resultCode"		, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	/**
	 * 사용자 로그인정보업데이트
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/updateLogoutInfo", method = RequestMethod.POST)
	public View updateLogoutInfo(HttpServletRequest request, ModelMap model) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		Map<String, String> map			= new HashMap<String, String>();
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		String userId					= StringUtils.defaultIfEmpty(sessionMap.get("userId"), "");
		
		try {
			if(StringUtils.isEmpty(userId)) 
				throw new Exception();
			
			map.put("user_id", userId);
			
			memService.updateLogoutInfo(map);
			
			returnMap.put("resultCode"		, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
	/**
	 * 사용자 추가정보 - 시군구정보 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/mem/selectSigunguInfo", method = RequestMethod.POST)
	public View selectSigunguInfo(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, Locale locale, Model model) {
		
		Map<String, Object> returnMap	= new HashMap<String, Object>();
		
		try {
			
			returnMap.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			returnMap.put("data"		, memService.selectSigunguAddress(inputMap));
		} catch(Exception e) {
			returnMap.put("resultCode"		, StaticObject.Code.CODE_FAILED);
			returnMap.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		model.addAttribute("result", returnMap);
		
		return jsonView;
	}
	
}