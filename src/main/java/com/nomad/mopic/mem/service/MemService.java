package com.nomad.mopic.mem.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nomad.mopic.mem.mapper.MemMapper;
import com.nomad.mopic.vo.MemberVO;

@Service
public class MemService implements MemMapper {
	@Resource(name = "memMapper")
    private MemMapper memMapper;
	
	@Override
	public MemberVO selectUserInfo(Map map) {
		return memMapper.selectUserInfo(map);
	}
	
	@Override
	public int insertUserInfo(Map<String, String> map) {
		return memMapper.insertUserInfo(map);
	}
	
	@Override
	public int updateUserInfo(MemberVO memberVO) {
		return memMapper.updateUserInfo(memberVO);
	}

	public void updateLoginInfo(Map<String, String> map) {
		memMapper.updateLoginInfo(map);
	}

	public void updateLogoutInfo(Map<String, String> map) {
		memMapper.updateLogoutInfo(map);
	}
	
	@Override
	public List selectSigunguAddress(Map map) {
		return memMapper.selectSigunguAddress(map);
	}
}