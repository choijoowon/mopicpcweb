package com.nomad.mopic.mem.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.MemberVO;

@Repository(value = "memMapper")
public interface MemMapper {
	public MemberVO selectUserInfo(Map userId);
	public int insertUserInfo(Map<String, String> userInfo);
	public int updateUserInfo(MemberVO memberVO);
	public void updateLoginInfo(Map<String, String> map);
	public void updateLogoutInfo(Map<String, String> map);
	
	public List selectSigunguAddress(Map map);
}