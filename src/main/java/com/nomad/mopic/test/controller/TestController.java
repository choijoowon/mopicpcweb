package com.nomad.mopic.test.controller;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.code.controller.CommonCodeController;
import com.nomad.mopic.code.service.CommonCodeService;

@Controller
public class TestController {
	private static final Logger logger = LoggerFactory.getLogger(TestController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	public CommonCodeService commonCodeService;
	

	/**
	 * 테스트 메인페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String index(Locale locale, Model model) {
		return "methodTest";
	}
	
	/**
	 * JSON REQUEST 테스트
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/test/getJsonRequestTestValue", method = RequestMethod.POST)
	public View getJsonRequestTestValue(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap)  {
		try {
			logger.debug("123");
			if(logger.isDebugEnabled()) {
				logger.debug("↓========================================↓");
				logger.debug("userId : " + (String) inputMap.get("userId"));
				logger.debug("↑========================================↑");
			}
			modelMap.addAttribute("msg"	, "OK");
		} catch (Exception e) {
			modelMap.put("msg", "ERROR");
		}
		
		return jsonView;
	}

}
