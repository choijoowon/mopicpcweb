package com.nomad.mopic.exchange.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.exchange.service.ExchangeService;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.ExchangeVO;

@Controller
public class ExchangeController {
	private static final Logger logger = LoggerFactory.getLogger(ExchangeController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	public ExchangeService exchangeService;

	@Autowired
	public CommonService commonService;
	
	/**
	 * 환율 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/exchange/exchange")
	public String exchange(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		
		model.put("nationOpts", exchangeService.selectExchangeNation(null));
		commonService.setReactSearchQuery(request, model);
		
		return "exchange-page/exchange-page";
	}
	
	
	
	/*****************************************************************************************************************************************/
	/* 이하 API 영역 
	/*****************************************************************************************************************************************/
	
	/**
	 * 환전할 국가 정보 조회
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/exchange/selectExchangeNation")
	public View selectExchangeNation(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		
		try {
			
			map.put("data", exchangeService.selectExchangeNation(inputMap));
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/exchange/selectExchangeNaion]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 환율 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/exchange/selectExchangeList")
	public View selectExchangeList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
		HashMap filterStat = new HashMap();
		
		try {
			
			
			List<Map> rList = exchangeService.selectExchangeList(inputMap);
			
			List filterStatList = new ArrayList();
			filterStat.put("filter_cnt", rList.size()+"");
			filterStat.put("filter_name", "전체");
			filterStat.put("filter_cd", "99999999");
			filterStatList.add(filterStat);
			
			for ( Map map : rList){
//				map.put("saved_money", ((java.math.BigDecimal)map.get("exchange_money")).subtract((java.math.BigDecimal)rList.get(rList.size()-1).get("exchange_money")));
				map.put("saved_money", String.format("%,.2f", Double.parseDouble(map.get("exchange_money")+"") - Double.parseDouble(rList.get(rList.size()-1).get("exchange_money")+"")) );
				map.put("exchange_money", String.format("%,.2f",Double.parseDouble(map.get("exchange_money")+"")));
			}

			CommonUtil.makeProductResponseMap("200", inputMap, rList, filterStatList, modelMap);
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/exchange/selectExchangeList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}
	
	/**
	 * 일배치 - 환전 리스트 가져오기
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/scrapExchangeList", method = RequestMethod.GET)
	public View scrapExchangeList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();
		
		map = exchangeService.scrapExchangeList();
		
		modelMap.put("result", map);
		
		return jsonView;
	}

}
