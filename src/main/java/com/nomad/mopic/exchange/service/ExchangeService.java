package com.nomad.mopic.exchange.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.exchange.mapper.ExchangeMapper;
import com.nomad.mopic.vo.ExchangeVO;

@Service
public class ExchangeService {
	@Resource(name = "exchangeMapper")
    private ExchangeMapper exchangeMapper;

	public void insertExchangeData(ExchangeVO vo) {
		exchangeMapper.insertExchangeData(vo);
	}
	
	public List selectExchangeNation(Map searchQuery){
		return exchangeMapper.selectExchangeNation(searchQuery);
	}
	
	public List selectExchangeList(Map searchQuery){
		return exchangeMapper.selectExchangeList(searchQuery);
	}
	
	public Map<String, Object> scrapExchangeList() {
		Map<String, Object> map	= new HashMap<String, Object>();
		String url		= StaticObject.Url.URL_TOUCHBANK_SERVER;
		String param	= "/api/Exchange.do";
		String result	= "";
		int scrapCnt	= 0;
		int scrapLimit	= 5;
		
		try {
			for(int i = 0; i < scrapLimit; i++) {
				result = CommonUtil.urlConnect(url, param, "UTF-8", null);
				
				ObjectMapper om	= new ObjectMapper();
				JsonNode node	= om.readTree(result);
				ArrayList<ExchangeVO> exchangeList	= om.readValue(node.get("result").get("list").toString(), new TypeReference<ArrayList<ExchangeVO>>(){});
				
				if(exchangeList.size() > 0) {
					ExchangeVO exchangeVO = new ExchangeVO();
					this.deleteExchangeData(exchangeVO);
					
					for(ExchangeVO vo : exchangeList) {
						this.insertExchangeData(vo);
						scrapCnt++;
					}
					
					break;
				}
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, scrapCnt);
			map.put("data"			, "");
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		return map;
	}

	public void deleteExchangeData(ExchangeVO exchangeVO) {
		exchangeMapper.deleteExchangeData(exchangeVO);
	}
}