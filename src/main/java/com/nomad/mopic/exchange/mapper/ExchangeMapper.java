package com.nomad.mopic.exchange.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.ExchangeVO;

@Repository(value = "exchangeMapper")
public interface ExchangeMapper {
	void insertExchangeData(ExchangeVO vo);
	
	public List selectExchangeNation(Map searchQuery);
	
	public List selectExchangeList(Map searchQuery);

	void deleteExchangeData(ExchangeVO exchangeVO);
	
}
