package com.nomad.mopic.vo;

public class GlobalVO {
	// 전역
	public String userId;			// 사용자 아이디
	public int insertKey;			// 인서트된 키값
	public String mainMenu;			// 메인메뉴아이디
	public String subMenu;			// 서브메뉴아이디
	public String searchKey;		// 검색어
	public String searchOption;		// 조회조건1
	public String searchOrder;		// 정렬
	public String searchStartDate;	// 조회시작일
	public String searchEndDate;	// 조회종료일
	public String regDate;			// 등록일
	public String regDateTime;		// 등록일시
	public String updDate;			// 수정일
	public String regId;			// 등록자아이디
	public String updId;			// 수정자아이디
	
	// 페이징
	public int pageNumber;			// 현재페이지
	public int rowCount;			// 한 페이지에 표시될 row 수
	public int totCnt;				// 조회결과 total
	public String naviCount;		// 네비게이션페이지 표시범위
	public String rnum;				// rowindex
	

	// getters and setters
	public int getInsertKey() {
		return insertKey;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setInsertKey(int insertKey) {
		this.insertKey = insertKey;
	}
	public String getMainMenu() {
		return mainMenu;
	}
	public void setMainMenu(String mainMenu) {
		this.mainMenu = mainMenu;
	}
	public String getSubMenu() {
		return subMenu;
	}
	public void setSubMenu(String subMenu) {
		this.subMenu = subMenu;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getSearchOption() {
		return searchOption;
	}
	public void setSearchOption(String searchOption) {
		this.searchOption = searchOption;
	}
	public String getSearchOrder() {
		return searchOrder;
	}
	public void setSearchOrder(String searchOrder) {
		this.searchOrder = searchOrder;
	}
	public String getSearchStartDate() {
		return searchStartDate;
	}
	public void setSearchStartDate(String searchStartDate) {
		this.searchStartDate = searchStartDate;
	}
	public String getSearchEndDate() {
		return searchEndDate;
	}
	public void setSearchEndDate(String searchEndDate) {
		this.searchEndDate = searchEndDate;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	public String getRegDateTime() {
		return regDateTime;
	}
	public void setRegDateTime(String regDateTime) {
		this.regDateTime = regDateTime;
	}
	public String getUpdDate() {
		return updDate;
	}
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	public String getRegId() {
		return regId;
	}
	public void setRegId(String regId) {
		this.regId = regId;
	}
	public String getUpdId() {
		return updId;
	}
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getRowCount() {
		return rowCount;
	}
	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}
	public int getTotCnt() {
		return totCnt;
	}
	public void setTotCnt(int totCnt) {
		this.totCnt = totCnt;
	}
	public String getNaviCount() {
		return naviCount;
	}
	public void setNaviCount(String naviCount) {
		this.naviCount = naviCount;
	}
	public String getRnum() {
		return rnum;
	}
	public void setRnum(String rnum) {
		this.rnum = rnum;
	}
}
