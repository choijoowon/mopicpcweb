package com.nomad.mopic.vo;

public class CommonCodeVO extends GlobalVO {
	// 공통코드그룹
	public String grpName;		// 그룹명
	public String grpDesc;		// 그룹설명
	public String grpCdList;	// 그룹코드리스트
	
	// 공통코드
	public String cd;			// 코드
	public String cdName;		// 코드명
	public String cdDesc;		// 코드설명
	
	// 공통
	public String grpCd;		// 그룹코드
	public int sort;			// 정렬
	public String detailYn;		// 상세조회여부
	public String delYn;		// 삭제여부
	public String ref1;			// 참고1
	public String ref2;			// 참고2
	public String ref3;			// 참고3
	public String ref4;			// 참고4
	public String ref5;			// 참고5
	public String category;		// 카테고리
	
	// Getters and Setters
	public String getGrpName() {
		return grpName;
	}
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	public String getGrpDesc() {
		return grpDesc;
	}
	public void setGrpDesc(String grpDesc) {
		this.grpDesc = grpDesc;
	}
	public String getGrpCdList() {
		return grpCdList;
	}
	public void setGrpCdList(String grpCdList) {
		this.grpCdList = grpCdList;
	}
	public String getCd() {
		return cd;
	}
	public void setCd(String cd) {
		this.cd = cd;
	}
	public String getCdName() {
		return cdName;
	}
	public void setCdName(String cdName) {
		this.cdName = cdName;
	}
	public String getCdDesc() {
		return cdDesc;
	}
	public void setCdDesc(String cdDesc) {
		this.cdDesc = cdDesc;
	}
	public String getGrpCd() {
		return grpCd;
	}
	public void setGrpCd(String grpCd) {
		this.grpCd = grpCd;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public String getDetailYn() {
		return detailYn;
	}
	public void setDetailYn(String detailYn) {
		this.detailYn = detailYn;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getRef1() {
		return ref1;
	}
	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}
	public String getRef2() {
		return ref2;
	}
	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}
	public String getRef3() {
		return ref3;
	}
	public void setRef3(String ref3) {
		this.ref3 = ref3;
	}
	public String getRef4() {
		return ref4;
	}
	public void setRef4(String ref4) {
		this.ref4 = ref4;
	}
	public String getRef5() {
		return ref5;
	}
	public void setRef5(String ref5) {
		this.ref5 = ref5;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
}
