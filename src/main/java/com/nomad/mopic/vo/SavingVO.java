package com.nomad.mopic.vo;

public class SavingVO extends GlobalVO {
	public int rowCnt;				// merge 쿼리용
	
	// 예금
	public int amount;				// 사용자입력예치금
	public int period;				// 사용자입력예치기간
	public String periodCd;			// 사용자입력예치기간코드
	public int totalAmount;			// 만기수령액
	public int beforeTax;			// 세전이자
	public int afterTax;			// 세후이자
	public int standardTax;			// 기준이자
	public String interest_yn;		// 관심상품여부
	
	public int totalCnt;			// 전체카운트
	public int firstGradeCnt;		// 1금융권카운트
	public int secondGradeCnt;		// 2금융권카운트
	public int elseGradeCnt;		// 기타카운트
	public String detail_url;		// 상세페이지 url
	
	public String AC_GROUP;			// 상품그룹(비교검색시)
	public String AC_CODE;			// 예적금상품코드
	public String DS_CODE;			// 예적금코드(A000101:예금, A000102:적금, A000103:MMDA, A000104:주택청약)
	public String DS_FN_CODE;		// 금융권코드
	public String DS_FN_CODE_NM;	// 금융권명
	public String DS_NM;			// 상품명
	public String DS_BANKING;		// 은행명코드
	public String DS_BANKING_NM;	// 은행명
	public String BANK_IMG;			// 은행로고
	public String DS_DATE1;			// 예치기간1코드
	public String DS_DATE1_NM;		// 예치기간1
	public String DS_DATE2;			// 예치기간2코드
	public String DS_DATE2_NM;		// 예치기간2
	public String DS_SUMMARY1;		// 상품설명1
	public String DS_SUMMARY2;		// 상품설명2
	public int DS_JOIN_LIMIT;		// 가입금액범위
	public String DS_TARGET;		// 가입대상
	public String DS_TARGET_NM;		// 가입대상명
	public String DS_PRE_TAX;		// 이율
	public String DS_SAVE;			// 적립방법
	public String DS_TAX;			// 절세혜택
	public String DS_CHARACTER;		// 특징
	public String DS_ETC;			// 기타안내
	public String DS_PR_INFO;		// 우대금리설명
	public String DS_INTER;			// 이자지급방식
	public String DS_RATE3;			// 기간별금리3개월
	public String DS_RATE6;			// 기간별금리6개월
	public String DS_RATE12;		// 기간별금리12개월
	public String DS_RATE24;		// 기간별금리24개월
	public String DS_RATE36;		// 기간별금리36개월
	public String DS_INTERNET_YN;	// 인터넷가입가입여부(Y/N)
	public String DS_PHONE_YN;		// 스마트폰가입여부(Y/N)
	public String DS_TAXFREE_YN;	// 비과세여부(Y/N)
	public String DS_PROTECT_YN;	// 예금자보호안내(Y/N)
	public String DS_IS_YN;			// 온라인가입여부
	public String UPD_YN;			// 업데이트여부(Y/N)
	public String USE_YN;			// 사용여부(Y/N)
	public String DEL_YN;			// 삭제여부(Y/N)
	public String INS_DATE;			// 상품출시일
	public String DS_LOCAL;			// 지역정보
	public String DS_PERIOD;		// 조회기간

	public String BASE_RATE;		// 조회기준금리
	public String LANDING_URL;		// 상품정보링크
	
	//예금수수료
	public String CH_BANK;			// 은행코드
	public int CH_BANKOPEN;			// 자행 마감 전 인출수수료
	public int CH_BANKCLOSE;		// 자행 마감 후 인출수수료
	public int CH_OBANKOPEN;		// 타행 마감 전 인출수수료
	public int CH_OBANKCLOSE;		// 타행 마감 후 인출수수료
	public int CH_SBCOUNTER;		// 자행 창구이용 송금수수료
	public int CH_SBAUTOOPEN;		// 자행 자동화기기 마감 전 송금수수료
	public int CH_SBAUTOCLOSE;		// 자행 자동화기기 마감 후 송금수수료
	public int CH_SBINBANK;			// 자행 인터넷뱅킹 송금수수료
	public int CH_SBTELBANK;		// 자행 텔레뱅킹 송금수수료
	public int CH_SBMOBANK;			// 자행 모바일뱅킹 송금수수료
	public int CH_OBCOUNTER;		// 타행 창구이용 송금수수료
	public int CH_OBAUTOOPEN;		// 타행 자동화기기 마감 전 송금수수료
	public int CH_OBAUTOCLOSE;		// 타행 자동화기기 마감 후 송금수수료
	public int CH_OBINBANK;			// 타행 인터넷뱅킹 송금수수료
	public int CH_OBTELBANK;		// 타행 텔레뱅킹 송금수수료
	public int CH_OBMOBANK;			// 타행 모바일뱅킹 송금수수료
	public String CH_ETCCLASS;		// 기타수수료 구분
	public String CH_CHBASIC;		// 기타수수료 기준
	public int CH_PAY;				// 기타수수료 금액
	public String CH_ETC;			// 기타수수료 기타
	public String CH_DATE;			// 날짜
	
	public int getRowCnt() {
		return rowCnt;
	}
	public void setRowCnt(int rowCnt) {
		this.rowCnt = rowCnt;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public String getPeriodCd() {
		return periodCd;
	}
	public void setPeriodCd(String periodCd) {
		this.periodCd = periodCd;
	}
	public int getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}
	public int getBeforeTax() {
		return beforeTax;
	}
	public void setBeforeTax(int beforeTax) {
		this.beforeTax = beforeTax;
	}
	public int getAfterTax() {
		return afterTax;
	}
	public void setAfterTax(int afterTax) {
		this.afterTax = afterTax;
	}
	public int getStandardTax() {
		return standardTax;
	}
	public void setStandardTax(int standardTax) {
		this.standardTax = standardTax;
	}
	public String getInterest_yn() {
		return interest_yn;
	}
	public void setInterest_yn(String interest_yn) {
		this.interest_yn = interest_yn;
	}
	public int getTotalCnt() {
		return totalCnt;
	}
	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}
	public int getFirstGradeCnt() {
		return firstGradeCnt;
	}
	public void setFirstGradeCnt(int firstGradeCnt) {
		this.firstGradeCnt = firstGradeCnt;
	}
	public int getSecondGradeCnt() {
		return secondGradeCnt;
	}
	public void setSecondGradeCnt(int secondGradeCnt) {
		this.secondGradeCnt = secondGradeCnt;
	}
	public int getElseGradeCnt() {
		return elseGradeCnt;
	}
	public void setElseGradeCnt(int elseGradeCnt) {
		this.elseGradeCnt = elseGradeCnt;
	}
	public String getDetail_url() {
		return detail_url;
	}
	public void setDetail_url(String detail_url) {
		this.detail_url = detail_url;
	}
	public String getAC_CODE() {
		return AC_CODE;
	}
	public void setAC_CODE(String aC_CODE) {
		AC_CODE = aC_CODE;
	}
	public String getAC_GROUP() {
		return AC_GROUP;
	}
	public void setAC_GROUP(String aC_GROUP) {
		AC_GROUP = aC_GROUP;
	}
	public String getDS_CODE() {
		return DS_CODE;
	}
	public void setDS_CODE(String dS_CODE) {
		DS_CODE = dS_CODE;
	}
	public String getDS_FN_CODE() {
		return DS_FN_CODE;
	}
	public void setDS_FN_CODE(String dS_FN_CODE) {
		DS_FN_CODE = dS_FN_CODE;
	}
	public String getDS_FN_CODE_NM() {
		return DS_FN_CODE_NM;
	}
	public void setDS_FN_CODE_NM(String dS_FN_CODE_NM) {
		DS_FN_CODE_NM = dS_FN_CODE_NM;
	}
	public String getDS_NM() {
		return DS_NM;
	}
	public void setDS_NM(String dS_NM) {
		DS_NM = dS_NM;
	}
	public String getDS_BANKING() {
		return DS_BANKING;
	}
	public void setDS_BANKING(String dS_BANKING) {
		DS_BANKING = dS_BANKING;
	}
	public String getDS_BANKING_NM() {
		return DS_BANKING_NM;
	}
	public void setDS_BANKING_NM(String dS_BANKING_NM) {
		DS_BANKING_NM = dS_BANKING_NM;
	}
	public String getBANK_IMG() {
		return BANK_IMG;
	}
	public void setBANK_IMG(String bANK_IMG) {
		BANK_IMG = bANK_IMG;
	}
	public String getDS_DATE1() {
		return DS_DATE1;
	}
	public void setDS_DATE1(String dS_DATE1) {
		DS_DATE1 = dS_DATE1;
	}
	public String getDS_DATE1_NM() {
		return DS_DATE1_NM;
	}
	public void setDS_DATE1_NM(String dS_DATE1_NM) {
		DS_DATE1_NM = dS_DATE1_NM;
	}
	public String getDS_DATE2() {
		return DS_DATE2;
	}
	public void setDS_DATE2(String dS_DATE2) {
		DS_DATE2 = dS_DATE2;
	}
	public String getDS_DATE2_NM() {
		return DS_DATE2_NM;
	}
	public void setDS_DATE2_NM(String dS_DATE2_NM) {
		DS_DATE2_NM = dS_DATE2_NM;
	}
	public String getDS_SUMMARY1() {
		return DS_SUMMARY1;
	}
	public void setDS_SUMMARY1(String dS_SUMMARY1) {
		DS_SUMMARY1 = dS_SUMMARY1;
	}
	public String getDS_SUMMARY2() {
		return DS_SUMMARY2;
	}
	public void setDS_SUMMARY2(String dS_SUMMARY2) {
		DS_SUMMARY2 = dS_SUMMARY2;
	}
	public int getDS_JOIN_LIMIT() {
		return DS_JOIN_LIMIT;
	}
	public void setDS_JOIN_LIMIT(int dS_JOIN_LIMIT) {
		DS_JOIN_LIMIT = dS_JOIN_LIMIT;
	}
	public String getDS_TARGET() {
		return DS_TARGET;
	}
	public void setDS_TARGET(String dS_TARGET) {
		DS_TARGET = dS_TARGET;
	}
	public String getDS_TARGET_NM() {
		return DS_TARGET_NM;
	}
	public void setDS_TARGET_NM(String dS_TARGET_NM) {
		DS_TARGET_NM = dS_TARGET_NM;
	}
	public String getDS_PRE_TAX() {
		return DS_PRE_TAX;
	}
	public void setDS_PRE_TAX(String dS_PRE_TAX) {
		DS_PRE_TAX = dS_PRE_TAX;
	}
	public String getDS_SAVE() {
		return DS_SAVE;
	}
	public void setDS_SAVE(String dS_SAVE) {
		DS_SAVE = dS_SAVE;
	}
	public String getDS_TAX() {
		return DS_TAX;
	}
	public void setDS_TAX(String dS_TAX) {
		DS_TAX = dS_TAX;
	}
	public String getDS_CHARACTER() {
		return DS_CHARACTER;
	}
	public void setDS_CHARACTER(String dS_CHARACTER) {
		DS_CHARACTER = dS_CHARACTER;
	}
	public String getDS_ETC() {
		return DS_ETC;
	}
	public void setDS_ETC(String dS_ETC) {
		DS_ETC = dS_ETC;
	}
	public String getDS_PR_INFO() {
		return DS_PR_INFO;
	}
	public void setDS_PR_INFO(String dS_PR_INFO) {
		DS_PR_INFO = dS_PR_INFO;
	}
	public String getDS_INTER() {
		return DS_INTER;
	}
	public void setDS_INTER(String dS_INTER) {
		DS_INTER = dS_INTER;
	}
	public String getDS_RATE3() {
		return DS_RATE3;
	}
	public void setDS_RATE3(String dS_RATE3) {
		DS_RATE3 = dS_RATE3;
	}
	public String getDS_RATE6() {
		return DS_RATE6;
	}
	public void setDS_RATE6(String dS_RATE6) {
		DS_RATE6 = dS_RATE6;
	}
	public String getDS_RATE12() {
		return DS_RATE12;
	}
	public void setDS_RATE12(String dS_RATE12) {
		DS_RATE12 = dS_RATE12;
	}
	public String getDS_RATE24() {
		return DS_RATE24;
	}
	public void setDS_RATE24(String dS_RATE24) {
		DS_RATE24 = dS_RATE24;
	}
	public String getDS_RATE36() {
		return DS_RATE36;
	}
	public void setDS_RATE36(String dS_RATE36) {
		DS_RATE36 = dS_RATE36;
	}
	public String getDS_INTERNET_YN() {
		return DS_INTERNET_YN;
	}
	public void setDS_INTERNET_YN(String dS_INTERNET_YN) {
		DS_INTERNET_YN = dS_INTERNET_YN;
	}
	public String getDS_PHONE_YN() {
		return DS_PHONE_YN;
	}
	public void setDS_PHONE_YN(String dS_PHONE_YN) {
		DS_PHONE_YN = dS_PHONE_YN;
	}
	public String getDS_TAXFREE_YN() {
		return DS_TAXFREE_YN;
	}
	public void setDS_TAXFREE_YN(String dS_TAXFREE_YN) {
		DS_TAXFREE_YN = dS_TAXFREE_YN;
	}
	public String getDS_PROTECT_YN() {
		return DS_PROTECT_YN;
	}
	public void setDS_PROTECT_YN(String dS_PROTECT_YN) {
		DS_PROTECT_YN = dS_PROTECT_YN;
	}
	public String getDS_IS_YN() {
		return DS_IS_YN;
	}
	public void setDS_IS_YN(String dS_IS_YN) {
		DS_IS_YN = dS_IS_YN;
	}
	public String getUPD_YN() {
		return UPD_YN;
	}
	public void setUPD_YN(String uPD_YN) {
		UPD_YN = uPD_YN;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getINS_DATE() {
		return INS_DATE;
	}
	public void setINS_DATE(String iNS_DATE) {
		INS_DATE = iNS_DATE;
	}
	public String getDS_LOCAL() {
		return DS_LOCAL;
	}
	public void setDS_LOCAL(String dS_LOCAL) {
		DS_LOCAL = dS_LOCAL;
	}
	public String getDS_PERIOD() {
		return DS_PERIOD;
	}
	public void setDS_PERIOD(String dS_PERIOD) {
		DS_PERIOD = dS_PERIOD;
	}
	public String getBASE_RATE() {
		return BASE_RATE;
	}
	public void setBASE_RATE(String bASE_RATE) {
		BASE_RATE = bASE_RATE;
	}
	public String getLANDING_URL() {
		return LANDING_URL;
	}
	public void setLANDING_URL(String lANDING_URL) {
		LANDING_URL = lANDING_URL;
	}
	public String getCH_BANK() {
		return CH_BANK;
	}
	public void setCH_BANK(String cH_BANK) {
		CH_BANK = cH_BANK;
	}
	public int getCH_BANKOPEN() {
		return CH_BANKOPEN;
	}
	public void setCH_BANKOPEN(int cH_BANKOPEN) {
		CH_BANKOPEN = cH_BANKOPEN;
	}
	public int getCH_BANKCLOSE() {
		return CH_BANKCLOSE;
	}
	public void setCH_BANKCLOSE(int cH_BANKCLOSE) {
		CH_BANKCLOSE = cH_BANKCLOSE;
	}
	public int getCH_OBANKOPEN() {
		return CH_OBANKOPEN;
	}
	public void setCH_OBANKOPEN(int cH_OBANKOPEN) {
		CH_OBANKOPEN = cH_OBANKOPEN;
	}
	public int getCH_OBANKCLOSE() {
		return CH_OBANKCLOSE;
	}
	public void setCH_OBANKCLOSE(int cH_OBANKCLOSE) {
		CH_OBANKCLOSE = cH_OBANKCLOSE;
	}
	public int getCH_SBCOUNTER() {
		return CH_SBCOUNTER;
	}
	public void setCH_SBCOUNTER(int cH_SBCOUNTER) {
		CH_SBCOUNTER = cH_SBCOUNTER;
	}
	public int getCH_SBAUTOOPEN() {
		return CH_SBAUTOOPEN;
	}
	public void setCH_SBAUTOOPEN(int cH_SBAUTOOPEN) {
		CH_SBAUTOOPEN = cH_SBAUTOOPEN;
	}
	public int getCH_SBAUTOCLOSE() {
		return CH_SBAUTOCLOSE;
	}
	public void setCH_SBAUTOCLOSE(int cH_SBAUTOCLOSE) {
		CH_SBAUTOCLOSE = cH_SBAUTOCLOSE;
	}
	public int getCH_SBINBANK() {
		return CH_SBINBANK;
	}
	public void setCH_SBINBANK(int cH_SBINBANK) {
		CH_SBINBANK = cH_SBINBANK;
	}
	public int getCH_SBTELBANK() {
		return CH_SBTELBANK;
	}
	public void setCH_SBTELBANK(int cH_SBTELBANK) {
		CH_SBTELBANK = cH_SBTELBANK;
	}
	public int getCH_SBMOBANK() {
		return CH_SBMOBANK;
	}
	public void setCH_SBMOBANK(int cH_SBMOBANK) {
		CH_SBMOBANK = cH_SBMOBANK;
	}
	public int getCH_OBCOUNTER() {
		return CH_OBCOUNTER;
	}
	public void setCH_OBCOUNTER(int cH_OBCOUNTER) {
		CH_OBCOUNTER = cH_OBCOUNTER;
	}
	public int getCH_OBAUTOOPEN() {
		return CH_OBAUTOOPEN;
	}
	public void setCH_OBAUTOOPEN(int cH_OBAUTOOPEN) {
		CH_OBAUTOOPEN = cH_OBAUTOOPEN;
	}
	public int getCH_OBAUTOCLOSE() {
		return CH_OBAUTOCLOSE;
	}
	public void setCH_OBAUTOCLOSE(int cH_OBAUTOCLOSE) {
		CH_OBAUTOCLOSE = cH_OBAUTOCLOSE;
	}
	public int getCH_OBINBANK() {
		return CH_OBINBANK;
	}
	public void setCH_OBINBANK(int cH_OBINBANK) {
		CH_OBINBANK = cH_OBINBANK;
	}
	public int getCH_OBTELBANK() {
		return CH_OBTELBANK;
	}
	public void setCH_OBTELBANK(int cH_OBTELBANK) {
		CH_OBTELBANK = cH_OBTELBANK;
	}
	public int getCH_OBMOBANK() {
		return CH_OBMOBANK;
	}
	public void setCH_OBMOBANK(int cH_OBMOBANK) {
		CH_OBMOBANK = cH_OBMOBANK;
	}
	public String getCH_ETCCLASS() {
		return CH_ETCCLASS;
	}
	public void setCH_ETCCLASS(String cH_ETCCLASS) {
		CH_ETCCLASS = cH_ETCCLASS;
	}
	public String getCH_CHBASIC() {
		return CH_CHBASIC;
	}
	public void setCH_CHBASIC(String cH_CHBASIC) {
		CH_CHBASIC = cH_CHBASIC;
	}
	public int getCH_PAY() {
		return CH_PAY;
	}
	public void setCH_PAY(int cH_PAY) {
		CH_PAY = cH_PAY;
	}
	public String getCH_ETC() {
		return CH_ETC;
	}
	public void setCH_ETC(String cH_ETC) {
		CH_ETC = cH_ETC;
	}
	public String getCH_DATE() {
		return CH_DATE;
	}
	public void setCH_DATE(String cH_DATE) {
		CH_DATE = cH_DATE;
	}
	@Override
	public String toString() {
		return "SavingVO [rowCnt=" + rowCnt + ", amount=" + amount + ", period=" + period + ", periodCd=" + periodCd
				+ ", totalAmount=" + totalAmount + ", beforeTax=" + beforeTax + ", afterTax=" + afterTax
				+ ", standardTax=" + standardTax + ", interest_yn=" + interest_yn + ", totalCnt=" + totalCnt
				+ ", firstGradeCnt=" + firstGradeCnt + ", secondGradeCnt=" + secondGradeCnt + ", elseGradeCnt="
				+ elseGradeCnt + ", detail_url=" + detail_url + ", AC_GROUP=" + AC_GROUP + ", AC_CODE=" + AC_CODE
				+ ", DS_CODE=" + DS_CODE + ", DS_FN_CODE=" + DS_FN_CODE + ", DS_FN_CODE_NM=" + DS_FN_CODE_NM
				+ ", DS_NM=" + DS_NM + ", DS_BANKING=" + DS_BANKING + ", DS_BANKING_NM=" + DS_BANKING_NM + ", BANK_IMG="
				+ BANK_IMG + ", DS_DATE1=" + DS_DATE1 + ", DS_DATE1_NM=" + DS_DATE1_NM + ", DS_DATE2=" + DS_DATE2
				+ ", DS_DATE2_NM=" + DS_DATE2_NM + ", DS_SUMMARY1=" + DS_SUMMARY1 + ", DS_SUMMARY2=" + DS_SUMMARY2
				+ ", DS_JOIN_LIMIT=" + DS_JOIN_LIMIT + ", DS_TARGET=" + DS_TARGET + ", DS_PRE_TAX=" + DS_PRE_TAX
				+ ", DS_SAVE=" + DS_SAVE + ", DS_TAX=" + DS_TAX + ", DS_CHARACTER=" + DS_CHARACTER + ", DS_ETC="
				+ DS_ETC + ", DS_PR_INFO=" + DS_PR_INFO + ", DS_INTER=" + DS_INTER + ", DS_RATE3=" + DS_RATE3
				+ ", DS_RATE6=" + DS_RATE6 + ", DS_RATE12=" + DS_RATE12 + ", DS_RATE24=" + DS_RATE24 + ", DS_RATE36="
				+ DS_RATE36 + ", DS_INTERNET_YN=" + DS_INTERNET_YN + ", DS_PHONE_YN=" + DS_PHONE_YN + ", DS_TAXFREE_YN="
				+ DS_TAXFREE_YN + ", DS_PROTECT_YN=" + DS_PROTECT_YN + ", DS_IS_YN=" + DS_IS_YN + ", UPD_YN=" + UPD_YN
				+ ", USE_YN=" + USE_YN + ", INS_DATE=" + INS_DATE + ", CH_BANK=" + CH_BANK + ", CH_BANKOPEN="
				+ CH_BANKOPEN + ", CH_BANKCLOSE=" + CH_BANKCLOSE + ", CH_OBANKOPEN=" + CH_OBANKOPEN + ", CH_OBANKCLOSE="
				+ CH_OBANKCLOSE + ", CH_SBCOUNTER=" + CH_SBCOUNTER + ", CH_SBAUTOOPEN=" + CH_SBAUTOOPEN
				+ ", CH_SBAUTOCLOSE=" + CH_SBAUTOCLOSE + ", CH_SBINBANK=" + CH_SBINBANK + ", CH_SBTELBANK="
				+ CH_SBTELBANK + ", CH_SBMOBANK=" + CH_SBMOBANK + ", CH_OBCOUNTER=" + CH_OBCOUNTER + ", CH_OBAUTOOPEN="
				+ CH_OBAUTOOPEN + ", CH_OBAUTOCLOSE=" + CH_OBAUTOCLOSE + ", CH_OBINBANK=" + CH_OBINBANK
				+ ", CH_OBTELBANK=" + CH_OBTELBANK + ", CH_OBMOBANK=" + CH_OBMOBANK + ", CH_ETCCLASS=" + CH_ETCCLASS
				+ ", CH_CHBASIC=" + CH_CHBASIC + ", CH_PAY=" + CH_PAY + ", CH_ETC=" + CH_ETC + ", CH_DATE=" + CH_DATE
				+ "]";
	}
}
