package com.nomad.mopic.vo;

import java.util.List;

public class BoardVO extends GlobalVO {
	// 공통
	public int bltnCd;					// 게시코드
	
	// 게시글
	public int bltnType;				// 게시유형
	public int bltnName;				// 유형명
	public int category;				// 카테고리
	public int categoryGrpCd;			// 카테고리그룹코드
	public int upperBltnCd;				// 상위게시코드
	public int bltnLvl;					// 게시레벨
	public String topNoticeYn;			// 상단공지여부
	public String categoryName;			// 카테고리명
	public String title;				// 제목
	public String cts;					// 내용
	public String bltnStrtDate;			// 게시시작일
	public String bltnEndDate;			// 게시종료일
	public String delYn;				// 삭제여부
	public String ansCnt;				// 답변수
	public String answer;				// 답변
	public String fileCnt;				// 첨부파일수
	public List<FileVO> fileVO;			// 첨부파일
	
	// getters and setters
	public int getBltnCd() {
		return bltnCd;
	}
	public void setBltnCd(int bltnCd) {
		this.bltnCd = bltnCd;
	}
	public int getBltnType() {
		return bltnType;
	}
	public void setBltnType(int bltnType) {
		this.bltnType = bltnType;
	}
	public int getBltnName() {
		return bltnName;
	}
	public void setBltnName(int bltnName) {
		this.bltnName = bltnName;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getCategoryGrpCd() {
		return categoryGrpCd;
	}
	public String getTopNoticeYn() {
		return topNoticeYn;
	}
	public void setTopNoticeYn(String topNoticeYn) {
		this.topNoticeYn = topNoticeYn;
	}
	public void setCategoryGrpCd(int categoryGrpCd) {
		this.categoryGrpCd = categoryGrpCd;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCts() {
		return cts;
	}
	public void setCts(String cts) {
		this.cts = cts;
	}
	public int getUpperBltnCd() {
		return upperBltnCd;
	}
	public void setUpperBltnCd(int upperBltnCd) {
		this.upperBltnCd = upperBltnCd;
	}
	public int getBltnLvl() {
		return bltnLvl;
	}
	public void setBltnLvl(int bltnLvl) {
		this.bltnLvl = bltnLvl;
	}
	public String getBltnStrtDate() {
		return bltnStrtDate;
	}
	public void setBltnStrtDate(String bltnStrtDate) {
		this.bltnStrtDate = bltnStrtDate;
	}
	public String getBltnEndDate() {
		return bltnEndDate;
	}
	public void setBltnEndDate(String bltnEndDate) {
		this.bltnEndDate = bltnEndDate;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getAnsCnt() {
		return ansCnt;
	}
	public void setAnsCnt(String ansCnt) {
		this.ansCnt = ansCnt;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getFileCnt() {
		return fileCnt;
	}
	public void setFileCnt(String fileCnt) {
		this.fileCnt = fileCnt;
	}
	public List<FileVO> getFileVO() {
		return fileVO;
	}
	public void setFileVO(List<FileVO> fileVO) {
		this.fileVO = fileVO;
	}
}
