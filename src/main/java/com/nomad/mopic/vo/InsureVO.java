package com.nomad.mopic.vo;

public class InsureVO extends GlobalVO {

	public String id;	//보험상품 아이디
	public String age;					// 성별
	public String sex;				// sex
	public String prod_code;				// 상품코드
	public String insurance_fee; //보험료
	public String company_code; //회사코드bi
	public String bi; //회사bi
	public String dambo1;
	
	public String dc_grade;
	public String car_type;
	public String age_contract;
	public String driver;
	public String dambo;
		
	public String birthConvertToAge;
	
	public String getBirthConvertToAge() {
		return birthConvertToAge;
	}
	public void setBirthConvertToAge(String birthConvertToAge) {
		this.birthConvertToAge = birthConvertToAge;
	}
	public String getDc_grade() {
		return dc_grade;
	}
	public void setDc_grade(String dc_grade) {
		this.dc_grade = dc_grade;
	}
	public String getCar_type() {
		return car_type;
	}
	public void setCar_type(String car_type) {
		this.car_type = car_type;
	}
	public String getAge_contract() {
		return age_contract;
	}
	public void setAge_contract(String age_contract) {
		this.age_contract = age_contract;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getDambo() {
		return dambo;
	}
	public void setDambo(String dambo) {
		this.dambo = dambo;
	}
	public String getDambo1() {
		return dambo1;
	}
	public void setDambo1(String dambo1) {
		this.dambo1 = dambo1;
	}
	public String getBi() {
		return bi;
	}
	public void setBi(String bi) {
		this.bi = bi;
	}
	public String getCompany_code() {
		return company_code;
	}
	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}
	public String getInsurance_fee() {
		return insurance_fee;
	}
	public void setInsurance_fee(String insurance_fee) {
		this.insurance_fee = insurance_fee;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getProd_code() {
		return prod_code;
	}
	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}
	
	

}
