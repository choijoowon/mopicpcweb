package com.nomad.mopic.vo;

public class LoanVO extends GlobalVO {
	public int rowCnt;					// merge용 
	
	public int totalCnt;				// 전체카운트
	public int firstGradeCnt;			// 1금융권카운트
	public int secondGradeCnt;			// 2금융권카운트
	public int elseGradeCnt;			// 기타카운트
	public int amount;					// 금액
	public int interest;				// 이자
	public String interest_yn;			// 관심상품여부
	public String detail_url;			// 상세페이지 url
	
	public String LA_GROUP;				// 상품그룹(비교검색시)
	public String LA_CODE;				// 대출상품코드
	public String LN_CODE;				// 대출그룹코드(B000101 : 신용, B000102 : 담보, B000103 기타)
	public String LN_FN_CODE;			// 금융권코드
	public String LN_FN_CODE_NM;		// 금융권코드명
	public String LN_BANKING;			// 은행코드
	public String LN_BANKING_NM;		// 은행명
	public String BANK_IMG;				// 은행로고
	public String LN_NM;				// 상품명
	public String LN_CLASS1;			// 신용등급코드1
	public String LN_CLASS1_NM;			// 신용등급1
	public String LN_CLASS2;			// 신용등급코드2
	public String LN_CLASS2_NM;			// 신용등급2
	public String LN_SUMMARY1;			// 상품설명
	public String LN_SUMMARY2;			// 상품설명
	public String LN_ASU_TYPE;			// 담보유형코드
	public String LN_ASU_TYPE_NM;		// 담보유형
	public String LN_RATE;				// 담보인정비율
	public String LN_PERIOD;			// 금리변동주기코드
	public String LN_PERIOD_NM;			// 금리변동주기
	public String LN_NOMAL_DATE;		// 대출기간코드
	public String LN_NOMAL_DATE_NM;		// 대출기간
	public int LN_MINIMUM;				// 대출금액
	public int LN_MAXIMUM;				// 대출금액
	public String LN_MINIMUM_NM;		// 대출금액한글
	public String LN_MAXIMUM_NM;		// 대출금액한
	public String LN_TAR_CONTENT;		// 대출대상
	public String LN_RATE_LOW;			// 최저연이율
	public String LN_RATE_HIGH;			// 최고연이율
	public String LN_PR_INFO;			// 우대금리설명
	public String LN_RATE_INFO;			// 금리설명
	public String LN_AMOUNT_LIMIT;		// 대출금액
	public String LN_REPAY_WAY;			// 상환방식
	public String REPAYMENT_TYPE;		// 상환방식
	public String LN_LM_CONTENT;		// 대출한도
	public String LN_ETC;				// 기타안내
	public String LN_DOCUMENT;			// 제출서류
	public String LN_IS_YN;				// 온라인가입여부
	public String UPD_YN;				// 업데이트여부
	public String USE_YN;				// 사용여부
	public String DEL_YN;				// 삭제여부
	public String INS_DATE;				// 상품출시일
	public String LN_LOCAL;				// 지역정보
	public String BASE_RATE;			// 기준금리
	public String LANDING_URL;			// 상품정보링크
	public String LN_TARGET;			// 가입대상
	
	public int getRowCnt() {
		return rowCnt;
	}
	public void setRowCnt(int rowCnt) {
		this.rowCnt = rowCnt;
	}
	public int getTotalCnt() {
		return totalCnt;
	}
	public void setTotalCnt(int totalCnt) {
		this.totalCnt = totalCnt;
	}
	public int getFirstGradeCnt() {
		return firstGradeCnt;
	}
	public void setFirstGradeCnt(int firstGradeCnt) {
		this.firstGradeCnt = firstGradeCnt;
	}
	public int getSecondGradeCnt() {
		return secondGradeCnt;
	}
	public void setSecondGradeCnt(int secondGradeCnt) {
		this.secondGradeCnt = secondGradeCnt;
	}
	public int getElseGradeCnt() {
		return elseGradeCnt;
	}
	public void setElseGradeCnt(int elseGradeCnt) {
		this.elseGradeCnt = elseGradeCnt;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getInterest() {
		return interest;
	}
	public void setInterest(int interest) {
		this.interest = interest;
	}
	public String getInterest_yn() {
		return interest_yn;
	}
	public void setInterest_yn(String interest_yn) {
		this.interest_yn = interest_yn;
	}
	public String getDetail_url() {
		return detail_url;
	}
	public void setDetail_url(String detail_url) {
		this.detail_url = detail_url;
	}
	public String getLA_GROUP() {
		return LA_GROUP;
	}
	public void setLA_GROUP(String lA_GROUP) {
		LA_GROUP = lA_GROUP;
	}
	public String getLA_CODE() {
		return LA_CODE;
	}
	public void setLA_CODE(String lA_CODE) {
		LA_CODE = lA_CODE;
	}
	public String getLN_CODE() {
		return LN_CODE;
	}
	public void setLN_CODE(String lN_CODE) {
		LN_CODE = lN_CODE;
	}
	public String getLN_FN_CODE() {
		return LN_FN_CODE;
	}
	public void setLN_FN_CODE(String lN_FN_CODE) {
		LN_FN_CODE = lN_FN_CODE;
	}
	public String getLN_FN_CODE_NM() {
		return LN_FN_CODE_NM;
	}
	public void setLN_FN_CODE_NM(String lN_FN_CODE_NM) {
		LN_FN_CODE_NM = lN_FN_CODE_NM;
	}
	public String getLN_BANKING() {
		return LN_BANKING;
	}
	public void setLN_BANKING(String lN_BANKING) {
		LN_BANKING = lN_BANKING;
	}
	public String getLN_BANKING_NM() {
		return LN_BANKING_NM;
	}
	public void setLN_BANKING_NM(String lN_BANKING_NM) {
		LN_BANKING_NM = lN_BANKING_NM;
	}
	public String getBANK_IMG() {
		return BANK_IMG;
	}
	public void setBANK_IMG(String bANK_IMG) {
		BANK_IMG = bANK_IMG;
	}
	public String getLN_NM() {
		return LN_NM;
	}
	public void setLN_NM(String lN_NM) {
		LN_NM = lN_NM;
	}
	public String getLN_CLASS1() {
		return LN_CLASS1;
	}
	public void setLN_CLASS1(String lN_CLASS1) {
		LN_CLASS1 = lN_CLASS1;
	}
	public String getLN_CLASS1_NM() {
		return LN_CLASS1_NM;
	}
	public void setLN_CLASS1_NM(String lN_CLASS1_NM) {
		LN_CLASS1_NM = lN_CLASS1_NM;
	}
	public String getLN_CLASS2() {
		return LN_CLASS2;
	}
	public void setLN_CLASS2(String lN_CLASS2) {
		LN_CLASS2 = lN_CLASS2;
	}
	public String getLN_CLASS2_NM() {
		return LN_CLASS2_NM;
	}
	public void setLN_CLASS2_NM(String lN_CLASS2_NM) {
		LN_CLASS2_NM = lN_CLASS2_NM;
	}
	public String getLN_SUMMARY1() {
		return LN_SUMMARY1;
	}
	public void setLN_SUMMARY1(String lN_SUMMARY1) {
		LN_SUMMARY1 = lN_SUMMARY1;
	}
	public String getLN_SUMMARY2() {
		return LN_SUMMARY2;
	}
	public void setLN_SUMMARY2(String lN_SUMMARY2) {
		LN_SUMMARY2 = lN_SUMMARY2;
	}
	public String getLN_ASU_TYPE() {
		return LN_ASU_TYPE;
	}
	public void setLN_ASU_TYPE(String lN_ASU_TYPE) {
		LN_ASU_TYPE = lN_ASU_TYPE;
	}
	public String getLN_ASU_TYPE_NM() {
		return LN_ASU_TYPE_NM;
	}
	public void setLN_ASU_TYPE_NM(String lN_ASU_TYPE_NM) {
		LN_ASU_TYPE_NM = lN_ASU_TYPE_NM;
	}
	public String getLN_RATE() {
		return LN_RATE;
	}
	public void setLN_RATE(String lN_RATE) {
		LN_RATE = lN_RATE;
	}
	public String getLN_PERIOD() {
		return LN_PERIOD;
	}
	public void setLN_PERIOD(String lN_PERIOD) {
		LN_PERIOD = lN_PERIOD;
	}
	public String getLN_PERIOD_NM() {
		return LN_PERIOD_NM;
	}
	public void setLN_PERIOD_NM(String lN_PERIOD_NM) {
		LN_PERIOD_NM = lN_PERIOD_NM;
	}
	public String getLN_NOMAL_DATE() {
		return LN_NOMAL_DATE;
	}
	public void setLN_NOMAL_DATE(String lN_NOMAL_DATE) {
		LN_NOMAL_DATE = lN_NOMAL_DATE;
	}
	public String getLN_NOMAL_DATE_NM() {
		return LN_NOMAL_DATE_NM;
	}
	public void setLN_NOMAL_DATE_NM(String lN_NOMAL_DATE_NM) {
		LN_NOMAL_DATE_NM = lN_NOMAL_DATE_NM;
	}
	public int getLN_MINIMUM() {
		return LN_MINIMUM;
	}
	public void setLN_MINIMUM(int lN_MINIMUM) {
		LN_MINIMUM = lN_MINIMUM;
	}
	public int getLN_MAXIMUM() {
		return LN_MAXIMUM;
	}
	public void setLN_MAXIMUM(int lN_MAXIMUM) {
		LN_MAXIMUM = lN_MAXIMUM;
	}
	public String getLN_MINIMUM_NM() {
		return LN_MINIMUM_NM;
	}
	public void setLN_MINIMUM_NM(String lN_MINIMUM_NM) {
		LN_MINIMUM_NM = lN_MINIMUM_NM;
	}
	public String getLN_MAXIMUM_NM() {
		return LN_MAXIMUM_NM;
	}
	public void setLN_MAXIMUM_NM(String lN_MAXIMUM_NM) {
		LN_MAXIMUM_NM = lN_MAXIMUM_NM;
	}
	public String getLN_TAR_CONTENT() {
		return LN_TAR_CONTENT;
	}
	public void setLN_TAR_CONTENT(String lN_TAR_CONTENT) {
		LN_TAR_CONTENT = lN_TAR_CONTENT;
	}
	public String getLN_RATE_LOW() {
		return LN_RATE_LOW;
	}
	public void setLN_RATE_LOW(String lN_RATE_LOW) {
		LN_RATE_LOW = lN_RATE_LOW;
	}
	public String getLN_RATE_HIGH() {
		return LN_RATE_HIGH;
	}
	public void setLN_RATE_HIGH(String lN_RATE_HIGH) {
		LN_RATE_HIGH = lN_RATE_HIGH;
	}
	public String getLN_PR_INFO() {
		return LN_PR_INFO;
	}
	public void setLN_PR_INFO(String lN_PR_INFO) {
		LN_PR_INFO = lN_PR_INFO;
	}
	public String getLN_RATE_INFO() {
		return LN_RATE_INFO;
	}
	public void setLN_RATE_INFO(String lN_RATE_INFO) {
		LN_RATE_INFO = lN_RATE_INFO;
	}
	public String getLN_AMOUNT_LIMIT() {
		return LN_AMOUNT_LIMIT;
	}
	public void setLN_AMOUNT_LIMIT(String lN_AMOUNT_LIMIT) {
		LN_AMOUNT_LIMIT = lN_AMOUNT_LIMIT;
	}
	public String getLN_REPAY_WAY() {
		return LN_REPAY_WAY;
	}
	public void setLN_REPAY_WAY(String lN_REPAY_WAY) {
		LN_REPAY_WAY = lN_REPAY_WAY;
	}
	public String getREPAYMENT_TYPE() {
		return REPAYMENT_TYPE;
	}
	public void setREPAYMENT_TYPE(String rEPAYMENT_TYPE) {
		REPAYMENT_TYPE = rEPAYMENT_TYPE;
	}
	public String getLN_LM_CONTENT() {
		return LN_LM_CONTENT;
	}
	public void setLN_LM_CONTENT(String lN_LM_CONTENT) {
		LN_LM_CONTENT = lN_LM_CONTENT;
	}
	public String getLN_ETC() {
		return LN_ETC;
	}
	public void setLN_ETC(String lN_ETC) {
		LN_ETC = lN_ETC;
	}
	public String getLN_DOCUMENT() {
		return LN_DOCUMENT;
	}
	public void setLN_DOCUMENT(String lN_DOCUMENT) {
		LN_DOCUMENT = lN_DOCUMENT;
	}
	public String getLN_IS_YN() {
		return LN_IS_YN;
	}
	public void setLN_IS_YN(String lN_IS_YN) {
		LN_IS_YN = lN_IS_YN;
	}
	public String getUPD_YN() {
		return UPD_YN;
	}
	public void setUPD_YN(String uPD_YN) {
		UPD_YN = uPD_YN;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getINS_DATE() {
		return INS_DATE;
	}
	public void setINS_DATE(String iNS_DATE) {
		INS_DATE = iNS_DATE;
	}
	public String getLN_LOCAL() {
		return LN_LOCAL;
	}
	public void setLN_LOCAL(String lN_LOCAL) {
		LN_LOCAL = lN_LOCAL;
	}
	public String getDEL_YN() {
		return DEL_YN;
	}
	public void setDEL_YN(String dEL_YN) {
		DEL_YN = dEL_YN;
	}
	public String getBASE_RATE() {
		return BASE_RATE;
	}
	public void setBASE_RATE(String bASE_RATE) {
		BASE_RATE = bASE_RATE;
	}
	public String getLANDING_URL() {
		return LANDING_URL;
	}
	public void setLANDING_URL(String lANDING_URL) {
		LANDING_URL = lANDING_URL;
	}
	public String getLN_TARGET() {
		return LN_TARGET;
	}
	public void setLN_TARGET(String lN_TARGET) {
		LN_TARGET = lN_TARGET;
	}
	
}
