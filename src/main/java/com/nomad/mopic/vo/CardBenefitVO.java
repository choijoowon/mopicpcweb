package com.nomad.mopic.vo;

public class CardBenefitVO {
	public String CARD_CD; 					// 카드코드
	public int BENEFIT_CD; 					// 혜택키
	public String BENEFIT_SEQ; 				// 혜택순서
	public String BENEFIT_NAME; 			// 혜택이름
	public String BENEFIT_EXPLAIN_IMG_U; 	// 혜택설명 이미지 상단
	public String BENEFIT_DETAIL; 			// 혜택설명
	public String BENEFIT_EXPLAIN_IMG_D; 	// 혜택설명 이미지 하단
	public String BEST_BENEFIT; 			// 주요혜택
	public String CATEGORY; 				// 주요혜택(대분류)
	public String SUB_CATEGORY; 			// 주요혜택(중분류)
	
	
	// getters and setters
	public String getCARD_CD() {
		return CARD_CD;
	}
	public void setCARD_CD(String cARD_CD) {
		CARD_CD = cARD_CD;
	}
	public int getBENEFIT_CD() {
		return BENEFIT_CD;
	}
	public void setBENEFIT_CD(int bENEFIT_CD) {
		BENEFIT_CD = bENEFIT_CD;
	}
	public String getBENEFIT_SEQ() {
		return BENEFIT_SEQ;
	}
	public void setBENEFIT_SEQ(String bENEFIT_SEQ) {
		BENEFIT_SEQ = bENEFIT_SEQ;
	}
	public String getBENEFIT_NAME() {
		return BENEFIT_NAME;
	}
	public void setBENEFIT_NAME(String bENEFIT_NAME) {
		BENEFIT_NAME = bENEFIT_NAME;
	}
	public String getBENEFIT_EXPLAIN_IMG_U() {
		return BENEFIT_EXPLAIN_IMG_U;
	}
	public void setBENEFIT_EXPLAIN_IMG_U(String bENEFIT_EXPLAIN_IMG_U) {
		BENEFIT_EXPLAIN_IMG_U = bENEFIT_EXPLAIN_IMG_U;
	}
	public String getBENEFIT_DETAIL() {
		return BENEFIT_DETAIL;
	}
	public void setBENEFIT_DETAIL(String bENEFIT_DETAIL) {
		BENEFIT_DETAIL = bENEFIT_DETAIL;
	}
	public String getBENEFIT_EXPLAIN_IMG_D() {
		return BENEFIT_EXPLAIN_IMG_D;
	}
	public void setBENEFIT_EXPLAIN_IMG_D(String bENEFIT_EXPLAIN_IMG_D) {
		BENEFIT_EXPLAIN_IMG_D = bENEFIT_EXPLAIN_IMG_D;
	}
	public String getBEST_BENEFIT() {
		return BEST_BENEFIT;
	}
	public void setBEST_BENEFIT(String bEST_BENEFIT) {
		BEST_BENEFIT = bEST_BENEFIT;
	}
	public String getCATEGORY() {
		return CATEGORY;
	}
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}
	public String getSUB_CATEGORY() {
		return SUB_CATEGORY;
	}
	public void setSUB_CATEGORY(String sUB_CATEGORY) {
		SUB_CATEGORY = sUB_CATEGORY;
	}
}
