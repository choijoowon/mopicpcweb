package com.nomad.mopic.vo;

public class ExchangeVO {
	public int rowCnt;					// merge용
	public String TR_BANK;				// 은행명
	public String TR_NATION;			// 국가명
	public String TR_BUY_CASH;			// 현찰살때
	public String TR_SELL_CASH;			// 현찰팔때
	public String TR_SEND_MONEY;		// 송금할때
	public String TR_RECEIVE_MONEY;		// 송금받을때
	public float TR_RATE;				// 매매기준율
	public String TR_DATE;				// 날짜
	public String TR_BUY_CHECK;			// 여행자수표살때
	public String TR_SELL_CHECK;		// 외화수표팔때
	public String TR_URL;				// 환율 URL
	
	// getters and setters
	public int getRowCnt() {
		return rowCnt;
	}
	public void setRowCnt(int rowCnt) {
		this.rowCnt = rowCnt;
	}
	public String getTR_BANK() {
		return TR_BANK;
	}
	public void setTR_BANK(String tR_BANK) {
		TR_BANK = tR_BANK;
	}
	public String getTR_NATION() {
		return TR_NATION;
	}
	public void setTR_NATION(String tR_NATION) {
		TR_NATION = tR_NATION;
	}
	public String getTR_BUY_CASH() {
		return TR_BUY_CASH;
	}
	public void setTR_BUY_CASH(String tR_BUY_CASH) {
		TR_BUY_CASH = tR_BUY_CASH;
	}
	public String getTR_SELL_CASH() {
		return TR_SELL_CASH;
	}
	public void setTR_SELL_CASH(String tR_SELL_CASH) {
		TR_SELL_CASH = tR_SELL_CASH;
	}
	public String getTR_SEND_MONEY() {
		return TR_SEND_MONEY;
	}
	public void setTR_SEND_MONEY(String tR_SEND_MONEY) {
		TR_SEND_MONEY = tR_SEND_MONEY;
	}
	public String getTR_RECEIVE_MONEY() {
		return TR_RECEIVE_MONEY;
	}
	public void setTR_RECEIVE_MONEY(String tR_RECEIVE_MONEY) {
		TR_RECEIVE_MONEY = tR_RECEIVE_MONEY;
	}
	public float getTR_RATE() {
		return TR_RATE;
	}
	public void setTR_RATE(float tR_RATE) {
		TR_RATE = tR_RATE;
	}
	public String getTR_DATE() {
		return TR_DATE;
	}
	public void setTR_DATE(String tR_DATE) {
		TR_DATE = tR_DATE;
	}
	public String getTR_SELL_CHECK() {
		return TR_SELL_CHECK;
	}
	public void setTR_SELL_CHECK(String tR_SELL_CHECK) {
		TR_SELL_CHECK = tR_SELL_CHECK;
	}
	public String getTR_BUY_CHECK() {
		return TR_BUY_CHECK;
	}
	public void setTR_BUY_CHECK(String tR_BUY_CHECK) {
		TR_BUY_CHECK = tR_BUY_CHECK;
	}
	public String getTR_URL() {
		return TR_URL;
	}
	public void setTR_URL(String tR_URL) {
		TR_URL = tR_URL;
	}
	@Override
	public String toString() {
		return "ExchangeVO [rowCnt=" + rowCnt + ", TR_BANK=" + TR_BANK + ", TR_NATION=" + TR_NATION + ", TR_BUY_CASH="
				+ TR_BUY_CASH + ", TR_SELL_CASH=" + TR_SELL_CASH + ", TR_SEND_MONEY=" + TR_SEND_MONEY
				+ ", TR_RECEIVE_MONEY=" + TR_RECEIVE_MONEY + ", TR_RATE=" + TR_RATE + ", TR_DATE=" + TR_DATE + "]";
	}
}
