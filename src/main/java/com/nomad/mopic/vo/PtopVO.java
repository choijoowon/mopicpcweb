package com.nomad.mopic.vo;

public class PtopVO {
	public int rowCnt;
	public String PP_COMPANY;			// P2P 업체명
	public String PP_NAME;				// 상품명
	public Double PP_RATE;				// 금리
	public int PP_LOAN;					// 모집금액
	public String PP_MATURITY;			// 상환일
	public String PP_GRADE;				// 신용등급
	public String PP_REPAYMENT;			// 상환방식
	public int PP_PRESENT;				// 투자현황(현재)
	public int PP_GOAL;					// 투자현황(목표)
	public String PL_NAME;				// 대출상품명
	public float PL_RATEST;				// 대출금리시작
	public float PL_RATEEN;				// 대출금리종료
	public int PL_LIMITST;				// 대출한도시작
	public int PL_LIMITEN;				// 대출한도종료
	public int PL_DATEST;				// 대출기간시작
	public int PL_DATEEN;				// 대출기간종료
	public String PL_REPAYMENT;			// 대출상환방식
	public String PL_QUALIFICATION;		// 대출신청자격
	public String PL_GRADE;				// 대출신청등급
	public String PL_DOCUMENT;			// 대출신청서류
	public String PL_COLLATERAL;		// 담보
	public String PP_DATE;				// 날짜
	public String PP_CODE;				// 날짜
	public String PP_URL;				// p2p url
	public String PP_STOCK;				// p2p url
	public String PP_SHARE;				// 
	public String PP_TYPE;				// 
	
	// getters and setters
	public int getRowCnt() {
		return rowCnt;
	}
	public void setRowCnt(int rowCnt) {
		this.rowCnt = rowCnt;
	}
	public String getPP_COMPANY() {
		return PP_COMPANY;
	}
	public void setPP_COMPANY(String pP_COMPANY) {
		PP_COMPANY = pP_COMPANY;
	}
	public String getPP_NAME() {
		return PP_NAME;
	}
	public void setPP_NAME(String pP_NAME) {
		PP_NAME = pP_NAME;
	}
	public Double getPP_RATE() {
		return PP_RATE;
	}
	public void setPP_RATE(Double pP_RATE) {
		PP_RATE = pP_RATE;
	}
	public int getPP_LOAN() {
		return PP_LOAN;
	}
	public void setPP_LOAN(int pP_LOAN) {
		PP_LOAN = pP_LOAN;
	}
	public String getPP_MATURITY() {
		return PP_MATURITY;
	}
	public void setPP_MATURITY(String pP_MATURITY) {
		PP_MATURITY = pP_MATURITY;
	}
	public String getPP_GRADE() {
		return PP_GRADE;
	}
	public void setPP_GRADE(String pP_GRADE) {
		PP_GRADE = pP_GRADE;
	}
	public String getPP_REPAYMENT() {
		return PP_REPAYMENT;
	}
	public void setPP_REPAYMENT(String pP_REPAYMENT) {
		PP_REPAYMENT = pP_REPAYMENT;
	}
	public int getPP_PRESENT() {
		return PP_PRESENT;
	}
	public void setPP_PRESENT(int pP_PRESENT) {
		PP_PRESENT = pP_PRESENT;
	}
	public int getPP_GOAL() {
		return PP_GOAL;
	}
	public void setPP_GOAL(int pP_GOAL) {
		PP_GOAL = pP_GOAL;
	}
	public String getPL_NAME() {
		return PL_NAME;
	}
	public void setPL_NAME(String pL_NAME) {
		PL_NAME = pL_NAME;
	}
	public float getPL_RATEST() {
		return PL_RATEST;
	}
	public void setPL_RATEST(float pL_RATEST) {
		PL_RATEST = pL_RATEST;
	}
	public float getPL_RATEEN() {
		return PL_RATEEN;
	}
	public void setPL_RATEEN(float pL_RATEEN) {
		PL_RATEEN = pL_RATEEN;
	}
	public int getPL_LIMITST() {
		return PL_LIMITST;
	}
	public void setPL_LIMITST(int pL_LIMITST) {
		PL_LIMITST = pL_LIMITST;
	}
	public int getPL_LIMITEN() {
		return PL_LIMITEN;
	}
	public void setPL_LIMITEN(int pL_LIMITEN) {
		PL_LIMITEN = pL_LIMITEN;
	}
	public int getPL_DATEST() {
		return PL_DATEST;
	}
	public void setPL_DATEST(int pL_DATEST) {
		PL_DATEST = pL_DATEST;
	}
	public int getPL_DATEEN() {
		return PL_DATEEN;
	}
	public void setPL_DATEEN(int pL_DATEEN) {
		PL_DATEEN = pL_DATEEN;
	}
	public String getPL_REPAYMENT() {
		return PL_REPAYMENT;
	}
	public void setPL_REPAYMENT(String pL_REPAYMENT) {
		PL_REPAYMENT = pL_REPAYMENT;
	}
	public String getPL_QUALIFICATION() {
		return PL_QUALIFICATION;
	}
	public void setPL_QUALIFICATION(String pL_QUALIFICATION) {
		PL_QUALIFICATION = pL_QUALIFICATION;
	}
	public String getPL_GRADE() {
		return PL_GRADE;
	}
	public void setPL_GRADE(String pL_GRADE) {
		PL_GRADE = pL_GRADE;
	}
	public String getPL_DOCUMENT() {
		return PL_DOCUMENT;
	}
	public void setPL_DOCUMENT(String pL_DOCUMENT) {
		PL_DOCUMENT = pL_DOCUMENT;
	}
	public String getPL_COLLATERAL() {
		return PL_COLLATERAL;
	}
	public void setPL_COLLATERAL(String pL_COLLATERAL) {
		PL_COLLATERAL = pL_COLLATERAL;
	}
	public String getPP_DATE() {
		return PP_DATE;
	}
	public void setPP_DATE(String pP_DATE) {
		PP_DATE = pP_DATE;
	}
	public String getPP_CODE() {
		return PP_CODE;
	}
	public void setPP_CODE(String pP_CODE) {
		PP_CODE = pP_CODE;
	}
	public String getPP_URL() {
		return PP_URL;
	}
	public void setPP_URL(String pP_URL) {
		PP_URL = pP_URL;
	}
	public String getPP_STOCK() {
		return PP_STOCK;
	}
	public void setPP_STOCK(String pP_STOCK) {
		PP_STOCK = pP_STOCK;
	}
	public String getPP_SHARE() {
		return PP_SHARE;
	}
	public void setPP_SHARE(String pP_SHARE) {
		PP_SHARE = pP_SHARE;
	}
	public String getPP_TYPE() {
		return PP_TYPE;
	}
	public void setPP_TYPE(String pP_TYPE) {
		PP_TYPE = pP_TYPE;
	}
	@Override
	public String toString() {
		return "PtopVO [rowCnt=" + rowCnt + ", PP_COMPANY=" + PP_COMPANY + ", PP_NAME=" + PP_NAME + ", PP_RATE="
				+ PP_RATE + ", PP_LOAN=" + PP_LOAN + ", PP_MATURITY=" + PP_MATURITY + ", PP_GRADE=" + PP_GRADE
				+ ", PP_REPAYMENT=" + PP_REPAYMENT + ", PP_PRESENT=" + PP_PRESENT + ", PP_GOAL=" + PP_GOAL
				+ ", PL_NAME=" + PL_NAME + ", PL_RATEST=" + PL_RATEST + ", PL_RATEEN=" + PL_RATEEN + ", PL_LIMITST="
				+ PL_LIMITST + ", PL_LIMITEN=" + PL_LIMITEN + ", PL_DATEST=" + PL_DATEST + ", PL_DATEEN=" + PL_DATEEN
				+ ", PL_REPAYMENT=" + PL_REPAYMENT + ", PL_QUALIFICATION=" + PL_QUALIFICATION + ", PL_GRADE=" + PL_GRADE
				+ ", PL_DOCUMENT=" + PL_DOCUMENT + ", PL_COLLATERAL=" + PL_COLLATERAL + ", PP_DATE=" + PP_DATE + "]";
	}
}
