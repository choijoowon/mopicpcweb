package com.nomad.mopic.vo;

public class CardCompanyVO {
	public String CARD_CD; 						// 카드 코드
	public String COMPANY_NAME; 				// 카드사 이름
	
	public String getCARD_CD() {
		return CARD_CD;
	}
	public void setCARD_CD(String cARD_CD) {
		CARD_CD = cARD_CD;
	}
	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}
	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}
}
