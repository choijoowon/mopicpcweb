package com.nomad.mopic.vo;

public class FileVO extends GlobalVO {
	// 파일
	public int fileNo;					// 파일코드
	public int bltnCd;					// 게시코드
	public int fileSeq;					// 파일순서
	public long fileSize;				// 파일크기
	public String fileNm;				// 원본파일명
	public String saveFileNm;			// 저장파일명
	public String saveFilePath;			// 저장경로
	public String fileExt;				// 파일확장자

	
	public int getBltnCd() {
		return bltnCd;
	}
	public void setBltnCd(int bltnCd) {
		this.bltnCd = bltnCd;
	}
	public int getFileNo() {
		return fileNo;
	}
	public void setFileNo(int fileNo) {
		this.fileNo = fileNo;
	}
	public int getFileSeq() {
		return fileSeq;
	}
	public void setFileSeq(int fileSeq) {
		this.fileSeq = fileSeq;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getSaveFileNm() {
		return saveFileNm;
	}
	public void setSaveFileNm(String saveFileNm) {
		this.saveFileNm = saveFileNm;
	}
	public String getSaveFilePath() {
		return saveFilePath;
	}
	public void setSaveFilePath(String saveFilePath) {
		this.saveFilePath = saveFilePath;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileExt() {
		return fileExt;
	}
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
}
