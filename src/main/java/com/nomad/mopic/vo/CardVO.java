package com.nomad.mopic.vo;

import java.util.List;

public class CardVO {
	public int rowCnt;								// merge용
	public String CARD_CD; 							// 카드 코드
	public String CARD_NAME; 						// 카드 이름
	public String COMPANY_CD; 						// 카드사 코드
	public String UPDATE_DATE; 						// 최종 수정일
	public String CARD_TYPE; 						// 체크 or 신용 카드
	public String BRAND_TYPE; 						// 브랜드
	public String INTERNAL_ANNUAL_FEE; 				// 국내연회비
	public String FOREIGN_ANNUAL_FEE;	 			// 해외연회비
	public String ETC_ANNUAL_FEE_NAME; 				// 기타연회비명
	public String ETC_ANNUAL_FEE; 					// 기타연회비
	public String ANNUAL_FEE_DETAIL; 				// 연회비 설명
	public String CARD_IMG; 						// 카드이미지 경로
	public String REQUIRE_RESULT; 					// 요구실적
	public String HEADCOPY; 						// 카드설명 타이틀정도로 생각하시면 됩니다.
	public String DISCOUNT_LIMIT; 					// 통합할인한도
	public String GUIDE_SENTENCE; 					// 추가안내사항
	public String ETC_SENTENCE; 					// 기타안내
	public String APPROVED_NO_W; 					// 웹 이율/심의필
	public String APPLY_URL; 						// 카드신청 URL
	public String START_DATE;						// 노출시작일
	public String END_DATE;							// 노출종료일
	public List<CardBenefitVO> BENEFIT; 			// 혜택
	
	public String userId;			// 사용자 아이디
	

	public int getRowCnt() {
		return rowCnt;
	}
	public void setRowCnt(int rowCnt) {
		this.rowCnt = rowCnt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	// getters and setters
	public String getCARD_CD() {
		return CARD_CD;
	}
	public void setCARD_CD(String cARD_CD) {
		CARD_CD = cARD_CD;
	}
	public String getCARD_NAME() {
		return CARD_NAME;
	}
	public void setCARD_NAME(String cARD_NAME) {
		CARD_NAME = cARD_NAME;
	}
	public String getCOMPANY_CD() {
		return COMPANY_CD;
	}
	public void setCOMPANY_CD(String cOMPANY_CD) {
		COMPANY_CD = cOMPANY_CD;
	}
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}
	public String getCARD_TYPE() {
		return CARD_TYPE;
	}
	public void setCARD_TYPE(String cARD_TYPE) {
		CARD_TYPE = cARD_TYPE;
	}
	public String getBRAND_TYPE() {
		return BRAND_TYPE;
	}
	public void setBRAND_TYPE(String bRAND_TYPE) {
		BRAND_TYPE = bRAND_TYPE;
	}
	public String getINTERNAL_ANNUAL_FEE() {
		return INTERNAL_ANNUAL_FEE;
	}
	public void setINTERNAL_ANNUAL_FEE(String iNTERNAL_ANNUAL_FEE) {
		INTERNAL_ANNUAL_FEE = iNTERNAL_ANNUAL_FEE;
	}
	public String getFOREIGN_ANNUAL_FEE() {
		return FOREIGN_ANNUAL_FEE;
	}
	public void setFOREIGN_ANNUAL_FEE(String fOREIGN_ANNUAL_FEE) {
		FOREIGN_ANNUAL_FEE = fOREIGN_ANNUAL_FEE;
	}
	public String getETC_ANNUAL_FEE_NAME() {
		return ETC_ANNUAL_FEE_NAME;
	}
	public void setETC_ANNUAL_FEE_NAME(String eTC_ANNUAL_FEE_NAME) {
		ETC_ANNUAL_FEE_NAME = eTC_ANNUAL_FEE_NAME;
	}
	public String getETC_ANNUAL_FEE() {
		return ETC_ANNUAL_FEE;
	}
	public void setETC_ANNUAL_FEE(String eTC_ANNUAL_FEE) {
		ETC_ANNUAL_FEE = eTC_ANNUAL_FEE;
	}
	public String getANNUAL_FEE_DETAIL() {
		return ANNUAL_FEE_DETAIL;
	}
	public void setANNUAL_FEE_DETAIL(String aNNUAL_FEE_DETAIL) {
		ANNUAL_FEE_DETAIL = aNNUAL_FEE_DETAIL;
	}
	public String getCARD_IMG() {
		return CARD_IMG;
	}
	public void setCARD_IMG(String cARD_IMG) {
		CARD_IMG = cARD_IMG;
	}
	public String getREQUIRE_RESULT() {
		return REQUIRE_RESULT;
	}
	public void setREQUIRE_RESULT(String rEQUIRE_RESULT) {
		REQUIRE_RESULT = rEQUIRE_RESULT;
	}
	public String getHEADCOPY() {
		return HEADCOPY;
	}
	public void setHEADCOPY(String hEADCOPY) {
		HEADCOPY = hEADCOPY;
	}
	public String getDISCOUNT_LIMIT() {
		return DISCOUNT_LIMIT;
	}
	public void setDISCOUNT_LIMIT(String dISCOUNT_LIMIT) {
		DISCOUNT_LIMIT = dISCOUNT_LIMIT;
	}
	public String getGUIDE_SENTENCE() {
		return GUIDE_SENTENCE;
	}
	public void setGUIDE_SENTENCE(String gUIDE_SENTENCE) {
		GUIDE_SENTENCE = gUIDE_SENTENCE;
	}
	public String getETC_SENTENCE() {
		return ETC_SENTENCE;
	}
	public void setETC_SENTENCE(String eTC_SENTENCE) {
		ETC_SENTENCE = eTC_SENTENCE;
	}
	public String getAPPROVED_NO_W() {
		return APPROVED_NO_W;
	}
	public void setAPPROVED_NO_W(String aPPROVED_NO_W) {
		APPROVED_NO_W = aPPROVED_NO_W;
	}
	public String getAPPLY_URL() {
		return APPLY_URL;
	}
	public void setAPPLY_URL(String aPPLY_URL) {
		APPLY_URL = aPPLY_URL;
	}
	public String getSTART_DATE() {
		return START_DATE;
	}
	public void setSTART_DATE(String sTART_DATE) {
		START_DATE = sTART_DATE;
	}
	public String getEND_DATE() {
		return END_DATE;
	}
	public void setEND_DATE(String eND_DATE) {
		END_DATE = eND_DATE;
	}
	public List<CardBenefitVO> getBENEFIT() {
		return BENEFIT;
	}
	public void setBENEFIT(List<CardBenefitVO> bENEFIT) {
		BENEFIT = bENEFIT;
	}
}
