package com.nomad.mopic.loan.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.loan.mapper.LoanMapper;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.LoanVO;
import com.nomad.mopic.vo.SavingVO;

@Service
public class LoanService implements LoanMapper {
	@Resource(name = "loanMapper")
    private LoanMapper loanMapper;

	@Value("#{repayment}")
	private Properties repayment;
	
	public void insertLoanData(LoanVO vo) {
		loanMapper.insertLoanData(vo);
	}
	
	public Map<String, Object> scrapLoanList() {
		Map<String, Object> map	= new HashMap<String, Object>();
		String url		= StaticObject.Url.URL_TOUCHBANK_SERVER;
		String param	= "/api/Loan.do";
		String result	= "";
		int scrapCnt	= 0;
		int scrapLimit	= 5;
		
		try {
			for(int i = 0; i < scrapLimit; i++) {
				result = CommonUtil.urlConnect(url, param, "UTF-8", null);
				
				ObjectMapper om = new ObjectMapper();
				JsonNode node = om.readTree(result);
				ArrayList<LoanVO> loanList	= om.readValue(node.get("result").get("list").toString(), new TypeReference<ArrayList<LoanVO>>(){});
				
				if(loanList.size() > 0) {
					LoanVO loanVO	= new LoanVO();
					this.deleteLoanData(loanVO);
					
					for(LoanVO vo : loanList) {
						if(("Y").equals(vo.getDEL_YN())) {
							LoanVO tmpVO = new LoanVO();
							tmpVO.setLA_CODE(vo.getLA_CODE());
							tmpVO.setUSE_YN("N");
							this.updateLoanUseYn(tmpVO);
						} else {
							this.insertLoanData(vo);
						}
						
						scrapCnt++;
					}
					
					break;
				}
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, scrapCnt);
			map.put("data"			, "");
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		return map;
	}

	public void updateLoanUseYn(LoanVO loanVO) {
		loanMapper.updateLoanUseYn(loanVO);
	}

	public void selectLoanList(Map<Object, Object> inputMap, ModelMap modelMap, LoanVO loanVO) {
		ArrayList<Map<String, Object>> cntList	= new ArrayList<Map<String, Object>>();
		List<LoanVO> loanList					= null;
		
		int pageNum			= 0;
		int total_count		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("total_count")	, "1"));
		int rowCount		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("rowCount")		, "15"));
		pageNum 			= (int) Math.floor(total_count/rowCount) + 1;
		
		//total_counts : 여기서 현재까지의 total cnt 를 받아서 몇페이지인지 계산 후 search
		//sorting : 정렬
		int period			= 0;
		int amount			= 0;
		
		String searchOption	= StringUtils.defaultIfEmpty((String) inputMap.get("searchOption")	, "NORMAL");
		String amountStr	= StringUtils.defaultIfEmpty((String) inputMap.get("amount")		, "0");
		String searchKey	= StringUtils.defaultIfEmpty((String) inputMap.get("searchKey")		, "");
		String lnFnCode		= StringUtils.defaultIfEmpty((String) inputMap.get("filter")		, "A");
		String searchOrder	= StringUtils.defaultIfEmpty((String) inputMap.get("sorting")		, "RATE");
		String secureType	= StringUtils.defaultIfEmpty((String) inputMap.get("secureType")	, "");
		
		String lnLocal		= StringUtils.defaultIfEmpty((String) inputMap.get("LN_LOCAL")		, "");
		String lnRepayWay	= StringUtils.defaultIfEmpty((String) inputMap.get("LN_REPAY_WAY")	, "");
		//String ac_code		= StringUtils.defaultIfEmpty((String) inputMap.get("id")			, "");
		int standardTax		= 0;

		try {
			if(("COMPARE").equals(searchOption)) {
				loanVO.setSearchKey(searchKey);
				Map<String, Object> myProd	= this.selectMyLoanDetail(loanVO);
				
				loanVO.setLA_CODE(myProd.get("prodCode").toString());
				
				amount		= Integer.parseInt(myProd.get("amountStr").toString());
				standardTax	= Integer.parseInt(myProd.get("afterTax").toString());
			} else {
				amountStr	= CommonUtil.removeComma(amountStr);
				amount		= Integer.parseInt(amountStr);
			}
			
			loanVO.setSearchOption(searchOption);
			loanVO.setAmount(amount);
			loanVO.setLN_FN_CODE(lnFnCode);
			loanVO.setLN_ASU_TYPE(secureType);
			loanVO.setSearchOrder(searchOrder);
			loanVO.setPageNumber(pageNum);
			loanVO.setRowCount(rowCount);
			loanVO.setLN_LOCAL(lnLocal);
			loanVO.setLN_REPAY_WAY(lnRepayWay);
			//savingVO.setPeriod(period);
			
			cntList = this.selectLoanListCnt(loanVO);
			
			loanList = this.selectLoanList(loanVO);
			
			inputMap.put("best_yn","N");
			for(LoanVO data : loanList) {
				// set best
				if(total_count == 0) {
					String lnRateLow	= StringUtils.defaultIfEmpty(data.getLN_RATE_LOW()	, "0");
					String baseRate		= StringUtils.defaultIfEmpty(data.getBASE_RATE()	, "0");
					if(("1").equals(data.getRnum()) && Double.parseDouble(lnRateLow) <= Double.parseDouble(baseRate)) {
						inputMap.put("best_yn","Y");
					}
				}
				// repayment decode
				data.setREPAYMENT_TYPE(this.replaceRepayment(data.getLN_REPAY_WAY()));
			}
			
			CommonUtil.makeProductResponseMap("200", inputMap, loanList, cntList, modelMap);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public List<LoanVO> selectLoanList(LoanVO loanVO) {
		return loanMapper.selectLoanList(loanVO);
	}

	@Override
	public ArrayList<Map<String, Object>> selectLoanListCnt(LoanVO loanVO) {
		return loanMapper.selectLoanListCnt(loanVO);
	}

	@Override
	public Map<String, Object> selectMyLoanDetail(LoanVO loanVO) {
		return loanMapper.selectMyLoanDetail(loanVO);
	}

	public LoanVO selectLoanDetail(LoanVO loanVO) {
		LoanVO returnVO = new LoanVO();
		returnVO = loanMapper.selectLoanDetail(loanVO);
		
		returnVO.setREPAYMENT_TYPE(this.replaceRepayment(returnVO.getLN_REPAY_WAY()));
		returnVO.setLN_MINIMUM_NM(this.amountToKor(returnVO.getLN_MINIMUM()));
		returnVO.setLN_MAXIMUM_NM(this.amountToKor(returnVO.getLN_MAXIMUM()));
		
		return returnVO;
	}

	public List<CommonCodeVO> selectLoanBankList(Map<String, String> paramMap) {
		return loanMapper.selectLoanBankList(paramMap);
	}

	public List selectMyLoanList(Map<Object, Object> inputMap) {
		return loanMapper.selectMyLoanList(inputMap);
	}

	public void deleteLoanData(LoanVO loanVO) {
		loanMapper.deleteLoanData(loanVO);
	}

	public String replaceRepayment(String repayCode) {
		if(StringUtils.isEmpty(repayCode))
			return "";
		
		String returnString = "";
		
		if(repayCode.indexOf(",") > 0) {
			String[] repayArr = repayCode.split(",");
			
			for(int i = 0; i < repayArr.length; i++) {
				returnString += ("").equals(returnString) ? repayment.getProperty(repayArr[i].trim()) : ", " + repayment.getProperty(repayArr[i].trim());
			}
		} else {
			returnString = repayment.getProperty(repayCode.trim());
		}
		
		return returnString;
	}
	
	public String amountToKor(int amount) {
		if(amount == 0)
			return Integer.toString(amount);
		if(amount < 10000)
			return amount + "만원";
		
		int first	= (int) Math.floor(amount / 10000);
		int second	= (int) Math.floor(amount % 10000);
		
		String returnStr = first + "억";
		
		if(second > 0)
			returnStr += " " + second + "만원";
		else
			returnStr += "원";
		
		return returnStr;
	}
}
