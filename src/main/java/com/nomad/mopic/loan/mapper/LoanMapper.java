package com.nomad.mopic.loan.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.LoanVO;

@Repository(value = "loanMapper")
public interface LoanMapper {
	public void insertLoanData(LoanVO vo);
	public Map<String, Object> selectMyLoanDetail(LoanVO loanVO);
	public ArrayList<Map<String, Object>> selectLoanListCnt(LoanVO loanVO);
	public List<LoanVO> selectLoanList(LoanVO loanVO);
	public LoanVO selectLoanDetail(LoanVO loanVO);
	public List<CommonCodeVO> selectLoanBankList(Map<String, String> paramMap);
	public List selectMyLoanList(Map<Object, Object> inputMap);
	public void deleteLoanData(LoanVO loanVO);
	public void updateLoanUseYn(LoanVO loanVO);
}
