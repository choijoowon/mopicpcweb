package com.nomad.mopic.loan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.loan.service.LoanService;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.LoanVO;

@Controller
public class LoanController {
	private static final Logger logger = LoggerFactory.getLogger(LoanController.class);

	@Autowired
	public View jsonView;

	@Autowired
	LoanService loanService;

	@Autowired
	public CommonService commonService;
	
	
	
	/**
	 * 신용대출 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/loan/signature")
	public String loanSignature(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);
		
		return "loan-page/loan-page";
	}
	
	/**
	 * 담보대출 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/loan/secured")
	public String loanSecured(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);
		
		return "loan-page/loan-page";
	}

	/**
	 * 신용대출 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/loan/signature/detail", method = RequestMethod.GET)
	public String loanSignatureDetail(HttpServletRequest request, LoanVO vo, Locale locale, Model model) {
		LoanVO loanVO	= new LoanVO();
		String laCode	= request.getParameter("LA_CODE");
		
		try {
			vo.setLA_CODE(laCode);
			loanVO = loanService.selectLoanDetail(vo);
			
			model.addAttribute("data"		, loanVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "loan-page/signature-detail-pop";
	}

	/**
	 * 담보대출 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/loan/secured/detail", method = RequestMethod.GET)
	public String loanSecuredDetail(HttpServletRequest request, LoanVO vo, Locale locale, Model model) {
		LoanVO loanVO	= new LoanVO();
		String laCode	= request.getParameter("LA_CODE");
		
		try {
			vo.setLA_CODE(laCode);
			loanVO = loanService.selectLoanDetail(vo);
			
			model.addAttribute("data"		, loanVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "loan-page/secured-detail-pop";
	}

	/**
	 * 맞춤대출 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/loan/else/detail", method = RequestMethod.GET)
	public String loanElseDetail(HttpServletRequest request, LoanVO vo, Locale locale, Model model) {
		LoanVO loanVO	= new LoanVO();
		String laCode	= request.getParameter("LA_CODE");
		
		try {
			vo.setLA_CODE(laCode);
			loanVO = loanService.selectLoanDetail(vo);
			
			model.addAttribute("data"		, loanVO);
			model.addAttribute("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			model.addAttribute("resultCode"		, StaticObject.Code.CODE_FAILED);
			model.addAttribute("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return "loan-page/else-detail-pop";
	}

	
	
	
	
	/*****************************************************************************************************************************************/
	/* 이하 API 영역 
	/*****************************************************************************************************************************************/

	/**
	 * 대출 리스트 가져오기
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/scrapLoanList", method = RequestMethod.GET)
	public View scrapLoanList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();

		map = loanService.scrapLoanList();
		
		modelMap.put("result", map);
		
		return jsonView;
	}

	/**
	 * 신용대출 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/loan/selectSignatureList", method = RequestMethod.POST)
	public View selectCreditLoanList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		LoanVO loanVO					= new LoanVO();
		
		loanVO.setUserId(sessionMap.get("userId"));
		loanVO.setLN_CODE("B000101");
		
		loanService.selectLoanList(inputMap, modelMap, loanVO);
		
		return jsonView;
	}

	/**
	 * 담보대출 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/loan/selectSecuredList", method = RequestMethod.POST)
	public View selectSecuredLoanList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		LoanVO loanVO					= new LoanVO();
		
		loanVO.setUserId(sessionMap.get("userId"));
		loanVO.setLN_CODE("B000102");
		
		loanService.selectLoanList(inputMap, modelMap, loanVO);
		
		return jsonView;
	}

	/**
	 * 맞대출 리스트
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectElseLoanList", method = RequestMethod.POST)
	public View selectElseLoanList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		LoanVO loanVO					= new LoanVO();
		
		loanVO.setUserId(sessionMap.get("userId"));
		loanVO.setLN_CODE("B000103");
		
		loanService.selectLoanList(inputMap, modelMap, loanVO);
		
		return jsonView;
	}

	/**
	 * 대출 상세
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectLoanDetail", method = RequestMethod.POST)
	public View selectLoanDetail(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		LoanVO loanVO					= new LoanVO();
		
		String laCode = StringUtils.defaultIfEmpty((String) inputMap.get("laCode"), "");

		try {
			if(StringUtils.isEmpty(laCode))
				throw new Exception(StaticObject.Message.DATA_SEARCH_FAILED);
			
			loanVO.setLA_CODE(laCode);
			loanVO = loanService.selectLoanDetail(loanVO);
			
			CommonUtil.makeProductResponseMap("200", inputMap, loanVO, null, modelMap);
		} catch(Exception e) {
			modelMap.addAttribute("resultMessage", e.getLocalizedMessage());
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		return jsonView;
	}

	/**
	 * 대출 금융기관목록(팝업)
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/loan/selectLoanBankList", method = RequestMethod.POST)
	public View selectLoanBankList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		List<CommonCodeVO> bankList		= null;
		Map<String, Object> map			= new HashMap<String, Object>();
		Map<String, String> paramMap	= new HashMap<String, String>();
		
		int totalCnt	= 0;
		String category	= StringUtils.defaultIfEmpty((String) inputMap.get("category"), "");
		
		try {
			paramMap.put("category", category);
			
			bankList	= loanService.selectLoanBankList(paramMap);
			
			totalCnt	= bankList != null ? bankList.size() : 0;

			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, bankList);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}

	/**
	 * 대출 상품목록(팝업)
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/loan/filterProductList", method = RequestMethod.POST)
	public View selectSavingProductList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  = CommonUtil.getSession(request);
		Map<String, Object> map			= new HashMap<String, Object>();
		LoanVO loanVO					= new LoanVO();
		List<LoanVO> productList		= null;
		int totalCnt					= 0;
		
		String lnCode			= StringUtils.defaultIfEmpty((String) inputMap.get("lnCode")		, "");
		String lnBankingCode	= StringUtils.defaultIfEmpty((String) inputMap.get("lnBankingCode")	, "");
		String secureType		= StringUtils.defaultIfEmpty((String) inputMap.get("secureType")	, "");
		
		try {
			if(!StringUtils.isEmpty(lnCode))
				loanVO.setLN_CODE(lnCode);
			if(!StringUtils.isEmpty(lnBankingCode))
				loanVO.setLN_BANKING(lnBankingCode);
			if(!StringUtils.isEmpty(secureType))
				loanVO.setLN_ASU_TYPE(secureType);
			
			loanVO.setPageNumber(1);
			loanVO.setRowCount(99999);
			loanVO.setUserId(sessionMap.get("userId"));
			
			productList = loanService.selectLoanList(loanVO);
			totalCnt = productList != null ? productList.size() : 0;

			map.put("data"			, productList);
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, totalCnt);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}
}
