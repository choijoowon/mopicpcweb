package com.nomad.mopic.mypage.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository(value = "mypageMapper")
public interface MyPageMapper {
	public List<Map<String, Object>> selectMyProduct(Map<String, Object> map);
	public List<Map<String, Object>> selectMyProductListCnt(String userId);
	public void deleteMyProductList(Map<String, String> paramMap);
	
}