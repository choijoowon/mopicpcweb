package com.nomad.mopic.mypage.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.mypage.mapper.MyPageMapper;

@Service
public class MyPageService implements MyPageMapper {
	@Resource(name = "mypageMapper")
    private MyPageMapper mypageMapper;

	public List<Map<String, Object>> selectMyProduct(Map<String, Object> map) {
		return mypageMapper.selectMyProduct(map);
	}

	public List<Map<String, Object>> selectMyProductListCnt(String userId) {
		return mypageMapper.selectMyProductListCnt(userId);
	}

	public Map<String, Object> selectMyProductListCnt(HttpServletRequest request) {
		Map<String, String> sessionMap 	= CommonUtil.getSession(request);
		Map<String, Object> cntMap 		= new HashMap<String, Object>();
		Map<String, Object> map 		= new HashMap<String, Object>();
		List<Map<String, Object>> list	= null;
		String userId					= StringUtils.defaultIfEmpty(sessionMap.get("userId"), "");
		int total1						= 0;
		int total2						= 0;
		
		try {
			list = this.selectMyProductListCnt(userId);
			
			for(Map<String, Object> listMap : list) {
				String key = String.valueOf(listMap.get("type"));
				String val = String.valueOf(listMap.get("cnt"));
				
				cntMap.put(key, val);
				
				if(key.indexOf("1") > -1)
					total1 += Integer.parseInt(String.valueOf(listMap.get("cnt")));
				else
					total2 += Integer.parseInt(String.valueOf(listMap.get("cnt")));
			}
			
			cntMap.put("TOTAL1", total1);
			cntMap.put("TOTAL2", total2);

			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data"			, cntMap);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		return map;
	}

	public void deleteMyProductList(Map<String, String> paramMap) {
		mypageMapper.deleteMyProductList(paramMap);
	}

	public String getServiceUrl(String type, String category) {
		if(StringUtils.isEmpty(type) || StringUtils.isEmpty(category))
			return "#";
		
		String serviceUrl	= "";
		String mainService	= type.toLowerCase();
		String subService	= "";
		
		if(("insure").equals(mainService))
			mainService = "ins";
		
		if(("A000101").equals(category))
			subService	= "deposit";
		else if(("A000102").equals(category))
			subService	= "saving";
		else if(("A000103").equals(category))
			subService	= "mmda";
		else if(("A000104").equals(category))
			subService	= "house";
		else if(("B000101").equals(category))
			subService	= "signature";
		else if(("B000102").equals(category))
			subService	= "secured";
		else if(("B000103").equals(category))
			subService	= "else";
		else if(("SHILSON").equals(category))
			subService	= "shilson";
		else if(("LIFE").equals(category))
			subService	= "life";
		else if(("ANNUITY").equals(category))
			subService	= "annuity";
		else if(("CAR").equals(category))
			subService	= "car";
		else if(("1").equals(category))
			subService	= "credit";
		else if(("2").equals(category))
			subService	= "check";
		else if(("L200003").equals(category))
			subService	= "invest";
		else if(("L200001").equals(category))
			subService	= "ploan";
		else
			subService	= "";
		
		serviceUrl = mainService + "/" + subService;
		
		return serviceUrl;
	}

}