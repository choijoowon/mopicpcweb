package com.nomad.mopic.mypage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.board.service.BoardService;
import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.mem.service.MemService;
import com.nomad.mopic.mypage.service.MyPageService;
import com.nomad.mopic.vo.BoardVO;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.MemberVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MyPageController {
	private static final Logger logger = LoggerFactory.getLogger(MyPageController.class);

	@Autowired
	public View jsonView;
	
	@Autowired
	public MyPageService mypageService;
	
	@Autowired
	public CommonCodeService commonCodeService;
	
	@Autowired
	public MemService memService;
	
	@Autowired
	public BoardService boardService;
	
	@Value("#{interest}")
	private Properties properties;
	

	/**
	 * 가입상품 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/joinProduct")
	public String deposit(Locale locale, Model model) {
		
		return "my-page/join-product";
	}

	/**
	 * 공지사항 리스트
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/information/noticeList")
	public String noticeList(HttpServletRequest request, Locale locale, Model model) {
		Map<String, Object> map	= new HashMap<String, Object>();
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		BoardVO boardVO			= new BoardVO();
		
		int bltnType = 1000001;		// 공지사항
		
		try {
			commonCodeVO.setCd(Integer.toString(bltnType));
			commonCodeVO.setDelYn("N");
			commonCodeVO.setDetailYn("N");
			
			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
			
			boardVO.setBltnType(bltnType);
			boardVO.setPageNumber(pageNum);
			map = boardService.selectBoardList(request, boardVO);
		} catch(Exception e) {
			
		}

		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		model.addAttribute("result", map);
		
		return "my-page/information-page";
	}

	/**
	 * faq 리스트
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/information/faqList")
	public String faqList(HttpServletRequest request, Locale locale, Model model) {
		Map<String, Object> map	= new HashMap<String, Object>();
		int pageNum					= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO	= new CommonCodeVO();
		CommonCodeVO faqCategoryVO	= new CommonCodeVO();
		List<CommonCodeVO> cList	= null;
		BoardVO boardVO				= new BoardVO();
		
		int bltnType = 1000002;		// faq
		
		try {
			// 게시판정보
			commonCodeVO.setCd(Integer.toString(bltnType));
			commonCodeVO.setDelYn("N");
			commonCodeVO.setDetailYn("N");
			
			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
			
			// 리스트 추출
			boardVO.setBltnType(bltnType);
			boardVO.setPageNumber(pageNum);
			
			map = boardService.selectBoardList(request, boardVO);
			
			// faq 카테고리
			faqCategoryVO.setGrpCd("1024");
			faqCategoryVO.setPageNumber(1);
			faqCategoryVO.setRowCount(9999999);
			faqCategoryVO.setDelYn("N");
			faqCategoryVO.setDetailYn("N");
			
			cList = commonCodeService.selectCmnCdList(faqCategoryVO);
		} catch(Exception e) {
			
		}

		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		model.addAttribute("category"	, cList);
		model.addAttribute("result"		, map);
		
		return "my-page/information-page";
	}

	/**
	 * 1:1문의 리스트
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/inquireList")
	public String inquireList(HttpServletRequest request, Locale locale, Model model) {
		Map<String, Object> map	= new HashMap<String, Object>();
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		BoardVO boardVO			= new BoardVO();
		
		int bltnType = 1000003;		// 1:1문의
		
		try {
			commonCodeVO.setCd(Integer.toString(bltnType));
			commonCodeVO.setDelYn("N");
			commonCodeVO.setDetailYn("N");
			
			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
			
			boardVO.setBltnType(bltnType);
			boardVO.setPageNumber(pageNum);
			map = boardService.selectBoardList(request, boardVO);
		} catch(Exception e) {
			
		}

		model.addAttribute("requestUri"	, request.getRequestURI());
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		model.addAttribute("result", map);
		
		return "my-page/inquireList-page";
	}

	/**
	 * 1:1문의 하기
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/inquire")
	public String inquire(HttpServletRequest request, Locale locale, Model model) {
		int pageNum	= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum"), "1"));
		
		String bltnCd				= StringUtils.defaultIfEmpty(request.getParameter("bltnCd"), "");
		CommonCodeVO commonCodeVO	= new CommonCodeVO();
		
		String bltnType = "1000003";		// faq
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);

		model.addAttribute("bltnCd"		, bltnCd);
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "my-page/inquire-page";
	}

	/**
	 * 1:1문의 상세
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/inquireView")
	public String inquireView(HttpServletRequest request, Locale locale, Model model) {
		int pageNum					= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum")	, "1"));
		String bltnCd				= StringUtils.defaultIfEmpty(request.getParameter("bltnCd")						, "");
		CommonCodeVO commonCodeVO	= new CommonCodeVO();
		
		String bltnType = "1000003";		// faq
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);

		model.addAttribute("bltnCd"		, bltnCd);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "my-page/inquireView-page";
	}

	/**
	 * 1:1문의 수정
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/inquireModify")
	public String inquireModify(HttpServletRequest request, Locale locale, Model model) {
		int pageNum					= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("pageNum")	, "1"));
		String bltnCd				= StringUtils.defaultIfEmpty(request.getParameter("bltnCd")						, "");
		CommonCodeVO commonCodeVO	= new CommonCodeVO();
		
		String bltnType = "1000003";		// faq
		
		commonCodeVO.setCd(bltnType);
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("N");
		
		commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);

		model.addAttribute("bltnCd"		, bltnCd);
		model.addAttribute("pageNum"	, pageNum);
		model.addAttribute("bltnType"	, bltnType);
		model.addAttribute("bltnName"	, commonCodeVO.getCdName());
		
		return "my-page/inquire-page";
	}

	/**
	 * 서비스정보 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/serviceInfo")
	public String personalInfo(HttpServletRequest request, Locale locale, Model model) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		Map<String, String> map			= new HashMap<String, String>();
		MemberVO memberVO				= new MemberVO();
		String interest					= "";
		HashMap<String, String> userInfoMap = null;
		
		try{
		
			JSONObject jsonParam = new JSONObject();
			jsonParam.put("access_token", sessionMap.get("access_token"));
	
			String result = CommonUtil.urlConnect(StaticObject.Url.URL_YELLO_PASS_DEV_PROFILE, "", "UTF-8", jsonParam.toString());
			
			//System.out.println("result::::"+result);
			
			ObjectMapper om	= new ObjectMapper();
			JsonNode node	= om.readTree(result);
			
			userInfoMap = om.readValue(result, new TypeReference<HashMap<String, Object>>(){});
		}catch(Exception e){
			e.printStackTrace();
		}
		
		map.put("user_id", sessionMap.get("userId"));
		memberVO = memService.selectUserInfo(map);
		//properties
		if(memberVO != null) {
			String tmpInterest	= StringUtils.defaultIfEmpty(memberVO.getProductInterest(), "");
			
			if(tmpInterest.indexOf(",") > 0) {
				String[] tmpInterestArr	= tmpInterest.split(",");
				
				for(int i = 0; i < tmpInterestArr.length; i++) {
					interest += ("").equals(interest) ? properties.getProperty(tmpInterestArr[i].trim()) : "," + properties.getProperty(tmpInterestArr[i].trim());
				}
			} else {
				interest = tmpInterest;
			}
		}
		
		model.addAttribute("userInfo", memberVO);
		model.addAttribute("interest", interest);
		model.addAttribute("yellopassInfo", userInfoMap);
		
		return "my-page/serviceInfo-page";
	}

	/**
	 * 가입상품 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/myProduct", method = RequestMethod.GET)
	public String myProduct(HttpServletRequest request, Locale locale, Model model) {
		String type		= StringUtils.defaultIfEmpty(request.getParameter("type")					, "SAVING");
		String category	= StringUtils.defaultIfEmpty(request.getParameter("category")				, "A000101");
		int gubun		= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("gubun")	, "30000001"));

		model.addAttribute("type"		, type);
		model.addAttribute("category"	, category);
		model.addAttribute("gubun"		, gubun);
		
		return "my-page/myProduct-page";
	}

	/**
	 * 관심상품 페이지
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mypage/myInterest", method = RequestMethod.GET)
	public String myInterest(HttpServletRequest request, Locale locale, Model model) {
		String type		= StringUtils.defaultIfEmpty(request.getParameter("type")					, "SAVING");
		String category	= StringUtils.defaultIfEmpty(request.getParameter("category")				, "A000101");
		int gubun		= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("gubun")	, "30000002"));

		model.addAttribute("type"		, type);
		model.addAttribute("category"	, category);
		model.addAttribute("gubun"		, gubun);
		
		return "my-page/myProduct-page";
	}

	
	
	
	/*****************************************************************************************************************************************/
	/** 이하 API 영역 
	/*****************************************************************************************************************************************/

	/**
	 * 내 가입상품 리스트<br/>
	 * type - <br/>
	 * 		SAVING : 예금, LOAN : 대출, INSURE : 보험, CARD : 카드<br/>
	 * category - <br/>
	 * 		SAVING : A000101:저축, A000102:적금, A000103:MMDA, A000104:주택청약<br/>
	 * 		LOAN : B000101:신용대출, B000102:담보대출, B000103:기타대출<br/>
	 * 		INSURE : SHILSON:실손, LIFE:정기, ANNUITY:연금/저축, CAR:자동차<br/>
	 * 		CARD : 1:신용, 2:체크<br/>
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/mypage/selectMyProduct", method = RequestMethod.POST)
	public View selectMyProduct(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap  	= CommonUtil.getSession(request);
		List<Map<String, Object>> resultMap	= null;
		Map<String, Object> map				= new HashMap<String, Object>();
		int totalCnt						= 0;
		
		String type							= StringUtils.defaultIfEmpty((String) inputMap.get("type")					, "SAVING");
		String category						= StringUtils.defaultIfEmpty((String) inputMap.get("category")				, "A000101");
		int gubun							= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("gubun"), "30000001"));
		
		try {
			map.put("userId"	, sessionMap.get("userId"));
			//map.put("userId"	, userId);
			map.put("type"		, type);
			map.put("category"	, category);
			map.put("gubun"		, gubun);
			map.put("serviceUrl", mypageService.getServiceUrl(type, category));
			
			resultMap	= mypageService.selectMyProduct(map);
			totalCnt	= resultMap != null ? resultMap.size() : 0;
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, resultMap);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}

	/**
	 * 내 상품 리스트카운트<br/>
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/mypage/selectMyProductListCnt", method = RequestMethod.POST)
	public View selectMyProductListCnt(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap 	= CommonUtil.getSession(request);
		Map<String, Object> cntMap 		= new HashMap<String, Object>();
		Map<String, Object> map 		= new HashMap<String, Object>();
		List<Map<String, Object>> list	= null;
		int total1						= 0;
		int total2						= 0;
		
		String userId	= StringUtils.defaultIfEmpty(sessionMap.get("userId"), "");
		
		try {
			list = mypageService.selectMyProductListCnt(userId);
			
			for(Map<String, Object> listMap : list) {
				String key = String.valueOf(listMap.get("type"));
				String val = String.valueOf(listMap.get("cnt"));
				
				cntMap.put(key, val);
				
				if(key.indexOf("1") > -1)
					total1 += Integer.parseInt(String.valueOf(listMap.get("cnt")));
				else
					total2 += Integer.parseInt(String.valueOf(listMap.get("cnt")));
			}
			
			cntMap.put("TOTAL1", total1);
			cntMap.put("TOTAL2", total2);

			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data"			, cntMap);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}

	/**
	 * 내 상품 삭제<br/>
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/mypage/deleteMyProductList", method = RequestMethod.POST)
	public View deleteMyProductList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, String> sessionMap 	= CommonUtil.getSession(request);
		Map<String, Object> map 		= new HashMap<String, Object>();
		Map<String, String> paramMap	= new HashMap<String, String>();
		
		String userId					= StringUtils.defaultIfEmpty(sessionMap.get("userId")			, "");
		String deleteList				= StringUtils.defaultIfEmpty((String) inputMap.get("idxList")	, "");
		String[] deleteArr				= deleteList.split(",");
		String idxList					= "";
		
		try {
			for(int i = 0; i < deleteArr.length; i++) {
				idxList += ("").equals(idxList) ? "'" + deleteArr[i].trim() + "'" : ",'" + deleteArr[i].trim() + "'";
			}
			
			paramMap.put("userId"	, userId);
			paramMap.put("idxList"	, idxList);
			
			mypageService.deleteMyProductList(paramMap);
					
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}
	
	
	
	
}
