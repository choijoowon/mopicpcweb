package com.nomad.mopic.ptop.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;

import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.ptop.mapper.PtopMapper;
import com.nomad.mopic.vo.PtopVO;

@Service
public class PtopService implements PtopMapper {
	@Resource(name = "ptopMapper")
    private PtopMapper ptopMapper;

	public void insertPtopData(PtopVO vo) {
		ptopMapper.insertPtopData(vo);
	}
	
	public Map<String, Object> scrapP2PList() {
		Map<String, Object> map	= new HashMap<String, Object>();
		String url		= StaticObject.Url.URL_TOUCHBANK_SERVER;
		String param	= "/api/P2P.do";
		String result	= "";
		int scrapCnt	= 0;
		int scrapLimit	= 5;
		
		try {
			for(int i = 0; i < scrapLimit; i++) {
				result = CommonUtil.urlConnect(url, param, "UTF-8", null);
				
				ObjectMapper om = new ObjectMapper();
				JsonNode node = om.readTree(result);
				ArrayList<PtopVO> ptopList	= om.readValue(node.get("result").get("list").toString(), new TypeReference<ArrayList<PtopVO>>(){});
				
				if(ptopList.size() > 0) {
					PtopVO ptopVO = new PtopVO();
					this.deleteP2PData(ptopVO);
					
					for(PtopVO vo : ptopList) {
						this.insertPtopData(vo);
						scrapCnt++;
					}
					
					break;
				}
				
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, scrapCnt);
			map.put("data"			, "");
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		return map;
	}
	
	@Override
	public List selectInvestList(Map searchQuery) {
		return ptopMapper.selectInvestList(searchQuery);
	}
	
	@Override
	public List selectInvestCountByFilter(Map searchQuery) {
		return ptopMapper.selectInvestCountByFilter(searchQuery);
	}
	
	@Override
	public List selectLoanList(Map searchQuery) {
		return ptopMapper.selectLoanList(searchQuery);
	}
	
	@Override
	public List selectLoanCountByFilter(Map searchQuery) {
		return ptopMapper.selectLoanCountByFilter(searchQuery);
	}

	public void deleteP2PData(PtopVO ptopVO) {
		ptopMapper.deleteP2PData(ptopVO);
	}
}
