package com.nomad.mopic.ptop.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.PtopVO;

@Repository(value = "ptopMapper")
public interface PtopMapper {
	void insertPtopData(PtopVO vo);
	public List selectInvestList(Map searchQuery);
	public List selectInvestCountByFilter(Map searchQuery);
	public List selectLoanList(Map searchQuery);
	public List selectLoanCountByFilter(Map searchQuery);
	void deleteP2PData(PtopVO ptopVO);

}
