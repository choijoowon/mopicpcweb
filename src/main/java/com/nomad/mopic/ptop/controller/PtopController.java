package com.nomad.mopic.ptop.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.ptop.service.PtopService;
import com.nomad.mopic.vo.CommonCodeVO;
import com.nomad.mopic.vo.LoanVO;
import com.nomad.mopic.vo.PtopVO;

@Controller
public class PtopController {
	private static final Logger logger = LoggerFactory.getLogger(PtopController.class);

	@Autowired
	public View jsonView;

	@Autowired
	PtopService ptopService;

	@Autowired
	public CommonCodeService commonCodeService;
	
	@Autowired
	public CommonService commonService;
	
	/**
	 * p2p 투자하기 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/p2p/invest")
	public String invest(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{
		
		commonService.setReactSearchQuery(request, model);
		
		return "p2p-page/p2p-page";
	}
	
	
	
	/**
	 * p2p 대출받기 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/p2p/ploan")
	public String loan(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{
		
		commonService.setReactSearchQuery(request, model);
		
		return "p2p-page/p2p-page";
	}
	
	
	/**
	 * P2P 투자하기 리스트 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/p2p/selectInvestList")
	public View selectInvestList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		try {
			
			try{
				commonCodeVO.setGrpCd("5100"); //실손보험 sorting기준 , 설정파일등에 추가 필요
				commonCodeVO.setCd(sorting==0?"5100001":Integer.toString(sorting));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("Y");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("orderby", commonCodeVO.getRef1());
				
			}catch(Exception e){
				logger.error("[/mopic/api/p2p/selectInvestList]" ,e);
				inputMap.put("orderby", "");
			}
			
			//System.out.println("commonCodeVO.getRef1():::"+commonCodeVO.getRef1());
			
//			commonCodeVO.setGrpCd(1102); //선택담보(종합/상해/질병)
//			commonCodeVO.setCd(dambo1);
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("dambo1_text", commonCodeVO.getCdName());
			
//			map.put("resultCode", "200");
//			map.put("productData", insService.selectShilsonList(inputMap));
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)

			CommonUtil.makeProductResponseMap("200", inputMap, ptopService.selectInvestList(inputMap), ptopService.selectInvestCountByFilter(inputMap), modelMap);
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/ins/getShilsonList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}
	
	
	/**
	 * P2P 대출받기 리스트 조회
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/p2p/selectPloanList")
	public View selectLoanList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		//System.out.println(inputMap.toString());
		
		int sorting				= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "0"));
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		try {
			
			try{
				commonCodeVO.setGrpCd("5200"); //실손보험 sorting기준 , 설정파일등에 추가 필요
				commonCodeVO.setCd(sorting==0?"5200001":Integer.toString(sorting));
				commonCodeVO.setDelYn("N");
				commonCodeVO.setDetailYn("Y");
				commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
				inputMap.put("orderby", commonCodeVO.getRef1());
				
			}catch(Exception e){
				logger.error("[/mopic/api/p2p/selectPloanList]" ,e);
				inputMap.put("orderby", "pl_ratest asc");
			}
			
			//System.out.println("commonCodeVO.getRef1():::"+commonCodeVO.getRef1());
			
//			commonCodeVO.setGrpCd(1102); //선택담보(종합/상해/질병)
//			commonCodeVO.setCd(dambo1);
//			commonCodeVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
//			inputMap.put("dambo1_text", commonCodeVO.getCdName());
			
//			map.put("resultCode", "200");
//			map.put("productData", insService.selectShilsonList(inputMap));
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
			
			List rList = ptopService.selectLoanList(inputMap);
			
			List filterStatList = new ArrayList();
			HashMap filterStat = new HashMap();
			filterStat.put("filter_cnt", rList.size()+"");
			filterStat.put("filter_name", "전체");
			filterStat.put("filter_cd", "99999999");
			filterStatList.add(filterStat);

			CommonUtil.makeProductResponseMap("200", inputMap, rList, filterStatList, modelMap);
			
		} catch(Exception e) {
			
//			map.put("resultCode", "500");
			logger.error("[/mopic/api/p2p/selectPloanList]" ,e);
			CommonUtil.makeProductResponseMap("500", inputMap, null, null, modelMap);
		}
		
		//modelMap.put("resultState"	, map);
		
		return jsonView;
	}

	
	/*****************************************************************************************************************************************/
	/* 이하 API 영역 
	/*****************************************************************************************************************************************/

	/**
	 * P2P 리스트 가져오기
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/scrapP2PList", method = RequestMethod.GET)
	public View scrapP2PList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();
		
		map = ptopService.scrapP2PList();
		
		modelMap.put("result", map);
		
		return jsonView;
	}

}
