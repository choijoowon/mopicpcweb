package com.nomad.mopic.log.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.nomad.mopic.log.mapper.LogMapper;

@Service
public class LogService implements LogMapper {
	@Resource(name = "logMapper")
    private LogMapper logMapper;

	public void insertVisitLog(Map<String, String> map) {
		logMapper.insertVisitLog(map);
	}

}
