package com.nomad.mopic.log.mapper;

import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository(value = "logMapper")
public interface LogMapper {
	void insertVisitLog(Map<String, String> map);

}
