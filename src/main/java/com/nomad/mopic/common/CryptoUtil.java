package com.nomad.mopic.common;

import java.security.spec.AlgorithmParameterSpec;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

public class CryptoUtil {
	private static final Logger logger = LoggerFactory.getLogger(CryptoUtil.class);
	//jre패치가 가능하다면
	//private final static String KEY	= "c261b87a549155cce0d615584a2fd3d5a9e6f7e6b87acd82f59b5187ded068d2"; //key (테스트용)
	//private final static String IV	= "473843dcfde7231f0e0247d2e4a44710"; //Initial Vector (테스트용)
	
	//불가능하면
	//private final static String KEY	= "c261b87a549155cc"; //key (테스트용)
	//private final static String IV	= "473843dcfde7231f"; //Initial Vector (테스트용)
	
	@Value("#{crypto['key64']}")
	private String KEY;

	@Value("#{crypto['iv64']}")
	private String IV;

	/*
	property 전체 사용시
	@Value("#{crypto}")
	private Properties properties;
	선언 후 함수 내에서
	properties.get("keyname")
	과 같이 사용하면 됩니다.
	 */
	
	//암호화
	public String enc(String message){
		String ENCODING = "UTF-8";
		String cryptoStr = "";

		//byte[] byteKey = strToHex(KEY);
		//byte[] byteIV = strToHex(IV);
		
		try {
			SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("ASCII"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec initVector = new IvParameterSpec(IV.getBytes("ASCII"));

			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, initVector);

			byte[] encrypted = cipher.doFinal(message.getBytes(ENCODING));
			cryptoStr = new String(hexToStr(encrypted));
		} catch(Exception ex) {
			//System.out.println(ex.toString());
			//ex.printStackTrace();
		}

		return cryptoStr;
	}
	 
	 //복호화
	public String dec(String message){
		try {
			SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("ASCII"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			AlgorithmParameterSpec initVector = new IvParameterSpec(IV.getBytes("ASCII"));

			cipher.init(Cipher.DECRYPT_MODE, skeySpec, initVector);

			byte[] original = cipher.doFinal(strToHex(message));

			return new String(original);
		} catch(Exception ex) {
			//System.out.println(ex.toString());
		}

		return null;
	}

	public byte[] strToHex(String hexStr) {
		if(hexStr == null) return null;
		
		int length = hexStr.length();
		
		if(length % 2 == 1) return null;
		
		byte[] bytes = new byte[length/2];

		for(int i = 0; i < bytes.length; ++i) {
			bytes[i] = (byte)Integer.parseInt(hexStr.substring(i*2, i*2+2), 16);
		}

		return bytes;
	}

	public String hexToStr(byte bytes[]) {
		StringBuffer strBuff = new StringBuffer();

		for(int i = 0; i < bytes.length; ++i) {
			strBuff.append(Integer.toHexString(0x0100 + (bytes[i] & 0x00FF)).substring(1));
		}

		return strBuff.toString();
	}
}
