package com.nomad.mopic.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.View;

import com.nomad.mopic.common.service.CommonService;

@Controller
public class MyProduct {
	private static final Logger logger = LoggerFactory.getLogger(MyProduct.class);

	@Autowired
	public View jsonView;

	@Autowired
	CommonService commonService;
	
	
}
