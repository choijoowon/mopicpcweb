package com.nomad.mopic.common.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.board.controller.BoardController;
import com.nomad.mopic.board.service.BoardService;
import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.vo.BoardVO;
import com.nomad.mopic.vo.FileVO;

@Controller
public class CommonController {
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);

	@Autowired
	public CryptoUtil cryptoUtil;

	@Autowired
	public View jsonView;
	
	@Autowired
	public BoardService boardService;

	/**
	 * 파일 다운로드
	 * @param request
	 * @param locale
	 * @param model
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "/common/download", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response, Locale locale, Model model) throws IOException {
		ServletOutputStream servletOutputStream	= null;
		FileInputStream fileInputStream			= null;
		FileVO fileVO							= new FileVO();
		
		int fileNo								= Integer.parseInt(StringUtils.defaultIfEmpty(request.getParameter("idx"), "0"));
		
		try {
			fileVO.setFileNo(fileNo);
			
			fileVO = boardService.selectBoardFile(fileVO);
			
			/*
			 * 권한체크
			 */
			File file = new File(fileVO.getSaveFilePath() + fileVO.getSaveFileNm());
			
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment;filename=\"" + fileVO.getFileNm() + "." + fileVO.getFileExt() + "\";");
			fileInputStream		= new FileInputStream(file);
			servletOutputStream	= response.getOutputStream();
			
			byte b [] = new byte[1024];
			int data = 0;
			
			while((data=(fileInputStream.read(b, 0, b.length))) != -1) {
				servletOutputStream.write(b, 0, data);
			}
			
			servletOutputStream.flush();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(servletOutputStream != null)
				servletOutputStream.close();
			if(fileInputStream != null)
				fileInputStream.close();
		}
	}
}
