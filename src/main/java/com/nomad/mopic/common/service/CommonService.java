package com.nomad.mopic.common.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.mapper.CommonMapper;
import com.nomad.mopic.main.mapper.MainMapper;
import com.nomad.mopic.mem.service.MemService;
import com.nomad.mopic.vo.CommonCodeVO;

@Service
public class CommonService implements CommonMapper {
	@Resource(name = "commonMapper")
	private CommonMapper commonMapper;
	
	@Resource(name = "mainMapper")
    private MainMapper mainMapper;

	@Value("#{menucode}")
	private Properties menucode;
	
	@Autowired
	CommonCodeService commonCodeService;
	
	@Autowired
	public MemService memService;
	
	public void setReactSearchQuery(HttpServletRequest request, ModelMap model) throws Exception{
		
		Map<String, Object> reqMap = CommonUtil.requestToMap(request);
		
		String isFirstLogin = (String)request.getSession().getAttribute("is_first");

//		if ( !reqMap.isEmpty() ){
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		CommonCodeVO resultVO = new CommonCodeVO();
		
		commonCodeVO.setGrpCd("2000"); //대 메뉴별 필요한 공통코드 조회
		commonCodeVO.setCd(menucode.getProperty(request.getRequestURI().substring(0,request.getRequestURI().lastIndexOf('/')+1)));
		commonCodeVO.setDelYn("N");
		commonCodeVO.setDetailYn("Y");
		resultVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
		
		if ( "Y".equals(isFirstLogin) ) { //최초 로그인일 경우
			
			commonCodeVO.setGrpCd("2000"); //대 메뉴별 필요한 공통코드 조회
			commonCodeVO.setCd("7000");
			commonCodeVO.setDelYn("N");
			commonCodeVO.setDetailYn("Y");
			
			if ( resultVO != null && !StringUtils.isEmpty(resultVO.getRef1()) ) {
				resultVO.setRef1(resultVO.getRef1()+","+commonCodeService.selectCmnCdDetail(commonCodeVO).getRef1());
			}else {
				resultVO = commonCodeService.selectCmnCdDetail(commonCodeVO);
			}
			
			model.put("isFirstLogin"	, "Y");
			request.getSession().removeAttribute("is_first");
		}
		
		if ( resultVO != null && !StringUtils.isEmpty(resultVO.getRef1()) ) {
			Map<Object, Object> inputMap = new HashMap<Object, Object>();
			inputMap.put("grpCdList", resultVO.getRef1());
			inputMap.put("detailYn", "Y");
			model.put("optCodeMap"	, commonCodeService.selectCmnCdGrpDetailList(inputMap).get("data"));
		}else {
			model.put("optCodeMap"	, new HashMap<String, Object>());
		}
		
		if ( "Y".equals(isFirstLogin) ) { //최초 로그인일 경우
			((Map)model.get("optCodeMap")).put("sidoOpts", memService.selectSigunguAddress(null) );
		}
		
		//환율예외처리 - 환전할 국가 정보 셋팅
		if ( model.containsKey("nationOpts"))
			((Map)model.get("optCodeMap")).put("nationOpts", model.get("nationOpts"));
		
		model.put("optCodeMap"	, CommonUtil.getReactJsonString(model.get("optCodeMap")));
		
		//카드 예외 케이스 - 헤택 string array로 변환
		if ( reqMap.containsKey("benefits")) {
			reqMap.put("benefitsToSplit", ((String)reqMap.get("benefits")).split("\\|"));
		}

		//카드 예외 케이스 - 모든 회사 셋팅
		if ( reqMap.containsKey("company") && reqMap.get("company").equals("all")) {
			String[]  allCompany = {"CT","EH","HD","HN","LT","SH","SS","WR","KB"};
			reqMap.put("companyJoinToString", allCompany);
		}
//		}
		
		CommonUtil.makeProductResponseMap("200", CommonUtil.getReactJsonString(reqMap), null, null, model);
	}
}
