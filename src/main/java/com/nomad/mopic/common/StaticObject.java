package com.nomad.mopic.common;

import java.util.HashMap;

public class StaticObject {
	public static final class Message {
		public static final String DATA_SEARCH_FAILED		= "데이터 조회에 실패하였습니다.";
		public static final String DATA_MODIFY_FAILED		= "데이터 수정에 실패하였습니다.";
		public static final String DATA_INSERT_FAILED		= "데이터 입력에 실패하였습니다.";
		
		public static final String FILE_UPLOAD_FAILED		= "파일 등록에 실패하였습니다.";
		
		public static final String MEMBER_LOGIN_FAILED		= "회원정보가 일치하지 않습니다.";
		public static final String MEMBER_NEED_LOGIN		= "로그인이 필요한 페이지 입니다.";
		
		public static final String BOARD_CATEGORY_IS_NULL	= "게시유형이 지정되지 않았습니다.";
		public static final String BOARD_TITLE_IS_NULL		= "제목이 입력되지 않았습니다.";
		public static final String BOARD_CTS_IS_NULL		= "내용이 입력되지 않았습니다.";
		public static final String BOARD_FILE_UPLOAD_ERROR	= "처리중 오류가 발생하였습니다.";
		
		public static final String PERMISSION_DENIED		= "권한이 없습니다.";
		
		public static final String AMOUNT_UNIT_5000			= "금액은 5천원 단위로 입력 가능합니다.";
	}
	
	public static final class Url {
		public static final String URL_YELLO_PASS_PROFILE		= "https://yellopass.yellofg.com/noco-itgr-auth/member/profile/token";
		public static final String URL_YELLO_PASS_DEV_PROFILE	= "https://yellopass.yellofg.com/noco-itgr-auth/member/profile/token";
		public static final String URL_IV_CARD_INFO_TYPE_G		= "http://www.cardbaro.com/card/join_site/mopic/getCardInfo.jsp";//?TYPE=G&COMPANY_CD=&FEE=&SORT=&MONTH_SUM=&SUB_CATEGORY1=&SUB_CATEGORY2=&SUB_CATEGORY3=";
		public static final String URL_IV_CARD_INFO_TYPE_C		= "http://www.cardbaro.com/card/join_site/mopic/getCardInfo.jsp";//?TYPE=C&CARD_CD=HD0001&MONTH_SUM=&FEE=&SORT=&SUB_CATEGORY1=E01&SUB_SUM1=10&SUB_CATEGORY2=&SUB_SUM2=&SUB_CATEGORY3=&SUB_SUM3=";
		public static final String URL_TOUCHBANK_SERVER			= "http://admin.touchbank.co.kr";
	}
	
	public static final class Code {
		public static final String CODE_SUCCESS				= "SUCCESS";
		public static final String CODE_FAILED				= "FAILED";
		
		public static final String CODE_SUCCESS_NUM				= "200";
		public static final String CODE_FAILED_NUM				= "500";
		
		public static final String CODE_MY_JOIN_PRODUCT		= "30000001";
		public static final String CODE_MY_INTEREST_PRODUCT	= "30000002";
		
		public static final String CODE_CARD_TYPE_CREDIT	= "1";
		public static final String CODE_CARD_TYPE_CHECK		= "2";
		
		public static HashMap codeMap;
	}
	
	public static final class Key {
		public static final String KEY_LOGIN			= "userId";
		public static final String KEY_QUALIFICATION	= "user_level";
	}
	
	public static final class Path {
		public static final String PATH_LOCAL_UPLOAD	= "user";
		public static final String PATH_SERVER_UPLOAD	= "/resources/user/";
	}

}
