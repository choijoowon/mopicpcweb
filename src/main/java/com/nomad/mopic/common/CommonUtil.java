package com.nomad.mopic.common;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.ModelMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.vo.CommonCodeVO;

public class CommonUtil {
	private static final Logger logger = LoggerFactory.getLogger(CommonUtil.class);
	
//	@Value("#{menucode['insshilson']}")
//	private static String KEY;
	
	/**
	 * 세션생성
	 * 
	 * @param request
	 * @param response
	 * @param map2
	 */
	public static void createSession(HttpServletRequest request, HttpServletResponse response, ModelMap map) {
		// 세션에 데이터 등록
		HttpSession session = request.getSession();
		
		session.setAttribute("userId"		, map.get("userId"));
		session.setAttribute("user_level"	, map.get("user_level"));
		session.setAttribute("access_token"	, map.get("access_token"));
		
		session.setMaxInactiveInterval(60*60*24);	// 30 mins
	}
	
	/**
	 * 세션삭제
	 * 
	 * @param request
	 * @param response
	 */
	public static void deleteSession(HttpServletRequest request, HttpServletResponse response) {
		// 쿠키삭제
		Cookie[] cookies = request.getCookies();
		
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}
		
		// 세션삭제
		HttpSession session = request.getSession(false);
		
		if(session != null) {
			session.invalidate();
		}
	}
	
	/**
	 * 세션정보 획득
	 * 
	 * @param request
	 * @return
	 */
	public static Map<String, String> getSession(HttpServletRequest request) {
		Map<String, String> map	= new HashMap<String, String>();
		HttpSession session		= request.getSession();
		
		map.put("userId", (String) session.getAttribute("userId"));
		//map.put("userId", "100");
		map.put("user_level", (String) session.getAttribute("user_level"));
		map.put("access_token", (String) session.getAttribute("access_token"));
		
		
		return map;
	}
	
	/**
	 * json String을 Map으로 변환 해 주는 함수
	 * 
	 * @param jsonData
	 * @return
	 */
	public static Map<String, Object> jsonToObj(String jsonData) {
		Map<String, Object> map	= new HashMap<String, Object>();
		
		if (!StringUtils.isEmpty(jsonData)) {
			JSONParser parser = new JSONParser();
			JSONObject jsonobject;
			
			try {
				jsonobject = (JSONObject) parser.parse(jsonData);
				
				for(Object key : jsonobject.keySet()) {
					Object jsongroup			= jsonobject.get(key);
					List<Object> jsonList		= new ArrayList<Object>();
					Map<String, Object> jsonMap	= new HashMap<String, Object>();
					
					if (jsongroup instanceof JSONObject) { // JSONObject일경우
						jsonMap = subJsonObject(key, (JSONObject) jsongroup);
						jsonList.add(new HashMap<String, Object> (jsonMap));
						map.put(key.toString(), new ArrayList<Object>(jsonList));
						
						jsonMap.clear();
						jsonList.clear();
					} else if (jsongroup instanceof ArrayList) { // JSONArray일경우
						map.put(key.toString(), new ArrayList<Object>(subJsonArray(key, (JSONArray) jsongroup)));
					} else {  // 일반 데이터 일경우
						map.put(key.toString(), jsongroup.toString());
					}
				}
			} catch (ParseException e) {
				//e.printStackTrace();
			}
		}
		
		return map;
	}

	/**
	 * jsonToObj 함수에서 json object를 처리 해 주는 함수
	 * @param key
	 * @param jsonobject
	 * @return
	 */
	private static Map<String, Object> subJsonObject(Object key, JSONObject jsonobject) {
		List<Object> jsonList	= new ArrayList<Object>();
		Map<String, Object> map	= new HashMap<String, Object>();

		for(Object nodename : jsonobject.keySet()) {
			if (jsonobject.get(nodename) instanceof ArrayList) { // JSONArray Type 일경우
				jsonList = subJsonArray(nodename, (JSONArray) jsonobject.get(nodename));
				map.put(nodename.toString(), new ArrayList<Object> (jsonList));
				
				jsonList.clear();
			} else {
				map.put(nodename.toString(), StringUtils.defaultIfEmpty(jsonobject.get(nodename).toString(), ""));
			}
		}

		return map;
	}

	/**
	 * jsonToObj 함수에서 json array를 처리 해 주는 함수
	 * @param key
	 * @param jsonobject
	 * @return
	 */
	private static List<Object> subJsonArray(Object key, JSONArray jsonarray) {
		List<Object> jsonList	= new ArrayList<Object>();
		Map<String, Object> map	= new HashMap<String, Object>();
		
		for(Object list : jsonarray) {
			map = subJsonObject(key, (JSONObject) list);
			jsonList.add(new HashMap<String, Object> (map));
			
			map.clear();
		}
		
		return jsonList;
	}

	/**
	 * URLConnection 으로 데이터 받아오는 함수
	 * 
	 * @param domain
	 * @param param
	 * @return
	 * @throws IOException
	 */
	public static String urlConnect(String domain, String param, String encoding, String output) throws IOException {
		URL url							= null;
		URLConnection urlconnection		= null;
		InputStream inputstream			= null;
		BufferedReader bufferedreader	= null;
		StringBuilder stringBuilder		= null;
		BufferedWriter bufferedwriter	= null;
		String result					= "";
		
		try {
			url				= new URL((new StringBuilder()).append(domain).append(param).toString());
			urlconnection	= url.openConnection();
			((HttpURLConnection)urlconnection).setRequestMethod("POST");            
			
			urlconnection.setDoOutput(true);		// 출력 스트림 사용 여부
			urlconnection.setDoInput(true);			// 입력 스트림 사용 여부
			urlconnection.setUseCaches(false);		// 캐시에 저장된 결과가 아닌 동적으로 그 순간에 생성된 결과를 읽도록, 
			urlconnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
			urlconnection.setConnectTimeout(10000);
			
			urlconnection.connect();
			
			if(output != null) {
				bufferedwriter  = new BufferedWriter(new OutputStreamWriter(urlconnection.getOutputStream(), "UTF-8")); 
				
				bufferedwriter.write(output);
				bufferedwriter.flush();
			}
			
			inputstream		= urlconnection.getInputStream();
			bufferedreader	= new BufferedReader(new InputStreamReader(inputstream, encoding));		//ISO-8859-1
			stringBuilder	= new StringBuilder();
			
			for(String x = bufferedreader.readLine(); x != null; x = bufferedreader.readLine()) {
				stringBuilder.append(x);
			}
			
			result = stringBuilder.toString();
			stringBuilder = null;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(inputstream != null)
				inputstream.close();
			if(bufferedreader != null)
				bufferedreader.close();
			if(bufferedwriter != null)
				bufferedwriter.close();
		}
		
		return result;
	}
	
	/**
	 * 프론트용 리턴 형식
	 * @param resultCode
	 * @param inputMap
	 * @param resultData
	 * @param filterStat
	 * @param responseMap
	 */
	public static void makeProductResponseMap(String resultCode, Object inputMap, Object resultData, Object filterStat, ModelMap responseMap){
		
		Map<String, Object> resultStateMap	= new HashMap<String, Object>();
		
		resultStateMap.put("resultCode"		, resultCode);		/* 200 : 정상 , 500 : 내부에러*/
		resultStateMap.put("resultMessage"	, responseMap.get("resultMessage"));	/* 에러메세지 */
		resultStateMap.put("productData"	, resultData);		/* 상품데이터 영역 */
		resultStateMap.put("filterStat"		, filterStat);		/* 필터별 카운트 영역 */
		
		responseMap.put("searchQuery"	, inputMap);		/* 클라이언트 검색 조건 영역 */
		responseMap.put("resultState"	, resultStateMap);
		
	}
	
	public static void setBestYN(Map inputMap, ModelMap modelMap, String targetValue, String originValue, String lowestIsBest){
		
		if ( "COMPARE".equals(inputMap.get("searchoption")) ) {	//if comparision
			
			Map<String, Object> resultState = (Map<String, Object>)modelMap.get("resultState");
			
			if ( "0".equals(inputMap.get("total_count")) ) {	//best check
				
				resultState.put("best_yn","N");
				
				if ( "Y".equals(lowestIsBest)){
					if (  Double.parseDouble(targetValue) <= Double.parseDouble(originValue) ) {
						resultState.put("best_yn","Y");
					}
				}else {
					if (  Double.parseDouble(targetValue) >= Double.parseDouble(originValue) ) {
						resultState.put("best_yn","Y");
					}
				}
			}
		}
	}
	
	/**
	 * 콤마 제거 함수
	 * @param amountStr
	 * @return
	 */
	public static String removeComma(String amountStr) {
		if(StringUtils.isEmpty(amountStr))
			return "";
		
		amountStr = amountStr.indexOf(",") > -1 ? amountStr.replaceAll(",", "") : amountStr;
		
		return amountStr;
	}
	
	public static Properties getProperties(String fileName) {
		Properties properties = new Properties();
		String location = "/WEB-INF/config/properties/";
		
		try {
			properties.load(new FileInputStream(location + fileName + ".properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return properties;
	}
	
	public static HashMap<String, Object> requestToMap(HttpServletRequest request)
	{
		HashMap<String, Object> returnMap = new HashMap<String, Object>();
		
		Enumeration enums = request.getParameterNames();
		
		while(enums.hasMoreElements()){
			String key = (String)enums.nextElement();
			
			returnMap.put(key, request.getParameter(key));
		}

		return returnMap;
	}
	
	public static String getReactJsonString(Object list){
		
		Gson gson = new GsonBuilder().create();
		
		//최상단 괄호({,[)는 제거(데이터 타입에 따라서 client에서 감싸주도록)
		return gson.toJson(list).substring(1,gson.toJson(list).length()-1);
		
//		JSONObject jsonParam = new JSONObject();
//        jsonParam.put("", list);
//        
//        //최상단 괄호({,[)는 제거(데이터 타입에 따라서 client에서 감싸주도록)
//        return jsonParam.toString().substring(5,jsonParam.toString().length()-2);
	}
}
