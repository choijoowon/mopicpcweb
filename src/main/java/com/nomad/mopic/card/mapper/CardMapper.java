package com.nomad.mopic.card.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.CardBenefitVO;
import com.nomad.mopic.vo.CardCompanyVO;
import com.nomad.mopic.vo.CardVO;

@Repository(value = "cardMapper")
public interface CardMapper {
	void insertCardData(CardVO vo);
	void insertBenefitData(CardBenefitVO bvo);
	void deleteBenefitData(CardVO cardVO);
	void insertCompanyData(CardCompanyVO vo);
	
	public List<java.util.HashMap> filterProductByCompany(Map searchQuery);
	
	//내 가입 카드목록 조회
	public List selectMyProductList(Map searchQuery);
	
	//카드별 기타 정보 및 즐겨찾기 등록 여부 조회
	public List selectCardDetailInfoWithInterest(Map searchQuery);
	//카드상세 정보(혜택포함)
	public List<java.util.HashMap> selectCardDetailInfo(CardVO vo);
	//카드 혜택 리스트
	public List<java.util.HashMap> selectCardBenefitList(Map searchQuery);
	void deleteCardData(CardVO cardVO);
	
	
}
