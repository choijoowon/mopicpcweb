package com.nomad.mopic.card.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.nomad.mopic.card.service.CardService;
import com.nomad.mopic.code.service.CommonCodeService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.common.service.CommonService;
import com.nomad.mopic.vo.CardBenefitVO;
import com.nomad.mopic.vo.CardVO;
import com.nomad.mopic.vo.CommonCodeVO;

@Controller
public class CardController {
	private static final Logger logger = LoggerFactory.getLogger(CardController.class);

	@Autowired
	public View jsonView;

	@Autowired
	CardService cardService;
	
	@Autowired
	public CommonCodeService commonCodeService;
	
	@Autowired
	public CommonService commonService;
	
	/**
	 * 신용카드 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card/credit", method = RequestMethod.GET)
	public String credit(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{		
		
		commonService.setReactSearchQuery(request, model);

		return "card-page/card-page";
	}
	
	
	
	
	/**
	 * 체크카드 페이지로 forward
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card/check", method = RequestMethod.GET)
	public String check(HttpServletRequest request, Locale locale, ModelMap model)  throws Exception{
		
		commonService.setReactSearchQuery(request, model);
		
		return "card-page/card-page";
	}
	
	
	
	
	
	/*****************************************************************************************************************************************/
	/* 이하 API 영역 
	/*****************************************************************************************************************************************/

	/**
	 * 카드 리스트 가져오기
	 * @param request
	 * @param response
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/scrapCardList", method = RequestMethod.GET)
	public String scrapCardList(HttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws Exception {
		Map<String, Object> map	= new HashMap<String, Object>();

		map = cardService.scrapCardList();
		
		modelMap.put("result", map);
		
		return "null";
	}
	
	
	
	/**
	 * 실손보험 가입상품등록 팝업에서 상품명으로 조회하는 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/card/filterProductByCompany")
	public View filterProductByCompany(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		List rlist = null;
		
		try {
			
//			if ( sessionMap.get("userId") == null )	//login check
//				throw new Exception("403");
//			
//			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			
			String menu_cd = (String)inputMap.get("menu_cd");
			
			if ( "4100".equals(menu_cd) )
				inputMap.put("card_type", StaticObject.Code.CODE_CARD_TYPE_CREDIT);
			else if ( "4200".equals(menu_cd) )
				inputMap.put("card_type", StaticObject.Code.CODE_CARD_TYPE_CHECK);
			
			map.put("data", cardService.filterProductByCompany(inputMap));
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/card/filterProductByCompany]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 내가 가입한 카드  조회 API
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/card/selectMyProductList")
	public View selectMyProductList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		Map<String, Object> map	= new HashMap<String, Object>();
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		List rlist = null;
		
		try {
			
			if ( sessionMap.get("userId") == null )	//login check
				throw new Exception("403");
			
			inputMap.put("user_id", sessionMap.get("userId"));//login id 셋팅
			
			String menu_cd = (String)inputMap.get("menu_cd");
			
			if ( "4100".equals(menu_cd) )
				inputMap.put("card_type", StaticObject.Code.CODE_CARD_TYPE_CREDIT);
			else if ( "4200".equals(menu_cd) )
				inputMap.put("card_type", StaticObject.Code.CODE_CARD_TYPE_CHECK);
			
			map.put("data", cardService.selectMyProductList(inputMap));
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			
		} catch(Exception e) {
			
			logger.error("[/mopic/api/card/selectMyProductList]" ,e);
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 인스밸리 카드정보 조회 
	 * 일반검색 : G, 비교검색 : C
	 * @param request
	 * @param response
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/card/selectCreditList", method = RequestMethod.POST)
	public View selectCreditList(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		String SORT = StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "B");	//sorting default : 혜택순
		inputMap.put("sort",SORT);
		
		return selectCardList(request, response, inputMap, modelMap, StaticObject.Code.CODE_CARD_TYPE_CREDIT);
	}
	
	/**
	 * 인스밸리 카드정보 조회 
	 * 일반검색 : G, 비교검색 : C
	 * @param request
	 * @param response
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/card/selectCheckList", method = RequestMethod.POST)
	public View selectCheckList(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		
		String SORT = StringUtils.defaultIfEmpty((String) inputMap.get("sorting")	, "B");	//sorting default : 혜택순
		inputMap.put("sort",SORT);
		
		return selectCardList(request, response, inputMap, modelMap, StaticObject.Code.CODE_CARD_TYPE_CHECK);
	}
	
	private View selectCardList(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap, String card_type){
		
		HashMap receiveMap = null;
		HashMap filterStat = new HashMap();
		
		String param = "";
		String result = "";
		String targetUrl = "";
		
		int total_count	= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("total_count")	, "0"));
		
		String TYPE = StringUtils.defaultIfEmpty((String) inputMap.get("type")	, "G");
		
		//System.out.println("inputMap:::::::::::::"+inputMap.toString());
		
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		try {

	        if ( "G".equals(TYPE)) {	//비교검색
	        	targetUrl = StaticObject.Url.URL_IV_CARD_INFO_TYPE_G;
	        }else {
	        	targetUrl = StaticObject.Url.URL_IV_CARD_INFO_TYPE_C;
	        }
	        
	        
	        inputMap.put("type",TYPE);
	        
	        
	        inputMap.put("card_type",card_type);	//신용카드,체크카드
	        
	        logger.debug("cardbaro search param : "+makeGetParamCardIV(inputMap));
	        result = CommonUtil.urlConnect(targetUrl, makeGetParamCardIV(inputMap), "ksc5601", null);
	        
	        /**
	         * 카드바로 예외 처리
	         */
	        if ( result == null || result.length() <= 3){
	        		        	
	        	filterStat.put("filter_cnt", "0");
				filterStat.put("filter_name", "전체");
				filterStat.put("filter_cd", "99999999");
				
				modelMap.put("resultMessage","cardbaro ERROR");
	        	CommonUtil.makeProductResponseMap(
	        			StaticObject.Code.CODE_FAILED_NUM
						, inputMap
						, new ArrayList()
						, filterStat
						, modelMap);
	        	
	        	return jsonView;
	        	
	        }
			
			ObjectMapper om	= new ObjectMapper();
			JsonNode node	= om.readTree(result);
			
			receiveMap = om.readValue(result, new TypeReference<HashMap<String, Object>>(){});
			receiveMap.put("resultCode", "200");
			
			int size = Integer.parseInt((String)receiveMap.get("TOTAL"));
			
			if ( size == 0){
	        	
	        	filterStat.put("filter_cnt", "0");
				filterStat.put("filter_name", "전체");
				filterStat.put("filter_cd", "99999999");
				
				modelMap.put("resultMessage","cardbaro ERROR");
	        	CommonUtil.makeProductResponseMap(
	        			StaticObject.Code.CODE_SUCCESS_NUM
						, inputMap
						, new ArrayList()
						, filterStat
						, modelMap);
	        	
	        	return jsonView;
	        	
	        }
			
			List<HashMap> subList = ((List)receiveMap.get("CARDLIST")).subList(total_count, total_count+15 > size ? size : total_count+15 );
			
			Map<String, Object> iMap = new HashMap<String, Object>();
			List cardCdList = new ArrayList();
			for(HashMap map : subList) {
				cardCdList.add((String)map.get("CARD_CD"));
			}			
			iMap.put("cardCdList",cardCdList);
			iMap.put("user_id", sessionMap.get("userId"));//login id 셋팅(관심상품 체크)
			
			Map<String, Map> rMap = new HashMap<String, Map>();
			for(Map map : cardService.selectCardDetailInfoWithInterest(iMap)) {
				rMap.put((String)map.get("card_cd"), map);
			}
			
			for(Map map : subList) {
				map.put("HEADCOPY", rMap.get((String)map.get("CARD_CD")).get("headcopy"));
				map.put("interest_yn", rMap.get((String)map.get("CARD_CD")).get("interest_yn"));
				map.put("detail_url", rMap.get((String)map.get("CARD_CD")).get("detail_url"));
				
				map.put("CARD_NAME", "["+(String)map.get("COMPANY_NAME")+"]"+(String)map.get("CARD_NAME"));
				
				
			}	
			
			List filterStatList = new ArrayList();
			filterStat.put("filter_cnt", receiveMap.get("TOTAL"));
			filterStat.put("filter_name", "전체");
			filterStat.put("filter_cd", "99999999");
			filterStatList.add(filterStat);
			
			CommonUtil.makeProductResponseMap(
					StaticObject.Code.CODE_SUCCESS_NUM
					, inputMap
					, subList	//paging
					, filterStatList
					, modelMap);
			/*
			System.out.println("SUB_SUM:::"+(Integer.parseInt(StringUtils.defaultIfEmpty((String)((HashMap)((HashMap)subList.get(0)).get("BENEFIT")).get("SUB_SUM1") , "0"))+
					Integer.parseInt(StringUtils.defaultIfEmpty((String)((HashMap)((HashMap)subList.get(0)).get("BENEFIT")).get("SUB_SUM2") , "0"))+
					Integer.parseInt(StringUtils.defaultIfEmpty((String)((HashMap)((HashMap)subList.get(0)).get("BENEFIT")).get("SUB_SUM3") , "0")))+"");
			*/
			CommonUtil.setBestYN(inputMap, modelMap, 
					"0",
					(Integer.parseInt(StringUtils.defaultIfEmpty((String)((HashMap)((HashMap)subList.get(0)).get("BENEFIT")).get("SUB_SUM1") , "0"))+
					Integer.parseInt(StringUtils.defaultIfEmpty((String)((HashMap)((HashMap)subList.get(0)).get("BENEFIT")).get("SUB_SUM2") , "0"))+
					Integer.parseInt(StringUtils.defaultIfEmpty((String)((HashMap)((HashMap)subList.get(0)).get("BENEFIT")).get("SUB_SUM3") , "0")))+""
					,
					"N");
			
		} catch(Exception e) {
			e.printStackTrace();
		}

		return jsonView;
	}
	
	private String makeGetParamCardIV(Map<Object, Object> inputMap){
		
		String TYPE = StringUtils.defaultIfEmpty((String) inputMap.get("type")	, "G");
		String COMPANY_CD = StringUtils.defaultIfEmpty((String) inputMap.get("company_cd")	, "");
		String FEE = StringUtils.defaultIfEmpty((String) inputMap.get("fee")	, "");
		String SORT = StringUtils.defaultIfEmpty((String) inputMap.get("sort")	, "");
		String MONTH_SUM = StringUtils.defaultIfEmpty((String) inputMap.get("month_sum")	, "");
		String SUB_CATEGORY1 = StringUtils.defaultIfEmpty((String) inputMap.get("sub_category1")	, "");
		String SUB_CATEGORY2 = StringUtils.defaultIfEmpty((String) inputMap.get("sub_category2")	, "");
		String SUB_CATEGORY3 = StringUtils.defaultIfEmpty((String) inputMap.get("sub_category3")	, "");
		
		String SUB_SUM1 = StringUtils.defaultIfEmpty((String) inputMap.get("sub_sum1")	, "");
		String SUB_SUM2 = StringUtils.defaultIfEmpty((String) inputMap.get("sub_sum2")	, "");
		String SUB_SUM3 = StringUtils.defaultIfEmpty((String) inputMap.get("sub_sum3")	, "");
		
		String CARD_CD = StringUtils.defaultIfEmpty((String) inputMap.get("card_cd")	, "");
		String CARD_TYPE = StringUtils.defaultIfEmpty((String) inputMap.get("card_type")	, "");	
		
		return "?TYPE="+TYPE
				+ "&COMPANY_CD="+COMPANY_CD
				+ "&FEE="+FEE
				+ "&SORT="+SORT
				+ "&MONTH_SUM="+MONTH_SUM
				+ "&SUB_CATEGORY1="+SUB_CATEGORY1
				+ "&SUB_CATEGORY2="+SUB_CATEGORY2
				+ "&SUB_CATEGORY3="+SUB_CATEGORY3
				+ "&SUB_SUM1="+SUB_SUM1
				+ "&SUB_SUM2="+SUB_SUM2
				+ "&SUB_SUM3="+SUB_SUM3
				+ "&CARD_CD="+CARD_CD
				+ "&CARD_TYPE="+CARD_TYPE;
				
		
	}
	
	/**
	 * 카드 상세보기
	 * @param request
	 * @param vo
	 * @param locale
	 * @param model
	 * @return
	 * @throws Exception
	 */
	private String cardDetail(HttpServletRequest request, CardVO vo, Locale locale, Model model) throws Exception{
		
		Map<String, String> sessionMap    = CommonUtil.getSession(request);
		
		CommonCodeVO commonCodeVO = new CommonCodeVO();
		
			
		vo.setUserId(sessionMap.get("userId"));
		
		Map rMap = cardService.selectCardDetailInfoWithBenefit(vo);
		
		if (StaticObject.Code.codeMap == null){
			
			StaticObject.Code.codeMap = new HashMap();
			
			commonCodeVO.setGrpCdList("'BRAND'");
			commonCodeVO.setDelYn("N");
			commonCodeVO.setDetailYn("N");
			
			for ( CommonCodeVO cVo : commonCodeService.selectCmnCdGrpDetailList(commonCodeVO)){
				StaticObject.Code.codeMap.put("BRAND_"+cVo.getCd(),cVo.getCdName());
			}
		}
		
		char[] brandArr = ((String)rMap.get("brand_type")).toCharArray();
		
		String brandList = new String();
		for ( char c : brandArr){
			brandList += ", "+StaticObject.Code.codeMap.get("BRAND_"+String.valueOf(c));
		}
		
		if ( brandList.length() > 2 )
			rMap.put("brand_type", brandList.substring(2));
		
		model.addAttribute("data",rMap);
		
		
		return "card-page/credit-detail-pop";
	}
	
	/**
	 * 신용카드 상세보기 팝업
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card/credit/detail")
	public String creditDetail(HttpServletRequest request, CardVO vo, Locale locale, Model model) {
		
		try {
			
			cardDetail(request, vo, locale, model);
		
		} catch(Exception e) {
			
			logger.error("[/card/credit/detail]" ,e);
		}
		
		return "card-page/credit-detail-pop";
	}
	
	/**
	 * 체크카드 상세보기 팝업
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/card/check/detail")
	public String checkDetail(HttpServletRequest request, CardVO vo, Locale locale, Model model) {
		
		try {
			
			cardDetail(request, vo, locale, model);
		
		} catch(Exception e) {
			
			logger.error("[/card/check/detail]" ,e);
		}
		
		return "card-page/check-detail-pop";
	}
}
