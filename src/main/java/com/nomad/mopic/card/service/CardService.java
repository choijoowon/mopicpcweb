package com.nomad.mopic.card.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.stereotype.Service;

import com.nomad.mopic.card.mapper.CardMapper;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.vo.CardBenefitVO;
import com.nomad.mopic.vo.CardCompanyVO;
import com.nomad.mopic.vo.CardVO;
import com.nomad.mopic.vo.CommonCodeVO;

@Service
public class CardService implements CardMapper {
	@Resource(name = "cardMapper")
    private CardMapper cardMapper;

	public void insertCardData(CardVO vo) {
		cardMapper.insertCardData(vo);
	}

	public void insertBenefitData(CardBenefitVO bvo) {
		cardMapper.insertBenefitData(bvo);
	}
	
	public Map<String, Object> scrapCardList() {
		Map<String, Object> map	= new HashMap<String, Object>();
		String url		= "http://www.cardbaro.com/";
		String param	= "/card/join_site/mopic/dailyData.jsp";
		String result	= "";
		int scrapCnt	= 0;
		int scrapLimit	= 5;
		
		try {
			for(int i = 0; i < scrapLimit; i++) {
				result = CommonUtil.urlConnect(url, param, "EUC-KR", null);
				
				ObjectMapper om = new ObjectMapper();
	
				JsonNode node = om.readTree(result);
				ArrayList<CardVO> cardList	= om.readValue(node.get("CARDLIST").toString(), new TypeReference<ArrayList<CardVO>>(){});
				
				if(cardList.size() > 0) {
					CardVO cardVO = new CardVO();
					this.deleteBenefitData(cardVO);
					this.deleteCardData(cardVO);
					
					for(CardVO vo : cardList) {
						this.insertCardData(vo);
						
						if(vo.getBENEFIT().size() > 0) {
							String cardCd = vo.getCARD_CD();
							cardVO = new CardVO();
							cardVO.setCARD_CD(cardCd);
							this.deleteBenefitData(cardVO);
							
							for(CardBenefitVO bvo : vo.getBENEFIT()) {
								bvo.setCARD_CD(cardCd);
								this.insertBenefitData(bvo);
							}
						}
						
						scrapCnt++;
					}
					
					break;
				}
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("totalCnt"		, scrapCnt);
			map.put("data"			, "");
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		return map;
	}

	public void deleteBenefitData(CardVO cardVO) {
		cardMapper.deleteBenefitData(cardVO);
	}

	public void insertCompanyData(CardCompanyVO vo) {
		cardMapper.insertCompanyData(vo);
	}
	
	public List filterProductByCompany(Map searchQuery) {
		
		List<java.util.HashMap> cardList = new ArrayList<java.util.HashMap>();
		
		HashMap cardMap = null;
		List<java.util.HashMap> cardBenefitList = null;
		HashMap cardBenefitMap = null;
		
		for(java.util.HashMap tempMap : cardMapper.filterProductByCompany(searchQuery)) {
			
			if ( cardMap != null && cardMap.containsValue(tempMap.get("card_cd"))) {
				
			}else {
				cardMap = new HashMap();
				cardBenefitList = new ArrayList<java.util.HashMap>();
				
				cardMap.put("card_cd", tempMap.get("card_cd"));
				cardMap.put("card_name", tempMap.get("card_name"));
				cardMap.put("company_cd", tempMap.get("company_cd"));
				
				cardMap.put("benefit", cardBenefitList);
				cardList.add(cardMap);
			
			}
			
			cardBenefitMap = new HashMap();
			
			cardBenefitMap.put("category", tempMap.get("category"));
			cardBenefitMap.put("sub_category", tempMap.get("sub_category"));
			cardBenefitMap.put("sub_category_nm", tempMap.get("sub_category_nm"));
			//cardBenefitMap.put("benefit_name", tempMap.get("benefit_name"));
			
			cardBenefitList.add(cardBenefitMap);
			
		}
		
		return cardList;
	}
	
	@Override
	public List selectMyProductList(Map searchQuery) {
		return cardMapper.selectMyProductList(searchQuery);
	}
	
	@Override
	public List<Map> selectCardDetailInfoWithInterest(Map searchQuery) {
		return cardMapper.selectCardDetailInfoWithInterest(searchQuery);
	}
	
	public Map selectCardDetailInfoWithBenefit(CardVO vo) {
		
		HashMap cardMap = null;
		List<java.util.HashMap> cardBenefitList = null;
		HashMap cardBenefitMap = null;
		
		for(java.util.HashMap tempMap : cardMapper.selectCardDetailInfo(vo)) {
			
			if ( cardMap != null && cardMap.containsValue(tempMap.get("card_cd"))) {
				
			}else {
				
				cardMap = new HashMap();
				cardBenefitList = new ArrayList<java.util.HashMap>();
				
				cardMap.put("card_cd", tempMap.get("card_cd"));
				cardMap.put("card_name", tempMap.get("card_name"));
				cardMap.put("require_result", tempMap.get("require_result"));
				cardMap.put("internal_annual_fee", tempMap.get("internal_annual_fee"));
				cardMap.put("foreign_annual_fee", tempMap.get("foreign_annual_fee"));
				cardMap.put("guide_sentence", tempMap.get("guide_sentence"));
				cardMap.put("brand_type", tempMap.get("brand_type"));
				cardMap.put("apply_url", tempMap.get("apply_url"));
				cardMap.put("approved_no_w", tempMap.get("approved_no_w"));
				cardMap.put("interest_yn", tempMap.get("interest_yn"));
				cardMap.put("headcopy", tempMap.get("headcopy"));

				cardMap.put("benefit", cardBenefitList);
			}
			
			cardBenefitMap = new HashMap();
			
			cardBenefitMap.put("best_benefit", tempMap.get("best_benefit"));
			cardBenefitMap.put("category", tempMap.get("category"));
			cardBenefitMap.put("sub_category", tempMap.get("sub_category"));
			cardBenefitMap.put("sub_category_nm", tempMap.get("sub_category_nm"));
			cardBenefitMap.put("benefit_name", tempMap.get("benefit_name"));
			cardBenefitMap.put("benefit_explain_img_u", tempMap.get("benefit_explain_img_u"));
			cardBenefitMap.put("benefit_detail", tempMap.get("benefit_detail"));
			cardBenefitMap.put("benefit_explain_img_d", tempMap.get("benefit_explain_img_d"));
			
			cardBenefitList.add(cardBenefitMap);
		}
		
		return cardMap;
	}
	
	@Override
	public List<HashMap> selectCardDetailInfo(CardVO vo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HashMap> selectCardBenefitList(Map searchQuery) {
		return cardMapper.selectCardBenefitList(searchQuery);
	}

	public void deleteCardData(CardVO cardVO) {
		cardMapper.deleteCardData(cardVO);
	}

}