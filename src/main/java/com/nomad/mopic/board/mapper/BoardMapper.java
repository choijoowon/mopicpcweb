package com.nomad.mopic.board.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.nomad.mopic.vo.BoardVO;
import com.nomad.mopic.vo.FileVO;

@Repository(value = "boardMapper")
public interface BoardMapper {
	public List<BoardVO> selectBoardList(BoardVO boardVO);

	public BoardVO selectBoardDetail(BoardVO boardVO);
	
	public List<BoardVO> selectBoardDetailList(BoardVO boardVO);

	public int insertBoardData(BoardVO boardVO);

	public int updateBoardData(BoardVO boardVO);

	public int insertBoardFile(FileVO fileVO);

	public FileVO selectBoardFile(FileVO fileVO);

	public List<FileVO> selectBoardFileList(FileVO fileVO);

	public int deleteBoardData(BoardVO boardVO);
}
