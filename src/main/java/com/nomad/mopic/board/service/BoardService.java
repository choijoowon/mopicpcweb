package com.nomad.mopic.board.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.nomad.mopic.board.mapper.BoardMapper;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.vo.BoardVO;
import com.nomad.mopic.vo.FileVO;

@Service
public class BoardService implements BoardMapper {
	@Resource(name = "boardMapper")
    private BoardMapper boardMapper;
	
	@Autowired
	CryptoUtil cryptoUtil;

	@Override
	public List<BoardVO> selectBoardList(BoardVO boardVO) {
		return boardMapper.selectBoardList(boardVO);
	}

	/**
	 * 비 ajax용 서비스
	 * @param request
	 * @return
	 */
	public Map<String, Object> selectBoardList(HttpServletRequest request, BoardVO boardVO) {
		Map<String, Object> map			= new HashMap<String, Object>();
		List<BoardVO> boardList			= null;
		FileVO fileVO					= new FileVO();
		List<FileVO> boardFileList		= null;
		
		int categoryGrpCd	= 0;
		int totalCnt		= 0;
		//int pageNum			= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("pageNum")		, "1"));
		int rowCount		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("rowCount")		, "10"));
		String mainMenu		= StringUtils.defaultIfEmpty((String) request.getParameter("mainMenu")						, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) request.getParameter("subMenu")						, "");
		
		int bltnCd			= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("bltnCd")		, "0"));
		//int bltnType		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("bltnType")		, "0"));
		int category		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("category")		, "0"));
		String delYn		= StringUtils.defaultIfEmpty((String) request.getParameter("delYn")							, "N");
		String searchKey	= StringUtils.defaultIfEmpty((String) request.getParameter("searchKey")						, "");
		String searchOption	= StringUtils.defaultIfEmpty((String) request.getParameter("searchOption")					, "");
		
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		if(!("super").equals(cryptoUtil.dec(sessionMap.get("user_level")))) {
			boardVO.setRegId(sessionMap.get("userId"));
		}
		
		try {
			boardVO.setBltnCd(bltnCd);
			//boardVO.setBltnType(bltnType);
			boardVO.setCategory(category);
			boardVO.setCategoryGrpCd(categoryGrpCd);
			boardVO.setDelYn(delYn);
			boardVO.setSearchKey(searchKey);
			boardVO.setSearchOption(searchOption);
			//boardVO.setPageNumber(pageNum);
			boardVO.setRowCount(rowCount);
			
			boardList	= this.selectBoardList(boardVO);
			totalCnt	= (Integer) (boardList.size() > 0 ? boardList.get(0).getTotCnt() : 0);
			
			for(BoardVO vo : boardList) {
				fileVO.setBltnCd(vo.getBltnCd());
				boardFileList = this.selectBoardFileList(fileVO);
				vo.setFileVO(boardFileList);
				System.out.println(boardFileList);
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, boardList);
			map.put("search"		, boardVO);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		return map;
	}

	@Override
	public BoardVO selectBoardDetail(BoardVO boardVO) {
		return boardMapper.selectBoardDetail(boardVO);
	}

	@Override
	public List<BoardVO> selectBoardDetailList(BoardVO boardVO) {
		return boardMapper.selectBoardDetailList(boardVO);
	}

	@Override
	public int insertBoardData(BoardVO boardVO) {
		return boardMapper.insertBoardData(boardVO);
	}

	@Override
	public int insertBoardFile(FileVO fileVO) {
		return boardMapper.insertBoardFile(fileVO);
	}

	@Override
	public int updateBoardData(BoardVO boardVO) {
		return boardMapper.updateBoardData(boardVO);
	}

	@Override
	public FileVO selectBoardFile(FileVO fileVO) {
		return boardMapper.selectBoardFile(fileVO);
	}
	
	@Override
	public List<FileVO> selectBoardFileList(FileVO fileVO) {
		return boardMapper.selectBoardFileList(fileVO);
	}
	
	public boolean fileUpload(MultipartHttpServletRequest request, MultipartFile[]  files, int bltnCd) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		FileVO fileVO		= new FileVO();
		LocalDateTime now	= LocalDateTime.now();
		//String uploadPath	= StaticObject.Path.PATH_LOCAL_UPLOAD;
		String uploadPath	= request.getSession().getServletContext().getRealPath(StaticObject.Path.PATH_SERVER_UPLOAD);
		
		try {
			if(bltnCd == 0) {
				throw new Exception();
			}
			
			File dir = new File(uploadPath);

			if (!dir.isDirectory()) dir.mkdirs();
			
			for(int i = 0 ; i < files.length; i++) {
				MultipartFile file	= files[i];
	            String fileName		= file.getOriginalFilename();
	            
	            String originName	= fileName.substring(0, fileName.lastIndexOf("."));
	            String saveName		= cryptoUtil.enc(now + originName);
	            String fileExt		= fileName.substring(fileName.lastIndexOf(".") + 1);
	            byte[] bytes		= file.getBytes();
	            
	            BufferedOutputStream buffStream = new BufferedOutputStream(new FileOutputStream(new File(uploadPath + saveName)));
	            
	            buffStream.write(bytes);
	            buffStream.close();
	            
	            fileVO.setRegId(sessionMap.get("userId"));
	            fileVO.setBltnCd(bltnCd);
	            fileVO.setFileSeq(i);
	            fileVO.setFileSize(file.getSize());
	            fileVO.setFileNm(originName);
	            fileVO.setSaveFileNm(saveName);
	            fileVO.setSaveFilePath(uploadPath);
	            fileVO.setFileExt(fileExt);
	            
	            /*
				System.out.println("======================================bltnCd : " + bltnCd);
				System.out.println("======================================file.getSize() : " + file.getSize());
				System.out.println("======================================originName : " + originName);
				System.out.println("======================================saveName : " + saveName);
				System.out.println("======================================uploadPath : " + uploadPath);
				System.out.println("======================================fileExt : " + fileExt);
	            */
	            this.insertBoardFile(fileVO);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}

	public int deleteBoardData(BoardVO boardVO) {
		return boardMapper.deleteBoardData(boardVO);
	}
}