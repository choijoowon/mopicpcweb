package com.nomad.mopic.board.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.View;

import com.nomad.mopic.board.service.BoardService;
import com.nomad.mopic.common.CommonUtil;
import com.nomad.mopic.common.CryptoUtil;
import com.nomad.mopic.common.StaticObject;
import com.nomad.mopic.vo.BoardVO;
import com.nomad.mopic.vo.FileVO;

@Controller
public class BoardController {
	private static final Logger logger = LoggerFactory.getLogger(BoardController.class);

	@Autowired
	public CryptoUtil cryptoUtil;

	@Autowired
	public View jsonView;
	
	@Autowired
	public BoardService boardService;
	
	/**
	 * 게시글 리스트
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectBoardList", method = RequestMethod.POST)
	public View selectBoardList(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();
		BoardVO boardVO			= new BoardVO();
		List<BoardVO> boardList	= null;
		 
		int categoryGrpCd	= 0;
		int totalCnt		= 0;
		int pageNum			= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("pageNum")		, "1"));
		int rowCount		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("rowCount")		, "9999999"));
		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")						, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")						, "");
		
		int bltnCd			= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bltnCd")		, "0"));
		int bltnType		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bltnType")		, "0"));
		int category		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("category")		, "0"));
		String delYn		= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")							, "N");
		String searchKey	= StringUtils.defaultIfEmpty((String) inputMap.get("searchKey")						, "");
		String searchOption	= StringUtils.defaultIfEmpty((String) inputMap.get("searchOption")					, "");
		
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		if(!("super").equals(cryptoUtil.dec(sessionMap.get("user_level")))) {
			boardVO.setRegId(sessionMap.get("userId"));
		}
		
		try {
			boardVO.setBltnCd(bltnCd);
			boardVO.setBltnType(bltnType);
			boardVO.setCategory(category);
			boardVO.setCategoryGrpCd(categoryGrpCd);
			boardVO.setDelYn(delYn);
			boardVO.setSearchKey(searchKey);
			boardVO.setSearchOption(searchOption);
			boardVO.setPageNumber(pageNum);
			boardVO.setRowCount(rowCount);
			
			boardList	= boardService.selectBoardList(boardVO);
			totalCnt	= (Integer) (boardList.size() > 0 ? boardList.get(0).getTotCnt() : 0);
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, boardList);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}

	/**
	 * 게시글 상세
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectBoardDetail", method = RequestMethod.POST)
	public View selectBoardDetail(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map	= new HashMap<String, Object>();
		BoardVO boardVO			= new BoardVO();
		BoardVO boardDetail		= new BoardVO();
		
		String mainMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")					, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")					, "");
		
		int bltnCd			= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bltnCd")	, "0"));
		
		try {
			boardVO.setBltnCd(bltnCd);
			
			boardDetail		= boardService.selectBoardDetail(boardVO);
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("data"			, boardDetail);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		modelMap.addAttribute("result", map);
		
		return jsonView;
	}

	/**
	 * 게시글상세(답글포함)
	 * @param inputMap
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/selectBoardDetailList", method = RequestMethod.POST)
	public View selectBoardDetailList(@RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map			= new HashMap<String, Object>();
		BoardVO boardVO					= new BoardVO();
		FileVO fileVO					= new FileVO();
		List<BoardVO> boardDetailList	= null;
		List<FileVO> boardFileList		= null;
		
		int totalCnt	= 0;
		String mainMenu	= StringUtils.defaultIfEmpty((String) inputMap.get("mainMenu")					, "");
		String subMenu	= StringUtils.defaultIfEmpty((String) inputMap.get("subMenu")					, "");
		
		int bltnCd		= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bltnCd")	, "0"));
		int bltnType	= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bltnType")	, "0"));
		String delYn	= StringUtils.defaultIfEmpty((String) inputMap.get("delYn")						, "N");
		
		try {
			boardVO.setBltnCd(bltnCd);
			boardVO.setBltnType(bltnType);
			boardVO.setDelYn(delYn);
			
			boardDetailList	= boardService.selectBoardDetailList(boardVO);
			totalCnt		= boardDetailList.size();
			
			for(BoardVO vo : boardDetailList) {
				fileVO.setBltnCd(vo.getBltnCd());
				boardFileList = boardService.selectBoardFileList(fileVO);
				vo.setFileVO(boardFileList);
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("totalCnt"		, totalCnt);
			map.put("data"			, boardDetailList);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, StaticObject.Message.DATA_SEARCH_FAILED);
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}

	/**
	 * 게시글 등록
	 * @param inputMap
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/insertBoardData", headers = "content-type=multipart/*", method = RequestMethod.POST)
	public View insertBoardData(MultipartHttpServletRequest request, @RequestParam("file") MultipartFile[]  files, ModelMap modelMap) {
		Map<String, Object> map			= new HashMap<String, Object>();
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		BoardVO boardVO					= new BoardVO();

		String userId		= sessionMap.get("userId");
		String mainMenu		= StringUtils.defaultIfEmpty((String) request.getParameter("mainMenu")						, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) request.getParameter("subMenu")						, "");
		
		int bltnCd			= 0;
		int bltnType		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("bltnType")		, "0"));
		int category		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("category")		, "0"));
		int upperBltnCd		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("upperBltnCd")	, "0"));
		int bltnLvl			= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("bltnLvl")		, "0"));
		String title		= StringUtils.defaultIfEmpty((String) request.getParameter("title")							, "");
		String cts			= StringUtils.defaultIfEmpty((String) request.getParameter("cts")							, "");
		String bltnStrtDate	= StringUtils.defaultIfEmpty((String) request.getParameter("bltnStrtDate")					, null);
		String bltnEndDate	= StringUtils.defaultIfEmpty((String) request.getParameter("bltnEndDate")					, null);
		String delYn		= StringUtils.defaultIfEmpty((String) request.getParameter("delYn")							, "N");
		String topNoticeYn	= StringUtils.defaultIfEmpty((String) request.getParameter("topNoticeYn")					, "N");
		
		try {
			// validate
			if(bltnType == 0) throw new Exception(StaticObject.Message.BOARD_CATEGORY_IS_NULL);
			if(("").equals(title)) throw new Exception(StaticObject.Message.BOARD_TITLE_IS_NULL);
			if(("").equals(cts)) throw new Exception(StaticObject.Message.BOARD_CTS_IS_NULL);
			
			boardVO.setBltnType(bltnType);
			boardVO.setCategory(category);
			boardVO.setUpperBltnCd(upperBltnCd);
			boardVO.setBltnLvl(bltnLvl);
			boardVO.setTitle(title);
			boardVO.setCts(cts);
			boardVO.setBltnStrtDate(bltnStrtDate);
			boardVO.setBltnEndDate(bltnEndDate);
			boardVO.setDelYn(delYn);
			boardVO.setTopNoticeYn(topNoticeYn);
			boardVO.setRegId(userId);
			boardVO.setUpdId(userId);
			
			boardService.insertBoardData(boardVO);
			bltnCd	= boardVO.getInsertKey();
			
			/* 파일 업로드 처리 */
			if (files != null && files.length > 0) {
				boardService.fileUpload(request, files, bltnCd);
				/*
				if(boardService.fileUpload(request, files, bltnCd))
					throw new Exception(StaticObject.Message.FILE_UPLOAD_FAILED);
				*/
			}
			/* 파일 업로드 처리 끝*/
			
			String returnPage = "";
			if(bltnType == 1000001) returnPage = "noticeList";
			if(bltnType == 1000002) returnPage = "faqList";
			if(bltnType == 1000003) returnPage = "inquireList";
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("data"			, null);
			map.put("returnPage"	, returnPage);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}

	/**
	 * 게시글 수정
	 * @param inputMap
	 * @param request
	 * @param modelMap
	 * @return
	 */
	@RequestMapping(value = "/mopic/api/updateBoardData", headers = "content-type=multipart/*", method = RequestMethod.POST)
	public View updateBoardData(MultipartHttpServletRequest request, @RequestParam("file") MultipartFile[]  files, ModelMap modelMap) {
		Map<String, Object> map			= new HashMap<String, Object>();
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		BoardVO boardVO					= new BoardVO();
		BoardVO existBoardVO			= new BoardVO();

		String userId		= sessionMap.get("userId");
		String mainMenu		= StringUtils.defaultIfEmpty((String) request.getParameter("mainMenu")					, "");
		String subMenu		= StringUtils.defaultIfEmpty((String) request.getParameter("subMenu")					, "");
		
		int bltnCd			= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("bltnCd")	, "0"));
		int bltnType		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("bltnType")	, "0"));
		int category		= Integer.parseInt(StringUtils.defaultIfEmpty((String) request.getParameter("category")	, "0"));
		String title		= StringUtils.defaultIfEmpty((String) request.getParameter("title")						, "");
		String cts			= StringUtils.defaultIfEmpty((String) request.getParameter("cts")						, "");
		String bltnStrtDate	= StringUtils.defaultIfEmpty((String) request.getParameter("bltnStrtDate")				, null);
		String bltnEndDate	= StringUtils.defaultIfEmpty((String) request.getParameter("bltnEndDate")				, null);
		String delYn		= StringUtils.defaultIfEmpty((String) request.getParameter("delYn")						, "N");
		String topNoticeYn	= StringUtils.defaultIfEmpty((String) request.getParameter("topNoticeYn")				, "");
		
		try {
			// check permission
			if(!this.checkBoardPermission(request, userId, bltnCd))
				throw new Exception(StaticObject.Message.PERMISSION_DENIED);

			// validate
			if(bltnCd == 0) throw new Exception(StaticObject.Message.DATA_SEARCH_FAILED);
			if(bltnType == 0) throw new Exception(StaticObject.Message.BOARD_CATEGORY_IS_NULL);
			if(!("Y").equals(delYn)) {
				if(("").equals(title)) throw new Exception(StaticObject.Message.BOARD_TITLE_IS_NULL);
				if(("").equals(cts)) throw new Exception(StaticObject.Message.BOARD_CTS_IS_NULL);
			}
			
			boardVO.setBltnCd(bltnCd);
			boardVO.setUpdId(userId);
			
			if(bltnType != existBoardVO.getBltnType())
				boardVO.setBltnType(bltnType);
			if(category != existBoardVO.getCategory())
				boardVO.setCategory(category);
			if(!StringUtils.isEmpty(title))
				boardVO.setTitle(title);
			if(!StringUtils.isEmpty(cts))
				boardVO.setCts(cts);
			if(!StringUtils.isEmpty(bltnStrtDate))
				boardVO.setBltnStrtDate(bltnStrtDate);
			if(!StringUtils.isEmpty(bltnEndDate))
				boardVO.setBltnEndDate(bltnEndDate);
			if(!StringUtils.isEmpty(delYn))
				boardVO.setDelYn(delYn);
			if(!StringUtils.isEmpty(topNoticeYn))
				boardVO.setTopNoticeYn(topNoticeYn);
			
			boardService.updateBoardData(boardVO);
			
			/* 파일 업로드 처리 */
			if (files != null && files.length >0) {
				boardService.fileUpload(request, files, bltnCd);
			}
			/* 파일 업로드 처리 끝*/
			
			String returnPage = "";
			if(bltnType == 1000001) returnPage = "noticeList";
			if(bltnType == 1000002) returnPage = "faqList";
			if(bltnType == 1000003) returnPage = "inquireList";
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("mainMenu"		, mainMenu);
			map.put("subMenu"		, subMenu);
			map.put("data"			, null);
			map.put("returnPage"	, returnPage);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, e.getMessage());
			e.printStackTrace();
		}
		
		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}

	@RequestMapping(value = "/mopic/api/deleteBoardData", method = RequestMethod.POST)
	public View deleteBoardData(HttpServletRequest request, @RequestBody Map<Object, Object> inputMap, ModelMap modelMap) {
		Map<String, Object> map			= new HashMap<String, Object>();
		BoardVO boardVO					= new BoardVO();
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		String userId					= sessionMap.get("userId");
		int bltnCd						= Integer.parseInt(StringUtils.defaultIfEmpty((String) inputMap.get("bltnCd")	, "0"));
		
		try {
			if(this.checkBoardPermission(request, userId, bltnCd)) {
				boardVO.setBltnCd(bltnCd);
				boardService.deleteBoardData(boardVO);
			}
			
			map.put("resultCode"	, StaticObject.Code.CODE_SUCCESS);
			map.put("data"			, null);
		} catch(Exception e) {
			map.put("resultCode"	, StaticObject.Code.CODE_FAILED);
			map.put("resultMessage"	, e.getMessage());
			e.printStackTrace();
		}

		modelMap.addAttribute("result"	, map);
		
		return jsonView;
	}
	
	/**
	 * 게시판 수정 및 삭제 권한 체크
	 * 
	 * @return
	 */
	public boolean checkBoardPermission(HttpServletRequest request, String userId, int bltnCd) {
		Map<String, String> sessionMap	= CommonUtil.getSession(request);
		BoardVO boardVO	= new BoardVO();
		String regId	= "";
		
		boardVO.setBltnCd(bltnCd);
		boardVO	= boardService.selectBoardDetail(boardVO);
		regId	= boardVO.getRegId();
		
		if(("super").equals(sessionMap.get("user_level"))) {
			return true;
		} else {
			if((regId).equals(sessionMap.get("userId")))
				return true;
		}
		
		return false;
	}
	
	@RequestMapping(value="/admin/test", method=RequestMethod.GET)
	public String test(HttpServletRequest request, ModelMap modelMap) {
		String path = request.getSession().getServletContext().getRealPath("/card-page");
		
		modelMap.addAttribute("path", path);
		return "admin/test";
	}
	
	@RequestMapping(value="/admin/test2", method=RequestMethod.GET)
	public String test2() {
		
		return "admin/test2";
	}
}