(function() {

	let React				= require('react'),
		ReactDOM			= require('react-dom'),
		QueryStore			= require('../common/stores/QueryStore'),
		ProductStore		= require('../common/stores/ProductStore'),
		ComparisonStore		= require('../common/stores/ComparisonStore'),
		CommonActions		= require('../common/actions/CommonActions'),
		getSearchCallback	= require('../common/utils/getSearchCallback'),
		P2pHelperSection	= require('./components/P2pHelperSection.jsx'),
		P2pResultSection	= require('./components/P2pResultSection.jsx');

	
	let searchQuery;
    
    if(window.App) {
		({searchQuery} = window.App.query);
		
		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons'); // just for login state
    }


//	CommonActions.fetchOptions([
//		'5100', '5101', '5200', '5201', 'P0001', 'P0002', 'P0003'
//	]);


	let searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away


    ReactDOM.render(
		<P2pHelperSection />,
		document.getElementById('helper'),
		searchCallback
    );

	ReactDOM.render(
		<P2pResultSection />,
		document.getElementById('results')
    );


})();
