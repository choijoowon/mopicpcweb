let React = require('react'),
	ProductCard			= require('../../common/components/ProductCard.jsx'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas;

const submenu_codes	= require('../../common/constants/submenu_codes');


let P2pResultSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'), // just for login state
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/resultSectionMixin.jsx')
	],
	
    propTypes: {

    },

    getDefaultProps: function() {
        return {
			
        };
    },

    getInitialState: function() {
        return {

        }
    },

	componentDidMount() {

	},

    componentWillUnmount: function() {

    },

	_getOptions(submenu, optCodeMap) {
		//  returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element
		
		let {resultState, comparisonState} = this.state;
		
		let { isLoggedIn } = comparisonState;
		
		let filterOptions, sortOptions, tooltipElem, resultsElem, helperElem;
		
		switch(submenu.toLowerCase()) {
			// 투자하기
			case('invest'):
				//filterOptions = this._getListOptions(optCodeMap, 5101);
				//filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = this._getListOptions(optCodeMap, '5100');
				//tooltipElem = <span className="badge" data-toggle="tooltip" data-placement="right" title="가입하신 상품보다 주요업종의 혜택 개수와 혜택률의 합이 많으며, 연회비가 낮은 카드가 추천됩니다.">?</span>;
				
				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						filter, // 투자중 or 투자마감
						pp_company, //업체코드
						pp_rate, //금리
						prod_name, //상품명
						prod_code, //상품코드
						pp_loan, //모집금액
						pp_maturity, //상환일
						pp_grade, //신용등급
						pp_repayment, //상환방식
						pp_goal, //투자목표
						pp_present, //투자현황
						interest_yn, //즐겨찾기여부
						pp_url // link url
					} = elem;

					let mainAttrSet = {
						'모집금액': `${numberWithCommas(pp_loan)} 만원`,
						'상환일': pp_maturity,
						'신용등급': pp_grade,
						'상환방식': pp_repayment
					};
					
					let acheiveRate = Math.round(pp_present * 1.0 / pp_goal * 10000.0) / 100.0;
					let progress = {
						label: '모집금액',
						rate: acheiveRate,
						unit: '%'
					};
					
					let isInterested = (interest_yn === 'Y'); // 관심상품?
					
					let submenuCode = submenu_codes[submenu.toLowerCase()];

					//targetModalSelector='#product_view_popup'
					
					return (
						<ProductCard
							key={prod_code}
							type='type4'
							provider={pp_company}
							leadingInfo={pp_rate}
							prefix='연'
							unit='%'
							productTitle={prod_name}
							progressBar={progress}
							mainAttrSet={mainAttrSet}
							
							isInterested={isInterested}
							index={idx}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, submenuCode, prod_code, idx)}
							companyLink={pp_url}
						/>
					);
				});

				break;
				
			// 대출받기
			case('ploan'):
				//filterOptions = this._getListOptions(optCodeMap, 5201);
				//filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = this._getListOptions(optCodeMap, '5200');
				//tooltipElem = <span className="badge" data-toggle="tooltip" data-placement="right" title="가입하신 상품보다 주요업종의 혜택 개수와 혜택률의 합이 많으며, 연회비가 낮은 카드가 추천됩니다.">?</span>;
				
				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						//filter
						pp_company, //업체코드
						//기타신용대출, 사회초년생신용대출,...
						pl_ratest, //금리 from
						pl_rateen, //금리 to
						prod_name, //상품명
						prod_code, //상품코드
						pl_limitst, //한도from
						pl_limiten, //한도to
						pl_datest, //기간from
						pl_dateen, //기간to
						pl_repayment, //상환방식
						pl_qualification, //신청자격나이
						pl_grade, //신청자격등급
						pl_document, //신청자격문서
						interest_yn, //즐겨찾기여부
						pp_url // Link URL
					} = elem;

					let mainAttrSet = {
						'금리': `연 ${pl_ratest} ~ ${pl_rateen}%`,
						'한도': `${pl_limitst}만원 ~ ${pl_limiten}만원`,
						'기간': `${pl_datest}개월 ~ ${pl_dateen}개월`,
						'상환': pl_repayment,
						'신청자격': `${pl_qualification} / ${pl_grade} / ${pl_document}`,
					};

					let isInterested = (interest_yn === 'Y'); // 관심상품?
					
					let submenuCode = submenu_codes[submenu.toLowerCase()];

					//targetModalSelector='#product_view_popup'
					
					return (
						<ProductCard
							key={prod_code}
							type='type4'
							provider={pp_company}
							leadingInfo={pl_ratest}
							prefix='연'
							unit='%~'
							productTitle={prod_name}
							mainAttrSet={mainAttrSet}
							
							isInterested={isInterested}
							index={idx}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, submenuCode, prod_code, idx)}
							companyLink={pp_url}
						/>
					);
				});

				break;


			default:
		}

		return {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem};
	},

	getPreSearchView(submenu) {
		let preSearchViewElem;
		
		switch(submenu.toLowerCase()) {
			case('shilson'):
				break;
			case('car'):
				break;
			default:
				
		}

		return preSearchViewElem;

	}
	

    
});

module.exports = P2pResultSection;
