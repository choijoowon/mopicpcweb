let React				= require('react'),
	formValidators		= require('../../common/utils/formValidators'),
	QuerySection		= require('../../common/components/QuerySection.jsx');

let { selectRequired } = formValidators;


class SelectOpts {
	constructor(label, value, iconCssPrefix) {
		this.label = label;
		this.value = value;

		if(iconCssPrefix) {
			this.icon = iconCssPrefix + value;
		}
	}
}


let P2pHelperSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/sendQueryMixin')
	],

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {
			
        }
    },
    
    componentDidMount() {

    },

    componentWillUnmount() {

    },

	render() {
		let {queryState, resultState} = this.state;
		let {optCodeMap, submenu} = queryState;


		let searchEntries = this._getSearchEntries(queryState);
		
        return (
            <div className='top_search'>
				<section className='tab-content'>
					<QuerySection
						queryState={queryState}
						resultState={resultState}
						entries={searchEntries} />
				</section>
            </div>
        );
    },

	getSelectOptions(optCodeMap, code, iconCssPrefix) {
		if(optCodeMap[code]) {
			return ( optCodeMap[code].map((e) => new SelectOpts(e.cdName, e.cd, iconCssPrefix)) );
		}
		
		// TODO: when no map for the code?
		return [];
	},

	_getSearchEntries(queryState) {
		let {submenu, searchQuery, optCodeMap} = queryState;
		let entries;

		switch(submenu.toLowerCase()) {
			// 투자하기
			case('invest'):
				entries = [
					{
						label: 'P2P 업체명',
						type: 'select',
						placeholder: '선택',
						id: 'company_code',
						options: this.getSelectOptions(optCodeMap, 'P0001'),
						validationFn: selectRequired
					}
				];
				break;
			// 대출하기
			case('ploan'):
				entries = [
					{
						label: '대출유형',
						type: 'radio-group',
						placeholder: '선택',
						id: 'gubun',
						options: this.getSelectOptions(optCodeMap, 'P0003'),
						validationFn: selectRequired
					},

					{
						label: '대출업체명',
						type: 'select',
						placeholder: '선택',
						id: 'company_code',
						options: this.getSelectOptions(optCodeMap, 'P0001'),
						validationFn: selectRequired
					}
				];
				
				break;


			default:
				entries = [];
		}

		return entries;
	}
	
	
    
});


module.exports = P2pHelperSection;
