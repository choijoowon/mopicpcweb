(function() {

	let React				= require('react'),
		ReactDOM			= require('react-dom'),
		QueryStore			= require('../common/stores/QueryStore'),
		ProductStore		= require('../common/stores/ProductStore'),
		ComparisonStore		= require('../common/stores/ComparisonStore'),
		CommonActions		= require('../common/actions/CommonActions'),
		getSearchCallback	= require('../common/utils/getSearchCallback'),
		CardHelperSection	= require('./components/CardHelperSection.jsx'),
		CardResultSection	= require('./components/CardResultSection.jsx'),
		CardProductAdder	= require('./components/CardProductAdder.jsx');

	
	let mainmenu, submenu, isLoggedIn, searchQuery;

	if(window.App) {
		({mainmenu, submenu, searchQuery} = window.App.query);
		isLoggedIn = window.App.comparisons.isLoggedIn;		
		
		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
    }


//	CommonActions.fetchOptions([
//		4100, 4101, 4102, 4103, 4104, 4105, 4106
//	]); // grpList slow yet


	if(isLoggedIn) {
	 	CommonActions.fetchMyProducts(mainmenu, submenu);
	}
	

	let productAdder = <CardProductAdder />;

	let searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away
	
	
    ReactDOM.render(
		<CardHelperSection
			adderModal={productAdder} />,
		document.getElementById('helper'),
		searchCallback
    );

	ReactDOM.render(
		<CardResultSection />,
		document.getElementById('results')
    );


})();
