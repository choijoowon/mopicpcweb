let React				= require('react'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas,
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, selectAtLeastOne } = formValidators;


let CardHelperSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/helperSectionMixin.jsx'),
	],
	

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {
			
        }
    },
    
    componentDidMount() {

    },

    componentWillUnmount() {

    },

	_getSearchEntries(queryState) {
		let {submenu, searchQuery, optCodeMap} = queryState;
		let entries;

		switch(submenu.toLowerCase()) {
			// 신용카드
			case('credit'):
			case('check'):
				let companyOptCode = submenu.toLowerCase() === 'credit' ? 4102 : 4105;
				let benefitOptCode = submenu.toLowerCase() === 'credit' ? 4104 : 4106;
				
				entries = [
					{
						label: '카드사',
						type: 'multi-select-icon',
						placeholder: '사용하실 카드사를 선택해 주세요',
						id: 'companyJoinToString',
						options: this.getSelectOptions(optCodeMap, companyOptCode, 'bi bi-'),
						validationFn: selectAtLeastOne
					},
					{
						label: '주요혜택',
						type: 'multi-select-icon',
						placeholder: '원하시는 혜택을 선택해주세요.(정확한 검색을 위해 3개까지 선택 가능합니다)',
						id: 'benefitsToSplit',
						limitSelection: 3,
						options: this.getSelectOptions(optCodeMap, benefitOptCode, 'benefits benefits-icon-'),
						validationFn: selectAtLeastOne
					},
					{
						label: '연회비',
						type: 'select',
						id: 'fee',
						options: this.getSelectOptions(optCodeMap, 4103),
						validationFn: selectRequired
					},
					{
						label: '월평균사용액',
						type: 'number-with-commas',
						id: 'month_sum',
						unit: '만원',
						validationFn: selectRequired
					}

				];
				break;


			default:
				entries = [];
		}

		return entries;
	},

	_getCmprCardContents(submenu, comparisonGroup) {
		let cmprCardContents = [];

		switch(submenu.toLowerCase()) {
			case('credit'):
			case('check'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						company_nm, //카드사
						card_name, //상품명
						require_result, //전월기준
						internal_annual_fee, //연회비 국내
						foreign_annual_fee //연회비 해외
					} = elem;
					
					return	{
						title: card_name,
						info: [
							{
								attr: '카드사명',
								value: company_nm,
								colSpan: 4,
								isProvider: true
							},
							{
								attr: '전월실적',
								value: require_result,
								colSpan: 4,
								isLeadingInfo: true
							},
							{
								attr: '연회비',
								value: `${numberWithCommas(internal_annual_fee)}원(해외겸용 ${numberWithCommas(foreign_annual_fee)}원)`,
								colSpan: 4
							}
						]
					}
				});
				break;
		}


		return cmprCardContents;
	}
	
    
});


module.exports = CardHelperSection;
