let React = require('react'),
	ProductCard			= require('../../common/components/ProductCard.jsx'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas;

const cardBrandMap	= require('../constants/cardBrandMap'),
	cardBenefitMap	= require('../constants/cardBenefitMap'),
	submenu_codes	= require('../../common/constants/submenu_codes');

let CardResultSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/resultSectionMixin.jsx')
	],
	
    propTypes: {

    },

    getDefaultProps: function() {
        return {
			
        };
    },

    getInitialState: function() {
        return {

        }
    },

	componentDidMount() {

	},

    componentWillUnmount: function() {

    },

	_getOptions(submenu, optCodeMap) {
		//  returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element
		
		let {resultState, comparisonState} = this.state;
		
		let { isCmprTabActive, isLoggedIn, comparisonGroup,	currentCmpr } = comparisonState;
		let showingCmpr = (isLoggedIn && isCmprTabActive && comparisonGroup.length > 0);
		let selectedCmpr = comparisonGroup[currentCmpr];
		
		let filterOptions, sortOptions, tooltipElem, resultsElem, helperElem;
		let comparisonContents = undefined;
		
		switch(submenu.toLowerCase()) {
			// 신용카드
			case('credit'):
			// 체크카드
			case('check'):
				sortOptions = this._getListOptions(optCodeMap, 4100);
				
				if ( showingCmpr ){
					tooltipElem = <span className="badge" data-toggle="tooltip" data-placement="right" title="가입하신 상품보다 주요업종의 혜택 개수와 혜택률의 합이 많으며, 연회비가 낮은 카드가 추천됩니다.">?</span>;
				}
				
				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						COMPANY_CD,
						CARD_CD, // 카드코드 (이미지 url결정)
						CARD_NAME,
						REQUIRE_RESULT, // 전월기준
						INTERNAL_ANNUAL_FEE, // 연회비 국내
						FOREIGN_ANNUAL_FEE, // 연회비 해외겸용
						BRAND_TYPE,
						BENEFIT,
						HEADCOPY,
						detail_url,
						interest_yn
					} = elem;

					let brandList = Array.prototype.map.call(BRAND_TYPE, (e) => cardBrandMap[e]);
					
					let mainAttrSet = {
						'전월실적': REQUIRE_RESULT || '조건없음',
						'연회비': `${numberWithCommas(INTERNAL_ANNUAL_FEE)}원(해외겸용 ${numberWithCommas(FOREIGN_ANNUAL_FEE)}원)`,
						'브랜드': brandList.join(', ')
					};
					

					let benefits = [];
					
					for(let i = 0; i < 3; i++) {
						let categoryCd = BENEFIT[`SUB_CATEGORY${i+1}`];
						let benefitDetail = BENEFIT[`BENEFIT_NAME${i+1}`];
						if(categoryCd) {
							benefits.push(`<div class="benefit" data-toggle="tooltip" data-placement="top" title="${benefitDetail}"><i class="benefits benefits-icon-${categoryCd}"></i><span>${String(cardBenefitMap[categoryCd]).replace('/','<br/>')}</span></div>`);
						}
					}
					
					if(showingCmpr) {
						comparisonContents = []
						
						for(let i = 0; i < 3; i++) {
							let categoryCd = BENEFIT[`SUB_CATEGORY${i+1}`];
							let benefitDetail = BENEFIT[`BENEFIT_NAME${i+1}`];
							if(categoryCd) {
								comparisonContents.push({
									attr: cardBenefitMap[categoryCd],
									diff: BENEFIT[`SUB_SUM${i+1}`],
									unit: '원'
								});
							}
						}
					}

					let isInterested = (interest_yn === 'Y'); // 관심상품?
					
					let submenuCode = submenu_codes[submenu.toLowerCase()];
					
					return (
						<ProductCard
							key={`creditcard${CARD_NAME}${BRAND_TYPE}`}
							type='type3'
							productImgUrl={`http://www.cardbaro.com/upload/card/${CARD_CD}.gif`}
							productTitle={CARD_NAME}
							description={HEADCOPY}
							features={benefits}
							mainAttrSet={mainAttrSet}
							targetModalSelector='#product_view_popup'
							detailUrl={detail_url}
							comparisonContents={comparisonContents}
							isInterested={isInterested}
							index={idx}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, submenuCode, CARD_CD, idx)}
							choiceReverse={false}
						/>
					);
				});

				break;


			default:
		}

		return {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem};
	},

	getPreSearchView(submenu) {
		let preSearchViewElem;
		
		switch(submenu.toLowerCase()) {
			case('shilson'):
				break;
			case('car'):
				break;
			default:
				
		}

		return preSearchViewElem;

	}

    
});

module.exports = CardResultSection;
