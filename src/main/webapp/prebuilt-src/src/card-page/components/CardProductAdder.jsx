let React			= require('react'),
	SelectDropdown	= require('../../common/components/SelectDropdown.jsx'),
	TextInput		= require('../../common/components/TextInput.jsx'),
	Api				= require('../../common/utils/api'),
	CommonActions	= require('../../common/actions/CommonActions'),
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, numberRequired } = formValidators;

const submenu_codes = require('../../common/constants/submenu_codes');


class SelectOpts {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		this.data = data;
	}
}


let CardProductAdder = React.createClass({
    propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
    },

    getDefaultProps() {
        return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {}
        };
    },
	
	getInitialState: function() {
		let { optCodeMap } = this.props;
		let companyOpts;
		
		if(optCodeMap[4102]) {
			companyOpts = ( optCodeMap[4102].map((e) => new SelectOpts(e.cdName, e.cd)) );
		}
		
        return {
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',

			amount: '',

			sub_category1: '',
			sub_category2: '',
			sub_category3: '',
			sub_sum1: '',
			sub_sum2: '',
			sub_sum3: '',
			
			prodNameOpts: [],
			companyOpts: companyOpts || '',
			benefitOpts: [],
			prodNamePlaceholder: '카드사를 선택해주세요.'
        };
    },
    

    componentDidMount() {

    },

    componentWillUnmount() {
		
    },

    render() {
		let {
			showValidation,
			company_code,
			prod_code,

			amount,

			sub_category1,
			sub_category2,
			sub_category3,
			sub_sum1,
			sub_sum2,
			sub_sum3,

			prodNameOpts,
			companyOpts,
			benefitOpts,

			prodNamePlaceholder
		} = this.state;
		let { submenu, searchEntries, optCodeMap } = this.props;

		let companyForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">카드사</label>
					<SelectDropdown
						placeholder='선택'
						options={companyOpts}
						handleSelect={this.handleCompanySelect}
						selected={company_code}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);

		let productNameForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">상품명</label>
					<SelectDropdown
						placeholder={prodNamePlaceholder}
						options={prodNameOpts}
						handleSelect={this.handleProductNameSelect}
						selected={prod_code}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);


		let amountEntry = searchEntries.find((e) => (e.id === 'month_sum'));
		let amountForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">월평균 사용금액</label>
					<TextInput
						type='number-with-commas'
						placeholder={amountEntry.placeholder || ' '}
						inputClass='text-input-normal'
						handleChange={this.handleTextInputChange.bind(null, 'amount')}
						value={amount}
						unit={amountEntry.unit || ''}
						validationFn={showValidation && numberRequired}
					/>
				</div>
			</div>
		);


		let benefitsForm = [];
		let disabled = false;
		let filteredOptions = benefitOpts.slice();
		
		for (let i = 0; i < 3; i++) {
			let currentSubCategory = this.state[`sub_category${i+1}`];
			let currentSubSum = this.state[`sub_sum${i+1}`];

			let categoryValidation, subsumValidation;
			if(i === 0) {
				categoryValidation = this.state.showValidation && selectRequired;
				subsumValidation = this.state.showValidation && numberRequired;
			}

			
			benefitsForm.push(
				<div key={`benefitform${i}`} className="row">
					<div className="col-xs-6">
						<label className="select-label">{i === 0 ? '혜택별 사용금액' : ''}</label>

						<SelectDropdown
							placeholder='-'
							options={filteredOptions.slice()}
							handleSelect={this.handleSelectChange.bind(null, `sub_category${i+1}`)}
							selected={currentSubCategory}
							disabled={disabled}
							validationFn={categoryValidation}
						/>
					</div>
					<div className="col-xs-6">
						<TextInput
							type='number-with-commas'
							placeholder={amountEntry.placeholder || ' '}
							inputClass='text-input-normal'
							handleChange={this.handleTextInputChange.bind(null, `sub_sum${i+1}`)}
							value={currentSubSum}
							unit={amountEntry.unit || ''}
							fixed={disabled}
							validationFn={subsumValidation}
						/>
					</div>
				</div>
			);

			// disable next part if current benefit is not selected
			disabled = disabled || !(currentSubCategory);

			// exclude current sub_category from next parts options
			if(currentSubCategory) {
				filteredOptions = filteredOptions.filter((e) => e.value !== currentSubCategory);
			}
			
		}
		
		

		
		
		return (
			<div className="modal fade" id="product_add_popup">
				<div className="modal-dialog">
					<div className="modal-content modal-small">

						<a className="close" data-dismiss="modal" aria-label="Close" />
						
						<div className="modal-header">
							<h4 className="modal-title" id="myModalLabel">가입상품 추가하기</h4>
						</div>

						
						<div className="modal-body">
							<div className="product_add">

								{companyForm}
								{productNameForm}
								{amountForm}
								{benefitsForm}
								
								<div className="center_btn">
									<a className="btn btn-submit" role="button" onClick={this.submitProduct}>확인</a>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		);
    },


	handleCompanySelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		let company_code = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			company_code,
			prod_code: '',
			prodNameOpts: [],
			benefitOpts: [],

			sub_category1: '',
			sub_category2: '',
			sub_category3: '',
			sub_sum1: '',
			sub_sum2: '',
			sub_sum3: ''
		});

		this.fetchProductNames(company_code)
		
	},

	fetchProductNames(company_code) {
		let menu_cd = submenu_codes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/card/filterProductByCompany', { menu_cd: String(menu_cd), company_code })
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;
					let options = ( resultData.map((e) => new SelectOpts(e.card_name, e.card_cd, e)) );

					if(resultData && resultData.length > 0) {
						this.setState({
							prodNameOpts: options,
							prodNamePlaceholder: '선택'
						});
					} else {
						this.setState({
							prodNamePlaceholder: '해당 카드사의 상품이 존재하지 않습니다'
						});
					}
				}
			}.bind(this));
		
	},
	

	handleProductNameSelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		let prod_code = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			prod_code: prod_code,
			benefitOpts: [],

			sub_category1: '',
			sub_category2: '',
			sub_category3: '',
			sub_sum1: '',
			sub_sum2: '',
			sub_sum3: ''
		});

		this.setBenefitOpts(prod_code);
	},

	
	handleSelectChange(queryAttr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		let val = $(target).closest('li').attr('data-val') || target.textContent;
		
		this.setState({
			[queryAttr]: val
		});
	},

	handleTextInputChange(queryAttr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		let val = target.value;
		
		this.setState({
			[queryAttr]: val
		});
	},


	clearStats() {
		this.setState({
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			prodNameOpts: []
		});
	},

	checkValidity() {
		let {
			company_code,
			prod_code,
			amount,
			sub_category1,
			sub_sum1
		} = this.state;
		
		let allValid = true;
		
		allValid = allValid && (selectRequired(company_code) === true);
		allValid = allValid && (selectRequired(prod_code) === true);
		allValid = allValid && (numberRequired(amount) === true);
		allValid = allValid && (selectRequired(sub_category1) === true);
		allValid = allValid && (numberRequired(sub_sum1) === true);
		
		return allValid;
	},

	setBenefitOpts(prodCd) {
		let benefitsRaw = this.state.prodNameOpts.find((e) => (e.value === prodCd)).data.benefit;
		let benefitOpts = benefitsRaw.map((e) => new SelectOpts(e.sub_category_nm, e.sub_category));

		if(benefitsRaw) {
			this.setState({
				benefitOpts
			});
		}
	},

	
	submitProduct(e) {
		e = e? e : window.event;
		let target = e.target || e.srcElement;
		let {
			prod_code,
			prodNameOpts,
			
			amount,

			sub_category1,
			sub_category2,
			sub_category3,
			sub_sum1,
			sub_sum2,
			sub_sum3,
		} = this.state;
		
		let { submenu, searchEntries } = this.props;
		let data;

		let allValid = this.checkValidity();

		if(allValid) {
			
			switch(submenu.toLowerCase()) {
				case('credit'):
				case('check'):
					data = {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label),
						amount: String(amount)
					};

					if(sub_category1 && sub_sum1) {
						data.sub_category1 = sub_category1;
						data.sub_sum1 = sub_sum1;

						if(sub_category2 && sub_sum2) {
							data.sub_category2 = sub_category2;
							data.sub_sum2 = sub_sum2;

							if(sub_category3 && sub_sum3) {
								data.sub_category3 = sub_category3;
								data.sub_sum3 = sub_sum3;
							}
						}
					}
					
					break;
			}
			
			let $target = $(target);
			CommonActions.addMyProduct(data, () => {
				$target.closest('.modal').modal('hide');
			});

		} else {
			this.setState({
				showValidation: true
			});
		}
	}
	
	
});

module.exports = CardProductAdder;
