let React			= require('react'),
	CommonActions	= require('../../common/actions/CommonActions'),
	formValidators	= require('../../common/utils/formValidators'),
	SelectDropdown	= require('../../common/components/SelectDropdown.jsx'),
	TextInput		= require('../../common/components/TextInput.jsx'),
	QuerySection	= require('../../common/components/QuerySection.jsx');
	


let { selectRequired, numberRequired } = formValidators;

let _tradeOptCache = {};
let _nationOptCache = {};

const initNationOpts = [
	{ code_nm: 'USD(미국 달러)', code_id: 'USD', nationlabel: '미국' },
	{ code_nm: 'JPY(일본 엔)', code_id: 'JPY', nationlabel: '일본' }
]; // initial options if not fetched yet


class SelectOpts {
	constructor(label, value) {
		this.label = label;
		this.value = value;
	}
}


function setDefaultIfAvailable(searchQuery, optCodeMap) {
	let tradeOpts = optCodeMap['EX003'];
	let nationOpts = optCodeMap['nationOpts'];

	// update nation state when fetched
	if( !searchQuery.tr_nation && nationOpts && nationOpts.length > 0 ) {
		let firstOpt = nationOpts[0];
		
		if(firstOpt) {
			setTimeout(() => {
				CommonActions.addOptions({ nationOpts });
				CommonActions.changeSearchQuery('tr_nation', firstOpt.code_id);
			}, 50);
		}
	}
	
	// update trade opt state when fetched
	if( !searchQuery.exchange_type && tradeOpts && tradeOpts.length > 0 ) {
		let firstOpt = tradeOpts[0];
		if(firstOpt) {
			setTimeout(() => {
				CommonActions.changeSearchQuery('exchange_type', firstOpt.cd);
				this.setState({
					toKRW: firstOpt.cdDesc === 'Y'
				});
			}, 50);
		}
	}

}



let ExchHelperSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
	],

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {
			currentNationIdx: 0,
			toKRW: false
        };
    },
    
    componentWillMount() {

    },

	componentDidMount() {
		/* set default options */
		let { searchQuery, optCodeMap } = this.state.queryState;

		//cache initial trade opt state
		_tradeOptCache = optCodeMap['EX003'];
		_nationOptCache = optCodeMap['nationOpts'];

		if(searchQuery.tr_nation) {
			this.setState({
				currentNationIdx: optCodeMap['nationOpts'].findIndex((e) => e.code_id === searchQuery.tr_nation)
			});
		}
		
		setDefaultIfAvailable.bind(this)(searchQuery, optCodeMap);
	},

    componentWillUnmount() {
		
    },

	componentDidUpdate(prevProps, prevState) {
		// set default trade option when optCode fetched
		let { searchQuery, optCodeMap } = this.state.queryState;
		let tradeOpts = optCodeMap['EX003'];
		let nationOpts = optCodeMap['nationOpts'];
		
		if(_tradeOptCache !== tradeOpts || _nationOptCache !== nationOpts) {
			_tradeOptCache = tradeOpts;
			_nationOptCache = nationOpts;

			setDefaultIfAvailable.bind(this)(searchQuery, optCodeMap);
		}
	},

	render() {
		let { queryState, resultState, currentNationIdx, toKRW } = this.state;
		let { optCodeMap, submenu, searchQuery } = queryState;

		let nationOpts = optCodeMap.nationOpts || initNationOpts;
		nationOpts = nationOpts.length > 0 ? nationOpts : initNationOpts;

		let tradeTypeOpts = this.getSelectOptions(optCodeMap, 'EX003');

		let nationForm = (
			<div className="row">
				<div className="col-xs-4">
					<label className="select-label">환전할 국가</label>
					<SelectDropdown
						tabIndex={0}
						placeholder='선택'
						options={nationOpts.map((e) => new SelectOpts(e.code_nm, e.code_id) )}
						handleSelect={this.handleSelectChange.bind(null, 'tr_nation', ['money', 'coupon'])}
						className='select-dropdown'
						selected={queryState.searchQuery['tr_nation']}
						validationFn={queryState.showValidation && selectRequired}
					/>
				</div>
			</div>
		);
		

		let rateTable;
		if(nationOpts[currentNationIdx].tr_rate) {
			let {
				tr_date, //기준일
				tr_rate, //매매기준율
				tr_buy_cash, //현찰 살때
				tr_sell_cash, //현찰 팔때
				tr_send_money, //송금 보낼때
				tr_receive_money //송금 받을때
			} = nationOpts[currentNationIdx];
			
			rateTable = (
				<div className="row" style={{marginTop: '-18px'}}>
					<div className='notice-info'><span>기준일: {tr_date}</span><span className="devide">|</span><span>제공: KEB하나은행</span></div>
					<div className="col-xs-12">
						<ul className="currency-box">
							<li className="first"><span>매매기준율</span><span className="val">{tr_rate || '-'}</span></li>
							<li><span>현찰(살 때)</span><span className="val">{tr_buy_cash || '-'}</span></li>
							<li><span>현찰(팔 때)</span><span className="val">{tr_sell_cash || '-'}</span></li>
							<li><span>송금(보낼 때)</span><span className="val">{tr_send_money || '-'}</span></li>
							<li><span>송금(받을 때)</span><span className="val">{tr_receive_money || '-'}</span></li>
						</ul>
					</div>
				</div>
			);
		}


		let tradeTypeForm = (
			<div className="row">
				<div className="col-xs-4">
					<label className="select-label">거래종류</label>
					<SelectDropdown
						tabIndex={0}
						placeholder='선택'
						options={tradeTypeOpts}
						handleSelect={this.handleSelectChange.bind(null, 'exchange_type', ['coupon'])}
						className='select-dropdown'
						selected={queryState.searchQuery['exchange_type']}
						validationFn={queryState.showValidation && selectRequired}
					/>
				</div>
			</div>
		);

		

		let baseNationLabel, baseMsg, quoteMsg;

		if(toKRW === 'Y') {
			baseNationLabel = nationOpts[currentNationIdx].nationlabel;
			baseMsg = `${nationOpts[currentNationIdx].code_nm}를`;
			quoteMsg = 'KRW(대한민국 원)으로 환전합니다.';
		} else {
			baseNationLabel = '대한민국';
			baseMsg = 'KRW(대한민국 원)를';
			quoteMsg = `${nationOpts[currentNationIdx].code_nm}으로 환전합니다.`;
		}
		
		let amountForm = (
			<div className="row">
				<div className="col-xs-4">
					<label className="select-label">{baseNationLabel}</label>
					<TextInput
						type='number-with-commas'
						placeholder='금액입력'
						inputClass='text-input-normal'
						handleChange={this.handleTextInputChange.bind(null, 'money')}
						value={queryState.searchQuery['money']}
						validationFn={queryState.showValidation && numberRequired}
					/>
				</div>
				<div className="currency-right">
					<ul>
						<li><span>{baseMsg}</span></li>
						<li><span className="quote">{quoteMsg}</span></li>
					</ul>
				</div>
			</div>
		);

		let couponElem;
		if(queryState.searchQuery.exchange_type === 'TR_BUY_CASH') {
			couponElem = (
				<div className="row">
					<div className="col-xs-4">
						<label htmlFor="select" className="select-label coupon">우대쿠폰</label>
						<TextInput
							type='number-with-commas'
							placeholder='쿠폰할인율'
							inputClass='text-input-normal'
							handleChange={this.handleTextInputChange.bind(null, 'coupon')}
							value={queryState.searchQuery['coupon']}
							disabled={queryState.searchQuery.exchange_type !== 'TR_BUY_CASH'}
							unit='%'
							maxlength={3}
							maximum={100}
						/>
					</div>
				</div>
			);
		}
		

		return (
			<div className="top_search">

				{nationForm}
				{rateTable}

				{tradeTypeForm}
				
				{amountForm}
				
				{couponElem}


				<div className='right_btn'>
					<a id="search-btn" className='btn btn-submit' role='button' onClick={this.handleSearchBtnClick}>검색</a>
				</div>

			</div>
		);


    },

	handleSelectChange(id, dependencies, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let liElem = $(target).closest('li');
		let val = liElem.attr('data-val');
		
		val = (val === 0) ? 0 : (val || target.textContent);

		CommonActions.changeSearchQuery(id, val);

		if(dependencies && dependencies.length > 0) {
			for(let i = 0, l = dependencies.length; i < l; i++) {
				let dependency = dependencies[i];
				CommonActions.changeSearchQuery(dependency, '');
			}
		}

		if(id === 'tr_nation') {
			this.setState({
				currentNationIdx: liElem.index()
			});
		}

		if(id === 'exchange_type') {
			let { optCodeMap } = this.state.queryState;
			this.setState({
				toKRW: optCodeMap['EX003'].find((e) => e.cd === val).cdDesc
			});
		}

	},

	handleTextInputChange(id, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		CommonActions.changeSearchQuery(id, target.value);
	},

	getSelectOptions(optCodeMap, code, iconCssPrefix) {
		if(optCodeMap[code]) {
			return ( optCodeMap[code].map((e) => new SelectOpts(e.cdName, e.cd)) );
		}
		
		// TODO: when no map for the code?
		return [];
	},

	handleSearchBtnClick() {
		let { queryState, resultState } = this.state;
		let { searchQuery } = queryState;

		let allValid = this.checkValidity();

		if(allValid) {
			CommonActions.setReceivedOnce(false);
			
			CommonActions.changeFilter(99999999);
			CommonActions.changeSorting('');

			$('body').addClass('masked'); // disable scrolling
			this.sendQueryWithCount(0, queryState, resultState, false);
		} else {
			CommonActions.validationToShow(true);
		}
	},

	checkValidity() {
		let allValid = true;
		let { searchQuery } = this.state.queryState;

		allValid = allValid && (selectRequired(searchQuery['tr_nation']) === true);
		allValid = allValid && (selectRequired(searchQuery['exchange_type']) === true);
		allValid = allValid && (numberRequired(searchQuery['money']) === true);

		return allValid;
	}

});

module.exports = ExchHelperSection;
