let React				= require('react'),
	ProductCard			= require('../../common/components/ProductCard.jsx'),
	SelectList			= require('../../common/components/SelectList.jsx'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas,
	Spinner				= require('../../common/components/spinner.jsx');


class ListOpts {
	constructor(label, value) {
		this.label = label;
		this.dataAttrs = { value: value };
	}
}


let InsureResultSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
	],
	
    propTypes: {

    },


    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {

        }
    },
    
    componentDidMount() {
		//let { queryState, resultState } = this.state;
		
		//if (resultState.productData.length === 0) {
		//	let {mainmenu, submenu} = queryState;
		//	let query = this.getAcceptableQuery(queryState);
		//	CommonActions.fetchResults(mainmenu, submenu, query);
		//}
    },

    componentWillUnmount() {

    },

	render() {
		let {queryState, resultState} = this.state;
		let optCodeMap = queryState.optCodeMap;
		
		let {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem} = this._getOptions(queryState.submenu, optCodeMap, resultState);


		if(resultState.receivedOnce) {
			if(resultState.productData.length > 0) {
				let unit = (queryState.searchQuery.toKRW === 'Y') ? '원' : (resultState.productData[0].unit || '-');
				
				let tableHeaderElem = (
					<tr>
						<th>은행명</th>
						<th>{`환전금액(${unit})`}</th>
						<th>{`절약금액(${unit})`}</th>
						<th>매매기준율(%)</th>
						<th>고지기준일자</th>
					</tr>
				);
				
				return (
					<section className='down_result'>

						<SelectList
							className='top_sort'
							options={sortOptions}
							value={ queryState.sorting || (sortOptions && sortOptions[0] && sortOptions[0].dataAttrs.value) || ''}
							handleSelect={()=>{}}>
							
							{tooltipElem}
						</SelectList>

						<section className='results'>
							
							<div className="car_list">
								
								<table summary="은형별 환율 정보">
									<caption>은형별 환율 정보</caption>
									<colgroup>
										<col width="190px" />
										<col width="140px" />
										<col width="140px" />
										<col width="140px" />
										<col width="*" />
									</colgroup>
									<thead>{tableHeaderElem}</thead>
									<tbody>{resultsElem}</tbody>
								</table>

								{ resultState.isFetching ? (
									<div className={resultState.isFetching ? 'text-center' : 'hide'} style={{padding: '25px 0'}}>
									    <Spinner width='40px' height='40px' />
									</div>
								 ) : undefined }
								
							</div>

						</section>
						
					</section>
				);
				
			} else {
				return (
					<div className='no-result-msg'>
						<p className='text-center'>검색결과가 없습니다.</p>
					</div>
				);

			}
		} else {

			let fetchingMsgElem, maskElem;
			
			if(resultState.isFetching) {
				maskElem = <div className="mask" />;
				
				fetchingMsgElem = (
					<div className="fetching-modal">
						<div className="fetching-dialog">
							<div className="fetching-msg text-center">
								<div>
									<Spinner width='80px' height='80px' />
								</div>
								<div className="message-box">
									<span>데이터를 불러오는 중입니다.<br />잠시만 기다려주세요.</span>
								</div>
							</div>
						</div>
					</div>
				);
			}
			
			return (
				<div className="pre-search text-center">
					<p>나에게 맞는 상품을 찾아 보세요.</p>
					{ maskElem }
					{ fetchingMsgElem }
				</div>
			);

		}


	},

	_getListOptions(optCodeMap, code) {
		if(optCodeMap[code]) {
			return ( optCodeMap[code].map((e) => new ListOpts(e.cdName, e.cd)) );
		}
		return [];
	},


	_getOptions(submenu, optCodeMap) {
		// returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element
		
		let { resultState, comparisonState } = this.state;
		
		let { isCmprTabActive, isLoggedIn, comparisonGroup,	currentCmpr } = comparisonState;
		let showingCmpr = (isLoggedIn && isCmprTabActive && comparisonGroup.length > 0);
		let selectedCmpr = comparisonGroup[currentCmpr];
		
		let filterOptions, sortOptions, tooltipElem, resultsElem, helperElem;
		let comparisonContents = undefined;

		
		sortOptions = this._getListOptions(optCodeMap, 6100);
		
		//tooltipElem = (
		//	<span className="badge" data-toggle="tooltip" data-placement="right" title="보험나이, 성별 등을 고려하여 가입하신 상품보다 보험료가 저렴한, 지급준비율이 높은, 부지급률이 낮은 상품을 추천해드립니다.">?</span>
		//);


		resultsElem = resultState.productData.map( (elem, idx) => {
			let {
				bi, //은행BI
				exchange_money, //환전금액
				saved_money, //절약금액
				tr_rate, //매매기준율
				tr_date, //고시기준일자
				tr_url
			} = elem;

			
			return (
				<tr key={`exch${bi}${exchange_money}${tr_rate}`}>
					<td><a href={tr_url} target="_blank"><i className={`bi bi-${bi}`}></i></a></td>
					<td>{exchange_money}</td>
					<td>{saved_money}</td>
					<td>{tr_rate}</td>
					<td>{tr_date}</td>
				</tr>
			);
		});



		return {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem};
	},

	getPreSearchView(submenu) {
		let preSearchViewElem;
		
		switch(submenu.toLowerCase()) {
			case('shilson'):
				break;
			case('car'):
				break;
			default:
				
		}

		return preSearchViewElem;

	}


    
});

module.exports = InsureResultSection;
