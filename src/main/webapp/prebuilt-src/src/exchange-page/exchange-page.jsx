(function() {
	let React				= require('react'),
		ReactDOM			= require('react-dom'),
		QueryStore			= require('../common/stores/QueryStore'),
		ProductStore		= require('../common/stores/ProductStore'),
		ComparisonStore		= require('../common/stores/ComparisonStore'),
		getSearchCallback	= require('../common/utils/getSearchCallback'),
		CommonActions		= require('../common/actions/CommonActions'),		
		ExchHelperSection	= require('./components/ExchHelperSection.jsx'),
		ExchResultSection	= require('./components/ExchResultSection.jsx');

	
	let searchQuery;
    
    if(window.App) {
		({searchQuery} = window.App.query);
		
		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons'); // just for login state
    }


//    CommonActions.fetchOptions([
//		'6100', 'EX002', 'EX003'
//	]);


	let searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away
	

    ReactDOM.render(
		<ExchHelperSection />,
		document.getElementById('helper'),
		searchCallback
    );

	ReactDOM.render(
		<ExchResultSection />,
		document.getElementById('results')
    );


})();
