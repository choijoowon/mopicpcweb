let React			= require('react'),
	SelectDropdown	= require('../../common/components/SelectDropdown.jsx'),
	TextInput		= require('../../common/components/TextInput.jsx'),
	Api				= require('../../common/utils/api'),
	CommonActions	= require('../../common/actions/CommonActions'),
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, numberRequired } = formValidators;


const submenu_codes = require('../../common/constants/submenu_codes');

const savingsMenuCodes = {
	deposit: 'A000101',
	saving: 'A000102',
	mmda: 'A000103',
	house: 'A000104'
};


function convertTermCode(termCd, submenu, optCodeMap) {
	let termGrpCode, termProp, termNum;


	if(submenu === 'mmda') {
		// 'mmda' does not have a term option
		termProp = 'DS_PRE_TAX';
		termNum = '0';
	} else {
		// else, find proper optCodeMap
		switch(submenu) { 
			case('deposit'):
				termGrpCode = 'A0001013';
				break;
			case('saving'):
				termGrpCode = 'A0001023';
				break;
			case('house'):
				termGrpCode = 'A0001043';
				break;
		}

		if(optCodeMap && termGrpCode) {
			let opt = optCodeMap[termGrpCode].find((e) => e.cd === termCd);

			if(opt) {
				switch(opt.cdName.trim()) {
					case('3개월'):
						termProp = 'DS_RATE3';
						termNum = '3';
						break;
					case('6개월'):
						termProp = 'DS_RATE6';
						termNum = '6';
						break;
					case('12개월'):
					case('1년'):
						termProp = 'DS_RATE12';
						termNum = '12';
						break;
					case('24개월'):
					case('2년'):
					case('2년이상'):
						termProp = 'DS_RATE24';
						termNum = '24';
						break;
					case('36개월'):
					case('3년'):
					case('3년이상'):
						termProp = 'DS_RATE36';
						termNum = '36';
						break;
					default:
						throw new Error(`cdName for the term code '${termCd}' is not one of followings: (3개월, 6개월, 12개월, 24개월, 36개월, 1년, 2년, 2년이상, 3년이상)`);
				}
			} else {
				new Error(`Cannot find '${termGrpCode}' property in optCodeMap`);
			}
			
		} else {
			throw new Error('optCodeMap or proper grpCode for term is not provided');
		}
	}


	return { termProp, termNum };
}



class SelectOpts {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		this.data = data;
	}
}


let SavingsProductAdder = React.createClass({
    propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
    },

    getDefaultProps() {
        return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {}
        };
    },
	
	getInitialState: function() {
        return {
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			companyOpts: [],
			prodNameOpts: [],

			prodNamePlaceholder: '기관명을 선택하세요'
        };
    },
    

    componentDidMount() {
		let category = savingsMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/saving/selectSavingBankList', {category})
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;
					let options = ( resultData.map((e) => new SelectOpts(e.cdName, e.cd)) );
					
					this.setState({
						companyOpts: options
					});
				}
			}.bind(this));

		
		// clear when closed
		$('#product_add_popup').on('hidden.bs.modal', this.clearStats);

    },

    componentWillUnmount() {
		
    },

    render() {
		let {
			showValidation,
			companyOpts,
			company_code,
			prodNameOpts,
			interest,
			prod_code,
			term,
			amount,
			prodNamePlaceholder
		} = this.state;
		let { submenu, searchEntries, optCodeMap } = this.props;

		let companyForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">기관명</label>
					<SelectDropdown
						placeholder='선택'
						options={companyOpts}
						handleSelect={this.handleCompanySelect}
						selected={company_code}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);

		let productNameForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">상품명</label>
					<SelectDropdown
						placeholder={prodNamePlaceholder}
						options={prodNameOpts}
						handleSelect={this.handleProductNameSelect}
						selected={prod_code}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);



		let termEntry = searchEntries.find((e) => (e.id === 'periodCd'));
		let termForm = undefined;
		if(termEntry) {
			termForm = (
				<div className="row">
					<div className="col-xs-6">
						<label className="select-label">가입기간</label>
						<SelectDropdown
							placeholder={termEntry.placeholder || '선택'}
							options={termEntry.options}
							handleSelect={this.handleTermSelect}
							className={termEntry.className || 'select-dropdown'}
							selected={term}
							validationFn={showValidation && termEntry.validationFn}
						/>
					</div>
				</div>
			);
		}

		let amountEntry = searchEntries.find((e) => (e.id === 'amount'));
		let amountForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">{amountEntry.label}</label>
					<TextInput
						type='number-with-commas'
						placeholder={amountEntry.placeholder || ' '}
						inputClass='text-input-normal'
						handleChange={this.handleTextInputChange.bind(null, 'amount')}
						value={amount}
						unit={amountEntry.unit || ''}
						validationFn={showValidation && amountEntry.validationFn}
					/>
				</div>
			</div>
		);


		
		let interestForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">금리</label>
					<TextInput
						type='number'
						placeholder=' '
						inputClass='text-input-normal'
						handleChange={this.handleTextInputChange.bind(null, 'interest')}
						value={interest}
						unit='%'
						validationFn={showValidation && numberRequired}
					/>
				</div>
			</div>
		);

		
		
		return (
			<div className="modal fade" id="product_add_popup">
				<div className="modal-dialog">
					<div className="modal-content modal-small">

						<a className="close" data-dismiss="modal" aria-label="Close" />
						
						<div className="modal-header">
							<h4 className="modal-title" id="myModalLabel">가입상품 추가하기</h4>
						</div>

						
						<div className="modal-body">
							<div className="product_add">

								{companyForm}
								{productNameForm}
								{termForm}
								{amountForm}
								{interestForm}
								
								<span className="txt">(금리를 입력하지 않으실 경우, 등록일 현재 기준 금리로 적용됩니다.)</span>
								<div className="center_btn">
									<a className="btn btn-submit" role="button" onClick={this.submitProduct}>확인</a>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		);
    },


	handleCompanySelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let companyCode = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			company_code: companyCode,
			prod_code: '',
			prodNameOpts: [],
			interest: ''
		});

		this.fetchProductNames(companyCode);
	},

	fetchProductNames(company_code) {
		let dsCode = savingsMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/saving/filterProductList', { dsBankingCode: company_code, dsCode })
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;

					if(resultData && resultData.length > 0) {
						let options = ( resultData.map((e) => new SelectOpts(e.DS_NM, e.AC_CODE, e)) );
						
						this.setState({
							prodNameOpts: options,
							prodNamePlaceholder: '선택'
						});
					} else {
						this.setState({
							prodNamePlaceholder: '해당 조건의 상품이 존재하지 않습니다'
						});
					}
				}
			}.bind(this));

	},

	handleProductNameSelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let prodCd = $(target).closest('li').attr('data-val') || target.textContent;
		let { term } = this.state;

		this.setState({
			prod_code: prodCd,
			interest: ''
		});

		if(term || this.props.submenu.toLowerCase() === 'mmda') {
			this.setInterestRate(prodCd, term);
		}
	},

	
	handleTermSelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let term = $(target).closest('li').attr('data-val') || target.textContent;
		let { prod_code } = this.state;
		
		this.setState({
			term: term,
			interest: ''
		});

		if(prod_code) {
			this.setInterestRate(prod_code, term);
		}
		
	},

	handleTextInputChange(queryAttr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let val = target.value;
		
		this.setState({  // reset prod_code and interest as well
			[queryAttr]: val
		});
	},


	clearStats() {
		this.setState({
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			prodNameOpts: []
		});
	},

	checkValidity() {
		let {
			company_code,
			prod_code,
			amount,
			term,
			interest
		} = this.state;
		
		let allValid = true;
		
		allValid = allValid && (selectRequired(company_code) === true);
		allValid = allValid && (selectRequired(prod_code) === true);
		allValid = allValid && (numberRequired(amount) === true);
		allValid = allValid && (numberRequired(interest) === true);
		if(this.props.submenu.toLowerCase() !== 'mmda') {
			allValid = allValid && (selectRequired(term) === true);
		}

		return allValid;
	},

	setInterestRate(prodCd, term) {
		let { termProp } = convertTermCode(term, this.props.submenu, this.props.optCodeMap);
		
		let opt = this.state.prodNameOpts.find((e) => (e.value === prodCd));
		let interest = 	opt.data[termProp] || opt.data['DS_PRE_TAX'];

		if(interest || interest === 0) {
			this.setState({
				interest: String(interest)
			});
		}
	},

	
	submitProduct(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let { 			
			company_code,
			prod_code,
			amount,
			term,
			interest,
			prodNameOpts,
		} = this.state;
		let { submenu, searchEntries, optCodeMap } = this.props;
		let data;

		let allValid = this.checkValidity();

		if(allValid) {
			
			switch(submenu.toLowerCase()) {
				case('deposit'):
				case('saving'):
				case('mmda'):
				case('house'):
					data = {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label),
						amount: String(amount),
						term_code: String(term),
						interest: String(interest),
						term: String(convertTermCode(term, submenu.toLowerCase(), optCodeMap).termNum)
					};
					break;
			}

			let $target = $(target);
			CommonActions.addMyProduct(data, () => {
				$target.closest('.modal').modal('hide');
			});

		} else {
			this.setState({
				showValidation: true
			});
		}
	}
	
	
});

module.exports = SavingsProductAdder;
