let React				= require('react'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas,
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, numberRequired } = formValidators;


let SavingsHelperSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/helperSectionMixin.jsx'),
	],
	

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {
			
        }
    },
    
    componentDidMount() {

    },

    componentWillUnmount() {

    },

	_getSearchEntries(queryState) {
		let { submenu, searchQuery, optCodeMap } = queryState;
		let entries;
		
		switch(submenu.toLowerCase()) {
			case('deposit'):
				entries = [
					{
						label: '예치금액',
						type: 'number-with-commas',
						id: 'amount',
						unit: '만원',
						validationFn: numberRequired
					},
					{
						label: '예치기간',
						type: 'select',
						id: 'periodCd',
						options: this.getSelectOptions(optCodeMap, 'A0001013'),
						validationFn: selectRequired
					}
				];
				break;
			case('saving'):
				entries = [
					{
						label: '월 저축금액',
						type: 'number-with-commas',
						id: 'amount',
						unit: '만원',
						validationFn: numberRequired
					},
					{
						label: '가입기간',
						type: 'select',
						id: 'periodCd',
						options: this.getSelectOptions(optCodeMap, 'A0001023'),
						validationFn: selectRequired
					}
				];
				break;
			case('mmda'):
				entries = [
					{
						label: '예치금액',
						type: 'number-with-commas',
						id: 'amount',
						unit: '만원',
						validationFn: numberRequired
					}
				];
				break;
			case('house'):
				entries = [
					{
						label: '월 저축금액',
						type: 'number-with-commas',
						id: 'amount',
						unit: '만원',
						validationFn: numberRequired
					},
					{
						label: '납입기간',
						type: 'select',
						id: 'periodCd',
						options: this.getSelectOptions(optCodeMap, 'A0001043'),
						validationFn: selectRequired
					}
				];
				break;
			default:
				entries = [];
		}

		return entries;
	},

	_getCmprCardContents(submenu, comparisonGroup) {

		let cmprCardContents = [];

		switch(submenu.toLowerCase()) {
			case('deposit'):
			case('saving'):
			case('mmda'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						ds_nm, //상품명
						ds_banking_nm, // 은행명
						interest, //금리
						totalamount, //만기수령액
						aftertax, //세후이자
						ds_period
					} = elem;

					let interestUnit = (submenu.toLowerCase() !== 'mmda') ? `% (${ds_period})` : '%';
					
					return	{
						title: ds_nm,
						info: [
							{
								attr: '기관명',
								value: ds_banking_nm,
								colSpan: 3,
								isProvider: true
							},
							{
								attr: '금리',
								value: interest,
								colSpan: 3,
								prefix: '연',
								unit: interestUnit,
								isLeadingInfo: true
							},
							{
								attr: '만기수령액',
								value: numberWithCommas(totalamount),
								unit: '원',
								colSpan: 3
							},
							{
								attr: '세후이자',
								value: numberWithCommas(aftertax),
								unit: '원',								
								colSpan: 3
							}
						]
					}
				});
				break;
			case('house'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						ds_nm, //상품명
						ds_banking_nm, //은행명
						interest, //금리
						amount, //월납입액
						totalamount, //만기수령액
						aftertax, //세후이자
						ds_period
					} = elem;

					let interestUnit = (submenu.toLowerCase() !== 'mmda') ? `% (${ds_period})` : '%';
					
					return	{
						title: ds_nm,
						info: [
							{
								attr: '은행명',
								value: ds_banking_nm,
								colSpan: 4,
								isProvider: true
							},
							{
								attr: '금리',
								value: interest,
								colSpan: 4,
								prefix: '연',
								unit: interestUnit,
								isLeadingInfo: true
							},
							{
								attr: '월 저축금액',
								value: numberWithCommas(amount),
								colSpan: 4,
								unit: '원'
							},
						]
					}
				});
				break;
				
		}

		return cmprCardContents;

	}
	
    
});

module.exports = SavingsHelperSection;
