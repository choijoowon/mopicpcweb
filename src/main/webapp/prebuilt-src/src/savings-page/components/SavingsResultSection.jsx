let React = require('react'),
	ProductCard			= require('../../common/components/ProductCard.jsx'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas;


class ListOpts {
	constructor(label, value) {
		this.label = label;
		this.dataAttrs = { value: value };
	}
}

function getSavingsFilterList() {
	return [
		new ListOpts('전체', '99999999'),
		new ListOpts('1금융권', 'FIRST'),
		new ListOpts('저축은행', 'SECOND'),
		new ListOpts('기타', 'ELSE')
	];
}


let SavingsResultSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/resultSectionMixin.jsx')
	],
	
    propTypes: {

    },

    getDefaultProps: function() {
        return {
			
        };
    },

    getInitialState: function() {
        return {

        }
    },

	componentDidMount() {

	},

    componentWillUnmount: function() {

    },

	_getOptions(submenu, optCodeMap) {
		//  returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element
		
		let {resultState, comparisonState} = this.state;
		
		let { isCmprTabActive, isLoggedIn, comparisonGroup,	currentCmpr } = comparisonState;
		let showingCmpr = (isLoggedIn && isCmprTabActive && comparisonGroup.length > 0);
		let selectedCmpr = comparisonGroup[currentCmpr];
		
		let filterOptions, sortOptions, tooltipElem, resultsElem, helperElem;
		let comparisonContents = undefined;
		
		switch(submenu.toLowerCase()) {
			// 예금
			case('deposit'):
				// 적금
			case('saving'):
				// MMDA
			case('mmda'):
				// 주택청약
			case('house'):
				
				filterOptions = getSavingsFilterList();
				filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = [
					new ListOpts('금리순', '1')
				]; // TODO: modify this to real code

				tooltipElem = undefined;

				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						AC_CODE, // 상품코드
						DS_FN_CODE_NM, // 카테고리 (1금융권, 2금융권, ...),
						BANK_IMG,
						DS_PRE_TAX, // 이율
						DS_NM, // 상품명
						DS_SUMMARY1, //상품설명 1
						DS_SUMMARY2, //상품설명 2
						totalAmount, // 만기수령액
						afterTax, // 세후이자
						DS_INTERNET_YN, // 인터넷가입 여부
						DS_PHONE_YN, // 스마트폰가입 여부
						DS_PROTECT_YN, // 예금자보호 여부
						DS_TAXFREE_YN, // 비과세 여부
						interest_yn, //관심상품 여부

						DS_PERIOD, //기간
						detail_url
						// isNew?
					} = elem;

					let addingInfo;
					let submenuCode;


					switch(submenu.toLowerCase()) {
						case('deposit'):
							submenuCode = 1100;
							addingInfo = `(${DS_PERIOD})`;
							break;
						case('saving'):
							submenuCode = 1200;
							addingInfo = `(${DS_PERIOD})`;
							break;
						case('mmda'):
							submenuCode = 1300;
							addingInfo = '';
							break;
						case('house'):
							submenuCode = 1400;
							addingInfo = `(${DS_PERIOD})`;
							break;
						default:
					}
					
					
					let mainAttrSet = {
						'만기수령액': `${numberWithCommas(totalAmount)} 원`,
						'세후이자': `${numberWithCommas(afterTax)} 원`
					};

					let features = [];
					if(DS_INTERNET_YN === 'Y') {
						features.push('<i class="banking-icon internet"></i><span>인터넷가입</span>');
					}
					if(DS_PHONE_YN === 'Y') {
						features.push('<i class="banking-icon smartphone"></i><span>스마트폰 가입</span>');
					}
					if(DS_PROTECT_YN === 'Y') {
						features.push('<i class="banking-icon save"></i><span>예금자 보호</span>');
					}
					if(DS_TAXFREE_YN === 'Y') {
						features.push('<i class="banking-icon free"></i><span>비과세</span>');
					}

					
					if(showingCmpr) {
						comparisonContents = [
							{
								attr: '만기수령액',
								diff: totalAmount - selectedCmpr.totalamount,
								unit: '원'
							},
							{
								attr: '세후이자',
								diff: afterTax - selectedCmpr.aftertax,
								unit: '원'
							}
						];
					}

					let isInterested = (interest_yn === 'Y'); // 관심상품?

					
					
					return (
						<ProductCard
							key={`savings${AC_CODE}`}
							type='type1'
							category={DS_FN_CODE_NM}
							provider={BANK_IMG}
							leadingInfo={DS_PRE_TAX}
							prefix={'연'}
							unit='%'
							addingInfo={addingInfo}
							productTitle={DS_NM}
							features={features}
							description={DS_SUMMARY1 + ' ' + DS_SUMMARY2}
							mainAttrSet={mainAttrSet}
							targetModalSelector='#product_view_popup'
							detailUrl={detail_url}
							comparisonContents={comparisonContents}
							isInterested={isInterested}
							index={idx}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, submenuCode, AC_CODE, idx)}
							choiceReverse={false}
						/>
					);
				});

				break;


			default:
		}

		return {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem};
	},

	getPreSearchView(submenu) {
		let preSearchViewElem;
		
		switch(submenu.toLowerCase()) {
			case('shilson'):
				break;
			case('car'):
				break;
			default:
				
		}

		return preSearchViewElem;

	}


});

module.exports = SavingsResultSection;
