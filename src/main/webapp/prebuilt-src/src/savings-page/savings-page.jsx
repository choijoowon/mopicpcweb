(function() {

	let React					= require('react'),
		ReactDOM				= require('react-dom'),
		QueryStore				= require('../common/stores/QueryStore'),
		ProductStore			= require('../common/stores/ProductStore'),
		ComparisonStore			= require('../common/stores/ComparisonStore'),		
		CommonActions			= require('../common/actions/CommonActions'),
		getSearchCallback		= require('../common/utils/getSearchCallback'),
		SavingsHelperSection	= require('./components/SavingsHelperSection.jsx'),
		SavingsResultSection	= require('./components/SavingsResultSection.jsx'),
		SavingsProductAdder		= require('./components/SavingsProductAdder.jsx');


	let mainmenu, submenu, isLoggedIn, searchQuery;
    
    if(window.App) {
		({mainmenu, submenu, searchQuery} = window.App.query);
		isLoggedIn = window.App.comparisons.isLoggedIn;
		
		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
    }


//    CommonActions.fetchOptions([
//		'A0001013', 'A0001023', 'A0001043'
//	]);


	if(isLoggedIn) {
		CommonActions.fetchMyProducts(mainmenu, submenu);
	}

	let productAdder = <SavingsProductAdder />;

	let searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away
	

    ReactDOM.render(
		<SavingsHelperSection
			adderModal={productAdder} />,
		document.getElementById('helper'),
		searchCallback
    );

	ReactDOM.render(
		<SavingsResultSection />,
		document.getElementById('results')
    );


})();
