(function() {
	let React			= require('react'),
		ReactDOM		= require('react-dom'),
		CommonActions	= require('../common/actions/CommonActions'),
		SearchSection	= require('./components/SearchSection.jsx');

	
	CommonActions.fetchOptions([
		'B0001041', 3102, 3302, 3406, 4104, 'P0001', 'P0003', 1234
	]);

	ReactDOM.render(
		<SearchSection />,
		document.getElementById('search')
	);
	
})();
