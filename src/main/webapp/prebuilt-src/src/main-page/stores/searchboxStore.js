'use strict';

let EventEmitter = require('events').EventEmitter;
let MainDispatcher = require('../../common/dispatcher/Dispatcher');
let MainConstants = require('../constants/mainConstants');
let CHANGE_EVENT = 'change';

// Define initial query points
let _searchboxState = {
    filledSteps: 0, // steps that are handled so far
    stepSelections: ['', '', {}]
};



function changeSelection(stepNum, option, targetProp) {
    let { filledSteps, stepSelections } = _searchboxState;

    if (stepNum < filledSteps) {
		// eliminate forward selections
		for (let i = stepNum; i < filledSteps; i++) {
			stepSelections[i] = '';
		}
    }
	
	// initialize step 3 selection
    if (stepNum === 2) {
		stepSelections[2] = {};
    }
	

    if(targetProp) {
		let stepSel = stepSelections[stepNum - 1];
		if (stepSel[targetProp] === option || !option) {
			stepSel[targetProp] = '';
			_searchboxState.filledSteps = stepNum - 1;
		} else {
			stepSel[targetProp] = option;
			_searchboxState.filledSteps = stepNum;
		}
    } else {
		if (stepSelections[stepNum - 1] === option) {
			stepSelections[stepNum - 1] = '';
			_searchboxState.filledSteps = stepNum - 1;
		} else {
			stepSelections[stepNum - 1] = option;
			_searchboxState.filledSteps = stepNum;
		}
    }
}


function toggleCheckbox (stepNum, option, targetProp) {
    let { stepSelections } = _searchboxState;
    let checkedOnes = stepSelections[stepNum - 1][targetProp];

    if( !(checkedOnes instanceof Array) ) {
		stepSelections[stepNum - 1][targetProp] = [];
		checkedOnes = stepSelections[stepNum - 1][targetProp];
    }

    // check if option is already selected
    let idx = checkedOnes.indexOf(option);
    
    if (idx > -1) {
		checkedOnes.splice(idx, 1);
    } else {
		checkedOnes.push(option);
    }

    // mark the step filled if at least one is selected
    if (checkedOnes.length > 0) {
		_searchboxState.filledSteps = stepNum;
    } else {
		_searchboxState.filledSteps = stepNum - 1;
    }
    
}




let SearchboxStore = Object.assign({}, EventEmitter.prototype, {
    addChangeListner: function addChangeListner(callback) {
		this.on(CHANGE_EVENT, callback);
    },

    removeChangeListner: function removeChangeListner(callback) {
		this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function emitChange() {
		this.emit(CHANGE_EVENT);
    },

    getState: function getState() {
		return _searchboxState;
    }
});


// Register callback
MainDispatcher.register(function (payload) {
    let action = payload.action;

    switch (action.actionType) {

	// Change selected
    case MainConstants.CHANGE_SELECTION:
		changeSelection(action.stepNum, action.option);
		break;

	// Change checkbox state
    case MainConstants.TOGGLE_CHECKBOX:
		toggleCheckbox(action.stepNum, action.option, action.target);
		break;

	// Change selected on target prop
    case MainConstants.CHANGE_WITH_TARGET:
		changeSelection(action.stepNum, action.option, action.target);
		break;

    default:
		return false;
    }

    SearchboxStore.emitChange();

    return true;
});

module.exports = SearchboxStore;
