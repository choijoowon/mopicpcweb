var React				= require('react'),
    SelectDropdown		= require('../../common/components/SelectDropdown.jsx'),
    CheckboxDropdown	= require('../../common/components/CheckboxDropdown.jsx'),
    TextInput			= require('../../common/components/TextInput.jsx'),
    InsureInputDropdown = require('./InsureInputDropdown.jsx'),
    MainActions			= require('../actions/MainActions');


class SelectOpt {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		if(data) {
			this.data = data;
		}
	}
}


var InputSteps = React.createClass({
    propTypes: {
		stepState: React.PropTypes.object.isRequired,
		stepOptions: React.PropTypes.object.isRequired,
		optCodeMap: React.PropTypes.object.isRequired
    },

    getDefaultProps: function() {

    },

    componentDidMount: function() {

    },

    componentWillUnmount: function() {

    },

    render: function() {
		let { stepState, stepOptions, optCodeMap } = this.props;
		let stepTwoPlaceholder, StepThreeElem, stepThreePlaceholder, stepThreeOptions, stepThreeUnit;

		let { stepSelections } = stepState;
		let { stepOneOptions } = stepOptions;
		let stepTwoOptions = stepOptions[`${stepSelections[0].toLowerCase()}Options`];
		

		StepThreeElem = (
			<SelectDropdown 
				placeholder='-'
				options={['-']}
				disabled={this.isTabToDisable(3)}
				handleSelect={this.handleSelectOptionClick.bind(null, 3, false)}
				selected={''}
				className='main-select-dropdown' />
		);
		
		switch(stepSelections[0].toLowerCase()) {
			case('saving'):
				stepTwoPlaceholder = '어떤 저축상품을 찾으세요?';

				if(stepSelections[1]) {
					stepThreeUnit = '만원';
					
					switch(stepSelections[1].toLowerCase()) {
						case('deposit'):
							stepThreePlaceholder = '예치할 금액을 입력하세요';
							break;
						case('saving'):
							stepThreePlaceholder = '월 납입금액을 입력하세요';
							break;
						case('mmda'):
							stepThreePlaceholder = '예치할 금액을 입력하세요';
							break;
						case('house'):
							stepThreePlaceholder = '월 납입금액을 입력하세요';
							break;
						default:
							stepThreePlaceholder = '-';
					}

					StepThreeElem = (
						<TextInput
							placeholder={stepThreePlaceholder}
							type='number-with-commas'
							handleChange={this.handleTextInputChange.bind(null, 3, 'amount')}
							value={stepSelections[2].amount || ''}
							unit={stepThreeUnit} />
					);
				}
				
				break;
				
			case('loan'):
				stepTwoPlaceholder = '어떤 대출상품을 찾으세요?';

				if(stepSelections[1]) {
					switch(stepSelections[1].toLowerCase()) {
						case('signature'):
							stepThreePlaceholder = '대출받을 금액을 입력해주세요.';
							stepThreeUnit = '만원';
							StepThreeElem = (
								<TextInput
									placeholder={stepThreePlaceholder}
									type='number-with-commas'
									handleChange={this.handleTextInputChange.bind(null, 3, 'amount')}
									value={stepSelections[2].amount || ''}
									unit={stepThreeUnit} />
							);
							break;
						case('secured'):
							stepThreeOptions = [
								{ value: 'B00010809', label: '없음' },
								{ value: 'B00010801', label: '아파트' },
								{ value: 'B00010802', label: '부동산' },
								{ value: 'B00010803', label: '전세' },
								{ value: 'B00010804', label: '월세' },
								{ value: 'B00010805', label: '자동차' },
								{ value: 'B00010806', label: '예적금' },
								{ value: 'B00010807', label: '주식' },
								{ value: 'B00010808', label: '기타' }
							];
							stepThreePlaceholder = '담보유형을 선택해주세요.';
							StepThreeElem = (
								<SelectDropdown 
									placeholder={stepThreePlaceholder}
									options={stepThreeOptions}
									disabled={this.isTabToDisable(3)}
									handleSelect={this.handleSelectOptionClick.bind(null, 3, 'secureType', false)}
									selected={stepSelections[2].secureType || ''}
									className='main-select-dropdown' />
							);
							break;			
						default:
							stepThreeOptions = [];
							stepThreePlaceholder = '-';
							StepThreeElem = (
								<SelectDropdown 
									placeholder={stepThreePlaceholder}
									options={[]}
									disabled={this.isTabToDisable(3)}
									selected={''}
									className='main-select-dropdown' />
							);
					}



				}
				
				break;
				
			case('ins'):
				stepTwoPlaceholder = '어떤 목적으로 상품을 찾으세요?';

				if(stepSelections[1]) {
					let entries;
					
					switch(stepSelections[1].toLowerCase()) {
						case('shilson'):
							entries = [
								{
									label: '생년월일',
									type: 'number',
									placeholder: 'YYMMDD',
									maxlength: '6',
									id: 'birthConvertToAge' // convert to insure age when sending requests
								},
								{
									label: '선택담보',
									type: 'select',
									id: 'dambo1',
									options: this.getSelectOptions(optCodeMap, 3102)
								}
							];
							
							break;
							
						case('life'):
							entries = [
								{
									label: '보험나이',
									type: 'numFixed',
									id: 'age',
									fixedAs: '40'
								}
							];
							
							break;
							
						case('annuity'):
							entries = [
								{
									label: '보험나이',
									type: 'select',
									id: 'age',
									options: this.getSelectOptions(optCodeMap, 3302)
								}
							];
							
							break;
							
						case('car'):
							entries = [
								{
									label: '보험나이',
									type: 'select',
									id: 'age',
									options: this.getSelectOptions(optCodeMap, 3406),
									dependencies: ['age_contract']
								}
							];
							break;
					}
					stepThreePlaceholder = '생년월일과 성별을 입력하세요';
					StepThreeElem = (
						<InsureInputDropdown
							placeholder={ stepThreePlaceholder }
							entries={entries}
							disabled={ this.isTabToDisable(3) }
							handleSelect={this.handleSelectOptionClick}
							handleTextChange={this.handleTextInputChange}
							stepSelections={stepSelections}
						/>
					);

				}
				
				break;
				
			case('card'):
				stepTwoPlaceholder = '어떤 카드를 찾으세요?';

				if(stepSelections[1]) {
					stepThreePlaceholder = '혜택을 최대 3개까지 선택해주세요';
					stepThreeOptions = this.getSelectOptions(optCodeMap, 4104);
					
					StepThreeElem = (
						<CheckboxDropdown
							placeholder={stepThreePlaceholder}
							options={stepThreeOptions}
							disabled={this.isTabToDisable(3)}
							handleSelect={this.handleSelectOptionClick.bind(null, 3, 'benefitsToSplit', true)}
							limitSelection={3}
							selected={stepSelections[2].benefitsToSplit}
							className='main-select-dropdown multi-select' />
					);
				}
				
				break;
			case('p2p'):
				stepTwoPlaceholder = '어떤 목적으로 상품을 찾으세요?';

				if(stepSelections[1]) {
					let attrId = 'none';
					
					switch(stepSelections[1]) {
						case('invest'):
							stepThreeOptions = this.getSelectOptions(optCodeMap, 'P0001');
							stepThreePlaceholder = '투자할 업체를 선택해주세요';
							attrId = 'company_code';
							break;
						case('ploan'):
							stepThreeOptions = this.getSelectOptions(optCodeMap, 'P0003');
							stepThreePlaceholder = '대출 유형을 선택해주세요';
							attrId = 'gubun';
							break;			
						default:
							stepThreeOptions = [];
							stepThreePlaceholder = '-';
					}

					StepThreeElem = (
						<SelectDropdown 
							placeholder={stepThreePlaceholder}
							options={stepThreeOptions}
							disabled={this.isTabToDisable(3)}
							handleSelect={this.handleSelectOptionClick.bind(null, 3, attrId, false)}
							selected={stepSelections[2][attrId] || ''}
							className='main-select-dropdown' />
					);

				}
				
				break;
				
			case('exchange'):
				stepTwoPlaceholder = '국가를 선택해 주세요';

				if(stepSelections[1]) {
					stepThreePlaceholder = '환전할 금액을 입력하세요';
					let nationOpt = stepTwoOptions.find((elem) => elem.value === stepSelections[1]);
					//stepThreeUnit = nationOpt.data.code_nm.replace(`${nationOpt.label} `, '');
					stepThreeUnit = 'KRW(원)';

					StepThreeElem = (
						<TextInput
							placeholder={stepThreePlaceholder}
							type='number-with-commas'
							handleChange={this.handleTextInputChange.bind(null, 3, 'money')}
							value={stepSelections[2].money || ''}
							unit={stepThreeUnit} />
					);
				}

				
				break;
				
			default:
				stepTwoOptions = [];
				stepTwoPlaceholder = '-';
				
				StepThreeElem = (
					<SelectDropdown
						placeholder='-'
						options={['1', '2']}
						disabled={this.isTabToDisable(3)}
						handleSelect={this.handleSelectOptionClick.bind(null, 3, false)}
						selected={''}
						className='main-select-dropdown' />
				);
		}

		
		return (
			<div className='steps-wrapper'>
				<div className='step' >
					<SelectDropdown 
						placeholder='금융상품을 선택해 주세요'
						options={stepOneOptions}
						disabled={this.isTabToDisable(1)}
						handleSelect={this.handleMenuOptionClick.bind(null, 1, false)}
						selected={stepSelections[0]}
						className='main-select-dropdown'
					/>
				</div>
				
				<div className='step' >
					<SelectDropdown 
						placeholder={stepTwoPlaceholder}
						options={stepTwoOptions}
						disabled={this.isTabToDisable(2)}
						handleSelect={this.handleMenuOptionClick.bind(null, 2, false)}
						selected={stepSelections[1]}
						className='main-select-dropdown'
					/>
				</div>

				<div className='step' >
					{StepThreeElem}
				</div>

				<a className='search_btn' onClick={this.sendQuery}>search</a>
			</div>
		);
    },


    isTabToDisable: function (tabNum) {
		return !( this.props.stepState.filledSteps >= tabNum - 1 );
    },

	handleMenuOptionClick: function(stepNum, isMultiSelect, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = $(target).closest('li').attr('data-val');
		val = (val === 0) ? 0 : (val || target.textContent);

		MainActions.changeSelection( stepNum, val );
    },

    handleTextInputChange: function(stepNum, attr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		MainActions.changeWithTarget( stepNum, attr, target.value );
    },

    handleSelectOptionClick: function(stepNum, attr, isMultiSelect, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		evt.stopPropagation();
		
		let val = $(target).closest('li').attr('data-val');
		val = (val === 0) ? 0 : (val || target.textContent);

		
		if(isMultiSelect) {
			MainActions.toggleCheckbox( stepNum, attr, val );
		} else {
			MainActions.changeWithTarget( stepNum, attr, val );
		}

    },

	getSelectOptions(optCodeMap, code) {
		if(optCodeMap[code]) {
			return ( optCodeMap[code].map((e) => new SelectOpt(e.cdName, e.cd)) );
		}
		// TODO: when no map for the code?
		return [];
	},

	sendQuery() {
		let { stepState } = this.props;
		let { filledSteps, stepSelections } = stepState;

		if(filledSteps === 3) {
			let searchQuery = stepSelections[2];

			if(stepSelections[0].toLowerCase() === 'exchange') {
				searchQuery = Object.assign({}, searchQuery, { tr_nation: stepSelections[1], exchange_type: 'TR_BUY_CASH' }); // 거래종류 현찰살때
				
				window.open(`exchange/exchange?${$.param(searchQuery)}`, '_self');
				return true;
			}

			switch(stepSelections[1].toLowerCase()) {
				case('deposit'):
					searchQuery = Object.assign({}, searchQuery, { periodCd: 'A0001010103' }); //예치기간 1년
					break;
				case('saving'):
					searchQuery = Object.assign({}, searchQuery, { periodCd: 'A0001020104' }); //가입기간 1년
					break;
				case('house'):
					searchQuery = Object.assign({}, searchQuery, { periodCd: 'A0001040101' }); //납입기간 1년
					break;
				case('credit'):
				case('check'):
					searchQuery.benefits = searchQuery.benefitsToSplit.join('|');
					delete searchQuery.benefitsToSplit;
					
					searchQuery = Object.assign({}, searchQuery, { company: 'all', fee: 'A', month_sum: '50' }); // 카드사 전체, 연회비 전체, 월평균사용액 50만원
					break;
				case('ploan'):
					searchQuery = Object.assign({}, searchQuery, { company_code: '99999999' }); //업체 전체
					break;
			}



			window.open(`${stepSelections[0]}/${stepSelections[1]}?${$.param(searchQuery)}`, '_self');
		}
	}

    
});

module.exports = InputSteps;
