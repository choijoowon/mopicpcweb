var React = require('react'),
    TextInput = require('../../common/components/TextInput.jsx'),
	SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    SelectList = require('../../common/components/SelectList.jsx'),
    MainActions = require('../actions/MainActions');


var InsureInputDropdown = React.createClass({
	mixins: [require('../../common/utils/click-outside-mixin')],
	
    propTypes: {
		placeholder: React.PropTypes.string,
		entries: React.PropTypes.array,
		disabled: React.PropTypes.bool,
		handleSelect: React.PropTypes.func,
		handleTextChange: React.PropTypes.func,
		stepSelections: React.PropTypes.array
    },

    getDefaultProps: function() {
		return {
			placeholder: '-',
			entries: [],
			disabled: false			
		};
    },

	getInitialState: function() {
        return {
			isOpen: false
        }
    },
    
    
    componentDidMount: function() {
		this.setOnClickOutside('main', this.handleOutsideClick);
    },

    componentWillUnmount: function() {

    },

    render: function() {
		let props = this.props;
		let {
			entries,
			handleSelect,
			handleTextChange,
			stepSelections
		} = this.props;

		let queries = entries.map( (entry) => {
			let queryElem;
			switch(entry.type) {
				case('number'):
				case('number-with-commas'):
				case('text'):
					queryElem = (
						<div className='dropdown-options-sm-form' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<TextInput
								type={entry.type}
								placeholder={entry.placeholder || ' '}
								inputClass='text-input-normal'
								maxlength={entry.maxlength}
								handleChange={handleTextChange.bind(null, 3, entry.id)}
								value={stepSelections[2][entry.id]}
								unit={entry.unit || ''}
							/>
						</div>
					);
					break;
				case('numFixed'):
					queryElem = (
						<div className='dropdown-options-sm-form' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<TextInput
								placeholder=' '
								inputClass='text-input-normal'
								fixed={true}
								value={entry.fixedAs}
								unit={entry.unit || ''} />
						</div>	
					);
					break;
				case('select'):
					queryElem = (
						<div className='dropdown-options-sm-form' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<SelectDropdown
								tabIndex={0}
								placeholder={entry.placeholder || '선택'}
								options={entry.options}
								handleSelect={handleSelect.bind(null, 3, entry.id, false)}
								className={entry.className || 'select-dropdown'}
								selected={stepSelections[2][entry.id]}
							/>
						</div>
					);
					break;

			}
			
			return queryElem;
		});
		
		return (
			<div className='main-select-dropdown' ref='main'
				onClick={this.toggleOpen}>
				<span>
					{ props.placeholder }
				</span>
				<div className={'dropdown-options-sm' + ((!props.disabled && this.state.isOpen) ? ' open' : '')}
					onClick={this.handleInsideClick} >

					{queries}

					<SelectList
						name='sex'
						options={ [
								 { label: '남', dataAttrs: { value: '1' }, icon: 'blank' },
								 { label: '여', dataAttrs: { value: '2' }, icon: 'blank' },
								 ] }
						value={stepSelections[2].sex}
						handleSelect={this.handleSelect} />

					
				</div>
			</div>
		);
    },

	toggleOpen: function(e) {
		if(!this.props.disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			});
		}
	},

	handleOutsideClick: function(evt) {
		this.setState({
			isOpen: false
		});
	},
	

    handleInsideClick: function(e) {
		e.stopPropagation();
    },
    
    handleSelect: function(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		evt.stopPropagation();

		let val = $(target).closest('li').attr('data-value');
		val = (val === 0) ? 0 : (val || target.textContent);
		
		MainActions.changeWithTarget( 3, 'sex', val );
    }
    
});

module.exports = InsureInputDropdown;
