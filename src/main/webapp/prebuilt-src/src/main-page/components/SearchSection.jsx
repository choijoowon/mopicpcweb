var React			= require('react'),
	Api				= require('../../common/utils/api'),
    SearchboxStore	= require('../stores/searchboxStore'),
	stepOptions		= require('../constants/stepOptions'),
    InputSteps		= require('./InputSteps.jsx');


let exchangeOptions = [
	{ code_nm: 'USD(미국 달러)', code_id: 'USD', nationlabel: '미국' },
	{ code_nm: 'JPY(일본 엔)', code_id: 'JPY', nationlabel: '일본' }
]; // initial options before api call

class SelectOpts {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		if(data) {
			this.data = data;
		}
	}
}


function _getStoreState() {
    return SearchboxStore.getState();
}

var SearchSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin')
	],
	
    propTypes: {
		
    },
    
    getInitialState: function() {
		return {
			stepState: _getStoreState(),
			exchangeOptions
		};
    },

    componentDidMount: function() {
		Api.post('/mopic/api/exchange/selectExchangeNation', {})
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					this.setState({
						exchangeOptions: res.result.data
					});
				}
			}.bind(this));
		
		SearchboxStore.addChangeListner(this._onChange);
    },

    componentWillUnmount: function() {
		SearchboxStore.removeChangeListner(this._onChange);
    },

    render: function() {
		let {exchangeOptions} = this.state;
		exchangeOptions = exchangeOptions.map((elem) => new SelectOpts(elem.nationlabel, elem.code_id, elem));
		
		return (
			<div>
				<h4>어떤 <strong>금융상품</strong>을 찾으러 오셨나요?</h4>
				<InputSteps
					stepState={this.state.stepState}
					stepOptions={Object.assign({}, stepOptions, { exchangeOptions })}
					optCodeMap={this.state.queryState.optCodeMap}
				/>
			</div>
		);
    },

    _onChange: function() {
		this.setState({
			stepState: _getStoreState()
		});
    }
});

module.exports = SearchSection;
