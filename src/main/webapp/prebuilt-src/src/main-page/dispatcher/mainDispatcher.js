'use strict';

var Dispatcher = require('flux').Dispatcher;

// Create dispatcher instance
var MainDispatcher = new Dispatcher();

// Convenience method to handle dispatch requests
MainDispatcher.handleAction = function (action) {
    this.dispatch({
        source: 'VIEW_ACTION',
        action: action
    });
};

module.exports = MainDispatcher;