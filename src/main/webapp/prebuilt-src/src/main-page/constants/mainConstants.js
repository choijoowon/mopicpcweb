'use strict';

var keyMirror = require('keymirror');

// Define action constants
module.exports = keyMirror({
    CHANGE_SELECTION: null,
    TOGGLE_CHECKBOX: null,
    CHANGE_WITH_TARGET: null
});
