// Categories and their sub-categories for the search-box
'use strict';

const stepOneOptions = [
	{ label: '저축', value: 'saving', icon: 'icon-01' },
	{ label: '대출', value: 'loan', icon: 'icon-06' },
	{ label: '보험', value: 'ins', icon: 'icon-10' },
	{ label: '카드', value: 'card', icon: 'icon-17' },
	{ label: 'P2P', value: 'p2p', icon: 'icon-20' },
	{ label: '환율', value: 'exchange', icon: 'icon-23' }
];

const savingOptions = [
	{ label: '예금', value: 'deposit', icon: 'icon-02' },
	{ label: '적금', value: 'saving', icon: 'icon-03' },
	{ label: 'MMDA', value: 'mmda', icon: 'icon-04' },
	{ label: '주택청약', value: 'house', icon: 'icon-05' }
];

const loanOptions = [
	{ label: '신용대출', value: 'signature', icon: 'icon-07' },
	{ label: '담보대출', value: 'secured', icon: 'icon-08' }
];

const insOptions = [
	{ label: '실손', value: 'shilson', icon: 'icon-11' },
	{ label: '정기', value: 'life', icon: 'icon-12' },
	{ label: '연금/저축', value: 'annuity', icon: 'icon-13' },
	{ label: '자동차', value: 'car', icon: 'icon-14' }
];

const cardOptions = [
	{ label: '신용카드', value: 'credit', icon: 'icon-18' },
	{ label: '체크카드', value: 'check', icon: 'icon-19' }
];

const p2pOptions = [
	{ label: '투자하기', value: 'invest', icon: 'icon-21' },
	{ label: '대출하기', value: 'ploan', icon: 'icon-22' }
];


module.exports = {
	stepOneOptions,
	savingOptions,
	loanOptions,
	insOptions,
	cardOptions,
	p2pOptions
};
