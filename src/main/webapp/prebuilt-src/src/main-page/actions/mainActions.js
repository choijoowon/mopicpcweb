var MainDispatcher = require('../../common/dispatcher/Dispatcher');
var MainConstants = require('../constants/mainConstants');


// Define action methods
var MainActions = {
    changeSelection: function changeSelection(stepNum, option) {
		MainDispatcher.handleAction({
			actionType: MainConstants.CHANGE_SELECTION,
			stepNum,
			option
		});
    },
    
    toggleCheckbox: function toggleCheckbox(stepNum, target, option) {
		MainDispatcher.handleAction({
			actionType: MainConstants.TOGGLE_CHECKBOX,
			stepNum,
			target,
			option
		});
    },

    changeWithTarget: function changeWithTarget(stepNum, target, option) {
		MainDispatcher.handleAction({
			actionType: MainConstants.CHANGE_WITH_TARGET,
			stepNum,
			target,
			option
		});
    }
    
};

module.exports = MainActions;
