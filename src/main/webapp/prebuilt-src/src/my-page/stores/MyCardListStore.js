'use strict';

var EventEmitter = require('events').EventEmitter;
var myCardListDispatcher = require('./dispatcher/myCardListDispatcher');
var myCardListConstants = require('./constants/myCardListConstants');

var _activeState = {
	activeNum : 0
};

function clickEvent(){

}

var myCardListStore = Object.assign({}, EventEmitter.prototype, {
	addEventListener : function addEventListener(callback){
		this.on(CHANGE_EVENT, callback);
	},
	removeEventListener : function removeEventListener(callback){
		this.removeEventListener(CHANGE_EVENT, callback);
	},
	emitChange : function emitChange(){
		this.emit(CHANGE_EVENT);
	},
	getState : function getState(callback){
		return _activeState;
	},
})


myCardListDispatcher.register(function(payload) {
	var _action = payload.action;
	switch (_action.actionType){
		case myCardListConstants.CLICK:
			clickEvent();
		break;

		default:
		return false;
	}
	myCardListStore.emitChange();

	return true;
});

module.exports = myCardListStore;








