<%@ page contentType = "text/html;charset=utf-8" %>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>모픽 - mainpage</title>
		<!--[if lt IE 9]>
			<script src="/resources/lib/es5-shim/es5-shim.min.js"></script>
			<script src="/resources/lib/es5-shim/es5-sham.min.js"></script>
			<script src="/resources/lib/html5shiv/dist/html5shiv.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="/resources/lib/bootstrap/dist/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/resources/css/common.css" />	
		<link rel="stylesheet" href="/resources/css/my-page-join.css" />
    </head>
    
     <body id="insure">

		<div class="wrapper">

			<div class="header main-h">
				<div class="head_inner">
					<h1 class="logo"><a href="/"></a></h1>
					<div class="top-menu">	
						<ul class="navbar-nav">
							<li><a href="#">로그인 </a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">마이페이지 <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">마이페이지</a></li>
									<li><a href="#">마이페이지</a></li>
									<li><a href="#">마이페이지</a></li>
									<!-- <li class="divider"></li> -->
									<li><a href="#">마이페이지</a></li>
								</ul>
							</li>				
							<li><a href="#">관심상품</a></li>
						</ul>
						<ul class="navbar-nav navbar-right">
							<li><a href="#">이벤트</a></li>
							<li class="dropdown last">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">고객센터 <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">고객센터</a></li>
									<li><a href="#">고객센터</a></li>
									<li><a href="#">고객센터</a></li>
									<!-- <li class="divider"></li> -->
									<li><a href="#">고객센터</a></li>
								</ul>
							</li>
						</ul>
					</div>	
					
					<ul class="top-menu-right">
						<li><a href="#" class="first">계산기</a></li>
						<li><a href="#" class="second">수수료</a></li>
						<li><a href="#" class="third">지점찾기</a></li>
					</ul>

					<!-- 서브메뉴 -->
					<div class="gnb_sub">
						<div class="gnb-list_sub">
							<ul class="gnb-depth2_sub">
								<!-- 보험 서브메뉴 -->
								<li><a href="/ins/shilson">실손보험</a></li>
								<li><a href="/ins/life">정기보험</a></li>
								<li><a href="/ins/annuity">연금/저축보험</a></li>
								<li><a href="/ins/car">자동차보험</a></li>
								<li><a href="/ins/">신규보험상담</a></li>
								<li><a href="/ins/">보유보험점검</a></li>
							</ul>
						</div>
					</div>
					
					<div class="gnb">
						<ul class="gnb-list">
							<li class="gnb-item item1">
								<a href="#" class="title savings"><span class="text">저축</span></a>
								<ul class="gnb-depth2">
									<li><a href="/saving/deposit">예금</a></li>
									<li><a href="/saving/saving">적금</a></li>
									<li><a href="/saving/mmda">MMDA</a></li>
									<li><a href="/saving/house">주택청약</a></li>
								</ul>
							</li>
							<li class="gnb-item item2">
								<a href="#" class="title"><span class="text">대출</span></a>
								<ul class="gnb-depth2">
									<li><a href="#">신용대출</a></li>
									<li><a href="#">담보대출</a></li>
									<li><a href="#">맞춤대출</a></li>
								</ul>
							</li>
							<li class="gnb-item item3">
								<a href="#" class="title insure"><span class="text">보험</span></a>
								<ul class="gnb-depth2 pd22">
									<li><a href="/ins/shilson">실손보험</a></li>
									<li><a href="/ins/life">정기보험</a></li>
									<li><a href="/ins/annuity">연금/저축보험</a></li>
									<li><a href="/ins/car">자동차보험</a></li>
									<li><a href="#">신규보험상담</a></li>
									<li><a href="#">보유보험점검</a></li>
								</ul>
							</li>
							<li class="gnb-item item4">
								<a href="#" class="title"><span class="text">카드</span></a>
								<ul class="gnb-depth2">
									<li><a href="#">신용카드</a></li>
									<li><a href="#">체크카드</a></li>
								</ul>
							</li>
							<li class="gnb-item item5">
								<a href="#" class="title"><span class="text">P2P</span></a>
								<ul class="gnb-depth2">
									<li><a href="#">투자하기</a></li>
									<li><a href="#">대출받기</a></li>
								</ul>
							</li>
							<li class="gnb-item item6 last">
								<a href="#" class="title"><span class="text">환율</span></a>
								<ul class="gnb-depth2">
									<li></li>
									<li></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>

				<div class="gnb-add">
					
				</div>
			</div><!-- /.header -->

		</div><!-- /.wrapper -->

		<div id="my-page" class="container">
			<p>마이페이지 - 가입 상품 페이지 입니다.</p>
			<div class="left-navi">
				
			</div>
			<div id="my-card-content">
				
			</div>
		</div>


		<div class="footer">
			<div class="inner">
				<ul>
					<li class="copy_logo"></li>
					<li><a href="#">모픽이란?</a></li>
					<li><a href="#">서비스약관</a></li>
					<li><a href="#">개인정보취급방침</a></li>
					<li class="last"><a href="#">Yello Financial Group</a></li>
				</ul>

				<ul class="copy">
					<li>서울특별시 영등포구 여의도</li>
					<li>대표이사/사장 : 홍길동</li>
					<li class="last">사업자 등록번호 : 000-00-00000</li><br/>
					<li>고객센터 : 1800-0000</li>
					<li class="last">이메일 문의 : <a href="#"> naver@naver.com</a></li>
				</ul>

			</div>
		</div>
		
		
		
		
		
		<script>
		/*window.App = {
			query: { mainmenu: 'ins', submenu: 'shilson', searchQuery: {}, sorting: ''},
			results: { productData: [], numberByFilter: {} },
			comparisons: { isLoggedIn: true, comparisonGroup: [] },
			optCodes: {}
		};*/
	    </script>

		
	    <script src="/resources/lib/jquery/dist/jquery.min.js"></script>
		<script src="/resources/lib/bootstrap/dist/js/bootstrap.min.js"></script>
	    <script src="/resources/js/common.bundle.js"></script>
	    <!-- <script src="/resources/js/my-page-join.bundle.js"></script> -->
	    <script src="/resources/js/common_menu.js"></script>

		<script>
		$(function () {
			
		});

	    /** 현재 메뉴 GNB active 처리 */
	    var pathname = location.pathname;
	    $('.gnb li a, .gnb_sub li a').each(function(){
	    	if (pathname.indexOf($(this).attr('href')) > -1) {
	    		$(this).addClass('active');
	    	}
	    });
	    
		</script>

			


		
    </body>
</html>
