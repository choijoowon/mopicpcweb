'use strict';

var Dispatcher = require('flux').Dispatcher;
var myCardListDispatcher = new Dispatcher();

myCardListDispatcher.handleAction = function(action){
	this.dispatch({
		source : 'VIEW_ACTION',
		action : action
	});
}

module.exports = myCardListDispatcher;