let React				= require('react'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas,
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, numberRequired } = formValidators;


let LoanHelperSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/helperSectionMixin.jsx'),
	],
	

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {
			
        }
    },
    
    componentDidMount() {

    },

    componentWillUnmount() {

    },

	_getSearchEntries(queryState) {
		let {submenu, searchQuery, optCodeMap} = queryState;
		let entries;
		
		switch(submenu.toLowerCase()) {
			case('signature'):
				entries = [
					{
						label: '대출금액',
						type: 'number-with-commas',
						id: 'amount',
						unit: '만원',
						validationFn: numberRequired
					}
				];
				break;
			case('secured'):
				entries = [
					{
						label: '담보유형',
						type: 'select',
						id: 'secureType',
						options: this.getSelectOptions(optCodeMap, 'B0001081'),
						validationFn: selectRequired
					}
				];
				break;
			default:
				entries = [];
		}

		return entries;
	},

	_getCmprCardContents(submenu, comparisonGroup) {

		let cmprCardContents = [];

		switch(submenu.toLowerCase()) {
			case('signature'):
			case('secured'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						ln_nm, //상품명
						ln_banking_nm, //은행명
						interest, //금리
						amount, //대출금액
						//이자
					} = elem;
					
					return	{
						title: ln_nm,
						info: [
							{
								attr: '기관명',
								value: ln_banking_nm,
								colSpan: 4,
								isProvider: true
							},
							{
								attr: '금리',
								value: interest,
								colSpan: 4,
								prefix: '연',
								unit: '%',
								isLeadingInfo: true
							},
							{
								attr: '대출금액',
								value: `${numberWithCommas(amount)}만원`,
								colSpan: 4
							},
							// {
							// 	attr: '이자',
							// 	value: elem.dambo_shilson_name,
							// 	colSpan: 3
							// }
						]
					};
				});
				break;
				
		}

		return cmprCardContents;

	}
	
    
});

module.exports = LoanHelperSection;
