let React			= require('react'),
	SelectDropdown	= require('../../common/components/SelectDropdown.jsx'),
	TextInput		= require('../../common/components/TextInput.jsx'),
	Api				= require('../../common/utils/api'),
	CommonActions	= require('../../common/actions/CommonActions'),
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, numberRequired } = formValidators;

const submenu_codes = require('../../common/constants/submenu_codes');

const loanMenuCodes = {
	signature: 'B000101',
	secured: 'B000102'
};

const termOpts = [
	{
		value: 'B00010901',
		label: '6개월'
	},
	{
		value: 'B00010902',
		label: '1년'
	},
	{
		value: 'B00010903',
		label: '3년'
	},
	{
		value: 'B00010904',
		label: '5년'
	},
	{
		value: 'B00010905',
		label: '7년'
	},
	{
		value: 'B00010906',
		label: '10년'
	},
	{
		value: 'B00010907',
		label: '15년'
	},
	{
		value: 'B00010908',
		label: '20년'
	},
	{
		value: 'B00010909',
		label: '25년'
	},
	{
		value: 'B00010910',
		label: '30년'
	},
];


function convertTermCode(termCd) {
	let termKey, termNum;
	
	switch(termCd) {
		case('A0001010101'):
			termKey = 'DS_RATE3';
			termNum = '3';
			break;
		case('A0001010102'):
			termKey = 'DS_RATE6';
			termNum = '6';
			break;
		case('A0001010103'):
			termKey = 'DS_RATE12';
			termNum = '12';
			break;
		case('A0001010104'):
			termKey = 'DS_RATE24';
			termNum = '24';
			break;
		case('A0001010105'):
			termKey = 'DS_RATE36';
			termNum = '36';
			break;
		default:
			termKey = 'DS_PRE_TAX'
			termNum = '0';
	}

	return { termKey, termNum };
}



class SelectOpts {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		this.data = data;
	}
}


let LoanProductAdder = React.createClass({
    propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
    },

    getDefaultProps() {
        return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {}
        };
    },
	
	getInitialState: function() {
		let placeholder;

		if(this.props.submenu.toLowerCase() === 'secured') {
			placeholder = '담보유형과 기관명을 선택해주세요.';
		} else {
			placeholder = '기관명을 선택해주세요.';
		}
		
        return {
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			secureType: '',
			companyOpts: [],
			prodNameOpts: [],
			repayType: '',

			prodNamePlaceholder: placeholder
        };
    },
    

    componentDidMount() {
		let category = loanMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/loan/selectLoanBankList', {category})
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;
					let options = ( resultData.map((e) => new SelectOpts(e.cdName, e.cd)) );
					
					this.setState({
						companyOpts: options
					});
				}
			}.bind(this));

		
		// clear when closed
		$('#product_add_popup').on('hidden.bs.modal', this.clearStats);

    },

    componentWillUnmount() {
		
    },

    render() {
		let {
			showValidation,
			companyOpts,
			company_code,
			prodNameOpts,
			interest,
			secureType,
			prod_code,
			term,
			amount,
			prodNamePlaceholder
		} = this.state;
		let { submenu, searchEntries, optCodeMap } = this.props;

		let securityForm;
		if(submenu.toLowerCase() === 'secured') {
			let securityEntry = this.props.searchEntries.find((e) => e.id === 'secureType');
			
			securityForm = (
				<div className="row">
					<div className='col-xs-6'>
						<label className='select-label'>{securityEntry.label || ''}</label>
						<SelectDropdown
							tabIndex={0}
							placeholder={securityEntry.placeholder || '선택'}
							options={securityEntry.options}
							handleSelect={this.handleSecuritySelect}
							className={securityEntry.className || 'select-dropdown'}
							selected={secureType}
							validationFn={showValidation && securityEntry.validationFn}
						/>
					</div>
				</div>
			);
		}

		let companyForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">기관명</label>
					<SelectDropdown
						placeholder='선택'
						options={companyOpts}
						handleSelect={this.handleCompanySelect}
						selected={company_code}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);

		let productNameForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">상품명</label>
					<SelectDropdown
						placeholder={prodNamePlaceholder}
						options={prodNameOpts}
						handleSelect={this.handleProductNameSelect}
						selected={prod_code}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);


		
		let termForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">대출기간</label>
					<SelectDropdown
						placeholder='선택'
						options={termOpts}
						handleSelect={this.handleSelect.bind(null, 'term')}
						className='select-dropdown'
						selected={term}
						validationFn={showValidation && selectRequired}
					/>
				</div>
			</div>
		);

		let amountForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">대출금액</label>
					<TextInput
						type='number-with-commas'
						placeholder=' '
						inputClass='text-input-normal'
						handleChange={this.handleTextInputChange.bind(null, 'amount')}
						value={amount}
						unit='만원'
						validationFn={showValidation && numberRequired}
					/>
				</div>
			</div>
		);


		
		let interestForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">금리</label>
					<TextInput
						type='number'
						placeholder=' '
						inputClass='text-input-normal'
						handleChange={this.handleTextInputChange.bind(null, 'interest')}
						value={interest}
						unit='%'
						validationFn={showValidation && numberRequired}						
					/>
				</div>
			</div>
		);

		
		
		return (
			<div className="modal fade" id="product_add_popup">
				<div className="modal-dialog">
					<div className="modal-content modal-small">

						<a className="close" data-dismiss="modal" aria-label="Close" />
						
						<div className="modal-header">
							<h4 className="modal-title" id="myModalLabel">가입상품 추가하기</h4>
						</div>

						
						<div className="modal-body">
							<div className="product_add">

								{securityForm}
								{companyForm}
								{productNameForm}
								{termForm}
								{amountForm}
								{interestForm}
								
								<span className="txt">(금리를 입력하지 않으실 경우, 등록일 현재 기준 금리로 적용됩니다.)</span>
								<div className="center_btn">
									<a className="btn btn-submit" role="button" onClick={this.submitProduct}>확인</a>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		);
    },

	handleSelect(attr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = $(target).closest('li').attr('data-val') || target.textContent;
		
		this.setState({  // reset prod_code and interest as well
			[attr]: val
		});
	},

	
	handleTextInputChange(attr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = target.value;
		
		this.setState({  // reset prod_code and interest as well
			[attr]: val
		});
	},




	handleProductNameSelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let prodCd = $(target).closest('li').attr('data-val') || target.textContent;
		let interest = this.state.prodNameOpts.find((e) => (e.value === prodCd)).data.LN_RATE_LOW;

		this.setState({
			prod_code: prodCd,
			interest: ( (interest || interest === 0) && String(interest) ) || ''
		});
	},

	
	handleSecuritySelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let secureType = $(target).closest('li').attr('data-val') || target.textContent;
		let { company_code } = this.state;
		
		this.setState({
			secureType,
			prod_code: ''
		});

		if(company_code) {
			this.fetchProductNames(company_code, secureType);
		}
		
	},

	handleCompanySelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let lnBankingCode = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			company_code: lnBankingCode,
			prod_code: '',
			prodNameOpts: [],
			interest: ''
		});

		let { secureType } = this.state;

		if(secureType || this.props.submenu.toLowerCase() !== 'secured') {
			this.fetchProductNames(lnBankingCode, secureType);
		}
	},

	fetchProductNames(company_code, secureType) {
		let lnCode = loanMenuCodes[this.props.submenu.toLowerCase()];
		
		Api.post('/mopic/api/loan/filterProductList', { lnBankingCode: company_code, lnCode, secureType })
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;

					if(resultData && resultData.length > 0) {
						let options = ( resultData.map((e) => new SelectOpts(e.LN_NM, e.LA_CODE, e)) );
						
						this.setState({
							prodNameOpts: options,
							prodNamePlaceholder: '선택'
						});
					} else {
						this.setState({
							prodNamePlaceholder: '해당 조건의 상품이 존재하지 않습니다'
						});
					}
				}
			}.bind(this));
	},


	clearStats() {
		// clear every state but company options
		this.setState({
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			secureType: '',
			prodNameOpts: [],
			prodNamePlaceholder: '담보유형과 기관명을 선택하세요.'
		});
	},

	checkValidity() {
		let {
			company_code,
			prod_code,
			amount,
			term,
			interest,
			secureType
		} = this.state;
		
		let allValid = true;
		
		allValid = allValid && (selectRequired(company_code) === true);
		allValid = allValid && (selectRequired(prod_code) === true);
		allValid = allValid && (selectRequired(term) === true);
		allValid = allValid && (numberRequired(amount) === true);
		allValid = allValid && (numberRequired(interest) === true);

		if(this.props.submenu.toLowerCase() === 'secured') {
			allValid = allValid && (selectRequired(secureType) === true);
		}
		
		return allValid;
	},
	
	submitProduct(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let {
			company_code,
			prod_code,
			amount,
			term,
			interest,
			secureType,
			prodNameOpts
		} = this.state;
		let { submenu, searchEntries } = this.props;
		let data;

		let allValid = this.checkValidity();

		if(allValid) {
			let data = {};
			
			switch(submenu.toLowerCase()) {
				case('secured'):
					data.secureType = secureType;
				case('signature'):
					data = Object.assign({}, data, {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label),
						amount: String(amount),
						term_code: String(term),
						interest: String(interest)
					});
					break;
			}

			let $target = $(target);
			CommonActions.addMyProduct(data, () => {
				$target.closest('.modal').modal('hide');
			});

		} else {
			this.setState({
				showValidation: true
			});
		}
	}
	
	
});

module.exports = LoanProductAdder;
