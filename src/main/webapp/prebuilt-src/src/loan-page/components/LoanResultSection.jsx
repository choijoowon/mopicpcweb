let React = require('react'),
	ProductCard			= require('../../common/components/ProductCard.jsx'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas;


class ListOpts {
	constructor(label, value) {
		this.label = label;
		this.dataAttrs = { value: value };
	}
}

function getLoanFilterList() {
	return [
		new ListOpts('전체', '99999999'),
		new ListOpts('1금융권', 'FIRST'),
		new ListOpts('저축은행', 'SECOND'),
		new ListOpts('기타', 'ELSE')
	];
}

function amountWithUnit(amount) {
	let parsed = '';
	if(amount >= 10000) {
		parsed += (parseInt(amount / 10000) + '억');
	}
	if(amount % 10000 !== 0) {
		parsed += ((amount % 10000) + '만');
	}
	parsed += '원';

	return parsed;
}



let LoanResultSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/resultSectionMixin.jsx')
	],
	
    propTypes: {

    },

    getDefaultProps: function() {
        return {
			
        };
    },

    getInitialState: function() {
        return {

        }
    },

	componentDidMount() {

	},

    componentWillUnmount: function() {

    },

	_getOptions(submenu, optCodeMap) {
		//  returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element
		
		let { resultState, comparisonState } = this.state;
		
		let { isCmprTabActive, isLoggedIn, comparisonGroup,	currentCmpr } = comparisonState;
		let showingCmpr = (isLoggedIn && isCmprTabActive && comparisonGroup.length > 0);
		let selectedCmpr = comparisonGroup[currentCmpr];
		
		let filterOptions, sortOptions, tooltipElem, resultsElem, helperElem;
		let comparisonContents = undefined;
		
		switch(submenu.toLowerCase()) {
			// 신용대출
			case('signature'):
			// 담보대출
			case('secured'):
				filterOptions = getLoanFilterList();
				filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = [
					new ListOpts('저금리순', 'RATE'),
					new ListOpts('최고한도순', 'AMOUNT'),
					new ListOpts('최장기간순', 'TERM'),
				]; // TODO: modify this to real code
				if(submenu.toLowerCase() === 'secured') {
					sortOptions.push(new ListOpts('담보인정비율순', 'LN_RATE'));
				}
				
				

				tooltipElem = undefined;

				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						LA_CODE, // 상품코드
						LN_FN_CODE_NM, // 카테고리 (1금융권, 2금융권, ...),
						BANK_IMG,
						LN_RATE_LOW, //최저연이율

						LN_NM, // 상품명
						LN_SUMMARY1, //상품설명 1
						LN_SUMMARY2, //상품설명 2
						LN_MINIMUM, //대출금액 최소
						LN_MAXIMUM, //대출금액 최대
						LN_NOMAL_DATE_NM, //(최대)대출기간
						
						LN_CLASS1_NM, //신용등급1
						LN_CLASS2_NM, //신용등급2
						LN_RATE, //담보인정비율

						LN_IS_YN, // 온라인가입 여부

						interest_yn, //관심상품 여부
						detail_url
						// isNew?
					} = elem;

					
					let submenuCode;
					let mainAttrSet = {
						'금액': (LN_MINIMUM !== 0) ? `${amountWithUnit(LN_MINIMUM)} ~ ${amountWithUnit(LN_MAXIMUM)}` : (LN_MAXIMUM !== 0 ? `최고 ${amountWithUnit(LN_MAXIMUM)}` : '자세히보기'),
						'기간': LN_NOMAL_DATE_NM ? `최대 ${LN_NOMAL_DATE_NM}` : '-',
					};
					
					switch(submenu.toLowerCase()) {
						case('signature'):
							submenuCode = 2100;
							mainAttrSet['신용등급'] = `${LN_CLASS1_NM} ~ ${LN_CLASS2_NM}`;
							break;
						case('secured'):
							submenuCode = 2200;
							mainAttrSet['담보인정비율'] = LN_RATE ? `${LN_RATE} %` : '-';
							break;
						default:
					}



					let features = [];
					if(LN_IS_YN === 'Y') {
						features.push('<i class="banking-icon internet"></i><span>인터넷가입</span>');
					}
					
					if(showingCmpr) {
						comparisonContents = [
							{
								attr: '최저금리기준 연',
								diff: LN_RATE_LOW - selectedCmpr.ln_rate_low,
								unit: '%'
							}
						];
					}

					let isInterested = (interest_yn === 'Y'); // 관심상품?
						
					return (
						<ProductCard
							key={`loan${LA_CODE}`}
							type='type1'
							category={LN_FN_CODE_NM}
							provider={BANK_IMG}
							leadingInfo={LN_RATE_LOW}
							prefix='연'
							unit='%~'
							productTitle={LN_NM}
							features={features}
							description={LN_SUMMARY1 + '<br />' + LN_SUMMARY2}
							mainAttrSet={mainAttrSet}
							targetModalSelector='#product_view_popup'
							detailUrl={detail_url}
							comparisonContents={comparisonContents}
							isInterested={isInterested}
							index={idx}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, submenuCode, LA_CODE, idx)}
							choiceReverse={true}
						/>
					);
				});

				break;


			default:
		}

		return {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem};
	},

	getPreSearchView(submenu) {
		let preSearchViewElem;
		
		switch(submenu.toLowerCase()) {
			case('shilson'):
				break;
			case('car'):
				break;
			default:
				
		}

		return preSearchViewElem;

	}

    
});

module.exports = LoanResultSection;
