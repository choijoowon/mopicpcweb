(function() {

	let React				= require('react'),
		ReactDOM			= require('react-dom'),
		QueryStore			= require('../common/stores/QueryStore'),
		ProductStore		= require('../common/stores/ProductStore'),
		ComparisonStore		= require('../common/stores/ComparisonStore'),		
		CommonActions		= require('../common/actions/CommonActions'),
		getSearchCallback	= require('../common/utils/getSearchCallback'),
		LoanHelperSection	= require('./components/LoanHelperSection.jsx'),
		LoanResultSection	= require('./components/LoanResultSection.jsx'),
		LoanProductAdder	= require('./components/LoanProductAdder.jsx');

	
	let mainmenu, submenu, isLoggedIn, searchQuery;
    
    if(window.App) {
		({mainmenu, submenu, searchQuery} = window.App.query);
		isLoggedIn = window.App.comparisons.isLoggedIn;
		
		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
    }


//    CommonActions.fetchOptions([
//		'B0001041'
//	]);


	if(isLoggedIn) {
		CommonActions.fetchMyProducts(mainmenu, submenu);
	}

	let productAdder = <LoanProductAdder />;

	let searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away
	

    ReactDOM.render(
		<LoanHelperSection
			adderModal={productAdder}
		/>,
		document.getElementById('helper'),
		searchCallback
    );

	ReactDOM.render(
		<LoanResultSection />,
		document.getElementById('results')
    );


})();
