let ProductStore = require('../../common/stores/ProductStore');

module.exports = {
	_getProductStoreState() {
		return ProductStore.getResults();
	},

	getInitialState() {
        return {
			resultState: this._getProductStoreState()
		};
	},

	componentDidMount() {
		ProductStore.addChangeListner(this._onResultChange);
	},

	componentWillUnmount() {
		ProductStore.removeChangeListner(this._onResultChange);
	},

	_onResultChange() {
        this.setState({
            resultState: this._getProductStoreState()
        });
    }
	
};
