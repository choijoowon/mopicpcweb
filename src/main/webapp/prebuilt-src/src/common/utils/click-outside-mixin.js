const hasAncestor = function (child, parent) {
	if (child.parentNode === parent) {
		return true;
	}
	if (child.parentNode === null) {
		return false;
	}
	return hasAncestor(child.parentNode, parent);
};

module.exports = {

	isNodeOutside: function (nodeOut, nodeIn) {
		if (nodeOut === nodeIn) {
			return false;
		}
		if (hasAncestor(nodeOut, nodeIn)) {
			return false;
		}
		return true;
	},

	isNodeInside: function (nodeIn, nodeOut) {
		return !this.isNodeOutside(nodeIn, nodeOut);
	},

	_onClickMousedown: function() {
		for(let ref in this.clickOutsideHandlers) {
			if (this.refs[ref]) {
				this._mousedownRefs[ref] = true;
			}
		}
	},

	_onClickMouseup: function (event) {
		event = event? event : window.event;
		let target = event.target || event.srcElement;
		
		for(let ref in this.clickOutsideHandlers) {
			let funcs = this.clickOutsideHandlers[ref];

			if (this.refs[ref] && this._mousedownRefs[ref]) {
				if (this.isNodeOutside(target, this.refs[ref])) {
					funcs.forEach(function (fn) {
						fn.call(this, event);
					}.bind(this));
				}
			}
			this._mousedownRefs[ref] = false;
		}
	},

	setOnClickOutside: function (ref, fn) {
		if (!this.clickOutsideHandlers[ref]) {
			this.clickOutsideHandlers[ref] = [];
		}
		this.clickOutsideHandlers[ref].push(fn);
	},

	componentDidMount: function () {
		this.clickOutsideHandlers = {};
		this._didMouseDown = false;
		$(document).bind('mousedown', this._onClickMousedown);
		$(document).bind('mouseup', this._onClickMouseup);
		//document.addEventListener('click', this._onClickDocument);
		this._mousedownRefs = {};
	},

	componentWillUnmount: function () {
		this.clickOutsideHandlers = {};
		//document.removeEventListener('click', this._onClickDocument);
		$(document).bind('mouseup', this._onClickMouseup);
		$(document).bind('mousedown', this._onClickMousedown);
	}
};
