let ComparisonStore = require('../../common/stores/ComparisonStore');

module.exports = {
	_getComparisonStoreState() {
		return ComparisonStore.getComparisonState();
	},

	getInitialState() {
        return {
			comparisonState: this._getComparisonStoreState()
		};
	},

	componentDidMount() {
		ComparisonStore.addChangeListner(this._onComparisonChange);
	},

	componentWillUnmount() {
		ComparisonStore.removeChangeListner(this._onComparisonChange);
	},

	_onComparisonChange() {
        this.setState({
            comparisonState: this._getComparisonStoreState()
        });
    }
	
};
