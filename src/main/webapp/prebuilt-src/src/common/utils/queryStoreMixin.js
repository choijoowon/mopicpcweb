let QueryStore = require('../../common/stores/QueryStore');

module.exports = {
	_getQueryStoreState() {
		return QueryStore.getQueryState();
	},

	getInitialState() {
        return {
			queryState: this._getQueryStoreState()
		};
	},

	componentDidMount() {
		QueryStore.addChangeListner(this._onQueryChange);
	},

	componentWillUnmount() {
		QueryStore.removeChangeListner(this._onQueryChange);
	},

	_onQueryChange() {
        this.setState({
            queryState: this._getQueryStoreState()
        });
    }
	
};
