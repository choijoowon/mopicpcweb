function getYearOfSixDigitDate(dateString) {
	// ex) '160129' on 2016.01.28 -> return 1916
	// ex) '160128' on 2016.01.28 -> return 2016

	let year = Number(dateString.substr(0,2)),
		month = Number(dateString.substr(2,2)),
		day = Number(dateString.substr(4,2));

	let dateNow = new Date();
	let currentYear = dateNow.getFullYear() % 100;

	if(currentYear === year) {
		let currentMonth = dateNow.getMonth() + 1;

		if(currentMonth === month) {
			let currentDate = dateNow.getDate();
			year = (currentDate < day) ? (year + 1900) : (year + 2000);
			
		} else {
			year = (currentMonth < month) ? (year + 1900) : (year + 2000);
		}
		
	} else {
		year = (currentYear < year) ? (year + 1900) : (year + 2000);
	}

	return year;
}


module.exports = {
		numberWithCommas(x) {
			let parts = x.toString().split(".");
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return parts.join(".");
		},

		calcInsureAge(birthDateString) {
			const pattern = /(\d{2})(\d{2})(\d{2})/;
			
			let today = new Date();

			let birthYear = getYearOfSixDigitDate(birthDateString);
			let birthDate = new Date(birthDateString.replace(pattern, `${birthYear}/$2/$3`));


			birthDate.setMonth(birthDate.getMonth() - 6); // insure ages are faster
			
			let age = today.getFullYear() - birthDate.getFullYear();
			let m = today.getMonth() - birthDate.getMonth();
			if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
				age--;
			}
			
			return age;
		},

		getYearOfSixDigitDate
		
	};
