'use strict';

let superagent		= require('superagent'),
//	legacyIESupport = require('superagent-legacyIESupport'),
	Promise			= require('promise');

/**
 * Wrapper for calling an API
 */
var Api = {
    get: function get(url, queryObj) {
		return new Promise(function (resolve, reject) {
			superagent
				.get(url)
//				.use(legacyIESupport)
				.set('Content-Type', 'application/json')			
				.query(queryObj)
				.end(function (err, res) {
					if (res.status === 404) {
						reject();
					} else {
						resolve(res.body);
					}
				});
		});
    },
	post: function post(url, queryObj) {
		return new Promise(function (resolve, reject) {
			superagent
				.post(url)
//				.use(legacyIESupport)
				.set('Content-Type', 'application/json')			
				.send(queryObj)
				.end(function (err, res) {
					if (res.status === 404) {
						reject();
					} else {
						resolve(res.body);
					}
				});
		});
    }
};


module.exports = Api;
