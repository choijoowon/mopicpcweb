let CommonActions = require('../actions/CommonActions'),
	calcInsureAge = require('./numberFilterFunctions').calcInsureAge;


let currBottomCallback;


module.exports = {
	sendQueryWithCount(count, queryState, resultState, isSelectionView) {
		let { mainmenu, submenu } = queryState;
		let query = this.getAcceptableQuery(queryState);

		query.total_count = String(count); // new request when search button clicked

		//console.log(query);

		this.unbindBottomReach();

		if(!isSelectionView) {
			CommonActions.setFetchingState(true); // to show loading spinner
		}

		if(String(count) === '0') {
			CommonActions.setBestState(false); // Set non-best before fetching new results
		}

		CommonActions.fetchResults(mainmenu, submenu, query, () => {
			if(!isSelectionView) {
				CommonActions.setFetchingState(false); // hide loading spinner
				CommonActions.setChangingState(false); // hide spinner on changing filter or sorting
				$('body').removeClass('masked'); // enable scrolling

				// if(String(count) === '0' && resultState.productData.length > 0) {
				// 	this.scrollToResult();
				// }
			}
			
			this.bindLoadMore(queryState, resultState);

		});

	},

	bindLoadMore(queryState, resultState) {
		let { productData, numberByFilter } = resultState;
		let currentFilter = queryState.filter;
		let currentNumData = productData.length;
		let $window = $(window);

		const footerHeight = $('.footer').height();

		let loadMoreAtBottom = function () {
			if ($(window).scrollTop() > $(document).height() - footerHeight - $(window).height()) {
				//console.log('bottom reached!');
				$window.unbind('scroll', currBottomCallback);

				this.sendQueryWithCount(currentNumData, queryState, resultState, false);
			}
		}.bind(this);
		
		// bind new bottom-reach event
		$window.unbind('scroll', currBottomCallback);
		
		if (currentNumData < numberByFilter[currentFilter]) {
			$window.bind('scroll', loadMoreAtBottom);
			currBottomCallback = loadMoreAtBottom;
		}
	},

	getAcceptableQuery(queryState) {
		let {filter, sorting, total_counts, searchQuery} = queryState;
		let query = Object.assign({}, { filter, sorting, total_counts }, searchQuery);

		// delete empty 
		for (let key in query) {
			let val = query[key];


			switch(key) {
				
			case('birthConvertToAge'): // in shilson insure
				query['age'] = String(calcInsureAge(val));
				
				delete query[key];
				
				break;
				
			case('benefitsToSplit'): // in card
				for(let i = 0, l = val.length; i < l; i++) {
					query[`sub_category${i+1}`] = String(val[i]);
				}
				
				delete query[key];
				
				break;

			case('companyJoinToString'): // in card
				query['company_cd'] = val.join('|');
				delete query[key];
				
				break;
				
			default:
				// number '99999999': non-sending code
				// number '0' : sending
				if(!(val || val === 0 ) || val == 99999999) {  // Caution '==': check '99999999' string
					delete query[key];
				} else {
					query[key] = String(val);
				}				
				
			}
		}

		return query;
	},

	sendComparisonQuery(comparisonState, idx = comparisonState.currentCmpr) {
		let { isLoggedIn, isCmprTabActive, isSelectionViewActive, comparisonGroup } = comparisonState;
		
		if( isLoggedIn && isCmprTabActive && comparisonGroup.length > 0) {
			CommonActions.clearResults();
			CommonActions.clearQuery();

			let cmprQuery = comparisonGroup[idx];
			
			for(let key in cmprQuery) {
				let val = cmprQuery[key];
				CommonActions.changeSearchQuery(key, val);
			}
			
			CommonActions.changeFilter(99999999);
			CommonActions.changeSorting('');

			this.setState({}, function() {  // call after state change
				let queryState = this.state.queryState || this.props.queryState;
				let resultState = this.state.resultState || this.props.resultState;
				
				this.sendQueryWithCount(0, queryState, resultState, isSelectionViewActive);
			});
		}
	},

	scrollToResult() {
		let resultSectionOffset = $('.results-wrap').offset().top;

		$('html, body').animate({
			scrollTop: resultSectionOffset
		}, 400);
	},

	unbindBottomReach() {
		$(window).unbind('scroll', currBottomCallback);
	}
};
