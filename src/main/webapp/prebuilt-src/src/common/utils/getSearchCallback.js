let CommonActions = require('../actions/CommonActions');

// if search query has any property return callback that clicks the search button
module.exports = function getSearchCallback(searchQuery) {
	if(Object.keys(searchQuery).length > 0) {
		return function() {
			document.getElementById('search-btn').click();
			CommonActions.validationToShow(false);
		};
	}
	else {
		return () => {};
	}
};
