let getYearOfSixDigitDate = require('./numberFilterFunctions').getYearOfSixDigitDate;

function isNumber(inputString) {
	// including float
	return !/^\s*$/.test(inputString) && !isNaN(inputString);
}


function isValidBirthdate(dateString) {
	if(dateString.length !== 6) { // must be length 8
		return false;
	}

	if( !isNumber(dateString) ) { // must contain only digits
		return false;
	}
	
	let y = getYearOfSixDigitDate(dateString),
		m = Number(dateString.substr(2,2)),
		d = Number(dateString.substr(4,2));

	let daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

	// If evenly divisible by 4 and not evenly divisible by 100,
	// or is evenly divisible by 400, then a leap year
	if ( (!(y % 4) && y % 100) || !(y % 400)) {
		daysInMonth[1] = 29;
	}
	if (m < 1 || m > 12) {  // month must be between 1 and 12
		return false;
	}
	if (d < 1 || d > daysInMonth[m-1]) { // day must between 1 and daysInMonth
		return false;
	}
	
	let birthDate = new Date(`${y}-${m}-${d}`);

	if (birthDate > new Date() ) { // must be past date
		return false;
	}
	
	return true;
}


/* returns true if valid, else an error message */
module.exports = {
	selectRequired(val){
		if(!val) {
			return '필수항목 입니다';
		}
		return true;
	},

	requiredBirthdate(val){
		if(!val) {
			return '필수항목 입니다';
		}
		if (!isValidBirthdate(val)) {
			return '잘못된 생년월일입니다';
		}
		
		return true;
	},

	birthdate(val){
		if(!val) {
			return true;
		}
		if (!isValidBirthdate(val)) {
			return '잘못된 생년월일입니다';
		}
		
		return true;
	},

	numberRequired(val){
		if(!val) {
			return '필수항목 입니다';
		}
		if (!isNumber(val)) {
			return '잘못된 입력값입니다';
		}
		
		return true;
	},

	selectAtLeastOne(val) {
		if(!val || val.length <= 0) {
			return '적어도 한개 이상 선택해주세요';
		}

		return true;
	}
	
};
