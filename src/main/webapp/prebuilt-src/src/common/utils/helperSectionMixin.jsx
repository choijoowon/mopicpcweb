let React				= require('react'),
	ComparisonStore		= require('../../common/stores/ComparisonStore'),
	CommonActions		= require('../../common/actions/CommonActions'),
	ComparisonSection	= require('../../common/components/ComparisonSection.jsx'),
	QuerySection		= require('../../common/components/QuerySection.jsx'),	
    SelectList			= require('../../common/components/SelectList.jsx');


class SelectOpts {
	constructor(label, value, iconCssPrefix) {
		this.label = label;
		this.value = value;

		if(iconCssPrefix) {
			this.icon = iconCssPrefix + value;
		}
	}
}
const _tabOptions = [ '일반검색', '비교검색' ];


module.exports = {
	propTypes: {
		adderModal: React.PropTypes.element
    },
	
	componentDidMount() {
		ComparisonStore.addTurnUpListner(this._onTurnUpEvent);
    },

    componentWillUnmount() {
		ComparisonStore.removeTurnUpListener(this._onTurnUpEvent);
    },

	_onTurnUpEvent() {
		let { comparisonState } = this.state;
		this.sendComparisonQuery(comparisonState);
    },

	getSelectOptions(optCodeMap, code, iconCssPrefix) {
		if(optCodeMap[code]) {
			return ( optCodeMap[code].map((e) => new SelectOpts(e.cdName, e.cd, iconCssPrefix)) );
		}
		// TODO: when no map for the code?
		return [];
	},

	render() {
		let {queryState, resultState, comparisonState} = this.state;
		let {optCodeMap, submenu} = queryState;
		let tabContent, modalElem;

		let searchEntries = this._getSearchEntries(queryState);

		let cmprTabLabel = '비교검색';
		if(comparisonState.comparisonGroup.length > 0) {
			cmprTabLabel += ` (${comparisonState.comparisonGroup.length})`;
		}
		
		if(comparisonState.isCmprTabActive) {
			// 비교검색
			let cmprCardContents = this._getCmprCardContents(submenu, comparisonState.comparisonGroup);
			
			modalElem = React.cloneElement(this.props.adderModal, { submenu, searchEntries,	optCodeMap });
			
			tabContent = <ComparisonSection
							 comparisonState={comparisonState}
							 queryState={queryState}
							 resultState={resultState}
							 adderModal={modalElem}
							 cmprCardContents={cmprCardContents}
						 />;
		} else {
			// 일반검색
			tabContent = <QuerySection
							 queryState={queryState}
							 resultState={resultState}
							 entries={searchEntries}/>;
		}
		
        return (
            <div className='top_search'>
                <SelectList
					className='nav nav-tabs'
                    options={[ { label: '일반검색', className: 'left' }, { label: cmprTabLabel, className: 'right' } ]}
                    value={ comparisonState.isCmprTabActive? cmprTabLabel : '일반검색' }
                    handleSelect={this.handleTabClick} />
				
				<div className='tab-content'>
					{ tabContent }
				</div>
            </div>
        );
    },

	handleTabClick(e) {
		e = e? e : window.event;
		let target = e.target || e.srcElement;
		let isCmpr = target.textContent.indexOf('비교검색') > -1;

		if(this.state.comparisonState.isCmprTabActive !== isCmpr) {
			CommonActions.setCmpTabState(isCmpr);

			if(isCmpr) {
				this._onTurnUpEvent();
			} else {
				CommonActions.clearQuery();
			}
			
			$('body').removeClass('masked');
		}
    }
};
