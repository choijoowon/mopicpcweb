let React				= require('react'),
    CommonActions		= require('../actions/CommonActions'),
	SelectList			= require('../components/SelectList.jsx'),
	ProductCard			= require('../components/ProductCard.jsx'),
	Spinner				= require('../components/Spinner.jsx'),
	numberWithCommas	= require('./numberFilterFunctions').numberWithCommas;

class ListOpts {
	constructor(label, value) {
		this.label = label;
		this.dataAttrs = { value: value };
	}
}


module.exports = {
	componentDidMount() {
		$('#product_view_popup').modal({ show: false });

		
		$(document).bind('scroll', this.toggleBackToTop);
		$(window).bind('resize', this.adjustBackTopBtnPosition);
	},

	componentWillUnmount: function() {
		$(document).unbind('scroll', this.toggleBackToTop);
		$(window).unbind('resize', this.adjustBackTopBtnPosition);
    },
	
	render() {
		let {queryState, resultState} = this.state;
		let optCodeMap = queryState.optCodeMap;

		if(resultState.receivedOnce) {
			if(resultState.productData.length > 0) {
				
				let {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem} = this._getOptions(queryState.submenu, optCodeMap, resultState);

				
				let filterElem, totalNumElem, spinnerWhenChanging;
				if(filterOptions) {
					filterElem = (
						<SelectList
							className='nav nav-tabs-gray'
							options={filterOptions}
							value={ queryState.filter || (filterOptions && filterOptions[0] && filterOptions[0].dataAttrs.value) || 99999999 }
							handleSelect={this.handleFilterChange.bind(null, queryState, resultState)} />
					);
				} else {
					totalNumElem = (
						<span className='total-num'>
							총 {parseInt(resultState.numberByFilter['99999999'])}개
						</span>
					);
				}

				if(resultState.isChangingStuff) {
					spinnerWhenChanging = (
						<div className='text-center' style={{padding: '50px 0'}}>
							<Spinner />
						</div>
					);
				}
				
				return (
					<div className='down_result'>
						<div className='text-center'>
							{filterElem}
						</div>

						<SelectList
							className='top_sort'
							options={sortOptions}
							value={ queryState.sorting || (sortOptions && sortOptions[0] && sortOptions[0].dataAttrs.value) || ''}
							handleSelect={this.handleSortingChange.bind(null, queryState, resultState)}>
							
							{tooltipElem}
							{totalNumElem}
						</SelectList>

						{spinnerWhenChanging}

						
						<div className={'results' + (resultState.isChangingStuff ? ' hide' : '')}>
							{resultsElem}

							{ resultState.isFetching ? (
								<div className={resultState.isFetching ? 'text-center' : 'hide'} style={{padding: '25px 0'}}>
									<Spinner width='40px' height='40px' />
								</div>
							) : undefined }
						</div>

						
						<div className="modal fade" id="product_view_popup" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
							<div className="modal-dialog">
								<div className="modal-content modal-large">
									<button className="close" data-dismiss="modal" aria-label="Close"></button>

									<div className="modal-body">
										상세내용을 가져올 수 없습니다.
									</div>
								</div>
							</div>
						</div>

						{helperElem}

						<div id="back-top" onClick={this.scrollToTop}>
							<span></span>
						</div>

					</div>
				);
			} else {

				
				return (
					<div className='no-result-msg'>
						<p className='text-center'>검색결과가 없습니다.</p>
					</div>
				);

			}

		} else {
			let fetchingMsgElem, maskElem;
			
			if(resultState.isFetching) {
				maskElem = <div className="mask" />;
				
				fetchingMsgElem = (
					<div className="fetching-modal">
						<div className="fetching-dialog">
							<div className="fetching-msg text-center">
								<div>
									<Spinner width='80px' height='80px' />
								</div>
								<div className="message-box">
									<span>데이터를 불러오는 중입니다.<br />잠시만 기다려주세요.</span>
								</div>
							</div>
						</div>
					</div>
				);
			}
			


			/*
			let BIElem = (
				<div className="BIList clearfix">
					<p><span className="emp">예치금액 / 예치기간</span>을 입력해서 나에게 맞는 예금상품을 찾아보세요.</p>
					<ul>
						<li className="biItem"><i className='bi bi-BNK001'></i><span>경남은행</span></li>
						<li className="biItem"><i className='bi bi-BNK002'></i><span>광주은행</span></li>
						<li className="biItem"><i className='bi bi-BNK003'></i><span>KB 국민은행</span></li>
						<li className="biItem"><i className='bi bi-BNK004'></i><span>IBK 기업은행</span></li>
						<li className="biItem"><i className='bi bi-BNK005'></i><span>NH농협은행</span></li>
						<li className="biItem"><i className='bi bi-BNK006'></i><span>대구은행</span></li>
					</ul>
				</div>
			);
			*/

			return this.getPreSearchView(queryState.submenu.toLowerCase(), maskElem, fetchingMsgElem) || (
				<div className="pre-search text-center">
					<p>나에게 맞는 상품을 찾아 보세요.</p>
					
					{ maskElem }
					{ fetchingMsgElem }
				</div>
			);


		}
    },

	_getListOptions(optCodeMap, code) {
		if(optCodeMap[code]) {
			return ( optCodeMap[code].map((e) => new ListOpts(e.cdName, e.cd)) );
		}
		return [];
	},

	_addNumResults(filterOptions) {
		// add number of results to each filter labels
		let {resultState} = this.state;
		
		if (resultState.productData.length > 0) {
			for(let i = 0, l = filterOptions.length; i < l; i++) {
				let option = filterOptions[i];
				let grpCode = option.dataAttrs.value;
				let filterNumResults = resultState.numberByFilter[grpCode];
				option.label += ` (${numberWithCommas(filterNumResults)})`;

				// add css class 'disabled' when no result exists for a filter
				if(filterNumResults === 0) {
					option.className = ['disabled'];
				}
			}
		}
		
		return filterOptions;
	},

	handleFilterChange: function(queryState, resultState, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = $(target).closest('li').data('value');

		let filterNumResults = this.state.resultState.numberByFilter[val];

		if(filterNumResults && filterNumResults !== 0) {
			CommonActions.changeFilter(val);
			CommonActions.setChangingState(true);
			this.sendQueryWithCount(0, queryState, resultState, false);
		}
    },

	handleSortingChange: function(queryState, resultState, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = $(target).closest('li').data('value');
		CommonActions.changeSorting(val);
		CommonActions.setChangingState(true);
		this.sendQueryWithCount(0, queryState, resultState);
    },
    
    handleInterestClick(isInterestedYet, product_type, prod_code, idx, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		evt.stopPropagation();

		if(this.state.comparisonState.isLoggedIn) {
			if(isInterestedYet) {
				CommonActions.removeInterested(prod_code, idx, function() {
					$('#concern-msg').text('관심상품에서 삭제 되었습니다.').addClass('slideIn');
        			setTimeout(function() { $('#concern-msg').removeClass('slideIn'); }, 2000);
				});
			} else {
				CommonActions.addInterested(product_type, prod_code, idx, function() {
					$('#concern-msg').text('관심상품으로 추가 되었습니다.').addClass('slideIn');
        			setTimeout(function() { $('#concern-msg').removeClass('slideIn'); }, 2000);
				});
			}
		} else {
			$('#loginModal').modal('show');
		}
	},

	toggleBackToTop() {
		if(this.state.resultState.productData.length > 0) {
			let scrollTopBtn = $('#back-top');
			

			if ($(window).scrollTop() > 500) {
				scrollTopBtn.fadeIn();
				this.adjustBackTopBtnPosition();
			} else {
				scrollTopBtn.fadeOut();
			}


		}
	},

	adjustBackTopBtnPosition() {
		if(this.state.resultState.productData.length > 0) {
			let scrollTopBtn = $('#back-top');
			
			if($(window).width() >= 1280) {
				let resElem = scrollTopBtn.closest('.down_result');
				let resultRightOffset = ($(window).width() - ($(resElem).offset().left + $(resElem).outerWidth()));
				scrollTopBtn.css({ right: resultRightOffset - 17 - scrollTopBtn.width()});
			} else if($(window).width() >= 1080) {
				scrollTopBtn.css({ right: '50px' });
			} else {
				scrollTopBtn.css({ right: '20px' });
			}
		}
	},

	scrollToTop() {
		$('html,body').animate({
			scrollTop: 0
		}, 500 );
		return false;
	}

};
