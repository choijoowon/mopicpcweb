module.exports = {
	// 저축
	deposit: 1100,
	saving: 1200,
	mmda: 1300,
	house: 1400,

	// 대출
	signature: 2100,
	secured: 2200,
	
	// 보험
	shilson: 3100,
	life: 3200,
	annuity: 3300,
	car: 3400,

	// 카드
	credit: 4100,
	check: 4200,

	//P2P
	invest: 5100,
	ploan: 5200
};
