let keyMirror = require('keymirror');

// Define action constants
module.exports = keyMirror({
    QUERY_CHECKBOX_TOGGLE: null,
	CHANGE_QUERY: null,
    RECEIVE_SEARCH_PRODUCTS: null,
    RECEIVE_MY_PRODUCTS: null,
    REMOVE_MY_PRODUCT: null,
    SELECT_SUBMENU: null,
    CHANGE_FILTER: null,
    CHANGE_SORTING: null,
    RECEIVE_OPT: null,
    CLEAR_QUERY: null,
    CLEAR_RESULTS: null,
    CLEAR_COMPARIONS: null,
    SET_COMPARISON_TAB_STATE: null,
    SET_SELECTION_VIEW_STATE: null,
    SET_CURRENT_COMPARISON: null,
    SET_SHOWING_VALIDATION: null,
    SET_INTERESTED: null,
    SET_NOT_INTERESTED: null,
	SET_RECEIVED_ONCE: null,
	SET_FETCHING_STATE: null,
	SET_CHANGING_STATE: null,
	SET_BEST_PRODUCT: null
});
