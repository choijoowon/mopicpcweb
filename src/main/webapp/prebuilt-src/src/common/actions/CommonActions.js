'use strict';

let Dispatcher		= require('../dispatcher/Dispatcher'),
	Api				= require('../utils/api'),
	ComparisonStore = require('../stores/ComparisonStore');

const submenu_codes	= require('../constants/submenu_codes'),
	  appConstants	= require('../constants/constants');


// Define action methods
var CommonActions = {
    // Toggle checkbox in a query attribute
    toggleCheckbox(queryAttr, checkboxName) {
		Dispatcher.handleAction({
			actionType: appConstants.QUERY_CHECKBOX_TOGGLE,
			queryAttr: queryAttr,
			checkboxName: checkboxName
		});
    },

    changeSearchQuery(queryAttr, value) {
		Dispatcher.handleAction({
			actionType: appConstants.CHANGE_QUERY,
			queryAttr: queryAttr,
			value: value
		});
    },

	fetchOptions(grpCodes, done) {
		let query = {grpCdList: grpCodes.join(', '), detailYn: "Y"};
		
		Api.post('/mopic/api/selectCmnCdGrpDetailList', query)  // TODO: fetch grpList by code list
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					Dispatcher.handleAction({
						actionType: appConstants.RECEIVE_OPT,
						optCodes: res.result.data
					});
					done();
				}
			});
	},

	addOptions(optCodes) {
		Dispatcher.handleAction({
			actionType: appConstants.RECEIVE_OPT,
			optCodes
		});
	},

    fetchResults(mainmenu, submenu, query, done) {
		let url = `/mopic/api/${mainmenu}/select${submenu.charAt(0).toUpperCase() + submenu.slice(1)}List`;

		Api.post(url, query)
			.then(function (res) {
				let { resultCode, best_yn } = res.resultState;

				if(resultCode === "200") {
					Dispatcher.handleAction({
						actionType: appConstants.RECEIVE_SEARCH_PRODUCTS,
						res
					});

					if(best_yn) {
						Dispatcher.handleAction({
							actionType: appConstants.SET_BEST_PRODUCT,
							isBest: best_yn === 'Y'
						});
					}
					
					done();
				} else {
					Dispatcher.handleAction({
						actionType: appConstants.SET_FETCHING_STATE,
						state: false
					});
					Dispatcher.handleAction({
						actionType: appConstants.SET_FETCHING_STATE,
						state: false
					});
					alert('결과를 가져올 수 없습니다. 잠시 후 다시 시도해주세요');
				}

			}, function() {
				// on Error
				Dispatcher.handleAction({
					actionType: appConstants.SET_FETCHING_STATE,
					state: false
				});
				Dispatcher.handleAction({
					actionType: appConstants.SET_FETCHING_STATE,
					state: false
				});
				alert('결과를 가져올 수 없습니다. 잠시 후 다시 시도해주세요');
			});
    },

	fetchMyProducts(mainmenu, submenu, done) {
		Api.post(`/mopic/api/ins/selectMyProductList`, { menu_cd: String(submenu_codes[submenu]) })
		// url is 'ins' for all now.. when different by mainmenu, use ${mainmenu}
			.then(function (res){
				if(res.result.resultCode === 'SUCCESS') {
					//console.log(res.result.data);
					
					Dispatcher.handleAction({
						actionType: appConstants.RECEIVE_MY_PRODUCTS,
						myProducts: res.result.data
					});
					
					done();
				} else {
					// Error
				}
				
			});
	},

	addMyProduct(data, done) {
		Api.post('/mopic/api/common/insertMyProduct', data)
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let product = res.result.data;
					
					Dispatcher.handleAction({
						actionType: appConstants.ADD_MY_PRODUCT,
						product
					});
				
					done();
				}
			});
	},

	removeMyProduct(id, done) {
		Dispatcher.handleAction({
			actionType: appConstants.REMOVE_MY_PRODUCT,
			id
		});
		Api.post('/mopic/api/common/deleteMyProduct', { id })
			.then(function (res){
				if(res.result.resultCode === 'SUCCESS') {
					done();
				}
			});
	},

	clearQuery() {
		Dispatcher.handleAction({
			actionType: appConstants.CLEAR_QUERY
		});
	},
	
	clearResults() {
		$(window).unbind('scroll');
		
		Dispatcher.handleAction({
			actionType: appConstants.CLEAR_RESULTS
		});
    },

	emptyComparisons() {
		Dispatcher.handleAction({
			actionType: appConstants.CLEAR_COMPARIONS
		});
    },
	
    selectSubmenu(mainmenu, submenu) {
		Dispatcher.handleAction({
			actionType: appConstants.SELECT_SUBMENU,
			mainmenu,
			submenu
		});
    },

	changeFilter(changeTo) {
		Dispatcher.handleAction({
			actionType: appConstants.CHANGE_FILTER,
			changeTo
		});
	},

	changeSorting(changeTo) {
		Dispatcher.handleAction({
			actionType: appConstants.CHANGE_SORTING,
			changeTo
		});
	},


	setCmpTabState(isActive) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_COMPARISON_TAB_STATE,
			isActive
		});
	},

	setCmpSelectionView(isActive) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_SELECTION_VIEW_STATE,
			isActive
		});
	},

	changeCurrentCmpr(idx) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_CURRENT_COMPARISON,
			idx
		});
	},

	validationToShow(toShow) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_SHOWING_VALIDATION,
			toShow
		});
	},

	addInterested(product_type, prod_code, idx, done) {
		let data = {
			product_type: String(product_type),
			prod_code: String(prod_code)
		};
		Api.post('/mopic/api/common/insertMyInterest', data)
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					Dispatcher.handleAction({
						actionType: appConstants.SET_INTERESTED,
						idx
					});
					
					done();
				}
			});
	},

	removeInterested(prod_code, idx, done) {
		let data = {
			prod_code: String(prod_code)
		};
		
		Api.post('/mopic/api/common/deleteMyInterest', data)
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					Dispatcher.handleAction({
						actionType: appConstants.SET_NOT_INTERESTED,
						idx
					});
					
					done();
				}
			});
	},

	setReceivedOnce(state) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_RECEIVED_ONCE,
			state
		});
	},
	
	setFetchingState(state) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_FETCHING_STATE,
			state
		});
	},

	setChangingState(state) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_CHANGING_STATE,
			state
		});
	},

	setBestState(isBest) {
		Dispatcher.handleAction({
			actionType: appConstants.SET_BEST_PRODUCT,
			isBest
		});
	}

};

module.exports = CommonActions;
