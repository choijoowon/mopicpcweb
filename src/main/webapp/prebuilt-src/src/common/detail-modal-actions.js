let CommonActions	= require('./actions/CommonActions'),
	checkIE			= require('./utils/checkIE');

let IE_version = checkIE();

if(!IE_version || IE_version >= 9) {
	$("#gray-scroll .gray-scroll-in").mCustomScrollbar({
		setHeight:410,
		theme:"dark-3"
	});
}

$('#btn-interest').click(function() {
	let $this = $(this);
	
	let resultIdx = $this.closest('.modal').data('idx');
	let product_type = $this.data('product_type');
	let prod_code = $this.data('prod_code');
	
	$this.remove();

	CommonActions.addInterested(product_type, prod_code, resultIdx, function() {
		// TODO : 추가되었습니다 + $this.remove
		$('#concern-msg').text('관심상품으로 추가 되었습니다.').addClass('slideIn');
        setTimeout(function() { $('#concern-msg').removeClass('slideIn'); }, 2000);
	});
});
