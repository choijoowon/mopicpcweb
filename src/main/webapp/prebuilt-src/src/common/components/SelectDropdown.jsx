let React		= require('react'),
	ReactDOM	= require('react-dom');






let SelectDropdown = React.createClass({
	mixins: [require('../utils/click-outside-mixin')],
	
    propTypes: {
		handleSelect: React.PropTypes.func,
		placeholder: React.PropTypes.string,
		options: React.PropTypes.array,
		selected: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		]),
		disabled: React.PropTypes.bool,
		className: React.PropTypes.string,
		tabIndex: React.PropTypes.number,
		validationFn: React.PropTypes.oneOfType([
			React.PropTypes.bool,
			React.PropTypes.func
		])
    },

    getDefaultProps: function() {
		return {
			placeholder: '-',
			options: ['-'],
			selected: '',
			disabled: false,
			className: 'select-dropdown',
			tabIndex: 0,
			validationFn: false
		};
    },

	getInitialState: function() {
        return {
			isOpen: false
        };
    },
    
    componentDidMount: function() {
		this.setOnClickOutside('main', this.handleOutsideClick);
		$(this.refs.options).bind('mousewheel DOMMouseScroll', this.handleScroll);
    },

    componentWillUnmount: function() {
		$(this.refs.options).unbind('mousewheel DOMMouseScroll', this.handleScroll);
    },

    render: function() {
		let {
			placeholder,
			options,
			selected,
			disabled,
			className,
			tabIndex,
			validationFn
		} = this.props;
		
		let	labelSelected, isInvalid, validationMsg;
		
		
		let selectOptions = options.map( function(el, idx) {
			let icon, label, optionCssClass;
			
			if( el !== null && typeof el === 'object' ) {
				icon = el.icon ? <i className={'glyph glyph-' + el.icon}></i> : undefined;
				label = el.label || '-';
				optionCssClass = this.getOptionClasses(label);

				if( (el.value === 0  || el.value) && ( String(el.value) == String(selected) ) ) {
					// '==' since data- attr's are read as int
					labelSelected = el.label;
					optionCssClass = this.getOptionClasses(el.value);					
				}
				
			} else if (typeof el === 'string' || el instanceof String) {
				label = el;
				optionCssClass = this.getOptionClasses(label);
			} else {
				return undefined;
			}
			
			return (
				<li key={ `selectOpt${idx}${label}` }
					onClick={this.handleOptionClick}
					data-val={(el.value === 0 ? 0 : (el.value || ''))}
					className={ optionCssClass } >
					<div>{icon}{label}</div>
				</li>
			);
		}.bind(this));

		
		if(validationFn) {
			validationMsg = validationFn(selected);
			isInvalid = (validationMsg !== true);
		}

		
		return (
			<div>
				<div
					className={className + ((!disabled && this.state.isOpen) ? ' open' : '') + (isInvalid ? ' invalid' : '')}
					onClick={this.toggleOpen}
					ref='main'
				>
					<div tabIndex={tabIndex} onKeyDown={this.handleKeySelection}>
						<div className='label-wrapper'>
							<span
								className={selected? 'selected-item' : 'placeholder'} >
								{ labelSelected || selected || placeholder }
							</span>
						</div>
					</div>

					<div ref='options' className={'dropdown-options' + ((!disabled && this.state.isOpen) ? ' open' : '')}>
						<ul>
							{selectOptions}
						</ul>
					</div>
					<span className="carat"></span>
				</div>
				{ isInvalid ? validationMsg : undefined }
			</div>
		);
    },

	handleOptionClick: function(e) {
		this.props.handleSelect(e);
		this.setState({
			isOpen: false
		});
	},

	handleOutsideClick: function(evt) {
		this.setState({
			isOpen: false
		});
	},

	handleScroll: function(evt) {
		evt = evt? evt : window.event;
		evt.stopPropagation();
	},
	
	
	toggleOpen: function(e) {
		if(!this.props.disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			});
		}
	},


    getOptionClasses: function(option) {
		let classes = '';
		
//		if (option.indexOf('준비중') > -1) {
//			classes += ' deactivated';
//		}
		if (option === this.props.selected) {
			classes += ' selected';
		}

		return classes.trim();
    },

	handleKeySelection(evt) {
		evt = evt? evt : window.event;
		let focusElem = evt.target || evt.srcElement;
		let charCode = evt.which || evt.keyCode;

		let $optionsWrapper = $(focusElem).parent().find('.dropdown-options');
		let $options = $optionsWrapper.find('li');
		let currentOptIdx = $options.filter('.selected').index();

		let isCurrentlyOpen  = this.state.isOpen;


		if(charCode === 13 || charCode === 32) {  	// enter or space
			evt.preventDefault();
			
			if(isCurrentlyOpen) {
				if(currentOptIdx === -1) {
					focusElem.click();
				} else {
					$options[currentOptIdx].click();
				}
			} else {
				focusElem.click();
			}

		} else if (charCode === 38 || charCode === 40) {  // up or down arrow
			evt.preventDefault();

			if(isCurrentlyOpen) {
				if(currentOptIdx === -1) {
					$options.eq(0).addClass('selected');
				} else {
					let wrapperScrollTop = $optionsWrapper.scrollTop();
					
					if( (currentOptIdx !== 0) && charCode === 38) {
						/* up arrow at non-first spot */
						let optionPosition = $options.eq(currentOptIdx - 1).position().top;

						if(optionPosition < 0) {
							let scrollTo = wrapperScrollTop + optionPosition;
							$optionsWrapper.scrollTop(scrollTo);
						}

						$options.eq(currentOptIdx).removeClass('selected');
						$options.eq(currentOptIdx - 1).addClass('selected');
						
					} else if( (currentOptIdx !== ($options.length - 1)) && charCode === 40) {
						/* down arrow at non-last spot */
						let optionPosition = $options.eq(currentOptIdx + 1).position().top;
						let optionHeight = $options.eq(currentOptIdx + 1).outerHeight();
						let wrapperHeight = $optionsWrapper.outerHeight();

						if(optionPosition + optionHeight > wrapperHeight) {
							let scrollTo = wrapperScrollTop + optionPosition + optionHeight - wrapperHeight;
							$optionsWrapper.scrollTop(scrollTo);
						}

						$options.eq(currentOptIdx).removeClass('selected');
						$options.eq(currentOptIdx + 1).addClass('selected');
					}
				}
			} else {
				focusElem.click();
			}
			
		} else if ( charCode === 27 ) {  // ESC
			if(isCurrentlyOpen) {
				evt.preventDefault();
				
				if(currentOptIdx !== -1) {
					let { options, selected } = this.props;
					let idxInitialSelection = options.findIndex((el, idx) => {
						if( el !== null && typeof el === 'object' ) {
							return ((el.value === 0  || el.value) && ( String(el.value) == String(selected) ));
						} else if (typeof el === 'string' || el instanceof String) {
							return el === selected;
						} else {
							return false;
						}
					});

					$options.eq(currentOptIdx).removeClass('selected');
					$options.eq(idxInitialSelection).addClass('selected');
				}
				
				this.setState({
					isOpen: false
				});
			}
		} else if ( charCode === 9 ) { // Tab
			if(isCurrentlyOpen) {
				evt.preventDefault();
			}
		}
	}



    
});



module.exports = SelectDropdown;
