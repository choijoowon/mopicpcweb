let React = require('react'),
	ReactTestUtils = require('react-addons-test-utils'),
	numberWithCommas	= require('../utils/numberFilterFunctions').numberWithCommas;
	
// flattenObj : {a: 1, b: 2} => ['a', 1, 'b', 2]
function flattenObj(obj) {
	let arr = [];

	for (let key in obj) {
		arr.push(key);
		arr.push(obj[key]);
	}
	
	return arr;
}


let ProductCard = React.createClass({
    propTypes: {
    	type: React.PropTypes.string,
		category: React.PropTypes.string,
		provider: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number
		]),
		leadingInfo: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number
		]),
		prefix: React.PropTypes.string,
		unit: React.PropTypes.string,
		leadingInfoName: React.PropTypes.string,
		addingInfo: React.PropTypes.string,
		leftTopRight: React.PropTypes.string,
		productTitle: React.PropTypes.string,
		isNew: React.PropTypes.bool,
		features: React.PropTypes.array,
		description: React.PropTypes.string,
		mainAttrSet: React.PropTypes.object,
		detailUrl: React.PropTypes.string,
		targetModalSelector: React.PropTypes.string,
		comparisonContents: React.PropTypes.array,
		index: React.PropTypes.number,
		isInterested: React.PropTypes.bool,
		productImgUrl: React.PropTypes.string,
		progressBar: React.PropTypes.object,
		companyLink: React.PropTypes.string
    },

    getDefaultProps: function() {
        return {
        	type: '',
			category: '',
			provider: '',
			leadingInfo: '',
			prefix: '',
			unit: '',
			leadingInfoName: '',
			addingInfo: '',
			leftTopRight: '',
			productTitle: '',
			isNew: false,
			features: [],
			description: '',
			mainAttrSet: {},
			detailUrl: '',
			targetModalSelector: '',
			comparisonContents: [],
			isInterested: false,
			productImgUrl: '',
			progressBar: { rate: 0, unit: '%'},
			companyLink: ''
        };
    },

    getInitialState: function() {
        return {
	
        }
    },

    componentDidMount: function() {
		$('.content_box').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
    },

    componentWillUnmount: function() {

    },

    render: function() {
		let props = this.props;
		let {
			type,
			category,
			provider,
			leadingInfo,
			prefix,
			unit,
			leadingInfoName,
			addingInfo,
			leftTopRight,
			productTitle,
			headCopy,
			isNew,
			features,
			description,
			mainAttrSet,
			detailUrl,
			comparisonContents,
			isInterested,
			productImgUrl,
			choiceReverse,
			progressBar,
			companyLink
		} = props;

		let labelNew = <font className="label label-danger">NEW</font>;
		let featuresElem = ((features.constructor === Array) && (features.length > 0)) ?
			(
				<ul className="comm">
					{ features.map((elem, idx) => {
						return (
							<li key={idx}
							dangerouslySetInnerHTML={ {__html: elem || '-'} } />
						);
					 })}
				</ul>
			) : undefined;
		// for using change location(benefit icon location) in card page
		let changeFeaturesElem = null;
		let descriptionElem = description ? (
			<div className="hcopy" dangerouslySetInnerHTML={{__html: description}} />
		) : undefined;
		
;
		
		if ( type == 'type3' ){
			changeFeaturesElem = featuresElem;
			featuresElem = null;
		}

		let attrSetFlat = flattenObj(mainAttrSet);
		let mainAttrsElem = ((attrSetFlat.constructor === Array) && (attrSetFlat.length > 0)) ?
			(
				<ul className="get_profit">
					{ attrSetFlat.map((elem, idx) => {
						let oddClass = (idx % 2 === 0) ? 'odd' : undefined;
						if(ReactTestUtils.isElement(elem)) {
							return (
								<li
								className={oddClass}
								key={idx}>
								    {elem}
								</li>
							);
						} else {
							return (
								<li
								className={oddClass}
								key={idx}
    							dangerouslySetInnerHTML={ {__html: elem || '-'} } />
							);
						}
					 })
					}
				</ul>
			) : undefined;

		
		let comparisonElem = undefined;
		if (comparisonContents.length > 0) {
			let total = 0;
			let cmprAttrs = comparisonContents.map((e, idx) => {
				let { attr, diff, unit } = e;
				let diffClass = diff > 0 ? 'plus' : 'minus';
				total += Number(diff);
				diff = Math.abs(diff); // absolute
				diff = Number(diff.toFixed(2)); // round
				diff = numberWithCommas(diff); // comma

				if ( String(diff).substr(-3) == '.' && String(diff).substr(-1) == '0' ){
					diff = diff.substr(0, diff.length-1);
				}
				return (
					<div key={idx} className="choice-item">
						<ul>
							<li><span className="dfn">{attr}</span><span className={diffClass}>{diffClass}</span><span className="diff">{diff}</span><span className="unit">{unit}</span></li>
						</ul>
					</div>
				);
			});

			if ( total > 0 ){
				total = 'gain';
			} else if ( total < 0 ) {
				total = 'loss';
			} else {
				total = '';
			}

			if ( choiceReverse && total !== '' ){
				total = total == 'gain' ? 'loss' : 'gain';
			}

			comparisonElem = (
				<div className={"bottom_view " + total }>
					<div className="left">Pic’s Choice</div>
					<div className="cmpr-box">{cmprAttrs}</div>
				</div>
			);
			total = 0;
		}


		let providerElem = provider ? (
			<i className={`bi bi-${provider}`}></i>
		) : undefined;

		let productImgElem = productImgUrl ? (
			<img src={productImgUrl} />
		) : undefined;


		// let progress = {
		// 	label: '모집금액',
		// 	rate: acheiveRate,
		// 	unit: '%'
		// };
		
		let progressElem;
		if(progressBar.label) {
			let { label, rate, unit } = progressBar;
			
			progressElem = (
				<div className="bar">									
					<div className="progress">
						<div className={ "progress-bar" + ((rate < 51) ? ' under' : '') } role="progressbar" style={{width: `${rate}%`}}>
							{label}<span>{`${rate}%`}</span>
						</div>
					</div>
				</div>
			);
		}
		

		

		//TODO : when provider not provided?
		


		return (
			<div className={"content_box_out" + (type? ` ${type}` : '')}>
				
				<div className="part">{category}</div>
				<div className="content_box clearfix" id="shilson" onClick={this.handleProductClick}>
					<div className="innerWrap">
						<div className="product_summary">
							<div className="left_logo">
								{providerElem}
								{productImgElem}
							</div>
							<div className="left_top_txt">{leftTopRight}</div>
							<div className="left_small_txt" dangerouslySetInnerHTML={{__html: leadingInfoName}} />
							<div className="product_benefit">
								<span className="adding-info">{addingInfo}</span>
								<span className="prefix">{prefix}</span>
								<span className="highlight">{leadingInfo}</span>
								<span className="unit">{unit}</span>
							</div>
						</div>
						<div className="detail_info">
							<div className={'star' + (isInterested ? ' select' : '')}>
								<a onClick={this.props.handleInterestClick}></a>
							</div>
							<div className="description">
								<h4>{productTitle}{isNew ? labelNew : undefined }</h4>

								{featuresElem}
								{descriptionElem}
								
								{progressElem}
								
							</div>

							{mainAttrsElem}
							{changeFeaturesElem}
						</div>
					</div>
					{comparisonElem}
				</div>

			</div>

		);
    },

	handleProductClick(e) {
		let {detailUrl, targetModalSelector, companyLink} = this.props;

		if(targetModalSelector) {
			let targetModalElem = $(targetModalSelector);

			targetModalElem.data('idx', this.props.index);
			targetModalElem.removeData('bs.modal');

			targetModalElem.addClass('hide');
			
			targetModalElem
				.modal()
				.find('.modal-body')
				.load(detailUrl, (responseText, textStatus) => {
					
					if( textStatus === 'success' || textStatus === 'notmodified' ) {
						targetModalElem.removeClass('in');
						targetModalElem.removeClass('hide');
						
						$.ajax({
							dataType: "script",
							cache: true,
							url: '/resources/js/detail-modal-actions.bundle.js'
						})

						setTimeout(() => { targetModalElem.addClass('in'); }, 50);
					} else {
						targetModalElem.modal('hide');
					}
				});
		} else {
			if ( companyLink ) { 
				window.open(companyLink, '_blank');
			}
		}
	},


    
});

module.exports = ProductCard;
