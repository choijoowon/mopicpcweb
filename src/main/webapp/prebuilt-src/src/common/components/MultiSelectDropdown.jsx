const React = require('react');


function compareArrays(arr1, arr2) {
	// returns whether two arrays have same elements or not
	return ($(arr1).not(arr2).length === 0) && ($(arr2).not(arr1).length === 0);
}


var MultiSelectDropdown = React.createClass({
	mixins: [require('../utils/click-outside-mixin')],
	
    propTypes: {
		handleSelect: React.PropTypes.func,
		placeholder: React.PropTypes.string,
		options: React.PropTypes.array,
		selected: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.array
		]),
		disabled: React.PropTypes.bool,
		className: React.PropTypes.string,
		validationFn: React.PropTypes.oneOfType([
			React.PropTypes.bool,
			React.PropTypes.func
		]),
		handleSelectAll: React.PropTypes.func,
		limitSelection: React.PropTypes.oneOfType([
			React.PropTypes.bool,
			React.PropTypes.number
		])
    },

    getDefaultProps: function() {
		return {
			placeholder: '-',
			options: ['-'],
			selected: [],
			disabled: false,
			className: 'multi-select-dropdown',
			validationFn: false,
			limitSelection: false
		};
    },

	getInitialState: function() {
        return {
			isOpen: false,
			reachedLimit: false
        };
    },
    
    componentDidMount: function() {
		this.setOnClickOutside('main', this.handleOutsideClick);
		$(this.refs.options).bind('mousewheel DOMMouseScroll', this.handleScroll);
    },

    componentWillUnmount: function() {
		$(this.refs.options).unbind('mousewheel DOMMouseScroll', this.handleScroll);
    },

    render: function() {
		let {
			placeholder,
			options,
			selected,
			disabled,
			className,
			validationFn,
			handleSelectAll,
			limitSelection
		} = this.props;
		
		let	labelSelected, isInvalid, validationMsg, selectAllElem, reachedLimitElem;
		
		labelSelected = [];
		
		let selectOptions = options.map( function(el, idx) {
			let icon, label, optionCssClass;


			// case option is in the form of object -> use value as option code
			if( el !== null && typeof el === 'object' ) {
				icon = el.icon ? <i className={el.icon}></i> : undefined;
				label = el.label || '-';

				if( (el.value === 0  || el.value)) {
					// regard number '0' as truthy
					if( selected && ( selected.indexOf(String(el.value)) > -1 ) ) {
						labelSelected.push( { label: String(label), val: el.value } );
					}
					optionCssClass = this.getOptionClasses(el.value);

				} else {
					if( selected && ( selected.indexOf(String(label)) > -1 ) ) {
						labelSelected.push( { label: String(label), val: String(label) } );
					}
					optionCssClass = this.getOptionClasses(label);
				}
				
			// case option is in the form of string -> label functions as option code
			} else if (typeof el === 'string' || el instanceof String) {
				label = el;
				optionCssClass = this.getOptionClasses(label);

				if( selected && ( selected.indexOf(String(label)) > -1 ) ) {
					labelSelected.push( { label: String(label), val: String(label) } );
				}
				
			} else {
				return undefined;
			}
			
			return (
				<li key={ `selectOpt${idx}${label}` }
					onClick={this.handleOptionClick}
					data-val={(el.value === 0 ? 0 : (el.value || ''))}
					className={ optionCssClass } >
					<a>{icon}{label}</a>
				</li>
			);
		}.bind(this));


		if(validationFn) {
			validationMsg = validationFn(selected);
			isInvalid = (validationMsg !== true);
		}

		if(!limitSelection && handleSelectAll) {
			let entireValues = options.map((e) => e.value);
			let selectedValues = labelSelected.map((e) => e.val);
			
			selectAllElem = (
				<div className="all-select">
					<input
						type="checkbox" id="select-all"
						checked={compareArrays(entireValues, selectedValues)}
						onChange={this.handleSelectAllClick} />
					<label htmlFor="select-all">전체선택</label>
				</div>
			);
		}

		if(limitSelection && selected && (selected.length > 0) && this.state.reachedLimit) {
			reachedLimitElem = (
				<div className='warning' style={{borderBottom: '1px solid #eceded', color: '#fa365c'}}>
					최대 {limitSelection}개까지 선택 할 수 있습니다
				</div>
			);
		}
		

		labelSelected = (
			<ul className='top-sort-list'>
				{labelSelected.map((elem, idx) => {
					return (
						<li data-val={elem.val}
						    key={JSON.stringify(elem)}>
						    <a onClick={this.handleOptionClick}>{elem.label}<span className='delete-btn'>x</span></a>
						</li>
					);
				 })}
			</ul>
		);

		
		return (
			<div>
				<div
					className={className + ((!disabled && this.state.isOpen) ? ' open' : '') + (isInvalid ? ' invalid' : '')}
					onClick={this.toggleOpen}
					ref='main'
				>
					
					<div className="select-top">
						{ (selected && (selected.length && selected.length > 0))? labelSelected : placeholder }
						<div className="carat" />
					</div>
					<div ref='options' className="card-dropdown-options">
						{selectAllElem}
						{reachedLimitElem}
						<ul className="benefit">
							{ selectOptions }
						</ul>
					</div>{/* card-dropdown-options */}
					
				</div>
				{ isInvalid ? validationMsg : undefined }
			</div>

		);
    },
	


	handleOptionClick: function(e) {
		e = e? e : window.event;
		let target = e.target || e.srcElement;
		
		e.stopPropagation();

		let { limitSelection, handleSelect, selected } = this.props;

		if(limitSelection) {
			let targetVal = $(target).closest('li').attr('data-val');
			targetVal = (targetVal === 0) ? 0 : (targetVal || target.textContent);

			if(selected) {
				if( (selected.indexOf(targetVal) > -1) || (selected.length < limitSelection) ) {
					this.setState({
						reachedLimit: false
					});
					handleSelect(e);
				} else {
					this.setState({
						reachedLimit: true
					});
				}
			} else {
				handleSelect(e);
			}


		} else {
			handleSelect(e);
		}
	},

	handleSelectAllClick: function(e) {
		this.props.handleSelectAll(e);
		this.setState({
			isOpen: true
		}); // stopPropagation does not work :(
	},
	

	handleOutsideClick: function(e) {
		this.setState({
			isOpen: false
		});
	},

	handleScroll: function(evt) {
		evt = evt? evt : window.event;
		evt.stopPropagation();
	},
	
	
	toggleOpen: function(e) {
		if(!this.props.disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			});
		}
	},

    getOptionClasses: function(option) {
		let classes = '';
		
		//		if (option.indexOf('준비중') > -1) {
		//			classes += ' deactivated';
		//		}
		if ( this.props.selected.indexOf( String(option) ) > -1 ) {
			classes += ' selected';
		}

		return classes.trim();
    }



    
});

module.exports = MultiSelectDropdown;
