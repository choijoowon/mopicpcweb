let React				= require('react'),
	CommonActions		= require('../actions/CommonActions'),
	ComparisonCard		= require('./ComparisonCard.jsx'),
	PurchasedProduct	= require('./PurchasedProduct.jsx');




let ComparisonSection = React.createClass({
	mixins: [
		require('../../common/utils/sendQueryMixin')
	],

	
    propTypes: {
		comparisonState: React.PropTypes.object.isRequired,
		queryState: React.PropTypes.object.isRequired,
		resultState: React.PropTypes.object.isRequired,
		adderModal: React.PropTypes.object.isRequired,
		cmprCardContents: React.PropTypes.array.isRequired
    },

    getDefaultProps() {
        return {

        };
    },

	getInitialState() {
        return {

        };
    },
	
    componentDidMount() {
		if(this.props.comparisonState.isBest) {
			$('.results-wrap').addClass('comparison-best');
		} else {
			$('.results-wrap').removeClass('comparison-best');
		}
    },

    componentWillUnmount() {
		$('.results-wrap').removeClass('comparison-best');
    },

	componentDidUpdate() {
		if(this.props.comparisonState.isBest) {
			$('.results-wrap').addClass('comparison-best');
		} else {
			$('.results-wrap').removeClass('comparison-best');
		}
	},

    render() {
		let { isLoggedIn, comparisonGroup, currentCmpr, isSelectionViewActive, isBest } = this.props.comparisonState;
		let { adderModal, cmprCardContents } = this.props;
		
		
		if(isLoggedIn) {
			
			const emptyComparison = (
				<div className="comparison_view">
					<div className="comparison_txt">
						<strong>비교할 상품</strong>을 선택해 주세요. mopic이 더 좋은 상품을 추천해 드립니다
					</div>
					<div className="view_comparison" onClick={this.openSelectionTab}>
						<span className="msg">비교할 상품 보기</span><span></span>
					</div>
				</div>
			);
			
			
			if(!isSelectionViewActive) {  // normal view

				if(cmprCardContents) {
					if(!cmprCardContents instanceof Array) {
						throw new Error('cmprCardContents should be an array of product objects!');
					}

					if(cmprCardContents.length > 0) {
						let indicators = cmprCardContents.map((elem, idx) => (
							<li key={`carouselIndicator${idx}`}
								className={(idx === currentCmpr)? 'active' : ''}
								onClick={this.setCurrentCmpr.bind(null, idx)} />
						));
						
						let items = cmprCardContents.map((elem, idx) => {
							return (
								<ComparisonCard
									key={`card${idx}${elem.title}`}
									productTitle={elem.title}
									productInfo={elem.info}
									style={elem.style}
								/>
							);
						});

						let leftChevronElem = (comparisonGroup.length > 1) ? (
							<button onClick={this.setCurrentCmpr.bind(null, currentCmpr-1)} className="left carousel-control" role="button">
								<span className="glyphicon-chevron-left" aria-hidden="true"></span>
							</button>
						) : undefined;
						
						let rightChevronElem = (comparisonGroup.length > 1) ? (
							<button onClick={this.setCurrentCmpr.bind(null, currentCmpr+1)} className="right carousel-control" role="button">
								<span className="glyphicon-chevron-right" aria-hidden="true"></span>
							</button>
						) : undefined;

						let bestIndicatorElem = isBest ? (
							<div className="best-product">
								고객님께서 등록하신 상품이 최상의 상품입니다.
							</div>
						) : undefined;
						
						
						return (
							<div className="comparison_container">
								<div className="cmp_set">
									<p className="txt">입력하신 상품을 기준으로 비교하였습니다.</p>
									<button className="set_btn" onClick={this.openSelectionTab}>비교상품설정</button>
								</div>
								
								<div id="myCarousel" className="carousel slide">
									<button onClick={this.removeProduct.bind(null, currentCmpr)} className="btn_close" />
									<ol className="carousel-indicators">
										{indicators}
									</ol>
									<div className="carousel-inner" role="listbox">
										<div className="item active">
											{items[currentCmpr]}
										</div>
									</div>
								</div>
								
								
								{ leftChevronElem }
								{ rightChevronElem }
								
								{ bestIndicatorElem }
								
								
							</div>
						);
					} else { // when comparisonGroup length: 0
						
						return emptyComparison;
						
					} // end statement: comparisonGroup length > 0

					
				} else {
					// if comparisonGroup falsy
					return emptyComparison;
				} // end statement: comparisonGroup truthy


				
			} else {  // if selection view active

				let purchasedElem = cmprCardContents.map((elem, idx) => {
					let provider = elem.info.find( (e) => e.isProvider );
					let leadingInfo = elem.info.find( (e) => e.isLeadingInfo ); 

					return (
						<PurchasedProduct
							key={`pchsd${idx}${elem.title}`}
							provider={ provider }
							title={ {attr: '상품명', value: elem.title.substring(0, 12)} }
							leadingInfo={leadingInfo}
							handleRemoveBtnClick={this.removeProduct.bind(null, idx)} />
					);
					// TODO: handle long title
				}); 

				
				return (
					<div>
						<div className="comparison_txt">
							<strong>비교할 상품</strong>을 선택해 주세요. mopic이 더 좋은 상품을 추천해 드립니다.
						</div>
						
						<div className='selection-wrapper'>
							<div className='selection-contents'>
								<div className="cmp-container">
									<a className={(comparisonGroup && comparisonGroup.length > 0) ? 'product-adder' : 'cmp-empty'} data-toggle="modal" data-target="#product_add_popup"><span>가입상품을 추가해 주세요</span></a>

									{purchasedElem}
									
								</div>
							</div>
							
							<div className='selection-mask' onClick={this.closeSelectionTab}>
								<div className="mask-close"></div>
							</div>
						</div>

						{adderModal}

						
					</div>
				);

			} // end statement: selectionView active

			
		} else { // if not logged in
			return (
				<div className='txt-center' id='comparison'>
					로그인 하시면 <b>비교검색</b>으로 더 좋은 상품을 추천해 드립니다.
					<a className="btn btn-submit-round" data-toggle="modal" data-target="#loginModal" href="#" role="button">로그인</a>
				</div>
			);
		}
    },

	openSelectionTab(e) {
		CommonActions.setCmpSelectionView(true);
		$('body').addClass('masked');
	},

	closeSelectionTab(e) {
		CommonActions.setCmpSelectionView(false);
		$('body').removeClass('masked');
	},

	setCurrentCmpr(toIdx, e) {
		let { comparisonState } = this.props;
		let { currentCmpr, comparisonGroup } = comparisonState;
		let cmprLen = comparisonGroup.length;

		if(toIdx >= cmprLen) {
			toIdx %= cmprLen;
		} else if(toIdx < 0) {
			toIdx += cmprLen;
		}
		
		if(this.state.currentCmpr !== toIdx) {
			CommonActions.changeCurrentCmpr(toIdx);
			this.sendComparisonQuery(comparisonState, toIdx);
		}
	},

	removeProduct(idx) {
		let { comparisonState } = this.props;
		let id = comparisonState.comparisonGroup[idx].id;

		CommonActions.clearResults();
		CommonActions.clearQuery();

		CommonActions.removeMyProduct(id, function() {
			let { comparisonGroup, currentCmpr } = this.props.comparisonState;
			let cmprLen = comparisonGroup.length;

			if(cmprLen > 0) {
				if(currentCmpr === cmprLen) {
					CommonActions.changeCurrentCmpr(currentCmpr - 1);
				}
				this.sendComparisonQuery(this.props.comparisonState);
			}
		}.bind(this));
	}
	
    
});

module.exports = ComparisonSection;
