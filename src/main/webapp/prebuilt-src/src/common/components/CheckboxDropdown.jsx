var React = require('react');


var CheckboxDropdown = React.createClass({
	mixins: [require('../utils/click-outside-mixin')],
	
    propTypes: {
		handleSelect: React.PropTypes.func.isRequired,
		placeholder: React.PropTypes.string,	
		options: React.PropTypes.array,
		selected: React.PropTypes.array,
		disabled: React.PropTypes.bool,
		className: React.PropTypes.string,
		limitSelection: React.PropTypes.oneOfType([
			React.PropTypes.bool,
			React.PropTypes.number
		])
    },

    getDefaultProps: function() {
		return {
			placeholder: '-',
			options: ['-'],
			selected: [],
			disabled: false,
			className: 'select-dropdown',
			limitSelection: false
		};
    },

	getInitialState: function() {
        return {
			isOpen: false
        }
    },
    
    componentDidMount: function() {
		this.setOnClickOutside('main', this.handleOutsideClick);
		$(this.refs.options).bind('mousewheel DOMMouseScroll', this.handleScroll);
    },

    componentWillUnmount: function() {
		$(this.refs.options).unbind('mousewheel DOMMouseScroll', this.handleScroll);
    },

    render: function() {
		let {
			placeholder,
			options,
			selected,
			disabled,
			className,
			handleSelect
		} = this.props;

		let	labelSelected = [];

		
		let selectOptions = options.map( (el, idx) => {
			let label, optionCssClass;
			
			// case option is in the form of object -> use value as option code
			if( el !== null && typeof el === 'object' ) {
				label = el.label || '-';

				if( (el.value === 0  || el.value)) {
					// regard number '0' as truthy
					if( selected && ( selected.indexOf(String(el.value)) > -1 ) ) {
						labelSelected.push( { label: String(label), val: el.value } );
					}
					optionCssClass = this.getOptionClasses(el.value);

				} else {
					if( selected && ( selected.indexOf(String(label)) > -1 ) ) {
						labelSelected.push( { label: String(label), val: String(label) } );
					}
					optionCssClass = this.getOptionClasses(label);
				}
				
				// case option is in the form of string -> label functions as option code
			} else if (typeof el === 'string' || el instanceof String) {
				label = el;
				optionCssClass = this.getOptionClasses(label);

				if( selected && ( selected.indexOf(String(label)) > -1 ) ) {
					labelSelected.push( { label: String(label), val: String(label) } );
				}
				
			} else {
				return undefined;
			}

			return (
				<li key={ 'opt' + idx + el }
					onClick={this.handleOptionClick}
					data-val={(el.value === 0 ? 0 : (el.value || ''))}
					className={ optionCssClass } >
					<div>
						{label}
					</div>
				</li>
			);
		});


		let labelSelectedText = labelSelected.map((el) => el.label).join(', ');
		
		return (
			<div className={className}
				onClick={this.toggleOpen}
				ref='main' >
				<span className='display' style={ selected.length > 0 ? {fontSize: '13px'} : {color: '#999'} } >
					{ labelSelectedText || placeholder }
				</span>
				<div ref='options' className={'dropdown-options-ck' + ((!disabled && this.state.isOpen) ? ' open' : '')} >
					<ul>
						{selectOptions}
					</ul>
				</div>
			</div>
		);
    },

	handleOutsideClick: function(evt) {
		this.setState({
			isOpen: false
		});
	},

	handleOptionClick: function(e) {
		e = e? e : window.event;
		let target = e.target || e.srcElement;
		
		e.stopPropagation();

		let { limitSelection, handleSelect, selected } = this.props;

		if(limitSelection) {
			let targetVal = $(target).closest('li').attr('data-val');
			targetVal = (targetVal === 0) ? 0 : (targetVal || target.textContent);

			if(selected) {
				if( (selected.indexOf(targetVal) > -1) || (selected.length < limitSelection) ) {
					handleSelect(e);
				}
			} else {
				handleSelect(e);
			}

		} else {
			handleSelect(e);
		}
	},

	handleScroll: function(evt) {
		evt = evt? evt : window.event;
		evt.stopPropagation();
	},
	

	toggleOpen: function(e) {
		if(!this.props.disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			});
		}
	},

    getOptionClasses: function(option) {
		let classes = '';
		
		if (this.props.selected.indexOf(String(option)) > -1) {
			classes += ' selected';
		}

		return classes.trim();
    }



});

module.exports = CheckboxDropdown;
