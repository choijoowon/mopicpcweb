let React		= require('react'),
	ReactDOM	= require('react-dom');

var CheckboxGroup = React.createClass({
	propTypes: {
		handleSelect: React.PropTypes.func,
		options: React.PropTypes.array,
		selected: React.PropTypes.array,
		//disabled: React.PropTypes.bool,
		//className: React.PropTypes.string,
		//tabIndex: React.PropTypes.number
	},

	getDefaultProps() {
		return {
			options: ['-'],
			selected: [],
			//disabled: false,
			//className: 'radio-group',
			//tabIndex: 0
		};
	},

	getInitialState() {
		return {

		};
	},
	
	componentDidMount() {

	},

	componentWillUnmount() {

	},

	render() {
		let {
			options,
			selected,
			//disabled,
			className,
			//tabIndex
		} = this.props;


		let optionsElem = options.map((el, idx) => {
			let { label, value } = el;
			
			return (
				<div className="check-bottom"
					key={`check${idx}`}
					data-val={(el.value === 0 ? 0 : (value || ''))}>
					<span>
						<input type="checkbox" id={value} checked={this.isChecked(value)} onChange={this.handleOptionClick}/>
						<label htmlFor={value}><i />{label}</label>
					</span>
				</div>
			);
		});
		
		
		return (
			<div className="check-top">
				<div className="check-top1">
					{optionsElem}
				</div>
			</div>
		);
	},

	handleOptionClick(e) {
		this.props.handleSelect(e);
		//this.setState({
		//	tabIndex: null
		//});
	},

	isChecked(val) {
		return this.props.selected.indexOf(val) > -1;
	}

	
});

module.exports = CheckboxGroup;
