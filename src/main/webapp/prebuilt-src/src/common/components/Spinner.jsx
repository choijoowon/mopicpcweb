let React = require('react');

let refreshIntervalId = '';

let Spinner = React.createClass({
	propTypes: {
		width: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number
		]),
		height: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number
		])
    },

    getDefaultProps() {
        return {
			width: '50px',
			height: '50px'
        };
    },
	
	render(){
		return (
			<img
				src="/resources/images/common/spinner.gif"
				style={{width: this.props.width, height: this.props.height}}
			/>
		);
	}
});

module.exports = Spinner;
