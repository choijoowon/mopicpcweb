let React = require('react');



let ComparisonCard = React.createClass({
    propTypes: {
		productTitle: React.PropTypes.string,
		productInfo: React.PropTypes.array,
    },

    getDefaultProps: function() {
        return {
			productTitle: '',
			productInfo: [],
        };
    },


    componentDidMount: function() {

    },

    componentWillUnmount: function() {

    },

    render: function() {
		let props = this.props;
		let { productTitle, productInfo } = props;

		let numInfo = productInfo.length;
		let defaultColSpan = Math.floor(12 / numInfo);

		let productInfoElem = productInfo.map((elem, idx) => {
			let {
				attr = '',
				value = '',
				colSpan = defaultColSpan,
				type = '',
				prefix,
				unit,
				style = {}
			} = elem;

			//let prefix = prefix && <span className='in'>{prefix}</span>
			 let _prefix = (prefix === undefined) ? (<span className="hide">{prefix}</span>) : (<span className="in">{prefix}</span>);
			return (

				<div key={`${attr}${value}`} className={`col-xs-${colSpan} ${type}`} style={style}>
					<span>{attr}</span>
					{_prefix}
					<span className="val">{value}</span>
					<span className='in'>{unit}</span>
				</div>
			);
		});


		return (
			<div className="container">
				<div className="carousel-caption">
					<div className="item_view">
						<span>가입상품</span>
						{productTitle}
					</div>
					<div className="row item_view_down">
						{productInfoElem}
					</div>
				</div>
			</div>
		);
    }
});

module.exports = ComparisonCard;
