let React = require('react');



let PurchasedProduct = React.createClass({
    propTypes: {
		provider: React.PropTypes.object.isRequired,
		title: React.PropTypes.object.isRequired,
		leadingInfo: React.PropTypes.object.isRequired,
		handleRemoveBtnClick: React.PropTypes.func.isRequired
    },

    getDefaultProps: function() {
        return {
			
        };
    },


    componentDidMount: function() {

    },

    componentWillUnmount: function() {

    },

    render: function() {
		let { provider, title, leadingInfo, handleRemoveBtnClick } = this.props;

		let { attr, value, prefix, unit } = leadingInfo;

		prefix = prefix && <span className="in">{prefix}</span>;
		unit = unit && <span className="in">{unit}</span>;
		
		return (
			<div className="cmpr-product">
				<div className="col-md-3">
					<span className="labelType">{provider.attr}</span>
					<span className="val">{provider.value}</span>
				</div>
				<div className="col-md-4">
					<span className="labelType">{title.attr || '상품명'}</span>
					<span className="val">{title.value}</span>
				</div>
				<div className="col-md-3 num">
					<span className="labelType">{leadingInfo.attr}</span>
					{prefix}<span className="val">{value}</span>{unit}
				</div>
				<div onClick={handleRemoveBtnClick} className="col-md-2">
					<a className="btn_right" title="선택제거">선택제거</a>
				</div>
			</div>
		);

		//TODO: delete product
    }
});

module.exports = PurchasedProduct;
