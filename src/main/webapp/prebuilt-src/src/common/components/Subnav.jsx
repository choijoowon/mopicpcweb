let React = require('react'),
    QueryStore = require('../stores/QueryStore'),
    CommonActions = require('../actions/CommonActions'),
    SelectList = require('../components/SelectList.jsx');


function getStoreState() {
    return QueryStore.getQueryState();
}


let Subnav = React.createClass({
    propTypes: {
		baseUrl: React.PropTypes.string.isRequired
    },

    getDefaultProps: function() {
		return {

		};
    },

    getInitialState: function() {
		return getStoreState();
    },
    
    componentDidMount: function() {
		QueryStore.addChangeListner(this._onChange);
    },

    componentWillUnmount: function() {
		QueryStore.removeChangeListner(this._onChange);
    },

    render: function() {
		return (
			<SelectList
				options={[
						 { label: '예금', dataAttrs: { value: 'deposits' } },
						 { label: '적금', dataAttrs: { value: '적금' } },
						 { label: 'MMDA', dataAttrs: { value: 'MMDA' } },
						 { label: '주택청약', dataAttrs: { value: '주택청약' } },
						 ]}
				value={this.state.submenu || 'deposits'}
				handleSelect={this.handleClick} />
		);
    },

    _onChange: function() {
		this.setState(getStoreState());
    },

    handleClick: function(e) {
		let baseUrl = this.props.baseUrl,
			subUrl = $(e.target).closest('li').data('value'),
			pageUrl = `${baseUrl}/${subUrl}`;

		// change URL without reloading if html5 supported
		if (typeof (history.pushState) != "undefined") {
            history.pushState({}, e.target.textContent, pageUrl);
		} else {
			window.location.href = pageUrl;
		}

		// some action here
		CommonActions.selectSubmenu(subUrl);
    }

    
});

module.exports = Subnav;
