let React		= require('react'),
	ReactDOM	= require('react-dom');

var RadioGroup = React.createClass({
	propTypes: {
		handleSelect: React.PropTypes.func,
		options: React.PropTypes.array,
		selected: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		]),
		//disabled: React.PropTypes.bool,
		//className: React.PropTypes.string,
		tabIndex: React.PropTypes.number
	},

	getDefaultProps() {
		return {
			options: ['-'],
			selected: '',
			//disabled: false,
			//className: 'radio-group',
			tabIndex: 0
		};
	},

	getInitialState() {
		return {

		};
	},
	
	componentDidMount() {
		// set default value as the first one: by triggering click
		let { options, selected } = this.props;
		
		if(!selected && options.length > 0) {
			let firstOpt = document.getElementById(options[0].value);
			if(firstOpt) {
				setTimeout(() => { firstOpt.click(); } , 50); // CAUTION: to end dispatch before emit
			}
		}
	},

	componentWillUnmount() {

	},

	componentDidUpdate(prevProps, prevState) {
		// set default value as the first one when options changed
		let { options, selected } = this.props;
		
		if(prevProps.options !== options) {
			if(!selected && options.length > 0) {
				let firstOpt = document.getElementById(options[0].value);
				if(firstOpt) {
					setTimeout(() => { firstOpt.click(); } , 50); // CAUTION: to end dispatch before emit
				}
			}
		}
	},

	render() {
		let {
			options,
			selected,
			//disabled,
			className,
			tabIndex
		} = this.props;


		let optionsElem = options.map((el, idx) => {
			let { label, value } = el;
			
			return (
				<div className="radio-bottom radio-bottom5"
					key={`radio${idx}`}
					data-val={(value === 0 ? 0 : (value || ''))}>
					
					<span>
						<input type="radio" id={value} checked={value === selected} onChange={this.handleOptionClick} />
						<label htmlFor={value}><i />{label}</label>
					</span>
					
				</div>
			);
		});
		
		
		return (
			<div className="radio_normal">
				<div className="radio-top1">
					{optionsElem}
				</div>
			</div>
		);
	},

	handleOptionClick(e) {
		this.props.handleSelect(e);
		this.setState({
			tabIndex: null
		});
	}
	
});

module.exports = RadioGroup;
