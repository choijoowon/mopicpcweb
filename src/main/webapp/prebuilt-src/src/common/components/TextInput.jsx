let React				= require('react'),
    ReactDOM			= require('react-dom'),
	numberWithCommas	= require('../utils/numberFilterFunctions').numberWithCommas,
	checkIE				= require('../utils/checkIE');


let IE_version = checkIE();


let TextInput = React.createClass({
    propTypes: {
		type: React.PropTypes.string,
		handleChange: React.PropTypes.func,
		value: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		]),
		placeholder: React.PropTypes.string,
		maxlength: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		]),
		unit: React.PropTypes.string,
		label: React.PropTypes.string,
		name: React.PropTypes.string,
		inputClass: React.PropTypes.string,
		fixed: React.PropTypes.bool,
		disabled: React.PropTypes.bool,
		validationFn: React.PropTypes.oneOfType([
			React.PropTypes.bool,
			React.PropTypes.func
		]),
		maximum: React.PropTypes.oneOfType([
			React.PropTypes.bool,
			React.PropTypes.number,
		])
    },

    getDefaultProps: function() {
		return {
			type: 'text',
			handleChange: () => {},
			value: '',
			placeholder: '-',
			maxlength: '',
			unit: '',
			label: '',
			name: '',
			inputClass: '',
			fixed: false,
			disabled: false,
			validationFn: false,
			maximum: false,
		};
    },
    
    componentDidMount: function() {
		this.putPlaceholderIfIE();
    },

    componentWillUnmount: function() {
		
    },

	componentDidUpdate(prevProps, prevState) {
		this.putPlaceholderIfIE();
	},

    render: function() {
		let {
			type,
			label,
			inputClass,
			value,
			placeholder,
			maxlength,
			unit,
			handleChange,
			fixed,
			disabled,
			validationFn
		} = this.props;

		

		label = label ? ( <label>{label}</label> ) : undefined;

		if(fixed) {
			return (
				<div className='text-input'>
					{label}
					<input
						ref='input'
						type='text'
						className={inputClass}
						value={value}
						placeholder={placeholder}
						maxLength={maxlength}
						disabled />
					<span className='unit'>{unit}</span>
				</div>
			);
		} else {
			let isInvalid, validationMsg;
			
			if(validationFn) {
				validationMsg = validationFn(value);
				isInvalid = (validationMsg !== true);
			}

			let handleKeyDown, valueToDisplay = value;
			let allowedKeys;

			switch(type) {

				case('number-with-commas'):
					// Allow: backspace, delete, tab, escape, and enter
					// ( disable '.' when with commas - 46? 190?)
					allowedKeys = [8, 9, 27, 13, 110];
					valueToDisplay = numberWithCommas(value);

					handleKeyDown = (evt) => {
						let charCode = evt.which || evt.keyCode;

						if ($.inArray(charCode, allowedKeys) !== -1 ||
							// Allow: Ctrl+A, Command+A
							(charCode == 65 && ( evt.ctrlKey === true || evt.metaKey === true ) ) || 
							// Allow: Ctrl+L, Command+L
							(charCode == 76 && ( evt.ctrlKey === true || evt.metaKey === true ) ) || 
							// Allow: home, end, left, right, down, up
							(charCode >= 35 && charCode <= 40)) {
								// let it happen, don't do anything
								return;
						}
						// Ensure that it is a number and stop the keypress
						if ((evt.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
							evt.preventDefault();
						}
					};
					break;
					
				case('number'):
					allowedKeys = [8, 9, 27, 13, 110, 46, 190];

					handleKeyDown = (evt) => {
						let charCode = evt.which || event.keyCode;

						if ($.inArray(charCode, allowedKeys) !== -1 ||
							// Allow: Ctrl+A, Command+A
							(charCode == 65 && ( evt.ctrlKey === true || evt.metaKey === true ) ) || 
							// Allow: Ctrl+L, Command+L
							(charCode == 76 && ( evt.ctrlKey === true || evt.metaKey === true ) ) || 
							// Allow: home, end, left, right, down, up
							(charCode >= 35 && charCode <= 40)) {
								// let it happen, don't do anything
								return;
						}
						// Ensure that it is a number and stop the keypress
						if ((evt.shiftKey || (charCode < 48 || charCode > 57)) && (charCode < 96 || charCode > 105)) {
							evt.preventDefault();
						}
					};

					// show when number with commas filter function numberFilterFunctions and
					// change onchange event in common actions

					break;
				default:
					handleKeyDown = () => {};
			}
			

			
			
			return (
				<div className={'text-input' + (isInvalid ? ' invalid' : '')}>
					{label}
					<input
						ref='input'
						type='text'
						className={inputClass}
						value={valueToDisplay}
						placeholder={placeholder}
						maxLength={maxlength}
						onKeyDown={handleKeyDown}
						onChange={this.handleTextChange}
						disabled={disabled}
					/>
					<span className='unit'>{unit}</span>

					{ isInvalid ?
					 (<span className='validation-msg'>{validationMsg}</span>) : undefined }
				</div>
			);
		}
    },

	handleTextChange(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		if(this.props.type === 'number-with-commas') {
			let targetVal = target.value;
			if(targetVal) {
				// remove all commas and 0's at front
				target.value = String( parseFloat(targetVal.replace(/,/g, '')) );

				let maximum = this.props.maximum;
				if(maximum) {
					if(target.value > maximum) {
						target.value = maximum;
					}
				}
				
			}
		}
		this.props.handleChange(evt);
	},

	putPlaceholderIfIE() {
		if(IE_version && IE_version <= 9) {
			let inputElem = $(this.refs.input);
			if( !inputElem.is(':focus') ) {
				inputElem.placeholderEnhanced('destroy');
				inputElem.placeholderEnhanced();
			}
		}
	}

});

module.exports = TextInput;
