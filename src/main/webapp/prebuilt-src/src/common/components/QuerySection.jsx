let React				= require('react'),
	CommonActions		= require('../actions/CommonActions'),
	TextInput			= require('./TextInput.jsx'),
	SelectDropdown		= require('./SelectDropdown.jsx'),
	MultiSelectDropdown = require('./MultiSelectDropdown.jsx'),
	RadioGroup			= require('./RadioGroup.jsx');


let QuerySection = React.createClass({
	mixins: [require('../../common/utils/sendQueryMixin')],
	
    propTypes: {
		queryState: React.PropTypes.object,
		resultState: React.PropTypes.object,
		entries: React.PropTypes.array
    },

    getDefaultProps() {
        return {
			entries: []
        };
    },

	getInitialState: function() {
        return {
			
        };
    },
    
	
    componentDidMount() {

    },

    componentWillUnmount() {

    },

	render() {
		let { entries, queryState, resultState } = this.props;
		let queries = entries.map( (entry) => {
			let queryElem;
			switch(entry.type) {
				case('number'):
				case('number-with-commas'):
				case('text'):
					queryElem = (
						<div className='col-md-4' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<TextInput
								type={entry.type}
								placeholder={entry.placeholder || ' '}
								inputClass='text-input-normal'
								maxlength={entry.maxlength}
								handleChange={this.handleTextInputChange.bind(null, entry.id)}
								value={queryState.searchQuery[entry.id]}
								unit={entry.unit || ''}
								validationFn={queryState.showValidation && entry.validationFn}
							/>
						</div>
					);
					break;
				case('numFixed'):
					queryElem = (
						<div className='col-md-4' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<TextInput
								placeholder=' '
								inputClass='text-input-normal'
								fixed={true}
								value={entry.fixedAs}
								unit={entry.unit || ''} />
						</div>	
					);
					break;
				case('select'):
					queryElem = (
						<div className='col-md-4' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<SelectDropdown
								tabIndex={0}
								placeholder={entry.placeholder || '선택'}
								options={entry.options}
								handleSelect={this.handleSelectChange.bind(null, entry.id, entry.dependencies, false)}
								className={entry.className || 'select-dropdown'}
								selected={queryState.searchQuery[entry.id]}
								validationFn={queryState.showValidation && entry.validationFn}
							/>
						</div>
					);
					break;
				case('multi-select-icon'):
					queryElem = (
						<div className='col-md-12' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<MultiSelectDropdown
								placeholder={entry.placeholder || '선택'}
								options={entry.options}
								handleSelect={this.handleSelectChange.bind(null, entry.id, entry.dependencies, true)}
								selected={queryState.searchQuery[entry.id]}
								className={entry.className || 'multi-select-dropdown'}
								limitSelection={entry.limitSelection || false}
								handleSelectAll={this.handleSelectAll.bind(null, entry.id, entry.options.map((e) => e.value))}
								validationFn={queryState.showValidation && entry.validationFn}
							/>
						</div>
					);
					break;
				case('radio-group'):
					queryElem = (
						<div className='col-xs-5 offset-right-7' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
							<RadioGroup
								options={entry.options}
								handleSelect={this.handleSelectChange.bind(null, entry.id, entry.dependencies, false)}
								selected={queryState.searchQuery[entry.id]}
							/>
						</div>
					);
					break;
				default:
					queryElem = (
						<div className='col-md-4' key={entry.label}>
							<label className='select-label'>{entry.label || ''}</label>
						</div>
					);
			}
			
			return queryElem;
		});
		
        return (
            <div>
				<div className='row'>
					{ queries }
				</div>
				
				<div className='right_btn'>
					<a className='btn btn-submit' id='search-btn' role='button' onClick={this.handleSearchBtnClick}>검색</a>
				</div>
            </div>
        );
    },


	handleTextInputChange(id, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		CommonActions.changeSearchQuery(id, target.value);
	},

	handleSearchBtnClick() {
		let { queryState, resultState } = this.props;
		let { searchQuery } = queryState;

		let allValid = this.checkValidity();

		if(allValid) {
			// If the 'address' of queryState or resultState is changed here (by clear actions),
			// sendQueryWithCount function won't catch the change since props of QuerySection
			// will change afterward
			
			//CommonActions.clearResults(); BAD

			CommonActions.setReceivedOnce(false);
			
			CommonActions.changeFilter(99999999);
			CommonActions.changeSorting('');

			$('body').addClass('masked'); // disable scrolling
			this.sendQueryWithCount(0, queryState, resultState, false);
			
		} else {
			CommonActions.validationToShow(true);
		}
	},

	handleSelectChange(id, dependencies, isMultiSelect, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = $(target).closest('[data-val]').attr('data-val');
		val = (val === 0) ? 0 : (val || target.textContent);

		if(isMultiSelect) {
			CommonActions.toggleCheckbox(id, val);
		} else {
			CommonActions.changeSearchQuery(id, val);
		}

		if(dependencies && dependencies.length > 0) {
			for(let i = 0, l = dependencies.length; i < l; i++) {
				let dependency = dependencies[i];
				CommonActions.changeSearchQuery(dependency, '');
			}
		}
	},


	handleSelectAll(id, optionVals, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		if(target.checked) {
			CommonActions.changeSearchQuery(id, optionVals);
		} else {
			CommonActions.changeSearchQuery(id, []);
		}
	},
	

	checkValidity() {
		let allValid = true;
		let { entries } = this.props;
		let { searchQuery } = this.props.queryState;
		
		for(let i = 0, l = entries.length; i < l; i++) {
			let entry = entries[i];
			
			if(entry.validationFn) {
				allValid = allValid && (entry.validationFn(searchQuery[entry.id]) === true);
			}
		}

		return allValid;
	}


    
});

module.exports = QuerySection;
