const React = require('react'),
	classNames = require('classnames');

var SelectList = React.createClass({
    propTypes: {
		handleSelect: React.PropTypes.func.isRequired,
		options: React.PropTypes.array,
		value: React.PropTypes.oneOfType([
			React.PropTypes.string,
			React.PropTypes.number,
		]),
		className: React.PropTypes.string
    },

    getDefaultProps: function() {
		return {
			options: ['-'],
			value: '',
			className: ''
		};
    },
    
    componentDidMount: function() {

    },

    componentWillUnmount: function() {

    },

    render: function() {
		let props = this.props,
			labelSelected;
		
		let options = props.options.map( (elem, idx) => {
			let {label, icon, dataAttrs, className, setHTML} = elem;
			let elemContent;
			
			if (typeof elem === 'string' || elem instanceof String) {
				label = elem;
			}

			if(dataAttrs) {
				// adding 'data-' to the beginning of each object keys		
				if(typeof dataAttrs === 'object') {
					for (let key in dataAttrs) {
						if(key.indexOf('data-') !== 0 ) {
							dataAttrs[`data-${key}`] = dataAttrs[key];
							delete dataAttrs[key];
						}
					}
				} else {
					throw new Error('dataAttrs must be passed in the form of an object');
				}
			}

			if(setHTML) {
				elemContent = <span dangerouslySetInnerHTML={setHTML} />;
			} else {
				elemContent = <a>{label}</a>;
			}
		


			let optClass = (dataAttrs && dataAttrs['data-value']) ?
				classNames([{ active: (props.value == dataAttrs['data-value']) }, className]) :
				classNames([{ active: (props.value === label) }, className]);
			// CAUTION : using double equal for cross type check
			
			
			let iconClass = classNames([
				'icon',
				`icon-${icon}`
			]);

			// put icon if provided
			return (
				<li className={optClass}
					onClick={props.handleSelect}
					{...dataAttrs}
					key={'opt' + idx + label} >
					{elem.icon? <i className={iconClass}></i> : undefined}
					{elemContent}
				</li>
			);
		});
		
		return (
			<ul className={this.props.className}>
				{ options }
				{ this.props.children }
			</ul>
		);
    },
    
});

module.exports = SelectList;
