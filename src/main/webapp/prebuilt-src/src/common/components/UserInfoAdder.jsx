let React			= require('react'),
	ReactDOM		= require('react-dom'),
	CommonActions	= require('../actions/CommonActions'),
	SelectDropdown	= require('./SelectDropdown.jsx'),
	TextInput		= require('./TextInput.jsx'),
	CheckboxGroup	= require('./CheckboxGroup.jsx'),
	Api				= require('../utils/api'),
	birthdate		= require('../utils/formValidators').birthdate;


class SelectOpts {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		this.data = data;
	}
}

function getSelectOptions(optCodeMap, code) {
	if(optCodeMap[code]) {
		return ( optCodeMap[code].map((e) => new SelectOpts(e.cdName, e.cd)) );
	}
	// TODO: when no map for the code?
	return [];
}


let UserInfoAdder = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
	],
	
    propTypes: {
		
    },

    getDefaultProps() {
        return {
			
        };
    },
	
	getInitialState() {
		return {
			occupation: '',
			sex: '',
			mainBank: '',
			bankCode: '',
			dob: '',
			income: '',
			productInterest: [],
			homeCity: '',
			homeTown: '',
			homeTownOpts: [],
			homeTownPlacholder: '시/군/도'
		};
    },
    

    componentDidMount() {
		CommonActions.fetchOptions([
			7001, 7002, 7003
		]);

		Api.post('/mopic/api/mem/selectSigunguInfo', {})
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					CommonActions.addOptions({ sidoOpts: res.result.data });
				}
			}.bind(this));
    },

    componentWillUnmount() {
		
    },

    render() {
		let {
			queryState,
			occupation,
			sex,
			mainBank,
			bankCode,
			dob,
			income,
			productInterest,
			homeCity,
			homeTown,
			homeTownOpts,
			homeTownPlacholder
		} = this.state;

		let { optCodeMap } = queryState;


		let sidoOpts = [];
		if(optCodeMap['sidoOpts']) {
			sidoOpts = optCodeMap['sidoOpts'].map((el) => new SelectOpts(el.sido_name, el.sido, el));
		}

		
		return (
			<div className="modal fade" id="infoAdder" tabIndex={-1} role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div className="modal-dialog">
					<div className="modal-content modal-small">
						<a href className="close" data-dismiss="modal" aria-label="Close" />
						<div className="modal-header">
							<h4 className="modal-title" id="myModalLabel"><img src="/resources/images/common/logo_blue.png" alt="mopic" width="110" /></h4>
						</div>
						<div className="modal-body">
							<div className="product_add">
								<p className="txt">추가정보를 입력하시면, 입력하신 정보를 기반으로 더 좋은 상품을 추천 해드립니다.</p>
								<div className="row">
									<div className="col-xs-5">
										<label htmlFor="select" className="select-label">직업</label>
										<SelectDropdown
											placeholder={'선택'}
											options={getSelectOptions(optCodeMap, 7001)}
											handleSelect={this.handleSelect.bind(null, 'occupation')}
											selected={occupation}
											validationFn={false}
										/>
									</div>
									<div className="col-xs-5 col-xs-offset-1">
										<label htmlFor="select" className="select-label">생년월일</label>
										<TextInput
											type='number'
											placeholder={'YYMMDD'}
											inputClass='text-input-normal'
											maxlength='6'
											handleChange={this.handleTextChange.bind(null, 'dob')}
											value={dob}
											validationFn={birthdate}
										/>
									</div>
								</div>
								<div className="row">
									<div className="col-xs-5">
										<label htmlFor="select" className="select-label">성별</label>
										<SelectDropdown
											placeholder={'선택'}
											options={getSelectOptions(optCodeMap, 1234)}
											handleSelect={this.handleSelect.bind(null, 'sex')}
											selected={sex}
											validationFn={false}
										/>
									</div>
									<div className="col-xs-5 col-xs-offset-1 income">
										<label htmlFor="select" className="select-label">가처분소득(월)</label>
										<TextInput
											type='number-with-commas'
											placeholder={' '}
											inputClass='text-input-normal'
											handleChange={this.handleTextChange.bind(null, 'income')}
											value={income}
											unit={'만원'}
											validationFn={false}
										/>
									</div>
								</div>
								<div className="row">
									<div className="col-xs-5">
										<label htmlFor="select" className="select-label">주거래은행</label>
										<SelectDropdown
											placeholder={'선택'}
											options={getSelectOptions(optCodeMap, 7002)}
											handleSelect={this.handleSelect.bind(null, 'bankCode')}
											selected={bankCode}
											validationFn={false}
										/>
									</div>
									<div className="col-xs-5 col-xs-offset-1">
										<label htmlFor="select" className="select-label">거주지역</label>
										<SelectDropdown
											placeholder={'시/도'}
											options={sidoOpts}
											handleSelect={this.handleSidoSelect.bind(null, sidoOpts)}
											selected={homeCity}
											validationFn={false}
										/>
									</div>
								</div>
								{/*<div className="row">
									<div className="col-xs-5" />
									<div className="col-xs-5 col-xs-offset-1">
										<label htmlFor="select" className="select-label" />
										<SelectDropdown
											placeholder={homeTownPlacholder}
											options={homeTownOpts}
											handleSelect={this.handleSelect.bind(null, 'homeTown')}
											selected={homeTown}
											validationFn={false}
										/>
									</div>
								</div>*/}
								<div className="row">
									<div className="col-xs-2">
										<label htmlFor="select" className="select-label">관심분야</label>
									</div>
									<div className="col-xs-10">
										<CheckboxGroup
											options={getSelectOptions(optCodeMap, 7003)}
											selected={productInterest}
											handleSelect={this.handleCheckboxToggle.bind(null, 'productInterest')}
										/>
									</div>
								</div>
								<div className="center_btn" onClick={this.submitForm}><a className="btn btn-submit" role="button">확인</a></div>							
							</div>
						</div>
					</div>
				</div>
			</div>
		);
    },

	handleTextChange(attr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = target.value;

		this.setState({
			[attr]: val
		});
	},

	handleSelect(attr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;

		let val = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			[attr]: val
		});
	},

	handleSidoSelect(sidoOpts, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;

		let val = $(target).closest('li').attr('data-val') || target.textContent;

		let sigundoOpts = sidoOpts.find((e) => e.value === val).data.sigungu_name;
		sigundoOpts = JSON.parse(`{${sigundoOpts}}`);

		let homeTownOpts = [];
		for(let key in sigundoOpts) {
			homeTownOpts.push(new SelectOpts(sigundoOpts[key], key));
		}

		this.setState({
			homeCity: val,
			homeTown: '',
			homeTownPlacholder: '선택',
			homeTownOpts
		});
	},

	handleCheckboxToggle(attr, evt) {
		let selected = this.state[attr];

		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;

		let val = $(target).closest('[data-val]').attr('data-val');
		let idx = selected.indexOf(val);

		if(idx > -1) {
			selected.splice(idx, 1);
		} else {
			selected.push(val);
		}

		
		this.setState({
			[attr]: selected
		});
	},

	submitForm(evt) {
		if(birthdate(this.state.dob) === true) {
			evt = evt? evt : window.event;
			let target = evt.target || evt.srcElement;

			let data = Object.assign({}, this.state, {
				productInterest: this.state.productInterest.join(',')
			});

			delete data.homeTownOpts;
			delete data.homeTownPlacholder;

			Api.post('/mopic/api/updateUserInfo', data)
				.then(function (res) {
					if(res.result.resultCode === 'SUCCESS') {
						$(target).closest('.modal').modal('hide');
					}
				}.bind(this));
		}
	}

	
});


module.exports = UserInfoAdder;
