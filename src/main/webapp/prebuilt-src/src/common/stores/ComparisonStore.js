let EventEmitter	= require('events').EventEmitter,
	Dispatcher		= require('../dispatcher/Dispatcher'),
	appConstants	= require('../constants/constants');

const CHANGE_EVENT	= 'change',
	  TURN_UP_EVENT = 'turnUp';



// Initial data
let _comparisons = getEmptyComparison();


function getEmptyComparison() {
	return {
		isCmprTabActive: false,
		isSelectionViewActive: false,
		isLoggedIn: false,
		isBest: false,
		comparisonGroup: [],
		currentCmpr: 0
	};
}

function setTabState(isActive) {
	_comparisons.isCmprTabActive = isActive;
}

function setSelectionView(isActive) {
	_comparisons.isSelectionViewActive = isActive;
}

function setSelectionViewState(isOpen) {
	_comparisons.isSelectionViewActive = isOpen;
}

function setBest(isBest) {
	_comparisons.isBest = isBest;
}

function setCurrentCmpr(idx) {
	_comparisons.currentCmpr = idx;
}

function setMyProducts(resData) {
	_comparisons.comparisonGroup = resData;
}

function addProduct(product) {
	_comparisons.comparisonGroup.push(product);
}

function removeProduct(id) {
	let {comparisonGroup} = _comparisons;
	comparisonGroup.splice(comparisonGroup.findIndex((e) => e.id === id), 1);
}

function clearStore() {
	let { isLoggedIn } = _comparisons;
	_comparisons = Object.assign({}, getEmptyComparison(), {isLoggedIn}); // preserve login state
}



let ComparisonStore = Object.assign({}, EventEmitter.prototype, {
	addChangeListner(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListner(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

	emitChange() {
		this.emit(CHANGE_EVENT);
	},

	getComparisonState() {
		return _comparisons;
	},

    rehydrate(loc) {
		let state = window.App;
		if (state) {
			if(state[loc]) {
				_comparisons = Object.assign({}, _comparisons, state[loc]);
			}
		}
    },

	
	// TURN_UP_EVENT : when comparisonGroup changes length from 0 to 1 or more -> send query
	
	addTurnUpListner(callback) {
		this.on(TURN_UP_EVENT, callback);
	},

	removeTurnUpListener(callback) {
		this.removeListener(TURN_UP_EVENT, callback);
	},

	emitTurnUp() {
		this.emit(TURN_UP_EVENT);
	}
	
	
});

// Register callback
Dispatcher.register(function (payload) {
	let action = payload.action;
	let formerComparisonLength = ComparisonStore.getComparisonState().comparisonGroup.length;
	

	switch (action.actionType) {

	case appConstants.SET_COMPARISON_TAB_STATE:
        setTabState(action.isActive);
        break;

	case appConstants.SET_SELECTION_VIEW_STATE:
        setSelectionView(action.isActive);
        break;

	case appConstants.SET_BEST_PRODUCT:
        setBest(action.isBest);
        break;

	case appConstants.SET_CURRENT_COMPARISON:
        setCurrentCmpr(action.idx);
        break;

	case appConstants.RECEIVE_MY_PRODUCTS:
        setMyProducts(action.myProducts);
        break;

	case appConstants.ADD_MY_PRODUCT:
		addProduct(action.product);
        break;

	case appConstants.REMOVE_MY_PRODUCT:
        removeProduct(action.id);
        break;

	case appConstants.CLEAR_COMPARIONS:
        clearStore();
        break;		

    default:
        return true;
	}

	ComparisonStore.emitChange();

	let newComparisonLength = ComparisonStore.getComparisonState().comparisonGroup.length;
	if(formerComparisonLength === 0 && newComparisonLength > 0) {
		setTimeout(() => { ComparisonStore.emitTurnUp(); } , 100); // CAUTION: to end dispatch before emit
	}

	return true;
});


module.exports = ComparisonStore;
