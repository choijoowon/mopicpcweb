const EventEmitter	= require('events').EventEmitter,
	  Dispatcher	= require('../dispatcher/Dispatcher'),
	  appConstants	= require('../constants/constants'),
	  CHANGE_EVENT	= 'change';


// Initial data
let _results = getEmptyResult();


function getEmptyResult() {
	return {
		receivedOnce: false,
		isFetching: false,
		isChangingStuff: false, // if changing filter or sorting
		productData: [],
		numberByFilter: {}
	};
}

function setResults(res) {
	let { filterStat, productData, resultCode } = res.resultState;

	let isLoadingMore = res.searchQuery.total_count !== "0";
	let totalNumData = 0;

	// if loading by reaching bottom, add to current array
	if(isLoadingMore) {
		for (let i = 0, l = productData.length; i < l; i++) {
			let product = productData[i];
			_results.productData.push(product);
		}
	} else {
		if(filterStat) {
			// caclulate total number of result and add as a filter
			for (let i = 0, l = filterStat.length; i < l; i++) {
				let countObj = filterStat[i];
				_results.numberByFilter[countObj.filter_cd] = countObj.filter_cnt;
				totalNumData += countObj.filter_cnt;
			}
			_results.numberByFilter[99999999] = totalNumData;
		}
		_results.productData = productData;
	}

	_results.receivedOnce = true;

}

function emptyStore() {
	_results = getEmptyResult();
}

function setInterestState(idx, isInterested) {
	let interest = isInterested ? 'Y' : 'N';

	if(_results.productData[idx]) {
		_results.productData[idx].interest_yn = interest;
	}
}


function setFetching(state) {
	_results.isFetching = state;
}

function setChanging(state) {
	_results.isChangingStuff = state;
}

function setReceivedOnce(state) {
	_results.receivedOnce = state;
}



var ResultStore = Object.assign({}, EventEmitter.prototype, {
	addChangeListner: function addChangeListner(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListner: function removeChangeListner(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	},

	emitChange: function emitChange() {
		this.emit(CHANGE_EVENT);
	},

	getResults: function getResults() {
		return _results;
	},

    rehydrate: function rehydrate(loc) {
		let state = window.App;
		if (state) {
			if(state[loc]) {
				_results = Object.assign({}, _results, state[loc]);
			}
		}
    }
	
});




// Register callback
Dispatcher.register(function (payload) {
	let action = payload.action;

	switch (action.actionType) {

		// Respond to RECEIVE_DATA action
    case appConstants.RECEIVE_SEARCH_PRODUCTS:
        setResults(action.res);
        break;

    case appConstants.CLEAR_RESULTS:
		emptyStore();
		break;
		
    case appConstants.SET_INTERESTED:
		setInterestState(action.idx, true);
		break;
		
	case appConstants.SET_NOT_INTERESTED:
		setInterestState(action.idx, false);
		break;

	case appConstants.SET_RECEIVED_ONCE:
		setReceivedOnce(action.state);
		break;
		
	case appConstants.SET_FETCHING_STATE:
		setFetching(action.state);
		break;

	case appConstants.SET_CHANGING_STATE:
		setChanging(action.state);
		break;

    default:
        return true;
	}

	ResultStore.emitChange();

	return true;
});

module.exports = ResultStore;
