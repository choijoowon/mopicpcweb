let EventEmitter	= require('events').EventEmitter,
	Dispatcher		= require('../dispatcher/Dispatcher'),
	appConstants	= require('../constants/constants'),
	CHANGE_EVENT	= 'change';



// Define initial query points
let _query = getEmptyQuery();


function getEmptyQuery() {
	return {
		showValidation: false,
		mainmenu: '',
		submenu: '',
		searchQuery: {},
		filter: 99999999, // 전체
		sorting: '',
		optCodeMap: {}
	};
}


// Method to change query by toggling checkboxes
function toggleCheckbox(queryAttr, checkboxName) {
	let selectedBoxArray = _query.searchQuery[queryAttr];

	if(selectedBoxArray && selectedBoxArray instanceof Array) {
		let	idx = selectedBoxArray.indexOf(checkboxName);

		// check if checkboxName is already selected
		if (idx > -1) {
			selectedBoxArray.splice(idx, 1);
		} else {
			selectedBoxArray.push(checkboxName);
		}
	} else {
		_query.searchQuery[queryAttr] = [checkboxName];
	}
}

function setSearchQuery(queryAttr, option) {
    _query.searchQuery[queryAttr] = option;
}

function setSubmenu(mainmenu, submenu) {
	let {optCodeMap} = _query;
    _query = Object.assign({}, getEmptyQuery(), {mainmenu, submenu, optCodeMap}); // preserve optCodeMap
}

function clearQuery() {
	setSubmenu(_query.mainmenu, _query.submenu);
}

function setFilter (changeTo) {
	_query.filter = changeTo;
}

function setSorting (changeTo) {
	_query.sorting = changeTo;
}

function setOptCodes(data) {
	_query.optCodeMap = Object.assign({}, _query.optCodeMap, data);   //->  if code list queried by grpCdList
	
	// let rawOpts = res.result.data;
	// let grpSet = [];
	// let optCodeMap = {};

	// for (let opt of rawOpts) {
	// 	let {grpCd} = opt;
		
	// 	if( !(grpSet.indexOf(grpCd) > -1) ) {
	// 		grpSet.push(grpCd);
	// 		optCodeMap[grpCd] = [];
	// 	}

	// 	optCodeMap[grpCd].push(opt);
	// }

	//_query.optCodeMap = optCodeMap;
}

function setShowValidation(toShow) {
	_query.showValidation = toShow;
}



var QueryStore = Object.assign({}, EventEmitter.prototype, {
    addChangeListner: function addChangeListner(callback) {
		this.on(CHANGE_EVENT, callback);
    },

    removeChangeListner: function removeChangeListner(callback) {
		this.removeListener(CHANGE_EVENT, callback);
    },

    emitChange: function emitChange() {
		this.emit(CHANGE_EVENT);
    },

    getQueryState: function getQuery() {
		return _query;
    },

    rehydrate: function rehydrate(loc) {
		let state = window.App;
		if (state) {
			if(state[loc]) {
				_query = Object.assign({}, _query, state[loc]);
			}
		}
    }
    
});

// Register callback
Dispatcher.register(function (payload) {
    var action = payload.action;

    switch (action.actionType) {

    case appConstants.QUERY_CHECKBOX_TOGGLE:
        toggleCheckbox(action.queryAttr, action.checkboxName);
        break;

    case appConstants.CHANGE_QUERY:
        setSearchQuery(action.queryAttr, action.value);
        break;

    case appConstants.CLEAR_QUERY:
		clearQuery();
        break;		

    case appConstants.SELECT_SUBMENU:
		setSubmenu(action.mainmenu, action.submenu);
		break;

    case appConstants.CHANGE_FILTER:
		setFilter(action.changeTo);
		break;

    case appConstants.CHANGE_SORTING:
		setSorting(action.changeTo);
		break;

    case appConstants.RECEIVE_OPT:
        setOptCodes(action.optCodes);
        break;

    case appConstants.SET_SHOWING_VALIDATION:
		setShowValidation(action.toShow);
        break;

    default:
        return true;
    }

    QueryStore.emitChange();

    return true;
});

module.exports = QueryStore;
