(function() {
	if(window.App.isFirstLogin && window.App.isFirstLogin === 'Y') {
		let React = require('react'),
			ReactDOM = require('react-dom'),
			UserInfoAdder = require('./components/UserInfoAdder.jsx');
		
		ReactDOM.render(
			<UserInfoAdder />,
			document.getElementById('first-time-container'),
			() => {
				//setCookie('isFirstLogin', 'N');
				$('#infoAdder').modal('show');
			}
		);
	}
})();
