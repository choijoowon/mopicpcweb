let React				= require('react'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas,
	formValidators		= require('../../common/utils/formValidators');

let { selectRequired, requiredBirthdate } = formValidators;


let InsureHelperSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/helperSectionMixin.jsx'),
	],
	

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {
    
        }
    },
    
    componentDidMount() {

    },

    componentWillUnmount() {

    },

	_getSearchEntries(queryState) {
		let {submenu, searchQuery, optCodeMap} = queryState;
		let entries;

		switch(submenu.toLowerCase()) {
			case('shilson'):
				entries = [
					{
						label: '생년월일',
						type: 'number',
						placeholder: 'YYMMDD',
						maxlength: '6',
						id: 'birthConvertToAge', // convert to insure age when sending requests
						validationFn: requiredBirthdate,
					},
					{
						label: '성별',
						type: 'select',
						id: 'sex',
						options: this.getSelectOptions(optCodeMap, 1234),
						validationFn: selectRequired
					},
					{
						label: '선택담보',
						type: 'select',
						id: 'dambo1',
						options: this.getSelectOptions(optCodeMap, 3102),
						validationFn: selectRequired
					}
				];
				break;
			case('life'):
				entries = [
					{
						label: '보험나이',
						type: 'numFixed',
						id: 'age',
						fixedAs: '40'
					},
					{
						label: '성별',
						type: 'select',
						id: 'sex',
						options: this.getSelectOptions(optCodeMap, 1234),
						validationFn: selectRequired
					}
				];
				break;
			case('annuity'):
				entries = [
					{
						label: '보험나이',
						type: 'select',
						id: 'age',
						options: this.getSelectOptions(optCodeMap, 3302),
						validationFn: selectRequired
					},
					{
						label: '성별',
						type: 'select',
						id: 'sex',
						options: this.getSelectOptions(optCodeMap, 1234),
						validationFn: selectRequired
					}
				];
				break;
			case('car'):
				let ageContractOpts = {
					label: '연령특약',
					type: 'select',
					id: 'age_contract',
					options: [],
					validationFn: selectRequired
				};
				
				if(searchQuery.age) {
					let ageSelected = optCodeMap[3406].find((e) => (e.cd == searchQuery.age));
					// caution:  using '==' since converting into string

					if(ageSelected && ageSelected.ref1) {
						let opts = [];
						let rawOpts = JSON.parse(`{ ${ageSelected.ref1} }`);
						
						for (let cd in rawOpts) {
							opts.push({
								label: rawOpts[cd],
								value: cd
							});
						}
						ageContractOpts.options = opts;
					}
				}
				
				entries = [
					{
						label: '보험나이',
						type: 'select',
						id: 'age',
						options: this.getSelectOptions(optCodeMap, 3406),
						dependencies: ['age_contract'],
						validationFn: selectRequired
					},

					ageContractOpts,
					
					{
						label: '성별',
						type: 'select',
						id: 'sex',
						options: this.getSelectOptions(optCodeMap, 1234),
						validationFn: selectRequired
					},
					{
						label: '운전자한정',
						type: 'select',
						id: 'driver',
						options: this.getSelectOptions(optCodeMap, 3402),
						validationFn: selectRequired
					},
					
					{
						label: '전담보가입',
						type: 'select',
						id: 'dambo',
						options: this.getSelectOptions(optCodeMap, 3403),
						validationFn: selectRequired
					},
					{
						label: '할인할증등급',
						type: 'select',
						id: 'dc_grade',
						className: 'select-dropdown',
						options: this.getSelectOptions(optCodeMap, 3404),
						validationFn: selectRequired
					},
					{
						label: '차종',
						type: 'select',
						id: 'car_type',
						options: this.getSelectOptions(optCodeMap, 3405),
						validationFn: selectRequired
					}

				];						
				break;
			default:
				entries = [];
		}

		return entries;
	},

	_getCmprCardContents(submenu, comparisonGroup) {
		let cmprCardContents = [];

		switch(submenu.toLowerCase()) {
			case('shilson'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						prod_name,
						company_nm,
						amount,
						filter_name,
						dambo_shilson_name
					} = elem;
					
					return	{
						title: prod_name,
						info: [
							{
								attr: '보험사명',
								value: elem.company_nm,
								colSpan: 3,
								isProvider: true
							},
							{
								attr: '보험료',
								value: numberWithCommas(elem.amount),
								colSpan: 3,
								prefix: '월',
								unit: '원',
								isLeadingInfo: true
							},
							{
								attr: '가입유형',
								value: elem.filter_name,
								colSpan: 3
							},
							{
								attr: '선택담보',
								value: elem.dambo_shilson_name,
								colSpan: 3
							}
						]
					}
				});
				break;
			case('life'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						prod_name,
						company_nm,
						amount,
						filter_name,
						price_index,
						public_rate,
						lowest_rate
					} = elem;
					
					return	{
						title: prod_name,
						info: [
							{
								attr: '보험사명',
								value: company_nm,
								colSpan: 3,
								isProvider: true
							},
							{
								attr: '보험가격지수',
								value: price_index,
								colSpan: 3,
								unit: '%',
								isLeadingInfo: true
							},
							{
								attr: '공시이율',
								value: public_rate,
								colSpan: 3,
								unit: '%'
							},
							{
								attr: '최저보증이율',
								value: lowest_rate,
								colSpan: 3,
								unit: '%'
							}
						]
					}
				});
				break;
			case('annuity'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						prod_name,
						company_nm,
						amount,
						gubun,
						saved_rate_10,
						public_rate,
						lowest_rate
					} = elem;
					
					return	{
						title: prod_name,
						info: [
							{
								attr: '보험사명',
								value: company_nm,
								colSpan: 2,
								isProvider: true
							},
							{
								attr: '10년 적립률',
								value: saved_rate_10,
								colSpan: 2,
								unit: '%',
								isLeadingInfo: true
							},
							{
								attr: '공시이율',
								value: public_rate,
								colSpan: 2,
								unit: '%'
							},
							{
								attr: '최저보증이율',
								value: lowest_rate,
								colSpan: 4,
								unit: '',
								style: { width: '40%', borderRight: 'none', borderLeft: '1px solid #77bdf1'}
							}
						]
					}
				});
				break;
			case('car'):
				cmprCardContents = comparisonGroup.map((elem, idx) => {
					let {
						prod_name,
						company_nm,
						amount
					} = elem;
					
					return	{
						title: prod_name,
						info: [
							{
								attr: '보험사명',
								value: company_nm,
								colSpan: 6,
								isProvider: true
							},
							{
								attr: '보험료',
								value: numberWithCommas(amount),
								colSpan: 6,
								prefix: '연',
								unit: '원',
								isLeadingInfo: true
							}
						]
					}
				});
				break;
				
		}


		return cmprCardContents;
	}
	
    
});

module.exports = InsureHelperSection;
