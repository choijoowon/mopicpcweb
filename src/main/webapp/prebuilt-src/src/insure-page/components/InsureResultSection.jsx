let React				= require('react'),
	ProductCard			= require('../../common/components/ProductCard.jsx'),
	numberWithCommas	= require('../../common/utils/numberFilterFunctions').numberWithCommas;



let InsureResultSection = React.createClass({
	mixins: [
		require('../../common/utils/queryStoreMixin'),
		require('../../common/utils/productStoreMixin'),
		require('../../common/utils/comparisonStoreMixin'),
		require('../../common/utils/sendQueryMixin'),
		require('../../common/utils/resultSectionMixin.jsx')
	],
	
    propTypes: {

    },

    getDefaultProps() {
        return {
			
        };
    },

    getInitialState() {
        return {

        }
    },
    
    componentDidMount() {
		//let { queryState, resultState } = this.state;
		
		//if (resultState.productData.length === 0) {
		//	let {mainmenu, submenu} = queryState;
		//	let query = this.getAcceptableQuery(queryState);
		//	CommonActions.fetchResults(mainmenu, submenu, query);
		//}
    },

    componentWillUnmount() {

    },


	_getOptions(submenu, optCodeMap) {
		// returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (Array of ProductCard's elements)
		//    helperElem  -> (if needed) modal to use or any other element
		
		let {resultState, comparisonState} = this.state;
		
		let { isCmprTabActive, isLoggedIn, comparisonGroup,	currentCmpr } = comparisonState;
		let showingCmpr = (isLoggedIn && isCmprTabActive && comparisonGroup.length > 0);
		let selectedCmpr = comparisonGroup[currentCmpr];
		
		let filterOptions, sortOptions, tooltipElem, resultsElem, helperElem;
		let comparisonContents = undefined;
		
		switch(submenu.toLowerCase()) {
			// 실손보험
			case('shilson'):
				filterOptions = this._getListOptions(optCodeMap, 3101);
				filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = this._getListOptions(optCodeMap, 3100);

				tooltipElem = (
					<span className="badge" data-toggle="tooltip" data-html="true" data-placement="right" title="지급준비율이란?(보험회사가 가입자에게 보험금을 제때 지급할수 있는지를 나타내는 것으로 보험회사의 재무건전성을 판단할수 있는 지표입니다.)<br>부지급률이란?(청구된 보험금 중 지급하지 않은 보험금 비율을 나타냅니다.)">?</span>
				);


				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						filter,
						dambo1,
						reserve_rate,
						unpaid_rate,
						prod_code,
						sex,
						age,
						insurance_fee,
						prod_name,
						detail_url,
						interest_yn,
						company_code,
					} = elem;
						
					
					let mainAttrSet = {
						'가입유형': filter,
						'선택담보': dambo1,
						'지급준비율': `${reserve_rate} %`,
						'부지급률': `${unpaid_rate} %`
					};


					
					if(showingCmpr) {
						comparisonContents = [
							{
								attr: '월 보험료',
								diff: insurance_fee - selectedCmpr.amount,
								unit: '원'
							},
						];
					}

					let isInterested = (interest_yn === 'Y');
					
					return (
						<ProductCard
							key={`shilson${prod_code}${sex}${age}`}
							type='type2 shilson'
							category={filter}
							provider={company_code}
							leadingInfo={numberWithCommas(insurance_fee)}
							prefix='월'
							unit='원'
							productTitle={prod_name}
							mainAttrSet={mainAttrSet}
							detailUrl={detail_url}
							targetModalSelector='#product_view_popup'
							comparisonContents={comparisonContents}
							index={idx}
							isInterested={isInterested}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, 3100, prod_code, idx)}
							choiceReverse={true}
						/>
					);
				});

				break;
			// 정기보험
			case('life'):
				filterOptions = this._getListOptions(optCodeMap, 3201);
				filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = this._getListOptions(optCodeMap, 3200);


				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						insurance_fee,
						public_rate,
						lowest_rate,
						id,
						gubun2,
						price_index,
						prod_name,
						detail_url,
						interest_yn,
						prod_code,
						company_code,
					} = elem;
					
					let mainAttrSet = {
						'보험료': `${numberWithCommas(elem.insurance_fee)} 원 (1개월, 40세 기준)`,
						'공시이율': `${elem.public_rate} %`,
						'최저보증이율': elem.lowest_rate
					};

					
					if(showingCmpr) {
						comparisonContents = [
							{
								attr: '보험가격지수',
								diff: price_index - selectedCmpr.price_index,
								unit: '%'
							},
						];
					}

					let isInterested = (interest_yn === 'Y');
					
					return (
						<ProductCard
							key={`life${elem.id}`}
							type='type1 life'
							category={elem.gubun2}
							provider={company_code}
							leadingInfoName={'보험가격지수 <span class="badge" data-toggle="tooltip" data-html="true" data-placement="right" title="" data-original-title="보험가격지수란?<br />해당 보험사의 보험료를 참조순 보험료와 업계평균사업비의 합으로 나눈 값으로, 100보다 높으면 평균 대비 비싸고 낮으면 저렴하다는 것을 의미합니다.">?</span>'}
							leadingInfo={elem.price_index}
							unit='%'
							productTitle={elem.prod_name}
							mainAttrSet={mainAttrSet}
							detailUrl={elem.detail_url}
							targetModalSelector='#product_view_popup'
							comparisonContents={comparisonContents}
							index={idx}
							isInterested={isInterested}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, 3200, prod_code, idx)}
							choiceReverse={true}
						/>
					);
				});
				
				break;
			case('annuity'):
				filterOptions = this._getListOptions(optCodeMap, 3301);
				filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = this._getListOptions(optCodeMap, 3300);

				//tooltipElem = (
			//	<span className="badge" data-toggle="tooltip" data-placement="bottom" title="보험나이, 성별 등을 고려하여 가입하신 상품보다 보험료가 저렴한, 지급준비율이 높은, 부지급률이 낮은 상품을 추천해드립니다.">?</span>
				//);

				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						prod_name,
						gubun,
						insurance_fee,
						public_rate,
						lowest_rate,
						id,
						ins_type,
						saved_rate_10,
						detail_url,
						refund_url,
						interest_yn,
						prod_code,
						company_code
					} = elem;
					
					
					let period = (gubun === '적립') ? '(월)' : '(일시)';

					let mainAttrSet = {};

					mainAttrSet[`${period}납입액`] = `${numberWithCommas(elem.insurance_fee)} 원`;

					mainAttrSet = Object.assign(mainAttrSet, {
						//'매년 연금 수령액': `${insurance_fee} 원`,
						'공시이율': `${public_rate} %`,
						'최저보증이율': lowest_rate,
						'해지환급금': (
							<a
								onClick={(e) => {
									 e.stopPropagation();
									 let targetElem = $('#refund_popup');
									 
									 targetElem.removeData('bs.modal');
									 targetElem.addClass('hide');
								
									 targetElem
										 .modal()
										 .find('.modal-body')
										 .load(refund_url, (responseText, textStatus) => {
											 
											 if( textStatus === 'success' || textStatus === 'notmodified' ) {
												 targetElem.removeClass('in');
												 targetElem.removeClass('hide');
												 
												 setTimeout(() => { targetElem.addClass('in'); }, 50);
											 } else {
												 targetModalElem.modal('hide');
											 }
										 });

									 }
								}>자세히보기</a>
						)
					});

					if(showingCmpr) {
						comparisonContents = [
							{
								attr: '10년 적립률',
								diff: saved_rate_10 - selectedCmpr.saved_rate_10,
								unit: '%'
							},
						];
					}

					let isInterested = (interest_yn === 'Y');
					
					return (
						<ProductCard
							key={`annuity${id}`}
							type='type1 annuity sm'
							category={ins_type}
							provider={company_code}							
							leadingInfoName={'10년 적립률 <span class="badge" data-toggle="tooltip" data-html="true" data-placement="right" title="" data-original-title="10년 적립률이란?<br>(보험상품에 가입한지 10년 후에 납부한 보험료 대비 받을 수 있는 환급금의 비율입니다.)">?</span>'}
							leadingInfo={saved_rate_10}
							unit='%'
							productTitle={prod_name}
							mainAttrSet={mainAttrSet}
							detailUrl={detail_url}
							targetModalSelector='#product_view_popup'
							comparisonContents={comparisonContents}
							index={idx}
							isInterested={isInterested}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, 3300, prod_code, idx)}
							choiceReverse={false}
						/>
					);
				});

				helperElem = (
					<div className="modal fade" id="refund_popup" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
						<div className="modal-dialog">
						    <div className="modal-content modal-large">
						        <button className="close" data-dismiss="modal" aria-label="Close"></button>
						        <div className="modal-body">
						            상세내용을 가져올 수 없습니다.
								</div>
							</div>
						</div>
					</div>
				);
				
				break;
			case('car'):
				filterOptions = this._getListOptions(optCodeMap, 3401);
				filterOptions = this._addNumResults(filterOptions);
				
				sortOptions = this._getListOptions(optCodeMap, 3400);

				//tooltipElem = (
			//	<span className="badge" data-toggle="tooltip" data-placement="bottom" title="보험나이, 성별 등을 고려하여 가입하신 상품보다 보험료가 저렴한, 지급준비율이 높은, 부지급률이 낮은 상품을 추천해드립니다.">?</span>
				//);

				resultsElem = resultState.productData.map( (elem, idx) => {
					let {
						insurance_fee,
						company_nm,
						filter,
						interest_yn,
						prod_code,
						company_code,
						bigo,
						homepage
					} = elem;

					if(showingCmpr) {
						comparisonContents = [
							{
								attr: '연 보험료',
								diff: insurance_fee - selectedCmpr.amount,
								unit: '원'
							},
						];
					}

					let isInterested = (interest_yn === 'Y');
					
					return (
						// TODO: modify key
						<ProductCard
							key={`cars${idx}`}
							type='type2'
							category={filter}
							provider={company_code}
							leadingInfo={numberWithCommas(elem.insurance_fee)}
							prefix='연'
							unit='원'
							productTitle={elem.company_nm}
							comparisonContents={comparisonContents}
							index={idx}
							isInterested={isInterested}
							description={bigo}
							handleInterestClick={this.handleInterestClick.bind(null, isInterested, 3400, prod_code, idx)}
							choiceReverse={true}
							companyLink={homepage}
						/>
					);
				});
				break;

			default:
		}

		return {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem};
	},

	getPreSearchView(submenu, maskElem, fetchingMsgElem) {
		let preSearchViewElem;
		
		switch(submenu.toLowerCase()) {
			case('shilson'):

				break;
			case('car'):
				preSearchViewElem = (
					<div className="car_list">
						<span className="notice blue">보험나이 / 연령특약 / 성별 / 운전자한정 / 전담보가입 / 할인할증등급 / 차종</span><span className="notice">을  입력해 나에게 맞는 자동차 보험을 찾아 보세요.</span>
						<table summary="보험사별 자동차 보험 상품 특징">	
							<caption>보험사별 자동차 보험 상품 특징</caption>
							<colgroup>
								<col width="194px;" />
								<col width="605px;" />
							</colgroup>
							<thead>
								<tr>
									<th scope="col">보험사</th>
									<th scope="col">상품특징</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi00.png" alt="동부화재" width="107" /></td>
									<td className="txt_left">
										<p>
											- 50년전통의 대한민국 최초 자동차보험 회사<br/>
											- 365일 24시간 빠른출동 서비스<br/>
											- Evergreen특약, 주행거리 특약등 추가할인 혜택<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi01.png" alt="삼성화재" width="107" /></td>
									<td className="txt_left">
										<p>
											- 3년무사고(타사가입경력포함)라면,보험료의 5~8%할인 혜택<br/>
											- 199개의 보상전국적인 보상조직 운영(14년 9월 기준)<br/>
											- 휴일에도 운영하는 우수협력정비업체 특화서비스 제공<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi02.png" alt="현대해상" width="107" /></td>
									<td className="txt_left">
										<p>
											- 글로벌 고객만족도 자동차보험부문 8년 연속1위<br/>
											- 주행거리 특약 최대 13.2% 추가할인<br/>
											- 여성 운전자 특별 서비스<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi03.png" alt="KB손해보험" width="107" /></td>
									<td className="txt_left">
										<p>
											- 가족구성원 특성에 따른 다양한 특약 구성<br/>
											- 서민을 위한 보험료 할인 혜택<br/>
											- (희망나누기/ 희망나눔자동차 특별약관 가입시)<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi04.png" alt="한화손해보험" width="107" /></td>
									<td className="txt_left">
										<p>
											- 외제차 충돌시 확대보상 특별약관<br/>
											- 60년 전통의 든든한 종합손해보험사<br/>
											- 1년 365일 24시간 함께하는 보상서비스<br/>
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi05.png" alt="메리츠화재" width="107" /></td>
									<td className="txt_left">
										<p>
											- 전국 200여개 가맹점을 통한 긴급출동 서비스<br/>
											- 자체 보상조직망을 통한 신속하고 세심한 보상서비스<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi06.png" alt="흥국화재" width="107" /></td>
									<td className="txt_left">
										<p>
											- 실속파 운전자를 위한 다양한 특약<br/>
											- 차량 장착물 할인을 통해 특별요율 적용으로 보험료 할인 혜택<br/>
											- 여성, 주말운전자 등에게 맞춘 특약 제공<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi07.png" alt="MG손해보험" width="107" /></td>
									<td className="txt_left">
										<p>
											- 긴급출동서비스(특약가입시)<br/>
											- 블랙박스 장착시 추가할인(특약가입시)<br/>
											- 마일리지 특약으로 추가할인!(특약가입시)<br/> 
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi08.png" alt="AXA다이렉트" width="107" /></td>
									<td className="txt_left">
										<p>
											- 후할인 마일리지 17.4%(연간주행거리 5000km 이하시)<br/>
											- 블랙박스 장착시 5%, 3년 무사고 13.2% 추가할인<br/>
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi09.png" alt="더케이손해보험" width="107" /></td>
									<td className="txt_left">
										<p>
											- 1만2천km이하 주행시 마일리지 특약 최대 24% 할인<br/>
											- 블랙박스 장착 5%, 3년 무사고시 10% 추가할인<br/>
										</p>
									</td>
								</tr>
								<tr>
									<td><img src="/resources/images/common/ins_bi/ins_bi10.png" alt="롯데손해보험" width="107" /></td>
									<td className="txt_left">
										<p>
											- 다이렉트로 한번, 주행거리로 또 한번 절감<br/>
											- 제휴카드 최대 3만원 청구 할인 (카드사 제공)<br/>
										</p>
									</td>
								</tr>
							</tbody>
						</table>
						{ maskElem }
						{ fetchingMsgElem }
					</div>

				);
				break;
			default:
				
		}

		return preSearchViewElem;
	}


    
});

module.exports = InsureResultSection;
