let React			= require('react'),
	SelectDropdown	= require('../../common/components/SelectDropdown.jsx'),
	TextInput		= require('../../common/components/TextInput.jsx'),
	Api				= require('../../common/utils/api'),
	calcInsureAge	= require('../../common/utils/numberFilterFunctions').calcInsureAge,
	CommonActions	= require('../../common/actions/CommonActions'),
	formValidators	= require('../../common/utils/formValidators');

let { selectRequired, numberRequired } = formValidators;

const submenu_codes = require('../../common/constants/submenu_codes');



class SelectOpts {
	constructor(label, value, data) {
		this.label = label;
		this.value = value;
		this.data = data;
	}
}


let InsureProductAdder = React.createClass({
    propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
    },

    getDefaultProps() {
        return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {},
        };
    },
	
	getInitialState: function() {
        return {
			showValidation: false,
			query: {
				prod_name: ''
			},
			companyOpts: [],
			prodNameOpts: [],
			prod_code: '',
			price: '',
			prodNamePlaceholder: '조건을 모두 입력해주세요',
        }
    },
    

    componentDidMount() {
		let menu_cd = submenu_codes[this.props.submenu];
		
		Api.post('/mopic/api/ins/selectAllCompanyListByInsType', {menu_cd})
			.then(function (res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;
					let options = ( resultData.map((e) => new SelectOpts(e.company_nm, e.company_code)) );
					
					this.setState({
						companyOpts: options
					});
				}
			}.bind(this));

		// clear when closed
		$('#product_add_popup').on('hidden.bs.modal', this.clearStats);


    },

    componentWillUnmount() {

    },

    render() {
		let { query, companyOpts, prodNameOpts, price, prod_code, prodNamePlaceholder } = this.state;
		let { submenu, searchEntries, optCodeMap } = this.props;

		let companyForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">기관명</label>
					<SelectDropdown
						placeholder='선택'
						options={companyOpts}
						handleSelect={this.changeSelectQuery.bind(null, 'company_code', null)}
						selected={query.company_code}
						validationFn={this.state.showValidation && selectRequired}
					/>
				</div>
			</div>
		);
		
		let basicsForm = searchEntries.map( (entry) => {
			let queryElem;

			if(entry.id === 'age_contract') {
				if(query.age) {
					let ageSelected = optCodeMap[3406].find((e) => e.cd == query.age);
					// caution:  using '==' since converting into string

					if(ageSelected && ageSelected.ref1) {
						let opts = [];
						let rawOpts = JSON.parse(`{ ${ageSelected.ref1} }`);
						
						for (let cd in rawOpts) {
							opts.push({
								label: rawOpts[cd],
								value: cd
							});
						}
						entry.options = opts;
					}
				}
			}
			
			switch(entry.type) {
				case('number'):
				case('number-with-commas'):
				case('text'):
					queryElem = <TextInput
									type={entry.type}
									placeholder={entry.placeholder || ' '}
									inputClass='text-input-normal'
									handleChange={this.handleTextInputChange.bind(null, entry.id)}
									maxlength={entry.maxlength}
									value={query[entry.id]}
									unit={entry.unit || ''}
									validationFn={this.state.showValidation && entry.validationFn}
								/>;
					break;
				case('numFixed'):
					queryElem = <TextInput
									placeholder=' '
									inputClass='text-input-normal'
									maxlength={entry.maxlength}
									fixed={true}
									value={entry.fixedAs}
									unit={entry.unit || ''} />;
					break;
				case('select'):
					queryElem = <SelectDropdown
									placeholder={entry.placeholder || '선택'}
									options={entry.options}
									handleSelect={this.changeSelectQuery.bind(null, entry.id, entry.dependencies)}
									className={entry.className || 'select-dropdown'}
									selected={query[entry.id]}
									validationFn={this.state.showValidation && entry.validationFn}
								/>;
					break;
				default:
					queryElem = undefined;
			}
			
			return (
				<div className="row" key={`adder-${entry.label}`}>
					<div className="col-xs-6">
						<label className="select-label">{entry.label || ''}</label>
						{ queryElem }
					</div>
				</div>
			);
		});

		let productNameForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">상품명</label>
					<SelectDropdown
						placeholder={prodNamePlaceholder}
						options={prodNameOpts}
						handleSelect={this.handleProductNameSelect}
						selected={prod_code}
						validationFn={this.state.showValidation && selectRequired}
					/>
				</div>
			</div>
		);

		let priceLabel = submenu.toLowerCase() === 'car' ? '보험료' : '월 보험료';
		let priceForm = (
			<div className="row">
				<div className="col-xs-6">
					<label className="select-label">{priceLabel}</label>
					<TextInput
						type='number-with-commas'
						placeholder=' '
						inputClass='text-input-normal'
						handleChange={this.handlePriceChange}
						value={price}
						unit='원'
						validationFn={this.state.showValidation && numberRequired}
					/>
				</div>
			</div>
		);

		
		
		return (
			<div className={`modal fade ${submenu.toLowerCase()}`} id="product_add_popup">
				<div className="modal-dialog">
					<div className="modal-content modal-small">

						<a className="close" data-dismiss="modal" aria-label="Close" />
						
						<div className="modal-header">
							<h4 className="modal-title" id="myModalLabel">가입상품 추가하기</h4>
						</div>

						
						<div className="modal-body">
							<div className="product_add">

								{companyForm}
								
								{basicsForm}

								{productNameForm}

								{priceForm}
								
								{/*<span className="txt">(금리를 입력하지 않으실 경우, 등록일 현재 기준 금리로 적용됩니다.)</span>*/}
								<div className="center_btn">
									<a className="btn btn-submit" role="button" onClick={this.submitProduct}>확인</a>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		);
    },


	changeSelectQuery(queryAttr, dependencies, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let val = $(target).closest('li').attr('data-val') || target.textContent;
		let query = Object.assign({}, this.state.query, {[queryAttr]: val});

		if(dependencies && dependencies.length > 0) {
			for(let i = 0, l = dependencies.length; i < l; i++) {
				let dependency = dependencies[i];
				query[dependency] = '';
			}
		}

		this.setState({
			query: query,
			prod_code: '',
			price: '',
			prodNameOpts: [],
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		}, function() {
			if(this.checkValidity()) {
				this.getProductNames(this.props.submenu, query);
			}
		}.bind(this));
	},

	handleTextInputChange(queryAttr, evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = target.value;
		let query = Object.assign({}, this.state.query, {[queryAttr]: val});
		
		this.setState({  // reset prod_code and price as well
			query: query,
			prod_code: '',
			price: '',
			prodNameOpts: [],
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		}, function() {
			if(this.checkValidity()) {
				this.getProductNames(this.props.submenu, query);
			}
		}.bind(this));

	},

	handlePriceChange(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = target.value;
		
		this.setState({
			price: val
		});
	},

	getProductNames(submenu, queryState) {
		let menu_cd = submenu_codes[this.props.submenu];
		let query = Object.assign({}, this.state.query, {menu_cd});

		this.setState({
			prodNameOpts: [],
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		});
		
		for(let key in query) {
			let val = query[key];
			
			if(key === 'birthConvertToAge') {
				query['age'] = String(calcInsureAge(val));
				delete query[key];

				continue;
			}

			// number '99999999': non-sending code
			if(val == 99999999) {  // Caution '==': check '99999999' string
				delete query[key];
			} else {
				query[key] = String(val);
			}
		}


		Api.post('/mopic/api/ins/filterProductByName', query)
			.then(function(res) {
				if(res.result.resultCode === 'SUCCESS') {
					let resultData = res.result.data;
					let opts = resultData.map((e) => new SelectOpts(e.prod_name, e.prod_code, e));

					if(opts.length === 0) {
						this.setState({
							prodNamePlaceholder: '조건에 맞는 상품이 존재하지 않습니다'
						});
					} else {
						this.setState({
							prodNamePlaceholder: '선택',
							prodNameOpts: opts
						});
					}
				}

			}.bind(this));

	},

	handleProductNameSelect(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
		
		let val = $(target).closest('li').attr('data-val') || target.textContent;
		let price = this.state.prodNameOpts.find((e) => (e.value === val)).data.insurance_fee; 

		this.setState({
			prod_code: val,
			price: String(price)
		});
	},

	submitProduct(evt) {
		evt = evt? evt : window.event;
		let target = evt.target || evt.srcElement;
			
		let { query, companyOpts, prodNameOpts, price, prod_code } = this.state;
		let { submenu, searchEntries } = this.props;
		let data;

		let allValid = this.checkValidity();

		if(allValid) {
			
			switch(submenu.toLowerCase()) {
				case('shilson'):
					data = {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label),
						amount: String(price),
						age: String(calcInsureAge(query.birthConvertToAge)),
						sex: String(query.sex),
						dambo_shilson: String(query.dambo1)
					};
					break;
				case('life'):
					data = {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label),
						amount: String(price),
						sex: String(query.sex),
						age: "40",
					};
					break;
				case('annuity'):
					data = {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label),
						amount: String(price),
						age: String(query.age),					
						sex: String(query.sex),
					};
					break;
				case('car'):
					data = {
						product_type: String(submenu_codes[submenu]),
						prod_code: String(prod_code),
						prod_name: String(prodNameOpts.find((elem) => (elem.value === prod_code)).label), 
						amount: String(price),
						age: String(query.age),					
						sex: String(query.sex),
						age_contract: String(query.age_contract),
						driver: String(query.driver),
						dambo: String(query.dambo),
						dc_grade: String(query.dc_grade),
						car_type: String(query.car_type),
					};
					break;
			}
			
			let $target = $(target);			
			CommonActions.addMyProduct(data, () => {
				$target.closest('.modal').modal('hide');
			});

		} else {
			this.setState({
				showValidation: true
			})
		}
	},

	clearStats() {
		this.setState({
			query: {
				prod_name: ''
			},
			prodNameOpts: [],
			prod_code: '',
			price: '',
			showValidation: false
		});
	},

	checkValidity() {
		let allValid = true;
		let { query } = this.state;
		let { searchEntries } = this.props;
		
		for(let i = 0, l = searchEntries.length; i < l; i++) {
			let entry = searchEntries[i];
			
			if(entry.validationFn) {
				allValid = allValid && (entry.validationFn(query[entry.id]) === true);
			}
		}

		return allValid;
	}
	
	
});

module.exports = InsureProductAdder;
