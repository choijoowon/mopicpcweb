(function() {

    let React				= require('react'),
		ReactDOM			= require('react-dom'),
		QueryStore			= require('../common/stores/QueryStore'),
		ProductStore		= require('../common/stores/ProductStore'),
		ComparisonStore		= require('../common/stores/ComparisonStore'),		
		CommonActions		= require('../common/actions/CommonActions'),
		getSearchCallback	= require('../common/utils/getSearchCallback'),
		InsureHelperSection = require('./components/InsureHelperSection.jsx'),
		InsureResultSection = require('./components/InsureResultSection.jsx'),
		InsureProductAdder	= require('./components/InsureProductAdder.jsx');


	let mainmenu, submenu, isLoggedIn, searchQuery;
    
    if(window.App) {
		({mainmenu, submenu, searchQuery} = window.App.query);
		isLoggedIn = window.App.comparisons.isLoggedIn;		
		
		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
    }


//	CommonActions.fetchOptions([
//		3100, 3101, 3102, 3103, 3200, 3201, 3300, 3301, 3302, 1234, 3400, 3401, 3402, 3403, 3404, 3405, 3406
//	]);


	if(isLoggedIn) {
		CommonActions.fetchMyProducts(mainmenu, submenu);
	}
		
	let productAdder = <InsureProductAdder />;

	let searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away
	

    ReactDOM.render(
		<InsureHelperSection
			adderModal={productAdder} />,
		document.getElementById('helper'),
		searchCallback
    );

	ReactDOM.render(
		<InsureResultSection />,
		document.getElementById('results')
    );


})();
