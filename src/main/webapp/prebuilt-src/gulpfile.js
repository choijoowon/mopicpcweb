'use strict';

var gulp = require('gulp'),
    browserify = require('browserify'), // Bundles JS
    flatten = require('gulp-flatten'),
	rename = require("gulp-rename"),
    source = require('vinyl-source-stream'), // Use conventional text streams with Gulp
    lint = require('gulp-eslint'),   //Lint JS files, including JSX
    sass = require('gulp-ruby-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    babel = require('babelify'),
    factor = require('factor-bundle'),
	iconfont = require('gulp-iconfont'),
	consolidate = require('gulp-consolidate'),
	del = require('del'),
	uglify = require('gulp-uglify'),
	runTimestamp = Math.round(Date.now()/1000);



var paths = {
    JS: ['./src/**/*.js', './src/**/*.jsx'],
	IMAGES: './images/**/*',
	BI: './images/BI/**/*',
	OTHER_IMAGES: ['./images/**/*', '!./images/BI/**/*'],
    FONTS: './fonts/**/*',
    CSS: [
		'./src/common/common.scss',
		'./src/common/**/*.scss',
		'./src/*-page/**/*.scss'
    ],
    MAIN_JS_ENTRIES: [
		'./src/main-page/main-page.jsx',
		'./src/savings-page/savings-page.jsx',
		'./src/loan-page/loan-page.jsx',
		'./src/insure-page/insure-page.jsx',
		'./src/common/common_menu.js',
		'./src/common/detail-modal-actions.js',
        './src/card-page/card-page.jsx',
        './src/p2p-page/p2p-page.jsx',
        './src/exchange-page/exchange-page.jsx',
		'./src/common/getUserInfo.jsx'
    ],
    MAIN_JS_OUTPUTS: [
		'../resources/js/main-page.bundle.js',
		'../resources/js/savings-page.bundle.js',
		'../resources/js/loan-page.bundle.js',
		'../resources/js/insure-page.bundle.js',
		'../resources/js/common_menu.bundle.js',
		'../resources/js/detail-modal-actions.bundle.js',
        '../resources/js/card-page.bundle.js',
        '../resources/js/p2p-page.bundle.js',
        '../resources/js/exchange-page.bundle.js',
		'../resources/js/getUserInfo.bundle.js'
    ],
	COMMON_JS: [
		'./src/common/common_menu.js'
	],
    RESOURCES: '../resources',
	SVG_ICONS: './images/svg-icons/*.svg'
};





gulp.task('css', function() {
	sass.clearCache();
	
    return sass(paths.CSS, {
		sourcemap: false,
		compass: true,
		style: 'compressed'
    })
    	.on('error', function(err) { console.log(err.message); })
    	.pipe(flatten())
    	//.pipe(sourcemaps.write())    
    	.pipe(gulp.dest(paths.RESOURCES + '/css'));
});


gulp.task('css-without-clear', function() {
    return sass(paths.CSS, {
		compass: true,		
		sourcemap: false,
		style: 'compressed'
    })
    	.on('error', function(err) { console.log(err.message); })
    	.pipe(flatten())
    	//.pipe(sourcemaps.write())    
    	.pipe(gulp.dest(paths.RESOURCES + '/css'));
});


gulp.task('js', function(done) {
	// gulp.src(paths.COMMON_JS)
	// 	.pipe(flatten())
	// 	.pipe(gulp.dest(paths.RESOURCES + '/js'));
	
    return browserify({
		entries: paths.MAIN_JS_ENTRIES,
		debug: true
    })

		.transform('babelify', {
			presets: ['es2015', 'react'],
			sourceMaps: true
		})

//		.plugin('minifyify', {
//			map: 'bundle.map.json',
//			output: './src/common/bundle.map.json'
//		})
	
		.plugin(factor, {
			outputs: paths.MAIN_JS_OUTPUTS
		})





		.bundle()
		.on('error', console.error.bind(console))
		.pipe(source('common.bundle.js'))
		.pipe(gulp.dest(paths.RESOURCES + '/js'));
});


gulp.task('uglify', ['js'], function() {
  return gulp.src(paths.RESOURCES + '/js/*.bundle.js')
		.pipe(uglify())
//		.pipe(rename({ extname: '.min' }))
		.pipe(gulp.dest(paths.RESOURCES + '/js'));
});




gulp.task('bi', ['css'], function () {
	return del([paths.RESOURCES + '/images/**/*.*'], {force: true})
		.then((deletedPaths) => {
			//console.log('Files and folders that would be deleted:\n', deletedPaths.join('\n'));
			gulp.src(paths.IMAGES)
				.pipe(gulp.dest(paths.RESOURCES + '/images'));

			console.log('Renewed images in resources directory');

		});
});


// OTHER THAN BI
gulp.task('other-images', function () {
	gulp.src(paths.OTHER_IMAGES)
		.pipe(gulp.dest(paths.RESOURCES + '/images'));
});




gulp.task('Iconfont', function(){
	// return gulp.src([paths.SVG_ICONS])
	// 	.pipe(iconfont({
	// 		fontName: 'mopic-glyphicon', // required
	// 		//appendUnicode: true, // recommended option
	// 		formats: ['ttf', 'eot', 'woff', 'svg'], // default, 'woff2' and 'svg' are available
	// 		timestamp: runTimestamp // recommended to get consistent builds when watching files
	// 	}))
	// 	.on('glyphs', function(glyphs) {
	// 		var options = {
	// 			glyphs: glyphs.map(function(glyph) {
	// 				// this line is needed because gulp-iconfont has changed the api from 2.0
	// 				return { name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0) };
	// 			}),
	// 			fontName: 'mopic-glyphicon',
	// 			fontPath: '../images/fonts/', // set path to font (from your CSS file if relative)
	// 			className: 'icon', // set class name in your CSS
	// 			normalize: true,
	// 			fontHeight: 1001
	// 		};
	// 		gulp.src('./src/common/_glyph-template.scss')
	// 			.pipe(consolidate('lodash', options))
	// 			.pipe(rename({ basename: '_glyphicon' }))
	// 			.pipe(gulp.dest('./src/common/')); // set path to export your CSS

	// 		// CSS templating, e.g.
	// 		console.log(glyphs, options);
	// 	})
	// 	.pipe(gulp.dest('./fonts'));

    gulp.src(paths.FONTS)
		.pipe(gulp.dest(paths.RESOURCES + '/fonts'));
	
});




gulp.task('lint', function() {
    return gulp.src(paths.JS)
		.pipe(lint({config: './eslint.config.json'}))
		.pipe(lint.format());
});


gulp.task('watch', function() {
    gulp.watch(paths.CSS, ['css-without-clear']);
    gulp.watch(paths.JS, ['lint', 'js']);
    gulp.watch(paths.BI, ['css', 'bi']);
    gulp.watch(paths.OTHER_IMAGES, ['other-images']);
});


gulp.task('apply-prod-environment', function() {
    process.env.NODE_ENV = 'production';
});



gulp.task('default',
		  [
			  'apply-prod-environment',
			  'lint',
			  'js',
			  'css',
			  'bi',
			  'other-images',
			  'watch',
			  //'uglify'
		  ]);
