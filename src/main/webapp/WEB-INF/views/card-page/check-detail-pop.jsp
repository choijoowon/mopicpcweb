<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<div class="product_top">
    <div class="modal-header">
        <h4 class="modal-title">상품 상세보기</h4>
    </div>
    <div class="card-top clearfix">
        <div class="product_top_view">
			<img src="http://www.cardbaro.com/upload/card/${data.card_cd}.gif">
        </div>
        <div class="summary-area clearfix">
			<p class="card-name">${data.card_name}</p>
			<p class="card-copy">${data.headcopy}</p>
			<div class="dt-type">전월실적</div>
			<div class="dd-type">
				<ul>
					<li><span>${data.require_result }</span></li>
				</ul>
			</div>      		 
			<div class="dt-type">연회비</div>
			<div class="dd-type">
				<ul>
					<li><span>국내 : ${data.internal_annual_fee }원, 해외겸용 : ${data.foreign_annual_fee }원</span></li>
				</ul>
			</div>
			<div class="dt-type">브랜드</div>
			<div class="dd-type">
				<ul>
					<li><span>${data.brand_type }</span></li>
				</ul>
			</div>    
			<div class="btnGroup">
                <a class="btn btn-submit" href="${data.apply_url}" target="_blank" id="btn-homepage" role="button">가입하기</a>
                <c:if test="${session.isLogin eq 'N'}">
                	<a class="btn btn-submit" role="button" data-toggle="modal" data-target="#loginModal">관심상품</a>
                </c:if>
                <c:if test="${session.isLogin eq 'Y' and data.interest_yn eq 'N'}">
                	<a class="btn btn-submit" role="button" id="btn-interest" data-toggle="modal" data-product_type="4100" data-prod_code="${data.card_cd}">관심상품</a>
                </c:if>
            </div>
        </div> 
    </div>
</div>


<!-- 
01) to dev : 아이콘 네이밍 리스트 ex)<i class="benefits-icon-E01"></i>
.benefits-icon-E01 : 패밀리 레스토랑
.benefits-icon-E02 : 커피/베이커리
.benefits-icon-E03 : 패스트푸드
.benefits-icon-E04 : 주류
.benefits-icon-E06 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-A01 : 주유
.benefits-icon-A02 : 하이패스
.benefits-icon-A01 : 주차 
.benefits-icon-A04 : 보험
.benefits-icon-A05 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-A06 : 자동차구매/정비
.benefits-icon-S01 : 백화점/면세점
.benefits-icon-S02 : 인터넷/홈쇼핑
.benefits-icon-S03 : 대형마트/할인점
.benefits-icon-S04 : 편의점
.benefits-icon-S05 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-C01 : 영화
.benefits-icon-C02 : 공연/전시
.benefits-icon-C03 : 도서
.benefits-icon-C04 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-T01 : 테마파크
.benefits-icon-T02 : 호텔/리조트
.benefits-icon-T03 : 여행사
.benefits-icon-T04 : 경기관람
.benefits-icon-B01 : 통신
.benefits-icon-B02 : 금융
.benefits-icon-B03 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-T05 : 골프
.benefits-icon-T06 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-H01 : 의료
.benefits-icon-H02 : 학원비
.benefits-icon-H03 : 육아 
.benefits-icon-H04 : 아파트관리비
.benefits-icon-H05 : 뷰티
.benefits-icon-H06 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-F01 : 대중교통
.benefits-icon-F02 : 항공/마일리지
.benefits-icon-F03 : 기타 (뭐 들어갈지 정해 지지 않음)
.benefits-icon-P01 : 할인
.benefits-icon-P02 : 리워드
.benefits-icon-P03 : 포인트
.benefits-icon-P04 : 무료/우대
.benefits-icon-P05 : 프리미엄
.benefits-icon-P06 : 기타 (뭐 들어갈지 정해 지지 않음)

02) to dev : 혜택 타이틀 ex)<div class="bft-copy">전국 모든 백화점 5%할인</div>
03) <div class="benefit-item"></div> 단위로 루핑해 주시면 됩니다.
-->

<div class="product_down" id="gray-scroll">
    <div class="gray-scroll-in" >
        <div class="in_tb" >
            <p class="benefit-txt">주요혜택</p>
            <!-- start looping -->
            <c:choose>
				<c:when test="${!empty data.benefit}"> 
					<c:forEach var="row" items="${data.benefit}">
						<div class="benefit-item">
							<div class="h_tit">
								<i class="benefits-icon-${row.sub_category }"></i>
								<div class="bft-copy">${row.benefit_name }</div>
								<c:if test="${row.benefit_detail != null and row.benefit_detail != ''}">
									<span class="arr">up,down</span>
								</c:if>
							</div>
							<div class="cont">
								<table>
									<tr>
										<td class="title">${row.sub_category_nm }</td>
										<td>
											<c:if test="${row.benefit_explain_img_u != null and row.benefit_explain_img_u != ''}">
											<p class="img"><img src="${row.benefit_explain_img_u}"></p>
											</c:if>
											<c:if test="${row.benefit_detail != null and row.benefit_detail != ''}">
											<p>${row.benefit_detail}</p>
											</c:if>
											<c:if test="${row.benefit_explain_img_d != null and row.benefit_explain_img_d != ''}">
											<p class="img"><img src="${row.benefit_explain_img_d}"></p>
											</c:if>
										</td>	
									</tr>
								</table>
							</div>
						</div>
               		</c:forEach>
				</c:when>
            </c:choose>
            <!-- end looping -->
            <ul class="down">
		        <c:choose>
		            <c:when test="${!empty data.guide_sentence}">
		                <li>※ ${data.guide_sentence }</li>
					</c:when>
				</c:choose>
					<li>${data.approved_no_w }</li>
		    </ul>
        </div>
    </div>

</div>

<script type="text/javascript">
$("#gray-scroll .gray-scroll-in").mCustomScrollbar({
    setHeight:410,
    theme:"dark-3"
});
$('.benefit-item').click( function(e) {
    e.preventDefault();
    var _index = $(this).index()-1;
    if ( whCheck( $(this).find('.title').next().text() ) !== '') {
    	$('.benefit-item').not(':eq(' + _index + ')').removeClass('select');
    	$(this).toggleClass('select');
    }
});

function whCheck(str){
	var rule=/\s{1,}/g;
		return  str.split(rule).join("");
}
</script>















