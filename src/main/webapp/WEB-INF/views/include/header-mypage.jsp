<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=1280">

	<link rel="apple-touch-icon" sizes="57x57" href="/resources/images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/resources/images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/resources/images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/resources/images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/resources/images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/resources/images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/resources/images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/resources/images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/resources/images/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="/resources/images/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/resources/images/favicons/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="/resources/images/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="/resources/images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/resources/images/favicons/manifest.json">
	<link rel="mask-icon" href="/resources/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="/resources/images/favicons/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<script type="text/javascript" src="/resources/lib/webfont.js"></script>
    <script type="text/javascript">
        WebFont.load({
            custom: {
            families: ['notokr-regular', 'notokr-medium', 'notokr-light', 'notokr-bold'],
                urls: ['/resources/css/font.css', '/resources/css/font.css']
            }
        });
    </script>
    
    
  <!-- GA -->
  <script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
	{ (i[r].q=i[r].q||[]).push(arguments)}
	,i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-74135463-1', 'auto');
	ga('send', 'pageview');
	</script>

	
	<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/js/common.js"></script>
	<!--[if lt IE 9]>
		<script src="../lib/es5-shim/es5-shim.min.js"></script>
		<script src="../lib/es5-shim/es5-sham.min.js"></script>
		<script src="../lib/html5shiv/dist/html5shiv.js"></script>
	<![endif]-->
	
	<!-- css -->
	<link rel="stylesheet" href="/resources/lib/bootstrap/dist/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/resources/css/jquery.mCustomScrollbar.css" />
	<link rel="stylesheet" href="/resources/css/common.css" />
	<link rel="stylesheet" href="/resources/css/form.css" />
	<link rel="stylesheet" href="/resources/css/jquery.countdown.css" />
	<link rel="stylesheet" href="/resources/css/my-page-join.css" />
	
	
	<!-- JS -->
	<script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
	<script type="text/javascript" src="/resources/lib/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.mCustomScrollbar.concat.min.js"></script>