<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="/resources/lib/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/resources/lib/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript">
// Array find polyfill
if (!Array.prototype.find) {
	Array.prototype.find = function(predicate) {
		if (this === null) {
			throw new TypeError('Array.prototype.find called on null or undefined');
		}
		if (typeof predicate !== 'function') {
			throw new TypeError('predicate must be a function');
		}
		var list = Object(this);
		var length = list.length >>> 0;
		var thisArg = arguments[1];
		var value;

		for (var i = 0; i < length; i++) {
			value = list[i];
			if (predicate.call(thisArg, value, i, list)) {
				return value;
			}
		}
		return undefined;
	};
}
// Array findIndex polyfill
if (!Array.prototype.findIndex) {
	Array.prototype.findIndex = function(predicate) {
		if (this === null) {
			throw new TypeError('Array.prototype.findIndex called on null or undefined');
		}
		if (typeof predicate !== 'function') {
			throw new TypeError('predicate must be a function');
		}
		var list = Object(this);
		var length = list.length >>> 0;
		var thisArg = arguments[1];
		var value;

		for (var i = 0; i < length; i++) {
			value = list[i];
			if (predicate.call(thisArg, value, i, list)) {
				return i;
			}
		}
		return -1;
	};
}



if(!window.App) {
	window.App = {};
	window.App.isLoggedIn = '${session.isLogin}';
}
window.App = {
	query: { mainmenu: window.location.pathname.split('/')[1], submenu: window.location.pathname.split('/')[2], searchQuery: {${searchQuery}}, optCodeMap : {${optCodeMap}} },
	results: { productData : [${resultState.productData}], numberByFilter: [${resultState.filterStat}] },
	comparisons: { isLoggedIn: window.App.isLoggedIn === 'Y' },
	isFirstLogin: '${isFirstLogin}'
};


$(function() {
	$('body').tooltip({
		selector: '[class="badge"]',
		trigger:'click'
	});

	$('body').on('shown.bs.tooltip', function (e) {
		setTimeout(function(){
			$('.badge').tooltip('hide');
		}, 2500);
	});

	var pathname = location.pathname;
	if(pathname !== '/') {
		$('.gnb li a, .gnb_sub li a').each(function(){
			if ($(this).attr('href').indexOf(pathname) > -1) {
				$(this).addClass('active');
			}
		});
	}
});


</script>
<script src="/resources/js/common.bundle.js"></script>
<script src="/resources/js/common_menu.bundle.js"></script>



<c:if test="${isFirstLogin == 'Y'}">
<!-- first login modal container -->
<div id="first-time-container"></div>
<script src="/resources/js/getUserInfo.bundle.js"></script>
</c:if>

<!--[if lt IE 10]>
    <script>
        $('input[placeholder], textarea[placeholder]').placeholderEnhanced();
    </script>
<![endif]-->
