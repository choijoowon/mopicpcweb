<%@ page contentType = "text/html;charset=utf-8" %>

<div id="loginModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">

				<div class="form-group">
					<label class="sr-only" for="loginId">ID</label>
					<input type="email" class="form-control" id="loginId" placeholder="Enter email">
				</div>
				<div class="form-group">
					<label class="sr-only" for="loginPwd">Password</label>
					<input type="password" class="form-control" id="loginPwd" placeholder="Password">
				</div>

				<button id="loginSubmit" class="btn">로그인</button>
				<p><a id="lookUpId">아이디찾기</a> | <a id="resetPwd">비밀번호 재설정</a></p>
				
				<div>
					<button id="loginFb" class="btn btn-primary">페이스북으로 로그인</button>
					<p>아직 가입 전이신가요? <a id="signUp"> PASS가입하기</a></p>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
