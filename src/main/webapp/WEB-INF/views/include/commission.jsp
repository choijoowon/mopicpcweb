<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- yello login -->

<link rel="stylesheet" href="/resources/css/form.css" />


<div class="modal fade" id="calculator" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-large ">
			<a href="/" class="close" data-dismiss="modal" aria-label="Close" title="금융계산기 팝업 닫기"></a>
			<div class="modal-body modal-cal">
				<div class="mypage-out cal_top">
					<div class="mypage-left">
						<h2>금융계산기</h2>
						<div class="panel-group" id="accordion">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title select">
										<a class="pNode" href="saving-cont">저축</a>
									</h4>
								</div>
								<div id="saving-cont" class="panel-collapse collapse in">
									<div class="panel-body">
										<div class="carat"></div>
										<ul>
											<li class="select"><a class="endNode" href="saving-deposit" title="예금 보기">예금</a><li>
											<li><a class="endNode" href="saving-cal" title="적금 보기">적금</a></li>
										</ul>
									</div>
								</div>
							</div><!-- panel -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="pNode" href="loan-cont" title="대출 보기">대출</a>
									</h4>
								</div>
								<div id="loan-cont" class="panel-collapse collapse">
									<div class="panel-body">
										<div class="carat"></div>
										<ul>
											<li><a class="endNode" href="loan-principal-cal" title="대출원리금 보기">대출원리금</a><li>
											<li><a class="endNode" href="loan-house-cal" title="주택자금대출 부대비용 보기">주택자금대출 부대비용</a></li>
										</ul>
									</div>
								</div>
							</div><!-- panel -->
						</div><!-- panel-group -->
					</div><!-- mypage-left -->

					<!-- //저축/예금 -->
					<div class="cal-right select" id="saving-deposit">
						<div class="cal_input" >
							<div class="row">
								<div class="col-xs-6">											
									<label for="deposit-money" class="select-label">예치금액</label>
									<input type="text" name="deposit-money" id="deposit-money" class="text-input-normal txt_right" placeholder="100,000,000,000 원" required autofocus onfocus="if (this.placeholder == '100,000,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '100,000,000,000 원';}" disabled/>
								</div>	
							</div>
							<div class="row">
								<div class="col-xs-6">											
									<label for="deposit-period" class="select-label">예치기간</label>
									<input type="text" name="deposit-period" id="deposit-period" class="text-input-normal txt_right" placeholder="12개월" required autofocus onfocus="if (this.placeholder == '12개월') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '12개월';}"/>
								</div>	
							</div>
							<div class="row">
								<div class="col-xs-6">
									<label for="deposit-interest-rate" class="select-label">이자율</label>
									<input type="text" name="deposit-interest-rate" id="deposit-interest-rate" class="text-input-normal txt_right" placeholder="연 3.0%" required autofocus onfocus="if (this.placeholder == '연 3.0%') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '연 3.0%';}"/>
								</div>									
								<div class="col-xs-3" style="margin-left:-11px;">
									<label for="deposit-dan-bok" style="display:none;"></label>
									<select id="deposit-dan-bok" class="dropdown-sy">
										<option value="단리">단리</option>
										<option value="복리">복리</option>
									</select>
								</div>
							</div>
						</div><!-- cal_input -->

						<div class="right_btn"><a class="btn btn-submit" href="#" role="button">계산하기</a></div>

						<div class="result_cal">
							<p class="desc">입력하신 정보의 계산결과 입니다.</p>
							<table summary="저축-예금, 예치금액.예치기간.이자율.단리.복리에 따른 만기지급액,세후이자 컬럼으로 이루어진 표">
								<caption>저축-예금, 예치금액.예치기간.이자율.단리.복리에 따른 만기지급액,세후이자 컬럼으로 이루어진 표</caption>
								<colgroup>
									<col width="141px;" />
									<col width="259px;" />
									<col width="160px;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="separation">구분</th>
										<th scope="col">만기지급액</th>
										<th scope="col">세후이자</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">일반</th>
										<td>1,025,380 원</td>
										<td>25,380 원</td>
									</tr>
									<tr>
										<th scope="row">세금우대</th>
										<td>1,025,380 원</td>
										<td>25,380 원</td>
									</tr>
									<tr>
										<th scope="row">비과세</th>
										<td>1,025,380 원</td>
										<td>25,380 원</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div><!-- cal-right -->

					<!-- //저축/적금 -->
					<div class="cal-right" id="saving-cal">
						<ul class="nav nav-tabs-cal" role="tablist">
							<li role="presentation" class="left active"><a href="#saving-month" aria-controls="general" role="tab" data-toggle="tab">월 납입액 기준</a></li>
							<li role="presentation" class="right"><a href="#saving-expiry" aria-controls="comparison" role="tab" data-toggle="tab">만기지급액 기준</a></li>					
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="saving-month"><!-- 월 납입액 기준 -->
								<div class="cal_input" >
									<div class="row">
										<div class="col-xs-6">											
											<label for="saving-m-money" class="select-label">월 납입금액</label>
											<input type="text" name="saving-m-money" id="saving-m-money" class="text-input-normal txt_right" placeholder="100,000,000,000 원" required autofocus onfocus="if (this.placeholder == '100,000,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '100,000,000,000 원';}"/>
										</div>	
									</div>
									<div class="row">
										<div class="col-xs-6">											
											<label for="saving-goal-m-period" class="select-label">목표기간</label>
											<input type="text" name="saving-goal-m-period" id="saving-goal-m-period" class="text-input-normal txt_right" placeholder="12개월" required autofocus onfocus="if (this.placeholder == '12개월') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '12개월';}"/>
										</div>	
									</div>
									<div class="row">
										<div class="col-xs-6">											
											<label for="saving-m-rate" class="select-label">이자율</label>
											<input type="text" name="saving-m-rate" id="saving-m-rate" class="text-input-normal txt_right" placeholder="연 3.0%" required autofocus onfocus="if (this.placeholder == '연 3.0%') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '연 3.0%';}"/>
										</div>									
										<div class="col-xs-3" style="margin-left:-11px;">
											<label for="saving-m-dan-bok" style="display:none;">단리.복리</label>
											<select id="saving-m-dan-bok" class="dropdown-sy">
												<option value="단리">단리</option>
												<option value="복리">복리</option>
											</select>
										</div>
									</div>
								</div><!-- cal_input -->
								<div class="right_btn"><a class="btn btn-submit" href="#" role="button">계산하기</a></div>
								<div class="result_cal">
									<p class="desc">입력하신 정보의 계산결과 입니다.</p>
									<table summary="저축-예금, 예치금액.예치기간.이자율.단리.복리에 따른 월납익액을 기준으로 한 만기지급액,세후이자 컬럼으로 이루어진 표">
										<caption>저축-예금, 예치금액.예치기간.이자율.단리.복리에 따른 월납익액을 기준으로 한 만기지급액,세후이자 컬럼으로 이루어진 표</caption>
										<colgroup>
											<col width="141px;" />
											<col width="259px;" />
											<col width="160px;" />
										</colgroup>
										<thead>
											<tr>
												<th scope="col" class="separation">구분</th>
												<th scope="col">만기지급액</th>
												<th scope="col">세후이자</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">일반</th>
												<td>1,025,380 원</td>
												<td>25,380 원</td>
											</tr>
											<tr>
												<th scope="row">세금우대</th>
												<td>1,025,380 원</td>
												<td>25,380 원</td>
											</tr>
											<tr>
												<th scope="row">비과세</th>
												<td>1,025,380 원</td>
												<td>25,380 원</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div role="tabpanel" class="tab-pane" id="saving-expiry"><!-- 만기지급액 기준 -->
								<div class="cal_input" >
									<div class="row">
										<div class="col-xs-6">											
											<label for="saving-expiry-money" class="select-label">만기 지급액</label>
											<input type="text" name="saving-expiry-money" id="saving-expiry-money" class="text-input-normal txt_right" placeholder="100,000,000,000 원" required autofocus onfocus="if (this.placeholder == '100,000,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '100,000,000,000 원';}" disabled/>
										</div>	
									</div>

									<div class="row">
										<div class="col-xs-6">											
											<label for="saving-expiry-goal" class="select-label">목표기간</label>
											<input type="text" name="saving-expiry-goal" id="saving-expiry-goal" class="text-input-normal txt_right" placeholder="12개월" required autofocus onfocus="if (this.placeholder == '12개월') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '12개월';}" disabled/>
										</div>	
									</div>

									<div class="row">
										<div class="col-xs-6">											
											<label for="saving-expiry-rate" class="select-label">이자율</label>
											<input type="text" name="saving-expiry-rate" id="saving-expiry-rate" class="text-input-normal txt_right" placeholder="연 3.0%" required autofocus onfocus="if (this.placeholder == '연 3.0%') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '연 3.0%';}" disabled/>
										</div>									
										<div class="col-xs-3" style="margin-left:-11px;"> 
											<label for="saving-expiry-dan-bok" style="display:none;">단리.복리</label>
											<select id="saving-expiry-dan-bok" class="dropdown-sy">
												<option value="단리">단리</option>
												<option value="복리">복리</option>
											</select>
										</div>
									</div>
								</div><!-- cal_input -->

								<div class="right_btn"><a class="btn btn-submit" href="#" role="button">계산하기</a></div>

								<div class="result_cal">
									<p class="desc">입력하신 정보의 계산결과 입니다.</p>
									<table summary="저축-예금, 예치금액.예치기간.이자율.단리.복리에 따른 만기지급액,세후이자 컬럼으로 이루어진 표">
										<caption>저축-예금, 예치금액.예치기간.이자율.단리.복리에 따른 만기지급액,세후이자 컬럼으로 이루어진 표</caption>
										<colgroup>
											<col width="141px;" />
											<col width="259px;" />
											<col width="160px;" />
										</colgroup>
										<thead>
											<tr>
												<th scope="col" class="separation">구분</th>
												<th scope="col">만기지급액</th>
												<th scope="col">세후이자</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">일반</th>
												<td>1,025,380 원</td>
												<td>25,380 원</td>
											</tr>
											<tr>
												<th scope="row">세금우대</th>
												<td>1,025,380 원</td>
												<td>25,380 원</td>
											</tr>
											<tr>
												<th scope="row">비과세</th>
												<td>1,025,380 원</td>
												<td>25,380 원</td>
											</tr>
										</tbody>
									</table>
								</div><!-- result_cal -->
							</div>
						</div>
					</div><!-- cal-right -->

					<!-- //대출/대출원리금 -->
					<div class="cal-right" id="loan-principal-cal">
						<div class="cal_input" >
							<div class="row">
								<div class="col-xs-6">											
									<label for="loan-principal-money" class="select-label">대출금액</label>
									<input type="text" name="loan-principal-money" id="loan-principal-money" class="text-input-normal txt_right" placeholder="100,000,000,000 원" required autofocus onfocus="if (this.placeholder == '100,000,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '100,000,000,000 원';}"/>
								</div>	
							</div>
							<div class="row">
								<div class="col-xs-6">											
									<label for="loan-principal-period" class="select-label">대출기간</label>
									<input type="text" name="loan-principal-period" id="loan-principal-period" class="text-input-normal txt_right" placeholder="12개월" required autofocus onfocus="if (this.placeholder == '12개월') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '12개월';}"/>
								</div>	
							</div>
							<div class="row">
								<div class="col-xs-6">											
									<label for="loan-principal-rate" class="select-label">대출금리</label>
									<input type="text" name="loan-principal-rate" id="loan-principal-rate" class="text-input-normal txt_right" placeholder="연 3.0%" required autofocus onfocus="if (this.placeholder == '연 3.0%') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '연 3.0%';}"/>
								</div>									
								<div class="col-xs-3" style="margin-left:-11px;">
									<label for="loan-principal-dan-bok" style="display:none;">단리.복리</label>
									<select id="loan-principal-dan-bok" class="dropdown-sy">
										<option value="단리">단리</option>
										<option value="복리">복리</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 w-drop">											
									<label for="loan-principal-term" class="select-label">상환주기</label>
									<select id="loan-principal-term" class="dropdown-sy ">
										<option value="1개월">1개월</option>
										<option value="2개월">2개월</option>
									</select>
								</div>									
								<div class="col-xs-5 w-drop" style="margin-left:-11px;">
									<label><input type="text" name="loan-principal-date" id="loan-principal-date" class="text-input-normal" placeholder="2016-00-00" required autofocus onfocus="if (this.placeholder == '2016-00-00') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '2016-00-00';}"/></label>
									<a href="" class="btn_calender disabled"></a>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 w-drop">											
									<label for="loan-principal-how" class="select-label">상환방법</label>
									<select id="loan-principal-how" class="dropdown-sy ">
										<option value="원리금균등상환" >원리금균등상환</option>
										<option value="원리금균등상환">원리금균등상환</option>
									</select>
								</div>	
							</div>
						</div><!-- cal_input -->

						<div class="right_btn"><a class="btn btn-submit" href="#" role="button">계산하기</a></div>

						<div class="result_cal">
							<p class="desc">입력하신 정보의 계산결과 입니다.</p>
							<table summary="대출-대출원리금, 대출금액.기간.금리.상환주기.상환방법에 따른 회차.상환일.상환금.납입원금.이자.납입원금합계.잔금 컬럼으로 이루어진 표">
								<caption>대출-대출원리금, 대출금액.기간.금리.상환주기.상환방법에 따른 회차.상환일.상환금.납입원금.이자.납입원금합계.잔금 컬럼으로 이루어진 표</caption>
								<colgroup>
									<col width="45px;" />
									<col width="89px;" />
									<col width="80px;" />
									<col width="80px;" />
									<col width="80px;" />
									<col width="90px;" />
									<col width="90px;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col">회차</th>
										<th scope="col">상환일</th>
										<th scope="col">상환금</th>
										<th scope="col">납입원금</th>
										<th scope="col">이자</th>
										<th scope="col">납입원금합계</th>
										<th scope="col">잔금</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>2015-10-18</td>
										<td>290,812 원</td>
										<td>265,812 원</td>
										<td>25,000 원</td>
										<td>265,812 원</td>
										<td>9,734,188 원</td>
									</tr>
								</tbody>
							</table>

						</div><!-- result_cal -->
					</div><!-- cal-right -->

					<!-- //대출/주택자금대출 -->
					<div class="cal-right" id="loan-house-cal">
						<div class="cal_input" >
							<div class="row">
								<div class="col-xs-6">											
									<label for="loan-house-money" class="select-label">대출금액</label>
									<input type="text" name="loan-house-money" id="loan-house-money" class="text-input-normal txt_right" placeholder="100,000,000,000 원" required autofocus onfocus="if (this.placeholder == '100,000,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '100,000,000,000 원';}"/>
								</div>	
							</div>
							<div class="row">
								<div class="col-xs-6 w-drop">											
									<label for="loan-house-how" class="select-label">상환방법</label>
									<select id="loan-house-how" class="dropdown-sy ">
										<option value="원리금균등상환" >원리금균등상환</option>
										<option value="원리금균등상환">원리금균등상환</option>
									</select>
								</div>	
							</div>

						</div><!-- cal_input -->

						<div class="right_btn"><a class="btn re-cal btn-submit" href="#" role="button">다시 계산하기</a></div>

						<div class="result_cal">
							<p class="desc"><span class="emp">1,000,000</span>원의 <span class="emp">전세자금대출</span>의 계산결과 입니다.
							<table summary="저축-주택자금대출 부대비용, 대출금액.상환방법에 따른 전세자금대출(1,000,000원의 전세자금대출) 결과로 이루어진 표">
								<caption>저축-주택자금대출 부대비용, 대출금액.상환방법에 따른 전세자금대출(1,000,000원의 전세자금대출) 결과로 이루어진 표</caption>
								<colgroup>
									<col width="141px;" />
									<col width="100px;" />
									<col width="308px;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" class="separation">구분</th>
										<th scope="col" colspan="2">금액</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">수입인지대금</th>
										<td colspan="2">채무자와 은행이 각각 50 % 씩 부담</td>
									</tr>
									<tr>
										<th scope="colgroup" rowspan="2"> 
											주택금융신용보증서<br/>
											보증수수료<br/>
											(기준보증료율<br/>
											+ 차등보증료율)
										</th>
										<td>기준보증료율</td>
										<td>발급금액의 0.2 ~ 0.5 %</td>
									</tr>
									<tr>
										<td>차등보증료율</td>
										<td class="bor_none">
											한국주택금융공사의 고객별 보증 CSS 등급에<br/>
											따라 차등적용 (보증 CSS 등급 관련문의 : <br/>
											한국주택금융공사 1688-8114)
										</td>
									</tr>
								</tbody>
							</table>

						</div><!-- result_cal -->
					</div><!-- cal-right -->

				</div><!-- mypage-out -->

			</div><!-- modal-body -->
		</div><!-- modal-content -->
	</div><!-- modal-dialog -->
</div><!-- modal -->

<div id="commission" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content modal-large ">
			<a href="/" class="close" data-dismiss="modal" aria-label="Close" title="수수료 팝업창 닫기"></a>
			<div class="modal-body modal-cal">
				<div class="mypage-out cal_top">
					<div class="mypage-left">
						<h2>수수료</h2>
						<div class="panel-group" id="accordion" role="tablist">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title none select">
										<a class="endNode" href="send-fee" title="송금수수료">송금수수료</a>
									</h4>
								</div>
							</div><!-- panel -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title none">
										<a class="endNode" href="atm-fee" title="ATM수수료 보기">ATM수수료</a>
									</h4>
								</div>
							</div><!-- panel -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title none">
										<a class="endNode" href="etc-fee" title="기타수수료 보기">기타수수료</a>
									</h4>
								</div>
							</div><!-- panel -->					
						</div><!-- panel-group -->
					</div><!-- mypage-left -->

					<!-- //송금수수료 -->
					<div class="cal-right select" id="send-fee">
						<div class="cal_input" >
							<div class="row">
								<div class="col-xs-12 w-drop">											
									<label for="select" class="select-label">은행종류<!-- <span class="badge" data-toggle="tooltip" data-placement="bottom" title="툴팁 내용">?</span> --></label>
									<input type="hidden" name="ch_etcclass_send" id="ch_etcclass_send" value="">
									<select class="dropdown-sy" name="bank_send" id="bank_send">
										<option value="">전체</option>
										<c:choose>
										<c:when test="${!empty charageCompanyOpts}"> 
											<c:forEach var="row" items="${charageCompanyOpts}">
												<option value="${ row.ch_bank}" >${ row.bank_nm}</option>
											</c:forEach>
										</c:when>
										</c:choose>
									</select>
								</div>
							</div>
							<div class="row" style="margin:10px 0 0 0;">
								<div class="col-xs-12 w-drop">											
									<label class="select-label">송금은행</label>
									<div class="radio_normal">
										<div class="radio-top1">
											<div class="radio-bottom radio-bottom5" style="margin-right:29px;">
												<span>
												  <input type="radio" id="gubun_same_sbank" name="gubun_send" value="0" checked />
												  <label for="gubun_same_sbank"><i></i>같은은행</label>
												</span>
											</div>
											<div class="radio-bottom radio-bottom5">
												<span>
												  <input type="radio" id="gubun_diff_dbank" name="gubun_send" value="1" />
												  <label for="gubun_diff_dbank"><i></i>다른은행</label>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="right_btn"><a class="btn btn-submit" href="#" role="button" onclick="selectChargeList('send');">검색하기</a></div>

						<div class="result_cal" style="margin-bottom:25px;">
							<p class="desc">입력하신 정보의 검색결과 입니다.</p>
							<table summary="은행별 자동화기기, 텔레뱅킹, 모바일뱅킹에서 인출 전,후로 인출한 금액으로 이루어진 송금수수료 표">
								<caption>은행별 자동화기기, 텔레뱅킹, 모바일뱅킹에서 인출 전,후로 인출한 금액으로 이루어진 송금수수료 표</caption>
								<colgroup>
									<col width="110px;" />
									<col width="74px;" />
									<col width="74px;" />
									<col width="74px;" />
									<col width="74px;" />
									<col width="74px;" />
									<col width="74px;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" rowspan="2">은행</th>
										<th scope="col" rowspan="2">창구이용</th>
										<th scope="colgroup" colspan="2">자동화기기</th>
										<th scope="col" rowspan="2">인터넷뱅킹</th>
										<th scope="col" rowspan="2">텔레뱅킹<br/>(ARS이용시)</th>
										<th scope="col" rowspan="2">모바일뱅킹</th>
									</tr>
									<tr class="col">
										<th scope="col">마감 전</th>
										<th scope="col">마감 후</th>
									</tr>
								</thead>
								<tbody id="listLayer_send"></tbody>
							</table>
						</div><!-- result_cal -->
					</div>

					<!-- //ATM수수료 -->
					<div class="cal-right" id="atm-fee">
						<div class="cal_input" >
							<div class="row">
								<div class="col-xs-12 w-drop">											
									<label for="gubun_atm" class="select-label">은행종류<!-- <span class="badge" data-toggle="tooltip" data-placement="bottom" title="툴팁 내용">?</span> --></label>
									<input type="hidden" name="ch_etcclass_atm" id="ch_etcclass_atm" value="">
									<input type="radio" id="gubun_atm" name="gubun_atm" value="2" checked style="display:none" />
									<select class="dropdown-sy " name="bank_atm" id="bank_atm">
										<option value="">전체</option>
										<c:choose>
										<c:when test="${!empty charageCompanyOpts}"> 
											<c:forEach var="row" items="${charageCompanyOpts}">
												<option value="${ row.ch_bank}" >${ row.bank_nm}</option>
											</c:forEach>
										</c:when>
										</c:choose>
									</select>
								</div>
							</div>
						</div><!-- cal_input -->
						<div class="right_btn"><a class="btn btn-submit" href="#" role="button" onclick="selectChargeList('atm');">검색하기</a></div>

						<div class="result_cal" style="margin-bottom:25px;">
							<p class="desc">입력하신 정보의 검색결과 입니다.</p>
							<table summary="인출은행 마감 전,후 인출금액으로 이루어진 ATM수수료 표">
								<caption>인출은행 마감 전,후 인출금액으로 이루어진 ATM수수료 표</caption>
								<colgroup>
									<col width="140px;" />
									<col width="104px;" />
									<col width="104px;" />
									<col width="104px;" />
									<col width="104px;" />
								</colgroup>
								<thead>
									<tr>
										<th scope="col" rowspan="2">은행</th>
										<th scope="colgroup" colspan="2">같은은행에서 인출시</th>
										<th scope="colgroup" colspan="2">다른은행에서 인출시</th>
									</tr>
									<tr class="col">
										<th scope="col">마감 전</th>
										<th scope="col">마감 후</th>
										<th scope="col">마감 전</th>
										<th scope="col">마감 후</th>
									</tr>
								</thead>
								<tbody id="listLayer_atm">
								</tbody>
							</table>

						</div><!-- result_cal -->
					</div>

					<!-- //기타수수료 -->
					<div class="cal-right" id="etc-fee">
						<div class="cal_input">
							<div class="row">
								<div class="col-xs-12 w2-drop">				
									<input type="radio" id="gubun_etc" name="gubun_etc" value="3" checked style="display:none" />
									<select tabindex="4" class="dropdown-sy " name="ch_etcclass_etc" id="ch_etcclass_etc">
										<option value="CH010" >자기앞수표 발행수수료 정액권</option>
									</select>
								</div>
							</div>
						</div><!-- cal_input -->

						<div class="right_btn"><a class="btn btn-submit" href="#" role="button" onclick="selectChargeList('etc');">검색하기</a></div>

						<div class="result_cal" style="margin-bottom:25px;">
							입력하신 정보의 검색결과 입니다.
							<table summary="은행별 자동화기기, 텔레뱅킹, 모바일뱅킹에서 인출 전,후로 인출한 금액으로 이루어진 송금수수료 표">
								<caption>은행별 자동화기기, 텔레뱅킹, 모바일뱅킹에서 인출 전,후로 인출한 금액으로 이루어진 송금수수료 표</caption>
								<colgroup>
									<col width="150px;" />
									<col width="120px;" />
									<col width="120px;" />
									<col width="240px;"/>
								</colgroup>
								<thead>
									<tr>
										<th scope="col" >은행</th>
										<th scope="col" >기준</th>
										<th scope="col" >금액</th>
										<th scope="col" >기타</th>
									</tr>
								</thead>
								<tbody id="listLayer_etc"></tbody>
							</table>
						</div><!-- result_cal -->

					</div>


				</div><!-- mypage-out -->
			</div><!-- modal-body -->
		</div><!-- modal-content -->
	</div><!-- modal-dialog -->
</div><!-- modal -->

<script type="text/javascript" src="/resources/lib/jquery.placeholder.js"></script>
<script type="text/javascript" src="/resources/lib/jquery.easydropdown.js"></script>
<script type="text/javascript" src="/resources/lib/jquery.easydropdown_scroll.js"></script>
		
<script async>

_key_set = [
            	["bank_nm","ch_sbcounter","ch_sbautoopen","ch_sbautoclose","ch_sbinbank","ch_sbtelbank","ch_sbmobank"],
            	["bank_nm","ch_obcounter","ch_obautoopen","ch_obautoclose","ch_obinbank","ch_obtelbank","ch_obmobank"],
            	["bank_nm","ch_bankopen","ch_bankclose","ch_obankopen","ch_obankclose"],
            	["bank_nm","ch_chbasic","ch_pay","ch_etc"]
            ];
            
_unit_set = [
        	["","원","원","원","원","원","원"],
        	["","원","원","원","원","원","원"],
        	["","원","원","원","원"],
        	["","","원",""]
        ];            

function selectChargeList(gubun){
	
	var data = {
		ch_bank : $('#'+'bank_'+gubun).val(),
		ch_etcclass : $('#'+'ch_etcclass_'+gubun).val()
	};
	
	check_idx = $(':radio[name=gubun_'+gubun+']:checked').val(); 

	$.ajax({
		type: 'POST',
		url: '/mopic/api/etc/selectAccountCharge',
		contentType: 'application/json;charset=UTF-8',
		data: JSON.stringify(data),
		dataType: 'json',
		success: function(response) {

			var $chargeList = response.result.data;
			var tr = '';
			
			for ( i = 0 ; i < $chargeList.length ; i++) {
				
				var charge = $chargeList[i];
				
				tr += "<tr>";
				
				for ( j = 0 ; j < _key_set[check_idx].length ; j++ ){
					tr += "<td>"+charge[_key_set[check_idx][j]]+_unit_set[check_idx][j]+"</td>";
				}
				tr += "</tr>";
				
			}
			
			$("#listLayer_"+gubun).html(tr);
			
		},
		error: function(jqXHR, textStatus, errorThrown) {
		}
	});
}
</script>
<script type="text/javascript">
	$('.modal').on('show.bs.modal', function (e) {
		$(e.target).find('tbody').children().remove();
		if ( e.target.id == 'commission' ){
			//수수료 
			$("#commission .panel .panel-title").removeClass('select');
			$("#commission .panel:eq(0) .panel-title").addClass('select');
			$("#commission .cal-right").removeClass('select');
			$("#send-fee").addClass('select');
		} else {
			//계산기
			$('.modal .panel-default .panel-collapse').slideUp();
			$("#calculator .panel-title").removeClass('select');
			$("#calculator .panel-collapse li").removeClass('select');

			$("#calculator .panel:eq(0) h4").addClass('select');
			$('#saving-cont li:eq(0)').addClass('select');
			$('#saving-cont').slideDown(100);

			$("#calculator .cal-right").removeClass('select');
			$("#saving-deposit").addClass('select');
		}
	});

	//$(".result_cal table").mCustomScrollbar({ setHeight:300 });

	$(".modal .panel a.endNode").click( function(e) {
		e.preventDefault();
		$(".modal .panel-title").removeClass('select');
		$(e.currentTarget).parent().addClass('select');
		$(e.currentTarget).parent().siblings().removeClass('select');

		$('.cal-right').removeClass('select');
		$('#' + $(e.currentTarget).attr('href')).addClass('select');

		//$('h4.panel-title').removeClass('select');
		var _id = $(e.currentTarget).attr('href').split('-')[0] + '-cont';
		$("a.pNode[href='" + _id + "']").parent().addClass('select');
	} );

	$('.modal a.pNode').click( function(e) {
		e.preventDefault();
		if ( !$(e.currentTarget).parent().hasClass('select') ){
			$('.modal .panel-default .panel-collapse').slideToggle(100);
			//$('#' + $(e.currentTarget).attr('href')).slideDown(100);
			$('#' + $(e.currentTarget).attr('href') + ' li').removeClass('select');
		} else {
			if ( !$('#' + $(e.currentTarget).attr('href')).hasClass('in') ){
				//$('.modal .panel-default .panel-collapse').slideUp();	
				//$('#' + $(e.currentTarget).attr('href')).slideDown(100);
			}
		}
	} );
</script>


