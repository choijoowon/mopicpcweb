<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="wrapper">
	<!-- top menu -->
	<div class="header<c:if test="${fn:substring(session.requestUrl, 0,7) == '/mypage'}"> main-h</c:if>">
		<div class="head_inner">
			<h1 class="logo"><a href="/"></a></h1>
			<div class="top-menu">	
				<ul class="navbar-nav">
					<c:choose>
						<c:when test="${session.isLogin == 'N'}">
							<li><a href="#" data-toggle="modal" data-target="#loginModal">로그인 </a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/member/logout?redirectUrl=">로그아웃 </a></li>
						</c:otherwise>
					</c:choose>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">마이페이지 <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
						<c:choose>
							<c:when test="${session.isLogin == 'N'}">
								<li><a href="#" data-toggle="modal" data-target="#loginModal" data-redirect_url="/mypage/myProduct">가입상품</a></li>
								<li><a href="#" data-toggle="modal" data-target="#loginModal" data-redirect_url="/mypage/myInterest">관심상품</a></li>
								<li><a href="#" data-toggle="modal" data-target="#loginModal" data-redirect_url="/mypage/inquireList">1:1문의</a></li>
								<li><a href="#" data-toggle="modal" data-target="#loginModal" data-redirect_url="/mypage/serviceInfo">회원정보관리</a></li>
							</c:when>
							<c:otherwise>
								<li><a href="/mypage/myProduct">가입상품(${myProduct.data.TOTAL1})</a></li>
								<li><a href="/mypage/myInterest">관심상품(${myProduct.data.TOTAL2})</a></li>
								<li><a href="/mypage/inquireList">1:1문의</a></li>
								<!-- <li class="divider"></li> -->
								<li><a href="/mypage/serviceInfo">회원정보관리</a></li>
							</c:otherwise>
						</c:choose>
						</ul>
					</li>
					<!-- 				
					<li><a href="#">관심상품</a></li>
					 -->
				</ul>
				<ul class="navbar-nav navbar-right">

					<li><a href="javascript:alert('진행중인 이벤트가 없습니다.');">이벤트</a></li>
					<li class="dropdown last">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">고객센터 <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/information/noticeList">공지사항</a></li>
							<li><a href="/information/faqList">FAQ</a></li>
							<!-- 
							<li><a href="#" class="second" data-toggle="modal" data-target="#calculator" >&nbsp;</a></li>
							<li><a href="#" class="second" data-toggle="modal" data-target="#commission" >&nbsp;</a></li>
							 -->
							<!-- <li class="divider"></li> -->
						</ul>
					</li>
				</ul>
			</div>	
			
			<ul class="top-menu-right">
			<!-- 
				<li><a href="#" class="first" data-toggle="modal" data-target="#calculator">계산기</a></li>
			 -->
				<li><a href="javascript:alert('준비 중 입니다.');" class="first">계산기</a></li>
				<li><a href="#" class="second" data-toggle="modal" data-target="#commission" >수수료</a></li>
				<li><a href="javascript:alert('준비 중 입니다.');" class="third">지점찾기</a></li>
			</ul>
			
			<div class="gnb">
				<ul class="gnb-list clearfix">
					<li class="gnb-item item1">
						<a href="/saving/deposit?amount=1000&periodCd=A0001010103" class="title savings"><span class="text">저축</span></a>
						<ul class="gnb-depth2">
							<li><a href="/saving/deposit?amount=1000&periodCd=A0001010103">예금</a></li>
							<li><a href="/saving/saving?amount=100&periodCd=A0001020104">적금</a></li>
							<li><a href="/saving/mmda?amount=1000">MMDA</a></li>
							<li><a href="/saving/house?amount=20&periodCd=A0001040102">주택청약</a></li>
						</ul>
					</li>
					<li class="gnb-item item2">
						<a href="/loan/signature?amount=1000" class="title loan"><span class="text">대출</span></a>
						<ul class="gnb-depth2">
							<li><a href="/loan/signature?amount=1000">신용대출</a></li>
							<li><a href="/loan/secured?secureType=99999999">담보대출</a></li>
							<!-- <li><a href="#">맞춤대출</a></li> -->
						</ul>
					</li>
					<li class="gnb-item item3">
						<a href="/ins/shilson?birthConvertToAge=800101&dambo1=999999991&sex=1" class="title insure"><span class="text">보험</span></a>
						<ul class="gnb-depth2 pd22">
							<li><a href="/ins/shilson?birthConvertToAge=800101&dambo1=999999991&sex=1">실손보험</a></li>
							<li><a href="/ins/life?sex=1">정기보험</a></li>
							<li><a href="/ins/annuity?age=33022555&sex=1">연금/저축보험</a></li>
							<li><a href="/ins/car">자동차보험</a></li>
						</ul>
					</li>
					<li class="gnb-item item4">
						<a href="/card/credit?benefits=C01%7CB01%7CF01&company=all&fee=C&month_sum=50" class="title card"><span class="text">카드</span></a>
						<ul class="gnb-depth2">
							<li><a href="/card/credit?benefits=C01%7CB01%7CF01&company=all&fee=C&month_sum=50">신용카드</a></li>
							<li><a href="/card/check?benefits=C01%7CB01%7CF01&company=all&fee=B&month_sum=50">체크카드</a></li>
						</ul>
					</li>
					<li class="gnb-item item5">
						<a href="/p2p/invest?company_code=99999999" class="title p2p"><span class="text">P2P</span></a>
						<ul class="gnb-depth2">
							<li><a href="/p2p/invest?company_code=99999999">투자하기</a></li>
							<li><a href="/p2p/ploan?gubun=L200001&company_code=99999999">대출받기</a></li>
						</ul>
					</li>
					<li class="gnb-item item6 last">
						<a href="/exchange/exchange?money=1000000&tr_nation=USD&exchange_type=TR_BUY_CASH&coupon=70" class="title exchange"><span class="text">환율</span></a>
						<ul class="gnb-depth2">
							<li></li>
							<li></li>
						</ul>
					</li>
				</ul>

				<!-- 서브메뉴 -->
				<div class="gnb_sub">
					<div class="gnb-list_sub">
						<ul class="gnb-depth2_sub clearfix">
						<c:choose>
						<c:when test="${fn:indexOf(session.requestUrl, '/ins/') != -1}">
							<!-- 보험 서브메뉴 -->
							<li><a href="/ins/shilson?birthConvertToAge=800101&dambo1=999999991&sex=1">실손보험</a></li>
							<li><a href="/ins/life?sex=1">정기보험</a></li>
							<li><a href="/ins/annuity?age=33022555&sex=1">연금/저축보험</a></li>
							<li><a href="/ins/car">자동차보험</a></li>
						</c:when>
						<c:when test="${fn:indexOf(session.requestUrl, '/card/') != -1}">
							<!-- 카드 서브메뉴 -->
							<li><a href="/card/credit?benefits=C01%7CB01%7CF01&company=all&fee=C&month_sum=50">신용카드</a></li>
							<li><a href="/card/check?benefits=C01%7CB01%7CF01&company=all&fee=B&month_sum=50">체크카드</a></li>
						</c:when>
						<c:when test="${fn:indexOf(session.requestUrl, '/saving/') != -1}">
							<!-- 카드 서브메뉴 -->
							<li><a href="/saving/deposit?amount=1000&periodCd=A0001010103">예금</a></li>
							<li><a href="/saving/saving?amount=100&periodCd=A0001020104">적금</a></li>
							<li><a href="/saving/mmda?amount=1000">MMDA</a></li>
							<li><a href="/saving/house?amount=20&periodCd=A0001040102">주택청약</a></li>
						</c:when>
						<c:when test="${fn:indexOf(session.requestUrl, '/loan/') != -1}">
							<!-- 대출 서브메뉴 -->
							<li><a href="/loan/signature?amount=1000">신용대출</a></li>
							<li><a href="/loan/secured?secureType=99999999">담보대출</a></li>
						</c:when>
						<c:when test="${fn:indexOf(session.requestUrl, '/p2p/') != -1}">
							<!-- p2p 서브메뉴 -->
							<li><a href="/p2p/invest?company_code=99999999">투자하기</a></li>
							<li><a href="/p2p/ploan?gubun=L200001&company_code=99999999">대출받기</a></li>
						</c:when>
						<c:otherwise>
						</c:otherwise>
						</c:choose>
						</ul>
					</div>
				</div>
				<!-- 서브메뉴 end -->
			</div>

		</div>

		<div class="gnb-add"></div>

	</div>
<!-- top menu end -->
</div>
