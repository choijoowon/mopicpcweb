<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<c:if test="${fn:indexOf(session.requestUrl, '/mypage/') > -1}">
		<script type="text/javascript">
		$(function() {
			/*
			$.ajax({
				type : "POST"
				, url : "/mopic/api/mypage/selectMyProductListCnt"
				, contentType : "application/json;charset=UTF-8"
				, data : "{}"
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						//alert(JSON.stringify(result));
						var data = result.data;
						
						$("#total1").text(data.TOTAL1);
						$("#saving1").text(data.SAVING1);
						$("#loan1").text(data.LOAN1);
						$("#insure1").text(data.INSURE1);
						$("#card1").text(data.CARD1);
						
						$("#total2").text(data.TOTAL2);
						$("#saving2").text(data.SAVING2);
						$("#loan2").text(data.LOAN2);
						$("#insure2").text(data.INSURE2);
						$("#card2").text(data.CARD2);
						$("#p2p2").text(data.P2P2);
					}
				}
			});
			*/
		});
		</script>
	</c:if>
	
		<c:if test="${fn:indexOf(session.requestUrl, '/information/') > -1}">
		<div class="mypage-left">
			<h2>고객센터</h2>
			<div class="panel-group" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title none<c:if test="${session.requestUrl == '/information/noticeList'}"> select</c:if>">
							<a data-parent="#accordion" href="/information/noticeList" aria-expanded="true" aria-controls="collapseOne">
							 공지사항
							</a>
						</h4>
					</div>
				</div><!-- panel -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title none<c:if test="${session.requestUrl == '/information/faqList'}"> select</c:if>">
							<a class="collapsed" data-parent="#accordion" href="/information/faqList" aria-expanded="false" aria-controls="collapseTwo">
							  FAQ
							</a>
						</h4>
					</div>
				</div><!-- panel -->				
			</div><!-- panel-group -->
		</div><!-- mypage-left -->
		</c:if>
		<c:if test="${fn:indexOf(session.requestUrl, '/mypage/') > -1}">
			<div class="mypage-left">
				<h2>마이페이지</h2>

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<c:choose>
								<c:when test="${session.requestUrl == '/mypage/myProduct'}">
									<h4 class="panel-title select">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										 가입상품 (<span id="total1">${myProduct.data.TOTAL1}</span>)
										</a>
									</h4>
								</c:when>
								<c:otherwise>
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
										 가입상품 (<span id="total1">${myProduct.data.TOTAL1}</span>)
										</a>
									</h4>
								</c:otherwise>
							</c:choose>
						</div>
						<div id="collapseOne" class="panel-collapse collapse <c:if test="${session.requestUrl == '/mypage/myProduct'}"> in</c:if>" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<div class="carat"></div>
								<ul>
									<li><a href="/mypage/myProduct?type=SAVING&category=A000101&gubun=30000001" <c:if test="${type == 'SAVING'}"> class="select"</c:if>>저축 (<span id="saving1">${myProduct.data.SAVING1}</span>)</a><li>
									<li><a href="/mypage/myProduct?type=LOAN&category=B000101&gubun=30000001" <c:if test="${type == 'LOAN'}"> class="select"</c:if>>대출 (<span id="loan1">${myProduct.data.LOAN1}</span>)</a></li>
									<li><a href="/mypage/myProduct?type=INSURE&category=SHILSON&gubun=30000001" <c:if test="${type == 'INSURE'}"> class="select"</c:if>>보험 (<span id="insure1">${myProduct.data.INSURE1}</span>)</a></li>
									<li><a href="/mypage/myProduct?type=CARD&category=1&gubun=30000001" <c:if test="${type == 'CARD'}"> class="select"</c:if>>카드 (<span id="card1">${myProduct.data.CARD1}</span>)</a></li>
								</ul>
							</div>
						</div>
					</div><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<c:choose>
								<c:when test="${session.requestUrl == '/mypage/myInterest'}">
									<h4 class="panel-title select">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
										  관심상품 (<span id="total2">${myProduct.data.TOTAL2}</span>)
										</a>
									</h4>
								</c:when>
								<c:otherwise>
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										  관심상품 (<span id="total2">${myProduct.data.TOTAL2}</span>)
										</a>
									</h4>
								</c:otherwise>
							</c:choose>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse<c:if test="${session.requestUrl == '/mypage/myInterest'}"> in</c:if>" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<div class="carat"></div>
								<ul>
									<li><a href="/mypage/myInterest?type=SAVING&category=A000101&gubun=30000002" <c:if test="${type == 'SAVING'}"> class="select"</c:if>>저축 (<span id="saving2">${myProduct.data.SAVING2}</span>)</a><li>
									<li><a href="/mypage/myInterest?type=LOAN&category=B000101&gubun=30000002" <c:if test="${type == 'LOAN'}"> class="select"</c:if>>대출 (<span id="loan2">${myProduct.data.LOAN2}</span>)</a></li>
									<li><a href="/mypage/myInterest?type=INSURE&category=SHILSON&gubun=30000002" <c:if test="${type == 'INSURE'}"> class="select"</c:if>>보험 (<span id="insure2">${myProduct.data.INSURE2}</span>)</a></li>
									<li><a href="/mypage/myInterest?type=CARD&category=1&gubun=30000002" <c:if test="${type == 'CARD'}"> class="select"</c:if>>카드 (<span id="card2">${myProduct.data.CARD2}</span>)</a></li>
									<li><a href="/mypage/myInterest?type=P2P&category=L200003&gubun=30000002" <c:if test="${type == 'P2P'}"> class="select"</c:if>>P2P (<span id="p2p2">${myProduct.data.P2P2}</span>)</a></li>
								</ul>
							</div>
						</div>
					</div><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThree">
							<c:choose>
								<c:when test="${session.requestUrl == '/mypage/inquireList'}">
									<h4 class="panel-title select">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
										 1:1 문의
										</a>
									</h4>
								</c:when>
								<c:otherwise>
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										 1:1 문의
										</a>
									</h4>
								</c:otherwise>
							</c:choose>
						</div>
						<div id="collapseThree" class="panel-collapse collapse<c:if test="${session.requestUrl == '/mypage/inquireList' or session.requestUrl == '/mypage/inquire'}"> in</c:if>" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<div class="carat"></div>
								<ul>
									<li><a href="/mypage/inquire" <c:if test="${session.requestUrl == '/mypage/inquire'}">  class="select"</c:if>>1:1 문의하기</a><li>
									<li><a href="/mypage/inquireList" <c:if test="${session.requestUrl == '/mypage/inquireList'}">  class="select"</c:if>>1:1 문의내역 확인</a></li>
								</ul>
							</div>
						</div>
					</div><!-- panel -->
<!-- 
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFour">
							<h4 class="panel-title none">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
								 회원정보관리
								</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour"></div>
					</div> --><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFive">
							<h4 class="panel-title none<c:if test="${session.requestUrl == '/mypage/serviceInfo'}"> select</c:if>">
								<a class="collapsed" data-parent="#accordion" href="/mypage/serviceInfo" aria-expanded="false" aria-controls="collapseFive">
								 회원정보관리
								</a>
							</h4>
						</div>
						<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive"></div>
					</div><!-- panel -->

				</div><!-- panel-group -->
			</div><!-- mypage-left -->
		</c:if>