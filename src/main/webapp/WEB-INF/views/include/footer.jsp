<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
		
	<div class="footer">
		<div class="inner">
			<ul>
				<li class="copy_logo"></li>
				<li><a href="/main/whatismopic" title="모픽이란?">모픽이란?</a></li>
				<li class="divide">|</li>
				<li><a href="/main/serviceTerm" title="서비스약관" target="_blank">서비스약관</a></li>
				<li class="divide">|</li>
				<li><a href="/main/privacy" title="개인정보취급방침" target="_blank">개인정보취급방침</a></li>
				<li class="divide">|</li>
				<li class="last">(주)옐로마켓플레이스</li>
			</ul>
			<ul class="copy">
				<li>서울특별시 영등포구 국제금융로 10, 19층(여의도동, Three IFC)</li>
				<li class="divide">|</li>
				<li>대표이사 : 이찬기</li>
				<li class="divide">|</li>
				<li>사업자 등록번호 : 290-88-00033</li><br/>
				<li class="last">이메일 문의 : <a href="mailto:mopic_info@yellofg.com"> mopic_info@yellofg.com</a></li>
			</ul>
			<p class="warn-info">
				본 금융상품 정보는 단순히 정보의 제공을 목적으로 하고 있으며, 사이트에서 제공되는 정보에 오류가 발생될 수 있습니다.<br/>
				제공된 정보이용에 따르는 책임은 이용자 본인에게 있으며, 당사는 이용자의 투자결과에 따른 법적책임을 지지 않습니다.<br/>
			</p>
			<p class="copyright">COPYRIGHTⓒYello Financial Group ALL RIGHTS RESERVED.</p>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
			var _htmlH = $('html').height();
			var _winH = $(window).height();
			if ( _htmlH <= _winH ){
				if ( $('.container .results-wrap').length > 0 ){
					var _rwH =  $('.container .results-wrap').height();
					$('.container .results-wrap').css('min-height' , (_rwH + (_winH - _htmlH)) +'px' );
				}
				if ( $('.mypage-out').length > 0 ){
					var _moH =  $('.mypage-out').height();
					$('.mypage-out').css( 'min-height' , (_moH + (_winH - _htmlH)) + 'px' );
				}
			}
		});
	</script>