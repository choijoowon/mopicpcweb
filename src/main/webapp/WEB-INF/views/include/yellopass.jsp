<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>



<!-- yello login -->

<div id="loginModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content modal-small">
			<a class="close" data-dismiss="modal" aria-label="Close"></a>
			<div class="modal-header">
				<h4 class="modal-title">로그인</h4>
			</div>
			<div class="modal-body">
				<div class="container_login">
					<div class="login_top_logo">
						<img src="/resources/images/common/big_logo.png" alt="mopic" width="237" />
					</div>

					<div class="form-signin">
						<label for="loginId" class="sr-only">Yello One PASS 아이디</label>
						<input type="email" id="loginId" class="text-input-normal" placeholder="Yello One PASS 아이디 ( 이메일 )" required="" onfocus="if (this.placeholder == 'Yello PASS 아이디 ( 이메일 )') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = 'Yello PASS 아이디 ( 이메일 )';}" />

						<label for="loginPwd" class="sr-only">비밀번호</label>		
						<input type="password" id="loginPwd" class="text-input-normal" placeholder="비밀번호" required="" onfocus="if (this.placeholder == '비밀번호') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '비밀번호';}">

						<a id="loginSubmit" class="btn btn-submit" role="button">로그인</a>
						<div class="login_bottom">
							<a id="lookUpId">아이디 찾기</a><span class="dv">|</span><a id="resetPwd" class="last">비밀번호 재설정</a>
						</div>
						<div class="login-external">
							<a id="loginFb" class="btn btn-submit-facebook" role="button">페이스북 계정으로 로그인</a>
							<a id="loginKakao" class="btn btn-submit-kakao" role="button">카카오톡 계정으로 로그인</a>
						</div>
						<div class="login_bottom login_bottom_none">
							아직 가입 전이신가요?
							<a id="signUp" class="last blue">Yello One PASS 가입하기</a>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>


<script src="https://yellopass.yellofg.com/js/noco.itgr.auth.js"></script>
<script async>
	if(!window.App) {
		window.App = {};
	}
window.App.origin = window.location.origin || window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');;
	
	/*yellopass login api 사용 호출함수*/
	var _redirect_url = '';
	yellopass.init({
		redirectUrl : window.App.origin + '/yellopass/redirect/', //결과받을 페이지
		javascriptApi : false, //javascript api 사용유무 ie8,9는 오류로 인해 사용불가 true 일 경우 ie10, 그외 브라우져에서 사용
		useYellopassKey : true, //true 인 경우 yellopass에 등록되어 있는 페이지 사용(facebook, kakao 앱등록 안해도 됨)
		mobile_YN : "N", // 기본 웹으로 사용
		result : function(){ // 결과 돌려받는 함수, 로그인이 완료되면 arguments 로 yellopass access_token넘겨줌
			var flag = arguments[0].result;
			var token = arguments[0].access_token;
	
			if (flag === 'success') {
				var data = {
						access_token : token
						, redirectUrl : '${session.requestUrl}'
				};
				
				$.ajax({
					type: 'POST',
					url: '/member/doLogin',
					contentType: 'application/json;charset=UTF-8',
					data: JSON.stringify(data),
					dataType: 'json',
					success: function(response) {
						var resultCode	= response.resultCode;
						var redirectUrl	= response.redirectUrl;

						if(resultCode === 'SUCCESS') {
							if(typeof(_redirect_url) != "undefined" && _redirect_url != ""){
								location.href=_redirect_url;
							}
							else if(redirectUrl == "") {
								location.reload();
							}
							else {
								location.href=redirectUrl;
							}
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
					}
				});
			} else {
				alert('login failed');
			}
		}
	});
	
	
	$('#loginSubmit').click(function() {
		yellopass.login('login',
			$('#loginId').val(),
			$('#loginPwd').val() );
	});
	
	$('#loginFb').click(function() {
		yellopass.login('facebook');
	});

	$('#loginKakao').click(function() {
		yellopass.login('kakao');
	});
	
	$('#signUp').click(function() {
		yellopass.bypass({
			url: '/page/member',
			redirect_url: window.App.origin + '/yellopass/redirect/',
			mobile_YN: 'N'
		});
	});
	
	$('#lookUpId').click(function() {
		yellopass.bypass({
			url: '/page/id',
			redirect_url: window.App.origin + '/yellopass/redirect/',
			mobile_YN: 'N'
		});
	});
	
	$('#resetPwd').click(function() {
		yellopass.bypass({
			url: '/page/password/reset',
			redirect_url: window.App.origin + '/yellopass/redirect/',
			mobile_YN: 'N'
		});
	});

	$('#loginModal').on('shown.bs.modal', function () {
	    $('#loginId').focus();
	});

	$( "#loginId, #loginPwd" ).keydown(function( e ) {
		if(e.keyCode==13) {
			logValidation();
		}
	});
	
	$('a[data-target="#loginModal"]').click(function () {
		var $this = $(this);
		
		if ( $this.data('redirect_url') != '' )
			_redirect_url = $this.data('redirect_url');
	});
	
    function logValidation(){
    	var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    	var user_id = $('#loginId');
    	var user_pw = $('#loginPwd');

    	if( !user_id.val() ){
            user_id.val('Yello PASS 아이디(이메일)를 입력해 주세요.');
            user_id.focus();
            return false;
        } else if( !regEmail.test( user_id.val() ) ) {
            user_id.focus();
            return false;
        } else if( !user_pw.val() ) {
        	user_pw.attr('placeholder','비밀번호를 입력해 주세요.');
            user_pw.val('');
            return false;
        }
        $('#loginSubmit').trigger('click');
    }
</script>











