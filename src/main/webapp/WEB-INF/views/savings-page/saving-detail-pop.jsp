<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><div class="product_top">
    <div class="modal-header">
        <h4 class="modal-title">상품 상세보기</h4>
    </div>
    <i class="bi bi-${data.BANK_IMG}"></i>
    <div class="product_top_view">
        <div class="left">${data.DS_NM}</div>
        <div class="right">
            <ul class="comm">
				<c:if test="${data.DS_INTERNET_YN eq 'Y'}"><li><i class="banking-icon internet"></i><span>인터넷 가입</span></li></c:if>
                <c:if test="${data.DS_PHONE_YN eq 'Y'}"><li><i class="banking-icon smartphone"></i><span>스마트폰 가입</span></li></c:if>
                <c:if test="${data.DS_TAXFREE_YN eq 'Y'}"><li class="last"><i class="banking-icon free"></i><span>비과세</span></li></c:if>
                <c:if test="${data.DS_PROTECT_YN eq 'Y'}"><li><i class="banking-icon save"></i><span>예금자보호</span></li></c:if>
            </ul>
        </div>
    </div>
    <p class="desc">${data.DS_SUMMARY1} ${data.DS_SUMMARY2}</p>
    
    <div class="summary-area annuity clearfix">
        <div class="info">
            <span class="label-type">이율</span>
            <ul>
            	<c:choose>
            		<c:when test="${data.DS_RATE12 == '0.00' and data.DS_RATE24 == '0.00' and data.DS_RATE36 == '0.00'}">
            			<c:if test="${data.DS_PRE_TAX != null and data.DS_PRE_TAX != '' and data.DS_PRE_TAX != '0.00'}"><li><span></span>${data.DS_PRE_TAX}%</li></c:if>
            		</c:when>
            		<c:otherwise>
		                <c:if test="${data.DS_RATE12 != null and data.DS_RATE12 != '' and data.DS_RATE12 != '0.00'}"><li><span>1년</span>${data.DS_RATE12}%</li></c:if>
		                <c:if test="${data.DS_RATE24 != null and data.DS_RATE24 != '' and data.DS_RATE24 != '0.00'}"><li><span>2년</span>${data.DS_RATE24}%</li></c:if>
		                <c:if test="${data.DS_RATE36 != null and data.DS_RATE36 != '' and data.DS_RATE36 != '0.00'}"><li><span>3년</span>${data.DS_RATE36}%</li></c:if>
            		</c:otherwise>
            	</c:choose>
            </ul>
        </div>
        <div class="btnGroup">
            <a class="btn btn-submit" href="${data.LANDING_URL}" target="_blank" id="btn-homepage" role="button">가입하기</a>
            <c:if test="${session.isLogin eq 'N'}">
	            <a class="btn btn-submit" role="button"  data-toggle="modal" data-target="#loginModal">관심상품</a>
	        </c:if>
	        <c:if test="${session.isLogin eq 'Y' and data.interest_yn eq 'N'}">
	            <a class="btn btn-submit" role="button" id="btn-interest" data-product_type="1200" data-prod_code="${data.AC_CODE}">관심상품</a>
	        </c:if>
        </div>
    </div>

</div>
<div class="product_down" id="gray-scroll">
    <div class="gray-scroll-in">
        <div class="in_tb">
            <table summary="저축-적금 상품, 금융기관.특징.가입대상.가입기간 범위.금리.우대금리.이자지급방식.젤세 혜택.온라인 가입여부.예금자보호여부.기타 안내등의 컬럼으로 이루어진 표">
                <caption>저축-적금 상품 상세내용 표</caption>
                <colgroup>
                    <col style="width:130px;" />
                    <col style="*" />
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">금융기관</th>
                        <td>${data.DS_BANKING_NM}</td>
                    </tr>
                    <tr>
                        <th scope="row">특징</th>
                        <td>${data.DS_CHARACTER}</td>
                    </tr>
                    <tr>
                        <th scope="row">가입대상</th>
                        <td>${data.DS_TARGET_NM}</td>
                    </tr>
                    <tr>
                        <th scope="row">가입기간 범위</th>
                        <td>
                           	<c:if test="${data.DS_DATE1_NM != null and data.DS_DATE1_NM != ''}">${data.DS_DATE1_NM}</c:if> <c:if test="${data.DS_DATE2_NM != null and data.DS_DATE2_NM != ''}">~ ${data.DS_DATE2_NM}</c:if>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">최대 가입금액</th>
                        <td><c:if test="${data.DS_JOIN_LIMIT > 0 }">${data.DS_JOIN_LIMIT}만원</c:if></td>
                    </tr>
                    <tr>
                        <th scope="row">금리</th>
                        <td>
                           	<div class="row">
                           		<c:if test="${data.DS_RATE3 != null and data.DS_RATE3 != ''}">
    								<div class="col-xs-2">
    									<span>3개월</span>
    									<c:choose>
    										<c:when test="${data.DS_RATE3 == '0.00' or data.DS_RATE3 == ''}">-</c:when>
    										<c:otherwise>${data.DS_RATE3} %</c:otherwise>
    									</c:choose>
    								</div>
    							</c:if>
    							<c:if test="${data.DS_RATE6 != null and data.DS_RATE6 != ''}">
    								<div class="col-xs-2">
    									<span>6개월</span>
    									<c:choose>
    										<c:when test="${data.DS_RATE6 == '0.00' or data.DS_RATE6 == ''}">-</c:when>
    										<c:otherwise>${data.DS_RATE6} %</c:otherwise>
    									</c:choose>
    								</div>
    							</c:if>
    							<c:if test="${data.DS_RATE12 != null and data.DS_RATE12 != ''}">
    								<div class="col-xs-2">
    									<span>12개월</span>
    									<c:choose>
    										<c:when test="${data.DS_RATE12 == '0.00' or data.DS_RATE12 == ''}">-</c:when>
    										<c:otherwise>${data.DS_RATE12} %</c:otherwise>
    									</c:choose>
    								</div>
    							</c:if>
    							<c:if test="${data.DS_RATE24 != null and data.DS_RATE24 != ''}">
    								<div class="col-xs-2">
    									<span>24개월</span>
    									<c:choose>
    										<c:when test="${data.DS_RATE24 == '0.00' or data.DS_RATE24 == ''}">-</c:when>
    										<c:otherwise>${data.DS_RATE24} %</c:otherwise>
    									</c:choose>
    								</div>
    							</c:if>
    							<c:if test="${data.DS_RATE36 != null and data.DS_RATE36 != ''}">
    								<div class="col-xs-2">
    									<span>36개월</span>
    									<c:choose>
    										<c:when test="${data.DS_RATE36 == '0.00' or data.DS_RATE36 == ''}">-</c:when>
    										<c:otherwise>${data.DS_RATE36} %</c:otherwise>
    									</c:choose>
    								</div>
    							</c:if>
    						</div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">우대금리</th>
                        <td>${data.DS_PR_INFO}</td>
                    </tr>
                    
                    <tr>
                        <th scope="row">이자지급방식</th>
                        <td>${data.DS_INTER}</td>
                    </tr>
                    <tr>
                        <th scope="row">절세혜택</th>
                        <td>${data.DS_TAX}</td>
                    </tr>
                    <tr>
                        <th scope="row">온라인 가입여부</th>
                        <td>${data.DS_IS_YN}</td>
                    </tr>
                    <tr>
                        <th scope="row">예금자보호여부</th>
                        <td>${data.DS_PROTECT_YN}</td>
                    </tr>
                    <tr>
                        <th scope="row">기타 안내</th>
                        <td>${data.DS_ETC}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
