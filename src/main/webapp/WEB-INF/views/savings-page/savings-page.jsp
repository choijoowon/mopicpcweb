<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
	<head>
		<title>모픽 - 저축</title>
		<jsp:include page="/WEB-INF/views/include/header.jsp" />
		<link rel="stylesheet" href="/resources/css/savings-page.css" />
	</head>

    <body id="savings">

		<jsp:include page="/WEB-INF/views/include/top.jsp" />
			
		<div class="container">
			<div class="helper-wrap">
				<div id="helper">
				</div>
			</div>
			<div class="results-wrap">
				<div id="results">
				</div>
			</div>
		</div>


		<jsp:include page="/WEB-INF/views/include/footer.jsp" />

		<div id="concern-msg">
		</div>

		<!-- scripts -->
		<jsp:include page="/WEB-INF/views/include/common-scripts.jsp" />
		<script src="/resources/js/savings-page.bundle.js"></script>
		
		<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
		<jsp:include page="/WEB-INF/views/include/commission.jsp" />
		
    </body>

</html>
