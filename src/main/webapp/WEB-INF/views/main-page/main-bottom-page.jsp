<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<title>모픽 - main page</title>
	<jsp:include page="/WEB-INF/views/include/header.jsp" />
	
	<link rel="stylesheet" href="/resources/css/main-product.css" />
	<link rel="stylesheet" href="/resources/css/insure-page.css" />
	
</head>
  
<body id="main2">
		
		<div class="container_main2">

			<ul class="nav nav-tabs wid_3tabs" role="tablist">
				<li role="presentation" class="left active"><a href="#01" aria-controls="01" role="tab" data-toggle="tab">오늘의 추천상품</a></li>
				<li role="presentation" class="center"><a href="#02" aria-controls="02" role="tab" data-toggle="tab">신상품</a></li>
				<li role="presentation" class="right"><a href="#03" aria-controls="03" role="tab" data-toggle="tab">이벤트)</a></li>
			</ul>

			<div class="login_comm txt-center">로그인 하시면 <strong>최적</strong>의 금융상품을 추천해 드립니다. <a class="btn btn-submit-round" href="#" role="button">로그인</a></div> 
		
			<div class="main2_down">
				
				<ul class="top_sort">
					<li><a href="#">상품추천기준</a><span class="badge" data-html="true" data-toggle="tooltip" data-placement="right" title="예금 : 연 예치금 1,000만원 기준<br/>MMDA(수시입출식) : 연 예치금 1,000만원 기준<br/>적금 : 월 적립금 100만원 기준<br/>주택청약 : 월 적립금 10만원 기준" data-reactid=".1.1.1">?</span></li>
				</ul>
<!--
				<div class="sy_tooltip">
					<span></span>
					<div class="in">
						<ul>
							<li>예금 : 연 예치금 1,000만원 기준</li>
							<li>MMDA(수시입출식) : 연 예치금 1,000만원 기준</li>
							<li>적금 : 월 적립금 100만원 기준</li>
							<li>주택청약 : 월 적립금 10만원 기준</li>
						</ul>
					</div>
				</div>
-->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane box_out active" id="01">
						<c:choose>
						<c:when test="${!empty today_product}"> 
							<c:forEach var="row" items="${today_product}">
								
								<div class="product_box">

									<div class="box_top">
										<div class="ico ${row.product_icon }"></div>
										<div class="r-title ${row.product_type_color }">${row.product_type_nm }</div>
									</div>
									<div class="box_down">
										${row.bi}
										<div class="d-title">${row.prod_name }</div>
										<span class="d-comm">${row.detail_text} </span>
		
										<div class="b-comm">
											<span class="d-comm"> ${row.leading_title } </span>
											<div class="b-comm-ft">
												
												<c:choose>  
												<c:when test="${row.unit eq ''}">
													<span class="d-num"></span>
													<span class="d-comm">${row.leading_info }</span>
												</c:when>
												<c:otherwise>
													<span class="d-comm">${row.payment_method } </span>
													<span class="d-num">${row.leading_info }</span>
													<span class="d-comm">${row.unit }</span>
												</c:otherwise>
												</c:choose>

											</div>
										</div>
									</div>
		
								</div><!-- product_box -->
		          </c:forEach>
						</c:when>
						</c:choose>
						
						
					</div>

					<div role="tabpanel" class="tab-pane box_out" id="02">

						<c:choose>
						<c:when test="${!empty new_product}"> 
							<c:forEach var="row" items="${new_product}">
								
								<div class="product_box">

									<div class="box_top">
										<div class="ico ${row.product_icon }"></div>
										<div class="r-title yellow">${row.product_type_nm }</div>
									</div>
									<div class="box_down">
										${row.bi}
										<div class="d-title">${row.prod_name }</div>
										<span class="d-comm">${row.detail_text} </span>
		
										<div class="b-comm">
											<span class="d-comm"> ${row.leading_title } </span>
											<div class="b-comm-ft">
												
												<c:choose>  
												<c:when test="${row.unit eq ''}">
													<span class="d-num"></span>
													<span class="d-comm">${row.leading_info }</span>
												</c:when>
												<c:otherwise>
													<span class="d-comm">${row.payment_method } </span>
													<span class="d-num">${row.leading_info }</span>
													<span class="d-comm">${row.unit }</span>
												</c:otherwise>
												</c:choose>

											</div>
										</div>
									</div>
		
								</div><!-- product_box -->
		          </c:forEach>
						</c:when>
						</c:choose>

					</div>

					<div role="tabpanel" class="tab-pane box_out" id="03">						
					</div>
				</div><!-- tab-content -->

			</div>
		</div><!-- main2_down -->

	<!-- scripts -->
	<jsp:include page="/WEB-INF/views/include/common-scripts.jsp" />
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />

</body>	
</html>

