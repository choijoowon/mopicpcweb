<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="no-js ie7" lang="ko"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="ko"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="ko"> <!--<![endif]-->
<head>
	<title>모픽이란?</title>
	<jsp:include page="/WEB-INF/views/include/header.jsp" />
	<link rel="stylesheet" href="/resources/css/main-page.css" />
</head>
  
<body id="service-info">
	<!-- ${session.isLogin} -->
	<!-- ${requestUrl} -->
	<jsp:include page="/WEB-INF/views/include/top.jsp" />
	
	<div class="service-slogan">
		<div class="slogan">
			<h2>국내 유일 <strong>금융상품 종합 비교 쇼핑몰</strong></h2>
			<p>금융을 몰라도 간단한 정보 입력만으로 개인별 맞춤 최신 금융상품을 추천</p>
			<p class="emp">‘모픽! 금융 마켓 플레이스’</p>
		</div>
		<div class="link-btn">
			<div class="btn-group">
				<a href="/member/loginPage" title="가입 하러가기">가입 하러가기</a><a href="/" title="비교 하러가기">비교 하러가기</a>
			</div>
		</div>
	</div>
	<div class="service-list">
		<h2>금융상품의 공정한 비교를 목적으로 하는 플랫폼</h2>
		<p class="sub-copy">저축상품부터 투자 상품까지 아우르는 금융상품 종합 비교 쇼핑몰</p>
		<ul class="clearfix">
			<li class="nomt">
				<div class="icon" style="padding-top:22px;"><i class="glyph glyph-icon-01"></i></div>
				<div class="txt">
					<span class="subject">저축</span>
					<p>
						사용자의 개인적 환경(주거래 은행, 거주지역, 직업)을 고려한 상품 추천<br/>
						만기 수령액과 세후이자의 차이를 통한 상품 비교 추천
					</p>
				</div>
			</li>
			<li class="nomt">
				<div class="icon" style="padding-top:14px;"><i class="glyph glyph-icon-17"></i></div>
				<div class="txt">
					<span class="subject">카드</span>
					<p>
						사용자가 원하는 카드유형, 카드사, 혜택에 따른 상품 추천<br/>
						보유한 카드보다 혜택이 더 많은 상품 비교 추천
					</p>
				</div>
			</li>
			<li>
				<div class="icon" style="padding-top:11px;"><i class="glyph glyph-icon-06"></i></div>
				<div class="txt">
					<span class="subject">대출</span>
					<p>
						사용자의 개인적 환경(주거래 은행, 거주지역, 직업)을 고려한 상품 추천<br/>
						저금리, 최고한도, 최장기간의 기준에 따른 상품 추천
					</p>
				</div>
			</li>
			<li>
				<div class="icon" style="padding-top:8px;"><i class="glyph glyph-icon-20"></i></div>
				<div class="txt">
					<span class="subject">P2P</span>
					<p>
						현재 진행 중인 투자처, 투자상품, 상세내용 제공<br/>
						신용, 담보 등 유형에 따른 대출상품 추천
					</p>
				</div>
			</li>
			<li style="margin-top:26px;">
				<div class="icon" style="padding-top:23px; padding-left:3px;"><i class="glyph glyph-icon-10"></i></div>
				<div class="txt">
					<span class="subject">보험</span>
					<p>
						월 보험료, 보험가격지수, 10년 적립률, 연 보험료를 기준으로 한 상품 추천<br/>
						보험나이, 성별 등을 고려하여 상품 추천
					</p>
				</div>
			</li>
			<li style="margin-top:26px;">
				<div class="icon" style="padding-top:11px;"><i class="glyph glyph-icon-23"></i></div>
				<div class="txt">
					<span class="subject">환율</span>
					<p>
						통화, 거래유형, 거래금액, 우대쿠폰을 고려<br/>
						환전시 최고 금액을 받을 수 있는 기관 추천
					</p>
				</div>
			</li>
		</ul>
		<h3><span class="ico">no phone</span>전화 권유 없이 금융 상품을 비교할 수 있는 서비스</h3>
	</div>
	<jsp:include page="/WEB-INF/views/include/footer.jsp" />
	
	<!-- scripts -->
	<jsp:include page="/WEB-INF/views/include/common-scripts.jsp" />
	<script src="/resources/js/main-page.bundle.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />

	<script type="text/javascript">
		
	</script>
</body>
</html>

