<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>모픽</title>
	<jsp:include page="/WEB-INF/views/include/header.jsp" />
	<script src="/resources/js/mobile-detect.min.js"></script>

	<link rel="stylesheet" href="/resources/css/card-page.css" />
	<link rel="stylesheet" href="/resources/css/main-page.css" />
	<link rel="stylesheet" href="/resources/css/logo-collection.css" />
	
	<style>
	.product_box:hover {
	overflow: hidden;
	outline: 2px solid #55acee;
	}
	</style>

	<script type="text/javascript">
		var md = new MobileDetect(window.navigator.userAgent);
        if ( md.mobile() ){
            var m_mq = $("<link rel='stylesheet' type='text/css' href='/resources/css/m_main.css' />");
            $("head").append(m_mq); 
        }
    </script>

</head>
  
<body class="main">
	<!-- ${session.isLogin} -->
	<!-- ${requestUrl} -->
	<jsp:include page="/WEB-INF/views/include/top.jsp" />
	
	<div class="main-intro">
		<div id="search"></div>
	</div>

	<div class="bottom_contents">
		<ul class="sub-nav">
			<li class="left active"><a href="rc-product">오늘의 추천상품</a></li>
			<li class="center"><a href="new-product">신상품</a></li>
			<li class="right"><a href="event">이벤트</a></li>
		</ul>
		
		<c:choose>
			<c:when test="${session.isLogin == 'N'}">
				<div class="login_comm txt-center">로그인 하시면 <strong>최적</strong>의 금융상품을 추천해 드립니다. <a class="btn btn-submit-round" href="#" data-toggle="modal" data-target="#loginModal">로그인</a></div>
			</c:when>
			<c:otherwise>
				<div style="height:1px; margin:20px 0 24px auto;"></div>
			</c:otherwise>
		</c:choose>
 
		<div class="subContent_list">
			<div class="tab-content">
				<div class="tab-pane box_out active" id="rc-product">
					<c:choose>
					<c:when test="${!empty today_product}"> 
						<c:forEach var="row" items="${today_product}">
							<div class="product_box" onClick="fn_clickTodayProduct('${row.detail_url}');">
								<a><div class="box_top">
									<div class="ico ${row.product_icon }"></div>
									<div class="r-title ${row.product_type_color }">${row.product_type_nm }</div>
								</div>
								<div class="box_down">
									${row.bi}
									<div class="d-title">${row.prod_name }</div>
									<p class="detail">${row.detail_text} </p>
									<div class="fee">
										<span class="lt"> ${row.leading_title } </span>
										<c:choose>  
											<c:when test="${row.unit eq ''}">
												<div class="b-comm-ft">
													<c:choose>  
														<c:when test="${row.leading_info2 == '' or row.leading_info2 == '0'}">
														</c:when>
														<c:otherwise>
															<span class="d-foreign">
																<span class="d-comm">(해외 겸용)</span><span class="d-num">${row.leading_info2 }원</span>
															</span>
														</c:otherwise>
													</c:choose>
													<span class="d-num">${row.leading_info }</span><span class="d-comm">원</span>
												</div>
											</c:when>
											<c:otherwise>
												<div class="b-comm-ft">
													<span class="d-comm">${row.payment_method } </span>
													<span class="d-num">${row.leading_info }</span>
													<span class="d-comm">${row.unit }</span>
												</div>
											</c:otherwise>
										</c:choose>
									</div>
								</div></a>
							</div>
	          			</c:forEach>
					</c:when>
					</c:choose>
				</div>

				<div class="tab-pane box_out" id="new-product">
					<c:choose>
					<c:when test="${!empty new_product}"> 
						<c:forEach var="row" items="${new_product}">
							<div class="product_box" onClick="fn_clickTodayProduct('${row.detail_url}');">
								<a><div class="box_top">
									<div class="ico ${row.product_icon }"></div>
									<div class="r-title yellow">${row.product_type_nm }</div>
								</div>
								<div class="box_down">
									${row.bi}
									<div class="d-title">${row.prod_name }</div>
									<p class="detail">${row.detail_text} </p>
	
									<div class="fee">
										<span class="d-comm"> ${row.leading_title } </span>
										<div class="b-comm-ft">
											<c:choose>  
											<c:when test="${row.unit eq ''}">
												<span class="d-num"></span>
												<span class="d-comm">${row.leading_info }</span>
											</c:when>
											<c:otherwise>
												<span class="d-comm">${row.payment_method } </span>
												<span class="d-num">${row.leading_info }</span>
												<span class="d-comm">${row.unit }</span>
											</c:otherwise>
											</c:choose>

										</div>
									</div>
								</div></a>
							</div>
	          			</c:forEach>
					</c:when>
					</c:choose>
				</div>

				<!--
				<div role="tabpanel" class="tab-pane box_out" id="event">
				</div>
				-->
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="product_view_popup" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog">
			<div class="modal-content modal-large">
				<button class="close" data-dismiss="modal" aria-label="Close"></button>
	
				<div class="modal-body">
					상세내용을 가져올 수 없습니다.
				</div>
			</div>
		</div>
	</div>
	
	<!-- scripts -->
	<script type="text/javascript" src="/resources/lib/TweenMax.min.js"></script>
	<script type="text/javascript" src="/resources/lib/ScrollToPlugin.min.js"></script>
	<jsp:include page="/WEB-INF/views/include/common-scripts.jsp" />
	<script src="/resources/js/main-page.bundle.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />
	

	<script type="text/javascript">
		var $window = $(window);
		var _wh;
		var _postion;
		var scrollTime = .5;
		var scrollDistance;
		var delta;
		var finalScroll = null;
		resize();

		$(function(){

			$(".sub-nav li a").click( function(e) {
				e.preventDefault();
				
				var currentTab = $(e.currentTarget).attr('href');
				var _index = $(e.currentTarget).parent().index();
				if ( currentTab == 'event' ){
					alert('진행중인 이벤트가 없습니다.');
				} else {
					$(".sub-nav li").removeClass('active');
					$(".sub-nav li").eq(_index).addClass('active');
					$('.tab-content .tab-pane').hide();
					$('#' + currentTab).addClass('active').show();
					$('body').addClass('sb-open');

					if ( md.mobile() ){
						$('.main-intro').hide();
						$('.bottom_contents').css('top',0).show();
					}
					finalScroll = $window.scrollTop() - parseInt(-2.4*scrollDistance);
					scrMove();
				}
			} );
			$(".subContent_list .tab-pane").mCustomScrollbar({ setHeight:getContentHeight() });
			$window.bind('resize', resize);

			if ( !md.mobile() ){
				$window.on("mousewheel DOMMouseScroll", function(event){
					event.preventDefault();
					delta = event.originalEvent.wheelDelta/50 || -event.originalEvent.detail/2 || event.deltaY;
					var scrollTop = $window.scrollTop();
						finalScroll = scrollTop - parseInt(delta*scrollDistance);
					if ( delta < 0 ) {
						$('body').addClass('sb-open');
					} else {
						$('body').removeClass('sb-open');
					}
					scrMove();
				});

				$window.ready(function() {
					setTimeout(function() {
						$('.bottom_contents').show();
					}, 400);
			    });
			}

		});

		function resize(){
			_wh = $window.height();
			var addH = $('body').hasClass('sb-open') ? -140 : 0;
			var _delta = $('body').hasClass('sb-open') ? -2.4 : 2.4;
			$('.main-intro').height(_wh);
			$('.bottom_contents').height(_wh + addH + 'px').css('top', _wh-100 + 'px');
			_postion = Math.round( _wh - $('#search').height() )/2;
			$('#search').css('margin-top', _postion + 'px');
			scrollDistance = _wh;
			$(".subContent_list .tab-pane").height(getContentHeight());
			finalScroll = $window.scrollTop() - parseInt(_delta*scrollDistance);
			scrMove();
		}

		function scrMove(){
			TweenMax.to($window, scrollTime, {
				scrollTo : { y: finalScroll, autoKill:true } ,
				ease: Power1.easeOut,
				overwrite: 1,
				onComplete:function() {}
			});
		}

		function getContentHeight(){
			var ratio = 1.5; 
			if ( _wh >= 960 ){
				ratio = 1.5;
			}  else if ( _wh < 960 && _wh >= 800 ){
				ratio = 1.8;
			} else {
				ratio = 2;
			}
			return Math.round(_wh/ratio);	
		}
	</script>
</body>
</html>
