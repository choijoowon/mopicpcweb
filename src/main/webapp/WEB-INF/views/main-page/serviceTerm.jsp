<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<title></title>
	<style type="text/css">
	/*
		@page { margin-right: 1.18in; margin-top: 1.38in; margin-bottom: 1.18in }
		p { margin-bottom: 0.1in; direction: ltr; font-variant: normal; color: #000000; line-height: 120%; text-align: left; page-break-inside: auto; widows: 2; orphans: 2; text-decoration: none; page-break-after: auto }
		p.western { font-family: "Arial", sans-serif; font-size: 11pt; so-language: en-US; font-style: normal; font-weight: normal }
		p.cjk { font-family: "Arial", sans-serif; font-size: 11pt; so-language: zh-CN; font-style: normal; font-weight: normal }
		p.ctl { font-family: "Arial", sans-serif; font-size: 11pt; so-language: hi-IN }
	*/
	</style>
	<style type="text/css">

		body, div, h4, ul, li, span{margin:0; padding:0;}
		
		header, section{display:block;}
		
		ul{list-style-type:none;}
		body,select{font-size:13px;
		      color:#333;
		      font-family: 'Noto Sans KR', sans-serif;}
		
		#yTerms{
		  width:1024px;
		  margin:0 auto;
		  margin-top:70px;
		}
		#yTerms h2{
		  margin-bottom:20px;
		  padding-bottom:20px;
		  /*text-align:center;*/
		}
		
		#yTerms h3{
		  margin-bottom:15px;
		  padding-bottom:15px;
		  /*text-align:center;*/
		}
		
  </style>
</head>

<body>
<div id="yTerms">
<h2>모픽 서비스 이용약관</h2>
<p></p>
<h3>제1장 총칙</h3>
<p></p>
<p>제1조 (목적) 이 약관은 주식회사 옐로마켓플레이스(이하 “회사”)가 온라인으로 제공하는 모픽 서비스 및 이에 부수된 제반 서비스(이하 “서비스”)의 이용과 관련하여 회사와 회원 간의 권리, 의무 및 책임사항, 기타 필요한 사항을 규정함을 목적으로 합니다.</p>

<p>제2조 (용어의 정의) ① 이 약관에서 사용하는 정의는 다음과 같습니다.</p>
<p>1. “회사”라 함은 온라인을 통하여 서비스를 제공하는 사업자를 의미합니다.</p>
<p>2. “이용자”라 함은 회사의 사이트에 접속하여 이 약관에 따라 회사가 제공하는 제반서비스를 이용하는 회원 및 비회원을 의미합니다.</p>
<p>3. “회원”이라 함은 회사와 이용계약을 체결하여 회사가 제공하는 모든 서비스를 계속적으로 이용할 수 있는 자를 의미합니다.</p>
<p> 4. “비회원”이라 함은 “회원”이 아니면서 회사가 제공하는 서비스를 이용하는 자를 의미합니다.</p>
<p> 5. “서비스”라 함은 회사가 이용자에게 온라인으로 제공하는 모픽 서비스 및 이에 부수된 제반 서비스를 의미합니다.</p>
<p> 6. “계정(ID)”이라 함은 회원의 식별과 서비스 이용을 위하여 회원이 선정하고 회사가 부여하는 문자, 숫자 또는 특수문자의 조합을 의미합니다. </p>
<p> 7. “계정정보“라 함은 회원의 계정, 비밀번호, 성명 등 회원이 회사에 제공한 일반정보 및 서비스이용정보 이용요금 결제상태 등 생성정보를 통칭합니다.</p>
<p> 8. “비밀번호”라 함은 회원이 부여받은 계정과 일치되는 회원임을 확인하고 회원의 정보 및 권익보호를 위해 회원 자신이 선정하여 관리하는 문자, 숫자 또는 특수문자의 조합을 의미합니다.</p>
<p> 9. “콘텐츠”라 함은 회사가 서비스에 제공하는 문자, 문서, 그림, 음성, 음향, 영상 또는 이들의 조합으로 이루어진 모든 정보를 말합니다.</p>
<p>② 이 약관에서 사용하는 용어의 정의는 제1항 각호에서 정하는 것을 제외하고는 관계법령 및 기타 일반적인 상관례에 의합니다.</p>

<p>제3조 (서비스의 종류) 회사는 이용자에게 아래와 같은 서비스를 제공합니다. 단, 맞춤대출, 신규보험상담, 보유보험점검 및 이벤트 참여 등 회원제 기반 서비스는 로그인을 한 이용자에 한해서 이용할 수 있습니다.</p>
<p>① 금융상품  및 환율 기타 금융정보 비교·제공 서비스</p>
<p>② 금융상품 검색 서비스</p>
<p>③ 상품 추천 서비스</p>
<p>④ 송금, ATM 및 기타 수수료 조회 서비스</p>
<p>⑤ 지점 찾기 서비스</p>
<p>⑥ 예·적금 만기지급액, 대출 납부예상금액을 계산해볼 수 있는 금융계산기 서비스</p>
<p>⑦ 기타 회사가 추가 개발하거나 다른 회사와의 제휴를 통해 이용자에게 제공하는 일체의 서비스</p>

<p>제4조 (약관의 명시와 개정) ① 회사는 이 약관의 내용을 이용자가 알 수 있도록 서비스 초기 화면이나 서비스 홈페이지에 게시하거나 연결화면을 제공하는 방법으로 이용자에게 공지합니다.</p>
<p>② 회사는 이용자가 회사와 이 약관의 내용에 관하여 질의 및 응답을 할 수 있도록 조치를 취합니다.</p>
<p>③ 회사는 이용자가 약관의 내용을 알 수 있도록 작성하고 약관에 동의하기에 앞서 약관에 정하여져 있는 내용 중 청약철회, 과오납금의 환급, 계약 해제ㆍ해지, 회사의 면책사항 및 회원에 대한 피해보상 등 중요한 내용을 회원이 알 수 있도록 합니다.</p>
<p>④ 회사는 「전자상거래 등에서의 소비자보호에 관한 법률」, 「약관의 규제에 관한 법률」, 「정보통신망이용촉진 및 정보보호 등에 관한 법률」, 「콘텐츠산업진흥법」 등 관련 법령에 위배하지 않는 범위에서 이 약관을 개정할 수 있습니다.</p>
<p>⑤ 회사가 약관을 개정할 경우에는 적용일자 및 개정내용, 개정사유 등을 현행 약관과 함께 명시하여 그 적용일자로부터 최소한 7일 이전(회원에게 불리하거나 중대한 사항의 변경은 30일 이전)부터 그 적용일자 경과 후 상당한 기간이 경과할 때까지 서비스 초기화면 또는 초기화면과의 연결화면을 통해 공지합니다.</p>
<p>⑥ 회사가 약관을 개정할 경우에는 개정약관 공지 후 개정약관의 적용에 대한 회원의 동의 여부를 확인합니다. 개정약관 공지시 회원이 동의 또는 거부의 의사표시를 하지 않으면 승낙한 것으로 간주하겠다는 내용도 함께 공지한 경우에는 회원이 약관 시행일까지 거부의사를 표시하지 않는다면 개정약관에 동의한 것으로 간주할 수 있습니다.</p>
<p>⑦ 회원이 개정약관의 적용에 동의하지 않는 경우 회사 또는 회원은 서비스 이용계약을 해지할 수 있습니다.</p>

<p>제5조 (약관 외 준칙) 이 약관에서 정하지 아니한 사항과 이 약관의 해석에 관하여는 「전자상거래 등에서의 소비자보호에 관한 법률」,「약관의 규제에 관한 법률」,「정보통신망이용촉진 및 정보보호 등에 관한 법률」,「콘텐츠산업진흥법」, 「신용정보의 보호 및 이용에 관한 법률」등 관련 법령에 따릅니다.</p>

<p>제6조 (운영정책) ① 약관을 적용하기 위하여 필요한 사항과 회원의 권익을 보호하고 서비스 내 질서를 유지하기 위하여 회사는 약관에서 구체적 범위를 정하여 위임한 사항을 서비스 운영정책(이하 “운영정책”이라 합니다)으로 정할 수 있습니다.</p>
<p>② 회사는 운영정책의 내용을 회원이 알 수 있도록 서비스 초기 화면이나 서비스 홈페이지에 게시하거나 연결화면을 제공하는 방법으로 회원에게 공지하여야 합니다.</p>
<p>③ 회원의 권리 또는 의무에 중대한 변경을 가져오거나 약관 내용을 변경하는 것과 동일한 효력이 발생하는 운영정책 개정의 경우에는 제4조의 절차에 따릅니다. 단, 운영정책 개정이 다음 각 호의 어느 하나에 해당하는 경우에는 제2항의 방법으로 사전에 공지합니다.</p>
<p> 1. 약관에서 구체적으로 범위를 정하여 위임한 사항을 개정하는 경우</p>
<p> 2. 회원의 권리·의무와 관련 없는 사항을 개정하는 경우</p>
<p> 3. 운영정책의 내용이 약관에서 정한 내용과 근본적으로 다르지 않고 회원이 예측가능한 범위내에서 운영정책을 개정하는 경우</p>
<br/>

<h3>제2장 이용계약의 체결</h3> 
<p></p>
<p>제7조  (서비스 이용계약의 체결) ① 서비스에 대한 이용계약(이하 “이용계약”이라 합니다)은 회원이 되고자 하는 자(이하 “가입신청자”)가 본 약관에 동의하고 정해진 가입양식에 회원정보(계정, 비밀번호, 기타 회사가 필요하다고 인정하는 사항)를 기입하면 회사가 이에 대해 승낙함으로써 체결됩니다.</p>
<p>② 가입신청자는 회사가 요구하는 개인정보를 성실히 제공하여야 합니다.</p>
<p>③ 회사는 가입신청자의 이용신청이 다음 각 호에 해당하는 경우 승낙하지 않거나 승낙을 유보할 수 있습니다.</p>
<p>1. 실명이 아니거나 타인의 명의를 이용한 경우</p>
<p>2. 허위의 정보를 기재하거나, 회사가 제시하는 내용을 기재하지 않은 경우</p>
<p>3. 가입신청자가 이 약관에 의하여 이전에 회원자격을 상실한 적이 있는 경우, 단 회사의 회원 재가입 승낙을 얻은 경우에는 예외로 함</p>
<p>4. 이용자의 귀책사유로 인하여 승인이 불가능하거나 기타 규정한 제반사항을 위반하여 신청하는 경우</p>
<p>5. 서비스 관련 설비의 여유가 없거나, 기술상 또는 업무상 문제가 있는 경우</p>
<p>④ 제1항에 따른 신청에 있어 회사는 회원의 종류 에 따라 전문기관을 통한 실명확인 및 본인인증을 요청할 수 있습니다.</p>
<p></p>
<p>제8조 (회원 계정(ID) 및 비밀번호) ① 회사는 회원에 대하여 회원의 정보 보호, 서비스 이용안내 등의 편의를 위해 회원이 선정한 일정한 문자, 숫자 또는 특수문자의 조합을 계정으로 부여합니다. </p>
<p>② 회사는 계정정보를 통하여 당해 회원의 서비스 이용가능 여부 등의 제반 회원 관리업무를 수행합니다.</p>
<p>③ 회원은 자신의 계정정보를 선량한 관리자로서의 주의 의무를 다하여 관리하여야 합니다. 회원이 본인의 계정정보를 소홀히 관리하거나 제3자에게 이용을 승낙함으로써 발생하는 손해에 대하여는 회원에게 책임이 있습니다.</p>
<p>④ 비밀번호의 관리책임은 회원에게 있으며, 회원이 원하는 경우에는 보안상의 이유 등으로 언제든지 변경이 가능합니다.</p>
<p>⑤ 회원은 정기적으로 비밀번호를 변경하여야 합니다.</p>
<p></p>
<p>제9조 (회원 정보의 수집) ① 회사는 이용계약을 위하여 회원이 제공한 정보 외에도 수집목적 또는 이용목적을 밝혀 회원으로부터 필요한 정보를 수집할 수 있습니다. 이 경우, 회사는 회원으로부터 정보수집에 대한 동의를 받습니다.</p>
<p>② 회사가 정보수집을 위하여 회원의 동의를 받는 경우, 회사는 수집하는 개인정보의 항목 및 수집방법, 수집목적 및 이용목적, 개인정보의 보유 및 이용기간, 제3자에 대한 정보제공 사항(제공받는 자, 제공받는 자의 이용목적, 제공정보의 항목, 보유 및 이용기간)을 개인정보취급방침으로 미리 명시하거나 고지합니다. 회원은 정보제공에 동의하더라도 언제든지 그 동의를 철회할 수 있습니다.</p>
<p>③ 회원은 회사에게 정보를 제공하고자 하는 경우 사실대로 제공하여야 합니다</p>
<p></p>
<p>제10조 (회원 정보의 제공 및 변경) ① 회원은 이 약관에 의하여 회사에 정보를 제공하여야 하는 경우에는 진실된 정보를 제공하여야 하며, 허위정보 제공으로 인해 발생한 불이익에 대해서는 보호받지 못합니다.</p>
<p>② 회원은 개인정보관리화면을 통하여 언제든지 자신의 개인정보를 열람하고 수정할 수 있습니다. 다만, 서비스 관리를 위해 필요한 실명, 계정(ID) 등은 수정이 불가능합니다.</p>
<p>③ 회원은 회원가입 신청 시 기재한 사항이 변경되었을 경우 온라인으로 수정을 하거나 기타 방법으로 회사에 대하여 그 변경사항을 알려야 합니다.</p>
<p>④ 제2항의 변경사항을 회사에 알리지 않아 발생한 불이익에 대하여 회사는 책임을 지지 않습니다.</p>
<p></p>
<p>제11조 (개인정보의 보호 및 관리) ① 회사는 관계 법령이 정하는 바에 따라 계정정보를 포함한 회원의 개인정보를 보호하기 위해 노력합니다. 회원 개인정보의 보호 및 사용에 대해서는 관계법령 및 회사가 별도로 고지하는 개인정보취급방침이 적용됩니다.</p>
<p>② 서비스의 일부로 제공되는 개별 서비스를 제외한 것으로서 홈페이지 및 서비스별 웹사이트에서 단순히 링크된 제3자 제공의 서비스에 대하여는 회사의 개인정보취급방침이 적용되지 않습니다.</p>
<p>③ 회사는 회원의 귀책사유로 인하여 노출된 회원의 계정정보를 포함한 모든 정보에 대해서 일체의 책임을 지지 않습니다.</p>
<p></p>

<br/>
<h3>제3장 계약 당사자의 의무</h3>
<p></p>
<p>제12조 (회사의 의무) ① 회사는 관련 법령을 준수하고, 이 약관이 정하는 권리의 행사와 의무의 이행을 신의에 따라 성실하게 합니다.</p>
<p>② 회사는 회원이 안전하게 서비스를 이용할 수 있도록 개인정보(신용정보 포함)보호를 위해 보안시스템을 갖추어야 하며 개인정보취급방침을 공시하고 준수합니다. 회사는 이 약관 및 개인정보취급방침에서 정한 경우를 제외하고는 회원의 개인정보가 제3자에게 공개 또는 제공되지 않도록 합니다. </p>
<p>③ 회사는 계속적이고 안정적인 서비스의 제공을 위하여 서비스 개선을 하던 중 설비에 장애가 생기거나 데이터 등이 멸실된 때에는 천재지변, 비상사태, 현재의 기술로는 해결이 불가능한 결함 및 장애 등 부득이한 사유가 없는 한 지체 없이 이를 수리 또는 복구하도록 최선의 노력을 다합니다.</p>
<p></p>
<p>제13조 (회원이용자의 의무) ① 회원은 다음 행위를 하여서는 안 됩니다. </p>
<p> 1. 가입신청 또는 변경 시 허위내용의 기재</p>
<p> 2. 타인의 정보도용 </p>
<p> 3. 회사의 임직원, 운영자, 기타 관계자를 사칭하는 행위</p>
<p> 4. 회사가 게시한 정보의 변경</p>
<p> 5. 회사가 금지한 정보(컴퓨터 프로그램 등)의 송·수신 </p>
<p> 6. 회사가 제공 또는 승인하지 아니한 컴퓨터 프로그램이나 기기 또는 장치를 제작, 배포, 이용, 광고하는 행위</p>
<p> 7. 회사와 기타 제3자의 저작권 등 지적재산권에 대한 침해 </p>
<p> 8. 회사 및 기타 제3자의 명예를 손상시키거나 업무를 방해하는 행위 </p>
<p>  9. 외설 또는 폭력적인 말이나 글, 화상, 음향, 기타 공서양속에 반하는 정보 등을 타인에게 유포하는 행위 </p>
<p> 9. 서비스 데이터를 유상으로 처분하거나 권리의 객체로 하는 행위</p>
<p> 10. 9호의 행위를 유도하거나 광고하는 행위</p>
<p> 11. 회사의 동의 없이 영리, 영업, 광고, 정치활동 등을 목적으로 서비스를 사용하는 행위 </p>
<p> 12. 기타 관련 법령에서 금지하거나 선량한 풍속 기타 사회통념상 허용되지 않는 행위</p>
<p>② 회원은 이 약관의 규정, 이용안내 및 서비스와 관련하여 공지한 주의사항, 회사가 통지하는 사항 등을 확인하고 준수할 의무가 있습니다.</p>
<p>③ 회사는 제1항, 제2항 및 다음 각 호의 어느 하나에 해당하는 행위의 구체적인 유형을 운영정책에서 정할 수 있으며, 회원은 이를 준수할 의무가 있습니다.</p>
<p>1. 회원의 계정명, 기타 서비스상 사용하는 명칭에 대한 제한</p>
<p>2. 서비스 이용방법에 대한 제한</p>
<p>3. 기타 회원의 서비스 이용에 대한 본질적 권리를 침해하지 않는 범위내에서 회사가 서비스 운영상 필요하다고 인정되는 사항</p>

<br/>
<h3>제4장 서비스 이용</h3>
<p></p>
<p>제14조 (서비스의 변경 및 내용수정) ① 이용자는 회사가 제공하는 서비스를 이 약관, 운영정책 및 회사가 설정한 서비스규칙에 따라 이용할 수 있습니다.</p>
<p>② 회사는 서비스 내용의 제작, 변경, 유지, 보수에 관한 포괄적인 권한을 가집니다.</p>
<p>③ 회사가 상당한 이유가 있는 경우에 운영상, 기술상의 필요에 따라 서비스 수정을 할 수 있으며, 서비스 수정을 하는 경우에는 변경 후 해당 서비스 화면 등을 통하여 공지합니다.</p>
<p></p>
<p>제15조 (서비스의 제공 및 중단 등) ① 회사는 영업방침에 따라 정해진 시간동안 서비스를 제공합니다. </p>
<p>② 제1항에도 불구하고, 다음 각 호의 어느 하나에 해당하는 경우에는 일정한 시간동안 서비스가 제공되지 아니할 수 있으며, 해당 시간 동안 회사는 서비스를 제공할 의무가 없습니다.</p>
<p> 1. 컴퓨터 등 정보통신설비의 보수점검, 교체, 정기점검 또는 서비스 내용이나 서비스의 수정을 위하여 필요한 경우</p>
<p> 2. 해킹 등의 전자적 침해사고, 통신사고, 회원들의 비정상적인 서비스 이용행태, 미처 예상하지 못한 서비스의 불안정성에 대응하기 위하여 필요한 경우</p>
<p> 3. 관련 법령에서 특정 시간 또는 방법으로 서비스 제공을 금지하는 경우</p>
<p> 4. 천재지변, 비상사태, 정전, 서비스 설비의 장애 또는 서비스 이용의 폭주 등으로 정상적인 서비스 제공이 불가능할 경우</p>
<p> 5. 회사의 분할, 합병, 영업양도, 영업의 폐지, 당해 서비스의 수익 악화 등 회사의 경영상 중대한 필요에 의한 경우</p>
<p>③ 회사는 제2항 제1호의 경우, 매주 또는 격주 단위로 일정 시간을 정하여 서비스를 중지할 수 있습니다. 이 경우 회사는 최소한 24시간 전에 그 사실을 회원에게 서비스 초기 화면이나 서비스 홈페이지에 고지합니다.  </p>
<p>④ 제2항 제2호의 경우, 회사는 사전 고지 없이 서비스를 일시 중지할 수 있습니다. 회사는 이러한 경우 그 사실을 서비스 초기 화면이나 서비스 홈페이지에 사후 고지할 수 있습니다.</p>
<p>⑤ 회사는 회사가 제공하는 무료서비스 이용과 관련하여 이용자에게 발생한 어떠한 손해에 대해서도 책임을 지지 않습니다. 다만, 회사의 고의 또는 중대한 과실로 인하여 발생한 손해의 경우는 제외합니다.</p>
<p>⑥ 제2항 제3호 내지 제5호의 경우에 회사는 기술상?운영상 필요에 의해 서비스 전부를 중단할 수 있으며, 사전에 공지하고 서비스의 제공을 중단할 수 있습니다. 사전에 공지할 수 없는 부득이한 사정이 있는 경우는 사후에 공지를 할 수 있습니다.</p>
<p>⑦ 회사가 부득이한 사유에 따라 서비스를 종료하는 경우 회원은 서비스 이용이 중단됨에 따른 손해배상을 청구할 수 없습니다.</p>
<p></p>
<p>제17조 (저작권 등의 귀속) ① 서비스에 대한 저작권 및 지적재산권은 회사에 귀속됩니다. 단, 제휴계약에 따라 제공된 저작물 등은 제외합니다. </p>
<p>② 회원은 회사가 제공하는 서비스를 이용함으로써 얻은 정보 중 회사 또는 제공업체에 지적재산권이 귀속된 정보를 회사 또는 제공업체의 사전승낙 없이 복제, 전송, 출판, 배포, 방송 기타 방법에 의하여 영리목적으로 이용하거나 제3자에게 이용하게 하여서는 안 됩니다.</p>


<br/>
<h3>제5장 계약 해제·해지 및 이용제한</h3>
<p></p>
<p>제18조 (회원의 계약해지) ① 회원은 언제든지 서비스 내 계정삭제화면을 통하여 이용계약 해지 신청을 할 수 있으며, 회사는 관련법 등이 정하는 바에 따라 이를 즉시 처리하여야 합니다.</p>
<p>② 회원이 계약을 해지할 경우, 관련법 및 개인정보취급방침에 따라 회사가 회원정보를 보유하는 경우를 제외하고는 해지 즉시 회원의 모든 데이터는 소멸됩니다.</p>
<p></p>
<p>제19조 (회사의 해제 및 해지) ① 회사는 회원이 이 약관에서 정한 회원의 의무를 위반한 경우에는 회원에 대한 사전 통보 후 계약을 해지할 수 있습니다. 다만, 회원이 현행법 위반 및 고의 또는 중대한 과실로 회사에 손해를 입힌 경우에는 사전 통보 없이 이용계약을 해지할 수 있습니다. </p>
<p>② 회사가 이용계약을 해지하는 경우 회사는 회원에게 서면, 전자우편 또는 이에 준하는 방법으로 다음 각 호의 사항을 회원에게 통보합니다.</p>
<p> 1. 해지사유</p>
<p> 2. 해지일</p>
<p>③ 제1항 단서의 경우, 회원은 유료서비스 의 사용권한을 상실하고 이로 인한 환불 및 손해배상을 청구할 수 없습니다.</p>
<p></p>
<p>제20조 (회원에 대한 서비스 이용제한) ① 회사는 회원에게 다음 각 호의 구분에 따라 회원의 서비스 이용을 제한할 수 있습니다. 이용제한이 이루어지는 구체적인 회원의 의무위반 사유는 서비스의 운영정책에서 정하기로 합니다.</p>
<p> 1. 계정 이용제한 : 일정기간 또는 영구히 회원 계정의 이용을 제한</p>
<p> 2. 회원 이용제한 : 일정기간 또는 영구히 회원의 서비스 이용을 제한</p>
<p>② 회사의 이용제한이 정당한 경우에 회사는 이용제한으로 인하여 회원이 입은 손해를 배상하지 않습니다.</p>
<p></p>
<p>제21조 (잠정조치로서의 이용제한) 회사는 다음 각 호에 해당하는 문제에 대한 조사가 완료될 때까지 계정을 정지할 수 있습니다.</p>
<p> 1. 계정이 해킹 또는 도용당하였다는 정당한 신고가 접수된 경우</p>
<p> 2. 불법프로그램 사용자, 작업장 등 위법행위자로 합리적으로 의심되는 경우</p>
<p> 3. 그 밖에 위 각호에 준하는 사유로 계정의 잠정조치가 필요한 경우 </p>


<br/>
<h3>제6장 손해배상 등</h3>
<p></p>
<p>제22조 (손해배상) ① 회사가 고의 또는 중과실로 회원에게 손해를 끼친 경우, 손해에 대하여 배상할 책임이 있습니다.</p>
<p>② 이용자가 본 약관을 위반하거나 위법한 행위로 회사에 손해를 끼친 경우, 회원은 회사에 대하여 그 손해에 대하여 배상할 책임이 있습니다.</p>
<p></p>
<p>제23조 (회사의 면책) ① 회사는 전시, 사변, 천재지변, 비상사태, 현재의 기술로는 해결이 불가능한 기술적 결함 기타 불가항력적 사유로 서비스를 제공할 수 없는 경우에는 책임이 면제됩니다.</p>
<p>② 회사는 회원의 귀책사유로 인한 서비스의 중지, 이용장애 및 계약해지에 대하여 책임이 면제됩니다.</p>
<p>③ 회사는 기간통신 사업자가 전기통신서비스를 중지하거나 정상적으로 제공하지 아니하여 회원에게 손해가 발생한 경우에 대해서 회사의 고의 또는 중대한 과실이 없는 한 책임이 면제됩니다.</p>
<p>④ 회사는 사전에 공지된 서비스용 설비의 보수, 교체, 정기점검, 공사 등 부득이한 사유로 서비스가 중지되거나 장애가 발생한 경우에 대해서 회사의 고의 또는 중대한 과실이 없는 한 책임이 면제됩니다.</p>
<p>⑤ 회사는 회원의 컴퓨터 환경으로 인하여 발생하는 제반 문제 또는 회사의 고의 또는 중대한 과실이 없는 네트워크 환경으로 인하여 발생하는 문제에 대해서 책임이 면제됩니다.</p>
<p>⑦ 회사는 회원 상호간 또는 회원과 제3자간에 서비스를 매개로 발생한 분쟁에 대해 개입할 의무가 없으며 이로 인한 손해를 배상할 책임도 없습니다.</p>
<p>⑧ 회사가 제공하는 서비스 중 무료서비스의 경우에는 회사의 고의 또는 중대한 과실이 없는 한 회사는 손해배상을 하지 않습니다.</p>
<p>⑨ 본 서비스 중 일부의 서비스는 다른 사업자가 제공하는 서비스를 통하여 제공될 수 있으며, 회사는 다른 사업자가 제공하는 서비스로 인하여 발생한 손해 등에 대해서는 회사의 고의 또는 중대한 과실이 없는 한 책임이 면제됩니다.</p>
<p>⑩ 회사는 회원의 컴퓨터 오류에 의한 손해가 발생한 경우 또는 신상정보 및 전자우편주소를 부정확하게 기재하거나 미기재하여 손해가 발생한 경우에 대하여 회사의 고의 또는 중대한 과실이 없는 한 책임이 면제됩니다.</p>
<p>⑪ 회사는 관련 법령, 정부 정책 등에 따라 서비스 또는 회원에 따라 서비스 이용시간 등을 제한할 수 있으며, 이러한 제한사항 및 제한에 따라 발생하는 서비스 이용 관련 제반사항에 대해서는 책임이 면제됩니다.</p>
<p></p>
<p>제24조 (회원에 대한 통지) ① 회사가 회원에게 통지를 하는 경우 회원이 지정한 전자우편주소, 전자메모 등으로 할 수 있습니다.</p>
<p>② 회사는 회원 전체에게 통지를 하는 경우 서비스 초기화면에 게시하거나 팝업화면 등을 제시함으로써 제1항의 통지에 갈음할 수 있습니다.</p>
<p></p>
<p>제25조 (재판권 및 준거법) 본 약관은 대한민국 법률에 따라 규율되고 해석되며, 회사와 회원간에 발생한 분쟁으로 소송이 제기되는 경우, 회사의 본점 소재지 관할 법원을 전속적 관할로 합니다.</p>
<p></p>
<p>부칙</p>
<p>제1조 이 약관은 2016년 2월25일부터 적용됩니다.</p>
<p></p>
</div>
</body>
</html>