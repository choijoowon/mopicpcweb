module.exports = {
	init : function ()	{
		localStorage.clear();
		localStorage.setItem('product'), JSON.stringfy([
			{
				"data" : [
					{	
						"prodcut" : "예금",
						"item" : [
							{
								"name" : "우리로모아 정기예금",
								"bnak" : "우리은행",	
								"inter_rate" : "연 3.6%",
								"profit" : "10,210,000",
								"interest" : "210,000"
							},
							{
								"name" : "KB골든라이프 연금우대예금",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							}
						]
					},
					{	
						"prodcut" : "적금",
						"item" : [
							{
								"name" : "KB골든라이프 연금우대적금1",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							},
							{
								"name" : "KB골든라이프 연금우대적금2",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							}
						]
					},
					{	
						"prodcut" : "MMDA",
						"item" : [
							{
								"name" : "KB골든라이프 MMDA1",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							},
							{
								"name" : "KB골든라이프 MMDA2",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							}
						]
					},
					{	
						"prodcut" : "청약저축",
						"item" : [
							{
								"name" : "KB골든라이프 청약저축1",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							},
							{
								"name" : "KB골든라이프 청약저축2",
								"bnak" : "KB국민은행",	
								"inter_rate" : "연 2.7%",
								"profit" : "10,210,000",
								"interest" : "180,000"
							}
						]
					}
				]
			}
		]);
	}
};