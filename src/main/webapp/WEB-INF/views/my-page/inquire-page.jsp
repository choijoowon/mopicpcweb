<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if IE 7 ]>    <html class="no-js ie7" lang="ko"> <![endif]-->
<!--[if IE 8 ]>    <html class="no-js ie8" lang="ko"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="no-js" lang="ko"> <!--<![endif]-->
<head>
	<title>모픽 - savings page</title>
	<jsp:include page="/WEB-INF/views/include/header-mypage.jsp" />
	<script type="text/javascript" src="/resources/js/board.js"></script>
	
	<script type="text/javascript">
	if(!window.App) {
		window.App = {};
		window.App.isLoggedIn = '${session.isLogin}';
	}
	window.App = {
		query: { mainmenu: window.location.pathname.split('/')[1], submenu: window.location.pathname.split('/')[2], searchQuery: {}},
		results: { productData: [], numberByFilter: {} },
		comparisons: { isLoggedIn: window.App.isLoggedIn === 'Y' }
	};

	$(function() {
		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
		
		/** 현재 메뉴 GNB active 처리 */
		var pathname = location.pathname;
		$('.gnb li a, .gnb_sub li a').each(function(){
			if (pathname.indexOf($(this).attr('href')) > -1) {
				$(this).addClass('active');
			}
		});
	});
	</script>
</head>
<!-- ${session.isAdmin} -->
<body id="interest">
	<jsp:include page="/WEB-INF/views/include/top.jsp" />
		
	<div class="mypage-out">
		<jsp:include page="/WEB-INF/views/include/left-mypage.jsp" />
		
		<c:choose>
			<c:when test="${bltnCd != ''}">
				<jsp:include page="/WEB-INF/views/board/modify.jsp" />
			</c:when>
			<c:otherwise>
				<jsp:include page="/WEB-INF/views/board/write.jsp" />
			</c:otherwise>
		</c:choose>
	</div><!-- mypage-out -->
	
	<jsp:include page="/WEB-INF/views/include/footer.jsp" />

    <script type="text/javascript" src="/resources/js/common.bundle.js"></script>
	<script type="text/javascript" src="/resources/js/common_menu.bundle.js"></script>

	<script type="text/javascript" src="/resources/lib/jquery.placeholder.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.easydropdown.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.easydropdown_scroll.js"></script>
	<script src="/resources/lib/jquery.mCustomScrollbar.concat.min.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />
</body>
</html>
