<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>모픽 - mypage</title>
		<!--[if lt IE 9]>
			<script src="../lib/es5-shim/es5-shim.min.js"></script>
			<script src="../lib/es5-shim/es5-sham.min.js"></script>
			<script src="../lib/html5shiv/dist/html5shiv.js"></script>
		<![endif]-->
		<link rel="stylesheet" href="../lib/bootstrap/dist/css/bootstrap.min.css" />
		<link rel="stylesheet" href="../css/common.css" />	
		<link rel="stylesheet" href="../css/form.css">	
		<link rel="stylesheet" href="../css/jquery.countdown.css" />

		<style>

		</style>
    </head>

    <body id="mypage">
		
		<div class="wrapper-line">

			<div class="header main-h">
				<div class="head_inner">
					<h1 class="logo"><a href="/"></a></h1>
					<div class="top-menu">	
						<ul class="navbar-nav">
							<li><a href="#" data-toggle="modal" data-target="#loginModal">로그인 </a></li>
							<li class="dropdown">
								<a href="/mypage/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">마이페이지 <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/mypage/">가입상품</a></li>
									<li><a href="#">관심상품</a></li>
									<li><a href="#">1:1문의</a></li>
									<li><a href="#">회원정보관리</a></li>
									<!-- <li class="divider"></li> -->
									<li><a href="#">서비스정보관리</a></li>
								</ul>
							</li>				
							<li><a href="/interest/">관심상품</a></li>
						</ul>
						<ul class="navbar-nav navbar-right">
							<li><a href="/events/">이벤트</a></li>
							<li class="dropdown last">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">고객센터 <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/help/">공지사항</a></li>
									<li><a href="#">FAQ</a></li>
								</ul>
							</li>
						</ul>
					</div>	
					
					<ul class="top-menu-right">
						<li><a href="#" class="first">계산기</a></li>
						<li><a href="#" class="second">수수료</a></li>
						<li><a href="#" class="third">지점찾기</a></li>
					</ul>

					<div class="gnb_sub savings"><!-- 저축 서브메뉴 -->
						<div class="gnb-list_sub">
							<ul class="gnb-depth2_sub">
								<li><a href="/savings/deposits">예금</a></li>
								<li><a href="#">적금</a></li>
								<li><a href="#">MMDA</a></li>
								<li><a href="#">주택청약</a></li>
							</ul>
						</div>
					</div>

					<div class="gnb_sub insure"><!-- 보험 서브메뉴 -->	
						<div class="gnb-list_sub">
							<ul class="gnb-depth2_sub">
								<li><a href="/insure/actual">실손보험</a></li>
								<li><a href="#" >정기보험</a></li>
								<li><a href="#">연금/저축보험</a></li>
								<li><a href="#">자동차보험</a></li>
								<li><a href="#">신규보험상담</a></li>
								<li><a href="#">보유보험점검</a></li>
							</ul>
						</div>
					</div>

					<div class="gnb_sub loan"><!-- 대출 서브메뉴 -->	
						<div class="gnb-list_sub">
							<ul class="gnb-depth2_sub">
								<li><a href="#">신용대출</a></li>
								<li><a href="#">담보대출</a></li>
								<li><a href="#">맞춤대출</a></li>
							</ul>
						</div>
					</div>

					<div class="gnb_sub card"><!-- 카드 서브메뉴 -->	
						<div class="gnb-list_sub">
							<ul class="gnb-depth2_sub">
								<li><a href="#">신용카드</a></li>
								<li><a href="#">체크카드</a></li>
							</ul>
						</div>
					</div>
					
					<div class="gnb_sub p2p"><!-- p2p 서브메뉴 -->	
						<div class="gnb-list_sub">
							<ul class="gnb-depth2_sub">
								<li><a href="#">투자하기</a></li>
								<li><a href="#">대출받기</a></li>
							</ul>
						</div>
					</div>


					<div class="gnb">
						<ul class="gnb-list">
							<li class="gnb-item item1">
								<a href="/savings/" class="title savings"><span class="text">저축</span></a>
								<ul class="gnb-depth2">
									<li><a href="/savings/deposits">예금</a></li>
									<li><a href="#">적금</a></li>
									<li><a href="#">MMDA</a></li>
									<li><a href="#">주택청약</a></li>
								</ul>
							</li>
							<li class="gnb-item item2">
								<a href="/loan/" class="title loan"><span class="text">대출</span></a>
								<ul class="gnb-depth2">
									<li><a href="#">신용대출</a></li>
									<li><a href="#">담보대출</a></li>
									<li><a href="#">맞춤대출</a></li>
								</ul>
							</li>
							<li class="gnb-item item3">
								<a href="/insure/" class="title insure"><span class="text">보험</span></a>
								<ul class="gnb-depth2 pd22">
									<li><a href="/insure/actual">실손보험</a></li>
									<li><a href="#">정기보험</a></li>
									<li><a href="#">연금/저축보험</a></li>
									<li><a href="#">자동차보험</a></li>
									<li><a href="#">신규보험상담</a></li>
									<li><a href="#">보유보험점검</a></li>
								</ul>
							</li>
							<li class="gnb-item item4">
								<a href="/card/" class="title card"><span class="text">카드</span></a>
								<ul class="gnb-depth2">
									<li><a href="#">신용카드</a></li>
									<li><a href="#">체크카드</a></li>
								</ul>
							</li>
							<li class="gnb-item item5">
								<a href="/p2p/" class="title p2p"><span class="text">P2P</span></a>
								<ul class="gnb-depth2">
									<li><a href="#">투자하기</a></li>
									<li><a href="#">대출받기</a></li>
								</ul>
							</li>
							<li class="gnb-item item6 last">
								<a href="/currency/" class="title currency"><span class="text">환율</span></a>
								<ul class="gnb-depth2">
									<li></li>
									<li></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>

				<div class="gnb-add">
					
				</div>
			</div><!-- /.header -->
			
			

		</div><!-- /.wrapper -->

		<div class="mypage-out">
			<div class="mypage-left">
				<h2>마이페이지</h2>

				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title select">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								 가입상품 (7)
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<div class="carat"></div>
								<ul>
									<li><a href="" class="select">저축 (3)</a><li>
									<li><a href="">대출 (3)</a></li>
									<li><a href="">보험 (3)</a></li>
									<li><a href="">카드 (3)</a></li>
									<li><a href="">p2p (3)</a></li>
								</ul>
							</div>
						</div>
					</div><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								  관심상품 (4)
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body">
								<div class="carat"></div>
								<ul>
									<li><a href="" class="select">저축 (3)</a><li>
									<li><a href="">대출 (3)</a></li>
									<li><a href="">보험 (3)</a></li>
									<li><a href="">카드 (3)</a></li>
									<li><a href="">p2p (3)</a></li>
								</ul>
							</div>
						</div>
					</div><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								 1:1 문의
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<div class="carat"></div>
								<ul>
									<li><a href="" class="select">1:1 문의하기</a><li>
									<li><a href="">1:1 문의내역 확인</a></li>
								</ul>
							</div>
						</div>
					</div><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFour">
							<h4 class="panel-title none">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
								 회원정보관리
								</a>
							</h4>
						</div>
						<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour"></div>
					</div><!-- panel -->

					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingFive">
							<h4 class="panel-title none">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
								 서비스정보관리
								</a>
							</h4>
						</div>
						<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive"></div>
					</div><!-- panel -->

				</div><!-- panel-group -->



			</div><!-- mypage-left -->
			

			<div class="mypage-right">
				<h3>가입상품</h3>
				
				<div class=""><!--/////////////// 가입상품 -->
					<ul class="mypage_top_sort">
						<li class="first"><a href="" class="select">예금</a></li>
						<li><a href="">적금</a></li>
						<li><a href="">MMDA</a></li>
						<li class="last"><a href="">청약저축</a></li>
					</ul>

					<div class="top_select">	
						<div class="row">
							<div class="col-xs-1">
								<div class="check-top">
									<div class="check-top1">
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check1" value="">
												<label for="check1"><i></i></label>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-4">
								<a class="btn btn-small" href="#" role="button">선택항목 삭제</a>
								<a class="btn btn-small" href="#" role="button">새로고침</a>
							</div>
							<div class="col-xs-7 txt_right">
								판매가 종료된 상품은 목록에서 <span class="red">자동삭제</span> 됩니다.
							</div>
						</div>
					</div><!-- top_select -->

					<div class="result_box">
						<div class="row">
							<div class="col-xs-1">
								<div class="check-top">
									<div class="check-top1">
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check2" value="">
												<label for="check2"><i></i></label>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-11">
								<a class="btn_close"></a>
								<div class="r_btn"><a class="btn btn-white" href="#" role="button">상세보기</a></div>
								<span class="txt_small">가입상품</span>
								<h3>우리로모아 정기예금</h3>

								<div class="row down">
									<div class="col-xs-6 none">
										<span class="pr">은행명</span><br/>
										우리은행
									</div>

									<div class="col-xs-6 num2">
										<span class="pr">금리</span><br/>
										<span class="pr">연</span> 3.6 <span class="pr">%</span>
									</div>
								</div>
							</div>
						</div>
					</div><!-- result_box -->

					<div class="result_box">
						<div class="row">
							<div class="col-xs-1">
								<div class="check-top">
									<div class="check-top1">
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check3" value="">
												<label for="check3"><i></i></label>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-11">
								<a class="btn_close"></a>
								<div class="r_btn"><a class="btn btn-white" href="#" role="button">상세보기</a></div>
								<span class="txt_small">가입상품</span>
								<h3>우리로모아 정기예금</h3>

								<div class="row down">
									<div class="col-xs-6 none">
										<span class="pr">은행명</span><br/>
										우리은행
									</div>

									<div class="col-xs-6 num2">
										<span class="pr">금리</span><br/>
										<span class="pr">연</span> 3.6 <span class="pr">%</span>
									</div>
								</div>
							</div>
						</div>
					</div><!-- result_box -->

					<div class="result_box">
						<div class="row">
							<div class="col-xs-1">
								<div class="check-top">
									<div class="check-top1">
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check4" value="">
												<label for="check4"><i></i></label>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-11">
								<a class="btn_close"></a>
								<div class="r_btn"><a class="btn btn-white" href="#" role="button">상세보기</a></div>
								<span class="txt_small">가입상품</span>
								<h3>우리로모아 정기예금</h3>

								<div class="row down">
									<div class="col-xs-6 none">
										<span class="pr">은행명</span><br/>
										우리은행
									</div>

									<div class="col-xs-6 num2">
										<span class="pr">금리</span><br/>
										<span class="pr">연</span> 3.6 <span class="pr">%</span>
									</div>
								</div>
							</div>
						</div>
					</div><!-- result_box -->

					
					<div class="result_box_none">
						<h3>담겨진 예금 상품이 없습니다.</h3>
						<span>회원님께 필요한 예금상품을 찾아보세요.</span>
						<div class="btn_go"><span></span><a href="" >예금상품 보러 가기</a></div>
					</div><!-- result_box_none -->

				</div>



				<div class="input-box "><!-- ///////////1:1 문의하기 -->
					<table>
						<tr>
							<td class="title2 td_w130">문의 분류</td>
							<td>
								<select tabindex="4" class="dropdown-sy" >
									<option value="" class="label" value="">선택</option>
									<option value="선택">선택</option>
									<option value="선택">선택</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="title2">제목</td>
							<td>
								<input type="text" name="textfield" id="textfield" class="text-input-normal" placeholder="제목을 입력하세요." required autofocus onfocus="if (this.placeholder == '제목을 입력하세요.') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '제목을 입력하세요.';}"/>
							</td>
						</tr>
						<tr>
							<td class="title2">내용</td>
							<td>
								<textarea name="textarea" id="textarea" cols="45" rows="10" class="text-area" placeholder="내용을 입력하세요." required autofocus onfocus="if (this.placeholder == '내용을 입력하세요.') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '내용을 입력하세요.';}"></textarea>
							</td>
						</tr>
						<tr>
							<td class="title2">파일첨부</td>
							<td>
								<ul class="file">
									<li>
									<input type="text" name="textfield" id="textfield" class="text-input-normal" placeholder="선택된 파일 없음" required autofocus onfocus="if (this.placeholder == '선택된 파일 없음') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '선택된 파일 없음';}"/>
									</li>
									<li>
										<a class="btn btn-submit" href="#" role="button">파일선택</a>
									</li>
								</ul>
							</td>
						</tr>
					</table>

					<div class="right_btn"><a class="btn btn-submit" href="#" role="button">보내기</a></div>
				</div><!-- input-box -->

				<div class="input-box "><!-- /////////////1:1 문의내역 확인 -->
					<table>
						<tr>
							<td class="title2 txt-center td_w100">문의 유형</td>
							<td class="title2 txt-center">제목</td>
							<td class="title2 txt-center td_w130">작성일</td>
							<td class="title2 txt-center td_w100">처리상태</td>
							<td class="title2 txt-center td_w70">삭제</td>
						</tr>
						<tr>
							<td class="txt-center">문의 유형</td>
							<td>제목</td>
							<td class="txt-center">2016. 00. 00</td>
							<td class="txt-center">처리</td>
							<td class="txt-center"><a href="" class="btn_close"></a></td>
						</tr>
						<!-- <tr>
							<td colspan="5" class="list_none">등록된 글이 없습니다.</td>
						</tr> -->
					</table>
				</div><!-- input-box -->

				<div class="password-box "><!-- ///////////회원정보관리 -->
					<h3>개인정보 보호를 위해<br/>비밀번호 확인이 필요합니다.</h3>
					<input type="text" name="textfield" id="textfield" class="text-input-normal" placeholder="비밀번호" required autofocus onfocus="if (this.placeholder == '비밀번호') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '비밀번호';}"/>
					<div class="right_btn"><a class="btn btn-submit" href="#" role="button">확인</a></div>
				</div><!-- password-box -->

				<div class="input-box "><!-- /////////////서비스정보관리 view-->
					<table>
						<tr>
							<td class="title2 td_w130">직업</td>
							<td class="right2">회사원</td>
						</tr>
						<tr>
							<td class="title2">생년월일</td>
							<td class="right2">19830403</td>
						</tr>
						<tr>
							<td class="title2">성별</td>
							<td class="right2">남</td>
						</tr>
						<tr>
							<td class="title2">가처분소득</td>
							<td class="right2">200,000,0000원</td>
						</tr>
						<tr>
							<td class="title2">주거래은행</td>
							<td class="right2">국민은행</td>
						</tr>
						<tr>
							<td class="title2">거주지역</td>
							<td class="right2">역서울시 양천구</td>
						</tr>
						<tr>
							<td class="title2">관심분야</td>
							<td class="right2">저축, 보험, 카드</td>
						</tr>
					</table>

					<div class="bottom_link">모픽 서비스를 더이상 사용하지 않습니다. <a href="">탈퇴하기</a></div>

					<div class="right_btn"><a class="btn btn-submit" href="#" role="button">수정</a></div>
				</div><!-- input-box -->

				<div class="input-box input-box-edit "><!-- /////////////서비스정보관리 eidt-->
					<table>
						<tr>
							<td class="title2 td_w130">직업</td>
							<td>
								<select tabindex="4" class="dropdown-sy" >
									<option value="회사원">회사원</option>
									<option value="자영업자">자영업자</option>
									<option value="학생">학생</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="title2">생년월일</td>
							<td>
								<input type="text" name="textfield" id="textfield" class="text-input-normal" placeholder="19830403" required autofocus onfocus="if (this.placeholder == '19830403') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '19830403';}"/>
							</td>
						</tr>
						<tr>
							<td class="title2">성별</td>
							<td>
								<select tabindex="4" class="dropdown-sy" >
									<option value="남자">남자</option>
									<option value="여자">여자</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="title2">가처분소득</td>
							<td>
								<input type="text" name="textfield" id="textfield" class="text-input-normal" placeholder="200,000,000 원" required autofocus onfocus="if (this.placeholder == '200,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '200,000,000 원';}"/>
							</td>
						</tr>
						<tr>
							<td class="title2">주거래은행</td>
							<td>
								<select tabindex="4" class="dropdown-sy" >
									<option value="국민은행">국민은행</option>
									<option value="우리은행">우리은행</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="title2">거주지역</td>
							<td>
								<select tabindex="4" class="dropdown-sy" >
									<option value="서울시">서울시</option>
									<option value="인천시">인천시</option>
								</select>

								<select tabindex="4" class="dropdown-sy" >
									<option value="양천구">양천구</option>
									<option value="강남구">강남구</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="title2">관심분야</td>
							<td>
								<div class="check-top">
									<div class="check-top1">
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check10" value="">
												<label for="check10"><i></i>저축</label>
											</span>
										</div>
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check11" value="">
												<label for="check11"><i></i>대출</label>
											</span>
										</div>
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check12" value="">
												<label for="check12"><i></i>보험</label>
											</span>
										</div>
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check13" value="">
												<label for="check13"><i></i>카드</label>
											</span>
										</div>
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check14" value="">
												<label for="check14"><i></i>p2p</label>
											</span>
										</div>
										<div class="check-bottom">
											<span>
												<input type="checkbox"  id="check15" value="">
												<label for="check15"><i></i>환율</label>
											</span>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</table>

					<div class="bottom_link">모픽 서비스를 더이상 사용하지 않습니다. <a href="">탈퇴하기</a></div>

					<div class="right_btn">
						<a class="btn btn-cancel" href="#" role="button">취소</a>
						<a class="btn btn-submit" href="#" role="button">완료</a>
					</div>
				</div><!-- input-box -->





			</div><!-- mypage-right -->

		</div><!-- mypage-out -->


		<div class="footer">
			<div class="inner">
				<ul>
					<li class="copy_logo"></li>
					<li><a href="#">모픽이란?</a></li>
					<li><a href="#">서비스약관</a></li>
					<li><a href="#">개인정보취급방침</a></li>
					<li class="last"><a href="#">Yello Financial Group</a></li>
				</ul>

				<ul class="copy">
					<li>서울특별시 영등포구 여의도</li>
					<li>대표이사/사장 : 홍길동</li>
					<li class="last">사업자 등록번호 : 000-00-00000</li><br/>
					<li>고객센터 : 1800-0000</li>
					<li class="last">이메일 문의 : <a href="#"> naver@naver.com</a></li>
				</ul>

			</div>
		</div>

		
    </body>

   <script>
		window.App = { query: { mainmenu: 'mypage', submenu: '', searchQuery: {}, sorting: ''}, results: { productData: [], numberByFilter: {} }  };	
	</script>
	
	


    <script src="../lib/jquery/dist/jquery.min.js"></script>
	<script src="../lib/bootstrap/dist/js/bootstrap.min.js"></script>
	
    <script src="../scripts/common.bundle.js"></script>
    <script src="../scripts/common_menu.bundle.js"></script>
	

	<script src="../scripts/jquery.placeholder.js"></script>
	<script src="../scripts/jquery.easydropdown.js"></script>
	<script src="../scripts/jquery.easydropdown_scroll.js"></script>

	
	<script>
		$(function() {
			// Invoke the plugin
			$('input, textarea').placeholder({customClass:'my-placeholder'});
			// That’s it, really.
			
		});

		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})

		
    	/** 현재 메뉴 GNB active 처리 */
    	var pathname = location.pathname;
    	$('.gnb li a, .gnb_sub li a').each(function(){
    		if (pathname.indexOf($(this).attr('href')) > -1) {
    			$(this).addClass('active');
    		}
    	});



	</script>

</html>
