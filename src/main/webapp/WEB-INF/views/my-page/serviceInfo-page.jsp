<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
	<title>모픽 - mypage</title>
	<jsp:include page="/WEB-INF/views/include/header-mypage.jsp" />
	
	<script type="text/javascript">
		window.App = { query: { mainmenu: 'mypage', submenu: '', searchQuery: {}, sorting: ''}, results: { productData: [], numberByFilter: {} }  };	
		var userData;
		
		$(function() {
			$("#txt_income").text(fn_parseComma("${userInfo.income}"));
			
			var request2 = {
				grpCdList	: "1234,7001,7002,7003"
			};
			
			$.ajax({
				type : 'POST'
				, url : "/mopic/api/selectCmnCdGrpDetailList"
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request2)
				, dataType : 'json'
				, success : function(response) {
					var result 		= response.result;
					
					if(result.resultCode == "SUCCESS") {
						var sex			= result.data[1234];
						var job			= result.data[7001];
						var bank		= result.data[7002];
						var interest	= result.data[7003];
						var userInterest= "${userInfo.productInterest}";
						var chk			= "";
						
						var sHTML = "<option value='' selected>선택</option>";
						for(var i = 0; i < job.length; i++) {
							sHTML += "<option value='" + job[i].cd + "'>" + job[i].cdName + "</option>";
						}
						$("#occupation").append(sHTML);
						
						sHTML = "<option value='' selected>선택</option>";
						for(var i = 0; i < sex.length; i++) {
							sHTML += "<option value='" + sex[i].cd + "'>" + sex[i].cdName + "</option>";
						}
						$("#sex").append(sHTML);
						
						sHTML = "<option value='' selected>선택</option>";
						for(var i = 0; i < bank.length; i++) {
							sHTML += "<option value='" + bank[i].cd + "'>" + bank[i].cdName + "</option>";
						}
						$("#bankCode").append(sHTML);
						
						sHTML = "";
						for(var i = 0; i < interest.length; i++) {
							chk = userInterest.indexOf(interest[i].cd) > -1 ? "checked" : "";
							sHTML += "<div class='check-bottom'>";
							sHTML += "	<span>";
							sHTML += "		<input type='checkbox' name='interest' id='check" + i + "' value='" + interest[i].cd + "' " + chk + ">";
							sHTML += "		<label for='check" + i + "'><i></i>" + interest[i].cdName + "</label>";
							sHTML += "	</span>";
							sHTML += "</div>";
						}
						$("#interest").append(sHTML);
						
						$("#occupation").val("${userInfo.occupation}");
						$("#sex").val("${userInfo.sex}");
						
						if ( "${userInfo.bankCode}" != "0" )
							$("#bankCode").val("${userInfo.bankCode}");
						
						var occupation	= "${userInfo.occupation}";
						var occArr		= occupation.split(",");
						
						$("input[name=interest]").each(function() {
							for(var i = 0; i < occArr.length; i++) {
								if($(this).val() == occArr[i].trim())
									$(this).attr('checked', true);
							} 
						});
						$("#dob").val("${userInfo.dob}");
						$("#income").val(fn_parseComma("${userInfo.income}"));
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
			
			$.ajax({
				type : "POST"
				, url : "/mopic/api/mem/selectSigunguInfo"
				, contentType : "application/json;charset=UTF-8"
				, data : "{}"
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						var data = result.data;
						var mHTML = "<option value='' selected>선택</option>"; 
						
						for(var i = 0; i < data.length; i++) {
							mHTML += "<option value='" + data[i].sido + "'>" + data[i].sido_name + "</option>";
						}
						
						$("#homeCity").append(mHTML);
						/*
						$("#homeCity").bind("change", function(e) {
							e.preventDefault();
							var sHTML = "";
							
							for(var i = 0; i < data.length; i++) {
								sHTML = "";
								
								if($(this).val() == data[i].sido) {
									var objData = data[i].sigungu_name.replace(/\"/gi, "");
									var objArr = objData.split(",");
									$("#homeTown").empty();
									
									sHTML = "<option value=''>선택</option>";
									for(var j = 0; j < objArr.length; j++) {
										var objSubArr = objArr[j].split(":");
										sHTML += "<option value='" + objSubArr[0] + "'>" + objSubArr[1] + "</option>";
									}
									
									$("#homeTown").append(sHTML);
								}
							}
						});
						*/

						if ( "${userInfo.homeCity}" != "" )
							$("#homeCity").val("${userInfo.homeCity}");
						$("#homeCity").change();
						$("#homeTown").val("${userInfo.homeTown}");
						$("#txt_homeCity").text($("#homeCity option:selected").text());
						//$("#txt_homeTown").text($("#homeTown option:selected").text());
					}
				}
			});
			
			// Invoke the plugin
			$('input, textarea').placeholder({customClass:'my-placeholder'});
			// That’s it, really.
			
			$('[data-toggle="tooltip"]').tooltip();
			
			$("#btnModify").bind("click", function() {
				$("#viewLayer").hide();
				$("#writeLayer").show();
			});
			
			$("#btnCancel").bind("click", function() {
				$("#writeLayer").hide();
				$("#viewLayer").show();
			});
				
			$("#btnSubmit").bind("click", function() {
				var productInterest = "";
				
				$("input[name=interest]").each(function() {
					if($(this).prop('checked') == true) {
						productInterest += productInterest == "" ? $(this).val() : "," + $(this).val();
					}
				});
				
				var request = {
						occupation : $("#occupation").val()
						, dob : $("#dob").val()
						, sex : $("#sex").val()
						, income : $("#income").val()
						, bankCode : $("#bankCode").val()
						, homeCity : $("#homeCity").val()
						, homeTown : $("#homeTown").val()
						, productInterest : productInterest
				}
				
				$.ajax({
					type : "POST"
					, url : "/mopic/api/updateUserInfo"
					, contentType : "application/json;charset=UTF-8"
					, data : JSON.stringify(request)
					, dataType : 'json'
					, success : function(response) {
						var result = response.result;
						
						if(result.resultCode == "SUCCESS") {
							location.reload();
						}
					}
				});
			});
		});
		
		/** 현재 메뉴 GNB active 처리 */
		var pathname = location.pathname;
		$('.gnb li a, .gnb_sub li a').each(function(){
			if (pathname.indexOf($(this).attr('href')) > -1) {
				$(this).addClass('active');
			}
		});
	</script>
</head>
<!-- ${member} -->
<body id="mypage">
	<jsp:include page="/WEB-INF/views/include/top.jsp" />
		
	<div class="mypage-out">
		<jsp:include page="/WEB-INF/views/include/left-mypage.jsp" />
		
		<div class="mypage-right">
			<h3>Yello One Pass통합 회원정보</h3>
			<div class="input-box chnage-my-info">
				<table summary="나의 직업, 생년월일, 성별, 가처분소득, 주거래은행, 거주지역, 관심분야 등으로 구성된 표">
					<caption>Yello One Pass통합 회원정보</caption>
					<colgroup>
						 <col style="width:130px;" />
						 <col style="*" />
					</colgroup>
					<thead><tr><th></th><th></th><th></th></tr></thead>
					<tbody>
						<tr>
							<th class="title2">이메일</th>
							<td class="right2"><span id="txt_occupation">
								<c:choose>
									<c:when test="${yellopassInfo.email == '' || yellopassInfo.email == null}">
										미등록
									</c:when>
									<c:otherwise>
										${yellopassInfo.email}
									</c:otherwise>
								</c:choose>
							</span>
							<c:choose>
								<c:when test="${yellopassInfo.social_id_yn == 'Y'}">
									<div class="right_btn" style="margin-top:2px;">
										<a id="btnModifyEmail" class="btn btn-submit" role="button" style="padding:4px 0;height:30px;font-size:12px;">
											<c:choose>
												<c:when test="${yellopassInfo.email == '' || yellopassInfo.email == null}">
													등록
												</c:when>
												<c:otherwise>
													변경
												</c:otherwise>
											</c:choose>
										</a>
									</div>
								</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
							</td>
						</tr>
						<c:if test="${yellopassInfo.social_id_yn == 'N'}">
						<tr>
							<th class="title2">비밀번호</th>
							<td class="right2">
								<span id="txt_occupation">최근변경일:${yellopassInfo.pass_mod_date}</span>
								<div class="right_btn" style="margin-top:2px;"><a id="btnModifyPwd" class="btn btn-submit" role="button" style="padding:4px 0;height:30px;font-size:12px;">변경</a></div>
							</td>
						</tr>
						</c:if>
						<tr>
							<th class="title2">휴대폰번호</th>
							<td class="right2">
								<span id="txt_dob">
									<c:choose>
										<c:when test="${yellopassInfo.mobile_num == '' || yellopassInfo.mobile_num == null}">
											미등록
										</c:when>
										<c:otherwise>
											${fn:substring(yellopassInfo.mobile_num,0, fn:length(yellopassInfo.mobile_num)-4 )}****
										</c:otherwise>
									</c:choose>
								</span>
								<div class="right_btn" style="margin-top:2px;">
									<a id="btnModifyMobile" class="btn btn-submit" role="button" style="padding:4px 0;height:28px;font-size:12px;">
										<c:choose>
											<c:when test="${yellopassInfo.mobile_num != ''}">
												변경
											</c:when>
											<c:otherwise>
												등록
											</c:otherwise>
										</c:choose>
									</a>
								</div>
							</td>
						</tr>
					</tbody> 
				</table>
				
				<div class="bottom_link" style="margin-bottom:33px;">Yello One Pass는 옐로금융그룹의 통합 아이디입니다.</div>
				
			</div><!-- input-box -->

		</div>
		
		

		<div class="mypage-right">
			<h3>mopic회원정보</h3>
			<div id="viewLayer" class="input-box ">
				<table summary="나의 직업, 생년월일, 성별, 가처분소득, 주거래은행, 거주지역, 관심분야 등으로 구성된 표">
					<caption>나의 서비스정보 관리표</caption>
					<colgroup>
						 <col style="width:130px;" />
						 <col style="*" />
					</colgroup>
					<thead><tr><th></th><th></th></tr></thead>
					<tbody>
						<tr>
							<th class="title2">직업</th>
							<td class="right2"><span id="txt_occupation">${userInfo.occupationName}</span></td>
						</tr>
						<tr>
							<th class="title2">생년월일</th>
							<td class="right2"><span id="txt_dob">${userInfo.dob}</span></td>
						</tr>
						<tr>
							<th class="title2">성별</th>
							<td class="right2"><span id="txt_sex">${userInfo.sexName}</span></td>
						</tr>
						<tr>
							<th class="title2">가처분소득(월)</th>
							<td class="right2"><span id="txt_income">0</span>만원</td>
						</tr>
						<tr>
							<th class="title2">주거래은행</th>
							<td class="right2"><span id="txt_bankCode">${userInfo.bankCodeName}</span></td>
						</tr>
						<tr>
							<th class="title2">거주지역</th>
							<td class="right2"><span id="txt_homeCity">${userInfo.homeCityName}</span> <span id="txt_homeTown">${userInfo.homeTownName}</span></td>
						</tr>
						<tr>
							<th class="title2">관심분야</th>
							<td class="right2"><span id="txt_productInterest">${interest}</span></td>
						</tr>
					</tbody>

				</table>

				<div class="bottom_link">모픽 서비스를 더이상 사용하지 않습니다. <a name="wayout" href="#">탈퇴하기</a></div>

				<div class="right_btn"><a id="btnModify" class="btn btn-submit" role="button">수정</a></div>
			</div><!-- input-box -->
			
			<div id="writeLayer" class="input-box input-box-edit" style="display:none;">
			<form name="serviceForm">
				<table summary="나의 직업, 생년월일, 성별, 가처분소득, 주거래은행, 거주지역, 관심분야 등을 입력하기 위해 구성된 표">
					<caption>나의 서비스정보 입력표</caption>
					<colgroup>
						 <col style="width:130px;" />
						 <col style="*" />
					</colgroup>
					<thead><tr><th></th><th></th></tr></thead>
					<tbody>
						<tr>
							<th class="title2"><label for="occupation">직업</label></th>
							<td>
								<select id="occupation" name="occupation" tabindex="4" class="dropdown-sy" >
								</select>
							</td>
						</tr>
						<tr>
							<th class="title2"><label for="dob">생년월일</label></th>
							<td>
								<input type="text" name="dob" id="dob" class="text-input-normal" placeholder="YYMMDD" required autofocus onfocus="if (this.placeholder == 'YYMMDD') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = 'YYMMDD';}"/>
							</td>
						</tr>
						<tr>
							<th class="title2"><label for="sex">성별</label></th>
							<td>
								<select id="sex" name="sex" tabindex="4" class="dropdown-sy" >
								</select>
							</td>
						</tr>
						<tr>
							<th class="title2"><label for="income">가처분소득(월)</label></th>
							<td>
								<div class="text-input">
									<input type="text" name="income" id="income" class="text-input-normal" placeholder="만원" required autofocus onfocus="if (this.placeholder == '200,000,000 원') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '200,000,000 원';}"/>
									<span style="position:relative;top:5px;">&nbsp;만원</span>
								</div>
							</td> 
						</tr> 
						<tr>
							<th class="title2"><label for="bankCode">주거래은행</label></th>
							<td>
								<select id="bankCode" name="bankCode" tabindex="4" class="dropdown-sy" >
								</select>
							</td>
						</tr>
						<tr>
							<th class="title2"><label for="homeCity">거주지역</label></th>
							<td>
								<select id="homeCity" name="homeCity" tabindex="4" class="dropdown-sy" >
							</select>

								<!-- <select id="homeTown" name="homeTown" tabindex="4" class="dropdown-sy" >
							</select> -->
							</td>
						</tr>
						<tr>
							<th class="title2"><label for="interest">관심분야</label></th>
							<td>
								<div class="check-top">
									<div id="interest" class="check-top1"></div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="bottom_link">모픽 서비스를 더이상 사용하지 않습니다. <a name="wayout" href="">탈퇴하기</a></div>
				<div class="right_btn">
					<a id="btnCancel" class="btn btn-cancel" href="#" role="button" style="margin-right:10px;">취소</a><a id="btnSubmit" class="btn btn-submit" href="#" role="button">완료</a>
				</div>
			</form>
			</div><!-- input-box -->
		</div>
	</div><!-- mypage-out -->

	<jsp:include page="/WEB-INF/views/include/footer.jsp" />

    <script type="text/javascript" src="/resources/js/common.bundle.js"></script>
	<script type="text/javascript" src="/resources/js/common_menu.bundle.js"></script>

	<script type="text/javascript" src="/resources/lib/jquery.placeholder.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />
	
	<script>

			$('#btnModifyPwd').click(function() {
				yellopass.bypass({
					url: '/page/modify/password',
					redirect_url: window.App.origin + '/yellopass/redirect/',
					next_page: window.App.origin + '/yellopass/redirect/',
					access_token:'${session.access_token}',
					mobile_YN: 'N'
				});
			});
	
			$('#btnModifyMobile').click(function() {
				yellopass.bypass({
					url: '/page/modify/mobile',
					redirect_url: window.App.origin + '/yellopass/redirect/',
					next_page: window.App.origin + '/yellopass/redirect/',
					access_token:'${session.access_token}',
					mobile_YN: 'N'
				});
			});
			
			$('#btnModifyEmail').click(function() {
				yellopass.bypass({
					url: '/page/modify/social/email',
					redirect_url: window.App.origin + '/yellopass/redirect/',
					next_page: window.App.origin + '/yellopass/redirect/',
					access_token:'${session.access_token}',
					mobile_YN: 'N'
				});
			});
			
		</script>
</body>
</html>
