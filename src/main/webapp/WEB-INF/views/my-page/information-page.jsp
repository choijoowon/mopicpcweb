<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<title>모픽 - 고객센터 - FAQ</title>
	<jsp:include page="/WEB-INF/views/include/header-mypage.jsp" />
	<!--<script type="text/javascript" src="/resources/js/board.js"></script>-->
	
	<script type="text/javascript">
	if(!window.App) {
		window.App = {};
		window.App.isLoggedIn = '${session.isLogin}';
	}
	window.App = {
		query: { mainmenu: window.location.pathname.split('/')[1], submenu: window.location.pathname.split('/')[2], searchQuery: {}},
		results: { productData: [], numberByFilter: {} },
		comparisons: { isLoggedIn: window.App.isLoggedIn === 'Y' }
	};

	$(function() {
		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
		
		/** 현재 메뉴 GNB active 처리 */
		var pathname = location.pathname;
		$('.gnb li a, .gnb_sub li a').each(function(){
			if (pathname.indexOf($(this).attr('href')) > -1) {
				$(this).addClass('active');
			}
		});
		
		$("#btnSubmit").bind("click", function() {
			if($("#searchKey").val().trim() == "") {
				alert("검색어를 입력하세요.");
				$("#searchKey").focus();
				return;
			}
			
			location.href="${session.requestUrl}?searchOption=" + $("#searchOption").val() + "&searchKey=" + $("#searchKey").val() + "&pageNum=1";
		});
		
		//$("#listLayer").callBoardList({bltnType : '${bltnType}', pageNum : '${pageNum}'});
		setPageNavigation($("#pageNavigation"), "${result.totalCnt}", "${pageNum}", "${session.requestUrl}");
		$("#searchOption").val("${result.search.searchOption}");
		$("#searchKey").val("${result.search.searchKey}");
	});
	</script>
</head>

<body id="interest">
	<jsp:include page="/WEB-INF/views/include/top.jsp" />
		
	<div class="mypage-out">
		<jsp:include page="/WEB-INF/views/include/left-mypage.jsp" />

		<jsp:include page="/WEB-INF/views/board/informationList.jsp" />
	</div><!-- mypage-out -->
	
	<jsp:include page="/WEB-INF/views/include/footer.jsp" />

    <script type="text/javascript" src="/resources/js/common.bundle.js"></script>
	<script type="text/javascript" src="/resources/js/common_menu.bundle.js"></script>

	<script type="text/javascript" src="/resources/lib/jquery.placeholder.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.easydropdown.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.easydropdown_scroll.js"></script>
	<script src="/resources/lib/jquery.mCustomScrollbar.concat.min.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />
	

	<script type="text/javascript">
		//previous contents close
		$('.panel').on('show.bs.collapse', function (e) {
			$('.panel').removeClass('select');
			$('.panel-collapse').removeClass('in');
			$(this).addClass('select');
		});
		$('.panel').on('hidden.bs.collapse', function (e) {
			$(this).removeClass('select');
		});
	</script>
</body>
</html>
