<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>모픽 - mypage</title>
	<jsp:include page="/WEB-INF/views/include/header-mypage.jsp" />
	
	<c:choose>
		<c:when test="${type == 'CARD'}">
			<c:set var="fieldName" value="연회비" />
		</c:when>
		<c:when test="${type == 'INSURE' and (category == 'SHILSON')}">
			<c:set var="fieldName" value="월 보험료" />
		</c:when>
		<c:when test="${type == 'INSURE' and category == 'LIFE'}">
			<c:set var="fieldName" value="보험가격지수" />
		</c:when>
		<c:when test="${type == 'INSURE' and category == 'ANNUITY'}">
			<c:set var="fieldName" value="10년 적립률" />
		</c:when>
		<c:when test="${type == 'INSURE' and category == 'CAR'}">
			<c:set var="fieldName" value="연 보험료" />
		</c:when>
		<c:otherwise>
			<c:set var="fieldName" value="금리" />
		</c:otherwise>
	</c:choose>
	
	<script type="text/javascript">
		var dsCode = "${dsCode}";
		
		window.App = { query: { mainmenu: 'mypage', submenu: '', searchQuery: {}, sorting: ''}, results: { productData: [], numberByFilter: {} }  };	
	
		$(function() {
			// Invoke the plugin
			$('input, textarea').placeholder({customClass:'my-placeholder'});
			// That’s it, really.
			
			$('[data-toggle="tooltip"]').tooltip()
			
			var request = {
				type : "${type}"
				, category : "${category}"
				, gubun : "${gubun}"
			};
			
			$.ajax({
				type : "POST"
				, url : "/mopic/api/mypage/selectMyProduct"
				, contentType : "application/json;charset=UTF-8"
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						var listHTML = "";
						//setUserData(result.data);
						if(result.totalCnt > 0) {
							for(var i = 0; i < result.totalCnt; i++) {
								var data = result.data[i];
								
								listHTML += "<div class='result_box'>";
								listHTML += "	<div class='row'>";
								listHTML += "		<div class='col-xs-1'>";
								listHTML += "			<div class='check-top'>";
								listHTML += "				<div class='check-top1'>";
								listHTML += "					<div class='check-bottom'>";
								listHTML += "						<span>";
								listHTML += "							<input type='checkbox' name='idx' id='check" + i + "' value='" + data.idx + "'>";
								listHTML += "							<label for='check" + i + "'><i></i></label>";
								listHTML += "						</span>";
								listHTML += "					</div>";
								listHTML += "				</div>";
								listHTML += "			</div>";
								listHTML += "		</div>";
								listHTML += "		<div class='col-xs-11'>";
								listHTML += "			<a href='javascript:removeItem(\"" + data.idx + "\")' class='btn_close'></a>";
								//listHTML += "			<div class='r_btn'><a class='btn btn-white' href='#' role='button'>상세보기</a></div>";
								listHTML += "			<span class='txt_small'>상품명</span>";
								listHTML += "			<h3>" + data.prodname + "</h3>";
								listHTML += "			<div class='row down'>";
								listHTML += "				<div class='col-xs-6 none'>";
								listHTML += "					<span class='pr'>기관명</span><br/>";
								listHTML += "					" + data.bankname + "";
								listHTML += "				</div>";
								listHTML += "				<div class='col-xs-6 num2'>";
								listHTML += "					<span class='pr'>" + "${fieldName}" + "</span><br/>";
								<c:choose>
									<c:when test="${type == 'CARD'}">
									listHTML += "					<span class='pr'></span> " + data.interest + " <span class='pr'></span>";
									</c:when>
									<c:when test="${type == 'INSURE' and (category == 'SHILSON' or category == 'CAR')}">
									listHTML += "					<span class='pr'></span> " + data.amount == "0" ? "" : fn_parseComma(data.amount) + " <span class='pr'>원</span>"
									</c:when>
									<c:otherwise>
									listHTML += "					<span class='pr'></span> " + data.interest + " <span class='pr'>%</span>";
									</c:otherwise>
								</c:choose>
								listHTML += "				</div>";
								listHTML += "			</div>";
								listHTML += "		</div>";
								listHTML += "	</div>";
								listHTML += "</div>";
							}
						} else {
							listHTML += "<div class='result_box_none'>";
							listHTML += "	<h3>담겨진 상품이 없습니다.</h3>";
							listHTML += "	<span>회원님께 필요한 상품을 찾아보세요.</span>";
							listHTML += "	<div class='btn_go clearfix'><span></span><a href='/" + result.serviceUrl + "' >상품 보러 가기</a></div>";
							listHTML += "</div>";
						}
						
						$("#listLayer").append(listHTML);
					}
				}
			});
			
			$("#btnDelete").bind("click", function() {
				var cnt		= $("input[name='idx']:checked").length;
				var idxList	= "";
				
				if(cnt > 0) {
					if(!confirm("선택된 " + cnt + "개 항목을 삭제 하시겠습니까?"))
						return;
					
					$("input[name='idx']:checked").each(function(index, item) {
						idxList += idxList == "" ? item.value : "," + item.value;
						cnt++;
					});
					
					var request = {
						idxList : idxList	
					};
					
					$.ajax({
						type : "POST"
						, url : "/mopic/api/mypage/deleteMyProductList"
						, contentType : "application/json;charset=UTF-8"
						, data : JSON.stringify(request)
						, dataType : 'json'
						, success : function(response) {
							var result = response.result;
							
							if(result.resultCode == "SUCCESS") {
								location.reload();
							}
						}
					});
				} else {
					alert("선택된 항목이 없습니다.");
				}
			});
			
			$("#btnRefresh").bind("click", function() {
				location.reload();
			});
			
			var checkStatus = false;
			$("#checkAll").bind("click", function() {
				$("input[name='idx']").each(function(index, item) {
					item.checked = checkStatus == false ? true : false;
				});
				checkStatus = !checkStatus;
			});
			/*
			$("#btnDetail").bind("click", function() {
				let targetModalElem = $(targetModalSelector);

	            targetModalElem.data('idx', this.props.index);
	            targetModalElem.removeData('bs.modal');

	            targetModalElem.addClass('hide');
	            
	            targetModalElem
	                .modal()
	                .find('.modal-body')
	                .load(detailUrl, (responseText, textStatus) => {
	                    
	                    if( textStatus === 'success' || textStatus === 'notmodified' ) {
	                        targetModalElem.removeClass('in');
	                        targetModalElem.removeClass('hide');
	                        
	                        $.ajax({
	                            dataType: "script",
	                            cache: true,
	                            url: '/resources/js/detail-modal-actions.bundle.js'
	                        })

	                        setTimeout(() => { targetModalElem.addClass('in'); }, 50);
	                    } else {
	                        targetModalElem.modal('hide');
	                    }
	                });
			});
			*/
		});
		
		/** 현재 메뉴 GNB active 처리 */
		var pathname = location.pathname;
		$('.gnb li a, .gnb_sub li a').each(function(){
			if (pathname.indexOf($(this).attr('href')) > -1) {
				$(this).addClass('active');
			}
		});
		
		function removeItem(item) {
			if(!confirm("선택된  항목을 삭제 하시겠습니까?"))
				return false;
			
			var request = {
				idxList : item	
			};
			
			$.ajax({
				type : "POST"
				, url : "/mopic/api/mypage/deleteMyProductList"
				, contentType : "application/json;charset=UTF-8"
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						location.reload();
					}
				}
			});
		}		
	</script>
</head>

<body id="mypage">
	<jsp:include page="/WEB-INF/views/include/top.jsp" />
		
	<div class="mypage-out">
		<jsp:include page="/WEB-INF/views/include/left-mypage.jsp" />

		<div class="mypage-right">
			<c:choose>
				<c:when test="${gubun == '30000001'}">
					<h3>가입상품</h3>
				</c:when>
				<c:otherwise>
					<h3>관심상품</h3>
				</c:otherwise>
			</c:choose>
			
			<div class=""><!--/////////////// 가입상품 -->
					<ul class="mypage_top_sort">
						<c:choose>
							<c:when test="${gubun == '30000001'}">
								<c:choose>
									<c:when test="${type == 'SAVING'}">
										<li class="first"><a href="/mypage/myProduct?type=SAVING&category=A000101&gubun=30000001"<c:if test="${category == 'A000101'}"> class="select"</c:if>>예금</a></li>
										<li><a href="/mypage/myProduct?type=SAVING&category=A000102&gubun=30000001"<c:if test="${category == 'A000102'}"> class="select"</c:if>>적금</a></li>
										<li><a href="/mypage/myProduct?type=SAVING&category=A000103&gubun=30000001"<c:if test="${category == 'A000103'}"> class="select"</c:if>>MMDA</a></li>
										<li class="last"><a href="/mypage/myProduct?type=SAVING&category=A000104&gubun=30000001"<c:if test="${category == 'A000104'}"> class="select"</c:if>>주택청약</a></li>
									</c:when>
									<c:when test="${type == 'LOAN'}">
										<li class="first"><a href="/mypage/myProduct?type=LOAN&category=B000101&gubun=30000001"<c:if test="${category == 'B000101'}"> class="select"</c:if>>신용대출</a></li>
										<li class="last"><a href="/mypage/myProduct?type=LOAN&category=B000102&gubun=30000001"<c:if test="${category == 'B000102'}"> class="select"</c:if>>담보대출</a></li>
									</c:when>
									<c:when test="${type == 'INSURE'}">
										<li class="first"><a href="/mypage/myProduct?type=INSURE&category=SHILSON&gubun=30000001"<c:if test="${category == 'SHILSON'}"> class="select"</c:if>>실손보험</a></li>
										<li><a href="/mypage/myProduct?type=INSURE&category=LIFE&gubun=30000001"<c:if test="${category == 'LIFE'}"> class="select"</c:if>>정기보험</a></li>
										<li><a href="/mypage/myProduct?type=INSURE&category=ANNUITY&gubun=30000001"<c:if test="${category == 'ANNUITY'}"> class="select"</c:if>>연금/저축보험</a></li>
										<li class="last"><a href="/mypage/myProduct?type=INSURE&category=CAR&gubun=30000001"<c:if test="${category == 'CAR'}"> class="select"</c:if>>자동차보험</a></li>
									</c:when>
									<c:when test="${type == 'CARD'}">
										<li class="first"><a href="/mypage/myProduct?type=CARD&category=1&gubun=30000001"<c:if test="${category == '1'}"> class="select"</c:if>>신용카드</a></li>
										<li><a href="/mypage/myProduct?type=CARD&category=2&gubun=30000001"<c:if test="${category == '2'}"> class="select"</c:if>>체크카드</a></li>
									</c:when>
								</c:choose>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${type == 'SAVING'}">
										<li class="first"><a href="/mypage/myInterest?type=SAVING&category=A000101&gubun=30000002"<c:if test="${category == 'A000101'}"> class="select"</c:if>>예금</a></li>
										<li><a href="/mypage/myInterest?type=SAVING&category=A000102&gubun=30000002"<c:if test="${category == 'A000102'}"> class="select"</c:if>>적금</a></li>
										<li><a href="/mypage/myInterest?type=SAVING&category=A000103&gubun=30000002"<c:if test="${category == 'A000103'}"> class="select"</c:if>>MMDA</a></li>
										<li class="last"><a href="/mypage/myInterest?type=SAVING&category=A000104&gubun=30000002"<c:if test="${category == 'A000104'}"> class="select"</c:if>>주택청약</a></li>
									</c:when>
									<c:when test="${type == 'LOAN'}">
										<li class="first"><a href="/mypage/myInterest?type=LOAN&category=B000101&gubun=30000002"<c:if test="${category == 'B000101'}"> class="select"</c:if>>신용대출</a></li>
										<li class="last"><a href="/mypage/myInterest?type=LOAN&category=B000102&gubun=30000002"<c:if test="${category == 'B000102'}"> class="select"</c:if>>담보대출</a></li>
									</c:when>
									<c:when test="${type == 'INSURE'}">
										<li class="first"><a href="/mypage/myInterest?type=INSURE&category=SHILSON&gubun=30000002"<c:if test="${category == 'SHILSON'}"> class="select"</c:if>>실손보험</a></li>
										<li><a href="/mypage/myInterest?type=INSURE&category=LIFE&gubun=30000002"<c:if test="${category == 'LIFE'}"> class="select"</c:if>>정기보험</a></li>
										<li><a href="/mypage/myInterest?type=INSURE&category=ANNUITY&gubun=30000002"<c:if test="${category == 'ANNUITY'}"> class="select"</c:if>>연금/저축보험</a></li>
										<li class="last"><a href="/mypage/myInterest?type=INSURE&category=CAR&gubun=30000002"<c:if test="${category == 'CAR'}"> class="select"</c:if>>자동차보험</a></li>
									</c:when>
									<c:when test="${type == 'CARD'}">
										<li class="first"><a href="/mypage/myInterest?type=CARD&category=1&gubun=30000002"<c:if test="${category == '1'}"> class="select"</c:if>>신용카드</a></li>
										<li><a href="/mypage/myInterest?type=CARD&category=2&gubun=30000002"<c:if test="${category == '2'}"> class="select"</c:if>>체크카드</a></li>
									</c:when>
									<c:when test="${type == 'P2P'}">
										<li class="first"><a href="/mypage/myInterest?type=P2P&category=L200003&gubun=30000002"<c:if test="${category == 'L200003'}"> class="select"</c:if>>투자하기</a></li>
										<li><a href="/mypage/myInterest?type=P2P&category=L200001&gubun=30000002"<c:if test="${category == 'L200001'}"> class="select"</c:if>>대출받기</a></li>
									</c:when>
								</c:choose>
							</c:otherwise>
						</c:choose>
					</ul>

					<div class="top_select">	
						<div class="row">
							<div class="col-xs-1">
								<div class="check-top">
									<div class="check-top1">
										<div class="check-bottom">
											<span>
												<input type="checkbox" id="checkbox_checkAll" value="1">
												<label id="checkAll" for="checkbox_checkAll"><i></i></label>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-4">
								<a id="btnDelete" class="btn btn-small" href="#" role="button">선택항목 삭제</a>
								<a id="btnRefresh" class="btn btn-small" href="#" role="button">새로고침</a>
							</div>
							<div class="col-xs-7 txt_right">
								판매가 종료된 상품은 목록에서 <span class="red">자동삭제</span> 됩니다.
							</div>
						</div>
					</div><!-- top_select -->

					<div id="listLayer">
					</div>
				</div>
		</div>
	</div><!-- mypage-out -->
	
	<div class="modal fade" id="product_view_popup" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog">
			<div class="modal-content modal-large">
				<button class="close" data-dismiss="modal" aria-label="Close"></button>
	
				<div class="modal-body">
					상세내용을 가져올 수 없습니다.
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="/WEB-INF/views/include/footer.jsp" />

	<script type="text/javascript" src="/resources/js/common.bundle.js"></script>
	<script type="text/javascript" src="/resources/js/common_menu.bundle.js"></script>

	<script type="text/javascript" src="/resources/lib/jquery.placeholder.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.easydropdown.js"></script>
	<script type="text/javascript" src="/resources/lib/jquery.easydropdown_scroll.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />
</body>
</html>
