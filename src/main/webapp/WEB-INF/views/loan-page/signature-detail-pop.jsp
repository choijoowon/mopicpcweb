<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><div class="product_top">
    <div class="modal-header">
        <h4 class="modal-title">상품 상세보기</h4>
    </div>
    <i class="bi bi-${data.BANK_IMG}"></i>
    <div class="product_top_view">
        <div class="left">${data.LN_NM}</div>
        <div class="right">
            <ul class="comm">
               	<c:if test="${data.LN_IS_YN eq 'Y'}"><li><i class="banking-icon internet"></i><span>온라인 가입</span></li></c:if>
            </ul>
        </div>
    </div>
    <p class="desc">${data.LN_SUMMARY1} ${data.LN_SUMMARY2}</p>
    
    <div class="summary-area annuity clearfix">
        <div class="info">
           	<c:if test="${data.LN_RATE_LOW} and ${data.LN_RATE_HIGH}">
				<span class="label-type"></span>
				<ul>
					<c:if test="${data.LN_RATE_LOW}"><li><span>최저</span>${data.LN_RATE_LOW}%</li></c:if>
					<c:if test="${data.LN_RATE_HIGH}"><li><span>최대</span>${data.LN_RATE_HIGH}%</li></c:if>
				</ul>
			</c:if>
        </div>
        <div class="btnGroup">
            <a class="btn btn-submit" href="${data.LANDING_URL}" target="_blank" id="btn-homepage" role="button">가입하기</a>
            <c:if test="${session.isLogin eq 'N'}">
	            <a class="btn btn-submit" role="button"  data-toggle="modal" data-target="#loginModal">관심상품</a>
	        </c:if>
	        <c:if test="${session.isLogin eq 'Y' and data.interest_yn eq 'N'}">
	            <a class="btn btn-submit" role="button" id="btn-interest" data-product_type="2100" data-prod_code="${data.LA_CODE}">관심상품</a>
	        </c:if>
        </div>
    </div>

</div>
<div class="product_down" id="gray-scroll">
    <div class="gray-scroll-in" >
        <div class="in_tb" >
            <table summary="신용 대출 상품, 금융기관.대출대상.대출한도.대출기간.대출금리.우대금리.원리금상환방법.기타 안내등의 컬럼으로 이루어진 표">
                <caption>신용 대출 상품 상세내용 표</caption>
                <colgroup>
                    <col style="width:130px;" />
                    <col style="*" />
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row">금융기관</th>
                        <td>${data.LN_BANKING_NM}</td>
                    </tr>
    				<tr>
                        <th scope="row">대출대상</th>
                        <td>${data.LN_TAR_CONTENT}</td>
                    </tr>
    				<tr>
                        <th scope="row">대출한도</th>
                        <td>${data.LN_LM_CONTENT}</td>
                    </tr>
                    <tr>
                        <th scope="row">대출기간</th>
                        <td>${data.LN_NOMAL_DATE_NM}</td>
                    </tr>
    				<tr>
                        <th scope="row">금리</th>
                        <td>${data.LN_RATE_INFO}~</td>
                    </tr>
                    <tr>
                        <th scope="row">우대금리</th>
                        <td>${data.LN_PR_INFO}</td>
                    </tr>
    				<tr>
                        <th scope="row">금리변동 주기</th>
                        <td>${data.LN_PERIOD_NM}</td>
                    </tr>
    				<tr>
                        <th scope="row">대출금액 범위</th>
                        <td>
                        	<c:if test="${data.LN_MINIMUM_NM != '0'}">${data.LN_MINIMUM_NM}</c:if>
                        	<c:choose>
                        		<c:when test="${data.LN_MAXIMUM_NM != '0'}"> ~ ${data.LN_MAXIMUM_NM}</c:when>
                        		<c:otherwise>제한없음</c:otherwise>
                        	</c:choose>
                        </td>
                    </tr>
    				<tr>
                        <th scope="row">신용등급</th>
                        <td>${data.LN_CLASS1_NM} ~ ${data.LN_CLASS2_NM}</td>
                    </tr>
    				<tr>
                        <th scope="row">상환방식</th>
                        <td>${data.REPAYMENT_TYPE}</td>
                    </tr>
    				<tr>
                        <th scope="row">제출서류</th>
                        <td>${data.LN_DOCUMENT}</td>
                    </tr>
    				<tr>
                        <th scope="row">온라인 가입여부</th>
                        <td>${data.LN_IS_YN}</td>
                    </tr>
                    <tr>
                        <th scope="row">기타 안내</th>
                        <td>${data.LN_ETC}</td>
                    </tr>
                </tbody>    
            </table>
        </div>
    </div>

</div>
