<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
	<head>
		<title>모픽 - 대출</title>
		<jsp:include page="/WEB-INF/views/include/header.jsp" />
		<link rel="stylesheet" href="/resources/css/loan-page.css" />
	</head>

    <body id="loan">

		<jsp:include page="/WEB-INF/views/include/top.jsp" />
		
		<div class="container">
			<div class="helper-wrap">
				<section id="helper">
				</section>
			</div>
			<div class="results-wrap">
				<section id="results">
				</section>
			</div>
		</div>


		<jsp:include page="/WEB-INF/views/include/footer.jsp" />

		<div id="concern-msg">
		</div>

		<!-- scripts -->
		<jsp:include page="/WEB-INF/views/include/common-scripts.jsp" />
		<script src="/resources/js/loan-page.bundle.js"></script>
		
		<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
		<jsp:include page="/WEB-INF/views/include/commission.jsp" />
		
    </body>

</html>
