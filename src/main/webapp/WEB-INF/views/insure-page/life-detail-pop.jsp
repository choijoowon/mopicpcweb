<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><div class="product_top">
    <style type="text/css">
        .product_down .in_tb .insure_tb td { text-align: left; padding:14px 20px 13px 20px; }
    </style>
    <div class="modal-header">
        <h4 class="modal-title">상품 상세보기</h4>
    </div>
    <i class="bi bi-${bi }"></i>
    <div class="product_top_view">
        <div class="left">${data.prod_name}</div>
        <!-- 
        <div class="right">
        <ul class="comm">
        <li class="internet">인터넷 가입</li>
        <li class="smartphone">스마트폰 가입</li>
        <li class="save">예금자보호</li>
        <li class="free">비과세</li>
        
        </ul>
        </div>
        -->
    </div>
    <div class="summary-area clearfix">
        <div class="dt-type">보험가격지수</div>
        <div class="dd-type">
            <ul>
                <li><span>${data.price_index }% ( 월보험료 : ${data.insurance_fee } 원 )</span></li>
            </ul>
        </div>
        <div class="btnGroup">
		    <a class="btn btn-submit" href="${data.homepage}" target="_blank" id="btn-homepage" role="button">홈페이지</a>
		    <c:if test="${session.isLogin eq 'N'}">
		        <a class="btn btn-submit" role="button"  data-toggle="modal" data-target="#loginModal">관심상품</a>
		    </c:if>
		    <c:if test="${session.isLogin eq 'Y' and data.interest_yn eq 'N'}">
		        <a class="btn btn-submit" role="button" id="btn-interest" data-product_type="3200" data-prod_code="${data.prod_code}">관심상품</a>
		    </c:if>
		</div>
    </div> 

</div>
<div class="product_down" id="gray-scroll">
    <div class="gray-scroll-in" >
        <div class="in_tb" >
            <table summary="정기보험 상품, 채널종류.변액여부.유니버셜여부.금리부가방식.판매일자.구분상세.보장위험명.나이.비고.첨부파일등의 컬럼으로 이루어진 표" class="insure_tb">
                <caption>실손보험 상품 상세내용 표</caption>
                <colgroup>
                    <col style="width:130px;" />
                    <col style="width:*;" />
                </colgroup>
                <thead></thead>
                <tbody>
                    <tr>
                        <th scope="row">채널종류</th>
                        <td>${data.channel}</td>
                    </tr>
                    <tr>
                        <th scope="row">변액여부</th>
                        <td>${data.variable_yn}</td>
                    </tr>
                    <tr>
                        <th scope="row">유니버셜여부</th>
                        <td>${data.universal_yn}</td>
                    </tr>
                    <tr>
                        <th scope="row">금리부가방식</th>
                        <td>${data.interest_type}</td>
                    </tr>
                    <tr>
                        <th scope="row">판매일자</th>
                        <td>${data.sell_date}</td>
                    </tr>
                    
                    <tr>
                        <th scope="row">구분상세</th>
                        <td>${data.gubun1}</td>
                    </tr>
                    <tr>
                        <th scope="row">보장위험명</th>
                        <td>${data.guarantee_item}</td>
                    </tr>
                    <tr>
                        <th scope="row">가입금액</th>
                        <td>${data.guarantee_amount }</td>
                    </tr>
                    <tr>
                        <th scope="row">나이</th>
                        <td>${data.age}</td>
                    </tr>
                    <tr>
                        <th scope="row">비고</th>
                        <td>${data.bigo}</td>
                    </tr>
                    <tr>
                        <th scope="row">첨부파일</th>
                        <td><a href=${data.pdf_url} title="첨부파일 보기" target="_blank">${data.pdf_url}</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
