<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><div class="product_top">
    <div class="modal-header">
        <h4 class="modal-title">상품 상세보기</h4>
    </div>
    <!-- 
    <img src="/resources/images/common/company_logo.png" />
    -->
    <i class="bi bi-${bi }"></i>
    
    <div class="product_top_view">
        <div class="left">${prod_name}</div>
    </div>
    <!-- <p class="desc">고객 한 분 한 분 맞춤 컨설팅을 통해 현재 보장에 미래보장을 더 해 드립니다.</p> -->
    <div class="summary-area clearfix">
        <div class="info">
            <span class="label-type">월</span>
            <ul>
                <li><span>${insurance_sum}원</span></li>
            </ul>
        </div>
        <div class="btnGroup">
            <a class="btn btn-submit" href="${homepage}" target="_blank" id="btn-homepage" role="button">홈페이지</a>
            <c:if test="${session.isLogin eq 'N'}">
            	<a class="btn btn-submit" role="button"  data-toggle="modal" data-target="#loginModal">관심상품</a>
            </c:if>
            <c:if test="${session.isLogin eq 'Y' and interest_yn eq 'N'}">
            	<a class="btn btn-submit" role="button" id="btn-interest" data-product_type="3100" data-prod_code="${prod_code}">관심상품</a>
            </c:if>
        </div>
    </div>
</div>
<div class="product_down" id="gray-scroll">
    <div class="gray-scroll-in" >
        <div class="in_tb" >
            <table summary="실손보험 상품, 보험료.직전 3년 보험료 인상률(%).직전 3년 손해율(%) 상해이비원.상해 통원. 질병 입원. 질병통원등의 컬럼으로 이루어진 표" class="insure_tb">
                <caption>실손보험 상품 상세내용 표</caption>
                <colgroup>
                    <col style="width:80px;" />
                    <col style="width:87px;" />
                    <col style="width:93px;" />
                    <col style="width:93px;" />
                    <col style="width:93px;" />
                    <col style="width:97px;" />
                    <col style="width:97px;" />
                    <col style="width:97px;" />
                </colgroup>
                <thead>
                    <tr>
                        <th scope="colgroup" rowspan="2" class="separation"></th>
                        <th scope="colgroup" rowspan="2" style="border-left:none;">보험료</th>
                        <th scope="colgroup" colspan="3">직전 3년 보험료 인상률(%)</th>
                        <th scope="colgroup" colspan="3">직전 3년 손해율(%)</th>
                    </tr>
                    <tr>
                        <th scope="col">2016</th>
                        <th scope="col">2015</th>
                        <th scope="col">2014</th>
                        <th scope="col">2014</th>
                        <th scope="col">2013</th>
                        <th scope="col">2012</th>
                    </tr>
                </thead>
                <tbody>
                    <c:choose>  
    					<c:when test="${!empty data}"> 
    						<c:forEach var="row" items="${data}">
    							<tr>
    			                    <th scope="row">${row.dambo1}${row.dambo2}</th>
    			                    <td>${row.insurance_fee}</td>
    			                    <td>${row.inc_2015}</td>
    			                    <td>${row.inc_2015}</td>
    			                    <td>${row.inc_2015}</td>
    			                    <td>${row.dec_2014}</td>
    			                    <td>${row.dec_2013}</td>
    			                    <td>${row.dec_2012}</td>
    			                </tr>
    						</c:forEach>
    					</c:when> 
    				</c:choose>
                </tbody>
            </table>
            
            <ul class="down">
            	<c:choose>
            		<c:when test="${!empty bigo}">
						<li>비고 : ${bigo }</li>
					</c:when> 
				</c:choose>
            </ul>
            
        </div>
    </div>

</div>
