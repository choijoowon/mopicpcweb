<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><div class="modal-header header_m5" style="padding-bottom:5px;">
    <h4 class="modal-title" id="myModalLabel">해지환급금 예시표</h4>
</div>
<div class="modal-body">
    <span class="s_txt" style="display:block; margin-top:-2px;">기간별 예상 해지환급금과 환급률</span>
    <div class="product_down bg_w" id="gray-scroll">
        <div class="gray-scroll-in" style="padding-top:15px;">
            <div class="in_tb" >
                <table summary="연금/저축 보험 상품, 기간별 예상 해지환급금과 환급률 컬럼으로 이루어진 표" class="insure_tb_return">
                    <caption>연금/저축 보험 상품, 기간별 예상 해지환급금과 환급률 표</caption>
                    <colgroup>
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                        <col style="width:82px;" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th scope="colgroup" rowspan="2">유지기간</th>
                            <th scope="colgroup" colspan="2">납입보험료(원)</th>
                            <th scope="colgroup" colspan="2">적립금(원)</th>
                            <th scope="colgroup" colspan="2">적립률(%)</th>
                            <th scope="colgroup" colspan="2">해지환급금(원)</th>
                        </tr>
                        <tr>
                            <th scope="col">남</th>
                            <th scope="col">여</th>
                            <th scope="col">남</th>
                            <th scope="col">여</th>
                            <th scope="col">남</th>
                            <th scope="col">여</th>
                            <th scope="col">남</th>
                            <th scope="col" class="last">여</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1년</td>
                            <td>${data[0].paid_1 }</td>
                            <td>${data[1].paid_1 }</td>
                            <td>${data[0].saved_1 }</td>
                            <td>${data[1].saved_1 }</td>
                            <td>${data[0].saved_rate_1 }</td>
                            <td>${data[1].saved_rate_1 }</td>
                            <td>${data[0].canceled_1 }</td>
                            <td class="last">${data[1].canceled_1 }</td>
                        </tr>
                        <tr>
                            <td>3년</td>
                            <td>${data[0].paid_3 }</td>
                            <td>${data[1].paid_3 }</td>
                            <td>${data[0].saved_3 }</td>
                            <td>${data[1].saved_3 }</td>
                            <td>${data[0].saved_rate_3 }</td>
                            <td>${data[1].saved_rate_3 }</td>
                            <td>${data[0].canceled_3 }</td>
                            <td class="last">${data[1].canceled_3 }</td>
                        </tr>
                        <tr>
                            <td>5년</td>
                            <td>${data[0].paid_5 }</td>
                            <td>${data[1].paid_5 }</td>
                            <td>${data[0].saved_5 }</td>
                            <td>${data[1].saved_5 }</td>
                            <td>${data[0].saved_rate_5 }</td>
                            <td>${data[1].saved_rate_5 }</td>
                            <td>${data[0].canceled_5 }</td>
                            <td class="last">${data[1].canceled_5 }</td>
                        </tr>
                        <tr>
                            <td>10년</td>
                            <td>${data[0].paid_10 }</td>
                            <td>${data[1].paid_10 }</td>
                            <td>${data[0].saved_10 }</td>
                            <td>${data[1].saved_10 }</td>
                            <td>${data[0].saved_rate_10 }</td>
                            <td>${data[1].saved_rate_10 }</td>
                            <td>${data[0].canceled_10 }</td>
                            <td class="last">${data[1].canceled_10 }</td>
                        </tr>
                        <tr>
                            <td>15년</td>
                            <td>${data[0].paid_15 }</td>
                            <td>${data[1].paid_15 }</td>
                            <td>${data[0].saved_15 }</td>
                            <td>${data[1].saved_15 }</td>
                            <td>${data[0].saved_rate_15 }</td>
                            <td>${data[1].saved_rate_15 }</td>
                            <td>${data[0].canceled_15 }</td>
                            <td class="last">${data[1].canceled_15 }</td>
                        </tr>
                        <tr>
                            <td>20년</td>
                            <td>${data[0].paid_20 }</td>
                            <td>${data[1].paid_20 }</td>
                            <td>${data[0].saved_20 }</td>
                            <td>${data[1].saved_20 }</td>
                            <td>${data[0].saved_rate_20 }</td>
                            <td>${data[1].saved_rate_20 }</td>
                            <td>${data[0].canceled_20 }</td>
                            <td class="last">${data[1].canceled_20 }</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
