<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
	<title>모픽 - 보험</title>
	<jsp:include page="/WEB-INF/views/include/header.jsp" />
	<link rel="stylesheet" href="/resources/css/insure-page.css" />
	
</head>

<body id="insure">

	<jsp:include page="/WEB-INF/views/include/top.jsp" />
	
	<div class="container">
		<div class="helper-wrap">
			<section id="helper">
			</section>
		</div>
		<c:choose>
			<c:when test="${session.requestUrl == '/ins/car'}">
				<div class="results-wrap insCar">
					<section id="results"></section>
				</div>
			</c:when>
			<c:otherwise>
				<div class="results-wrap">
					<section id="results"></section>
				</div>
			</c:otherwise>
		</c:choose>
	</div>		
	
	<jsp:include page="/WEB-INF/views/include/footer.jsp" />

	<div id="concern-msg">
	</div>
	
	<!-- scripts -->
	<jsp:include page="/WEB-INF/views/include/common-scripts.jsp" />
	<script src="/resources/js/insure-page.bundle.js"></script>
	
	<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
	<jsp:include page="/WEB-INF/views/include/commission.jsp" />
	
</body>
</html>
