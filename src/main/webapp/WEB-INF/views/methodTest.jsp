<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<html>
<header>
<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery/dist/json2.js"></script>
<script type="text/javascript">
(function($) {
    $(document).ready(function(){
        $('#btnSendTest').bind("click", function(){
        	var request = {
        		userId : $('#userId').val() 
       		};

       		$('#requestJson').text( JSON.stringify(request)); 
       		
       		$.ajax({
       			type : 'POST',									// Http Request Method로 POST로 지정
       			url : '/test/getJsonRequestTestValue',			// 서버 요청 주소
       			contentType : 'application/json;charset=UTF-8', // 서버 요청시 전송할 데이터가 UTF-8 형식의 JSON 객체임을 명시
       			data : JSON.stringify(request),					// JavaScript 객체를 JSON 객체로 변환하여 서버 요청시 전송
       			dataType : 'json',								// 서버로부터 응답받을 데이터가 JSON 객체임을 명시하여 수작업 없이 응답 데이터를 JavaScript 객체로 획득
       			success : function(response) {
       				$('#responseJson').text(JSON.stringify(response)); 
       			}
       			, error : function(jqXHR, textStatus, errorThrown) {
       				alert(jqXHR.status);
       				/*
       				if (jqXHR.status == 0) {
   	                    alert("You are offline!\nPlease Check Your Network.");
   	                } else if ( jqXHR.status == 404 ) {
   	                    alert("Requested URL not found.");
   	                } else if ( jqXHR.status == 500 ) {
   	                    alert("Internel Server Error.");
   	                } else if ( textStatus == "parsererror" ) {
   	                    alert("Error.\nParsing JSON Request failed.");
   	                } else if ( textStatus == "timeout" ) {
   	                    alert("Request Time Out.");
   	                } else {
   	                    alert("Unknow Error.\n"+jqXHR.responseText);
   	                }
       				*/
       			}
       		});
        });

    });
})(jQuery);
</script>
</header>

<body>
<table width="100%">
<tr>
	<td colspan="2">getJsonRequestTestValue</td>
</tr>
<tr>
	<td>userId</td>
	<td>
		<input type="text" id="userId" name="userId" /> 
		<input type="button" id="btnSendTest" value="btnSendTest" />
	</td>
</tr>
<tr>
	<td>userId</td>
	<td>
		<textarea id="requestJson" name="requestJson" style="width:100%; height:60px;"></textarea> 
	</td>
</tr>
<tr>
	<td>userId</td>
	<td>
		<textarea id="responseJson" name="responseJson" style="width:100%; height:60px;"></textarea>
	</td>
</tr>
</table>
</body>
</html>