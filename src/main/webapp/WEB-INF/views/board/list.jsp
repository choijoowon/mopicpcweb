<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<div class="mypage-right">
			<c:choose>
				<c:when test="${bltnName == '1:1문의'}">
					<h3>${bltnName}내역확인</h3>
				</c:when>
				<c:otherwise>
					<h3>${bltnName}</h3>
				</c:otherwise>
			</c:choose>
			
			<div id="face-to-face" class="notice_box">
				<div class="list_top">
					<span class="ques-type">문의 유형</span>
					<span class="title txt-center">제목</span>
					<span class="date">작성일</span>
					<span class="status">처리상태</span>
					<span class="delete">삭제</span>
					<span class="blank"></span>
				</div>
				<div class="panel-group" id="listLayer" role="tablist" aria-multiselectable="false">
					<c:choose>
						<c:when test="${result.data == null}">
							<div class='panel panel-default'>
								<div class='panel-heading' role='tab' id='heading01'>
									<h4 class='panel-title notice'>조회된 컨텐츠가 없습니다.</h4>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<c:forEach var="data" items="${result.data}" varStatus="x">
								<div class='panel panel-default'>
									<div class='panel-heading' role='tab' id='heading${x.index}'>
										<h4 class='panel-title notice'>
											<a class="clearfix" data-toggle='collapse' data-parent='#accordion' href='#collapse${x.index}' aria-expanded='false' aria-controls='collapse${x.index}'>
												<span class='type'>${data.categoryName}</span>
												<span class='title ellipsis'>${data.title}</span>
												<span class='date'>${data.regDate}</span>
												<c:choose>
													<c:when test="${data.ansCnt == 0}">
														<span class='ask-status'>답변대기</span>
													</c:when>
													<c:otherwise>
														<span class='ask-status'>답변완료</span>
													</c:otherwise>
												</c:choose>
												<span class="btn-area">
													<button type="button" onclick='javascript:deleteBoardData("${data.bltnCd}");' class='btn-delete'>삭제</button>
												</span>
											</a>
										</h4>
									</div>
									<div id='collapse${x.index}' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading${x.index}'>
										<div class='panel-body'>
											<div class='event_out'>${data.cts}</div>
											${data.fileVO}
											<c:if test="${data.fileVO != null and data.fileVO != '[]'}">
												<div>123${data.fileVO}
													<c:forEach var="files" items="${data.fileVO}" varStatus="y">
													<a href='/common/download?idx=${files.fileNo}'>${files.fileNm}.${files.fileExt}</a>
													</c:forEach>
												</div>
											</c:if>
											<c:if test="${data.ansCnt > 0}">
												<div class='answer'>${data.answer}</div>
											</c:if>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div><!-- panel-group -->
			
				<div id="pageNavigation" class="pagination_out"></div>
			</div>
		</div>