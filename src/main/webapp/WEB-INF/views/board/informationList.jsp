<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<div class="mypage-right">
			<h3>${bltnName}</h3>
			<c:choose>
				<c:when test="${session.requestUrl == '/information/faqList'}">
					<div id="faq" class="notice_box" style="padding:3px 0 0 0;">		
				</c:when>
				<c:otherwise>
					<div id="faq" class="notice_box" style="padding:39px 0 0 0;">
				</c:otherwise>
			</c:choose>
				<c:if test="${session.requestUrl == '/information/faqList'}">
					<div class="search_box wide" style="margin-bottom:50px;">
						<select name="searchOption" id="searchOption" tabindex="4" class="dropdown-sy" >
							<option value="" class="label" value="">전체</option>
							<c:forEach var="data" items="${category}" varStatus="x">
								<option value="${data.cd}">${data.cdName}</option>
							</c:forEach>
						</select>
						<input type="text" name="searchKey" id="searchKey" class="text-input-normal" placeholder="검색어를 입력하세요." required autofocus onfocus="if (this.placeholder == '검색어를 입력하세요.') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '검색어를 입력하세요.';}"/>
						<a href="#" id="btnSubmit" class="search_btn"></a>
					</div>
					<p class="subTitle">검색결과</p>
				</c:if>
				<c:choose>
					<c:when test="${session.requestUrl == '/information/faqList'}">
						<div class="list_top">
							<span class="sort-th">분류</span><span class="ques-th">질문</span>
						</div>
						<div class="panel-group" id="listLayer" role="tablist" aria-multiselectable="false">
					</c:when>
					<c:otherwise>
						<div class="list_top notice-header">
							<span class="number">No.</span><span class="subject">제목</span><span class="date">작성일</span><span class="blank"></span>
						</div>
						<div class="panel-group notice-board" id="listLayer" role="tablist" aria-multiselectable="false">
					</c:otherwise>
				</c:choose>
				<!--
				${result.data} 
				 -->
					<c:choose>
						<c:when test="${result.data == null}">
							<div class='panel panel-default'>
								<div class='panel-heading' role='tab' id='heading01'>
									<h4 class='panel-title notice'>조회된 컨텐츠가 없습니다.</h4>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<c:forEach var="data" items="${result.data}" varStatus="x">
								<div class='panel panel-default'>
									<div class='panel-heading' role='tab' id='heading${x.index}'>
										<h4 class='panel-title notice'>
											<a data-toggle='collapse' data-parent='#accordion' href='#collapse${x.index}' aria-expanded='false' aria-controls='collapse${x.index}'>
												<p class='num'>${data.rnum}</p>
												<p class='title'>${data.title}</p>
												<c:if test="${session.requestUrl == '/information/noticeList'}">
													<p class='date'>${data.regDate}</p>
													<p class='blank'></p>
												</c:if>
												<!--to dev -> 파란 글씨색은 <span class="emp">강조 글씨</span>-->
											</a>
										</h4>
									</div>
									<div id='collapse${x.index}' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading${x.index}'>
										<div class='panel-body'>
											<div class='carat'></div>
											<div class='event_out'>${data.cts}</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:otherwise>
					</c:choose>
				</div><!-- panel-group -->

				<div id="pageNavigation" class="pagination_out"></div>
				<c:if test="${session.requestUrl == '/information/faqList'}">
					<div class="inqury clearfix">
						<p>원하시는 답변을 찾지 못했다면 1:1 문의하기를 이용해 주세요.</p>
						<c:choose>
						<c:when test="${session.isLogin == 'N'}">
							<a href="#" data-toggle="modal" data-target="#loginModal" title="1:1 문의하기" class="btn">1:1 문의하기</a>
						</c:when>
						<c:otherwise>
							<a href="/mypage/inquire" title="1:1 문의하기" class="btn">1:1 문의하기</a>
						</c:otherwise>
					</c:choose>
					</div>
				</c:if>
			</div>
		</div>