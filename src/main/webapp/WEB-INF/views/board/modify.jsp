<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.form.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			var options = {
				url: "/mopic/api/updateBoardData"
				, type : 'POST'
				, dataType : 'text'
				, processData: false
				, contentType : false
				, beforeSend: function() {
				}
				, uploadProgress: function(event, position, total, percentComplete) {
				}
				, success: function(data) {
					location.href="/mypage/inquireList";
				}
				, complete: function(response) {
				}
				, error: function() {
				}
			};
				
			$('#boardform').ajaxForm(options);
			
			$("#btnSubmit").bind("click", function() {
				if($('#title').val().trim() == "" ) {
					alert("타이틀은 필수 입력 사항입니다.");
					$('#title').focus();
					return;
				}
				if($('#cts').val().trim() == "" ) {
					alert("내용은 필수 입력 사항입니다.");
					$('#cts').focus();
					return;
				}
				
				$("#boardform").submit();
			});
			
			$("#modifyTbody").callBoardData({bltnType : '${bltnType}', pageType : 'modify', bltnCd : '${bltnCd}', pageNum : '${pageNum}'});
		});
		</script>

		<div class="mypage-right">
			<h3>${bltnName}</h3>
			
			<form id="boardform" name="boardform" method="post" enctype="multipart/form-data">
				<input type="hidden" id="bltnType" name="bltnType" value="${bltnType}" />
				<input type="hidden" id="bltnCd" name="bltnCd" value="${bltnCd}" />
				<div class="input-box">
					<table summary="1:1 문의를 위한 문의분류, 제목, 내용, 파일첨부 입력으로 구성된 표">
						<caption>1:1 문의 입력 게시판</caption>
						<colgroup>
							 <col style="width:130px;" />
							 <col style="*" />
						</colgroup>
						<thead><tr><th></th><th></th></tr></thead>
						<tbody id="modifyTbody">
							
						</tbody>
					</table>

					<div class="right_btn"><a href="#" id="btnSubmit" class="btn btn-submit" role="button">보내기</a></div>
				</div>
			</form>
		</div>