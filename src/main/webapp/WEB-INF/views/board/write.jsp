<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.form.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			var request2 = {
				grpCd	: "1020"
				, delYn : "N"
			};
			
			$.ajax({
				type : 'POST'
				, url : "/mopic/api/selectCmnCdList"
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request2)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						var sHTML	= "";
						
						for( var i = 0; i < result.data.length; i++) {
							sHTML += "<option value='" + result.data[i].cd + "'>" + result.data[i].cdName + "</option>";
						}
						$("#category").append(sHTML);
						$("#category").addClass("dropdown-sy");
						$("#category").coverCss();
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
				
			var options = {
				url: "/mopic/api/insertBoardData"
				, type : 'POST'
				, dataType : 'text'
				, processData: false
				, contentType : false
				, beforeSend: function() {
				}
				, uploadProgress: function(event, position, total, percentComplete) {
				}
				, success: function(data) {
					location.href="/mypage/inquireList";
				}
				, complete: function(response) {
				}
				, error: function() {
				}
			};
				
			$('#boardform').ajaxForm(options);
			
			$("#btnSubmit").bind("click", function() {
				if($('#title').val().trim() == "" ) {
					alert("타이틀은 필수 입력 사항입니다.");
					$('#title').focus();
					return;
				}
				if($('#cts').val().trim() == "" ) {
					alert("내용은 필수 입력 사항입니다.");
					$('#cts').focus();
					return;
				}
				
				$("#boardform").submit();
			});

			//$('#file1').customFileInput();

		});
		</script>


		<div class="mypage-right">
			<h3>${bltnName}</h3>
			
			<form id="boardform" name="boardform" method="post" enctype="multipart/form-data">
				<input type="hidden" id="bltnType" name="bltnType" value="${bltnType}" />
				<div class="input-box">
					<table summary="1:1 문의를 위한 문의분류, 제목, 내용, 파일첨부 입력으로 구성된 표">
						<caption>1:1 문의 입력 게시판</caption>
						<colgroup>
							 <col style="width:130px;" />
							 <col style="*" />
						</colgroup>
						<thead><tr><th></th><th></th></tr></thead>
						<tbody>
							<tr>
								<th class="title2"><label for="category">문의 분류</label></th>
								<td>
									<select name="category" id="category" tabindex="4">
									</select>
								</td>
							</tr>
							<tr>
								<th class="title2"><label for="title">제목</label></th>
								<td>
									<input type="text" name="title" id="title" class="text-input-normal" placeholder="제목을 입력하세요." required autofocus onfocus="if (this.placeholder == '제목을 입력하세요.') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '제목을 입력하세요.';}"/>
								</td>
							</tr>
							<tr>
								<th class="title2"><label for="cts">내용</label></th>
								<td>
									<textarea name="cts" id="cts" cols="45" rows="10" class="text-area" placeholder="내용을 입력하세요." required autofocus onfocus="if (this.placeholder == '내용을 입력하세요.') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '내용을 입력하세요.';}"></textarea>
								</td>
							</tr>
							<!-- 
							<tr>
								<th class="title2">파일첨부</th>
								<td>
									<input type="file" id="file1" class="inputfile" name="file1" data-multiple-caption="{count} files selected" multiple /><label for="file1"><span>선택된 파일 없음</span><strong>파일선택</strong></label>
								</td>
							</tr>
							-->
						</tbody>
					</table>

					<div class="right_btn"><a href="#" id="btnSubmit" class="btn btn-submit" role="button">보내기</a></div>
				</div>
			</form>
		</div>

<script type="text/javascript" src="/resources/lib/custom-file-input.js"></script>