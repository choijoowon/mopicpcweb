<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<script src="/resources/lib/jquery/dist/jquery.min.js"></script>
<script>
/*redirectUrl에 작성 */
/*charset, 파일저장 UTF-8*/
/*한글이 깨질경우 <request.setCharacterEncoding("UTF-8");>*/

$(function(){
    opener.yellopass.fn.redirect({
        result:"<%=request.getParameter("result")%>",
        code:"<%=request.getParameter("code")%>",
        state:<%=request.getParameter("state")%>,
        access_token:"<%=request.getParameter("access_token")%>",
        auth_code:"<%=request.getParameter("auth_code")%>"
    });
});

</script>