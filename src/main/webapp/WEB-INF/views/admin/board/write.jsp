<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
					<div class="form-wrap">
						<form id="boardform" name="boardform" method="post" enctype="multipart/form-data">
						<input type="hidden" id="bltnType" name="bltnType" value="${bltnType}" />
						<input type="hidden" id="upperBltnCd" name="upperBltnCd" value="${upperBltnCd}" />
							<h3>${bltnName} 글 등록</h3>
							<table class="table">
								<caption>${bltnName} 글 등록</caption>
								<colgroup>
									<col width="130px;"></col>
									<col width="*"></col>
								</colgroup>
								<thead></thead>
								<tbody id="writeTbody">
									<c:choose>
										<c:when test="${bltnType == '1000003'}">
											<input type="hidden" id="title" name="title" value="answer" />
											<tr>
												<th>타이틀</th>
												<td>
													<span id="txt_title"></span>
												</td>
											</tr>
											<tr>
												<th>${bltnName} 글 내용</th>
												<td>
													<pre>
														<span id="txt_cts"></span>
													</pre>
												</td>
											</tr>
											<tr>
												<th>답변</th>
												<td>
													<div class="text-editor"></div>
													<textarea name="cts" id="cts" cols="45" rows="10" style="display:none;"></textarea>
												</td>
											</tr>
										</c:when>
										<c:otherwise>
											<c:if test="${bltnType == '1000002'}">
											<tr>
												<th>카테고리</th>
												<td>
													<select name="category" id="category" tabindex="4" class="dropdown-sy" >
														<c:forEach var="data" items="${category}" varStatus="x">
															<option value="${data.cd}">${data.cdName}</option>
														</c:forEach>
													</select>
												</td>
											</tr>
											</c:if>
											<tr>
												<th>타이틀</th>
												<td>
													<div class="input-group"><input type="text" maxlength="60" name="title" id="title" class="form-control" placeholder="타이틀을 입력하세요." aria-label="타이틀을 입력하세요." value="" /> <span class="input-group-addon"><span id="titleLen">0</span>/60</span></div>
												</td>
											</tr>
											<tr>
												<th>${bltnName} 글 내용</th>
												<td>
													<div class="text-editor"></div>
													<textarea name="cts" id="cts" cols="45" rows="10" style="display:none;"></textarea>
												</td>
											</tr>
										</c:otherwise>
									</c:choose>
								</tbody>
							</table>
							<div class="submit-area">
								<!-- 
								<button type="submit" id="notice-regiser-save" name="notice-regiser-save">저장</button>
								 -->
								<input type="button"  id="notice-regiser-save" name="notice-regiser-save" value="저장">
							</div>
						</form>
					</div>
					
					<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.form.min.js"></script>
					<script type="text/javascript">
					$(function() {
						$("#title").keyup(function() {
							if(!fn_checkLen($(this), 60)) {
								var temp = $(this).val();
								$(this).val(temp.substring(0, 60));
							}
							
							$("#titleLen").text($(this).val().length);
						});
						
						var requestUri = "${requestUri}";
						
						var options = {
							url: "/mopic/api/insertBoardData"
							, type : 'POST'
							, dataType : 'json'
							, processData: false
							, contentType : false
							, beforeSend: function() {
							}
							, uploadProgress: function(event, position, total, percentComplete) {
							}
							, success: function(data) {
								location.href="/admin/" + data.result.returnPage;
							}
							, complete: function(response) {
							}
							, error: function() {
							}
						};
							
						$('#boardform').ajaxForm(options);
						
						$("#notice-regiser-save").bind("click", function() {
							<c:if test="${bltnType != '1000003'}">
							if($('#title').val().trim() == "" ) {
								alert("타이틀은 필수 입력 사항입니다.");
								$('#title').focus();
								return false;
							}
							</c:if>
							
							if($('.note-editable').html() == "" ) {
								alert("내용은 필수 입력 사항입니다.");
								$('#title').focus();
								return false;
							}
							
							$("#cts").val($('.note-editable').html());
							
							$('#boardform').submit();
						});
						
						//공지사항 글 작성 textarea
						$('.text-editor').summernote({
							lang: 'ko-KR',
							height:300
						});
						
						$("#writeTbody").callBoardData({bltnType : '${bltnType}', pageType : 'write', bltnCd : '${upperBltnCd}', pageNum : '${pageNum}'});
					});
					</script>