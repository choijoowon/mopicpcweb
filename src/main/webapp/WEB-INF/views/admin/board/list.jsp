<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
					<form name="hiddenForm" id="hiddenForm">
						<input type="hidden" id="bltnType" name="bltnType" value="${bltnType}" />
						<input type="hidden" id="pageNum" name="pageNum" value="${pageNum}" />
					</form>
					<c:if test="${bltnType == 1000001}">
						<!-- 공지사항 -->
						<div class="faq-list">
							<div class="table-list">
								<div class="table-list-header clearfix">
									<h3>${bltnName}</h3>
								</div>
							</div>
							<!-- to dev -> faq 게시판과 형태가 유사합니다. -->
							<table class="faq-table table">
								<caption>${bltnName} 리스트</caption>
								<colgroup>
									<col width="90px;"></col>
									<col width="*;"></col>
									<col width="98px;"></col>
									<col width="50px;"></col>
								</colgroup>
								<thead>
									<tr>
										<th>No</th>
										<th>제목</th>
										<th>작성일</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="boardListArea"></tbody>
							</table>
						</div>
					</c:if>
					<c:if test="${bltnType == 1000002}">
						<!-- faq -->
						<div class="faq-list">
							<div class="table-list">
								<div class="table-list-header clearfix">
									<h3>${bltnName}</h3>
								</div>
							</div>
							<!-- to dev -> faq 게시판과 형태가 유사합니다. -->
							<table class="faq-table table">
								<caption>${bltnName} 리스트</caption>
								<colgroup>
									<col width="*;"></col>
									<col width="50px;"></col>
								</colgroup>
								<thead>
									<tr>
										<th>질문</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="boardListArea"></tbody>
							</table>
						</div>
					</c:if>
					<c:if test="${bltnType == 1000003}">
						<!-- 1:1문의 -->
						<div class="faq-list">
							<div class="table-list">
								<div class="table-list-header clearfix">
									<h3>${bltnName} 답변</h3>
									<div class="list-sort">
										<ul class="clearfix">
											<li class="active"><a href="/" title="전체" onclick="return false;">전체</a></li>
											<li class="vl">|</li>
											<li><a href="/" title="답변완료">답변완료</a></li>
											<li class="vl">|</li>
											<li><a href="/" title="답변미완료">답변미완료</a></li>
										</ul>
									</div>
								</div>
							</div>
							<!-- to dev 
								1. 이미지 미리보기 사이즈 가로 maxium 148px로 잡아놨고, 세로 높이값은 88px 넘어가면 안보이게 처리해놨습니다. 
								2. 답변 미완료인 경우 답변 답변하기 버튼
								3. 답변 완료인 경우 답변 수정하기 버튼 
								4. 페이지당 보여지는 리스트 8개 
								-->
							<table class="faq-table table">
								<caption>1:1 문의 답변</caption>
								<colgroup>
									<col width="90px;"></col>
									<col width="90px;"></col>
									<col width="*;"></col>
									<col width="105px;"></col>
									<col width="98px;"></col>
									<col width="50px;"></col>
								</colgroup>
								<thead>
									<tr>
										<th>처리상태</th>
										<th>문의유형</th>
										<th>제목</th>
										<th>아이디</th>
										<th>작성일</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="boardListArea"></tbody>
							</table>
						</div>
					</c:if>
						
					<!-- page nav -->
					<div id="pageNavigation"></div>
					<!-- page nav end -->