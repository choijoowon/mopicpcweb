<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <title>금융 쿠차 모픽 관리자</title>
        <meta http-equiv="X-UA-Compatible" content="text/html; charset=utf-8; IE=edge;" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keyword" content="금융, 저축, 보험, 대출, 카드, P2P, 환율, 금융 계산기, 수수료 " />
        <meta name="description" content="yfg 모픽" />
        <meta name="author" content="yfg" />
        <meta name="publisher" content="yfg" />
        <meta name="robots" content="ALL" /> 
        <meta name="robots" content="INDEX, FOLLOW" />
        
        <link type="text/css" rel="stylesheet" href="/resources/lib/admin/bootstrap/dist/css/bootstrap.css" />
        <link type="text/css" rel="stylesheet" href="/resources/lib/admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/lib/admin/font-awesome/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="/resources/lib/admin/summernote/dist/summernote.css" />
        <link type="text/css" rel="stylesheet" href="/resources/lib/admin/bootstrap-fileinput/css/fileinput.min.css" media="all" />
        <link type="text/css" rel="stylesheet" href="/resources/css/admin/admin-ui.css" />
    </head>

    <body id="admin">
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <h1><a href="/" title="Mo-Pic"><img src="/resources/images/admin/common/mp_logo.png" alt="mopic" width="114"></a></h1>
            </div>
        </div>
        <div id="content-wrapper" class="container">
            <div class="row">
                <div class="clearfix">
                    <nav class="left-navi">
                        <h2>관리</h2>
                        <ol>
                            <li>
                                <a href="/" title="추천상품 관리">추천상품 관리<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
                                <ol class="sub-list">
                                    <li><a href="/" title="상품정보 조회">상품정보 조회</a></li>
                                    <li><a href="/" title="상품등록">상품등록</a></li>
                                </ol>
                            </li>
                            <li>
                                <a href="/" title="이벤트 관리">이벤트 관리<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
                                <ol class="sub-list">
                                    <li><a href="/" title="이벤트 조회">이벤트 조회</a></li>
                                    <li><a href="/" title="이벤트 등록">이벤트 등록</a></li>
                                </ol>
                            </li>
                            <li>
                                <a href="/" title="메뉴관리">메뉴관리</a>
                            </li>
                            <li>
                                <a href="/" title="배경이미지설정">배경이미지설정</a>
                            </li>
                            <li>
                                <a href="/" title="1:1 문의 답변">1:1 문의 답변</a>
                            </li>
                            <li class="select">
                                <a href="/" title="통계">통계<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span></a>
                                <ol class="sub-list">
                                    <li class="select"><a href="/" title="Dashboard">Dashboard</a></li>
                                    <li><a href="/" title="방문현황">방문현황</a></li>
                                    <li><a href="/" title="회원분석">회원분석</a></li>
                                </ol>
                            </li>
                        </ol>
                    </nav>
                    <div class="cont-area">
                        <div class="form-wrap clearfix" style="width:730px;">
                            <form id="user-visit-view" name="user-visit-view" method="post" action="/">
                                <h3>Dashboard</h3>
                                <table class="table">
                                    <caption>방문자 Dashboard</caption>
                                    <colgroup>
                                        <col width="130px;"></col>
                                        <col width="*"></col>
                                    </colgroup>
                                    <thead></thead>
                                    <tbody>
                                        <tr>
                                            <th>기간</th>
                                            <td>
                                                <div class="register-period btn-group" role="group" aria-label="방문자 조회">
                                                    <button type="button" class="btn btn-default">일간</button>
                                                    <button type="button" class="btn btn-default">주간</button>
                                                    <button type="button" class="btn btn-default">월간</button>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="date-pic-group clearfix">
                                                    <div class="form-group">
                                                        <div class='input-group date' id='from-dpic'>
                                                            <input type='text' class="form-control" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group hv">-</div>
                                                    <div class="form-group">
                                                        <div class='input-group date' id='to-dpic'>
                                                            <input type='text' class="form-control" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="submit-area">
                                    <button type="submit" id="product-search" name="product-search">검색</button>
                                </div>
                            </form>
                        </div>
                        <div class="user-visit-count">
                            <table class="statistics table">
                                <caption>방문자 Dashboard</caption>
                                <colgroup>
                                    <col width="*"></col>
                                    <col width="120px;"></col>
                                    <col width="120px;"></col>
                                    <col width="120px;"></col>
                                    <col width="120px;"></col>
                                    <col width="120px;"></col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th rowspan="2" colspan="2" class="devide">구분</th>
                                        <th rowspan="2">전체(회원)</th>
                                        <th colspan="3">증감률</th>
                                    </tr>
                                    <tr>
                                        <th>전일 대비</th>
                                        <th>전주 대비</th>
                                        <th>전월 대비</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th rowspan="3">방문현황</th>
                                        <th>방문 횟수</th>
                                        <td>3000(400)</td>
                                        <td><span class="up">up</span>15%</td>
                                        <td><span class="up">up</span>15%</td>
                                        <td><span class="up">up</span>15%</td>
                                    </tr>
                                    <tr>
                                        <th>방문자 수</th>
                                        <td>5012(400)</td>
                                        <td><span class="down">down</span>15%</td>
                                        <td><span class="down">down</span>15%</td>
                                        <td><span class="down">down</span>15%</td>
                                    </tr>
                                    <tr>
                                        <th>페이지 뷰</th>
                                        <td>11024(430)</td>
                                        <td><span class="down">down</span>15%</td>
                                        <td><span class="down">down</span>15%</td>
                                        <td><span class="down">down</span>15%</td>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">회원현황</th>
                                        <th>전체</th>
                                        <td>3000(400)</td>
                                        <td><span class="up">up</span>15%</td>
                                        <td><span class="up">up</span>15%</td>
                                        <td><span class="up">up</span>15%</td>
                                    </tr>
                                    <tr>
                                        <th>신규</th>
                                        <td>1000(400)</td>
                                        <td><span class="up">up</span>15%</td>
                                        <td><span class="up">up</span>15%</td>
                                        <td><span class="up">up</span>15%</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <ul class="clearfix">
                <li><a href="/" title="Mo-Pic란?">Mo-Pic란?</a></li>
                <li class="vl"><span>|</span></li>
                <li><a href="/" title="서비스약관">서비스약관</a></li>
                <li class="vl"><span>|</span></li>
                <li><a href="/" title="개인정보취급방침">개인정보취급방침</a></li>
                <li class="vl"><span>|</span></li>
                <li><a href="/" title="Yello Financial Group">Yello Financial Group</a></li>
            </ul>
        </footer>

        <script src="/resources/lib/admin/jquery/dist/jquery.min.js"></script>
        <!-- file upload plugin -->
        <script src="/resources/lib/admin/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
        <script src="/resources/lib/admin/bootstrap-fileinput/js/fileinput.min.js"></script>
        <!-- datepicker plugin -->
        <script src="/resources/lib/admin/moment/min/moment-with-locales.js"></script>  
        <script src="/resources/lib/admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
        <!-- text editor plugin -->
        <script src="/resources/lib/admin/summernote/dist/summernote.min.js"></script>

        <script src="/resources/lib/admin/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/resources/lib/admin/jquery-easyui-1.4.4/jquery.easyui.min.js"></script>
        <script src="/resources/js/admin/admin-ui.js"></script>
        <script type="text/javascript">
            $(function() {

                $('#from-dpic').datetimepicker();
                $('#to-dpic').datetimepicker({
                    useCurrent: false
                });
                $("#from-dpic").on("dp.change", function (e) {
                    $('#to-dpic').data("DateTimePicker").minDate(e.date);
                });
                $("#to-dpic").on("dp.change", function (e) {
                    $('#from-dpic').data("DateTimePicker").maxDate(e.date);
                });  
            });
        </script>
    </body>
</html>