<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<!-- header -->
<%@ include file="/WEB-INF/views/admin/include/header.jsp" %>
<!-- header end -->

<script type="text/javascript">
var requestUri = "${requestUri}";
var pageNum = "${pageNum}";

$(function() {
	$("#boardListArea").callBoardList({bltnType : '${bltnType}', pageNum : '${pageNum}'});
});
</script>
	
<body id="admin">
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<h1><a href="/" title="Mo-Pic"><img src="/resources/images/admin/common/mp_logo.png" alt="mopic" width="114"></a></h1>
		</div>
	</div>
	<div id="content-wrapper" class="container">
		<div class="row">
			<div class="clearfix">
				<!-- left menu -->
				<%@ include file="/WEB-INF/views/admin/include/leftMenu.jsp" %>
				<!-- left menu end -->
				
				<!-- board list -->
				<div class="cont-area">
					<%@ include file="/WEB-INF/views/admin/board/list.jsp" %>
				</div>
				<!-- board list end -->
			</div>
		</div>
	</div>
	
	<!-- footer -->
	<%@ include file="/WEB-INF/views/admin/include/footer.jsp" %>
	<!-- footer end -->
</body>
</html>