<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <title>금융 쿠차 모픽 관리자</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keyword" content="금융, 저축, 보험, 대출, 카드, P2P, 환율, 금융 계산기, 수수료 " />
        <meta name="description" content="yfg 모픽" />
        <meta name="author" content="yfg" />
        <meta name="publisher" content="yfg" />
        <meta name="robots" content="ALL" /> 
        <meta name="robots" content="INDEX, FOLLOW" />

        <style type="text/css">
            body{font:11px/1.5 'Malgun Gothic', Verdana, Dotum, sans-serif;margin:0;padding:10px 10px 30px 10px;letter-spacing:0}
            h1 span{color:#E40509;}
            h2{font-size:11px}
            table{border-collapse:collapse;border-spacing:0}
            caption{visibility:hidden;width:0;height:0;font-size:0}
            table th{background:#eee;border:1px solid #d1d1d1;padding:3px 0}
            table td{border:1px solid #d1d1d1;padding-left:10px;padding:3px 0 3px 10px}
            table th.right, table td.right{text-align:right;padding-right:10px;padding-left:0}
            table td.center{text-align:center;padding-left:0}
            table td a{color:#193d79;}
            .index{background:#fff6dd}
            .edit{font-weight:bold;}
            .undone{color:#b90b0b;background-color:#feeeee !important;font-weight:bold}
            .done{color:#5f8b33;background-color:#eaf5e1 !important;font-weight:bold}
            .pass{color:#5f8b33;background-color:#eaf5e1 !important;font-weight:bold}
            .red{color:#e40000;}
            .red a{color:#e40000;}
            .gre{color:#006F37}
            .gre a{color:#006F37;}
                /* table info */
                .tbl-info{width:600px; margin-bottom:20px;  border-top:1px solid #ccc;  border-left:1px solid #ccc;}
                .tbl-info th{background:#e9e9e9; border-bottom:1px solid #ccc;padding:3px 0}
                .tbl-info td{border-bottom:1px solid #ccc;padding-left:10px;padding:3px 0 3px 10px}
                .tbl-info tr:hover td{background:none;}

                /* tab nav */
                .tab_nav{margin:10px 0; border-bottom:1px solid #ddd;position:relative; text-align:left;}
                .tab_nav ul{display:inline-block; margin:0 0 0 3px  !important; padding:0 !important;}
                .tab_nav ul li{display: inline-block;margin-left:-3px;}
                .ie7 .tab_nav ul, .lte7 .tab_nav ul li{display:inline;zoom:1;}
                .tab_nav ul li a{display: inline-block;margin-bottom: -1px; padding: 8px 12px; border: 1px solid #ddd;border-radius: 3px 3px 0 0; border-bottom: 0; font-size: 14px; color: #666; text-decoration: none;}
                .tab_nav ul li.current a{border:1px solid #bbb; border-bottom:1px solid #ddd; border-radius: 3px 3px 0 0; color: #fff; font-weight: bold; background: #bbb;}
                .tab_nav .tab_nav_last{position:absolute;top:0;right:0;}
                .tab_nav .count{position: relative; top: -1px; margin: 0 0 0 5px; padding: 1px 5px 2px 5px; font-size: 10px; font-weight: bold; color: #666; background: #E5E5E5; border-radius: 10px; font:11px verdana, sans-serif; line-height:1.25;}

                .ie6 .tab_nav ul{}
                .ie6 .tab_nav ul li{float:left;}
                .ie6 .tab_nav .tab_nav_last ul{float:right;}

                .tab_contents_wrap{width:100%; margin-top:10px;background:#fff;border-top:none;}
                .hide{display:none;}
                .source_download{margin:10px 0;list-style-type:disc;margin-left:10px;padding-left:10px;}

            .table {width:100%; font-size:11px; border-top:1px solid #dcdcdc; border-left:1px solid #dcdcdc; color:#666;}
        </style>
    </head>

    <body id="admin">
        <h1>모픽 관리자 메뉴링크[개발용]</h1> (<a href="/admin/pub">디자인링크 확인</a>)
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
            <colgroup>
            <col width="auto" />
            <col width="auto" />
            <col width="auto" />
            <col width="320px" />
            </colgroup>
              <tr> 
                <th width="10%">1 depth</th>
                <th width="15%">2 depth</th>
                <th width="20%">LINK</th>
                <th width="15%">작업상태</th>
                <th width="*">기타 전달 상황</th>
              </tr>
              <tr> 
                <td bgcolor="#E9F8F9">기타</td>
                <td bgcolor="#E9F8F9">기타</td>
                <td bgcolor="#E9F8F9">기타</td>
                <td bgcolor="#E9F8F9" class="bg"></td>
                <td bgcolor="#E9F8F9" class="bg"></td>
              </tr>
              <tr> 
                <td rowspan="2"><strong>1. 추천상품관리</strong></td>
                <td><strong>1-1. 상품정보 조회</strong></td>
                <td class="bg"><a href="/admin/productList">/admin/product/productList.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>1-2. 상품등록</strong></td>
                <td class="bg"><a href="/admin/productAdd">/admin/product/productAdd.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td rowspan="2"><strong>2. 이벤트관리</strong></td>
                <td><strong>2-1. 이벤트 조회</strong></td>
                <td class="bg"><a href="/admin/eventList">/admin/event/eventList.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>2-2. 이벤트 등록</strong></td>
                <td class="bg"><a href="/admin/eventAdd">/admin/event/eventAdd.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>3. 메뉴관리</strong></td>
                <td><strong>3-1. 이메뉴관리</strong></td>
                <td class="bg"><a href="/admin/menuManage">/admin/menu/menuManage.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>4. 배경이미지설정</strong></td>
                <td><strong>4-1. 배경이미지설정</strong></td>
                <td class="bg"><a href="/admin/background">/admin/background/background.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>5. 1:1 문의 답변</strong></td>
                <td><strong>5-1. 1:1 문의 답변</strong></td>
                <td class="bg"><a href="/admin/inquireList">/admin/inquire/inquireList.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td rowspan="3"><strong>6. 통계</strong></td>
                <td><strong>6-1. DashBoard</strong></td>
                <td class="bg"><a href="/admin/statisticBoard">/admin/statistic/statisticBoard.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>6-2. 방문현황</strong></td>
                <td class="bg"><a href="/admin/visitorBoard">/admin/statistic/visitorBoard.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>6-3. 회원분석</strong></td>
                <td class="bg"><a href="/admin/memberBoard">/admin/statistic/memberBoard.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td rowspan="2">7. 공지사항</td>
                <td><strong>7-1. 공지사항 글 등록</strong></td>
                <td class="bg"><a href="/admin/noticeWrite">/admin/notice/noticeWrite.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr> 
                <td><strong>7-1. 공지사항 글 리스트, 수정</strong></td>
                <td class="bg"><a href="/admin/noticeList">/admin/notice/noticeList.jsp</a></td>
                <td></td>
                <td></td>
              </tr>
            </table>
        
        <script type="text/javascript">
            
        </script>
    </body>
</html>