<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="keywords" content="금융 쿠차 모픽 관리자 공통코드">
	<meta name="description" content="금융 쿠차 모픽 관리자 공통코드">
	<title>금융 쿠차 모픽 관리자 공통코드</title>
	<link rel="stylesheet" type="text/css" href="/resources/lib/admin/jquery-easyui-1.4.4/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="/resources/lib/admin/jquery-easyui-1.4.4/themes/icon.css" />
	<link rel="stylesheet" type="text/css" href="/resources/lib/admin/jquery-easyui-1.4.4/demo/demo.css" />
	
	<style type="text/css">
	p.no_data { text-align: center; font-weight: bold; font-size: 2em; padding: 7% 0; }
	.delete-chk { display: inline-table; padding:0; margin:0 30px 0 10px; }
	.delete-chk li { display:inline-block; list-style: none; font-size: 12px; line-height: 18px; margin-left: 10px; }
	.delete-chk li:first-child { margin-left:0; }
	#minor-tb { float: right; display: none; }
	</style>
	
	<script src="/resources/lib/jquery/dist/jquery.min.js"></script>
	<script src="/resources/lib/admin/jquery-easyui-1.4.4/jquery.easyui.min.js"></script>
	
</head>
<body>
	<div>
		<h2>모픽관리자 공통코드(Major)</h2>
		<div id="major-dgt" style="width:1230px; margin:10px 0;">
			<strong>삭제 여부</strong>
			<ul class="delete-chk">
				<li><input type="radio" id="major-dc-all" name="major-delete-chk" value="A" checked="checked" /><label for="major-dc-all">전체</label></li>
				<li><input type="radio" id="major-dc-del" name="major-delete-chk" value="Y"><label for="major-dc-del">삭제</label></input></li>
				<li><input type="radio" id="major-dc-use" name="major-delete-chk" value="N" /><label for="major-dc-use">사용</label></li>
			</ul>
			<strong style="padding-right:10px;">그룹명 검색</strong>
			<input id="grp-search" class="easyui-textbox" data-options="buttonText:'조회'" style="width:200px;height:28px;" />
			<div style="float:right;">
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="insert(major_dgt)" style="display:inline-block;">신규 추가</a>
			</div>
		</div>
		<table id="major-category" class="data-grid"></table>
		<div id="major-paging" style="width:1230px; background:#eee;border:1px solid #ccc;"></div>
	</div>

	<div style="margin-top:50px;">
		<div id="minor-dgt" style="width:1230px; margin:10px 0;">
			<h2 style="display:inline-block; margin:0;">모픽관리자 공통코드(Minor)</h2>
			<div id="minor-tb">
				<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="insert(minor_dgt)" style="display:inline-block;">신규 추가</a>
			</div>
		</div>
		<table id="minor-category" class="data-grid"></table>
		<div id="minor-paging" style="width:1230px; background:#eee;border:1px solid #ccc;"></div>
	</div>
	

	<script>
		var products = [
		    {categoryid:'SAVE-S-01',name:'저축'},
		    {categoryid:'LOAN-L-01',name:'대출'},
		    {categoryid:'INSURANCE-I-01',name:'보험'},
		    {categoryid:'CARD-C-02',name:'카드'},
		    {categoryid:'P2P-P-01',name:'p2p'},
		    {categoryid:'CHANGE-C-02',name:'환율'},
		];

		/*
		major
		insertCmnGrp - grpName, grpDesc, delYn, ref1, ref2, ref3, ref4, ref5
		selectCmnCdGrpList - grpCd, grpName, cdName, delYn, pageNumber, rowCount
		updateCmnGrp - grpCd, grpName, grpDesc, delYn, sort, ref1, ref2, ref3, ref4, ref5
		minor
		insertCmnCd - grpCd, cdName, cdDesc, delYn, ref1, ref2, ref3, ref4, ref5
		selectCmnCdList - grpCd, grpName, cd, cdName, delYn, detailYn, pageNumber, rowCount
		updateCmnCd - cd, cdName, cdDesc, delYn, ref1, ref2, ref3, ref4, ref5
		*/

		var defaultUrl = "/mopic/api/";
		var config = {
			"major" : {
				"pageNumber" : "1",
				"rowCount" : "10",
				"insert" : "insertCmnGrp",
				"update" : "updateCmnGrp",
				"select" : "selectCmnCdGrpList",
				"incertCon" :
					{
						"grpName" : "",
						"grpDesc" : "",
						"delYn" : "",
						"ref1" : "",
						"ref2" : "",
						"ref3" : "",
						"ref4" : "",
						"ref5" : ""
					},
				"upCon" :
					{
						"grpCd" : "",
						"grpName" : "",
						"grpDesc" : "",
						"delYn" : "",
						"sort" : "",
						"ref1" : "",
						"ref2" : "",
						"ref3" : "",
						"ref4" : "",
						"ref5" : ""
					}
			},
			"minor" : {
				"pageNumber" : "1",
				"rowCount" : "10",
				"insert" : "insertCmnCd",
				"update" : "updateCmnCd",
				"select" : "selectCmnCdList",
				"incertCon" :
					{
						"grpCd" : "",
						"cdName" : "",
						"cdDesc" : "",
						"delYn" : "",
						"sort" : "",
						"ref1" : "",
						"ref2" : "",
						"ref3" : "",
						"ref4" : "",
						"ref5" : ""
					},
				"upCon" :
					{
						"cd" : "",
						"cdName" : "",
						"cdDesc" : "",
						"delYn" : "",
						"sort" : "",
						"ref1" : "",
						"ref2" : "",
						"ref3" : "",
						"ref4" : "",
						"ref5" : ""
					}
			}
		}

		var major_dgt;
		var minor_dgt;
		var major_reqObj = {
			'delYn' : 'A',
			'pageNumber' : '1',
			'rowCount' : '10'
		};
		var minor_reqObj = {
			'delYn' : 'A',
			'pageNumber' : '1',
			'rowCount' : '10'
		};
		var major_data;
		var minor_data;

		function convertJSON(data) {
		    var newJSON = {};
		    newJSON["total"] = data.result.totalCnt;
		    newJSON['rows'] = [];
		    for (var i = 0; i < data.result.data.length; i++) {
		        newJSON['rows'][i] = data.result.data[i];
		    }
		    return newJSON;
		}

		function searchGrp(){
			$('.textbox-button').click(function(e) {
				e.preventDefault();
				var _val = $('#grp-search').val().trim();
				if ( _val !== '' ){
					major_reqObj['grpName'] = _val;
				} else {
					major_reqObj['grpName'] = '';
				}
				common_ajax(config['major']['select'], major_reqObj, major_data, majorSet);
			});
		}

		$(function(){
			delOpt( 'major' );
			delOpt( 'minor' );
			common_ajax(config['major']['select'], major_reqObj, major_data, majorSet);
			searchGrp();
		});

		function delOpt( naming ){
			$( 'input[name="' + naming + '-delete-chk"' ).change(function(e) {
				if ( $(this).attr('name').split('-')[0] == 'major'){
					major_reqObj['delYn'] = $(this).val();
					common_ajax(config['major']['select'], major_reqObj, major_data, majorSet);
				} else {
					minor_reqObj['delYn'] = $(this).val();
					common_ajax(config['minor']['select'], minor_reqObj, minor_data, minorSet);
				}
			});
		}

		function common_ajax(url, reqObj, store, setType){
			$.ajax(
			{
				type : 'POST',
				url : defaultUrl + url,
				contentType : 'application/json;charset=UTF-8',
				data : JSON.stringify(reqObj),
				dataType : 'json',
				success : function(response) {
					if ( response.result.data !== undefined ){
						store = convertJSON(response);
						//if ( store.rows.length == 0 ){
						setType(store);
					}
				},
			    error : function(request, status, error) {
			        console.log(request);
			        console.log(status);
			        console.log(error);
			    }
			});
		}
		
		function majorSet(data_list){
			major_data = data_list;
			major_dgt = new $('#major-category').datagrid({
				title:'모픽 관리자 공통코드(Major)',
				iconCls:'icon-edit',
				width:1230,
				height:300,
				singleSelect:true,
				loadMsg:'Major Data 로딩중...',
				idField:'grpCd', 
				pagination:false,
				columns:[[
					{field:'ck',title:'ck',width:100, checkbox:"true"},
					{field:'grpCd',title:'그룹코드',width:100,align:'center'},
					{field:'grpName',title:'그룹명',width:100,align:'center',required:true, editor:{type:'validatebox', options:{ required:true }}},
					{field:'grpDesc',title:'그룹상세',width:310,align:'left',editor:'text'},
					{field:'sort',title:'정렬',width:50,align:'center',editor:{type:'numberbox'}},
					{field:'delYn',title:'삭제여부',width:50,align:'center',editor:{type:'checkbox',options:{on:'Y',off:'N'}}},
					{field:'ref1',title:'참조1',width:70,align:'center',editor:'text'},
					{field:'ref2',title:'참조2',width:70,align:'center',editor:'text'},
					{field:'ref3',title:'참조3',width:70,align:'center',editor:'text'},
					{field:'ref4',title:'참조4',width:70,align:'center',editor:'text'},
					{field:'ref5',title:'참조5',width:70,align:'center',editor:'text'},
					{field:'regDate',title:'등록일자',width:80,align:'center'},
					{field:'updDate',title:'수정일자',width:80,align:'center'},
					{field:'action',title:'Action',width:80,align:'center',
                        formatter:function(value,row,index){
                            if (row.editing){
                                var s = '<a href="javascript:void(0)" onclick="saverow(major_dgt, this)">저장</a> ';
                                var c = '<a href="javascript:void(0)" onclick="cancelrow(major_dgt, this)">취소</a>';
                                return s+c;
                            } else {
                                var e = '<a href="javascript:void(0)" onclick="editrow(major_dgt, this)">수정</a> ';
                                // var d = '<a href="javascript:void(0)" onclick="deleterow(major_dgt, this)">삭제</a>';
                                var d = '';
                                return e+d;
                            }
                        }
                    }
				]],
				onBeforeEdit:function(index,row){
					row.editing = true;
					updateActions(major_dgt, index);
				},
				onAfterEdit:function(index,row){
					row.editing = false;
					updateActions(major_dgt, index);
				},
				onCancelEdit:function(index,row){
					row.editing = false;
					updateActions(major_dgt, index);
				},
				onSelect:function(index,row){
					//minor
					if ( row.grpCd !== undefined ){
						minor_reqObj['grpCd'] = String(row.grpCd);
						minor_reqObj['grpName'] = String(row.grpName);
						minor_reqObj['detailYn'] = 'Y';
						common_ajax(
							config['minor']['select'], 
							{"grpCd":String(row.grpCd), "grpName":row.grpName, "detailYn":"A", "pageNumber":'1', "rowCount":minor_reqObj['rowCount'] }, minor_data, minorSet
							//{"grpCd":String(row.grpCd), "grpName":row.grpName, "detailYn":"Y", "pageNumber":major_reqObj['pageNumber'], "rowCount":minor_reqObj['rowCount'] }, minor_data, minorSet
						);
					}
				},
				onLoadSuccess:function (data){
					if(!data.total) {
						var no_data = '<tr><td colspan="16"><p class="no_data">데이타가 없습니다.</p></td></tr>';
						$(this).prev().find('.datagrid-btable tbody').append(no_data);
						minor_dgt.datagrid();
						$('#minor-tb').hide();
					};
				}
			}).datagrid('loadData', data_list);
			//
			$('#major-paging').pagination({
				pageList: [10,20,30,50],
				total: major_data.total,
				onSelectPage:function(pageNumber, pageSize){
					major_reqObj['pageNumber'] = String(pageNumber);
					major_reqObj['rowCount'] = String(pageSize);
					common_ajax(config['major']['select'], major_reqObj, major_data, majorSet);
				}
			});
		}

		function minorSet(data_list){
			minor_data = data_list;
			minor_dgt = new $('#minor-category').datagrid({
				title:'모픽 관리자 공통코드(Minor)',
				iconCls:'icon-edit',
				width:1230,
				height:300,
				singleSelect:true,
				loadMsg:'Minor Data 로딩중...',
				idField:'cd',
				columns:[[
					{field:'ck',title:'ck',width:100, checkbox:"true"},
					{field:'grpCd',title:'그룹코드',width:100,align:'center'},
					{field:'grpName',title:'그룹명',width:100,align:'center'},
					{field:'cd',title:'코드',width:100,align:'center'},
					{field:'cdName',title:'코드명',width:100,align:'center',editor:{type:'validatebox', options:{ required:true }}},
					{field:'cdDesc',title:'코드설명',width:200,align:'left',editor:'text'},
					{field:'sort',title:'정렬',width:50,align:'center',editor:{type:'numberbox'}},
					{field:'delYn',title:'삭제여부',width:50,align:'center',editor:{type:'checkbox',options:{on:'Y',off:'N'}}},
					{field:'ref1',title:'참조1',width:50,align:'center',editor:'text'},
					{field:'ref2',title:'참조2',width:50,align:'center',editor:'text'},
					{field:'ref3',title:'참조3',width:50,align:'center',editor:'text'},
					{field:'ref4',title:'참조4',width:50,align:'center',editor:'text'},
					{field:'ref5',title:'참조5',width:50,align:'center',editor:'text'},
					{field:'regDate',title:'등록일자',width:80,align:'center'},
					{field:'updDate',title:'수정일자',width:80,align:'center'},
					{field:'action',title:'Action',width:80,align:'center',
			            formatter:function(value,row,index){
			                if (row.editing){
			                    var s = '<a href="javascript:void(0)" onclick="saverow(minor_dgt, this)">저장</a> ';
			                    var c = '<a href="javascript:void(0)" onclick="cancelrow(minor_dgt, this)">취소</a>';
			                    return s+c;
			                } else {
			                    var e = '<a href="javascript:void(0)" onclick="editrow(minor_dgt, this)">수정</a> ';
			                    //var d = '<a href="javascript:void(0)" onclick="deleterow(minor_dgt, this)">삭제</a>';
			                    var d = '';
			                    return e+d;
			                }
			            }
			        }
				]],
				onBeforeEdit:function(index,row){
					row.editing = true;
					updateActions(minor_dgt, index);
				},
				onAfterEdit:function(index,row){
					row.editing = false;
					updateActions(minor_dgt, index);
				},
				onCancelEdit:function(index,row){
					row.editing = false;
					updateActions(minor_dgt, index);
				},
				onSelect:function(index,row){
				},
				onLoadSuccess:function (data){
					if(!data.total) {
						var no_data = '<tr><td colspan="16"><p class="no_data">데이타가 없습니다.</p></td></tr>';
						$(this).prev().find('.datagrid-btable tbody').append(no_data);
					};
					$('#minor-tb').show();
				}
			}).datagrid('loadData', data_list);
			//
			$('#minor-paging').pagination({
				pageList: [10,20,30,50],
				total: minor_data.total,
				onSelectPage:function(pageNumber, pageSize){
					minor_reqObj['pageNumber'] = String(pageNumber);
					minor_reqObj['rowCount'] = String(pageSize);
					common_ajax(config['minor']['select'], minor_reqObj, minor_data, minorSet);
				}
			});
		}

		function updateActions(dgt, index){
			dgt.datagrid('refreshRow', index);
		}
		function getRowIndex(target){
			var tr = $(target).closest('tr.datagrid-row');
			return parseInt(tr.attr('datagrid-row-index'));
		}
		function editrow(dgt, target){
			dgt.datagrid('beginEdit', getRowIndex(target));
		}
		function deleterow(dgt, target){
			var target_data = dgt.datagrid( 'getSelected', getRowIndex(target) );		
			if ( target_data == null ){
				target_data = dgt.datagrid('getData')['rows'][getRowIndex(target)];
			}
			 if ( target_data['delYn'] == 'Y' ){
			 	 $.messager.confirm('Confirm','삭제 하시겠습니까?',function(r){
			 		if (r){
			 			dgt.datagrid('deleteRow', getRowIndex(target));
			 			//common_ajax(config[dgt['selector'].split('-')[0].substr(1)]['update'], {"grpCd":target_data['target_data'], "delYn":"Y"} );
			 		}
			 	});	 
			 } else {
			 	alert('삭제 불가능합니다.');
			} 
		}
		function saverow(dgt, target){
			var convertData;
			var target_data = dgt.datagrid( 'getSelected', getRowIndex(target) );
			if ( target_data == null ){
				target_data = dgt.datagrid('getData')['rows'][getRowIndex(target)];
			}
 			dgt.datagrid('endEdit', getRowIndex(target));
 			var dgt_type = dgt['selector'].split('-')[0].substr(1);
 			var store, setType;
 			if ( dgt_type == 'major' ){
 				store = major_data;
 				setType = majorSet;
 			} else {
 				store = minor_data;
 				setType = minorSet;
 			}
 			
 			if ( target_data['status'] == 'P' ){
 				//추가 
 				convertData = convertAPIObj(dgt_type, target_data, "incertCon");
 				if ( target_data['grpName'] === undefined ){
 					//minor
	 				convertData['grpCd'] = minor_reqObj['grpCd'];
	 				common_ajax(config[dgt_type]['insert'], convertData, store, setType );
 				} else {
 					//major
 					if ( convertData['grpName'] !== '' ){
	 					common_ajax(config[dgt_type]['insert'], convertData, store, setType );
	 				}
 				}
 			} else {
 				//업데이트
 				console.log(config[dgt_type]['update']);
 				console.log(convertAPIObj(dgt_type, target_data, "upCon"));
 				common_ajax( config[dgt_type]['update'], convertAPIObj(dgt_type, target_data, "upCon"), store, setType );	
 			}
		}

		//api에 맞는 형식의 object로 변환
		function convertAPIObj(type, target_data, send_type){
			var objProto;
			if ( type == 'major' ){
				objProto = config.major[send_type];
			} else {
				objProto = config.minor[send_type];
			}
			var returnJSON = {};
			for ( property in objProto) {
			    returnJSON[property] = String(target_data[property]);
			}
		    return returnJSON;
		}
		function cancelrow(dgt, target){
			dgt.datagrid('cancelEdit', getRowIndex(target));
		}
		function insert(dgt){
			var row = dgt.datagrid('getSelected');
			if (row){
				var index = dgt.datagrid('getRowIndex', row);
			} else {
				index = 0;
			}
			if ( index == -1 ) { index = 0; }
			dgt.datagrid('insertRow', {
				index: index,
				row:{
					status:'P'
				}
			});
			dgt.datagrid('selectRow',index);
			dgt.datagrid('beginEdit',index);
		}
	</script>
	
</body>
</html>