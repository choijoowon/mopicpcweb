<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<!-- header -->
<%@ include file="/WEB-INF/views/admin/include/header.jsp" %>
<!-- header end -->

<script type="text/javascript">
$(function() {
});
</script>

<body id="admin">
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<h1><a href="/" title="Mo-Pic"><img src="/resources/images/admin/common/mp_logo.png" alt="mopic" width="114"></a></h1>
		</div>
	</div>
	<div id="content-wrapper" class="container">
		<div class="row">
			<div class="clearfix">
				<!-- left menu -->
				<%@ include file="/WEB-INF/views/admin/include/leftMenu.jsp" %>
				<!-- left menu end -->
				
				<div class="cont-area">
					<!-- board list -->
					<div class="cont-area">
						<c:choose>
							<c:when test="${bltnCd == 0 || bltnCd == null}">
								<%@ include file="/WEB-INF/views/admin/board/write.jsp" %>
							</c:when>
							<c:otherwise>
								<%@ include file="/WEB-INF/views/admin/board/modify.jsp" %>
							</c:otherwise>
						</c:choose>
						
					</div>
					<!-- board list end -->
				</div>
			</div>
		</div>
	</div>

	<!-- footer -->
	<%@ include file="/WEB-INF/views/admin/include/footer.jsp" %>
	<!-- footer end -->
</body>
</html>