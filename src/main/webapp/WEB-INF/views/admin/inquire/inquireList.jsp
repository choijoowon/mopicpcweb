<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<!-- header -->
<%@ include file="/WEB-INF/views/admin/include/header.jsp" %>
<!-- header end -->

<script type="text/javascript">
var requestUri = "${requestUri}";
var pageNum = "${pageNum}";

$(function() {
	$("#boardListArea").callBoardList({bltnType : '${bltnType}', pageNum : '${pageNum}'});
});
	/*
	$(function() {
		$('.faq-table tbody tr:not(.ques-cont)').click( function(e) {
			e.preventDefault();
			var _currentTarget = $(e.currentTarget);
				_currentTarget.toggleClass('select');
				_currentTarget.find('td .glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up')
				_currentTarget.next().toggle();
		} );

		//to dev -> _txtID 같은 경우는 답변 수정,등록시 form 전송시 글에 대한 구분자값이 필요할거 같아서 
		//안에 layer-pop hidden data로 _txtID값을 넣어서 보내도 될거 같습니다.
		$('.ques-cont button').click( function(e) {
			var _response = $(this).parent().parent().find('.response').text();
			var _txtID = $(this).parent().parent().attr('data-id');
			var _className = $(this).parent().attr('class');
			if ( _className == 'modify'){
				//답변 수정하기 -> 텍스트 수정할 수 있도록 textarea 내용 뿌려주기
				$('.note-editable').text(_response);
			};
			layerPopUp.open();
		} );
	});
		*/
</script>

<body id="admin">
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<h1><a href="/" title="Mo-Pic"><img src="/resources/images/admin/common/mp_logo.png" alt="mopic" width="114"></a></h1>
		</div>
	</div>
	<div id="content-wrapper" class="container">
		<div class="row">
			<div class="clearfix">
				<!-- left menu -->
				<%@ include file="/WEB-INF/views/admin/include/leftMenu.jsp" %>
				<!-- left menu end -->
				
				<!-- board list -->
				<div class="cont-area col-md-10">
					<%@ include file="/WEB-INF/views/admin/board/list.jsp" %>
				</div>
				<!-- board list end -->
			</div>
		</div>
	</div>

	<!-- layer popup -->
	<div class="pop-bg"></div>
	<div id="faq-answer-set" class="layer-pop">
		<a href="/" title="팝업 닫기" class="close"><img src="/resources/images/admin/btn/btn_close.png" alt="팝업닫기" width="29" /></a>
		<div class="cont-box">
			<h3>답변하기</h3>
			<div class="form-wrap">
				<form id="faq-register" name="faq-register" method="post" action="/">
					<table class="table">
						<caption>답변 내용 등록</caption>
						<colgroup>
							<col width="130px;"></col>
							<col width="*"></col>
						</colgroup>
						<thead></thead>
						<tbody>
							<tr>
								<th>답변내용</th>
								<td><div class="text-editor"></div></td>
							</tr>
						</tbody>
					</table>
					<div class="submit-area">
						<button type="submit" id="pro-regiser-save" name="pro-regiser-save">저장</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- footer -->
	<%@ include file="/WEB-INF/views/admin/include/footer.jsp" %>
	<!-- footer end -->
</body>
</html>