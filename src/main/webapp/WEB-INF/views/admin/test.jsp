<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>파일 업로드 화면</title>
<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.form.min.js"></script>
<script>
$(document).ready(function() {
	var options = {
		url: "/mopic/api/insertBoardData2"
		, type : 'POST'
		, dataType : 'text'
		, processData: false
		, contentType : false
		, beforeSend: function() {
			$("#progress").show();
	        //clear everything
	        $("#bar").width('0%');
	        $("#message").html("");
	        $("#percent").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete) {
			$("#bar").width(percentComplete+'%');
	        $("#percent").html(percentComplete+'%');
		},
		success: function(data) {
			$("#bar").width('100%');
	        $("#percent").html('100%');
		},
		complete: function(response) {
		//response text from the server.
		},
		error: function() {
		
		}
	};
		
	$('#boardform').ajaxForm(options);
	
	$("#btnSubmit").bind("click", function() {
		$("#boardform").submit();
	});
});
</script>
</head>
<body>
	<form id="boardform" name="boardform" method="post" enctype="multipart/form-data">
		<input type="text" name="asdf" id="asdf" value="123" />
		파일첨부 1 : <input type="file" id="file1" name="file" />
		<br />
		파일첨부 2 : <input type="file" id="file2" name="file" />
		<br />
		파일첨부 3 : <input type="file" id="file3" name="file" />
		<br />
		<input type="button" id="btnSubmit" name="btnSubmit" value="upload" />
	</form>
	${path}
<div id="progress">
	<div id="bar"></div>
	<div id="percent">0%</div >
</div>
<br/>
<div id="message"></div>
</body>
</html>