<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>파일 업로드 화면</title>
<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="/resources/lib/jquery/dist/jquery.form.min.js"></script>
<script type="text/javascript">
$(function() {
		var options = {
			url: "/mopic/api/insertBoardData2"
			, beforeSend: function() {
				$("#progress").show();
		        //clear everything
		        $("#bar").width('0%');
		        $("#message").html("");
		        $("#percent").html("0%");
			},
			uploadProgress: function(event, position, total, percentComplete) {
				$("#bar").width(percentComplete+'%');
		        $("#percent").html(percentComplete+'%');
			},
			success: function(data) {
				$("#bar").width('100%');
		        $("#percent").html('100%');
			},
			complete: function(response) {
			//response text from the server.
			},
			error: function() {
			
			}
		};
		
		$('#myForm').ajaxForm(options);
});
</script>
</head>
<body>

<form id="myForm" method="post" enctype="multipart/form-data">
	<input type="text" name="asdf" id="asdf" value="123" />
    <input type="file" size="60" name="myfile">
    <input type="submit" id="aa" name="aa" value="Ajax File Upload">
 </form>
 
 <div id="progress">
        <div id="bar"></div>
        <div id="percent">0%</div >
</div>
<br/>
 
<div id="message"></div>
</body>
</html>