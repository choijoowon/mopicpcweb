<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<title>금융 쿠차 모픽 관리자</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keyword" content="금융, 저축, 보험, 대출, 카드, P2P, 환율, 금융 계산기, 수수료 " />
<meta name="description" content="yfg 모픽" />
<meta name="author" content="yfg" />
<meta name="publisher" content="yfg" />
<meta name="robots" content="ALL" /> 
<meta name="robots" content="INDEX, FOLLOW" />

<style type="text/css">
	body{font:11px/1.5 'Malgun Gothic', Verdana, Dotum, sans-serif;margin:0;padding:10px 10px 30px 10px;letter-spacing:0}
	h1 span{color:#E40509;}
	h2{font-size:11px}
	table{border-collapse:collapse;border-spacing:0}
	caption{visibility:hidden;width:0;height:0;font-size:0}
	table th{background:#eee;border:1px solid #d1d1d1;padding:3px 0}
	table td{border:1px solid #d1d1d1;padding-left:0px;padding:px 0 px 0px}
	table th.right, table td.right{text-align:right;padding-right:0px;padding-left:0}
	table td.center{text-align:center;padding-left:0}
	table td a{color:#193d79;}
	.index{background:#fff6dd}
	.edit{font-weight:bold;}
	.undone{color:#b90b0b;background-color:#feeeee !important;font-weight:bold}
	.done{color:#5f8b33;background-color:#eaf5e1 !important;font-weight:bold}
	.pass{color:#5f8b33;background-color:#eaf5e1 !important;font-weight:bold}
	.red{color:#e40000;}
	.red a{color:#e40000;}
	.gre{color:#006F37}
	.gre a{color:#006F37;}
		/* table info */
		.tbl-info{width:600px; margin-bottom:20px;	border-top:1px solid #ccc;	border-left:1px solid #ccc;}
		.tbl-info th{background:#e9e9e9; border-bottom:1px solid #ccc;padding:3px 0}
		.tbl-info td{border-bottom:1px solid #ccc;padding-left:10px;padding:3px 0 3px 10px}
		.tbl-info tr:hover td{background:none;}

		/* tab nav */
		.tab_nav{margin:10px 0; border-bottom:1px solid #ddd;position:relative; text-align:left;}
		.tab_nav ul{display:inline-block; margin:0 0 0 3px	!important; padding:0 !important;}
		.tab_nav ul li{display: inline-block;margin-left:-3px;}
		.ie7 .tab_nav ul, .lte7 .tab_nav ul li{display:inline;zoom:1;}
		.tab_nav ul li a{display: inline-block;margin-bottom: -1px; padding: 8px 12px; border: 1px solid #ddd;border-radius: 3px 3px 0 0; border-bottom: 0; font-size: 14px; color: #666; text-decoration: none;}
		.tab_nav ul li.current a{border:1px solid #bbb; border-bottom:1px solid #ddd; border-radius: 3px 3px 0 0; color: #fff; font-weight: bold; background: #bbb;}
		.tab_nav .tab_nav_last{position:absolute;top:0;right:0;}
		.tab_nav .count{position: relative; top: -1px; margin: 0 0 0 5px; padding: 1px 5px 2px 5px; font-size: 10px; font-weight: bold; color: #666; background: #E5E5E5; border-radius: 10px; font:11px verdana, sans-serif; line-height:1.25;}

		.ie6 .tab_nav ul{}
		.ie6 .tab_nav ul li{float:left;}
		.ie6 .tab_nav .tab_nav_last ul{float:right;}

		.tab_contents_wrap{width:30%; margin-top:10px;background:#fff;border-top:none;}
		.hide{display:none;}
		.source_download{margin:10px 0;list-style-type:disc;margin-left:10px;padding-left:10px;}

	.table {width:30%; font-size:11px; border-top:1px solid #dcdcdc; border-left:1px solid #dcdcdc; color:#666; align:center;}
</style>

<script type="text/javascript" src="/resources/lib/admin/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
	$("#btnSubmit").bind("click", function() {
		var userId		= $("#userId").val().trim();
		var userPwd		= $("#userPwd").val().trim();
		var redirectUrl	= $("#redirectUrl").val().trim();
		
		if(userId == "") {
			alert("아이디를 입력해 주세요.");
			$("#userId").focus();
			return false;
		}
		if(userPwd == "") {
			alert("비밀번호를 입력해 주세요.");
			$("#userPwd").focus();
			return false;
		}
		
		$("#loginForm").submit();
		/*
		var request = {
			userId : userId
			, userPwd : userPwd
			, redirectUrl : redirectUrl
		};
		
		$.ajax({
			type : 'POST'
			, url : '/mopic/api/doAdminLogin'
			, contentType : 'application/json;charset=UTF-8'
			, data : JSON.stringify(request)
			, dataType : 'json'
			, success : function(response) {
				var result = response.result;
				alert(result);
				if(result.resultCode == "SUCCESS") {
					location.href="/admin";
				} else {
					alert(result.resultMessage);
				}
			}
		});
		*/
	});
	
	$("#userId").bind("enter", function() {
		$("#btnSubmit").click();
	});
	
	$("#userPwd").bind("enter", function() {
		$("#btnSubmit").click();
	});
});
</script>
</head>

<body id="admin">
<form id="loginForm" name="loginForm" method="post" action="/mopic/api/doAdminLogin">
<input type="hidden" id="redirectUrl" name="redirectUrl" value="${redirectUrl}" />
	<table align="center">
		<thead>
		</thead>
		<tbody>
			<colgroup>
				<col width="70%" />
				<col width="30%" />
			</colgroup>
			<tr> 
				<th width="10%" colspan="2">Mopic</th>
			</tr>
			<tr> 
				<td bgcolor="#E9F8F9">
					<input type="text" id="userId" name="userId" tabindex="1" />
				</td>
				<td bgcolor="#E9F8F9" rowspan="2">
					<input type="button" value="submit" id="btnSubmit" style="width:100%; height:50px;" tabindex="3" />
				</td>
			</tr>
			<tr> 
				<td bgcolor="#E9F8F9">
					<input type="password" id="userPwd" name="userPwd" tabindex="2" />
				</td>
			</tr>
		</tbody>
	</table>
</form>
</body>
</html>