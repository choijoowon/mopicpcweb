<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
					<nav class="left-navi col-md-2">
						<h2>관리</h2>
						<ol>
							<li <c:if test="${requestUri == '/admin/productList' || requestUri == '/admin/productAdd'}">class="select"</c:if>>
								<a href="/admin/productList" title="추천상품 관리">추천상품 관리<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/productList'}">
											<li class="select"><a href="/admin/productList" title="상품정보 조회">상품정보 조회</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/productList" title="상품정보 조회">상품정보 조회</a></li>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${requestUri == '/admin/productAdd'}">
											<li class="select"><a href="/admin/productAdd" title="상품등록">상품등록</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/productAdd" title="상품등록">상품등록</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
							</li>
							<li <c:if test="${requestUri == '/admin/eventList' || requestUri == '/admin/eventAdd'}">class="select"</c:if>>
								<a href="/admin/eventList" title="이벤트 관리">이벤트 관리<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/menuManage'}">
											<li class="select"><a href="/admin/eventList" title="이벤트 조회">이벤트 조회</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/eventList" title="이벤트 조회">이벤트 조회</a></li>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${requestUri == '/admin/eventAdd'}">
											<li class="select"><a href="/admin/eventAdd" title="이벤트 등록">이벤트 등록</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/eventAdd" title="이벤트 등록">이벤트 등록</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
							</li>
							<li <c:if test="${requestUri == '/admin/menuManage'}">class="select"</c:if>>
								<a href="/admin/menuManage" title="메뉴관리">메뉴관리</a>
								<!-- 
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/menuManage'}">
											<li class="select"><a href="/admin/menuManage" title="메뉴관리">메뉴관리</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/menuManage" title="메뉴관리">메뉴관리</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
								-->
							</li>
							<li <c:if test="${requestUri == '/admin/background'}">class="select"</c:if>>
								<a href="/admin/background" title="배경이미지설정">배경이미지설정</a>
								<!-- 
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/background'}">
											<li class="select"><a href="/admin/background" title="배경이미지설정">배경이미지설정</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/background" title="배경이미지설정">배경이미지설정</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
								-->
							</li>
							<li <c:if test="${requestUri == '/admin/inquireList'}">class="select"</c:if>>
								<a href="/admin/inquireList" title="1:1 문의 답변">1:1 문의 답변</a>
								<!-- 
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/inquireList'}">
											<li class="select"><a href="/admin/inquireList" title="1:1 문의 답변">1:1 문의 답변</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/inquireList" title="1:1 문의 답변">1:1 문의 답변</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
								-->
							</li>
							<li <c:if test="${requestUri == '/admin/statisticBoard' || requestUri == '/admin/visitorBoard' || requestUri == '/admin/memberBoard'}">class="select"</c:if>>
								<a href="/admin/statisticBoard" title="통계">통계<span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/statisticBoard'}">
											<li class="select"><a href="/admin/statisticBoard" title="Dashboard">Dashboard</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/statisticBoard" title="Dashboard">Dashboard</a></li>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${requestUri == '/admin/visitorBoard'}">
											<li class="select"><a href="/admin/visitorBoard" title="방문현황">방문현황</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/visitorBoard" title="방문현황">방문현황</a></li>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${requestUri == '/admin/memberBoard'}">
											<li class="select"><a href="/admin/memberBoard" title="회원분석">회원분석</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/memberBoard" title="회원분석">회원분석</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
							</li>
							<li <c:if test="${requestUri == '/admin/noticeWrite' || requestUri == '/admin/noticeList' || requestUri == '/admin/noticeModify'}">class="select"</c:if>>
								<a href="/admin/noticeList" title="공지사항">공지사항<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span></a>
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/noticeWrite'}">
											<li class="select"><a href="/admin/noticeWrite" title="공지사항 등록">공지사항 등록</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/noticeWrite" title="공지사항 등록">공지사항 등록</a></li>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${requestUri == '/admin/noticeList' || requestUri == '/admin/noticeModify'}">
											<li class="select"><a href="/admin/noticeList" title="공지사항 수정">공지사항 수정</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/noticeList" title="공지사항 수정">공지사항 수정</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
							</li>
							<li <c:if test="${requestUri == '/admin/faqWrite' || requestUri == '/admin/faqList' || requestUri == '/admin/faqModify'}">class="select"</c:if>>
								<a href="/admin/faqList" title="FAQ">FAQ<span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span></a>
								<ol class="sub-list">
									<c:choose>
										<c:when test="${requestUri == '/admin/faqWrite'}">
											<li class="select"><a href="/admin/faqWrite" title="공지사항 등록">FAQ 등록</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/faqWrite" title="공지사항 등록">FAQ 등록</a></li>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${requestUri == '/admin/faqList' || requestUri == '/admin/faqModify'}">
											<li class="select"><a href="/admin/faqList" title="공지사항 수정">FAQ 수정</a></li>
										</c:when>
										<c:otherwise>
											<li><a href="/admin/faqList" title="공지사항 수정">FAQ 수정</a></li>
										</c:otherwise>
									</c:choose>
								</ol>
							</li>
						</ol>
						<ol>
							<li>
								<a href="/admin/logout" title="1:1 문의 답변">Logout</a>
							</li>
						</ol>
					</nav>