<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<head>
	<title>금융 쿠차 모픽 관리자</title>
	<meta http-equiv="X-UA-Compatible" content="text/html; charset=utf-8; IE=edge;" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keyword" content="금융, 저축, 보험, 대출, 카드, P2P, 환율, 금융 계산기, 수수료 " />
	<meta name="description" content="yfg 모픽" />
	<meta name="author" content="yfg" />
	<meta name="publisher" content="yfg" />
	<meta name="robots" content="ALL" /> 
	<meta name="robots" content="INDEX, FOLLOW" />
	
	<link type="text/css" rel="stylesheet" href="/resources/lib/admin/bootstrap/dist/css/bootstrap.css" />
	<link type="text/css" rel="stylesheet" href="/resources/lib/admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
	<link type="text/css" rel="stylesheet" href="/resources/lib/admin/font-awesome/css/font-awesome.min.css" />
	<link type="text/css" rel="stylesheet" href="/resources/lib/admin/summernote/dist/summernote.css" />
	<link type="text/css" rel="stylesheet" href="/resources/lib/admin/bootstrap-fileinput/css/fileinput.min.css" media="all" />
	<link type="text/css" rel="stylesheet" href="/resources/css/admin/admin-ui.css" />
	
	<script type="text/javascript" src="/resources/lib/admin/jquery/dist/jquery.min.js"></script>
	<!-- file upload plugin -->
	<script type="text/javascript" src="/resources/lib/admin/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="/resources/lib/admin/bootstrap-fileinput/js/fileinput.min.js"></script>
	<!-- datepicker plugin -->
	<script type="text/javascript" src="/resources/lib/admin/moment/min/moment-with-locales.js"></script>  
	<script type="text/javascript" src="/resources/lib/admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<!-- text editor plugin -->
	<script type="text/javascript" src="/resources/lib/admin/summernote/dist/summernote.min.js"></script>

	<script type="text/javascript" src="/resources/lib/admin/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/resources/lib/admin/jquery-easyui-1.4.4/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="/resources/js/admin/admin-ui.js"></script>
	<script type="text/javascript" src="/resources/js/admin/board.js"></script>
	<script type="text/javascript" src="/resources/js/admin/common.js"></script>
</head>