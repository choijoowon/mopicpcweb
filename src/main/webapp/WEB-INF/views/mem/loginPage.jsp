<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
    <head>
    	<jsp:include page="/WEB-INF/views/include/header.jsp" />
    </head>
    
    <body id="login">
		<!-- ${session.isLogin} -->
		<jsp:include page="/WEB-INF/views/include/top.jsp" />

		<div class="container_login">
			<div class="login_top_logo">
				<img src="/resources/images/common/big_logo.png" alt="mopic" width="237" />
			</div>

			<div class="form-signin">
				<label for="page_loginId" class="sr-only">Yello One PASS 아이디</label>
				<input type="email" id="page_loginId" class="text-input-normal" placeholder="Yello One PASS 아이디 ( 이메일 )" required="" onfocus="if (this.placeholder == 'Yello PASS 아이디 ( 이메일 )') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = 'Yello PASS 아이디 ( 이메일 )';}" />

				<label for="page_loginPwd" class="sr-only">비밀번호</label>		
				<input type="password" id="page_loginPwd" class="text-input-normal" placeholder="비밀번호" required="" onfocus="if (this.placeholder == '비밀번호') {this.placeholder = '';}" onblur="if (this.placeholder == '') {this.placeholder = '비밀번호';}">

				<a id="page_loginSubmit" class="btn btn-submit" role="button">로그인</a>
				<div class="login_bottom" style="padding:10px 0 27px 0;">
					<a id="page_lookUpId">아이디 찾기</a><span class="dv">|</span><a id="page_resetPwd" class="last">비밀번호 재설정</a>
				</div>
				<a id="page_loginFb" class="btn btn-submit-facebook" role="button">페이스북으로 로그인</a> 
				<div class="login_bottom login_bottom_none">
					아직 가입 전이신가요?
					<a id="page_signUp" class="last blue">Yello One PASS 가입하기</a>
				</div>
			</div>
		</div>

		<!--<jsp:include page="/WEB-INF/views/include/footer.jsp" />-->

		<!-- scripts -->
		<script src="/resources/lib/jquery/dist/jquery.min.js"></script>
		<script src="/resources/lib/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="/resources/lib/jquery.mCustomScrollbar.concat.min.js"></script>
			
		<script src="/resources/js/common.bundle.js"></script>
		<script src="/resources/js/common_menu.bundle.js"></script>
		
		<jsp:include page="/WEB-INF/views/include/yellopass.jsp" />
		<script>
			$('#page_loginSubmit').click(function() {
				yellopass.login('login',
					$('#page_loginId').val(),
					$('#page_loginPwd').val() );
			});
			
			$('#page_loginFb').click(function() {
				yellopass.login('facebook');
			});
			
			$('#page_signUp').click(function() {
				yellopass.bypass({
					url: '/page/member',
					redirect_url: window.App.origin + '/yellopass/redirect/',
					mobile_YN: 'N'
				});
			});
			
			$('#page_lookUpId').click(function() {
				yellopass.bypass({
					url: '/page/id',
					redirect_url: window.App.origin + '/yellopass/redirect/',
					mobile_YN: 'N'
				});
			});
			
			$('#page_resetPwd').click(function() {
				yellopass.bypass({
					url: '/page/password/reset',
					redirect_url: window.App.origin + '/yellopass/redirect/',
					mobile_YN: 'N'
				});
			});
		</script>
		
		
    </body>
</html>
