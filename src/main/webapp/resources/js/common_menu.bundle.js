require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({187:[function(require,module,exports){
'use strict';

var CommonActions = require('./actions/CommonActions'),
    submenu_codes = require('./constants/submenu_codes'),
    QueryStore = require('./stores/QueryStore'),
    ProductStore = require('./stores/ProductStore');

$(function () {
	/* GNB */
	$('.gnb-list').mouseleave(function () {
		$('.gnb-depth2, .gnb-add').slideUp(200);
	});

	$('.gnb-list').mouseover(function () {
		$('.gnb-depth2:not(:animated), .gnb-add:not(:animated)').slideDown(200);
		return false;
	});

	$('.gnb-add').mouseleave(function () {
		$('.gnb-depth2, .gnb-add').slideUp(200);
	});

	// $('.gnb-depth2_sub a').click(function(evt) {
	// 	evt = evt? evt : window.event;
	// 	let target = evt.target || evt.srcElement;

	// 	evt.preventDefault();

	// 	let [, mainmenu, submenu] = $(this).attr('href').split('/');

	// 	// change URL without reloading if html5 supported
	// 	if (typeof (history.pushState) != 'undefined') {
	// 		let $elem = $(this);

	//         history.pushState({}, target.textContent, `/${mainmenu}/${submenu}`);

	// 		// change submenu
	// 		$elem.closest('ul').find('.active').removeClass('active');
	// 		$elem.addClass('active');

	// 		$elem.closest('.header')
	// 			.find('.gnb-list .gnb-depth2 a')
	// 			.removeClass('active')
	// 			.filter(`[href="/${mainmenu}/${submenu}"]`)
	// 			.addClass('active');

	// 		CommonActions.clearResults();
	// 		CommonActions.emptyComparisons();
	// 		CommonActions.selectSubmenu(mainmenu, submenu);

	// 		if(window.App && window.App.comparisons.isLoggedIn) {
	// 			if( ($.inArray(mainmenu, ['p2p', 'exchange'])) <= -1 ) {
	// 				CommonActions.fetchMyProducts(mainmenu, submenu);
	// 			}
	// 		}

	// 	} else {
	// 		window.location.href = submenu;
	// 	}
	// });
});

},{"./actions/CommonActions":186,"./constants/submenu_codes":204,"./stores/ProductStore":209,"./stores/QueryStore":210}]},{},[187])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvY29tbW9uL2NvbW1vbl9tZW51LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxJQUFJLGdCQUFnQixRQUFRLHlCQUFSLENBQWhCO0lBQ0gsZ0JBQWdCLFFBQVEsMkJBQVIsQ0FBaEI7SUFDQSxhQUFjLFFBQVEscUJBQVIsQ0FBZDtJQUNBLGVBQWUsUUFBUSx1QkFBUixDQUFmOztBQUVELEVBQUUsWUFBVzs7QUFFWixHQUFFLFdBQUYsRUFBZSxVQUFmLENBQTBCLFlBQVU7QUFDbkMsSUFBRSx1QkFBRixFQUEyQixPQUEzQixDQUFtQyxHQUFuQyxFQURtQztFQUFWLENBQTFCLENBRlk7O0FBTVosR0FBRSxXQUFGLEVBQWUsU0FBZixDQUF5QixZQUFVO0FBQ2xDLElBQUUscURBQUYsRUFBeUQsU0FBekQsQ0FBbUUsR0FBbkUsRUFEa0M7QUFFbEMsU0FBTyxLQUFQLENBRmtDO0VBQVYsQ0FBekIsQ0FOWTs7QUFXWixHQUFFLFVBQUYsRUFBYyxVQUFkLENBQXlCLFlBQVU7QUFDbEMsSUFBRSx1QkFBRixFQUEyQixPQUEzQixDQUFtQyxHQUFuQyxFQURrQztFQUFWLENBQXpCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NBWEMsQ0FBRjtBQUFhIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImxldCBDb21tb25BY3Rpb25zXHQ9IHJlcXVpcmUoJy4vYWN0aW9ucy9Db21tb25BY3Rpb25zJyksXHJcblx0c3VibWVudV9jb2Rlc1x0PSByZXF1aXJlKCcuL2NvbnN0YW50cy9zdWJtZW51X2NvZGVzJyksXHJcblx0UXVlcnlTdG9yZVx0XHQ9IHJlcXVpcmUoJy4vc3RvcmVzL1F1ZXJ5U3RvcmUnKSxcclxuXHRQcm9kdWN0U3RvcmVcdD0gcmVxdWlyZSgnLi9zdG9yZXMvUHJvZHVjdFN0b3JlJyk7XHJcblxyXG4kKGZ1bmN0aW9uKCkge1xyXG5cdC8qIEdOQiAqL1xyXG5cdCQoJy5nbmItbGlzdCcpLm1vdXNlbGVhdmUoZnVuY3Rpb24oKXtcclxuXHRcdCQoJy5nbmItZGVwdGgyLCAuZ25iLWFkZCcpLnNsaWRlVXAoMjAwKTtcclxuXHR9KTtcclxuXHJcblx0JCgnLmduYi1saXN0JykubW91c2VvdmVyKGZ1bmN0aW9uKCl7XHJcblx0XHQkKCcuZ25iLWRlcHRoMjpub3QoOmFuaW1hdGVkKSwgLmduYi1hZGQ6bm90KDphbmltYXRlZCknKS5zbGlkZURvd24oMjAwKTtcclxuXHRcdHJldHVybiBmYWxzZTtcclxuXHR9KTtcclxuXHJcblx0JCgnLmduYi1hZGQnKS5tb3VzZWxlYXZlKGZ1bmN0aW9uKCl7XHJcblx0XHQkKCcuZ25iLWRlcHRoMiwgLmduYi1hZGQnKS5zbGlkZVVwKDIwMCk7XHJcblx0fSk7XHJcblxyXG5cdC8vICQoJy5nbmItZGVwdGgyX3N1YiBhJykuY2xpY2soZnVuY3Rpb24oZXZ0KSB7XHJcblx0Ly8gXHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcclxuXHQvLyBcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xyXG5cdFx0XHJcblx0Ly8gXHRldnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcblx0Ly8gXHRsZXQgWywgbWFpbm1lbnUsIHN1Ym1lbnVdID0gJCh0aGlzKS5hdHRyKCdocmVmJykuc3BsaXQoJy8nKTtcclxuXHRcdFxyXG5cdC8vIFx0Ly8gY2hhbmdlIFVSTCB3aXRob3V0IHJlbG9hZGluZyBpZiBodG1sNSBzdXBwb3J0ZWRcclxuXHQvLyBcdGlmICh0eXBlb2YgKGhpc3RvcnkucHVzaFN0YXRlKSAhPSAndW5kZWZpbmVkJykge1xyXG5cdC8vIFx0XHRsZXQgJGVsZW0gPSAkKHRoaXMpO1xyXG5cdFx0XHRcclxuICAgIC8vICAgICAgICAgaGlzdG9yeS5wdXNoU3RhdGUoe30sIHRhcmdldC50ZXh0Q29udGVudCwgYC8ke21haW5tZW51fS8ke3N1Ym1lbnV9YCk7XHJcblxyXG5cdC8vIFx0XHQvLyBjaGFuZ2Ugc3VibWVudVxyXG5cdC8vIFx0XHQkZWxlbS5jbG9zZXN0KCd1bCcpLmZpbmQoJy5hY3RpdmUnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcblx0Ly8gXHRcdCRlbGVtLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuXHJcblx0Ly8gXHRcdCRlbGVtLmNsb3Nlc3QoJy5oZWFkZXInKVxyXG5cdC8vIFx0XHRcdC5maW5kKCcuZ25iLWxpc3QgLmduYi1kZXB0aDIgYScpXHJcblx0Ly8gXHRcdFx0LnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxyXG5cdC8vIFx0XHRcdC5maWx0ZXIoYFtocmVmPVwiLyR7bWFpbm1lbnV9LyR7c3VibWVudX1cIl1gKVxyXG5cdC8vIFx0XHRcdC5hZGRDbGFzcygnYWN0aXZlJyk7XHJcblxyXG5cclxuXHRcdFx0XHJcblx0Ly8gXHRcdENvbW1vbkFjdGlvbnMuY2xlYXJSZXN1bHRzKCk7XHJcblx0Ly8gXHRcdENvbW1vbkFjdGlvbnMuZW1wdHlDb21wYXJpc29ucygpO1xyXG5cdC8vIFx0XHRDb21tb25BY3Rpb25zLnNlbGVjdFN1Ym1lbnUobWFpbm1lbnUsIHN1Ym1lbnUpO1xyXG5cclxuXHQvLyBcdFx0aWYod2luZG93LkFwcCAmJiB3aW5kb3cuQXBwLmNvbXBhcmlzb25zLmlzTG9nZ2VkSW4pIHtcclxuXHQvLyBcdFx0XHRpZiggKCQuaW5BcnJheShtYWlubWVudSwgWydwMnAnLCAnZXhjaGFuZ2UnXSkpIDw9IC0xICkge1xyXG5cdC8vIFx0XHRcdFx0Q29tbW9uQWN0aW9ucy5mZXRjaE15UHJvZHVjdHMobWFpbm1lbnUsIHN1Ym1lbnUpO1xyXG5cdC8vIFx0XHRcdH1cclxuXHQvLyBcdFx0fVxyXG5cdFx0XHRcclxuXHJcblx0Ly8gXHR9IGVsc2Uge1xyXG5cdC8vIFx0XHR3aW5kb3cubG9jYXRpb24uaHJlZiA9IHN1Ym1lbnU7XHJcblx0Ly8gXHR9XHJcblx0Ly8gfSk7XHJcblxyXG5cdFxyXG59KTtcclxuXHJcblxyXG5cclxuXHJcbiJdfQ==
