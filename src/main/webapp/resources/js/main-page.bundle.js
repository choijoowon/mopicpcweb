require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({240:[function(require,module,exports){
'use strict';

(function () {
	var React = require('react'),
	    ReactDOM = require('react-dom'),
	    CommonActions = require('../common/actions/CommonActions'),
	    SearchSection = require('./components/SearchSection.jsx');

	CommonActions.fetchOptions(['B0001041', 3102, 3302, 3406, 4104, 'P0001', 'P0003', 1234]);

	ReactDOM.render(React.createElement(SearchSection, null), document.getElementById('search'));
})();

},{"../common/actions/CommonActions":186,"./components/SearchSection.jsx":237,"react":177,"react-dom":20}],237:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    Api = require('../../common/utils/api'),
    SearchboxStore = require('../stores/searchboxStore'),
    stepOptions = require('../constants/stepOptions'),
    InputSteps = require('./InputSteps.jsx');

var exchangeOptions = [{ code_nm: 'USD(미국 달러)', code_id: 'USD', nationlabel: '미국' }, { code_nm: 'JPY(일본 엔)', code_id: 'JPY', nationlabel: '일본' }]; // initial options before api call

var SelectOpts = function SelectOpts(label, value, data) {
	_classCallCheck(this, SelectOpts);

	this.label = label;
	this.value = value;
	if (data) {
		this.data = data;
	}
};

function _getStoreState() {
	return SearchboxStore.getState();
}

var SearchSection = React.createClass({
	displayName: 'SearchSection',

	mixins: [require('../../common/utils/queryStoreMixin')],

	propTypes: {},

	getInitialState: function getInitialState() {
		return {
			stepState: _getStoreState(),
			exchangeOptions: exchangeOptions
		};
	},

	componentDidMount: function componentDidMount() {
		Api.post('/mopic/api/exchange/selectExchangeNation', {}).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				this.setState({
					exchangeOptions: res.result.data
				});
			}
		}.bind(this));

		SearchboxStore.addChangeListner(this._onChange);
	},

	componentWillUnmount: function componentWillUnmount() {
		SearchboxStore.removeChangeListner(this._onChange);
	},

	render: function render() {
		var exchangeOptions = this.state.exchangeOptions;

		exchangeOptions = exchangeOptions.map(function (elem) {
			return new SelectOpts(elem.nationlabel, elem.code_id, elem);
		});

		return React.createElement(
			'div',
			null,
			React.createElement(
				'h4',
				null,
				'어떤 ',
				React.createElement(
					'strong',
					null,
					'금융상품'
				),
				'을 찾으러 오셨나요?'
			),
			React.createElement(InputSteps, {
				stepState: this.state.stepState,
				stepOptions: _extends({}, stepOptions, { exchangeOptions: exchangeOptions }),
				optCodeMap: this.state.queryState.optCodeMap
			})
		);
	},

	_onChange: function _onChange() {
		this.setState({
			stepState: _getStoreState()
		});
	}
});

module.exports = SearchSection;

},{"../../common/utils/api":211,"../../common/utils/queryStoreMixin":220,"../constants/stepOptions":239,"../stores/searchboxStore":241,"./InputSteps.jsx":235,"react":177}],241:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var EventEmitter = require('events').EventEmitter;
var MainDispatcher = require('../../common/dispatcher/Dispatcher');
var MainConstants = require('../constants/mainConstants');
var CHANGE_EVENT = 'change';

// Define initial query points
var _searchboxState = {
		filledSteps: 0, // steps that are handled so far
		stepSelections: ['', '', {}]
};

function changeSelection(stepNum, option, targetProp) {
		var filledSteps = _searchboxState.filledSteps;
		var stepSelections = _searchboxState.stepSelections;


		if (stepNum < filledSteps) {
				// eliminate forward selections
				for (var i = stepNum; i < filledSteps; i++) {
						stepSelections[i] = '';
				}
		}

		// initialize step 3 selection
		if (stepNum === 2) {
				stepSelections[2] = {};
		}

		if (targetProp) {
				var stepSel = stepSelections[stepNum - 1];
				if (stepSel[targetProp] === option || !option) {
						stepSel[targetProp] = '';
						_searchboxState.filledSteps = stepNum - 1;
				} else {
						stepSel[targetProp] = option;
						_searchboxState.filledSteps = stepNum;
				}
		} else {
				if (stepSelections[stepNum - 1] === option) {
						stepSelections[stepNum - 1] = '';
						_searchboxState.filledSteps = stepNum - 1;
				} else {
						stepSelections[stepNum - 1] = option;
						_searchboxState.filledSteps = stepNum;
				}
		}
}

function toggleCheckbox(stepNum, option, targetProp) {
		var stepSelections = _searchboxState.stepSelections;

		var checkedOnes = stepSelections[stepNum - 1][targetProp];

		if (!(checkedOnes instanceof Array)) {
				stepSelections[stepNum - 1][targetProp] = [];
				checkedOnes = stepSelections[stepNum - 1][targetProp];
		}

		// check if option is already selected
		var idx = checkedOnes.indexOf(option);

		if (idx > -1) {
				checkedOnes.splice(idx, 1);
		} else {
				checkedOnes.push(option);
		}

		// mark the step filled if at least one is selected
		if (checkedOnes.length > 0) {
				_searchboxState.filledSteps = stepNum;
		} else {
				_searchboxState.filledSteps = stepNum - 1;
		}
}

var SearchboxStore = _extends({}, EventEmitter.prototype, {
		addChangeListner: function addChangeListner(callback) {
				this.on(CHANGE_EVENT, callback);
		},

		removeChangeListner: function removeChangeListner(callback) {
				this.removeListener(CHANGE_EVENT, callback);
		},

		emitChange: function emitChange() {
				this.emit(CHANGE_EVENT);
		},

		getState: function getState() {
				return _searchboxState;
		}
});

// Register callback
MainDispatcher.register(function (payload) {
		var action = payload.action;

		switch (action.actionType) {

				// Change selected
				case MainConstants.CHANGE_SELECTION:
						changeSelection(action.stepNum, action.option);
						break;

				// Change checkbox state
				case MainConstants.TOGGLE_CHECKBOX:
						toggleCheckbox(action.stepNum, action.option, action.target);
						break;

				// Change selected on target prop
				case MainConstants.CHANGE_WITH_TARGET:
						changeSelection(action.stepNum, action.option, action.target);
						break;

				default:
						return false;
		}

		SearchboxStore.emitChange();

		return true;
});

module.exports = SearchboxStore;

},{"../../common/dispatcher/Dispatcher":206,"../constants/mainConstants":238,"events":5}],239:[function(require,module,exports){
// Categories and their sub-categories for the search-box
'use strict';

var stepOneOptions = [{ label: '저축', value: 'saving', icon: 'icon-01' }, { label: '대출', value: 'loan', icon: 'icon-06' }, { label: '보험', value: 'ins', icon: 'icon-10' }, { label: '카드', value: 'card', icon: 'icon-17' }, { label: 'P2P', value: 'p2p', icon: 'icon-20' }, { label: '환율', value: 'exchange', icon: 'icon-23' }];

var savingOptions = [{ label: '예금', value: 'deposit', icon: 'icon-02' }, { label: '적금', value: 'saving', icon: 'icon-03' }, { label: 'MMDA', value: 'mmda', icon: 'icon-04' }, { label: '주택청약', value: 'house', icon: 'icon-05' }];

var loanOptions = [{ label: '신용대출', value: 'signature', icon: 'icon-07' }, { label: '담보대출', value: 'secured', icon: 'icon-08' }];

var insOptions = [{ label: '실손', value: 'shilson', icon: 'icon-11' }, { label: '정기', value: 'life', icon: 'icon-12' }, { label: '연금/저축', value: 'annuity', icon: 'icon-13' }, { label: '자동차', value: 'car', icon: 'icon-14' }];

var cardOptions = [{ label: '신용카드', value: 'credit', icon: 'icon-18' }, { label: '체크카드', value: 'check', icon: 'icon-19' }];

var p2pOptions = [{ label: '투자하기', value: 'invest', icon: 'icon-21' }, { label: '대출하기', value: 'ploan', icon: 'icon-22' }];

module.exports = {
	stepOneOptions: stepOneOptions,
	savingOptions: savingOptions,
	loanOptions: loanOptions,
	insOptions: insOptions,
	cardOptions: cardOptions,
	p2pOptions: p2pOptions
};

},{}],235:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    CheckboxDropdown = require('../../common/components/CheckboxDropdown.jsx'),
    TextInput = require('../../common/components/TextInput.jsx'),
    InsureInputDropdown = require('./InsureInputDropdown.jsx'),
    MainActions = require('../actions/MainActions');

var SelectOpt = function SelectOpt(label, value, data) {
	_classCallCheck(this, SelectOpt);

	this.label = label;
	this.value = value;
	if (data) {
		this.data = data;
	}
};

var InputSteps = React.createClass({
	displayName: 'InputSteps',

	propTypes: {
		stepState: React.PropTypes.object.isRequired,
		stepOptions: React.PropTypes.object.isRequired,
		optCodeMap: React.PropTypes.object.isRequired
	},

	getDefaultProps: function getDefaultProps() {},

	componentDidMount: function componentDidMount() {},

	componentWillUnmount: function componentWillUnmount() {},

	render: function render() {
		var _props = this.props;
		var stepState = _props.stepState;
		var stepOptions = _props.stepOptions;
		var optCodeMap = _props.optCodeMap;

		var stepTwoPlaceholder = undefined,
		    StepThreeElem = undefined,
		    stepThreePlaceholder = undefined,
		    stepThreeOptions = undefined,
		    stepThreeUnit = undefined;

		var stepSelections = stepState.stepSelections;
		var stepOneOptions = stepOptions.stepOneOptions;

		var stepTwoOptions = stepOptions[stepSelections[0].toLowerCase() + 'Options'];

		StepThreeElem = React.createElement(SelectDropdown, {
			placeholder: '-',
			options: ['-'],
			disabled: this.isTabToDisable(3),
			handleSelect: this.handleSelectOptionClick.bind(null, 3, false),
			selected: '',
			className: 'main-select-dropdown' });

		switch (stepSelections[0].toLowerCase()) {
			case 'saving':
				stepTwoPlaceholder = '어떤 저축상품을 찾으세요?';

				if (stepSelections[1]) {
					stepThreeUnit = '만원';

					switch (stepSelections[1].toLowerCase()) {
						case 'deposit':
							stepThreePlaceholder = '예치할 금액을 입력하세요';
							break;
						case 'saving':
							stepThreePlaceholder = '월 납입금액을 입력하세요';
							break;
						case 'mmda':
							stepThreePlaceholder = '예치할 금액을 입력하세요';
							break;
						case 'house':
							stepThreePlaceholder = '월 납입금액을 입력하세요';
							break;
						default:
							stepThreePlaceholder = '-';
					}

					StepThreeElem = React.createElement(TextInput, {
						placeholder: stepThreePlaceholder,
						type: 'number-with-commas',
						handleChange: this.handleTextInputChange.bind(null, 3, 'amount'),
						value: stepSelections[2].amount || '',
						unit: stepThreeUnit });
				}

				break;

			case 'loan':
				stepTwoPlaceholder = '어떤 대출상품을 찾으세요?';

				if (stepSelections[1]) {
					switch (stepSelections[1].toLowerCase()) {
						case 'signature':
							stepThreePlaceholder = '대출받을 금액을 입력해주세요.';
							stepThreeUnit = '만원';
							StepThreeElem = React.createElement(TextInput, {
								placeholder: stepThreePlaceholder,
								type: 'number-with-commas',
								handleChange: this.handleTextInputChange.bind(null, 3, 'amount'),
								value: stepSelections[2].amount || '',
								unit: stepThreeUnit });
							break;
						case 'secured':
							stepThreeOptions = [{ value: 'B00010809', label: '없음' }, { value: 'B00010801', label: '아파트' }, { value: 'B00010802', label: '부동산' }, { value: 'B00010803', label: '전세' }, { value: 'B00010804', label: '월세' }, { value: 'B00010805', label: '자동차' }, { value: 'B00010806', label: '예적금' }, { value: 'B00010807', label: '주식' }, { value: 'B00010808', label: '기타' }];
							stepThreePlaceholder = '담보유형을 선택해주세요.';
							StepThreeElem = React.createElement(SelectDropdown, {
								placeholder: stepThreePlaceholder,
								options: stepThreeOptions,
								disabled: this.isTabToDisable(3),
								handleSelect: this.handleSelectOptionClick.bind(null, 3, 'secureType', false),
								selected: stepSelections[2].secureType || '',
								className: 'main-select-dropdown' });
							break;
						default:
							stepThreeOptions = [];
							stepThreePlaceholder = '-';
							StepThreeElem = React.createElement(SelectDropdown, {
								placeholder: stepThreePlaceholder,
								options: [],
								disabled: this.isTabToDisable(3),
								selected: '',
								className: 'main-select-dropdown' });
					}
				}

				break;

			case 'ins':
				stepTwoPlaceholder = '어떤 목적으로 상품을 찾으세요?';

				if (stepSelections[1]) {
					var entries = undefined;

					switch (stepSelections[1].toLowerCase()) {
						case 'shilson':
							entries = [{
								label: '생년월일',
								type: 'number',
								placeholder: 'YYMMDD',
								maxlength: '6',
								id: 'birthConvertToAge' // convert to insure age when sending requests
							}, {
								label: '선택담보',
								type: 'select',
								id: 'dambo1',
								options: this.getSelectOptions(optCodeMap, 3102)
							}];

							break;

						case 'life':
							entries = [{
								label: '보험나이',
								type: 'numFixed',
								id: 'age',
								fixedAs: '40'
							}];

							break;

						case 'annuity':
							entries = [{
								label: '보험나이',
								type: 'select',
								id: 'age',
								options: this.getSelectOptions(optCodeMap, 3302)
							}];

							break;

						case 'car':
							entries = [{
								label: '보험나이',
								type: 'select',
								id: 'age',
								options: this.getSelectOptions(optCodeMap, 3406),
								dependencies: ['age_contract']
							}];
							break;
					}
					stepThreePlaceholder = '생년월일과 성별을 입력하세요';
					StepThreeElem = React.createElement(InsureInputDropdown, {
						placeholder: stepThreePlaceholder,
						entries: entries,
						disabled: this.isTabToDisable(3),
						handleSelect: this.handleSelectOptionClick,
						handleTextChange: this.handleTextInputChange,
						stepSelections: stepSelections
					});
				}

				break;

			case 'card':
				stepTwoPlaceholder = '어떤 카드를 찾으세요?';

				if (stepSelections[1]) {
					stepThreePlaceholder = '혜택을 최대 3개까지 선택해주세요';
					stepThreeOptions = this.getSelectOptions(optCodeMap, 4104);

					StepThreeElem = React.createElement(CheckboxDropdown, {
						placeholder: stepThreePlaceholder,
						options: stepThreeOptions,
						disabled: this.isTabToDisable(3),
						handleSelect: this.handleSelectOptionClick.bind(null, 3, 'benefitsToSplit', true),
						limitSelection: 3,
						selected: stepSelections[2].benefitsToSplit,
						className: 'main-select-dropdown multi-select' });
				}

				break;
			case 'p2p':
				stepTwoPlaceholder = '어떤 목적으로 상품을 찾으세요?';

				if (stepSelections[1]) {
					var attrId = 'none';

					switch (stepSelections[1]) {
						case 'invest':
							stepThreeOptions = this.getSelectOptions(optCodeMap, 'P0001');
							stepThreePlaceholder = '투자할 업체를 선택해주세요';
							attrId = 'company_code';
							break;
						case 'ploan':
							stepThreeOptions = this.getSelectOptions(optCodeMap, 'P0003');
							stepThreePlaceholder = '대출 유형을 선택해주세요';
							attrId = 'gubun';
							break;
						default:
							stepThreeOptions = [];
							stepThreePlaceholder = '-';
					}

					StepThreeElem = React.createElement(SelectDropdown, {
						placeholder: stepThreePlaceholder,
						options: stepThreeOptions,
						disabled: this.isTabToDisable(3),
						handleSelect: this.handleSelectOptionClick.bind(null, 3, attrId, false),
						selected: stepSelections[2][attrId] || '',
						className: 'main-select-dropdown' });
				}

				break;

			case 'exchange':
				stepTwoPlaceholder = '국가를 선택해 주세요';

				if (stepSelections[1]) {
					stepThreePlaceholder = '환전할 금액을 입력하세요';
					var nationOpt = stepTwoOptions.find(function (elem) {
						return elem.value === stepSelections[1];
					});
					//stepThreeUnit = nationOpt.data.code_nm.replace(`${nationOpt.label} `, '');
					stepThreeUnit = 'KRW(원)';

					StepThreeElem = React.createElement(TextInput, {
						placeholder: stepThreePlaceholder,
						type: 'number-with-commas',
						handleChange: this.handleTextInputChange.bind(null, 3, 'money'),
						value: stepSelections[2].money || '',
						unit: stepThreeUnit });
				}

				break;

			default:
				stepTwoOptions = [];
				stepTwoPlaceholder = '-';

				StepThreeElem = React.createElement(SelectDropdown, {
					placeholder: '-',
					options: ['1', '2'],
					disabled: this.isTabToDisable(3),
					handleSelect: this.handleSelectOptionClick.bind(null, 3, false),
					selected: '',
					className: 'main-select-dropdown' });
		}

		return React.createElement(
			'div',
			{ className: 'steps-wrapper' },
			React.createElement(
				'div',
				{ className: 'step' },
				React.createElement(SelectDropdown, {
					placeholder: '금융상품을 선택해 주세요',
					options: stepOneOptions,
					disabled: this.isTabToDisable(1),
					handleSelect: this.handleMenuOptionClick.bind(null, 1, false),
					selected: stepSelections[0],
					className: 'main-select-dropdown'
				})
			),
			React.createElement(
				'div',
				{ className: 'step' },
				React.createElement(SelectDropdown, {
					placeholder: stepTwoPlaceholder,
					options: stepTwoOptions,
					disabled: this.isTabToDisable(2),
					handleSelect: this.handleMenuOptionClick.bind(null, 2, false),
					selected: stepSelections[1],
					className: 'main-select-dropdown'
				})
			),
			React.createElement(
				'div',
				{ className: 'step' },
				StepThreeElem
			),
			React.createElement(
				'a',
				{ className: 'search_btn', onClick: this.sendQuery },
				'search'
			)
		);
	},

	isTabToDisable: function isTabToDisable(tabNum) {
		return !(this.props.stepState.filledSteps >= tabNum - 1);
	},

	handleMenuOptionClick: function handleMenuOptionClick(stepNum, isMultiSelect, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('li').attr('data-val');
		val = val === 0 ? 0 : val || target.textContent;

		MainActions.changeSelection(stepNum, val);
	},

	handleTextInputChange: function handleTextInputChange(stepNum, attr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		MainActions.changeWithTarget(stepNum, attr, target.value);
	},

	handleSelectOptionClick: function handleSelectOptionClick(stepNum, attr, isMultiSelect, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		evt.stopPropagation();

		var val = $(target).closest('li').attr('data-val');
		val = val === 0 ? 0 : val || target.textContent;

		if (isMultiSelect) {
			MainActions.toggleCheckbox(stepNum, attr, val);
		} else {
			MainActions.changeWithTarget(stepNum, attr, val);
		}
	},

	getSelectOptions: function getSelectOptions(optCodeMap, code) {
		if (optCodeMap[code]) {
			return optCodeMap[code].map(function (e) {
				return new SelectOpt(e.cdName, e.cd);
			});
		}
		// TODO: when no map for the code?
		return [];
	},
	sendQuery: function sendQuery() {
		var stepState = this.props.stepState;
		var filledSteps = stepState.filledSteps;
		var stepSelections = stepState.stepSelections;


		if (filledSteps === 3) {
			var searchQuery = stepSelections[2];

			if (stepSelections[0].toLowerCase() === 'exchange') {
				searchQuery = _extends({}, searchQuery, { tr_nation: stepSelections[1], exchange_type: 'TR_BUY_CASH' }); // 거래종류 현찰살때

				window.open('exchange/exchange?' + $.param(searchQuery), '_self');
				return true;
			}

			switch (stepSelections[1].toLowerCase()) {
				case 'deposit':
					searchQuery = _extends({}, searchQuery, { periodCd: 'A0001010103' }); //예치기간 1년
					break;
				case 'saving':
					searchQuery = _extends({}, searchQuery, { periodCd: 'A0001020104' }); //가입기간 1년
					break;
				case 'house':
					searchQuery = _extends({}, searchQuery, { periodCd: 'A0001040101' }); //납입기간 1년
					break;
				case 'credit':
				case 'check':
					searchQuery.benefits = searchQuery.benefitsToSplit.join('|');
					delete searchQuery.benefitsToSplit;

					searchQuery = _extends({}, searchQuery, { company: 'all', fee: 'A', month_sum: '50' }); // 카드사 전체, 연회비 전체, 월평균사용액 50만원
					break;
				case 'ploan':
					searchQuery = _extends({}, searchQuery, { company_code: '99999999' }); //업체 전체
					break;
			}

			window.open(stepSelections[0] + '/' + stepSelections[1] + '?' + $.param(searchQuery), '_self');
		}
	}
});

module.exports = InputSteps;

},{"../../common/components/CheckboxDropdown.jsx":188,"../../common/components/SelectDropdown.jsx":197,"../../common/components/TextInput.jsx":200,"../actions/MainActions":234,"./InsureInputDropdown.jsx":236,"react":177}],236:[function(require,module,exports){
'use strict';

var React = require('react'),
    TextInput = require('../../common/components/TextInput.jsx'),
    SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    SelectList = require('../../common/components/SelectList.jsx'),
    MainActions = require('../actions/MainActions');

var InsureInputDropdown = React.createClass({
	displayName: 'InsureInputDropdown',

	mixins: [require('../../common/utils/click-outside-mixin')],

	propTypes: {
		placeholder: React.PropTypes.string,
		entries: React.PropTypes.array,
		disabled: React.PropTypes.bool,
		handleSelect: React.PropTypes.func,
		handleTextChange: React.PropTypes.func,
		stepSelections: React.PropTypes.array
	},

	getDefaultProps: function getDefaultProps() {
		return {
			placeholder: '-',
			entries: [],
			disabled: false
		};
	},

	getInitialState: function getInitialState() {
		return {
			isOpen: false
		};
	},

	componentDidMount: function componentDidMount() {
		this.setOnClickOutside('main', this.handleOutsideClick);
	},

	componentWillUnmount: function componentWillUnmount() {},

	render: function render() {
		var props = this.props;
		var _props = this.props;
		var entries = _props.entries;
		var handleSelect = _props.handleSelect;
		var handleTextChange = _props.handleTextChange;
		var stepSelections = _props.stepSelections;


		var queries = entries.map(function (entry) {
			var queryElem = undefined;
			switch (entry.type) {
				case 'number':
				case 'number-with-commas':
				case 'text':
					queryElem = React.createElement(
						'div',
						{ className: 'dropdown-options-sm-form', key: entry.label },
						React.createElement(
							'label',
							{ className: 'select-label' },
							entry.label || ''
						),
						React.createElement(TextInput, {
							type: entry.type,
							placeholder: entry.placeholder || ' ',
							inputClass: 'text-input-normal',
							maxlength: entry.maxlength,
							handleChange: handleTextChange.bind(null, 3, entry.id),
							value: stepSelections[2][entry.id],
							unit: entry.unit || ''
						})
					);
					break;
				case 'numFixed':
					queryElem = React.createElement(
						'div',
						{ className: 'dropdown-options-sm-form', key: entry.label },
						React.createElement(
							'label',
							{ className: 'select-label' },
							entry.label || ''
						),
						React.createElement(TextInput, {
							placeholder: ' ',
							inputClass: 'text-input-normal',
							fixed: true,
							value: entry.fixedAs,
							unit: entry.unit || '' })
					);
					break;
				case 'select':
					queryElem = React.createElement(
						'div',
						{ className: 'dropdown-options-sm-form', key: entry.label },
						React.createElement(
							'label',
							{ className: 'select-label' },
							entry.label || ''
						),
						React.createElement(SelectDropdown, {
							tabIndex: 0,
							placeholder: entry.placeholder || '선택',
							options: entry.options,
							handleSelect: handleSelect.bind(null, 3, entry.id, false),
							className: entry.className || 'select-dropdown',
							selected: stepSelections[2][entry.id]
						})
					);
					break;

			}

			return queryElem;
		});

		return React.createElement(
			'div',
			{ className: 'main-select-dropdown', ref: 'main',
				onClick: this.toggleOpen },
			React.createElement(
				'span',
				null,
				props.placeholder
			),
			React.createElement(
				'div',
				{ className: 'dropdown-options-sm' + (!props.disabled && this.state.isOpen ? ' open' : ''),
					onClick: this.handleInsideClick },
				queries,
				React.createElement(SelectList, {
					name: 'sex',
					options: [{ label: '남', dataAttrs: { value: '1' }, icon: 'blank' }, { label: '여', dataAttrs: { value: '2' }, icon: 'blank' }],
					value: stepSelections[2].sex,
					handleSelect: this.handleSelect })
			)
		);
	},

	toggleOpen: function toggleOpen(e) {
		if (!this.props.disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			});
		}
	},

	handleOutsideClick: function handleOutsideClick(evt) {
		this.setState({
			isOpen: false
		});
	},

	handleInsideClick: function handleInsideClick(e) {
		e.stopPropagation();
	},

	handleSelect: function handleSelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		evt.stopPropagation();

		var val = $(target).closest('li').attr('data-value');
		val = val === 0 ? 0 : val || target.textContent;

		MainActions.changeWithTarget(3, 'sex', val);
	}

});

module.exports = InsureInputDropdown;

},{"../../common/components/SelectDropdown.jsx":197,"../../common/components/SelectList.jsx":198,"../../common/components/TextInput.jsx":200,"../../common/utils/click-outside-mixin":213,"../actions/MainActions":234,"react":177}],234:[function(require,module,exports){
'use strict';

var MainDispatcher = require('../../common/dispatcher/Dispatcher');
var MainConstants = require('../constants/mainConstants');

// Define action methods
var MainActions = {
	changeSelection: function changeSelection(stepNum, option) {
		MainDispatcher.handleAction({
			actionType: MainConstants.CHANGE_SELECTION,
			stepNum: stepNum,
			option: option
		});
	},

	toggleCheckbox: function toggleCheckbox(stepNum, target, option) {
		MainDispatcher.handleAction({
			actionType: MainConstants.TOGGLE_CHECKBOX,
			stepNum: stepNum,
			target: target,
			option: option
		});
	},

	changeWithTarget: function changeWithTarget(stepNum, target, option) {
		MainDispatcher.handleAction({
			actionType: MainConstants.CHANGE_WITH_TARGET,
			stepNum: stepNum,
			target: target,
			option: option
		});
	}

};

module.exports = MainActions;

},{"../../common/dispatcher/Dispatcher":206,"../constants/mainConstants":238}],238:[function(require,module,exports){
'use strict';

var keyMirror = require('keymirror');

// Define action constants
module.exports = keyMirror({
    CHANGE_SELECTION: null,
    TOGGLE_CHECKBOX: null,
    CHANGE_WITH_TARGET: null
});

},{"keymirror":9}],188:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

var React = require('react');

var CheckboxDropdown = React.createClass({
	displayName: 'CheckboxDropdown',

	mixins: [require('../utils/click-outside-mixin')],

	propTypes: {
		handleSelect: React.PropTypes.func.isRequired,
		placeholder: React.PropTypes.string,
		options: React.PropTypes.array,
		selected: React.PropTypes.array,
		disabled: React.PropTypes.bool,
		className: React.PropTypes.string,
		limitSelection: React.PropTypes.oneOfType([React.PropTypes.bool, React.PropTypes.number])
	},

	getDefaultProps: function getDefaultProps() {
		return {
			placeholder: '-',
			options: ['-'],
			selected: [],
			disabled: false,
			className: 'select-dropdown',
			limitSelection: false
		};
	},

	getInitialState: function getInitialState() {
		return {
			isOpen: false
		};
	},

	componentDidMount: function componentDidMount() {
		this.setOnClickOutside('main', this.handleOutsideClick);
		$(this.refs.options).bind('mousewheel DOMMouseScroll', this.handleScroll);
	},

	componentWillUnmount: function componentWillUnmount() {
		$(this.refs.options).unbind('mousewheel DOMMouseScroll', this.handleScroll);
	},

	render: function render() {
		var _this = this;

		var _props = this.props;
		var placeholder = _props.placeholder;
		var options = _props.options;
		var selected = _props.selected;
		var disabled = _props.disabled;
		var className = _props.className;
		var handleSelect = _props.handleSelect;


		var labelSelected = [];

		var selectOptions = options.map(function (el, idx) {
			var label = undefined,
			    optionCssClass = undefined;

			// case option is in the form of object -> use value as option code
			if (el !== null && (typeof el === 'undefined' ? 'undefined' : _typeof(el)) === 'object') {
				label = el.label || '-';

				if (el.value === 0 || el.value) {
					// regard number '0' as truthy
					if (selected && selected.indexOf(String(el.value)) > -1) {
						labelSelected.push({ label: String(label), val: el.value });
					}
					optionCssClass = _this.getOptionClasses(el.value);
				} else {
					if (selected && selected.indexOf(String(label)) > -1) {
						labelSelected.push({ label: String(label), val: String(label) });
					}
					optionCssClass = _this.getOptionClasses(label);
				}

				// case option is in the form of string -> label functions as option code
			} else if (typeof el === 'string' || el instanceof String) {
					label = el;
					optionCssClass = _this.getOptionClasses(label);

					if (selected && selected.indexOf(String(label)) > -1) {
						labelSelected.push({ label: String(label), val: String(label) });
					}
				} else {
					return undefined;
				}

			return React.createElement(
				'li',
				{ key: 'opt' + idx + el,
					onClick: _this.handleOptionClick,
					'data-val': el.value === 0 ? 0 : el.value || '',
					className: optionCssClass },
				React.createElement(
					'div',
					null,
					label
				)
			);
		});

		var labelSelectedText = labelSelected.map(function (el) {
			return el.label;
		}).join(', ');

		return React.createElement(
			'div',
			{ className: className,
				onClick: this.toggleOpen,
				ref: 'main' },
			React.createElement(
				'span',
				{ className: 'display', style: selected.length > 0 ? { fontSize: '13px' } : { color: '#999' } },
				labelSelectedText || placeholder
			),
			React.createElement(
				'div',
				{ ref: 'options', className: 'dropdown-options-ck' + (!disabled && this.state.isOpen ? ' open' : '') },
				React.createElement(
					'ul',
					null,
					selectOptions
				)
			)
		);
	},

	handleOutsideClick: function handleOutsideClick(evt) {
		this.setState({
			isOpen: false
		});
	},

	handleOptionClick: function handleOptionClick(e) {
		e = e ? e : window.event;
		var target = e.target || e.srcElement;

		e.stopPropagation();

		var _props2 = this.props;
		var limitSelection = _props2.limitSelection;
		var handleSelect = _props2.handleSelect;
		var selected = _props2.selected;


		if (limitSelection) {
			var targetVal = $(target).closest('li').attr('data-val');
			targetVal = targetVal === 0 ? 0 : targetVal || target.textContent;

			if (selected) {
				if (selected.indexOf(targetVal) > -1 || selected.length < limitSelection) {
					handleSelect(e);
				}
			} else {
				handleSelect(e);
			}
		} else {
			handleSelect(e);
		}
	},

	handleScroll: function handleScroll(evt) {
		evt = evt ? evt : window.event;
		evt.stopPropagation();
	},

	toggleOpen: function toggleOpen(e) {
		if (!this.props.disabled) {
			this.setState({
				isOpen: !this.state.isOpen
			});
		}
	},

	getOptionClasses: function getOptionClasses(option) {
		var classes = '';

		if (this.props.selected.indexOf(String(option)) > -1) {
			classes += ' selected';
		}

		return classes.trim();
	}

});

module.exports = CheckboxDropdown;

},{"../utils/click-outside-mixin":213,"react":177}]},{},[240])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvbWFpbi1wYWdlL21haW4tcGFnZS5qc3giLCJzcmMvbWFpbi1wYWdlL2NvbXBvbmVudHMvU2VhcmNoU2VjdGlvbi5qc3giLCJzcmMvbWFpbi1wYWdlL3N0b3Jlcy9zZWFyY2hib3hTdG9yZS5qcyIsInNyYy9tYWluLXBhZ2UvY29uc3RhbnRzL3N0ZXBPcHRpb25zLmpzIiwic3JjL21haW4tcGFnZS9jb21wb25lbnRzL0lucHV0U3RlcHMuanN4Iiwic3JjL21haW4tcGFnZS9jb21wb25lbnRzL0luc3VyZUlucHV0RHJvcGRvd24uanN4Iiwic3JjL21haW4tcGFnZS9hY3Rpb25zL01haW5BY3Rpb25zLmpzIiwic3JjL21haW4tcGFnZS9jb25zdGFudHMvbWFpbkNvbnN0YW50cy5qcyIsInNyYy9jb21tb24vY29tcG9uZW50cy9DaGVja2JveERyb3Bkb3duLmpzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsQ0FBQyxZQUFXO0FBQ1gsS0FBSSxRQUFVLFFBQVEsT0FBUixDQUFWO0tBQ0gsV0FBWSxRQUFRLFdBQVIsQ0FBWjtLQUNBLGdCQUFnQixRQUFRLGlDQUFSLENBQWhCO0tBQ0EsZ0JBQWdCLFFBQVEsZ0NBQVIsQ0FBaEIsQ0FKVTs7QUFPWCxlQUFjLFlBQWQsQ0FBMkIsQ0FDMUIsVUFEMEIsRUFDZCxJQURjLEVBQ1IsSUFEUSxFQUNGLElBREUsRUFDSSxJQURKLEVBQ1UsT0FEVixFQUNtQixPQURuQixFQUM0QixJQUQ1QixDQUEzQixFQVBXOztBQVdYLFVBQVMsTUFBVCxDQUNDLG9CQUFDLGFBQUQsT0FERCxFQUVDLFNBQVMsY0FBVCxDQUF3QixRQUF4QixDQUZELEVBWFc7Q0FBWCxDQUFEOzs7Ozs7Ozs7QUNBQSxJQUFJLFFBQVUsUUFBUSxPQUFSLENBQVY7SUFDSCxNQUFTLFFBQVEsd0JBQVIsQ0FBVDtJQUNHLGlCQUFpQixRQUFRLDBCQUFSLENBQWpCO0lBQ0gsY0FBZSxRQUFRLDBCQUFSLENBQWY7SUFDRyxhQUFjLFFBQVEsa0JBQVIsQ0FBZDs7QUFHSixJQUFJLGtCQUFrQixDQUNyQixFQUFFLFNBQVMsWUFBVCxFQUF1QixTQUFTLEtBQVQsRUFBZ0IsYUFBYSxJQUFiLEVBRHBCLEVBRXJCLEVBQUUsU0FBUyxXQUFULEVBQXNCLFNBQVMsS0FBVCxFQUFnQixhQUFhLElBQWIsRUFGbkIsQ0FBbEI7O0lBS0UsYUFDTCxTQURLLFVBQ0wsQ0FBWSxLQUFaLEVBQW1CLEtBQW5CLEVBQTBCLElBQTFCLEVBQWdDO3VCQUQzQixZQUMyQjs7QUFDL0IsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUQrQjtBQUUvQixNQUFLLEtBQUwsR0FBYSxLQUFiLENBRitCO0FBRy9CLEtBQUcsSUFBSCxFQUFTO0FBQ1IsT0FBSyxJQUFMLEdBQVksSUFBWixDQURRO0VBQVQ7Q0FIRDs7QUFVRCxTQUFTLGNBQVQsR0FBMEI7QUFDdEIsUUFBTyxlQUFlLFFBQWYsRUFBUCxDQURzQjtDQUExQjs7QUFJQSxJQUFJLGdCQUFnQixNQUFNLFdBQU4sQ0FBa0I7OztBQUNyQyxTQUFRLENBQ1AsUUFBUSxvQ0FBUixDQURPLENBQVI7O0FBSUcsWUFBVyxFQUFYOztBQUlBLGtCQUFpQiwyQkFBVztBQUM5QixTQUFPO0FBQ04sY0FBVyxnQkFBWDtBQUNBLG1DQUZNO0dBQVAsQ0FEOEI7RUFBWDs7QUFPakIsb0JBQW1CLDZCQUFXO0FBQ2hDLE1BQUksSUFBSixDQUFTLDBDQUFULEVBQXFELEVBQXJELEVBQ0UsSUFERixDQUNPLFVBQVUsR0FBVixFQUFlO0FBQ3BCLE9BQUcsSUFBSSxNQUFKLENBQVcsVUFBWCxLQUEwQixTQUExQixFQUFxQztBQUN2QyxTQUFLLFFBQUwsQ0FBYztBQUNiLHNCQUFpQixJQUFJLE1BQUosQ0FBVyxJQUFYO0tBRGxCLEVBRHVDO0lBQXhDO0dBREssQ0FNSixJQU5JLENBTUMsSUFORCxDQURQLEVBRGdDOztBQVVoQyxpQkFBZSxnQkFBZixDQUFnQyxLQUFLLFNBQUwsQ0FBaEMsQ0FWZ0M7RUFBWDs7QUFhbkIsdUJBQXNCLGdDQUFXO0FBQ25DLGlCQUFlLG1CQUFmLENBQW1DLEtBQUssU0FBTCxDQUFuQyxDQURtQztFQUFYOztBQUl0QixTQUFRLGtCQUFXO01BQ2hCLGtCQUFtQixLQUFLLEtBQUwsQ0FBbkIsZ0JBRGdCOztBQUVyQixvQkFBa0IsZ0JBQWdCLEdBQWhCLENBQW9CLFVBQUMsSUFBRDtVQUFVLElBQUksVUFBSixDQUFlLEtBQUssV0FBTCxFQUFrQixLQUFLLE9BQUwsRUFBYyxJQUEvQztHQUFWLENBQXRDLENBRnFCOztBQUlyQixTQUNDOzs7R0FDQzs7OztJQUFPOzs7O0tBQVA7O0lBREQ7R0FFQyxvQkFBQyxVQUFEO0FBQ0MsZUFBVyxLQUFLLEtBQUwsQ0FBVyxTQUFYO0FBQ1gsaUJBQWEsU0FBYyxFQUFkLEVBQWtCLFdBQWxCLEVBQStCLEVBQUUsZ0NBQUYsRUFBL0IsQ0FBYjtBQUNBLGdCQUFZLEtBQUssS0FBTCxDQUFXLFVBQVgsQ0FBc0IsVUFBdEI7SUFIYixDQUZEO0dBREQsQ0FKcUI7RUFBWDs7QUFnQlIsWUFBVyxxQkFBVztBQUN4QixPQUFLLFFBQUwsQ0FBYztBQUNiLGNBQVcsZ0JBQVg7R0FERCxFQUR3QjtFQUFYO0NBakRLLENBQWhCOztBQXdESixPQUFPLE9BQVAsR0FBaUIsYUFBakI7OztBQ25GQTs7OztBQUVBLElBQUksZUFBZSxRQUFRLFFBQVIsRUFBa0IsWUFBbEI7QUFDbkIsSUFBSSxpQkFBaUIsUUFBUSxvQ0FBUixDQUFqQjtBQUNKLElBQUksZ0JBQWdCLFFBQVEsNEJBQVIsQ0FBaEI7QUFDSixJQUFJLGVBQWUsUUFBZjs7O0FBR0osSUFBSSxrQkFBa0I7QUFDbEIsZUFBYSxDQUFiO0FBQ0Esa0JBQWdCLENBQUMsRUFBRCxFQUFLLEVBQUwsRUFBUyxFQUFULENBQWhCO0NBRkE7O0FBT0osU0FBUyxlQUFULENBQXlCLE9BQXpCLEVBQWtDLE1BQWxDLEVBQTBDLFVBQTFDLEVBQXNEO01BQzVDLGNBQWdDLGdCQUFoQyxZQUQ0QztNQUMvQixpQkFBbUIsZ0JBQW5CLGVBRCtCOzs7QUFHbEQsTUFBSSxVQUFVLFdBQVYsRUFBdUI7O0FBRTdCLFNBQUssSUFBSSxJQUFJLE9BQUosRUFBYSxJQUFJLFdBQUosRUFBaUIsR0FBdkMsRUFBNEM7QUFDM0MscUJBQWUsQ0FBZixJQUFvQixFQUFwQixDQUQyQztLQUE1QztHQUZFOzs7QUFIa0QsTUFXOUMsWUFBWSxDQUFaLEVBQWU7QUFDckIsbUJBQWUsQ0FBZixJQUFvQixFQUFwQixDQURxQjtHQUFuQjs7QUFLQSxNQUFHLFVBQUgsRUFBZTtBQUNqQixRQUFJLFVBQVUsZUFBZSxVQUFVLENBQVYsQ0FBekIsQ0FEYTtBQUVqQixRQUFJLFFBQVEsVUFBUixNQUF3QixNQUF4QixJQUFrQyxDQUFDLE1BQUQsRUFBUztBQUM5QyxjQUFRLFVBQVIsSUFBc0IsRUFBdEIsQ0FEOEM7QUFFOUMsc0JBQWdCLFdBQWhCLEdBQThCLFVBQVUsQ0FBVixDQUZnQjtLQUEvQyxNQUdPO0FBQ04sY0FBUSxVQUFSLElBQXNCLE1BQXRCLENBRE07QUFFTixzQkFBZ0IsV0FBaEIsR0FBOEIsT0FBOUIsQ0FGTTtLQUhQO0dBRkUsTUFTTztBQUNULFFBQUksZUFBZSxVQUFVLENBQVYsQ0FBZixLQUFnQyxNQUFoQyxFQUF3QztBQUMzQyxxQkFBZSxVQUFVLENBQVYsQ0FBZixHQUE4QixFQUE5QixDQUQyQztBQUUzQyxzQkFBZ0IsV0FBaEIsR0FBOEIsVUFBVSxDQUFWLENBRmE7S0FBNUMsTUFHTztBQUNOLHFCQUFlLFVBQVUsQ0FBVixDQUFmLEdBQThCLE1BQTlCLENBRE07QUFFTixzQkFBZ0IsV0FBaEIsR0FBOEIsT0FBOUIsQ0FGTTtLQUhQO0dBVkU7Q0FoQko7O0FBcUNBLFNBQVMsY0FBVCxDQUF5QixPQUF6QixFQUFrQyxNQUFsQyxFQUEwQyxVQUExQyxFQUFzRDtNQUM1QyxpQkFBbUIsZ0JBQW5CLGVBRDRDOztBQUVsRCxNQUFJLGNBQWMsZUFBZSxVQUFVLENBQVYsQ0FBZixDQUE0QixVQUE1QixDQUFkLENBRjhDOztBQUlsRCxNQUFJLEVBQUUsdUJBQXVCLEtBQXZCLENBQUYsRUFBa0M7QUFDeEMsbUJBQWUsVUFBVSxDQUFWLENBQWYsQ0FBNEIsVUFBNUIsSUFBMEMsRUFBMUMsQ0FEd0M7QUFFeEMsa0JBQWMsZUFBZSxVQUFVLENBQVYsQ0FBZixDQUE0QixVQUE1QixDQUFkLENBRndDO0dBQXRDOzs7QUFKa0QsTUFVOUMsTUFBTSxZQUFZLE9BQVosQ0FBb0IsTUFBcEIsQ0FBTixDQVY4Qzs7QUFZbEQsTUFBSSxNQUFNLENBQUMsQ0FBRCxFQUFJO0FBQ2hCLGdCQUFZLE1BQVosQ0FBbUIsR0FBbkIsRUFBd0IsQ0FBeEIsRUFEZ0I7R0FBZCxNQUVPO0FBQ1QsZ0JBQVksSUFBWixDQUFpQixNQUFqQixFQURTO0dBRlA7OztBQVprRCxNQW1COUMsWUFBWSxNQUFaLEdBQXFCLENBQXJCLEVBQXdCO0FBQzlCLG9CQUFnQixXQUFoQixHQUE4QixPQUE5QixDQUQ4QjtHQUE1QixNQUVPO0FBQ1Qsb0JBQWdCLFdBQWhCLEdBQThCLFVBQVUsQ0FBVixDQURyQjtHQUZQO0NBbkJKOztBQThCQSxJQUFJLGlCQUFpQixTQUFjLEVBQWQsRUFBa0IsYUFBYSxTQUFiLEVBQXdCO0FBQzNELG9CQUFrQixTQUFTLGdCQUFULENBQTBCLFFBQTFCLEVBQW9DO0FBQ3hELFNBQUssRUFBTCxDQUFRLFlBQVIsRUFBc0IsUUFBdEIsRUFEd0Q7R0FBcEM7O0FBSWxCLHVCQUFxQixTQUFTLG1CQUFULENBQTZCLFFBQTdCLEVBQXVDO0FBQzlELFNBQUssY0FBTCxDQUFvQixZQUFwQixFQUFrQyxRQUFsQyxFQUQ4RDtHQUF2Qzs7QUFJckIsY0FBWSxTQUFTLFVBQVQsR0FBc0I7QUFDcEMsU0FBSyxJQUFMLENBQVUsWUFBVixFQURvQztHQUF0Qjs7QUFJWixZQUFVLFNBQVMsUUFBVCxHQUFvQjtBQUNoQyxXQUFPLGVBQVAsQ0FEZ0M7R0FBcEI7Q0FiTyxDQUFqQjs7O0FBb0JKLGVBQWUsUUFBZixDQUF3QixVQUFVLE9BQVYsRUFBbUI7QUFDdkMsTUFBSSxTQUFTLFFBQVEsTUFBUixDQUQwQjs7QUFHdkMsVUFBUSxPQUFPLFVBQVA7OztBQUdSLFNBQUssY0FBYyxnQkFBZDtBQUNQLHNCQUFnQixPQUFPLE9BQVAsRUFBZ0IsT0FBTyxNQUFQLENBQWhDLENBREU7QUFFRixZQUZFOzs7QUFIQSxTQVFLLGNBQWMsZUFBZDtBQUNQLHFCQUFlLE9BQU8sT0FBUCxFQUFnQixPQUFPLE1BQVAsRUFBZSxPQUFPLE1BQVAsQ0FBOUMsQ0FERTtBQUVGLFlBRkU7OztBQVJBLFNBYUssY0FBYyxrQkFBZDtBQUNQLHNCQUFnQixPQUFPLE9BQVAsRUFBZ0IsT0FBTyxNQUFQLEVBQWUsT0FBTyxNQUFQLENBQS9DLENBREU7QUFFRixZQUZFOztBQWJBO0FBa0JGLGFBQU8sS0FBUCxDQURFO0FBakJBLEdBSHVDOztBQXdCdkMsaUJBQWUsVUFBZixHQXhCdUM7O0FBMEJ2QyxTQUFPLElBQVAsQ0ExQnVDO0NBQW5CLENBQXhCOztBQTZCQSxPQUFPLE9BQVAsR0FBaUIsY0FBakI7Ozs7QUNsSUE7O0FBRUEsSUFBTSxpQkFBaUIsQ0FDdEIsRUFBRSxPQUFPLElBQVAsRUFBYSxPQUFPLFFBQVAsRUFBaUIsTUFBTSxTQUFOLEVBRFYsRUFFdEIsRUFBRSxPQUFPLElBQVAsRUFBYSxPQUFPLE1BQVAsRUFBZSxNQUFNLFNBQU4sRUFGUixFQUd0QixFQUFFLE9BQU8sSUFBUCxFQUFhLE9BQU8sS0FBUCxFQUFjLE1BQU0sU0FBTixFQUhQLEVBSXRCLEVBQUUsT0FBTyxJQUFQLEVBQWEsT0FBTyxNQUFQLEVBQWUsTUFBTSxTQUFOLEVBSlIsRUFLdEIsRUFBRSxPQUFPLEtBQVAsRUFBYyxPQUFPLEtBQVAsRUFBYyxNQUFNLFNBQU4sRUFMUixFQU10QixFQUFFLE9BQU8sSUFBUCxFQUFhLE9BQU8sVUFBUCxFQUFtQixNQUFNLFNBQU4sRUFOWixDQUFqQjs7QUFTTixJQUFNLGdCQUFnQixDQUNyQixFQUFFLE9BQU8sSUFBUCxFQUFhLE9BQU8sU0FBUCxFQUFrQixNQUFNLFNBQU4sRUFEWixFQUVyQixFQUFFLE9BQU8sSUFBUCxFQUFhLE9BQU8sUUFBUCxFQUFpQixNQUFNLFNBQU4sRUFGWCxFQUdyQixFQUFFLE9BQU8sTUFBUCxFQUFlLE9BQU8sTUFBUCxFQUFlLE1BQU0sU0FBTixFQUhYLEVBSXJCLEVBQUUsT0FBTyxNQUFQLEVBQWUsT0FBTyxPQUFQLEVBQWdCLE1BQU0sU0FBTixFQUpaLENBQWhCOztBQU9OLElBQU0sY0FBYyxDQUNuQixFQUFFLE9BQU8sTUFBUCxFQUFlLE9BQU8sV0FBUCxFQUFvQixNQUFNLFNBQU4sRUFEbEIsRUFFbkIsRUFBRSxPQUFPLE1BQVAsRUFBZSxPQUFPLFNBQVAsRUFBa0IsTUFBTSxTQUFOLEVBRmhCLENBQWQ7O0FBS04sSUFBTSxhQUFhLENBQ2xCLEVBQUUsT0FBTyxJQUFQLEVBQWEsT0FBTyxTQUFQLEVBQWtCLE1BQU0sU0FBTixFQURmLEVBRWxCLEVBQUUsT0FBTyxJQUFQLEVBQWEsT0FBTyxNQUFQLEVBQWUsTUFBTSxTQUFOLEVBRlosRUFHbEIsRUFBRSxPQUFPLE9BQVAsRUFBZ0IsT0FBTyxTQUFQLEVBQWtCLE1BQU0sU0FBTixFQUhsQixFQUlsQixFQUFFLE9BQU8sS0FBUCxFQUFjLE9BQU8sS0FBUCxFQUFjLE1BQU0sU0FBTixFQUpaLENBQWI7O0FBT04sSUFBTSxjQUFjLENBQ25CLEVBQUUsT0FBTyxNQUFQLEVBQWUsT0FBTyxRQUFQLEVBQWlCLE1BQU0sU0FBTixFQURmLEVBRW5CLEVBQUUsT0FBTyxNQUFQLEVBQWUsT0FBTyxPQUFQLEVBQWdCLE1BQU0sU0FBTixFQUZkLENBQWQ7O0FBS04sSUFBTSxhQUFhLENBQ2xCLEVBQUUsT0FBTyxNQUFQLEVBQWUsT0FBTyxRQUFQLEVBQWlCLE1BQU0sU0FBTixFQURoQixFQUVsQixFQUFFLE9BQU8sTUFBUCxFQUFlLE9BQU8sT0FBUCxFQUFnQixNQUFNLFNBQU4sRUFGZixDQUFiOztBQU1OLE9BQU8sT0FBUCxHQUFpQjtBQUNoQiwrQkFEZ0I7QUFFaEIsNkJBRmdCO0FBR2hCLHlCQUhnQjtBQUloQix1QkFKZ0I7QUFLaEIseUJBTGdCO0FBTWhCLHVCQU5nQjtDQUFqQjs7Ozs7Ozs7O0FDMUNBLElBQUksUUFBVyxRQUFRLE9BQVIsQ0FBWDtJQUNBLGlCQUFrQixRQUFRLDRDQUFSLENBQWxCO0lBQ0EsbUJBQW1CLFFBQVEsOENBQVIsQ0FBbkI7SUFDQSxZQUFjLFFBQVEsdUNBQVIsQ0FBZDtJQUNBLHNCQUFzQixRQUFRLDJCQUFSLENBQXRCO0lBQ0EsY0FBZ0IsUUFBUSx3QkFBUixDQUFoQjs7SUFHRSxZQUNMLFNBREssU0FDTCxDQUFZLEtBQVosRUFBbUIsS0FBbkIsRUFBMEIsSUFBMUIsRUFBZ0M7dUJBRDNCLFdBQzJCOztBQUMvQixNQUFLLEtBQUwsR0FBYSxLQUFiLENBRCtCO0FBRS9CLE1BQUssS0FBTCxHQUFhLEtBQWIsQ0FGK0I7QUFHL0IsS0FBRyxJQUFILEVBQVM7QUFDUixPQUFLLElBQUwsR0FBWSxJQUFaLENBRFE7RUFBVDtDQUhEOztBQVVELElBQUksYUFBYSxNQUFNLFdBQU4sQ0FBa0I7OztBQUMvQixZQUFXO0FBQ2IsYUFBVyxNQUFNLFNBQU4sQ0FBZ0IsTUFBaEIsQ0FBdUIsVUFBdkI7QUFDWCxlQUFhLE1BQU0sU0FBTixDQUFnQixNQUFoQixDQUF1QixVQUF2QjtBQUNiLGNBQVksTUFBTSxTQUFOLENBQWdCLE1BQWhCLENBQXVCLFVBQXZCO0VBSFY7O0FBTUEsa0JBQWlCLDJCQUFXLEVBQVg7O0FBSWpCLG9CQUFtQiw2QkFBVyxFQUFYOztBQUluQix1QkFBc0IsZ0NBQVcsRUFBWDs7QUFJdEIsU0FBUSxrQkFBVztlQUN3QixLQUFLLEtBQUwsQ0FEeEI7TUFDZiw2QkFEZTtNQUNKLGlDQURJO01BQ1MsK0JBRFQ7O0FBRXJCLE1BQUksOEJBQUo7TUFBd0IseUJBQXhCO01BQXVDLGdDQUF2QztNQUE2RCw0QkFBN0Q7TUFBK0UseUJBQS9FLENBRnFCOztNQUlmLGlCQUFtQixVQUFuQixlQUplO01BS2YsaUJBQW1CLFlBQW5CLGVBTGU7O0FBTXJCLE1BQUksaUJBQWlCLFlBQWUsZUFBZSxDQUFmLEVBQWtCLFdBQWxCLGNBQWYsQ0FBakIsQ0FOaUI7O0FBU3JCLGtCQUNDLG9CQUFDLGNBQUQ7QUFDQyxnQkFBWSxHQUFaO0FBQ0EsWUFBUyxDQUFDLEdBQUQsQ0FBVDtBQUNBLGFBQVUsS0FBSyxjQUFMLENBQW9CLENBQXBCLENBQVY7QUFDQSxpQkFBYyxLQUFLLHVCQUFMLENBQTZCLElBQTdCLENBQWtDLElBQWxDLEVBQXdDLENBQXhDLEVBQTJDLEtBQTNDLENBQWQ7QUFDQSxhQUFVLEVBQVY7QUFDQSxjQUFVLHNCQUFWLEVBTkQsQ0FERCxDQVRxQjs7QUFtQnJCLFVBQU8sZUFBZSxDQUFmLEVBQWtCLFdBQWxCLEVBQVA7QUFDQyxRQUFLLFFBQUw7QUFDQyx5QkFBcUIsZ0JBQXJCLENBREQ7O0FBR0MsUUFBRyxlQUFlLENBQWYsQ0FBSCxFQUFzQjtBQUNyQixxQkFBZ0IsSUFBaEIsQ0FEcUI7O0FBR3JCLGFBQU8sZUFBZSxDQUFmLEVBQWtCLFdBQWxCLEVBQVA7QUFDQyxXQUFLLFNBQUw7QUFDQyw4QkFBdUIsZUFBdkIsQ0FERDtBQUVDLGFBRkQ7QUFERCxXQUlNLFFBQUw7QUFDQyw4QkFBdUIsZUFBdkIsQ0FERDtBQUVDLGFBRkQ7QUFKRCxXQU9NLE1BQUw7QUFDQyw4QkFBdUIsZUFBdkIsQ0FERDtBQUVDLGFBRkQ7QUFQRCxXQVVNLE9BQUw7QUFDQyw4QkFBdUIsZUFBdkIsQ0FERDtBQUVDLGFBRkQ7QUFWRDtBQWNFLDhCQUF1QixHQUF2QixDQUREO0FBYkQsTUFIcUI7O0FBb0JyQixxQkFDQyxvQkFBQyxTQUFEO0FBQ0MsbUJBQWEsb0JBQWI7QUFDQSxZQUFLLG9CQUFMO0FBQ0Esb0JBQWMsS0FBSyxxQkFBTCxDQUEyQixJQUEzQixDQUFnQyxJQUFoQyxFQUFzQyxDQUF0QyxFQUF5QyxRQUF6QyxDQUFkO0FBQ0EsYUFBTyxlQUFlLENBQWYsRUFBa0IsTUFBbEIsSUFBNEIsRUFBNUI7QUFDUCxZQUFNLGFBQU4sRUFMRCxDQURELENBcEJxQjtLQUF0Qjs7QUE4QkEsVUFqQ0Q7O0FBREQsUUFvQ00sTUFBTDtBQUNDLHlCQUFxQixnQkFBckIsQ0FERDs7QUFHQyxRQUFHLGVBQWUsQ0FBZixDQUFILEVBQXNCO0FBQ3JCLGFBQU8sZUFBZSxDQUFmLEVBQWtCLFdBQWxCLEVBQVA7QUFDQyxXQUFLLFdBQUw7QUFDQyw4QkFBdUIsa0JBQXZCLENBREQ7QUFFQyx1QkFBZ0IsSUFBaEIsQ0FGRDtBQUdDLHVCQUNDLG9CQUFDLFNBQUQ7QUFDQyxxQkFBYSxvQkFBYjtBQUNBLGNBQUssb0JBQUw7QUFDQSxzQkFBYyxLQUFLLHFCQUFMLENBQTJCLElBQTNCLENBQWdDLElBQWhDLEVBQXNDLENBQXRDLEVBQXlDLFFBQXpDLENBQWQ7QUFDQSxlQUFPLGVBQWUsQ0FBZixFQUFrQixNQUFsQixJQUE0QixFQUE1QjtBQUNQLGNBQU0sYUFBTixFQUxELENBREQsQ0FIRDtBQVdDLGFBWEQ7QUFERCxXQWFNLFNBQUw7QUFDQywwQkFBbUIsQ0FDbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxJQUFQLEVBREosRUFFbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxLQUFQLEVBRkosRUFHbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxLQUFQLEVBSEosRUFJbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxJQUFQLEVBSkosRUFLbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxJQUFQLEVBTEosRUFNbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxLQUFQLEVBTkosRUFPbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxLQUFQLEVBUEosRUFRbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxJQUFQLEVBUkosRUFTbEIsRUFBRSxPQUFPLFdBQVAsRUFBb0IsT0FBTyxJQUFQLEVBVEosQ0FBbkIsQ0FERDtBQVlDLDhCQUF1QixlQUF2QixDQVpEO0FBYUMsdUJBQ0Msb0JBQUMsY0FBRDtBQUNDLHFCQUFhLG9CQUFiO0FBQ0EsaUJBQVMsZ0JBQVQ7QUFDQSxrQkFBVSxLQUFLLGNBQUwsQ0FBb0IsQ0FBcEIsQ0FBVjtBQUNBLHNCQUFjLEtBQUssdUJBQUwsQ0FBNkIsSUFBN0IsQ0FBa0MsSUFBbEMsRUFBd0MsQ0FBeEMsRUFBMkMsWUFBM0MsRUFBeUQsS0FBekQsQ0FBZDtBQUNBLGtCQUFVLGVBQWUsQ0FBZixFQUFrQixVQUFsQixJQUFnQyxFQUFoQztBQUNWLG1CQUFVLHNCQUFWLEVBTkQsQ0FERCxDQWJEO0FBc0JDLGFBdEJEO0FBYkQ7QUFxQ0UsMEJBQW1CLEVBQW5CLENBREQ7QUFFQyw4QkFBdUIsR0FBdkIsQ0FGRDtBQUdDLHVCQUNDLG9CQUFDLGNBQUQ7QUFDQyxxQkFBYSxvQkFBYjtBQUNBLGlCQUFTLEVBQVQ7QUFDQSxrQkFBVSxLQUFLLGNBQUwsQ0FBb0IsQ0FBcEIsQ0FBVjtBQUNBLGtCQUFVLEVBQVY7QUFDQSxtQkFBVSxzQkFBVixFQUxELENBREQsQ0FIRDtBQXBDRCxNQURxQjtLQUF0Qjs7QUFzREEsVUF6REQ7O0FBcENELFFBK0ZNLEtBQUw7QUFDQyx5QkFBcUIsbUJBQXJCLENBREQ7O0FBR0MsUUFBRyxlQUFlLENBQWYsQ0FBSCxFQUFzQjtBQUNyQixTQUFJLG1CQUFKLENBRHFCOztBQUdyQixhQUFPLGVBQWUsQ0FBZixFQUFrQixXQUFsQixFQUFQO0FBQ0MsV0FBSyxTQUFMO0FBQ0MsaUJBQVUsQ0FDVDtBQUNDLGVBQU8sTUFBUDtBQUNBLGNBQU0sUUFBTjtBQUNBLHFCQUFhLFFBQWI7QUFDQSxtQkFBVyxHQUFYO0FBQ0EsWUFBSSxtQkFBSjtBQUxELFFBRFMsRUFRVDtBQUNDLGVBQU8sTUFBUDtBQUNBLGNBQU0sUUFBTjtBQUNBLFlBQUksUUFBSjtBQUNBLGlCQUFTLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsSUFBbEMsQ0FBVDtRQVpRLENBQVYsQ0FERDs7QUFpQkMsYUFqQkQ7O0FBREQsV0FvQk0sTUFBTDtBQUNDLGlCQUFVLENBQ1Q7QUFDQyxlQUFPLE1BQVA7QUFDQSxjQUFNLFVBQU47QUFDQSxZQUFJLEtBQUo7QUFDQSxpQkFBUyxJQUFUO1FBTFEsQ0FBVixDQUREOztBQVVDLGFBVkQ7O0FBcEJELFdBZ0NNLFNBQUw7QUFDQyxpQkFBVSxDQUNUO0FBQ0MsZUFBTyxNQUFQO0FBQ0EsY0FBTSxRQUFOO0FBQ0EsWUFBSSxLQUFKO0FBQ0EsaUJBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFUO1FBTFEsQ0FBVixDQUREOztBQVVDLGFBVkQ7O0FBaENELFdBNENNLEtBQUw7QUFDQyxpQkFBVSxDQUNUO0FBQ0MsZUFBTyxNQUFQO0FBQ0EsY0FBTSxRQUFOO0FBQ0EsWUFBSSxLQUFKO0FBQ0EsaUJBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFUO0FBQ0Esc0JBQWMsQ0FBQyxjQUFELENBQWQ7UUFOUSxDQUFWLENBREQ7QUFVQyxhQVZEO0FBNUNELE1BSHFCO0FBMkRyQiw0QkFBdUIsaUJBQXZCLENBM0RxQjtBQTREckIscUJBQ0Msb0JBQUMsbUJBQUQ7QUFDQyxtQkFBYyxvQkFBZDtBQUNBLGVBQVMsT0FBVDtBQUNBLGdCQUFXLEtBQUssY0FBTCxDQUFvQixDQUFwQixDQUFYO0FBQ0Esb0JBQWMsS0FBSyx1QkFBTDtBQUNkLHdCQUFrQixLQUFLLHFCQUFMO0FBQ2xCLHNCQUFnQixjQUFoQjtNQU5ELENBREQsQ0E1RHFCO0tBQXRCOztBQXlFQSxVQTVFRDs7QUEvRkQsUUE2S00sTUFBTDtBQUNDLHlCQUFxQixjQUFyQixDQUREOztBQUdDLFFBQUcsZUFBZSxDQUFmLENBQUgsRUFBc0I7QUFDckIsNEJBQXVCLG9CQUF2QixDQURxQjtBQUVyQix3QkFBbUIsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFuQixDQUZxQjs7QUFJckIscUJBQ0Msb0JBQUMsZ0JBQUQ7QUFDQyxtQkFBYSxvQkFBYjtBQUNBLGVBQVMsZ0JBQVQ7QUFDQSxnQkFBVSxLQUFLLGNBQUwsQ0FBb0IsQ0FBcEIsQ0FBVjtBQUNBLG9CQUFjLEtBQUssdUJBQUwsQ0FBNkIsSUFBN0IsQ0FBa0MsSUFBbEMsRUFBd0MsQ0FBeEMsRUFBMkMsaUJBQTNDLEVBQThELElBQTlELENBQWQ7QUFDQSxzQkFBZ0IsQ0FBaEI7QUFDQSxnQkFBVSxlQUFlLENBQWYsRUFBa0IsZUFBbEI7QUFDVixpQkFBVSxtQ0FBVixFQVBELENBREQsQ0FKcUI7S0FBdEI7O0FBZ0JBLFVBbkJEO0FBN0tELFFBaU1NLEtBQUw7QUFDQyx5QkFBcUIsbUJBQXJCLENBREQ7O0FBR0MsUUFBRyxlQUFlLENBQWYsQ0FBSCxFQUFzQjtBQUNyQixTQUFJLFNBQVMsTUFBVCxDQURpQjs7QUFHckIsYUFBTyxlQUFlLENBQWYsQ0FBUDtBQUNDLFdBQUssUUFBTDtBQUNDLDBCQUFtQixLQUFLLGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDLE9BQWxDLENBQW5CLENBREQ7QUFFQyw4QkFBdUIsZ0JBQXZCLENBRkQ7QUFHQyxnQkFBUyxjQUFULENBSEQ7QUFJQyxhQUpEO0FBREQsV0FNTSxPQUFMO0FBQ0MsMEJBQW1CLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsT0FBbEMsQ0FBbkIsQ0FERDtBQUVDLDhCQUF1QixlQUF2QixDQUZEO0FBR0MsZ0JBQVMsT0FBVCxDQUhEO0FBSUMsYUFKRDtBQU5EO0FBWUUsMEJBQW1CLEVBQW5CLENBREQ7QUFFQyw4QkFBdUIsR0FBdkIsQ0FGRDtBQVhELE1BSHFCOztBQW1CckIscUJBQ0Msb0JBQUMsY0FBRDtBQUNDLG1CQUFhLG9CQUFiO0FBQ0EsZUFBUyxnQkFBVDtBQUNBLGdCQUFVLEtBQUssY0FBTCxDQUFvQixDQUFwQixDQUFWO0FBQ0Esb0JBQWMsS0FBSyx1QkFBTCxDQUE2QixJQUE3QixDQUFrQyxJQUFsQyxFQUF3QyxDQUF4QyxFQUEyQyxNQUEzQyxFQUFtRCxLQUFuRCxDQUFkO0FBQ0EsZ0JBQVUsZUFBZSxDQUFmLEVBQWtCLE1BQWxCLEtBQTZCLEVBQTdCO0FBQ1YsaUJBQVUsc0JBQVYsRUFORCxDQURELENBbkJxQjtLQUF0Qjs7QUErQkEsVUFsQ0Q7O0FBak1ELFFBcU9NLFVBQUw7QUFDQyx5QkFBcUIsYUFBckIsQ0FERDs7QUFHQyxRQUFHLGVBQWUsQ0FBZixDQUFILEVBQXNCO0FBQ3JCLDRCQUF1QixlQUF2QixDQURxQjtBQUVyQixTQUFJLFlBQVksZUFBZSxJQUFmLENBQW9CLFVBQUMsSUFBRDthQUFVLEtBQUssS0FBTCxLQUFlLGVBQWUsQ0FBZixDQUFmO01BQVYsQ0FBaEM7O0FBRmlCLGtCQUlyQixHQUFnQixRQUFoQixDQUpxQjs7QUFNckIscUJBQ0Msb0JBQUMsU0FBRDtBQUNDLG1CQUFhLG9CQUFiO0FBQ0EsWUFBSyxvQkFBTDtBQUNBLG9CQUFjLEtBQUsscUJBQUwsQ0FBMkIsSUFBM0IsQ0FBZ0MsSUFBaEMsRUFBc0MsQ0FBdEMsRUFBeUMsT0FBekMsQ0FBZDtBQUNBLGFBQU8sZUFBZSxDQUFmLEVBQWtCLEtBQWxCLElBQTJCLEVBQTNCO0FBQ1AsWUFBTSxhQUFOLEVBTEQsQ0FERCxDQU5xQjtLQUF0Qjs7QUFpQkEsVUFwQkQ7O0FBck9EO0FBNFBFLHFCQUFpQixFQUFqQixDQUREO0FBRUMseUJBQXFCLEdBQXJCLENBRkQ7O0FBSUMsb0JBQ0Msb0JBQUMsY0FBRDtBQUNDLGtCQUFZLEdBQVo7QUFDQSxjQUFTLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FBVDtBQUNBLGVBQVUsS0FBSyxjQUFMLENBQW9CLENBQXBCLENBQVY7QUFDQSxtQkFBYyxLQUFLLHVCQUFMLENBQTZCLElBQTdCLENBQWtDLElBQWxDLEVBQXdDLENBQXhDLEVBQTJDLEtBQTNDLENBQWQ7QUFDQSxlQUFVLEVBQVY7QUFDQSxnQkFBVSxzQkFBVixFQU5ELENBREQsQ0FKRDtBQTNQRCxHQW5CcUI7O0FBOFJyQixTQUNDOztLQUFLLFdBQVUsZUFBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxNQUFWLEVBQUw7SUFDQyxvQkFBQyxjQUFEO0FBQ0Msa0JBQVksZUFBWjtBQUNBLGNBQVMsY0FBVDtBQUNBLGVBQVUsS0FBSyxjQUFMLENBQW9CLENBQXBCLENBQVY7QUFDQSxtQkFBYyxLQUFLLHFCQUFMLENBQTJCLElBQTNCLENBQWdDLElBQWhDLEVBQXNDLENBQXRDLEVBQXlDLEtBQXpDLENBQWQ7QUFDQSxlQUFVLGVBQWUsQ0FBZixDQUFWO0FBQ0EsZ0JBQVUsc0JBQVY7S0FORCxDQUREO0lBREQ7R0FZQzs7TUFBSyxXQUFVLE1BQVYsRUFBTDtJQUNDLG9CQUFDLGNBQUQ7QUFDQyxrQkFBYSxrQkFBYjtBQUNBLGNBQVMsY0FBVDtBQUNBLGVBQVUsS0FBSyxjQUFMLENBQW9CLENBQXBCLENBQVY7QUFDQSxtQkFBYyxLQUFLLHFCQUFMLENBQTJCLElBQTNCLENBQWdDLElBQWhDLEVBQXNDLENBQXRDLEVBQXlDLEtBQXpDLENBQWQ7QUFDQSxlQUFVLGVBQWUsQ0FBZixDQUFWO0FBQ0EsZ0JBQVUsc0JBQVY7S0FORCxDQUREO0lBWkQ7R0F1QkM7O01BQUssV0FBVSxNQUFWLEVBQUw7SUFDRSxhQURGO0lBdkJEO0dBMkJDOztNQUFHLFdBQVUsWUFBVixFQUF1QixTQUFTLEtBQUssU0FBTCxFQUFuQzs7SUEzQkQ7R0FERCxDQTlScUI7RUFBWDs7QUFnVVIsaUJBQWdCLHdCQUFVLE1BQVYsRUFBa0I7QUFDcEMsU0FBTyxFQUFHLEtBQUssS0FBTCxDQUFXLFNBQVgsQ0FBcUIsV0FBckIsSUFBb0MsU0FBUyxDQUFULENBQXZDLENBRDZCO0VBQWxCOztBQUluQix3QkFBdUIsK0JBQVMsT0FBVCxFQUFrQixhQUFsQixFQUFpQyxHQUFqQyxFQUFzQztBQUM1RCxRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQUQyQztBQUU1RCxNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRmlDOztBQUk1RCxNQUFJLE1BQU0sRUFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixJQUFsQixFQUF3QixJQUF4QixDQUE2QixVQUE3QixDQUFOLENBSndEO0FBSzVELFFBQU0sR0FBQyxLQUFRLENBQVIsR0FBYSxDQUFkLEdBQW1CLE9BQU8sT0FBTyxXQUFQLENBTDRCOztBQU81RCxjQUFZLGVBQVosQ0FBNkIsT0FBN0IsRUFBc0MsR0FBdEMsRUFQNEQ7RUFBdEM7O0FBVXBCLHdCQUF1QiwrQkFBUyxPQUFULEVBQWtCLElBQWxCLEVBQXdCLEdBQXhCLEVBQTZCO0FBQ3RELFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRHFDO0FBRXRELE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGMkI7O0FBSXRELGNBQVksZ0JBQVosQ0FBOEIsT0FBOUIsRUFBdUMsSUFBdkMsRUFBNkMsT0FBTyxLQUFQLENBQTdDLENBSnNEO0VBQTdCOztBQU92QiwwQkFBeUIsaUNBQVMsT0FBVCxFQUFrQixJQUFsQixFQUF3QixhQUF4QixFQUF1QyxHQUF2QyxFQUE0QztBQUN2RSxRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURzRDtBQUV2RSxNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRjRDOztBQUl2RSxNQUFJLGVBQUosR0FKdUU7O0FBTXZFLE1BQUksTUFBTSxFQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLElBQWxCLEVBQXdCLElBQXhCLENBQTZCLFVBQTdCLENBQU4sQ0FObUU7QUFPdkUsUUFBTSxHQUFDLEtBQVEsQ0FBUixHQUFhLENBQWQsR0FBbUIsT0FBTyxPQUFPLFdBQVAsQ0FQdUM7O0FBVXZFLE1BQUcsYUFBSCxFQUFrQjtBQUNqQixlQUFZLGNBQVosQ0FBNEIsT0FBNUIsRUFBcUMsSUFBckMsRUFBMkMsR0FBM0MsRUFEaUI7R0FBbEIsTUFFTztBQUNOLGVBQVksZ0JBQVosQ0FBOEIsT0FBOUIsRUFBdUMsSUFBdkMsRUFBNkMsR0FBN0MsRUFETTtHQUZQO0VBVjJCOztBQWtCNUIsNkNBQWlCLFlBQVksTUFBTTtBQUNsQyxNQUFHLFdBQVcsSUFBWCxDQUFILEVBQXFCO0FBQ3BCLFVBQVMsV0FBVyxJQUFYLEVBQWlCLEdBQWpCLENBQXFCLFVBQUMsQ0FBRDtXQUFPLElBQUksU0FBSixDQUFjLEVBQUUsTUFBRixFQUFVLEVBQUUsRUFBRjtJQUEvQixDQUE5QixDQURvQjtHQUFyQjs7QUFEa0MsU0FLM0IsRUFBUCxDQUxrQztFQTFYRDtBQWtZbEMsaUNBQVk7TUFDTCxZQUFjLEtBQUssS0FBTCxDQUFkLFVBREs7TUFFTCxjQUFnQyxVQUFoQyxZQUZLO01BRVEsaUJBQW1CLFVBQW5CLGVBRlI7OztBQUlYLE1BQUcsZ0JBQWdCLENBQWhCLEVBQW1CO0FBQ3JCLE9BQUksY0FBYyxlQUFlLENBQWYsQ0FBZCxDQURpQjs7QUFHckIsT0FBRyxlQUFlLENBQWYsRUFBa0IsV0FBbEIsT0FBb0MsVUFBcEMsRUFBZ0Q7QUFDbEQsa0JBQWMsU0FBYyxFQUFkLEVBQWtCLFdBQWxCLEVBQStCLEVBQUUsV0FBVyxlQUFlLENBQWYsQ0FBWCxFQUE4QixlQUFlLGFBQWYsRUFBL0QsQ0FBZDs7QUFEa0QsVUFHbEQsQ0FBTyxJQUFQLHdCQUFpQyxFQUFFLEtBQUYsQ0FBUSxXQUFSLENBQWpDLEVBQXlELE9BQXpELEVBSGtEO0FBSWxELFdBQU8sSUFBUCxDQUprRDtJQUFuRDs7QUFPQSxXQUFPLGVBQWUsQ0FBZixFQUFrQixXQUFsQixFQUFQO0FBQ0MsU0FBSyxTQUFMO0FBQ0MsbUJBQWMsU0FBYyxFQUFkLEVBQWtCLFdBQWxCLEVBQStCLEVBQUUsVUFBVSxhQUFWLEVBQWpDLENBQWQ7QUFERDtBQURELFNBSU0sUUFBTDtBQUNDLG1CQUFjLFNBQWMsRUFBZCxFQUFrQixXQUFsQixFQUErQixFQUFFLFVBQVUsYUFBVixFQUFqQyxDQUFkO0FBREQ7QUFKRCxTQU9NLE9BQUw7QUFDQyxtQkFBYyxTQUFjLEVBQWQsRUFBa0IsV0FBbEIsRUFBK0IsRUFBRSxVQUFVLGFBQVYsRUFBakMsQ0FBZDtBQUREO0FBUEQsU0FVTSxRQUFMLENBVkQ7QUFXQyxTQUFLLE9BQUw7QUFDQyxpQkFBWSxRQUFaLEdBQXVCLFlBQVksZUFBWixDQUE0QixJQUE1QixDQUFpQyxHQUFqQyxDQUF2QixDQUREO0FBRUMsWUFBTyxZQUFZLGVBQVosQ0FGUjs7QUFJQyxtQkFBYyxTQUFjLEVBQWQsRUFBa0IsV0FBbEIsRUFBK0IsRUFBRSxTQUFTLEtBQVQsRUFBZ0IsS0FBSyxHQUFMLEVBQVUsV0FBVyxJQUFYLEVBQTNELENBQWQ7QUFKRDtBQVhELFNBaUJNLE9BQUw7QUFDQyxtQkFBYyxTQUFjLEVBQWQsRUFBa0IsV0FBbEIsRUFBK0IsRUFBRSxjQUFjLFVBQWQsRUFBakMsQ0FBZDtBQUREO0FBakJELElBVnFCOztBQWtDckIsVUFBTyxJQUFQLENBQWUsZUFBZSxDQUFmLFVBQXFCLGVBQWUsQ0FBZixVQUFxQixFQUFFLEtBQUYsQ0FBUSxXQUFSLENBQXpELEVBQWlGLE9BQWpGLEVBbENxQjtHQUF0QjtFQXRZaUM7Q0FBbEIsQ0FBYjs7QUErYUosT0FBTyxPQUFQLEdBQWlCLFVBQWpCOzs7OztBQ2xjQSxJQUFJLFFBQVEsUUFBUSxPQUFSLENBQVI7SUFDQSxZQUFZLFFBQVEsdUNBQVIsQ0FBWjtJQUNILGlCQUFpQixRQUFRLDRDQUFSLENBQWpCO0lBQ0csYUFBYSxRQUFRLHdDQUFSLENBQWI7SUFDQSxjQUFjLFFBQVEsd0JBQVIsQ0FBZDs7QUFHSixJQUFJLHNCQUFzQixNQUFNLFdBQU4sQ0FBa0I7OztBQUMzQyxTQUFRLENBQUMsUUFBUSx3Q0FBUixDQUFELENBQVI7O0FBRUcsWUFBVztBQUNiLGVBQWEsTUFBTSxTQUFOLENBQWdCLE1BQWhCO0FBQ2IsV0FBUyxNQUFNLFNBQU4sQ0FBZ0IsS0FBaEI7QUFDVCxZQUFVLE1BQU0sU0FBTixDQUFnQixJQUFoQjtBQUNWLGdCQUFjLE1BQU0sU0FBTixDQUFnQixJQUFoQjtBQUNkLG9CQUFrQixNQUFNLFNBQU4sQ0FBZ0IsSUFBaEI7QUFDbEIsa0JBQWdCLE1BQU0sU0FBTixDQUFnQixLQUFoQjtFQU5kOztBQVNBLGtCQUFpQiwyQkFBVztBQUM5QixTQUFPO0FBQ04sZ0JBQWEsR0FBYjtBQUNBLFlBQVMsRUFBVDtBQUNBLGFBQVUsS0FBVjtHQUhELENBRDhCO0VBQVg7O0FBUXBCLGtCQUFpQiwyQkFBVztBQUNyQixTQUFPO0FBQ1osV0FBUSxLQUFSO0dBREssQ0FEcUI7RUFBWDs7QUFPZCxvQkFBbUIsNkJBQVc7QUFDaEMsT0FBSyxpQkFBTCxDQUF1QixNQUF2QixFQUErQixLQUFLLGtCQUFMLENBQS9CLENBRGdDO0VBQVg7O0FBSW5CLHVCQUFzQixnQ0FBVyxFQUFYOztBQUl0QixTQUFRLGtCQUFXO0FBQ3JCLE1BQUksUUFBUSxLQUFLLEtBQUwsQ0FEUztlQU9qQixLQUFLLEtBQUwsQ0FQaUI7TUFHcEIseUJBSG9CO01BSXBCLG1DQUpvQjtNQUtwQiwyQ0FMb0I7TUFNcEIsdUNBTm9COzs7QUFTckIsTUFBSSxVQUFVLFFBQVEsR0FBUixDQUFhLFVBQUMsS0FBRCxFQUFXO0FBQ3JDLE9BQUkscUJBQUosQ0FEcUM7QUFFckMsV0FBTyxNQUFNLElBQU47QUFDTixTQUFLLFFBQUwsQ0FERDtBQUVDLFNBQUssb0JBQUwsQ0FGRDtBQUdDLFNBQUssTUFBTDtBQUNDLGlCQUNDOztRQUFLLFdBQVUsMEJBQVYsRUFBcUMsS0FBSyxNQUFNLEtBQU4sRUFBL0M7TUFDQzs7U0FBTyxXQUFVLGNBQVYsRUFBUDtPQUFpQyxNQUFNLEtBQU4sSUFBZSxFQUFmO09BRGxDO01BRUMsb0JBQUMsU0FBRDtBQUNDLGFBQU0sTUFBTSxJQUFOO0FBQ04sb0JBQWEsTUFBTSxXQUFOLElBQXFCLEdBQXJCO0FBQ2IsbUJBQVcsbUJBQVg7QUFDQSxrQkFBVyxNQUFNLFNBQU47QUFDWCxxQkFBYyxpQkFBaUIsSUFBakIsQ0FBc0IsSUFBdEIsRUFBNEIsQ0FBNUIsRUFBK0IsTUFBTSxFQUFOLENBQTdDO0FBQ0EsY0FBTyxlQUFlLENBQWYsRUFBa0IsTUFBTSxFQUFOLENBQXpCO0FBQ0EsYUFBTSxNQUFNLElBQU4sSUFBYyxFQUFkO09BUFAsQ0FGRDtNQURELENBREQ7QUFlQyxXQWZEO0FBSEQsU0FtQk0sVUFBTDtBQUNDLGlCQUNDOztRQUFLLFdBQVUsMEJBQVYsRUFBcUMsS0FBSyxNQUFNLEtBQU4sRUFBL0M7TUFDQzs7U0FBTyxXQUFVLGNBQVYsRUFBUDtPQUFpQyxNQUFNLEtBQU4sSUFBZSxFQUFmO09BRGxDO01BRUMsb0JBQUMsU0FBRDtBQUNDLG9CQUFZLEdBQVo7QUFDQSxtQkFBVyxtQkFBWDtBQUNBLGNBQU8sSUFBUDtBQUNBLGNBQU8sTUFBTSxPQUFOO0FBQ1AsYUFBTSxNQUFNLElBQU4sSUFBYyxFQUFkLEVBTFAsQ0FGRDtNQURELENBREQ7QUFZQyxXQVpEO0FBbkJELFNBZ0NNLFFBQUw7QUFDQyxpQkFDQzs7UUFBSyxXQUFVLDBCQUFWLEVBQXFDLEtBQUssTUFBTSxLQUFOLEVBQS9DO01BQ0M7O1NBQU8sV0FBVSxjQUFWLEVBQVA7T0FBaUMsTUFBTSxLQUFOLElBQWUsRUFBZjtPQURsQztNQUVDLG9CQUFDLGNBQUQ7QUFDQyxpQkFBVSxDQUFWO0FBQ0Esb0JBQWEsTUFBTSxXQUFOLElBQXFCLElBQXJCO0FBQ2IsZ0JBQVMsTUFBTSxPQUFOO0FBQ1QscUJBQWMsYUFBYSxJQUFiLENBQWtCLElBQWxCLEVBQXdCLENBQXhCLEVBQTJCLE1BQU0sRUFBTixFQUFVLEtBQXJDLENBQWQ7QUFDQSxrQkFBVyxNQUFNLFNBQU4sSUFBbUIsaUJBQW5CO0FBQ1gsaUJBQVUsZUFBZSxDQUFmLEVBQWtCLE1BQU0sRUFBTixDQUE1QjtPQU5ELENBRkQ7TUFERCxDQUREO0FBY0MsV0FkRDs7QUFoQ0QsSUFGcUM7O0FBb0RyQyxVQUFPLFNBQVAsQ0FwRHFDO0dBQVgsQ0FBdkIsQ0FUaUI7O0FBZ0VyQixTQUNDOztLQUFLLFdBQVUsc0JBQVYsRUFBaUMsS0FBSSxNQUFKO0FBQ3JDLGFBQVMsS0FBSyxVQUFMLEVBRFY7R0FFQzs7O0lBQ0csTUFBTSxXQUFOO0lBSEo7R0FLQzs7TUFBSyxXQUFXLHlCQUF5QixDQUFFLE1BQU0sUUFBTixJQUFrQixLQUFLLEtBQUwsQ0FBVyxNQUFYLEdBQXFCLE9BQXpDLEdBQW1ELEVBQW5ELENBQXpCO0FBQ2YsY0FBUyxLQUFLLGlCQUFMLEVBRFY7SUFHRSxPQUhGO0lBS0Msb0JBQUMsVUFBRDtBQUNDLFdBQUssS0FBTDtBQUNBLGNBQVUsQ0FDUCxFQUFFLE9BQU8sR0FBUCxFQUFZLFdBQVcsRUFBRSxPQUFPLEdBQVAsRUFBYixFQUEyQixNQUFNLE9BQU4sRUFEbEMsRUFFUCxFQUFFLE9BQU8sR0FBUCxFQUFZLFdBQVcsRUFBRSxPQUFPLEdBQVAsRUFBYixFQUEyQixNQUFNLE9BQU4sRUFGbEMsQ0FBVjtBQUlBLFlBQU8sZUFBZSxDQUFmLEVBQWtCLEdBQWxCO0FBQ1AsbUJBQWMsS0FBSyxZQUFMLEVBUGYsQ0FMRDtJQUxEO0dBREQsQ0FoRXFCO0VBQVg7O0FBMEZYLGFBQVksb0JBQVMsQ0FBVCxFQUFZO0FBQ3ZCLE1BQUcsQ0FBQyxLQUFLLEtBQUwsQ0FBVyxRQUFYLEVBQXFCO0FBQ3hCLFFBQUssUUFBTCxDQUFjO0FBQ2IsWUFBUSxDQUFDLEtBQUssS0FBTCxDQUFXLE1BQVg7SUFEVixFQUR3QjtHQUF6QjtFQURXOztBQVFaLHFCQUFvQiw0QkFBUyxHQUFULEVBQWM7QUFDakMsT0FBSyxRQUFMLENBQWM7QUFDYixXQUFRLEtBQVI7R0FERCxFQURpQztFQUFkOztBQU9qQixvQkFBbUIsMkJBQVMsQ0FBVCxFQUFZO0FBQ2pDLElBQUUsZUFBRixHQURpQztFQUFaOztBQUluQixlQUFjLHNCQUFTLEdBQVQsRUFBYztBQUM5QixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURhO0FBRTlCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGRzs7QUFJOUIsTUFBSSxlQUFKLEdBSjhCOztBQU05QixNQUFJLE1BQU0sRUFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixJQUFsQixFQUF3QixJQUF4QixDQUE2QixZQUE3QixDQUFOLENBTjBCO0FBTzlCLFFBQU0sR0FBQyxLQUFRLENBQVIsR0FBYSxDQUFkLEdBQW1CLE9BQU8sT0FBTyxXQUFQLENBUEY7O0FBUzlCLGNBQVksZ0JBQVosQ0FBOEIsQ0FBOUIsRUFBaUMsS0FBakMsRUFBd0MsR0FBeEMsRUFUOEI7RUFBZDs7Q0FoSlEsQ0FBdEI7O0FBOEpKLE9BQU8sT0FBUCxHQUFpQixtQkFBakI7Ozs7O0FDcktBLElBQUksaUJBQWlCLFFBQVEsb0NBQVIsQ0FBakI7QUFDSixJQUFJLGdCQUFnQixRQUFRLDRCQUFSLENBQWhCOzs7QUFJSixJQUFJLGNBQWM7QUFDZCxrQkFBaUIsU0FBUyxlQUFULENBQXlCLE9BQXpCLEVBQWtDLE1BQWxDLEVBQTBDO0FBQzdELGlCQUFlLFlBQWYsQ0FBNEI7QUFDM0IsZUFBWSxjQUFjLGdCQUFkO0FBQ1osbUJBRjJCO0FBRzNCLGlCQUgyQjtHQUE1QixFQUQ2RDtFQUExQzs7QUFRakIsaUJBQWdCLFNBQVMsY0FBVCxDQUF3QixPQUF4QixFQUFpQyxNQUFqQyxFQUF5QyxNQUF6QyxFQUFpRDtBQUNuRSxpQkFBZSxZQUFmLENBQTRCO0FBQzNCLGVBQVksY0FBYyxlQUFkO0FBQ1osbUJBRjJCO0FBRzNCLGlCQUgyQjtBQUkzQixpQkFKMkI7R0FBNUIsRUFEbUU7RUFBakQ7O0FBU2hCLG1CQUFrQixTQUFTLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLE1BQW5DLEVBQTJDLE1BQTNDLEVBQW1EO0FBQ3ZFLGlCQUFlLFlBQWYsQ0FBNEI7QUFDM0IsZUFBWSxjQUFjLGtCQUFkO0FBQ1osbUJBRjJCO0FBRzNCLGlCQUgyQjtBQUkzQixpQkFKMkI7R0FBNUIsRUFEdUU7RUFBbkQ7O0NBbEJsQjs7QUE2QkosT0FBTyxPQUFQLEdBQWlCLFdBQWpCOzs7QUNsQ0E7O0FBRUEsSUFBSSxZQUFZLFFBQVEsV0FBUixDQUFaOzs7QUFHSixPQUFPLE9BQVAsR0FBaUIsVUFBVTtBQUN2QixzQkFBa0IsSUFBbEI7QUFDQSxxQkFBaUIsSUFBakI7QUFDQSx3QkFBb0IsSUFBcEI7Q0FIYSxDQUFqQjs7Ozs7OztBQ0xBLElBQUksUUFBUSxRQUFRLE9BQVIsQ0FBUjs7QUFHSixJQUFJLG1CQUFtQixNQUFNLFdBQU4sQ0FBa0I7OztBQUN4QyxTQUFRLENBQUMsUUFBUSw4QkFBUixDQUFELENBQVI7O0FBRUcsWUFBVztBQUNiLGdCQUFjLE1BQU0sU0FBTixDQUFnQixJQUFoQixDQUFxQixVQUFyQjtBQUNkLGVBQWEsTUFBTSxTQUFOLENBQWdCLE1BQWhCO0FBQ2IsV0FBUyxNQUFNLFNBQU4sQ0FBZ0IsS0FBaEI7QUFDVCxZQUFVLE1BQU0sU0FBTixDQUFnQixLQUFoQjtBQUNWLFlBQVUsTUFBTSxTQUFOLENBQWdCLElBQWhCO0FBQ1YsYUFBVyxNQUFNLFNBQU4sQ0FBZ0IsTUFBaEI7QUFDWCxrQkFBZ0IsTUFBTSxTQUFOLENBQWdCLFNBQWhCLENBQTBCLENBQ3pDLE1BQU0sU0FBTixDQUFnQixJQUFoQixFQUNBLE1BQU0sU0FBTixDQUFnQixNQUFoQixDQUZlLENBQWhCO0VBUEU7O0FBYUEsa0JBQWlCLDJCQUFXO0FBQzlCLFNBQU87QUFDTixnQkFBYSxHQUFiO0FBQ0EsWUFBUyxDQUFDLEdBQUQsQ0FBVDtBQUNBLGFBQVUsRUFBVjtBQUNBLGFBQVUsS0FBVjtBQUNBLGNBQVcsaUJBQVg7QUFDQSxtQkFBZ0IsS0FBaEI7R0FORCxDQUQ4QjtFQUFYOztBQVdwQixrQkFBaUIsMkJBQVc7QUFDckIsU0FBTztBQUNaLFdBQVEsS0FBUjtHQURLLENBRHFCO0VBQVg7O0FBTWQsb0JBQW1CLDZCQUFXO0FBQ2hDLE9BQUssaUJBQUwsQ0FBdUIsTUFBdkIsRUFBK0IsS0FBSyxrQkFBTCxDQUEvQixDQURnQztBQUVoQyxJQUFFLEtBQUssSUFBTCxDQUFVLE9BQVYsQ0FBRixDQUFxQixJQUFyQixDQUEwQiwyQkFBMUIsRUFBdUQsS0FBSyxZQUFMLENBQXZELENBRmdDO0VBQVg7O0FBS25CLHVCQUFzQixnQ0FBVztBQUNuQyxJQUFFLEtBQUssSUFBTCxDQUFVLE9BQVYsQ0FBRixDQUFxQixNQUFyQixDQUE0QiwyQkFBNUIsRUFBeUQsS0FBSyxZQUFMLENBQXpELENBRG1DO0VBQVg7O0FBSXRCLFNBQVEsa0JBQVc7OztlQVFqQixLQUFLLEtBQUwsQ0FSaUI7TUFFcEIsaUNBRm9CO01BR3BCLHlCQUhvQjtNQUlwQiwyQkFKb0I7TUFLcEIsMkJBTG9CO01BTXBCLDZCQU5vQjtNQU9wQixtQ0FQb0I7OztBQVVyQixNQUFJLGdCQUFnQixFQUFoQixDQVZpQjs7QUFhckIsTUFBSSxnQkFBZ0IsUUFBUSxHQUFSLENBQWEsVUFBQyxFQUFELEVBQUssR0FBTCxFQUFhO0FBQzdDLE9BQUksaUJBQUo7T0FBVywwQkFBWDs7O0FBRDZDLE9BSXpDLE9BQU8sSUFBUCxJQUFlLFFBQU8sK0NBQVAsS0FBYyxRQUFkLEVBQXlCO0FBQzNDLFlBQVEsR0FBRyxLQUFILElBQVksR0FBWixDQURtQzs7QUFHM0MsUUFBSyxHQUFHLEtBQUgsS0FBYSxDQUFiLElBQW1CLEdBQUcsS0FBSCxFQUFXOztBQUVsQyxTQUFJLFlBQWMsU0FBUyxPQUFULENBQWlCLE9BQU8sR0FBRyxLQUFILENBQXhCLElBQXFDLENBQUMsQ0FBRCxFQUFPO0FBQzdELG9CQUFjLElBQWQsQ0FBb0IsRUFBRSxPQUFPLE9BQU8sS0FBUCxDQUFQLEVBQXNCLEtBQUssR0FBRyxLQUFILEVBQWpELEVBRDZEO01BQTlEO0FBR0Esc0JBQWlCLE1BQUssZ0JBQUwsQ0FBc0IsR0FBRyxLQUFILENBQXZDLENBTGtDO0tBQW5DLE1BT087QUFDTixTQUFJLFlBQWMsU0FBUyxPQUFULENBQWlCLE9BQU8sS0FBUCxDQUFqQixJQUFrQyxDQUFDLENBQUQsRUFBTztBQUMxRCxvQkFBYyxJQUFkLENBQW9CLEVBQUUsT0FBTyxPQUFPLEtBQVAsQ0FBUCxFQUFzQixLQUFLLE9BQU8sS0FBUCxDQUFMLEVBQTVDLEVBRDBEO01BQTNEO0FBR0Esc0JBQWlCLE1BQUssZ0JBQUwsQ0FBc0IsS0FBdEIsQ0FBakIsQ0FKTTtLQVBQOzs7QUFIMkMsSUFBNUMsTUFrQk8sSUFBSSxPQUFPLEVBQVAsS0FBYyxRQUFkLElBQTBCLGNBQWMsTUFBZCxFQUFzQjtBQUMxRCxhQUFRLEVBQVIsQ0FEMEQ7QUFFMUQsc0JBQWlCLE1BQUssZ0JBQUwsQ0FBc0IsS0FBdEIsQ0FBakIsQ0FGMEQ7O0FBSTFELFNBQUksWUFBYyxTQUFTLE9BQVQsQ0FBaUIsT0FBTyxLQUFQLENBQWpCLElBQWtDLENBQUMsQ0FBRCxFQUFPO0FBQzFELG9CQUFjLElBQWQsQ0FBb0IsRUFBRSxPQUFPLE9BQU8sS0FBUCxDQUFQLEVBQXNCLEtBQUssT0FBTyxLQUFQLENBQUwsRUFBNUMsRUFEMEQ7TUFBM0Q7S0FKTSxNQVFBO0FBQ04sWUFBTyxTQUFQLENBRE07S0FSQTs7QUFZUCxVQUNDOztNQUFJLEtBQU0sUUFBUSxHQUFSLEdBQWMsRUFBZDtBQUNULGNBQVMsTUFBSyxpQkFBTDtBQUNULGlCQUFXLEdBQUcsS0FBSCxLQUFhLENBQWIsR0FBaUIsQ0FBakIsR0FBc0IsR0FBRyxLQUFILElBQVksRUFBWjtBQUNqQyxnQkFBWSxjQUFaLEVBSEQ7SUFJQzs7O0tBQ0UsS0FERjtLQUpEO0lBREQsQ0FsQzZDO0dBQWIsQ0FBN0IsQ0FiaUI7O0FBNERyQixNQUFJLG9CQUFvQixjQUFjLEdBQWQsQ0FBa0IsVUFBQyxFQUFEO1VBQVEsR0FBRyxLQUFIO0dBQVIsQ0FBbEIsQ0FBb0MsSUFBcEMsQ0FBeUMsSUFBekMsQ0FBcEIsQ0E1RGlCOztBQThEckIsU0FDQzs7S0FBSyxXQUFXLFNBQVg7QUFDSixhQUFTLEtBQUssVUFBTDtBQUNULFNBQUksTUFBSixFQUZEO0dBR0M7O01BQU0sV0FBVSxTQUFWLEVBQW9CLE9BQVEsU0FBUyxNQUFULEdBQWtCLENBQWxCLEdBQXNCLEVBQUMsVUFBVSxNQUFWLEVBQXZCLEdBQTJDLEVBQUMsT0FBTyxNQUFQLEVBQTVDLEVBQWxDO0lBQ0cscUJBQXFCLFdBQXJCO0lBSko7R0FNQzs7TUFBSyxLQUFJLFNBQUosRUFBYyxXQUFXLHlCQUF5QixDQUFFLFFBQUQsSUFBYSxLQUFLLEtBQUwsQ0FBVyxNQUFYLEdBQXFCLE9BQW5DLEdBQTZDLEVBQTdDLENBQXpCLEVBQTlCO0lBQ0M7OztLQUNFLGFBREY7S0FERDtJQU5EO0dBREQsQ0E5RHFCO0VBQVg7O0FBOEVYLHFCQUFvQiw0QkFBUyxHQUFULEVBQWM7QUFDakMsT0FBSyxRQUFMLENBQWM7QUFDYixXQUFRLEtBQVI7R0FERCxFQURpQztFQUFkOztBQU1wQixvQkFBbUIsMkJBQVMsQ0FBVCxFQUFZO0FBQzlCLE1BQUksSUFBRyxDQUFILEdBQU8sT0FBTyxLQUFQLENBRG1CO0FBRTlCLE1BQUksU0FBUyxFQUFFLE1BQUYsSUFBWSxFQUFFLFVBQUYsQ0FGSzs7QUFJOUIsSUFBRSxlQUFGLEdBSjhCOztnQkFNbUIsS0FBSyxLQUFMLENBTm5CO01BTXhCLHdDQU53QjtNQU1SLG9DQU5RO01BTU0sNEJBTk47OztBQVE5QixNQUFHLGNBQUgsRUFBbUI7QUFDbEIsT0FBSSxZQUFZLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBN0IsQ0FBWixDQURjO0FBRWxCLGVBQVksU0FBQyxLQUFjLENBQWQsR0FBbUIsQ0FBcEIsR0FBeUIsYUFBYSxPQUFPLFdBQVAsQ0FGaEM7O0FBSWxCLE9BQUcsUUFBSCxFQUFhO0FBQ1osUUFBSSxRQUFDLENBQVMsT0FBVCxDQUFpQixTQUFqQixJQUE4QixDQUFDLENBQUQsSUFBUSxTQUFTLE1BQVQsR0FBa0IsY0FBbEIsRUFBb0M7QUFDOUUsa0JBQWEsQ0FBYixFQUQ4RTtLQUEvRTtJQURELE1BSU87QUFDTixpQkFBYSxDQUFiLEVBRE07SUFKUDtHQUpELE1BWU87QUFDTixnQkFBYSxDQUFiLEVBRE07R0FaUDtFQVJrQjs7QUF5Qm5CLGVBQWMsc0JBQVMsR0FBVCxFQUFjO0FBQzNCLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRFU7QUFFM0IsTUFBSSxlQUFKLEdBRjJCO0VBQWQ7O0FBTWQsYUFBWSxvQkFBUyxDQUFULEVBQVk7QUFDdkIsTUFBRyxDQUFDLEtBQUssS0FBTCxDQUFXLFFBQVgsRUFBcUI7QUFDeEIsUUFBSyxRQUFMLENBQWM7QUFDYixZQUFRLENBQUMsS0FBSyxLQUFMLENBQVcsTUFBWDtJQURWLEVBRHdCO0dBQXpCO0VBRFc7O0FBUVQsbUJBQWtCLDBCQUFTLE1BQVQsRUFBaUI7QUFDckMsTUFBSSxVQUFVLEVBQVYsQ0FEaUM7O0FBR3JDLE1BQUksS0FBSyxLQUFMLENBQVcsUUFBWCxDQUFvQixPQUFwQixDQUE0QixPQUFPLE1BQVAsQ0FBNUIsSUFBOEMsQ0FBQyxDQUFELEVBQUk7QUFDckQsY0FBVyxXQUFYLENBRHFEO0dBQXREOztBQUlBLFNBQU8sUUFBUSxJQUFSLEVBQVAsQ0FQcUM7RUFBakI7O0NBcktDLENBQW5COztBQW1MSixPQUFPLE9BQVAsR0FBaUIsZ0JBQWpCIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIihmdW5jdGlvbigpIHtcblx0bGV0IFJlYWN0XHRcdFx0PSByZXF1aXJlKCdyZWFjdCcpLFxuXHRcdFJlYWN0RE9NXHRcdD0gcmVxdWlyZSgncmVhY3QtZG9tJyksXG5cdFx0Q29tbW9uQWN0aW9uc1x0PSByZXF1aXJlKCcuLi9jb21tb24vYWN0aW9ucy9Db21tb25BY3Rpb25zJyksXG5cdFx0U2VhcmNoU2VjdGlvblx0PSByZXF1aXJlKCcuL2NvbXBvbmVudHMvU2VhcmNoU2VjdGlvbi5qc3gnKTtcblxuXHRcblx0Q29tbW9uQWN0aW9ucy5mZXRjaE9wdGlvbnMoW1xuXHRcdCdCMDAwMTA0MScsIDMxMDIsIDMzMDIsIDM0MDYsIDQxMDQsICdQMDAwMScsICdQMDAwMycsIDEyMzRcblx0XSk7XG5cblx0UmVhY3RET00ucmVuZGVyKFxuXHRcdDxTZWFyY2hTZWN0aW9uIC8+LFxuXHRcdGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzZWFyY2gnKVxuXHQpO1xuXHRcbn0pKCk7XG4iLCJ2YXIgUmVhY3RcdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0JyksXG5cdEFwaVx0XHRcdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvYXBpJyksXG4gICAgU2VhcmNoYm94U3RvcmVcdD0gcmVxdWlyZSgnLi4vc3RvcmVzL3NlYXJjaGJveFN0b3JlJyksXG5cdHN0ZXBPcHRpb25zXHRcdD0gcmVxdWlyZSgnLi4vY29uc3RhbnRzL3N0ZXBPcHRpb25zJyksXG4gICAgSW5wdXRTdGVwc1x0XHQ9IHJlcXVpcmUoJy4vSW5wdXRTdGVwcy5qc3gnKTtcblxuXG5sZXQgZXhjaGFuZ2VPcHRpb25zID0gW1xuXHR7IGNvZGVfbm06ICdVU0Qo66+46rWtIOuLrOufrCknLCBjb2RlX2lkOiAnVVNEJywgbmF0aW9ubGFiZWw6ICfrr7jqta0nIH0sXG5cdHsgY29kZV9ubTogJ0pQWSjsnbzrs7gg7JeUKScsIGNvZGVfaWQ6ICdKUFknLCBuYXRpb25sYWJlbDogJ+ydvOuzuCcgfVxuXTsgLy8gaW5pdGlhbCBvcHRpb25zIGJlZm9yZSBhcGkgY2FsbFxuXG5jbGFzcyBTZWxlY3RPcHRzIHtcblx0Y29uc3RydWN0b3IobGFiZWwsIHZhbHVlLCBkYXRhKSB7XG5cdFx0dGhpcy5sYWJlbCA9IGxhYmVsO1xuXHRcdHRoaXMudmFsdWUgPSB2YWx1ZTtcblx0XHRpZihkYXRhKSB7XG5cdFx0XHR0aGlzLmRhdGEgPSBkYXRhO1xuXHRcdH1cblx0fVxufVxuXG5cbmZ1bmN0aW9uIF9nZXRTdG9yZVN0YXRlKCkge1xuICAgIHJldHVybiBTZWFyY2hib3hTdG9yZS5nZXRTdGF0ZSgpO1xufVxuXG52YXIgU2VhcmNoU2VjdGlvbiA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcblx0bWl4aW5zOiBbXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3F1ZXJ5U3RvcmVNaXhpbicpXG5cdF0sXG5cdFxuICAgIHByb3BUeXBlczoge1xuXHRcdFxuICAgIH0sXG4gICAgXG4gICAgZ2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4ge1xuXHRcdFx0c3RlcFN0YXRlOiBfZ2V0U3RvcmVTdGF0ZSgpLFxuXHRcdFx0ZXhjaGFuZ2VPcHRpb25zXG5cdFx0fTtcbiAgICB9LFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xuXHRcdEFwaS5wb3N0KCcvbW9waWMvYXBpL2V4Y2hhbmdlL3NlbGVjdEV4Y2hhbmdlTmF0aW9uJywge30pXG5cdFx0XHQudGhlbihmdW5jdGlvbiAocmVzKSB7XG5cdFx0XHRcdGlmKHJlcy5yZXN1bHQucmVzdWx0Q29kZSA9PT0gJ1NVQ0NFU1MnKSB7XG5cdFx0XHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdFx0XHRleGNoYW5nZU9wdGlvbnM6IHJlcy5yZXN1bHQuZGF0YVxuXHRcdFx0XHRcdH0pO1xuXHRcdFx0XHR9XG5cdFx0XHR9LmJpbmQodGhpcykpO1xuXHRcdFxuXHRcdFNlYXJjaGJveFN0b3JlLmFkZENoYW5nZUxpc3RuZXIodGhpcy5fb25DaGFuZ2UpO1xuICAgIH0sXG5cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XG5cdFx0U2VhcmNoYm94U3RvcmUucmVtb3ZlQ2hhbmdlTGlzdG5lcih0aGlzLl9vbkNoYW5nZSk7XG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG5cdFx0bGV0IHtleGNoYW5nZU9wdGlvbnN9ID0gdGhpcy5zdGF0ZTtcblx0XHRleGNoYW5nZU9wdGlvbnMgPSBleGNoYW5nZU9wdGlvbnMubWFwKChlbGVtKSA9PiBuZXcgU2VsZWN0T3B0cyhlbGVtLm5hdGlvbmxhYmVsLCBlbGVtLmNvZGVfaWQsIGVsZW0pKTtcblx0XHRcblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdj5cblx0XHRcdFx0PGg0PuyWtOuWpCA8c3Ryb25nPuq4iOycteyDge2SiDwvc3Ryb25nPuydhCDssL7snLzrn6wg7Jik7IWo64KY7JqUPzwvaDQ+XG5cdFx0XHRcdDxJbnB1dFN0ZXBzXG5cdFx0XHRcdFx0c3RlcFN0YXRlPXt0aGlzLnN0YXRlLnN0ZXBTdGF0ZX1cblx0XHRcdFx0XHRzdGVwT3B0aW9ucz17T2JqZWN0LmFzc2lnbih7fSwgc3RlcE9wdGlvbnMsIHsgZXhjaGFuZ2VPcHRpb25zIH0pfVxuXHRcdFx0XHRcdG9wdENvZGVNYXA9e3RoaXMuc3RhdGUucXVlcnlTdGF0ZS5vcHRDb2RlTWFwfVxuXHRcdFx0XHQvPlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcbiAgICB9LFxuXG4gICAgX29uQ2hhbmdlOiBmdW5jdGlvbigpIHtcblx0XHR0aGlzLnNldFN0YXRlKHtcblx0XHRcdHN0ZXBTdGF0ZTogX2dldFN0b3JlU3RhdGUoKVxuXHRcdH0pO1xuICAgIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFNlYXJjaFNlY3Rpb247XG4iLCIndXNlIHN0cmljdCc7XG5cbmxldCBFdmVudEVtaXR0ZXIgPSByZXF1aXJlKCdldmVudHMnKS5FdmVudEVtaXR0ZXI7XG5sZXQgTWFpbkRpc3BhdGNoZXIgPSByZXF1aXJlKCcuLi8uLi9jb21tb24vZGlzcGF0Y2hlci9EaXNwYXRjaGVyJyk7XG5sZXQgTWFpbkNvbnN0YW50cyA9IHJlcXVpcmUoJy4uL2NvbnN0YW50cy9tYWluQ29uc3RhbnRzJyk7XG5sZXQgQ0hBTkdFX0VWRU5UID0gJ2NoYW5nZSc7XG5cbi8vIERlZmluZSBpbml0aWFsIHF1ZXJ5IHBvaW50c1xubGV0IF9zZWFyY2hib3hTdGF0ZSA9IHtcbiAgICBmaWxsZWRTdGVwczogMCwgLy8gc3RlcHMgdGhhdCBhcmUgaGFuZGxlZCBzbyBmYXJcbiAgICBzdGVwU2VsZWN0aW9uczogWycnLCAnJywge31dXG59O1xuXG5cblxuZnVuY3Rpb24gY2hhbmdlU2VsZWN0aW9uKHN0ZXBOdW0sIG9wdGlvbiwgdGFyZ2V0UHJvcCkge1xuICAgIGxldCB7IGZpbGxlZFN0ZXBzLCBzdGVwU2VsZWN0aW9ucyB9ID0gX3NlYXJjaGJveFN0YXRlO1xuXG4gICAgaWYgKHN0ZXBOdW0gPCBmaWxsZWRTdGVwcykge1xuXHRcdC8vIGVsaW1pbmF0ZSBmb3J3YXJkIHNlbGVjdGlvbnNcblx0XHRmb3IgKGxldCBpID0gc3RlcE51bTsgaSA8IGZpbGxlZFN0ZXBzOyBpKyspIHtcblx0XHRcdHN0ZXBTZWxlY3Rpb25zW2ldID0gJyc7XG5cdFx0fVxuICAgIH1cblx0XG5cdC8vIGluaXRpYWxpemUgc3RlcCAzIHNlbGVjdGlvblxuICAgIGlmIChzdGVwTnVtID09PSAyKSB7XG5cdFx0c3RlcFNlbGVjdGlvbnNbMl0gPSB7fTtcbiAgICB9XG5cdFxuXG4gICAgaWYodGFyZ2V0UHJvcCkge1xuXHRcdGxldCBzdGVwU2VsID0gc3RlcFNlbGVjdGlvbnNbc3RlcE51bSAtIDFdO1xuXHRcdGlmIChzdGVwU2VsW3RhcmdldFByb3BdID09PSBvcHRpb24gfHwgIW9wdGlvbikge1xuXHRcdFx0c3RlcFNlbFt0YXJnZXRQcm9wXSA9ICcnO1xuXHRcdFx0X3NlYXJjaGJveFN0YXRlLmZpbGxlZFN0ZXBzID0gc3RlcE51bSAtIDE7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHN0ZXBTZWxbdGFyZ2V0UHJvcF0gPSBvcHRpb247XG5cdFx0XHRfc2VhcmNoYm94U3RhdGUuZmlsbGVkU3RlcHMgPSBzdGVwTnVtO1xuXHRcdH1cbiAgICB9IGVsc2Uge1xuXHRcdGlmIChzdGVwU2VsZWN0aW9uc1tzdGVwTnVtIC0gMV0gPT09IG9wdGlvbikge1xuXHRcdFx0c3RlcFNlbGVjdGlvbnNbc3RlcE51bSAtIDFdID0gJyc7XG5cdFx0XHRfc2VhcmNoYm94U3RhdGUuZmlsbGVkU3RlcHMgPSBzdGVwTnVtIC0gMTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0c3RlcFNlbGVjdGlvbnNbc3RlcE51bSAtIDFdID0gb3B0aW9uO1xuXHRcdFx0X3NlYXJjaGJveFN0YXRlLmZpbGxlZFN0ZXBzID0gc3RlcE51bTtcblx0XHR9XG4gICAgfVxufVxuXG5cbmZ1bmN0aW9uIHRvZ2dsZUNoZWNrYm94IChzdGVwTnVtLCBvcHRpb24sIHRhcmdldFByb3ApIHtcbiAgICBsZXQgeyBzdGVwU2VsZWN0aW9ucyB9ID0gX3NlYXJjaGJveFN0YXRlO1xuICAgIGxldCBjaGVja2VkT25lcyA9IHN0ZXBTZWxlY3Rpb25zW3N0ZXBOdW0gLSAxXVt0YXJnZXRQcm9wXTtcblxuICAgIGlmKCAhKGNoZWNrZWRPbmVzIGluc3RhbmNlb2YgQXJyYXkpICkge1xuXHRcdHN0ZXBTZWxlY3Rpb25zW3N0ZXBOdW0gLSAxXVt0YXJnZXRQcm9wXSA9IFtdO1xuXHRcdGNoZWNrZWRPbmVzID0gc3RlcFNlbGVjdGlvbnNbc3RlcE51bSAtIDFdW3RhcmdldFByb3BdO1xuICAgIH1cblxuICAgIC8vIGNoZWNrIGlmIG9wdGlvbiBpcyBhbHJlYWR5IHNlbGVjdGVkXG4gICAgbGV0IGlkeCA9IGNoZWNrZWRPbmVzLmluZGV4T2Yob3B0aW9uKTtcbiAgICBcbiAgICBpZiAoaWR4ID4gLTEpIHtcblx0XHRjaGVja2VkT25lcy5zcGxpY2UoaWR4LCAxKTtcbiAgICB9IGVsc2Uge1xuXHRcdGNoZWNrZWRPbmVzLnB1c2gob3B0aW9uKTtcbiAgICB9XG5cbiAgICAvLyBtYXJrIHRoZSBzdGVwIGZpbGxlZCBpZiBhdCBsZWFzdCBvbmUgaXMgc2VsZWN0ZWRcbiAgICBpZiAoY2hlY2tlZE9uZXMubGVuZ3RoID4gMCkge1xuXHRcdF9zZWFyY2hib3hTdGF0ZS5maWxsZWRTdGVwcyA9IHN0ZXBOdW07XG4gICAgfSBlbHNlIHtcblx0XHRfc2VhcmNoYm94U3RhdGUuZmlsbGVkU3RlcHMgPSBzdGVwTnVtIC0gMTtcbiAgICB9XG4gICAgXG59XG5cblxuXG5cbmxldCBTZWFyY2hib3hTdG9yZSA9IE9iamVjdC5hc3NpZ24oe30sIEV2ZW50RW1pdHRlci5wcm90b3R5cGUsIHtcbiAgICBhZGRDaGFuZ2VMaXN0bmVyOiBmdW5jdGlvbiBhZGRDaGFuZ2VMaXN0bmVyKGNhbGxiYWNrKSB7XG5cdFx0dGhpcy5vbihDSEFOR0VfRVZFTlQsIGNhbGxiYWNrKTtcbiAgICB9LFxuXG4gICAgcmVtb3ZlQ2hhbmdlTGlzdG5lcjogZnVuY3Rpb24gcmVtb3ZlQ2hhbmdlTGlzdG5lcihjYWxsYmFjaykge1xuXHRcdHRoaXMucmVtb3ZlTGlzdGVuZXIoQ0hBTkdFX0VWRU5ULCBjYWxsYmFjayk7XG4gICAgfSxcblxuICAgIGVtaXRDaGFuZ2U6IGZ1bmN0aW9uIGVtaXRDaGFuZ2UoKSB7XG5cdFx0dGhpcy5lbWl0KENIQU5HRV9FVkVOVCk7XG4gICAgfSxcblxuICAgIGdldFN0YXRlOiBmdW5jdGlvbiBnZXRTdGF0ZSgpIHtcblx0XHRyZXR1cm4gX3NlYXJjaGJveFN0YXRlO1xuICAgIH1cbn0pO1xuXG5cbi8vIFJlZ2lzdGVyIGNhbGxiYWNrXG5NYWluRGlzcGF0Y2hlci5yZWdpc3RlcihmdW5jdGlvbiAocGF5bG9hZCkge1xuICAgIGxldCBhY3Rpb24gPSBwYXlsb2FkLmFjdGlvbjtcblxuICAgIHN3aXRjaCAoYWN0aW9uLmFjdGlvblR5cGUpIHtcblxuXHQvLyBDaGFuZ2Ugc2VsZWN0ZWRcbiAgICBjYXNlIE1haW5Db25zdGFudHMuQ0hBTkdFX1NFTEVDVElPTjpcblx0XHRjaGFuZ2VTZWxlY3Rpb24oYWN0aW9uLnN0ZXBOdW0sIGFjdGlvbi5vcHRpb24pO1xuXHRcdGJyZWFrO1xuXG5cdC8vIENoYW5nZSBjaGVja2JveCBzdGF0ZVxuICAgIGNhc2UgTWFpbkNvbnN0YW50cy5UT0dHTEVfQ0hFQ0tCT1g6XG5cdFx0dG9nZ2xlQ2hlY2tib3goYWN0aW9uLnN0ZXBOdW0sIGFjdGlvbi5vcHRpb24sIGFjdGlvbi50YXJnZXQpO1xuXHRcdGJyZWFrO1xuXG5cdC8vIENoYW5nZSBzZWxlY3RlZCBvbiB0YXJnZXQgcHJvcFxuICAgIGNhc2UgTWFpbkNvbnN0YW50cy5DSEFOR0VfV0lUSF9UQVJHRVQ6XG5cdFx0Y2hhbmdlU2VsZWN0aW9uKGFjdGlvbi5zdGVwTnVtLCBhY3Rpb24ub3B0aW9uLCBhY3Rpb24udGFyZ2V0KTtcblx0XHRicmVhaztcblxuICAgIGRlZmF1bHQ6XG5cdFx0cmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIFNlYXJjaGJveFN0b3JlLmVtaXRDaGFuZ2UoKTtcblxuICAgIHJldHVybiB0cnVlO1xufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gU2VhcmNoYm94U3RvcmU7XG4iLCIvLyBDYXRlZ29yaWVzIGFuZCB0aGVpciBzdWItY2F0ZWdvcmllcyBmb3IgdGhlIHNlYXJjaC1ib3hcbid1c2Ugc3RyaWN0JztcblxuY29uc3Qgc3RlcE9uZU9wdGlvbnMgPSBbXG5cdHsgbGFiZWw6ICfsoIDstpUnLCB2YWx1ZTogJ3NhdmluZycsIGljb246ICdpY29uLTAxJyB9LFxuXHR7IGxhYmVsOiAn64yA7LacJywgdmFsdWU6ICdsb2FuJywgaWNvbjogJ2ljb24tMDYnIH0sXG5cdHsgbGFiZWw6ICfrs7Ttl5gnLCB2YWx1ZTogJ2lucycsIGljb246ICdpY29uLTEwJyB9LFxuXHR7IGxhYmVsOiAn7Lm065OcJywgdmFsdWU6ICdjYXJkJywgaWNvbjogJ2ljb24tMTcnIH0sXG5cdHsgbGFiZWw6ICdQMlAnLCB2YWx1ZTogJ3AycCcsIGljb246ICdpY29uLTIwJyB9LFxuXHR7IGxhYmVsOiAn7ZmY7JyoJywgdmFsdWU6ICdleGNoYW5nZScsIGljb246ICdpY29uLTIzJyB9XG5dO1xuXG5jb25zdCBzYXZpbmdPcHRpb25zID0gW1xuXHR7IGxhYmVsOiAn7JiI6riIJywgdmFsdWU6ICdkZXBvc2l0JywgaWNvbjogJ2ljb24tMDInIH0sXG5cdHsgbGFiZWw6ICfsoIHquIgnLCB2YWx1ZTogJ3NhdmluZycsIGljb246ICdpY29uLTAzJyB9LFxuXHR7IGxhYmVsOiAnTU1EQScsIHZhbHVlOiAnbW1kYScsIGljb246ICdpY29uLTA0JyB9LFxuXHR7IGxhYmVsOiAn7KO87YOd7LKt7JW9JywgdmFsdWU6ICdob3VzZScsIGljb246ICdpY29uLTA1JyB9XG5dO1xuXG5jb25zdCBsb2FuT3B0aW9ucyA9IFtcblx0eyBsYWJlbDogJ+yLoOyaqeuMgOy2nCcsIHZhbHVlOiAnc2lnbmF0dXJlJywgaWNvbjogJ2ljb24tMDcnIH0sXG5cdHsgbGFiZWw6ICfri7Trs7TrjIDstpwnLCB2YWx1ZTogJ3NlY3VyZWQnLCBpY29uOiAnaWNvbi0wOCcgfVxuXTtcblxuY29uc3QgaW5zT3B0aW9ucyA9IFtcblx0eyBsYWJlbDogJ+yLpOyGkCcsIHZhbHVlOiAnc2hpbHNvbicsIGljb246ICdpY29uLTExJyB9LFxuXHR7IGxhYmVsOiAn7KCV6riwJywgdmFsdWU6ICdsaWZlJywgaWNvbjogJ2ljb24tMTInIH0sXG5cdHsgbGFiZWw6ICfsl7DquIgv7KCA7LaVJywgdmFsdWU6ICdhbm51aXR5JywgaWNvbjogJ2ljb24tMTMnIH0sXG5cdHsgbGFiZWw6ICfsnpDrj5nssKgnLCB2YWx1ZTogJ2NhcicsIGljb246ICdpY29uLTE0JyB9XG5dO1xuXG5jb25zdCBjYXJkT3B0aW9ucyA9IFtcblx0eyBsYWJlbDogJ+yLoOyaqey5tOuTnCcsIHZhbHVlOiAnY3JlZGl0JywgaWNvbjogJ2ljb24tMTgnIH0sXG5cdHsgbGFiZWw6ICfssrTtgazsubTrk5wnLCB2YWx1ZTogJ2NoZWNrJywgaWNvbjogJ2ljb24tMTknIH1cbl07XG5cbmNvbnN0IHAycE9wdGlvbnMgPSBbXG5cdHsgbGFiZWw6ICftiKzsnpDtlZjquLAnLCB2YWx1ZTogJ2ludmVzdCcsIGljb246ICdpY29uLTIxJyB9LFxuXHR7IGxhYmVsOiAn64yA7Lac7ZWY6riwJywgdmFsdWU6ICdwbG9hbicsIGljb246ICdpY29uLTIyJyB9XG5dO1xuXG5cbm1vZHVsZS5leHBvcnRzID0ge1xuXHRzdGVwT25lT3B0aW9ucyxcblx0c2F2aW5nT3B0aW9ucyxcblx0bG9hbk9wdGlvbnMsXG5cdGluc09wdGlvbnMsXG5cdGNhcmRPcHRpb25zLFxuXHRwMnBPcHRpb25zXG59O1xuIiwidmFyIFJlYWN0XHRcdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0JyksXG4gICAgU2VsZWN0RHJvcGRvd25cdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9TZWxlY3REcm9wZG93bi5qc3gnKSxcbiAgICBDaGVja2JveERyb3Bkb3duXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi9jb21wb25lbnRzL0NoZWNrYm94RHJvcGRvd24uanN4JyksXG4gICAgVGV4dElucHV0XHRcdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9UZXh0SW5wdXQuanN4JyksXG4gICAgSW5zdXJlSW5wdXREcm9wZG93biA9IHJlcXVpcmUoJy4vSW5zdXJlSW5wdXREcm9wZG93bi5qc3gnKSxcbiAgICBNYWluQWN0aW9uc1x0XHRcdD0gcmVxdWlyZSgnLi4vYWN0aW9ucy9NYWluQWN0aW9ucycpO1xuXG5cbmNsYXNzIFNlbGVjdE9wdCB7XG5cdGNvbnN0cnVjdG9yKGxhYmVsLCB2YWx1ZSwgZGF0YSkge1xuXHRcdHRoaXMubGFiZWwgPSBsYWJlbDtcblx0XHR0aGlzLnZhbHVlID0gdmFsdWU7XG5cdFx0aWYoZGF0YSkge1xuXHRcdFx0dGhpcy5kYXRhID0gZGF0YTtcblx0XHR9XG5cdH1cbn1cblxuXG52YXIgSW5wdXRTdGVwcyA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcbiAgICBwcm9wVHlwZXM6IHtcblx0XHRzdGVwU3RhdGU6IFJlYWN0LlByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblx0XHRzdGVwT3B0aW9uczogUmVhY3QuUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXHRcdG9wdENvZGVNYXA6IFJlYWN0LlByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZFxuICAgIH0sXG5cbiAgICBnZXREZWZhdWx0UHJvcHM6IGZ1bmN0aW9uKCkge1xuXG4gICAgfSxcblxuICAgIGNvbXBvbmVudERpZE1vdW50OiBmdW5jdGlvbigpIHtcblxuICAgIH0sXG5cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudDogZnVuY3Rpb24oKSB7XG5cbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcblx0XHRsZXQgeyBzdGVwU3RhdGUsIHN0ZXBPcHRpb25zLCBvcHRDb2RlTWFwIH0gPSB0aGlzLnByb3BzO1xuXHRcdGxldCBzdGVwVHdvUGxhY2Vob2xkZXIsIFN0ZXBUaHJlZUVsZW0sIHN0ZXBUaHJlZVBsYWNlaG9sZGVyLCBzdGVwVGhyZWVPcHRpb25zLCBzdGVwVGhyZWVVbml0O1xuXG5cdFx0bGV0IHsgc3RlcFNlbGVjdGlvbnMgfSA9IHN0ZXBTdGF0ZTtcblx0XHRsZXQgeyBzdGVwT25lT3B0aW9ucyB9ID0gc3RlcE9wdGlvbnM7XG5cdFx0bGV0IHN0ZXBUd29PcHRpb25zID0gc3RlcE9wdGlvbnNbYCR7c3RlcFNlbGVjdGlvbnNbMF0udG9Mb3dlckNhc2UoKX1PcHRpb25zYF07XG5cdFx0XG5cblx0XHRTdGVwVGhyZWVFbGVtID0gKFxuXHRcdFx0PFNlbGVjdERyb3Bkb3duIFxuXHRcdFx0XHRwbGFjZWhvbGRlcj0nLSdcblx0XHRcdFx0b3B0aW9ucz17WyctJ119XG5cdFx0XHRcdGRpc2FibGVkPXt0aGlzLmlzVGFiVG9EaXNhYmxlKDMpfVxuXHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlU2VsZWN0T3B0aW9uQ2xpY2suYmluZChudWxsLCAzLCBmYWxzZSl9XG5cdFx0XHRcdHNlbGVjdGVkPXsnJ31cblx0XHRcdFx0Y2xhc3NOYW1lPSdtYWluLXNlbGVjdC1kcm9wZG93bicgLz5cblx0XHQpO1xuXHRcdFxuXHRcdHN3aXRjaChzdGVwU2VsZWN0aW9uc1swXS50b0xvd2VyQ2FzZSgpKSB7XG5cdFx0XHRjYXNlKCdzYXZpbmcnKTpcblx0XHRcdFx0c3RlcFR3b1BsYWNlaG9sZGVyID0gJ+yWtOuWpCDsoIDstpXsg4HtkojsnYQg7LC+7Jy87IS47JqUPyc7XG5cblx0XHRcdFx0aWYoc3RlcFNlbGVjdGlvbnNbMV0pIHtcblx0XHRcdFx0XHRzdGVwVGhyZWVVbml0ID0gJ+unjOybkCc7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0c3dpdGNoKHN0ZXBTZWxlY3Rpb25zWzFdLnRvTG93ZXJDYXNlKCkpIHtcblx0XHRcdFx0XHRcdGNhc2UoJ2RlcG9zaXQnKTpcblx0XHRcdFx0XHRcdFx0c3RlcFRocmVlUGxhY2Vob2xkZXIgPSAn7JiI7LmY7ZWgIOq4iOyVoeydhCDsnoXroKXtlZjshLjsmpQnO1xuXHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdGNhc2UoJ3NhdmluZycpOlxuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVQbGFjZWhvbGRlciA9ICfsm5Qg64Kp7J6F6riI7JWh7J2EIOyeheugpe2VmOyEuOyalCc7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSgnbW1kYScpOlxuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVQbGFjZWhvbGRlciA9ICfsmIjsuZjtlaAg6riI7JWh7J2EIOyeheugpe2VmOyEuOyalCc7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSgnaG91c2UnKTpcblx0XHRcdFx0XHRcdFx0c3RlcFRocmVlUGxhY2Vob2xkZXIgPSAn7JuUIOuCqeyeheq4iOyVoeydhCDsnoXroKXtlZjshLjsmpQnO1xuXHRcdFx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0XHRcdGRlZmF1bHQ6XG5cdFx0XHRcdFx0XHRcdHN0ZXBUaHJlZVBsYWNlaG9sZGVyID0gJy0nO1xuXHRcdFx0XHRcdH1cblxuXHRcdFx0XHRcdFN0ZXBUaHJlZUVsZW0gPSAoXG5cdFx0XHRcdFx0XHQ8VGV4dElucHV0XG5cdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtzdGVwVGhyZWVQbGFjZWhvbGRlcn1cblx0XHRcdFx0XHRcdFx0dHlwZT0nbnVtYmVyLXdpdGgtY29tbWFzJ1xuXHRcdFx0XHRcdFx0XHRoYW5kbGVDaGFuZ2U9e3RoaXMuaGFuZGxlVGV4dElucHV0Q2hhbmdlLmJpbmQobnVsbCwgMywgJ2Ftb3VudCcpfVxuXHRcdFx0XHRcdFx0XHR2YWx1ZT17c3RlcFNlbGVjdGlvbnNbMl0uYW1vdW50IHx8ICcnfVxuXHRcdFx0XHRcdFx0XHR1bml0PXtzdGVwVGhyZWVVbml0fSAvPlxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcblx0XHRcdGNhc2UoJ2xvYW4nKTpcblx0XHRcdFx0c3RlcFR3b1BsYWNlaG9sZGVyID0gJ+yWtOuWpCDrjIDstpzsg4HtkojsnYQg7LC+7Jy87IS47JqUPyc7XG5cblx0XHRcdFx0aWYoc3RlcFNlbGVjdGlvbnNbMV0pIHtcblx0XHRcdFx0XHRzd2l0Y2goc3RlcFNlbGVjdGlvbnNbMV0udG9Mb3dlckNhc2UoKSkge1xuXHRcdFx0XHRcdFx0Y2FzZSgnc2lnbmF0dXJlJyk6XG5cdFx0XHRcdFx0XHRcdHN0ZXBUaHJlZVBsYWNlaG9sZGVyID0gJ+uMgOy2nOuwm+ydhCDquIjslaHsnYQg7J6F66Cl7ZW07KO87IS47JqULic7XG5cdFx0XHRcdFx0XHRcdHN0ZXBUaHJlZVVuaXQgPSAn66eM7JuQJztcblx0XHRcdFx0XHRcdFx0U3RlcFRocmVlRWxlbSA9IChcblx0XHRcdFx0XHRcdFx0XHQ8VGV4dElucHV0XG5cdFx0XHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj17c3RlcFRocmVlUGxhY2Vob2xkZXJ9XG5cdFx0XHRcdFx0XHRcdFx0XHR0eXBlPSdudW1iZXItd2l0aC1jb21tYXMnXG5cdFx0XHRcdFx0XHRcdFx0XHRoYW5kbGVDaGFuZ2U9e3RoaXMuaGFuZGxlVGV4dElucHV0Q2hhbmdlLmJpbmQobnVsbCwgMywgJ2Ftb3VudCcpfVxuXHRcdFx0XHRcdFx0XHRcdFx0dmFsdWU9e3N0ZXBTZWxlY3Rpb25zWzJdLmFtb3VudCB8fCAnJ31cblx0XHRcdFx0XHRcdFx0XHRcdHVuaXQ9e3N0ZXBUaHJlZVVuaXR9IC8+XG5cdFx0XHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0Y2FzZSgnc2VjdXJlZCcpOlxuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVPcHRpb25zID0gW1xuXHRcdFx0XHRcdFx0XHRcdHsgdmFsdWU6ICdCMDAwMTA4MDknLCBsYWJlbDogJ+yXhuydjCcgfSxcblx0XHRcdFx0XHRcdFx0XHR7IHZhbHVlOiAnQjAwMDEwODAxJywgbGFiZWw6ICfslYTtjIztirgnIH0sXG5cdFx0XHRcdFx0XHRcdFx0eyB2YWx1ZTogJ0IwMDAxMDgwMicsIGxhYmVsOiAn67aA64+Z7IKwJyB9LFxuXHRcdFx0XHRcdFx0XHRcdHsgdmFsdWU6ICdCMDAwMTA4MDMnLCBsYWJlbDogJ+yghOyEuCcgfSxcblx0XHRcdFx0XHRcdFx0XHR7IHZhbHVlOiAnQjAwMDEwODA0JywgbGFiZWw6ICfsm5TshLgnIH0sXG5cdFx0XHRcdFx0XHRcdFx0eyB2YWx1ZTogJ0IwMDAxMDgwNScsIGxhYmVsOiAn7J6Q64+Z7LCoJyB9LFxuXHRcdFx0XHRcdFx0XHRcdHsgdmFsdWU6ICdCMDAwMTA4MDYnLCBsYWJlbDogJ+yYiOyggeq4iCcgfSxcblx0XHRcdFx0XHRcdFx0XHR7IHZhbHVlOiAnQjAwMDEwODA3JywgbGFiZWw6ICfso7zsi50nIH0sXG5cdFx0XHRcdFx0XHRcdFx0eyB2YWx1ZTogJ0IwMDAxMDgwOCcsIGxhYmVsOiAn6riw7YOAJyB9XG5cdFx0XHRcdFx0XHRcdF07XG5cdFx0XHRcdFx0XHRcdHN0ZXBUaHJlZVBsYWNlaG9sZGVyID0gJ+uLtOuztOycoO2YleydhCDshKDtg53tlbTso7zshLjsmpQuJztcblx0XHRcdFx0XHRcdFx0U3RlcFRocmVlRWxlbSA9IChcblx0XHRcdFx0XHRcdFx0XHQ8U2VsZWN0RHJvcGRvd24gXG5cdFx0XHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj17c3RlcFRocmVlUGxhY2Vob2xkZXJ9XG5cdFx0XHRcdFx0XHRcdFx0XHRvcHRpb25zPXtzdGVwVGhyZWVPcHRpb25zfVxuXHRcdFx0XHRcdFx0XHRcdFx0ZGlzYWJsZWQ9e3RoaXMuaXNUYWJUb0Rpc2FibGUoMyl9XG5cdFx0XHRcdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlU2VsZWN0T3B0aW9uQ2xpY2suYmluZChudWxsLCAzLCAnc2VjdXJlVHlwZScsIGZhbHNlKX1cblx0XHRcdFx0XHRcdFx0XHRcdHNlbGVjdGVkPXtzdGVwU2VsZWN0aW9uc1syXS5zZWN1cmVUeXBlIHx8ICcnfVxuXHRcdFx0XHRcdFx0XHRcdFx0Y2xhc3NOYW1lPSdtYWluLXNlbGVjdC1kcm9wZG93bicgLz5cblx0XHRcdFx0XHRcdFx0KTtcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHRcdFx0XG5cdFx0XHRcdFx0XHRkZWZhdWx0OlxuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVPcHRpb25zID0gW107XG5cdFx0XHRcdFx0XHRcdHN0ZXBUaHJlZVBsYWNlaG9sZGVyID0gJy0nO1xuXHRcdFx0XHRcdFx0XHRTdGVwVGhyZWVFbGVtID0gKFxuXHRcdFx0XHRcdFx0XHRcdDxTZWxlY3REcm9wZG93biBcblx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtzdGVwVGhyZWVQbGFjZWhvbGRlcn1cblx0XHRcdFx0XHRcdFx0XHRcdG9wdGlvbnM9e1tdfVxuXHRcdFx0XHRcdFx0XHRcdFx0ZGlzYWJsZWQ9e3RoaXMuaXNUYWJUb0Rpc2FibGUoMyl9XG5cdFx0XHRcdFx0XHRcdFx0XHRzZWxlY3RlZD17Jyd9XG5cdFx0XHRcdFx0XHRcdFx0XHRjbGFzc05hbWU9J21haW4tc2VsZWN0LWRyb3Bkb3duJyAvPlxuXHRcdFx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdH1cblxuXG5cblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFxuXHRcdFx0Y2FzZSgnaW5zJyk6XG5cdFx0XHRcdHN0ZXBUd29QbGFjZWhvbGRlciA9ICfslrTrlqQg66qp7KCB7Jy866GcIOyDge2SiOydhCDssL7snLzshLjsmpQ/JztcblxuXHRcdFx0XHRpZihzdGVwU2VsZWN0aW9uc1sxXSkge1xuXHRcdFx0XHRcdGxldCBlbnRyaWVzO1xuXHRcdFx0XHRcdFxuXHRcdFx0XHRcdHN3aXRjaChzdGVwU2VsZWN0aW9uc1sxXS50b0xvd2VyQ2FzZSgpKSB7XG5cdFx0XHRcdFx0XHRjYXNlKCdzaGlsc29uJyk6XG5cdFx0XHRcdFx0XHRcdGVudHJpZXMgPSBbXG5cdFx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdFx0bGFiZWw6ICfsg53rhYTsm5TsnbwnLFxuXHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogJ251bWJlcicsXG5cdFx0XHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcjogJ1lZTU1ERCcsXG5cdFx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg6ICc2Jyxcblx0XHRcdFx0XHRcdFx0XHRcdGlkOiAnYmlydGhDb252ZXJ0VG9BZ2UnIC8vIGNvbnZlcnQgdG8gaW5zdXJlIGFnZSB3aGVuIHNlbmRpbmcgcmVxdWVzdHNcblx0XHRcdFx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHRcdGxhYmVsOiAn7ISg7YOd64u067O0Jyxcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6ICdzZWxlY3QnLFxuXHRcdFx0XHRcdFx0XHRcdFx0aWQ6ICdkYW1ibzEnLFxuXHRcdFx0XHRcdFx0XHRcdFx0b3B0aW9uczogdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIDMxMDIpXG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRdO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Y2FzZSgnbGlmZScpOlxuXHRcdFx0XHRcdFx0XHRlbnRyaWVzID0gW1xuXHRcdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHRcdGxhYmVsOiAn67O07ZeY64KY7J20Jyxcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6ICdudW1GaXhlZCcsXG5cdFx0XHRcdFx0XHRcdFx0XHRpZDogJ2FnZScsXG5cdFx0XHRcdFx0XHRcdFx0XHRmaXhlZEFzOiAnNDAnXG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRdO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Y2FzZSgnYW5udWl0eScpOlxuXHRcdFx0XHRcdFx0XHRlbnRyaWVzID0gW1xuXHRcdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHRcdGxhYmVsOiAn67O07ZeY64KY7J20Jyxcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU6ICdzZWxlY3QnLFxuXHRcdFx0XHRcdFx0XHRcdFx0aWQ6ICdhZ2UnLFxuXHRcdFx0XHRcdFx0XHRcdFx0b3B0aW9uczogdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIDMzMDIpXG5cdFx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XHRdO1xuXHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdFxuXHRcdFx0XHRcdFx0Y2FzZSgnY2FyJyk6XG5cdFx0XHRcdFx0XHRcdGVudHJpZXMgPSBbXG5cdFx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdFx0bGFiZWw6ICfrs7Ttl5jrgpjsnbQnLFxuXHRcdFx0XHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXG5cdFx0XHRcdFx0XHRcdFx0XHRpZDogJ2FnZScsXG5cdFx0XHRcdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMzQwNiksXG5cdFx0XHRcdFx0XHRcdFx0XHRkZXBlbmRlbmNpZXM6IFsnYWdlX2NvbnRyYWN0J11cblx0XHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRcdF07XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRzdGVwVGhyZWVQbGFjZWhvbGRlciA9ICfsg53rhYTsm5Tsnbzqs7wg7ISx67OE7J2EIOyeheugpe2VmOyEuOyalCc7XG5cdFx0XHRcdFx0U3RlcFRocmVlRWxlbSA9IChcblx0XHRcdFx0XHRcdDxJbnN1cmVJbnB1dERyb3Bkb3duXG5cdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXsgc3RlcFRocmVlUGxhY2Vob2xkZXIgfVxuXHRcdFx0XHRcdFx0XHRlbnRyaWVzPXtlbnRyaWVzfVxuXHRcdFx0XHRcdFx0XHRkaXNhYmxlZD17IHRoaXMuaXNUYWJUb0Rpc2FibGUoMykgfVxuXHRcdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlU2VsZWN0T3B0aW9uQ2xpY2t9XG5cdFx0XHRcdFx0XHRcdGhhbmRsZVRleHRDaGFuZ2U9e3RoaXMuaGFuZGxlVGV4dElucHV0Q2hhbmdlfVxuXHRcdFx0XHRcdFx0XHRzdGVwU2VsZWN0aW9ucz17c3RlcFNlbGVjdGlvbnN9XG5cdFx0XHRcdFx0XHQvPlxuXHRcdFx0XHRcdCk7XG5cblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFxuXHRcdFx0Y2FzZSgnY2FyZCcpOlxuXHRcdFx0XHRzdGVwVHdvUGxhY2Vob2xkZXIgPSAn7Ja065akIOy5tOuTnOulvCDssL7snLzshLjsmpQ/JztcblxuXHRcdFx0XHRpZihzdGVwU2VsZWN0aW9uc1sxXSkge1xuXHRcdFx0XHRcdHN0ZXBUaHJlZVBsYWNlaG9sZGVyID0gJ+2YnO2DneydhCDstZzrjIAgM+qwnOq5jOyngCDshKDtg53tlbTso7zshLjsmpQnO1xuXHRcdFx0XHRcdHN0ZXBUaHJlZU9wdGlvbnMgPSB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgNDEwNCk7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0U3RlcFRocmVlRWxlbSA9IChcblx0XHRcdFx0XHRcdDxDaGVja2JveERyb3Bkb3duXG5cdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtzdGVwVGhyZWVQbGFjZWhvbGRlcn1cblx0XHRcdFx0XHRcdFx0b3B0aW9ucz17c3RlcFRocmVlT3B0aW9uc31cblx0XHRcdFx0XHRcdFx0ZGlzYWJsZWQ9e3RoaXMuaXNUYWJUb0Rpc2FibGUoMyl9XG5cdFx0XHRcdFx0XHRcdGhhbmRsZVNlbGVjdD17dGhpcy5oYW5kbGVTZWxlY3RPcHRpb25DbGljay5iaW5kKG51bGwsIDMsICdiZW5lZml0c1RvU3BsaXQnLCB0cnVlKX1cblx0XHRcdFx0XHRcdFx0bGltaXRTZWxlY3Rpb249ezN9XG5cdFx0XHRcdFx0XHRcdHNlbGVjdGVkPXtzdGVwU2VsZWN0aW9uc1syXS5iZW5lZml0c1RvU3BsaXR9XG5cdFx0XHRcdFx0XHRcdGNsYXNzTmFtZT0nbWFpbi1zZWxlY3QtZHJvcGRvd24gbXVsdGktc2VsZWN0JyAvPlxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHRcdGJyZWFrO1xuXHRcdFx0Y2FzZSgncDJwJyk6XG5cdFx0XHRcdHN0ZXBUd29QbGFjZWhvbGRlciA9ICfslrTrlqQg66qp7KCB7Jy866GcIOyDge2SiOydhCDssL7snLzshLjsmpQ/JztcblxuXHRcdFx0XHRpZihzdGVwU2VsZWN0aW9uc1sxXSkge1xuXHRcdFx0XHRcdGxldCBhdHRySWQgPSAnbm9uZSc7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0c3dpdGNoKHN0ZXBTZWxlY3Rpb25zWzFdKSB7XG5cdFx0XHRcdFx0XHRjYXNlKCdpbnZlc3QnKTpcblx0XHRcdFx0XHRcdFx0c3RlcFRocmVlT3B0aW9ucyA9IHRoaXMuZ2V0U2VsZWN0T3B0aW9ucyhvcHRDb2RlTWFwLCAnUDAwMDEnKTtcblx0XHRcdFx0XHRcdFx0c3RlcFRocmVlUGxhY2Vob2xkZXIgPSAn7Yis7J6Q7ZWgIOyXheyytOulvCDshKDtg53tlbTso7zshLjsmpQnO1xuXHRcdFx0XHRcdFx0XHRhdHRySWQgPSAnY29tcGFueV9jb2RlJztcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRjYXNlKCdwbG9hbicpOlxuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVPcHRpb25zID0gdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsICdQMDAwMycpO1xuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVQbGFjZWhvbGRlciA9ICfrjIDstpwg7Jyg7ZiV7J2EIOyEoO2Dne2VtOyjvOyEuOyalCc7XG5cdFx0XHRcdFx0XHRcdGF0dHJJZCA9ICdndWJ1bic7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1x0XHRcdFxuXHRcdFx0XHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0XHRcdFx0c3RlcFRocmVlT3B0aW9ucyA9IFtdO1xuXHRcdFx0XHRcdFx0XHRzdGVwVGhyZWVQbGFjZWhvbGRlciA9ICctJztcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRTdGVwVGhyZWVFbGVtID0gKFxuXHRcdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duIFxuXHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj17c3RlcFRocmVlUGxhY2Vob2xkZXJ9XG5cdFx0XHRcdFx0XHRcdG9wdGlvbnM9e3N0ZXBUaHJlZU9wdGlvbnN9XG5cdFx0XHRcdFx0XHRcdGRpc2FibGVkPXt0aGlzLmlzVGFiVG9EaXNhYmxlKDMpfVxuXHRcdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlU2VsZWN0T3B0aW9uQ2xpY2suYmluZChudWxsLCAzLCBhdHRySWQsIGZhbHNlKX1cblx0XHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e3N0ZXBTZWxlY3Rpb25zWzJdW2F0dHJJZF0gfHwgJyd9XG5cdFx0XHRcdFx0XHRcdGNsYXNzTmFtZT0nbWFpbi1zZWxlY3QtZHJvcGRvd24nIC8+XG5cdFx0XHRcdFx0KTtcblxuXHRcdFx0XHR9XG5cdFx0XHRcdFxuXHRcdFx0XHRicmVhaztcblx0XHRcdFx0XG5cdFx0XHRjYXNlKCdleGNoYW5nZScpOlxuXHRcdFx0XHRzdGVwVHdvUGxhY2Vob2xkZXIgPSAn6rWt6rCA66W8IOyEoO2Dne2VtCDso7zshLjsmpQnO1xuXG5cdFx0XHRcdGlmKHN0ZXBTZWxlY3Rpb25zWzFdKSB7XG5cdFx0XHRcdFx0c3RlcFRocmVlUGxhY2Vob2xkZXIgPSAn7ZmY7KCE7ZWgIOq4iOyVoeydhCDsnoXroKXtlZjshLjsmpQnO1xuXHRcdFx0XHRcdGxldCBuYXRpb25PcHQgPSBzdGVwVHdvT3B0aW9ucy5maW5kKChlbGVtKSA9PiBlbGVtLnZhbHVlID09PSBzdGVwU2VsZWN0aW9uc1sxXSk7XG5cdFx0XHRcdFx0Ly9zdGVwVGhyZWVVbml0ID0gbmF0aW9uT3B0LmRhdGEuY29kZV9ubS5yZXBsYWNlKGAke25hdGlvbk9wdC5sYWJlbH0gYCwgJycpO1xuXHRcdFx0XHRcdHN0ZXBUaHJlZVVuaXQgPSAnS1JXKOybkCknO1xuXG5cdFx0XHRcdFx0U3RlcFRocmVlRWxlbSA9IChcblx0XHRcdFx0XHRcdDxUZXh0SW5wdXRcblx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e3N0ZXBUaHJlZVBsYWNlaG9sZGVyfVxuXHRcdFx0XHRcdFx0XHR0eXBlPSdudW1iZXItd2l0aC1jb21tYXMnXG5cdFx0XHRcdFx0XHRcdGhhbmRsZUNoYW5nZT17dGhpcy5oYW5kbGVUZXh0SW5wdXRDaGFuZ2UuYmluZChudWxsLCAzLCAnbW9uZXknKX1cblx0XHRcdFx0XHRcdFx0dmFsdWU9e3N0ZXBTZWxlY3Rpb25zWzJdLm1vbmV5IHx8ICcnfVxuXHRcdFx0XHRcdFx0XHR1bml0PXtzdGVwVGhyZWVVbml0fSAvPlxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRcblx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFxuXHRcdFx0ZGVmYXVsdDpcblx0XHRcdFx0c3RlcFR3b09wdGlvbnMgPSBbXTtcblx0XHRcdFx0c3RlcFR3b1BsYWNlaG9sZGVyID0gJy0nO1xuXHRcdFx0XHRcblx0XHRcdFx0U3RlcFRocmVlRWxlbSA9IChcblx0XHRcdFx0XHQ8U2VsZWN0RHJvcGRvd25cblx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPSctJ1xuXHRcdFx0XHRcdFx0b3B0aW9ucz17WycxJywgJzInXX1cblx0XHRcdFx0XHRcdGRpc2FibGVkPXt0aGlzLmlzVGFiVG9EaXNhYmxlKDMpfVxuXHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlbGVjdE9wdGlvbkNsaWNrLmJpbmQobnVsbCwgMywgZmFsc2UpfVxuXHRcdFx0XHRcdFx0c2VsZWN0ZWQ9eycnfVxuXHRcdFx0XHRcdFx0Y2xhc3NOYW1lPSdtYWluLXNlbGVjdC1kcm9wZG93bicgLz5cblx0XHRcdFx0KTtcblx0XHR9XG5cblx0XHRcblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9J3N0ZXBzLXdyYXBwZXInPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT0nc3RlcCcgPlxuXHRcdFx0XHRcdDxTZWxlY3REcm9wZG93biBcblx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPSfquIjsnLXsg4HtkojsnYQg7ISg7YOd7ZW0IOyjvOyEuOyalCdcblx0XHRcdFx0XHRcdG9wdGlvbnM9e3N0ZXBPbmVPcHRpb25zfVxuXHRcdFx0XHRcdFx0ZGlzYWJsZWQ9e3RoaXMuaXNUYWJUb0Rpc2FibGUoMSl9XG5cdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlTWVudU9wdGlvbkNsaWNrLmJpbmQobnVsbCwgMSwgZmFsc2UpfVxuXHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e3N0ZXBTZWxlY3Rpb25zWzBdfVxuXHRcdFx0XHRcdFx0Y2xhc3NOYW1lPSdtYWluLXNlbGVjdC1kcm9wZG93bidcblx0XHRcdFx0XHQvPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPSdzdGVwJyA+XG5cdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duIFxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e3N0ZXBUd29QbGFjZWhvbGRlcn1cblx0XHRcdFx0XHRcdG9wdGlvbnM9e3N0ZXBUd29PcHRpb25zfVxuXHRcdFx0XHRcdFx0ZGlzYWJsZWQ9e3RoaXMuaXNUYWJUb0Rpc2FibGUoMil9XG5cdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlTWVudU9wdGlvbkNsaWNrLmJpbmQobnVsbCwgMiwgZmFsc2UpfVxuXHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e3N0ZXBTZWxlY3Rpb25zWzFdfVxuXHRcdFx0XHRcdFx0Y2xhc3NOYW1lPSdtYWluLXNlbGVjdC1kcm9wZG93bidcblx0XHRcdFx0XHQvPlxuXHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT0nc3RlcCcgPlxuXHRcdFx0XHRcdHtTdGVwVGhyZWVFbGVtfVxuXHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHQ8YSBjbGFzc05hbWU9J3NlYXJjaF9idG4nIG9uQ2xpY2s9e3RoaXMuc2VuZFF1ZXJ5fT5zZWFyY2g8L2E+XG5cdFx0XHQ8L2Rpdj5cblx0XHQpO1xuICAgIH0sXG5cblxuICAgIGlzVGFiVG9EaXNhYmxlOiBmdW5jdGlvbiAodGFiTnVtKSB7XG5cdFx0cmV0dXJuICEoIHRoaXMucHJvcHMuc3RlcFN0YXRlLmZpbGxlZFN0ZXBzID49IHRhYk51bSAtIDEgKTtcbiAgICB9LFxuXG5cdGhhbmRsZU1lbnVPcHRpb25DbGljazogZnVuY3Rpb24oc3RlcE51bSwgaXNNdWx0aVNlbGVjdCwgZXZ0KSB7XG5cdFx0ZXZ0ID0gZXZ0PyBldnQgOiB3aW5kb3cuZXZlbnQ7XG5cdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XG5cdFx0XG5cdFx0bGV0IHZhbCA9ICQodGFyZ2V0KS5jbG9zZXN0KCdsaScpLmF0dHIoJ2RhdGEtdmFsJyk7XG5cdFx0dmFsID0gKHZhbCA9PT0gMCkgPyAwIDogKHZhbCB8fCB0YXJnZXQudGV4dENvbnRlbnQpO1xuXG5cdFx0TWFpbkFjdGlvbnMuY2hhbmdlU2VsZWN0aW9uKCBzdGVwTnVtLCB2YWwgKTtcbiAgICB9LFxuXG4gICAgaGFuZGxlVGV4dElucHV0Q2hhbmdlOiBmdW5jdGlvbihzdGVwTnVtLCBhdHRyLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblx0XHRcblx0XHRNYWluQWN0aW9ucy5jaGFuZ2VXaXRoVGFyZ2V0KCBzdGVwTnVtLCBhdHRyLCB0YXJnZXQudmFsdWUgKTtcbiAgICB9LFxuXG4gICAgaGFuZGxlU2VsZWN0T3B0aW9uQ2xpY2s6IGZ1bmN0aW9uKHN0ZXBOdW0sIGF0dHIsIGlzTXVsdGlTZWxlY3QsIGV2dCkge1xuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xuXHRcdFx0XG5cdFx0ZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xuXHRcdFxuXHRcdGxldCB2YWwgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpO1xuXHRcdHZhbCA9ICh2YWwgPT09IDApID8gMCA6ICh2YWwgfHwgdGFyZ2V0LnRleHRDb250ZW50KTtcblxuXHRcdFxuXHRcdGlmKGlzTXVsdGlTZWxlY3QpIHtcblx0XHRcdE1haW5BY3Rpb25zLnRvZ2dsZUNoZWNrYm94KCBzdGVwTnVtLCBhdHRyLCB2YWwgKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0TWFpbkFjdGlvbnMuY2hhbmdlV2l0aFRhcmdldCggc3RlcE51bSwgYXR0ciwgdmFsICk7XG5cdFx0fVxuXG4gICAgfSxcblxuXHRnZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIGNvZGUpIHtcblx0XHRpZihvcHRDb2RlTWFwW2NvZGVdKSB7XG5cdFx0XHRyZXR1cm4gKCBvcHRDb2RlTWFwW2NvZGVdLm1hcCgoZSkgPT4gbmV3IFNlbGVjdE9wdChlLmNkTmFtZSwgZS5jZCkpICk7XG5cdFx0fVxuXHRcdC8vIFRPRE86IHdoZW4gbm8gbWFwIGZvciB0aGUgY29kZT9cblx0XHRyZXR1cm4gW107XG5cdH0sXG5cblx0c2VuZFF1ZXJ5KCkge1xuXHRcdGxldCB7IHN0ZXBTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblx0XHRsZXQgeyBmaWxsZWRTdGVwcywgc3RlcFNlbGVjdGlvbnMgfSA9IHN0ZXBTdGF0ZTtcblxuXHRcdGlmKGZpbGxlZFN0ZXBzID09PSAzKSB7XG5cdFx0XHRsZXQgc2VhcmNoUXVlcnkgPSBzdGVwU2VsZWN0aW9uc1syXTtcblxuXHRcdFx0aWYoc3RlcFNlbGVjdGlvbnNbMF0udG9Mb3dlckNhc2UoKSA9PT0gJ2V4Y2hhbmdlJykge1xuXHRcdFx0XHRzZWFyY2hRdWVyeSA9IE9iamVjdC5hc3NpZ24oe30sIHNlYXJjaFF1ZXJ5LCB7IHRyX25hdGlvbjogc3RlcFNlbGVjdGlvbnNbMV0sIGV4Y2hhbmdlX3R5cGU6ICdUUl9CVVlfQ0FTSCcgfSk7IC8vIOqxsOuemOyiheulmCDtmITssLDsgrTrlYxcblx0XHRcdFx0XG5cdFx0XHRcdHdpbmRvdy5vcGVuKGBleGNoYW5nZS9leGNoYW5nZT8keyQucGFyYW0oc2VhcmNoUXVlcnkpfWAsICdfc2VsZicpO1xuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdH1cblxuXHRcdFx0c3dpdGNoKHN0ZXBTZWxlY3Rpb25zWzFdLnRvTG93ZXJDYXNlKCkpIHtcblx0XHRcdFx0Y2FzZSgnZGVwb3NpdCcpOlxuXHRcdFx0XHRcdHNlYXJjaFF1ZXJ5ID0gT2JqZWN0LmFzc2lnbih7fSwgc2VhcmNoUXVlcnksIHsgcGVyaW9kQ2Q6ICdBMDAwMTAxMDEwMycgfSk7IC8v7JiI7LmY6riw6rCEIDHrhYRcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSgnc2F2aW5nJyk6XG5cdFx0XHRcdFx0c2VhcmNoUXVlcnkgPSBPYmplY3QuYXNzaWduKHt9LCBzZWFyY2hRdWVyeSwgeyBwZXJpb2RDZDogJ0EwMDAxMDIwMTA0JyB9KTsgLy/qsIDsnoXquLDqsIQgMeuFhFxuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlKCdob3VzZScpOlxuXHRcdFx0XHRcdHNlYXJjaFF1ZXJ5ID0gT2JqZWN0LmFzc2lnbih7fSwgc2VhcmNoUXVlcnksIHsgcGVyaW9kQ2Q6ICdBMDAwMTA0MDEwMScgfSk7IC8v64Kp7J6F6riw6rCEIDHrhYRcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSgnY3JlZGl0Jyk6XG5cdFx0XHRcdGNhc2UoJ2NoZWNrJyk6XG5cdFx0XHRcdFx0c2VhcmNoUXVlcnkuYmVuZWZpdHMgPSBzZWFyY2hRdWVyeS5iZW5lZml0c1RvU3BsaXQuam9pbignfCcpO1xuXHRcdFx0XHRcdGRlbGV0ZSBzZWFyY2hRdWVyeS5iZW5lZml0c1RvU3BsaXQ7XG5cdFx0XHRcdFx0XG5cdFx0XHRcdFx0c2VhcmNoUXVlcnkgPSBPYmplY3QuYXNzaWduKHt9LCBzZWFyY2hRdWVyeSwgeyBjb21wYW55OiAnYWxsJywgZmVlOiAnQScsIG1vbnRoX3N1bTogJzUwJyB9KTsgLy8g7Lm065Oc7IKsIOyghOyytCwg7Jew7ZqM67mEIOyghOyytCwg7JuU7Y+J6reg7IKs7Jqp7JWhIDUw66eM7JuQXG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UoJ3Bsb2FuJyk6XG5cdFx0XHRcdFx0c2VhcmNoUXVlcnkgPSBPYmplY3QuYXNzaWduKHt9LCBzZWFyY2hRdWVyeSwgeyBjb21wYW55X2NvZGU6ICc5OTk5OTk5OScgfSk7IC8v7JeF7LK0IOyghOyytFxuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0fVxuXG5cblxuXHRcdFx0d2luZG93Lm9wZW4oYCR7c3RlcFNlbGVjdGlvbnNbMF19LyR7c3RlcFNlbGVjdGlvbnNbMV19PyR7JC5wYXJhbShzZWFyY2hRdWVyeSl9YCwgJ19zZWxmJyk7XG5cdFx0fVxuXHR9XG5cbiAgICBcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IElucHV0U3RlcHM7XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpLFxuICAgIFRleHRJbnB1dCA9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi9jb21wb25lbnRzL1RleHRJbnB1dC5qc3gnKSxcblx0U2VsZWN0RHJvcGRvd24gPSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9TZWxlY3REcm9wZG93bi5qc3gnKSxcbiAgICBTZWxlY3RMaXN0ID0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvU2VsZWN0TGlzdC5qc3gnKSxcbiAgICBNYWluQWN0aW9ucyA9IHJlcXVpcmUoJy4uL2FjdGlvbnMvTWFpbkFjdGlvbnMnKTtcblxuXG52YXIgSW5zdXJlSW5wdXREcm9wZG93biA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcblx0bWl4aW5zOiBbcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2NsaWNrLW91dHNpZGUtbWl4aW4nKV0sXG5cdFxuICAgIHByb3BUeXBlczoge1xuXHRcdHBsYWNlaG9sZGVyOiBSZWFjdC5Qcm9wVHlwZXMuc3RyaW5nLFxuXHRcdGVudHJpZXM6IFJlYWN0LlByb3BUeXBlcy5hcnJheSxcblx0XHRkaXNhYmxlZDogUmVhY3QuUHJvcFR5cGVzLmJvb2wsXG5cdFx0aGFuZGxlU2VsZWN0OiBSZWFjdC5Qcm9wVHlwZXMuZnVuYyxcblx0XHRoYW5kbGVUZXh0Q2hhbmdlOiBSZWFjdC5Qcm9wVHlwZXMuZnVuYyxcblx0XHRzdGVwU2VsZWN0aW9uczogUmVhY3QuUHJvcFR5cGVzLmFycmF5XG4gICAgfSxcblxuICAgIGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdHBsYWNlaG9sZGVyOiAnLScsXG5cdFx0XHRlbnRyaWVzOiBbXSxcblx0XHRcdGRpc2FibGVkOiBmYWxzZVx0XHRcdFxuXHRcdH07XG4gICAgfSxcblxuXHRnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4ge1xuXHRcdFx0aXNPcGVuOiBmYWxzZVxuICAgICAgICB9XG4gICAgfSxcbiAgICBcbiAgICBcbiAgICBjb21wb25lbnREaWRNb3VudDogZnVuY3Rpb24oKSB7XG5cdFx0dGhpcy5zZXRPbkNsaWNrT3V0c2lkZSgnbWFpbicsIHRoaXMuaGFuZGxlT3V0c2lkZUNsaWNrKTtcbiAgICB9LFxuXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xuXG4gICAgfSxcblxuICAgIHJlbmRlcjogZnVuY3Rpb24oKSB7XG5cdFx0bGV0IHByb3BzID0gdGhpcy5wcm9wcztcblx0XHRsZXQge1xuXHRcdFx0ZW50cmllcyxcblx0XHRcdGhhbmRsZVNlbGVjdCxcblx0XHRcdGhhbmRsZVRleHRDaGFuZ2UsXG5cdFx0XHRzdGVwU2VsZWN0aW9uc1xuXHRcdH0gPSB0aGlzLnByb3BzO1xuXG5cdFx0bGV0IHF1ZXJpZXMgPSBlbnRyaWVzLm1hcCggKGVudHJ5KSA9PiB7XG5cdFx0XHRsZXQgcXVlcnlFbGVtO1xuXHRcdFx0c3dpdGNoKGVudHJ5LnR5cGUpIHtcblx0XHRcdFx0Y2FzZSgnbnVtYmVyJyk6XG5cdFx0XHRcdGNhc2UoJ251bWJlci13aXRoLWNvbW1hcycpOlxuXHRcdFx0XHRjYXNlKCd0ZXh0Jyk6XG5cdFx0XHRcdFx0cXVlcnlFbGVtID0gKFxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9J2Ryb3Bkb3duLW9wdGlvbnMtc20tZm9ybScga2V5PXtlbnRyeS5sYWJlbH0+XG5cdFx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9J3NlbGVjdC1sYWJlbCc+e2VudHJ5LmxhYmVsIHx8ICcnfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdDxUZXh0SW5wdXRcblx0XHRcdFx0XHRcdFx0XHR0eXBlPXtlbnRyeS50eXBlfVxuXHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtlbnRyeS5wbGFjZWhvbGRlciB8fCAnICd9XG5cdFx0XHRcdFx0XHRcdFx0aW5wdXRDbGFzcz0ndGV4dC1pbnB1dC1ub3JtYWwnXG5cdFx0XHRcdFx0XHRcdFx0bWF4bGVuZ3RoPXtlbnRyeS5tYXhsZW5ndGh9XG5cdFx0XHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXtoYW5kbGVUZXh0Q2hhbmdlLmJpbmQobnVsbCwgMywgZW50cnkuaWQpfVxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlPXtzdGVwU2VsZWN0aW9uc1syXVtlbnRyeS5pZF19XG5cdFx0XHRcdFx0XHRcdFx0dW5pdD17ZW50cnkudW5pdCB8fCAnJ31cblx0XHRcdFx0XHRcdFx0Lz5cblx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdGNhc2UoJ251bUZpeGVkJyk6XG5cdFx0XHRcdFx0cXVlcnlFbGVtID0gKFxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9J2Ryb3Bkb3duLW9wdGlvbnMtc20tZm9ybScga2V5PXtlbnRyeS5sYWJlbH0+XG5cdFx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9J3NlbGVjdC1sYWJlbCc+e2VudHJ5LmxhYmVsIHx8ICcnfTwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdDxUZXh0SW5wdXRcblx0XHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0nICdcblx0XHRcdFx0XHRcdFx0XHRpbnB1dENsYXNzPSd0ZXh0LWlucHV0LW5vcm1hbCdcblx0XHRcdFx0XHRcdFx0XHRmaXhlZD17dHJ1ZX1cblx0XHRcdFx0XHRcdFx0XHR2YWx1ZT17ZW50cnkuZml4ZWRBc31cblx0XHRcdFx0XHRcdFx0XHR1bml0PXtlbnRyeS51bml0IHx8ICcnfSAvPlxuXHRcdFx0XHRcdFx0PC9kaXY+XHRcblx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlKCdzZWxlY3QnKTpcblx0XHRcdFx0XHRxdWVyeUVsZW0gPSAoXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT0nZHJvcGRvd24tb3B0aW9ucy1zbS1mb3JtJyBrZXk9e2VudHJ5LmxhYmVsfT5cblx0XHRcdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT0nc2VsZWN0LWxhYmVsJz57ZW50cnkubGFiZWwgfHwgJyd9PC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXG5cdFx0XHRcdFx0XHRcdFx0dGFiSW5kZXg9ezB9XG5cdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e2VudHJ5LnBsYWNlaG9sZGVyIHx8ICfshKDtg50nfVxuXHRcdFx0XHRcdFx0XHRcdG9wdGlvbnM9e2VudHJ5Lm9wdGlvbnN9XG5cdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXtoYW5kbGVTZWxlY3QuYmluZChudWxsLCAzLCBlbnRyeS5pZCwgZmFsc2UpfVxuXHRcdFx0XHRcdFx0XHRcdGNsYXNzTmFtZT17ZW50cnkuY2xhc3NOYW1lIHx8ICdzZWxlY3QtZHJvcGRvd24nfVxuXHRcdFx0XHRcdFx0XHRcdHNlbGVjdGVkPXtzdGVwU2VsZWN0aW9uc1syXVtlbnRyeS5pZF19XG5cdFx0XHRcdFx0XHRcdC8+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHQpO1xuXHRcdFx0XHRcdGJyZWFrO1xuXG5cdFx0XHR9XG5cdFx0XHRcblx0XHRcdHJldHVybiBxdWVyeUVsZW07XG5cdFx0fSk7XG5cdFx0XG5cdFx0cmV0dXJuIChcblx0XHRcdDxkaXYgY2xhc3NOYW1lPSdtYWluLXNlbGVjdC1kcm9wZG93bicgcmVmPSdtYWluJ1xuXHRcdFx0XHRvbkNsaWNrPXt0aGlzLnRvZ2dsZU9wZW59PlxuXHRcdFx0XHQ8c3Bhbj5cblx0XHRcdFx0XHR7IHByb3BzLnBsYWNlaG9sZGVyIH1cblx0XHRcdFx0PC9zcGFuPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT17J2Ryb3Bkb3duLW9wdGlvbnMtc20nICsgKCghcHJvcHMuZGlzYWJsZWQgJiYgdGhpcy5zdGF0ZS5pc09wZW4pID8gJyBvcGVuJyA6ICcnKX1cblx0XHRcdFx0XHRvbkNsaWNrPXt0aGlzLmhhbmRsZUluc2lkZUNsaWNrfSA+XG5cblx0XHRcdFx0XHR7cXVlcmllc31cblxuXHRcdFx0XHRcdDxTZWxlY3RMaXN0XG5cdFx0XHRcdFx0XHRuYW1lPSdzZXgnXG5cdFx0XHRcdFx0XHRvcHRpb25zPXsgW1xuXHRcdFx0XHRcdFx0XHRcdCB7IGxhYmVsOiAn64KoJywgZGF0YUF0dHJzOiB7IHZhbHVlOiAnMScgfSwgaWNvbjogJ2JsYW5rJyB9LFxuXHRcdFx0XHRcdFx0XHRcdCB7IGxhYmVsOiAn7JesJywgZGF0YUF0dHJzOiB7IHZhbHVlOiAnMicgfSwgaWNvbjogJ2JsYW5rJyB9LFxuXHRcdFx0XHRcdFx0XHRcdCBdIH1cblx0XHRcdFx0XHRcdHZhbHVlPXtzdGVwU2VsZWN0aW9uc1syXS5zZXh9XG5cdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlU2VsZWN0fSAvPlxuXG5cdFx0XHRcdFx0XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcbiAgICB9LFxuXG5cdHRvZ2dsZU9wZW46IGZ1bmN0aW9uKGUpIHtcblx0XHRpZighdGhpcy5wcm9wcy5kaXNhYmxlZCkge1xuXHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdGlzT3BlbjogIXRoaXMuc3RhdGUuaXNPcGVuXG5cdFx0XHR9KTtcblx0XHR9XG5cdH0sXG5cblx0aGFuZGxlT3V0c2lkZUNsaWNrOiBmdW5jdGlvbihldnQpIHtcblx0XHR0aGlzLnNldFN0YXRlKHtcblx0XHRcdGlzT3BlbjogZmFsc2Vcblx0XHR9KTtcblx0fSxcblx0XG5cbiAgICBoYW5kbGVJbnNpZGVDbGljazogZnVuY3Rpb24oZSkge1xuXHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgfSxcbiAgICBcbiAgICBoYW5kbGVTZWxlY3Q6IGZ1bmN0aW9uKGV2dCkge1xuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xuXHRcdFxuXHRcdGV2dC5zdG9wUHJvcGFnYXRpb24oKTtcblxuXHRcdGxldCB2YWwgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbHVlJyk7XG5cdFx0dmFsID0gKHZhbCA9PT0gMCkgPyAwIDogKHZhbCB8fCB0YXJnZXQudGV4dENvbnRlbnQpO1xuXHRcdFxuXHRcdE1haW5BY3Rpb25zLmNoYW5nZVdpdGhUYXJnZXQoIDMsICdzZXgnLCB2YWwgKTtcbiAgICB9XG4gICAgXG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBJbnN1cmVJbnB1dERyb3Bkb3duO1xuIiwidmFyIE1haW5EaXNwYXRjaGVyID0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2Rpc3BhdGNoZXIvRGlzcGF0Y2hlcicpO1xudmFyIE1haW5Db25zdGFudHMgPSByZXF1aXJlKCcuLi9jb25zdGFudHMvbWFpbkNvbnN0YW50cycpO1xuXG5cbi8vIERlZmluZSBhY3Rpb24gbWV0aG9kc1xudmFyIE1haW5BY3Rpb25zID0ge1xuICAgIGNoYW5nZVNlbGVjdGlvbjogZnVuY3Rpb24gY2hhbmdlU2VsZWN0aW9uKHN0ZXBOdW0sIG9wdGlvbikge1xuXHRcdE1haW5EaXNwYXRjaGVyLmhhbmRsZUFjdGlvbih7XG5cdFx0XHRhY3Rpb25UeXBlOiBNYWluQ29uc3RhbnRzLkNIQU5HRV9TRUxFQ1RJT04sXG5cdFx0XHRzdGVwTnVtLFxuXHRcdFx0b3B0aW9uXG5cdFx0fSk7XG4gICAgfSxcbiAgICBcbiAgICB0b2dnbGVDaGVja2JveDogZnVuY3Rpb24gdG9nZ2xlQ2hlY2tib3goc3RlcE51bSwgdGFyZ2V0LCBvcHRpb24pIHtcblx0XHRNYWluRGlzcGF0Y2hlci5oYW5kbGVBY3Rpb24oe1xuXHRcdFx0YWN0aW9uVHlwZTogTWFpbkNvbnN0YW50cy5UT0dHTEVfQ0hFQ0tCT1gsXG5cdFx0XHRzdGVwTnVtLFxuXHRcdFx0dGFyZ2V0LFxuXHRcdFx0b3B0aW9uXG5cdFx0fSk7XG4gICAgfSxcblxuICAgIGNoYW5nZVdpdGhUYXJnZXQ6IGZ1bmN0aW9uIGNoYW5nZVdpdGhUYXJnZXQoc3RlcE51bSwgdGFyZ2V0LCBvcHRpb24pIHtcblx0XHRNYWluRGlzcGF0Y2hlci5oYW5kbGVBY3Rpb24oe1xuXHRcdFx0YWN0aW9uVHlwZTogTWFpbkNvbnN0YW50cy5DSEFOR0VfV0lUSF9UQVJHRVQsXG5cdFx0XHRzdGVwTnVtLFxuXHRcdFx0dGFyZ2V0LFxuXHRcdFx0b3B0aW9uXG5cdFx0fSk7XG4gICAgfVxuICAgIFxufTtcblxubW9kdWxlLmV4cG9ydHMgPSBNYWluQWN0aW9ucztcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIGtleU1pcnJvciA9IHJlcXVpcmUoJ2tleW1pcnJvcicpO1xuXG4vLyBEZWZpbmUgYWN0aW9uIGNvbnN0YW50c1xubW9kdWxlLmV4cG9ydHMgPSBrZXlNaXJyb3Ioe1xuICAgIENIQU5HRV9TRUxFQ1RJT046IG51bGwsXG4gICAgVE9HR0xFX0NIRUNLQk9YOiBudWxsLFxuICAgIENIQU5HRV9XSVRIX1RBUkdFVDogbnVsbFxufSk7XG4iLCJ2YXIgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG5cbnZhciBDaGVja2JveERyb3Bkb3duID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xuXHRtaXhpbnM6IFtyZXF1aXJlKCcuLi91dGlscy9jbGljay1vdXRzaWRlLW1peGluJyldLFxuXHRcbiAgICBwcm9wVHlwZXM6IHtcblx0XHRoYW5kbGVTZWxlY3Q6IFJlYWN0LlByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG5cdFx0cGxhY2Vob2xkZXI6IFJlYWN0LlByb3BUeXBlcy5zdHJpbmcsXHRcblx0XHRvcHRpb25zOiBSZWFjdC5Qcm9wVHlwZXMuYXJyYXksXG5cdFx0c2VsZWN0ZWQ6IFJlYWN0LlByb3BUeXBlcy5hcnJheSxcblx0XHRkaXNhYmxlZDogUmVhY3QuUHJvcFR5cGVzLmJvb2wsXG5cdFx0Y2xhc3NOYW1lOiBSZWFjdC5Qcm9wVHlwZXMuc3RyaW5nLFxuXHRcdGxpbWl0U2VsZWN0aW9uOiBSZWFjdC5Qcm9wVHlwZXMub25lT2ZUeXBlKFtcblx0XHRcdFJlYWN0LlByb3BUeXBlcy5ib29sLFxuXHRcdFx0UmVhY3QuUHJvcFR5cGVzLm51bWJlclxuXHRcdF0pXG4gICAgfSxcblxuICAgIGdldERlZmF1bHRQcm9wczogZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIHtcblx0XHRcdHBsYWNlaG9sZGVyOiAnLScsXG5cdFx0XHRvcHRpb25zOiBbJy0nXSxcblx0XHRcdHNlbGVjdGVkOiBbXSxcblx0XHRcdGRpc2FibGVkOiBmYWxzZSxcblx0XHRcdGNsYXNzTmFtZTogJ3NlbGVjdC1kcm9wZG93bicsXG5cdFx0XHRsaW1pdFNlbGVjdGlvbjogZmFsc2Vcblx0XHR9O1xuICAgIH0sXG5cblx0Z2V0SW5pdGlhbFN0YXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHtcblx0XHRcdGlzT3BlbjogZmFsc2VcbiAgICAgICAgfVxuICAgIH0sXG4gICAgXG4gICAgY29tcG9uZW50RGlkTW91bnQ6IGZ1bmN0aW9uKCkge1xuXHRcdHRoaXMuc2V0T25DbGlja091dHNpZGUoJ21haW4nLCB0aGlzLmhhbmRsZU91dHNpZGVDbGljayk7XG5cdFx0JCh0aGlzLnJlZnMub3B0aW9ucykuYmluZCgnbW91c2V3aGVlbCBET01Nb3VzZVNjcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgICB9LFxuXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQ6IGZ1bmN0aW9uKCkge1xuXHRcdCQodGhpcy5yZWZzLm9wdGlvbnMpLnVuYmluZCgnbW91c2V3aGVlbCBET01Nb3VzZVNjcm9sbCcsIHRoaXMuaGFuZGxlU2Nyb2xsKTtcbiAgICB9LFxuXG4gICAgcmVuZGVyOiBmdW5jdGlvbigpIHtcblx0XHRsZXQge1xuXHRcdFx0cGxhY2Vob2xkZXIsXG5cdFx0XHRvcHRpb25zLFxuXHRcdFx0c2VsZWN0ZWQsXG5cdFx0XHRkaXNhYmxlZCxcblx0XHRcdGNsYXNzTmFtZSxcblx0XHRcdGhhbmRsZVNlbGVjdFxuXHRcdH0gPSB0aGlzLnByb3BzO1xuXG5cdFx0bGV0XHRsYWJlbFNlbGVjdGVkID0gW107XG5cblx0XHRcblx0XHRsZXQgc2VsZWN0T3B0aW9ucyA9IG9wdGlvbnMubWFwKCAoZWwsIGlkeCkgPT4ge1xuXHRcdFx0bGV0IGxhYmVsLCBvcHRpb25Dc3NDbGFzcztcblx0XHRcdFxuXHRcdFx0Ly8gY2FzZSBvcHRpb24gaXMgaW4gdGhlIGZvcm0gb2Ygb2JqZWN0IC0+IHVzZSB2YWx1ZSBhcyBvcHRpb24gY29kZVxuXHRcdFx0aWYoIGVsICE9PSBudWxsICYmIHR5cGVvZiBlbCA9PT0gJ29iamVjdCcgKSB7XG5cdFx0XHRcdGxhYmVsID0gZWwubGFiZWwgfHwgJy0nO1xuXG5cdFx0XHRcdGlmKCAoZWwudmFsdWUgPT09IDAgIHx8IGVsLnZhbHVlKSkge1xuXHRcdFx0XHRcdC8vIHJlZ2FyZCBudW1iZXIgJzAnIGFzIHRydXRoeVxuXHRcdFx0XHRcdGlmKCBzZWxlY3RlZCAmJiAoIHNlbGVjdGVkLmluZGV4T2YoU3RyaW5nKGVsLnZhbHVlKSkgPiAtMSApICkge1xuXHRcdFx0XHRcdFx0bGFiZWxTZWxlY3RlZC5wdXNoKCB7IGxhYmVsOiBTdHJpbmcobGFiZWwpLCB2YWw6IGVsLnZhbHVlIH0gKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0b3B0aW9uQ3NzQ2xhc3MgPSB0aGlzLmdldE9wdGlvbkNsYXNzZXMoZWwudmFsdWUpO1xuXG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0aWYoIHNlbGVjdGVkICYmICggc2VsZWN0ZWQuaW5kZXhPZihTdHJpbmcobGFiZWwpKSA+IC0xICkgKSB7XG5cdFx0XHRcdFx0XHRsYWJlbFNlbGVjdGVkLnB1c2goIHsgbGFiZWw6IFN0cmluZyhsYWJlbCksIHZhbDogU3RyaW5nKGxhYmVsKSB9ICk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdG9wdGlvbkNzc0NsYXNzID0gdGhpcy5nZXRPcHRpb25DbGFzc2VzKGxhYmVsKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRcblx0XHRcdFx0Ly8gY2FzZSBvcHRpb24gaXMgaW4gdGhlIGZvcm0gb2Ygc3RyaW5nIC0+IGxhYmVsIGZ1bmN0aW9ucyBhcyBvcHRpb24gY29kZVxuXHRcdFx0fSBlbHNlIGlmICh0eXBlb2YgZWwgPT09ICdzdHJpbmcnIHx8IGVsIGluc3RhbmNlb2YgU3RyaW5nKSB7XG5cdFx0XHRcdGxhYmVsID0gZWw7XG5cdFx0XHRcdG9wdGlvbkNzc0NsYXNzID0gdGhpcy5nZXRPcHRpb25DbGFzc2VzKGxhYmVsKTtcblxuXHRcdFx0XHRpZiggc2VsZWN0ZWQgJiYgKCBzZWxlY3RlZC5pbmRleE9mKFN0cmluZyhsYWJlbCkpID4gLTEgKSApIHtcblx0XHRcdFx0XHRsYWJlbFNlbGVjdGVkLnB1c2goIHsgbGFiZWw6IFN0cmluZyhsYWJlbCksIHZhbDogU3RyaW5nKGxhYmVsKSB9ICk7XG5cdFx0XHRcdH1cblx0XHRcdFx0XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXR1cm4gdW5kZWZpbmVkO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gKFxuXHRcdFx0XHQ8bGkga2V5PXsgJ29wdCcgKyBpZHggKyBlbCB9XG5cdFx0XHRcdFx0b25DbGljaz17dGhpcy5oYW5kbGVPcHRpb25DbGlja31cblx0XHRcdFx0XHRkYXRhLXZhbD17KGVsLnZhbHVlID09PSAwID8gMCA6IChlbC52YWx1ZSB8fCAnJykpfVxuXHRcdFx0XHRcdGNsYXNzTmFtZT17IG9wdGlvbkNzc0NsYXNzIH0gPlxuXHRcdFx0XHRcdDxkaXY+XG5cdFx0XHRcdFx0XHR7bGFiZWx9XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvbGk+XG5cdFx0XHQpO1xuXHRcdH0pO1xuXG5cblx0XHRsZXQgbGFiZWxTZWxlY3RlZFRleHQgPSBsYWJlbFNlbGVjdGVkLm1hcCgoZWwpID0+IGVsLmxhYmVsKS5qb2luKCcsICcpO1xuXHRcdFxuXHRcdHJldHVybiAoXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lfVxuXHRcdFx0XHRvbkNsaWNrPXt0aGlzLnRvZ2dsZU9wZW59XG5cdFx0XHRcdHJlZj0nbWFpbicgPlxuXHRcdFx0XHQ8c3BhbiBjbGFzc05hbWU9J2Rpc3BsYXknIHN0eWxlPXsgc2VsZWN0ZWQubGVuZ3RoID4gMCA/IHtmb250U2l6ZTogJzEzcHgnfSA6IHtjb2xvcjogJyM5OTknfSB9ID5cblx0XHRcdFx0XHR7IGxhYmVsU2VsZWN0ZWRUZXh0IHx8IHBsYWNlaG9sZGVyIH1cblx0XHRcdFx0PC9zcGFuPlxuXHRcdFx0XHQ8ZGl2IHJlZj0nb3B0aW9ucycgY2xhc3NOYW1lPXsnZHJvcGRvd24tb3B0aW9ucy1jaycgKyAoKCFkaXNhYmxlZCAmJiB0aGlzLnN0YXRlLmlzT3BlbikgPyAnIG9wZW4nIDogJycpfSA+XG5cdFx0XHRcdFx0PHVsPlxuXHRcdFx0XHRcdFx0e3NlbGVjdE9wdGlvbnN9XG5cdFx0XHRcdFx0PC91bD5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQpO1xuICAgIH0sXG5cblx0aGFuZGxlT3V0c2lkZUNsaWNrOiBmdW5jdGlvbihldnQpIHtcblx0XHR0aGlzLnNldFN0YXRlKHtcblx0XHRcdGlzT3BlbjogZmFsc2Vcblx0XHR9KTtcblx0fSxcblxuXHRoYW5kbGVPcHRpb25DbGljazogZnVuY3Rpb24oZSkge1xuXHRcdGUgPSBlPyBlIDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBlLnRhcmdldCB8fCBlLnNyY0VsZW1lbnQ7XG5cdFx0XG5cdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcblxuXHRcdGxldCB7IGxpbWl0U2VsZWN0aW9uLCBoYW5kbGVTZWxlY3QsIHNlbGVjdGVkIH0gPSB0aGlzLnByb3BzO1xuXG5cdFx0aWYobGltaXRTZWxlY3Rpb24pIHtcblx0XHRcdGxldCB0YXJnZXRWYWwgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpO1xuXHRcdFx0dGFyZ2V0VmFsID0gKHRhcmdldFZhbCA9PT0gMCkgPyAwIDogKHRhcmdldFZhbCB8fCB0YXJnZXQudGV4dENvbnRlbnQpO1xuXG5cdFx0XHRpZihzZWxlY3RlZCkge1xuXHRcdFx0XHRpZiggKHNlbGVjdGVkLmluZGV4T2YodGFyZ2V0VmFsKSA+IC0xKSB8fCAoc2VsZWN0ZWQubGVuZ3RoIDwgbGltaXRTZWxlY3Rpb24pICkge1xuXHRcdFx0XHRcdGhhbmRsZVNlbGVjdChlKTtcblx0XHRcdFx0fVxuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0aGFuZGxlU2VsZWN0KGUpO1xuXHRcdFx0fVxuXG5cdFx0fSBlbHNlIHtcblx0XHRcdGhhbmRsZVNlbGVjdChlKTtcblx0XHR9XG5cdH0sXG5cblx0aGFuZGxlU2Nyb2xsOiBmdW5jdGlvbihldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRldnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdH0sXG5cdFxuXG5cdHRvZ2dsZU9wZW46IGZ1bmN0aW9uKGUpIHtcblx0XHRpZighdGhpcy5wcm9wcy5kaXNhYmxlZCkge1xuXHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdGlzT3BlbjogIXRoaXMuc3RhdGUuaXNPcGVuXG5cdFx0XHR9KTtcblx0XHR9XG5cdH0sXG5cbiAgICBnZXRPcHRpb25DbGFzc2VzOiBmdW5jdGlvbihvcHRpb24pIHtcblx0XHRsZXQgY2xhc3NlcyA9ICcnO1xuXHRcdFxuXHRcdGlmICh0aGlzLnByb3BzLnNlbGVjdGVkLmluZGV4T2YoU3RyaW5nKG9wdGlvbikpID4gLTEpIHtcblx0XHRcdGNsYXNzZXMgKz0gJyBzZWxlY3RlZCc7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGNsYXNzZXMudHJpbSgpO1xuICAgIH1cblxuXG5cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENoZWNrYm94RHJvcGRvd247XG4iXX0=
