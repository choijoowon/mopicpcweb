require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({229:[function(require,module,exports){
'use strict';

(function () {

	var React = require('react'),
	    ReactDOM = require('react-dom'),
	    QueryStore = require('../common/stores/QueryStore'),
	    ProductStore = require('../common/stores/ProductStore'),
	    ComparisonStore = require('../common/stores/ComparisonStore'),
	    CommonActions = require('../common/actions/CommonActions'),
	    getSearchCallback = require('../common/utils/getSearchCallback'),
	    InsureHelperSection = require('./components/InsureHelperSection.jsx'),
	    InsureResultSection = require('./components/InsureResultSection.jsx'),
	    InsureProductAdder = require('./components/InsureProductAdder.jsx');

	var mainmenu = undefined,
	    submenu = undefined,
	    isLoggedIn = undefined,
	    searchQuery = undefined;

	if (window.App) {
		var _window$App$query = window.App.query;
		mainmenu = _window$App$query.mainmenu;
		submenu = _window$App$query.submenu;
		searchQuery = _window$App$query.searchQuery;

		isLoggedIn = window.App.comparisons.isLoggedIn;

		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
	}

	//	CommonActions.fetchOptions([
	//		3100, 3101, 3102, 3103, 3200, 3201, 3300, 3301, 3302, 1234, 3400, 3401, 3402, 3403, 3404, 3405, 3406
	//	]);

	if (isLoggedIn) {
		CommonActions.fetchMyProducts(mainmenu, submenu);
	}

	var productAdder = React.createElement(InsureProductAdder, null);

	var searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away

	ReactDOM.render(React.createElement(InsureHelperSection, {
		adderModal: productAdder }), document.getElementById('helper'), searchCallback);

	ReactDOM.render(React.createElement(InsureResultSection, null), document.getElementById('results'));
})();

},{"../common/actions/CommonActions":186,"../common/stores/ComparisonStore":208,"../common/stores/ProductStore":209,"../common/stores/QueryStore":210,"../common/utils/getSearchCallback":216,"./components/InsureHelperSection.jsx":226,"./components/InsureProductAdder.jsx":227,"./components/InsureResultSection.jsx":228,"react":177,"react-dom":20}],228:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var React = require('react'),
    ProductCard = require('../../common/components/ProductCard.jsx'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas;

var InsureResultSection = React.createClass({
	displayName: 'InsureResultSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin'), require('../../common/utils/resultSectionMixin.jsx')],

	propTypes: {},

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {};
	},
	componentDidMount: function componentDidMount() {
		//let { queryState, resultState } = this.state;

		//if (resultState.productData.length === 0) {
		//	let {mainmenu, submenu} = queryState;
		//	let query = this.getAcceptableQuery(queryState);
		//	CommonActions.fetchResults(mainmenu, submenu, query);
		//}
	},
	componentWillUnmount: function componentWillUnmount() {},
	_getOptions: function _getOptions(submenu, optCodeMap) {
		var _this = this;

		// returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (Array of ProductCard's elements)
		//    helperElem  -> (if needed) modal to use or any other element

		var _state = this.state;
		var resultState = _state.resultState;
		var comparisonState = _state.comparisonState;
		var isCmprTabActive = comparisonState.isCmprTabActive;
		var isLoggedIn = comparisonState.isLoggedIn;
		var comparisonGroup = comparisonState.comparisonGroup;
		var currentCmpr = comparisonState.currentCmpr;

		var showingCmpr = isLoggedIn && isCmprTabActive && comparisonGroup.length > 0;
		var selectedCmpr = comparisonGroup[currentCmpr];

		var filterOptions = undefined,
		    sortOptions = undefined,
		    tooltipElem = undefined,
		    resultsElem = undefined,
		    helperElem = undefined;
		var comparisonContents = undefined;

		switch (submenu.toLowerCase()) {
			// 실손보험
			case 'shilson':
				filterOptions = this._getListOptions(optCodeMap, 3101);
				filterOptions = this._addNumResults(filterOptions);

				sortOptions = this._getListOptions(optCodeMap, 3100);

				tooltipElem = React.createElement(
					'span',
					{ className: 'badge', 'data-toggle': 'tooltip', 'data-html': 'true', 'data-placement': 'right', title: '지급준비율이란?(보험회사가 가입자에게 보험금을 제때 지급할수 있는지를 나타내는 것으로 보험회사의 재무건전성을 판단할수 있는 지표입니다.)<br>부지급률이란?(청구된 보험금 중 지급하지 않은 보험금 비율을 나타냅니다.)' },
					'?'
				);

				resultsElem = resultState.productData.map(function (elem, idx) {
					var filter = elem.filter;
					var dambo1 = elem.dambo1;
					var reserve_rate = elem.reserve_rate;
					var unpaid_rate = elem.unpaid_rate;
					var prod_code = elem.prod_code;
					var sex = elem.sex;
					var age = elem.age;
					var insurance_fee = elem.insurance_fee;
					var prod_name = elem.prod_name;
					var detail_url = elem.detail_url;
					var interest_yn = elem.interest_yn;
					var company_code = elem.company_code;


					var mainAttrSet = {
						'가입유형': filter,
						'선택담보': dambo1,
						'지급준비율': reserve_rate + ' %',
						'부지급률': unpaid_rate + ' %'
					};

					if (showingCmpr) {
						comparisonContents = [{
							attr: '월 보험료',
							diff: insurance_fee - selectedCmpr.amount,
							unit: '원'
						}];
					}

					var isInterested = interest_yn === 'Y';

					return React.createElement(ProductCard, {
						key: 'shilson' + prod_code + sex + age,
						type: 'type2 shilson',
						category: filter,
						provider: company_code,
						leadingInfo: numberWithCommas(insurance_fee),
						prefix: '월',
						unit: '원',
						productTitle: prod_name,
						mainAttrSet: mainAttrSet,
						detailUrl: detail_url,
						targetModalSelector: '#product_view_popup',
						comparisonContents: comparisonContents,
						index: idx,
						isInterested: isInterested,
						handleInterestClick: _this.handleInterestClick.bind(null, isInterested, 3100, prod_code, idx),
						choiceReverse: true
					});
				});

				break;
			// 정기보험
			case 'life':
				filterOptions = this._getListOptions(optCodeMap, 3201);
				filterOptions = this._addNumResults(filterOptions);

				sortOptions = this._getListOptions(optCodeMap, 3200);

				resultsElem = resultState.productData.map(function (elem, idx) {
					var insurance_fee = elem.insurance_fee;
					var public_rate = elem.public_rate;
					var lowest_rate = elem.lowest_rate;
					var id = elem.id;
					var gubun2 = elem.gubun2;
					var price_index = elem.price_index;
					var prod_name = elem.prod_name;
					var detail_url = elem.detail_url;
					var interest_yn = elem.interest_yn;
					var prod_code = elem.prod_code;
					var company_code = elem.company_code;


					var mainAttrSet = {
						'보험료': numberWithCommas(elem.insurance_fee) + ' 원 (1개월, 40세 기준)',
						'공시이율': elem.public_rate + ' %',
						'최저보증이율': elem.lowest_rate
					};

					if (showingCmpr) {
						comparisonContents = [{
							attr: '보험가격지수',
							diff: price_index - selectedCmpr.price_index,
							unit: '%'
						}];
					}

					var isInterested = interest_yn === 'Y';

					return React.createElement(ProductCard, {
						key: 'life' + elem.id,
						type: 'type1 life',
						category: elem.gubun2,
						provider: company_code,
						leadingInfoName: '보험가격지수 <span class="badge" data-toggle="tooltip" data-html="true" data-placement="right" title="" data-original-title="보험가격지수란?<br />해당 보험사의 보험료를 참조순 보험료와 업계평균사업비의 합으로 나눈 값으로, 100보다 높으면 평균 대비 비싸고 낮으면 저렴하다는 것을 의미합니다.">?</span>',
						leadingInfo: elem.price_index,
						unit: '%',
						productTitle: elem.prod_name,
						mainAttrSet: mainAttrSet,
						detailUrl: elem.detail_url,
						targetModalSelector: '#product_view_popup',
						comparisonContents: comparisonContents,
						index: idx,
						isInterested: isInterested,
						handleInterestClick: _this.handleInterestClick.bind(null, isInterested, 3200, prod_code, idx),
						choiceReverse: true
					});
				});

				break;
			case 'annuity':
				filterOptions = this._getListOptions(optCodeMap, 3301);
				filterOptions = this._addNumResults(filterOptions);

				sortOptions = this._getListOptions(optCodeMap, 3300);

				//tooltipElem = (
				//	<span className="badge" data-toggle="tooltip" data-placement="bottom" title="보험나이, 성별 등을 고려하여 가입하신 상품보다 보험료가 저렴한, 지급준비율이 높은, 부지급률이 낮은 상품을 추천해드립니다.">?</span>
				//);

				resultsElem = resultState.productData.map(function (elem, idx) {
					var prod_name = elem.prod_name;
					var gubun = elem.gubun;
					var insurance_fee = elem.insurance_fee;
					var public_rate = elem.public_rate;
					var lowest_rate = elem.lowest_rate;
					var id = elem.id;
					var ins_type = elem.ins_type;
					var saved_rate_10 = elem.saved_rate_10;
					var detail_url = elem.detail_url;
					var refund_url = elem.refund_url;
					var interest_yn = elem.interest_yn;
					var prod_code = elem.prod_code;
					var company_code = elem.company_code;


					var period = gubun === '적립' ? '(월)' : '(일시)';

					var mainAttrSet = {};

					mainAttrSet[period + '납입액'] = numberWithCommas(elem.insurance_fee) + ' 원';

					mainAttrSet = _extends(mainAttrSet, {
						//'매년 연금 수령액': `${insurance_fee} 원`,
						'공시이율': public_rate + ' %',
						'최저보증이율': lowest_rate,
						'해지환급금': React.createElement(
							'a',
							{
								onClick: function onClick(e) {
									e.stopPropagation();
									var targetElem = $('#refund_popup');

									targetElem.removeData('bs.modal');
									targetElem.addClass('hide');

									targetElem.modal().find('.modal-body').load(refund_url, function (responseText, textStatus) {

										if (textStatus === 'success' || textStatus === 'notmodified') {
											targetElem.removeClass('in');
											targetElem.removeClass('hide');

											setTimeout(function () {
												targetElem.addClass('in');
											}, 50);
										} else {
											targetModalElem.modal('hide');
										}
									});
								} },
							'자세히보기'
						)
					});

					if (showingCmpr) {
						comparisonContents = [{
							attr: '10년 적립률',
							diff: saved_rate_10 - selectedCmpr.saved_rate_10,
							unit: '%'
						}];
					}

					var isInterested = interest_yn === 'Y';

					return React.createElement(ProductCard, {
						key: 'annuity' + id,
						type: 'type1 annuity sm',
						category: ins_type,
						provider: company_code,
						leadingInfoName: '10년 적립률 <span class="badge" data-toggle="tooltip" data-html="true" data-placement="right" title="" data-original-title="10년 적립률이란?<br>(보험상품에 가입한지 10년 후에 납부한 보험료 대비 받을 수 있는 환급금의 비율입니다.)">?</span>',
						leadingInfo: saved_rate_10,
						unit: '%',
						productTitle: prod_name,
						mainAttrSet: mainAttrSet,
						detailUrl: detail_url,
						targetModalSelector: '#product_view_popup',
						comparisonContents: comparisonContents,
						index: idx,
						isInterested: isInterested,
						handleInterestClick: _this.handleInterestClick.bind(null, isInterested, 3300, prod_code, idx),
						choiceReverse: false
					});
				});

				helperElem = React.createElement(
					'div',
					{ className: 'modal fade', id: 'refund_popup', role: 'dialog', 'aria-labelledby': 'myModalLabel', 'aria-hidden': 'true' },
					React.createElement(
						'div',
						{ className: 'modal-dialog' },
						React.createElement(
							'div',
							{ className: 'modal-content modal-large' },
							React.createElement('button', { className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' }),
							React.createElement(
								'div',
								{ className: 'modal-body' },
								'상세내용을 가져올 수 없습니다.'
							)
						)
					)
				);

				break;
			case 'car':
				filterOptions = this._getListOptions(optCodeMap, 3401);
				filterOptions = this._addNumResults(filterOptions);

				sortOptions = this._getListOptions(optCodeMap, 3400);

				//tooltipElem = (
				//	<span className="badge" data-toggle="tooltip" data-placement="bottom" title="보험나이, 성별 등을 고려하여 가입하신 상품보다 보험료가 저렴한, 지급준비율이 높은, 부지급률이 낮은 상품을 추천해드립니다.">?</span>
				//);

				resultsElem = resultState.productData.map(function (elem, idx) {
					var insurance_fee = elem.insurance_fee;
					var company_nm = elem.company_nm;
					var filter = elem.filter;
					var interest_yn = elem.interest_yn;
					var prod_code = elem.prod_code;
					var company_code = elem.company_code;
					var bigo = elem.bigo;
					var homepage = elem.homepage;


					if (showingCmpr) {
						comparisonContents = [{
							attr: '연 보험료',
							diff: insurance_fee - selectedCmpr.amount,
							unit: '원'
						}];
					}

					var isInterested = interest_yn === 'Y';

					return(
						// TODO: modify key
						React.createElement(ProductCard, {
							key: 'cars' + idx,
							type: 'type2',
							category: filter,
							provider: company_code,
							leadingInfo: numberWithCommas(elem.insurance_fee),
							prefix: '연',
							unit: '원',
							productTitle: elem.company_nm,
							comparisonContents: comparisonContents,
							index: idx,
							isInterested: isInterested,
							description: bigo,
							handleInterestClick: _this.handleInterestClick.bind(null, isInterested, 3400, prod_code, idx),
							choiceReverse: true,
							companyLink: homepage
						})
					);
				});
				break;

			default:
		}

		return { filterOptions: filterOptions, sortOptions: sortOptions, tooltipElem: tooltipElem, resultsElem: resultsElem, helperElem: helperElem };
	},
	getPreSearchView: function getPreSearchView(submenu, maskElem, fetchingMsgElem) {
		var preSearchViewElem = undefined;

		switch (submenu.toLowerCase()) {
			case 'shilson':

				break;
			case 'car':
				preSearchViewElem = React.createElement(
					'div',
					{ className: 'car_list' },
					React.createElement(
						'span',
						{ className: 'notice blue' },
						'보험나이 / 연령특약 / 성별 / 운전자한정 / 전담보가입 / 할인할증등급 / 차종'
					),
					React.createElement(
						'span',
						{ className: 'notice' },
						'을  입력해 나에게 맞는 자동차 보험을 찾아 보세요.'
					),
					React.createElement(
						'table',
						{ summary: '보험사별 자동차 보험 상품 특징' },
						React.createElement(
							'caption',
							null,
							'보험사별 자동차 보험 상품 특징'
						),
						React.createElement(
							'colgroup',
							null,
							React.createElement('col', { width: '194px;' }),
							React.createElement('col', { width: '605px;' })
						),
						React.createElement(
							'thead',
							null,
							React.createElement(
								'tr',
								null,
								React.createElement(
									'th',
									{ scope: 'col' },
									'보험사'
								),
								React.createElement(
									'th',
									{ scope: 'col' },
									'상품특징'
								)
							)
						),
						React.createElement(
							'tbody',
							null,
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi00.png', alt: '동부화재', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 50년전통의 대한민국 최초 자동차보험 회사',
										React.createElement('br', null),
										'- 365일 24시간 빠른출동 서비스',
										React.createElement('br', null),
										'- Evergreen특약, 주행거리 특약등 추가할인 혜택',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi01.png', alt: '삼성화재', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 3년무사고(타사가입경력포함)라면,보험료의 5~8%할인 혜택',
										React.createElement('br', null),
										'- 199개의 보상전국적인 보상조직 운영(14년 9월 기준)',
										React.createElement('br', null),
										'- 휴일에도 운영하는 우수협력정비업체 특화서비스 제공',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi02.png', alt: '현대해상', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 글로벌 고객만족도 자동차보험부문 8년 연속1위',
										React.createElement('br', null),
										'- 주행거리 특약 최대 13.2% 추가할인',
										React.createElement('br', null),
										'- 여성 운전자 특별 서비스',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi03.png', alt: 'KB손해보험', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 가족구성원 특성에 따른 다양한 특약 구성',
										React.createElement('br', null),
										'- 서민을 위한 보험료 할인 혜택',
										React.createElement('br', null),
										'- (희망나누기/ 희망나눔자동차 특별약관 가입시)',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi04.png', alt: '한화손해보험', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 외제차 충돌시 확대보상 특별약관',
										React.createElement('br', null),
										'- 60년 전통의 든든한 종합손해보험사',
										React.createElement('br', null),
										'- 1년 365일 24시간 함께하는 보상서비스',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi05.png', alt: '메리츠화재', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 전국 200여개 가맹점을 통한 긴급출동 서비스',
										React.createElement('br', null),
										'- 자체 보상조직망을 통한 신속하고 세심한 보상서비스',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi06.png', alt: '흥국화재', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 실속파 운전자를 위한 다양한 특약',
										React.createElement('br', null),
										'- 차량 장착물 할인을 통해 특별요율 적용으로 보험료 할인 혜택',
										React.createElement('br', null),
										'- 여성, 주말운전자 등에게 맞춘 특약 제공',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi07.png', alt: 'MG손해보험', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 긴급출동서비스(특약가입시)',
										React.createElement('br', null),
										'- 블랙박스 장착시 추가할인(특약가입시)',
										React.createElement('br', null),
										'- 마일리지 특약으로 추가할인!(특약가입시)',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi08.png', alt: 'AXA다이렉트', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 후할인 마일리지 17.4%(연간주행거리 5000km 이하시)',
										React.createElement('br', null),
										'- 블랙박스 장착시 5%, 3년 무사고 13.2% 추가할인',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi09.png', alt: '더케이손해보험', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 1만2천km이하 주행시 마일리지 특약 최대 24% 할인',
										React.createElement('br', null),
										'- 블랙박스 장착 5%, 3년 무사고시 10% 추가할인',
										React.createElement('br', null)
									)
								)
							),
							React.createElement(
								'tr',
								null,
								React.createElement(
									'td',
									null,
									React.createElement('img', { src: '/resources/images/common/ins_bi/ins_bi10.png', alt: '롯데손해보험', width: '107' })
								),
								React.createElement(
									'td',
									{ className: 'txt_left' },
									React.createElement(
										'p',
										null,
										'- 다이렉트로 한번, 주행거리로 또 한번 절감',
										React.createElement('br', null),
										'- 제휴카드 최대 3만원 청구 할인 (카드사 제공)',
										React.createElement('br', null)
									)
								)
							)
						)
					),
					maskElem,
					fetchingMsgElem
				);
				break;
			default:

		}

		return preSearchViewElem;
	}
});

module.exports = InsureResultSection;

},{"../../common/components/ProductCard.jsx":193,"../../common/utils/comparisonStoreMixin":214,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/resultSectionMixin.jsx":221,"../../common/utils/sendQueryMixin":222,"react":177}],227:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    TextInput = require('../../common/components/TextInput.jsx'),
    Api = require('../../common/utils/api'),
    calcInsureAge = require('../../common/utils/numberFilterFunctions').calcInsureAge,
    CommonActions = require('../../common/actions/CommonActions'),
    formValidators = require('../../common/utils/formValidators');

var selectRequired = formValidators.selectRequired;
var numberRequired = formValidators.numberRequired;


var submenu_codes = require('../../common/constants/submenu_codes');

var SelectOpts = function SelectOpts(label, value, data) {
	_classCallCheck(this, SelectOpts);

	this.label = label;
	this.value = value;
	this.data = data;
};

var InsureProductAdder = React.createClass({
	displayName: 'InsureProductAdder',

	propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
	},

	getDefaultProps: function getDefaultProps() {
		return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {}
		};
	},


	getInitialState: function getInitialState() {
		return {
			showValidation: false,
			query: {
				prod_name: ''
			},
			companyOpts: [],
			prodNameOpts: [],
			prod_code: '',
			price: '',
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		};
	},

	componentDidMount: function componentDidMount() {
		var menu_cd = submenu_codes[this.props.submenu];

		Api.post('/mopic/api/ins/selectAllCompanyListByInsType', { menu_cd: menu_cd }).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				var resultData = res.result.data;
				var options = resultData.map(function (e) {
					return new SelectOpts(e.company_nm, e.company_code);
				});

				this.setState({
					companyOpts: options
				});
			}
		}.bind(this));

		// clear when closed
		$('#product_add_popup').on('hidden.bs.modal', this.clearStats);
	},
	componentWillUnmount: function componentWillUnmount() {},
	render: function render() {
		var _this = this;

		var _state = this.state;
		var query = _state.query;
		var companyOpts = _state.companyOpts;
		var prodNameOpts = _state.prodNameOpts;
		var price = _state.price;
		var prod_code = _state.prod_code;
		var prodNamePlaceholder = _state.prodNamePlaceholder;
		var _props = this.props;
		var submenu = _props.submenu;
		var searchEntries = _props.searchEntries;
		var optCodeMap = _props.optCodeMap;


		var companyForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'기관명'
				),
				React.createElement(SelectDropdown, {
					placeholder: '선택',
					options: companyOpts,
					handleSelect: this.changeSelectQuery.bind(null, 'company_code', null),
					selected: query.company_code,
					validationFn: this.state.showValidation && selectRequired
				})
			)
		);

		var basicsForm = searchEntries.map(function (entry) {
			var queryElem = undefined;

			if (entry.id === 'age_contract') {
				if (query.age) {
					var ageSelected = optCodeMap[3406].find(function (e) {
						return e.cd == query.age;
					});
					// caution:  using '==' since converting into string

					if (ageSelected && ageSelected.ref1) {
						var opts = [];
						var rawOpts = JSON.parse('{ ' + ageSelected.ref1 + ' }');

						for (var cd in rawOpts) {
							opts.push({
								label: rawOpts[cd],
								value: cd
							});
						}
						entry.options = opts;
					}
				}
			}

			switch (entry.type) {
				case 'number':
				case 'number-with-commas':
				case 'text':
					queryElem = React.createElement(TextInput, {
						type: entry.type,
						placeholder: entry.placeholder || ' ',
						inputClass: 'text-input-normal',
						handleChange: _this.handleTextInputChange.bind(null, entry.id),
						maxlength: entry.maxlength,
						value: query[entry.id],
						unit: entry.unit || '',
						validationFn: _this.state.showValidation && entry.validationFn
					});
					break;
				case 'numFixed':
					queryElem = React.createElement(TextInput, {
						placeholder: ' ',
						inputClass: 'text-input-normal',
						maxlength: entry.maxlength,
						fixed: true,
						value: entry.fixedAs,
						unit: entry.unit || '' });
					break;
				case 'select':
					queryElem = React.createElement(SelectDropdown, {
						placeholder: entry.placeholder || '선택',
						options: entry.options,
						handleSelect: _this.changeSelectQuery.bind(null, entry.id, entry.dependencies),
						className: entry.className || 'select-dropdown',
						selected: query[entry.id],
						validationFn: _this.state.showValidation && entry.validationFn
					});
					break;
				default:
					queryElem = undefined;
			}

			return React.createElement(
				'div',
				{ className: 'row', key: 'adder-' + entry.label },
				React.createElement(
					'div',
					{ className: 'col-xs-6' },
					React.createElement(
						'label',
						{ className: 'select-label' },
						entry.label || ''
					),
					queryElem
				)
			);
		});

		var productNameForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'상품명'
				),
				React.createElement(SelectDropdown, {
					placeholder: prodNamePlaceholder,
					options: prodNameOpts,
					handleSelect: this.handleProductNameSelect,
					selected: prod_code,
					validationFn: this.state.showValidation && selectRequired
				})
			)
		);

		var priceLabel = submenu.toLowerCase() === 'car' ? '보험료' : '월 보험료';
		var priceForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					priceLabel
				),
				React.createElement(TextInput, {
					type: 'number-with-commas',
					placeholder: ' ',
					inputClass: 'text-input-normal',
					handleChange: this.handlePriceChange,
					value: price,
					unit: '원',
					validationFn: this.state.showValidation && numberRequired
				})
			)
		);

		return React.createElement(
			'div',
			{ className: 'modal fade ' + submenu.toLowerCase(), id: 'product_add_popup' },
			React.createElement(
				'div',
				{ className: 'modal-dialog' },
				React.createElement(
					'div',
					{ className: 'modal-content modal-small' },
					React.createElement('a', { className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' }),
					React.createElement(
						'div',
						{ className: 'modal-header' },
						React.createElement(
							'h4',
							{ className: 'modal-title', id: 'myModalLabel' },
							'가입상품 추가하기'
						)
					),
					React.createElement(
						'div',
						{ className: 'modal-body' },
						React.createElement(
							'div',
							{ className: 'product_add' },
							companyForm,
							basicsForm,
							productNameForm,
							priceForm,
							React.createElement(
								'div',
								{ className: 'center_btn' },
								React.createElement(
									'a',
									{ className: 'btn btn-submit', role: 'button', onClick: this.submitProduct },
									'확인'
								)
							)
						)
					)
				)
			)
		);
	},
	changeSelectQuery: function changeSelectQuery(queryAttr, dependencies, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('li').attr('data-val') || target.textContent;
		var query = _extends({}, this.state.query, _defineProperty({}, queryAttr, val));

		if (dependencies && dependencies.length > 0) {
			for (var i = 0, l = dependencies.length; i < l; i++) {
				var dependency = dependencies[i];
				query[dependency] = '';
			}
		}

		this.setState({
			query: query,
			prod_code: '',
			price: '',
			prodNameOpts: [],
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		}, function () {
			if (this.checkValidity()) {
				this.getProductNames(this.props.submenu, query);
			}
		}.bind(this));
	},
	handleTextInputChange: function handleTextInputChange(queryAttr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = target.value;
		var query = _extends({}, this.state.query, _defineProperty({}, queryAttr, val));

		this.setState({ // reset prod_code and price as well
			query: query,
			prod_code: '',
			price: '',
			prodNameOpts: [],
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		}, function () {
			if (this.checkValidity()) {
				this.getProductNames(this.props.submenu, query);
			}
		}.bind(this));
	},
	handlePriceChange: function handlePriceChange(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = target.value;

		this.setState({
			price: val
		});
	},
	getProductNames: function getProductNames(submenu, queryState) {
		var menu_cd = submenu_codes[this.props.submenu];
		var query = _extends({}, this.state.query, { menu_cd: menu_cd });

		this.setState({
			prodNameOpts: [],
			prodNamePlaceholder: '조건을 모두 입력해주세요'
		});

		for (var key in query) {
			var val = query[key];

			if (key === 'birthConvertToAge') {
				query['age'] = String(calcInsureAge(val));
				delete query[key];

				continue;
			}

			// number '99999999': non-sending code
			if (val == 99999999) {
				// Caution '==': check '99999999' string
				delete query[key];
			} else {
				query[key] = String(val);
			}
		}

		Api.post('/mopic/api/ins/filterProductByName', query).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				var resultData = res.result.data;
				var opts = resultData.map(function (e) {
					return new SelectOpts(e.prod_name, e.prod_code, e);
				});

				if (opts.length === 0) {
					this.setState({
						prodNamePlaceholder: '조건에 맞는 상품이 존재하지 않습니다'
					});
				} else {
					this.setState({
						prodNamePlaceholder: '선택',
						prodNameOpts: opts
					});
				}
			}
		}.bind(this));
	},
	handleProductNameSelect: function handleProductNameSelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('li').attr('data-val') || target.textContent;
		var price = this.state.prodNameOpts.find(function (e) {
			return e.value === val;
		}).data.insurance_fee;

		this.setState({
			prod_code: val,
			price: String(price)
		});
	},
	submitProduct: function submitProduct(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var _state2 = this.state;
		var query = _state2.query;
		var companyOpts = _state2.companyOpts;
		var prodNameOpts = _state2.prodNameOpts;
		var price = _state2.price;
		var prod_code = _state2.prod_code;
		var _props2 = this.props;
		var submenu = _props2.submenu;
		var searchEntries = _props2.searchEntries;

		var data = undefined;

		var allValid = this.checkValidity();

		if (allValid) {
			(function () {

				switch (submenu.toLowerCase()) {
					case 'shilson':
						data = {
							product_type: String(submenu_codes[submenu]),
							prod_code: String(prod_code),
							prod_name: String(prodNameOpts.find(function (elem) {
								return elem.value === prod_code;
							}).label),
							amount: String(price),
							age: String(calcInsureAge(query.birthConvertToAge)),
							sex: String(query.sex),
							dambo_shilson: String(query.dambo1)
						};
						break;
					case 'life':
						data = {
							product_type: String(submenu_codes[submenu]),
							prod_code: String(prod_code),
							prod_name: String(prodNameOpts.find(function (elem) {
								return elem.value === prod_code;
							}).label),
							amount: String(price),
							sex: String(query.sex),
							age: "40"
						};
						break;
					case 'annuity':
						data = {
							product_type: String(submenu_codes[submenu]),
							prod_code: String(prod_code),
							prod_name: String(prodNameOpts.find(function (elem) {
								return elem.value === prod_code;
							}).label),
							amount: String(price),
							age: String(query.age),
							sex: String(query.sex)
						};
						break;
					case 'car':
						data = {
							product_type: String(submenu_codes[submenu]),
							prod_code: String(prod_code),
							prod_name: String(prodNameOpts.find(function (elem) {
								return elem.value === prod_code;
							}).label),
							amount: String(price),
							age: String(query.age),
							sex: String(query.sex),
							age_contract: String(query.age_contract),
							driver: String(query.driver),
							dambo: String(query.dambo),
							dc_grade: String(query.dc_grade),
							car_type: String(query.car_type)
						};
						break;
				}

				var $target = $(target);
				CommonActions.addMyProduct(data, function () {
					$target.closest('.modal').modal('hide');
				});
			})();
		} else {
			this.setState({
				showValidation: true
			});
		}
	},
	clearStats: function clearStats() {
		this.setState({
			query: {
				prod_name: ''
			},
			prodNameOpts: [],
			prod_code: '',
			price: '',
			showValidation: false
		});
	},
	checkValidity: function checkValidity() {
		var allValid = true;
		var query = this.state.query;
		var searchEntries = this.props.searchEntries;


		for (var i = 0, l = searchEntries.length; i < l; i++) {
			var entry = searchEntries[i];

			if (entry.validationFn) {
				allValid = allValid && entry.validationFn(query[entry.id]) === true;
			}
		}

		return allValid;
	}
});

module.exports = InsureProductAdder;

},{"../../common/actions/CommonActions":186,"../../common/components/SelectDropdown.jsx":197,"../../common/components/TextInput.jsx":200,"../../common/constants/submenu_codes":204,"../../common/utils/api":211,"../../common/utils/formValidators":215,"../../common/utils/numberFilterFunctions":218,"react":177}],226:[function(require,module,exports){
'use strict';

var React = require('react'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas,
    formValidators = require('../../common/utils/formValidators');

var selectRequired = formValidators.selectRequired;
var requiredBirthdate = formValidators.requiredBirthdate;


var InsureHelperSection = React.createClass({
	displayName: 'InsureHelperSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin'), require('../../common/utils/helperSectionMixin.jsx')],

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {};
	},
	componentDidMount: function componentDidMount() {},
	componentWillUnmount: function componentWillUnmount() {},
	_getSearchEntries: function _getSearchEntries(queryState) {
		var submenu = queryState.submenu;
		var searchQuery = queryState.searchQuery;
		var optCodeMap = queryState.optCodeMap;

		var entries = undefined;

		switch (submenu.toLowerCase()) {
			case 'shilson':
				entries = [{
					label: '생년월일',
					type: 'number',
					placeholder: 'YYMMDD',
					maxlength: '6',
					id: 'birthConvertToAge', // convert to insure age when sending requests
					validationFn: requiredBirthdate
				}, {
					label: '성별',
					type: 'select',
					id: 'sex',
					options: this.getSelectOptions(optCodeMap, 1234),
					validationFn: selectRequired
				}, {
					label: '선택담보',
					type: 'select',
					id: 'dambo1',
					options: this.getSelectOptions(optCodeMap, 3102),
					validationFn: selectRequired
				}];
				break;
			case 'life':
				entries = [{
					label: '보험나이',
					type: 'numFixed',
					id: 'age',
					fixedAs: '40'
				}, {
					label: '성별',
					type: 'select',
					id: 'sex',
					options: this.getSelectOptions(optCodeMap, 1234),
					validationFn: selectRequired
				}];
				break;
			case 'annuity':
				entries = [{
					label: '보험나이',
					type: 'select',
					id: 'age',
					options: this.getSelectOptions(optCodeMap, 3302),
					validationFn: selectRequired
				}, {
					label: '성별',
					type: 'select',
					id: 'sex',
					options: this.getSelectOptions(optCodeMap, 1234),
					validationFn: selectRequired
				}];
				break;
			case 'car':
				var ageContractOpts = {
					label: '연령특약',
					type: 'select',
					id: 'age_contract',
					options: [],
					validationFn: selectRequired
				};

				if (searchQuery.age) {
					var ageSelected = optCodeMap[3406].find(function (e) {
						return e.cd == searchQuery.age;
					});
					// caution:  using '==' since converting into string

					if (ageSelected && ageSelected.ref1) {
						var opts = [];
						var rawOpts = JSON.parse('{ ' + ageSelected.ref1 + ' }');

						for (var cd in rawOpts) {
							opts.push({
								label: rawOpts[cd],
								value: cd
							});
						}
						ageContractOpts.options = opts;
					}
				}

				entries = [{
					label: '보험나이',
					type: 'select',
					id: 'age',
					options: this.getSelectOptions(optCodeMap, 3406),
					dependencies: ['age_contract'],
					validationFn: selectRequired
				}, ageContractOpts, {
					label: '성별',
					type: 'select',
					id: 'sex',
					options: this.getSelectOptions(optCodeMap, 1234),
					validationFn: selectRequired
				}, {
					label: '운전자한정',
					type: 'select',
					id: 'driver',
					options: this.getSelectOptions(optCodeMap, 3402),
					validationFn: selectRequired
				}, {
					label: '전담보가입',
					type: 'select',
					id: 'dambo',
					options: this.getSelectOptions(optCodeMap, 3403),
					validationFn: selectRequired
				}, {
					label: '할인할증등급',
					type: 'select',
					id: 'dc_grade',
					className: 'select-dropdown',
					options: this.getSelectOptions(optCodeMap, 3404),
					validationFn: selectRequired
				}, {
					label: '차종',
					type: 'select',
					id: 'car_type',
					options: this.getSelectOptions(optCodeMap, 3405),
					validationFn: selectRequired
				}];
				break;
			default:
				entries = [];
		}

		return entries;
	},
	_getCmprCardContents: function _getCmprCardContents(submenu, comparisonGroup) {
		var cmprCardContents = [];

		switch (submenu.toLowerCase()) {
			case 'shilson':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var prod_name = elem.prod_name;
					var company_nm = elem.company_nm;
					var amount = elem.amount;
					var filter_name = elem.filter_name;
					var dambo_shilson_name = elem.dambo_shilson_name;


					return {
						title: prod_name,
						info: [{
							attr: '보험사명',
							value: elem.company_nm,
							colSpan: 3,
							isProvider: true
						}, {
							attr: '보험료',
							value: numberWithCommas(elem.amount),
							colSpan: 3,
							prefix: '월',
							unit: '원',
							isLeadingInfo: true
						}, {
							attr: '가입유형',
							value: elem.filter_name,
							colSpan: 3
						}, {
							attr: '선택담보',
							value: elem.dambo_shilson_name,
							colSpan: 3
						}]
					};
				});
				break;
			case 'life':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var prod_name = elem.prod_name;
					var company_nm = elem.company_nm;
					var amount = elem.amount;
					var filter_name = elem.filter_name;
					var price_index = elem.price_index;
					var public_rate = elem.public_rate;
					var lowest_rate = elem.lowest_rate;


					return {
						title: prod_name,
						info: [{
							attr: '보험사명',
							value: company_nm,
							colSpan: 3,
							isProvider: true
						}, {
							attr: '보험가격지수',
							value: price_index,
							colSpan: 3,
							unit: '%',
							isLeadingInfo: true
						}, {
							attr: '공시이율',
							value: public_rate,
							colSpan: 3,
							unit: '%'
						}, {
							attr: '최저보증이율',
							value: lowest_rate,
							colSpan: 3,
							unit: '%'
						}]
					};
				});
				break;
			case 'annuity':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var prod_name = elem.prod_name;
					var company_nm = elem.company_nm;
					var amount = elem.amount;
					var gubun = elem.gubun;
					var saved_rate_10 = elem.saved_rate_10;
					var public_rate = elem.public_rate;
					var lowest_rate = elem.lowest_rate;


					return {
						title: prod_name,
						info: [{
							attr: '보험사명',
							value: company_nm,
							colSpan: 2,
							isProvider: true
						}, {
							attr: '10년 적립률',
							value: saved_rate_10,
							colSpan: 2,
							unit: '%',
							isLeadingInfo: true
						}, {
							attr: '공시이율',
							value: public_rate,
							colSpan: 2,
							unit: '%'
						}, {
							attr: '최저보증이율',
							value: lowest_rate,
							colSpan: 4,
							unit: '',
							style: { width: '40%', borderRight: 'none', borderLeft: '1px solid #77bdf1' }
						}]
					};
				});
				break;
			case 'car':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var prod_name = elem.prod_name;
					var company_nm = elem.company_nm;
					var amount = elem.amount;


					return {
						title: prod_name,
						info: [{
							attr: '보험사명',
							value: company_nm,
							colSpan: 6,
							isProvider: true
						}, {
							attr: '보험료',
							value: numberWithCommas(amount),
							colSpan: 6,
							prefix: '연',
							unit: '원',
							isLeadingInfo: true
						}]
					};
				});
				break;

		}

		return cmprCardContents;
	}
});

module.exports = InsureHelperSection;

},{"../../common/utils/comparisonStoreMixin":214,"../../common/utils/formValidators":215,"../../common/utils/helperSectionMixin.jsx":217,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/sendQueryMixin":222,"react":177}]},{},[229])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvaW5zdXJlLXBhZ2UvaW5zdXJlLXBhZ2UuanN4Iiwic3JjL2luc3VyZS1wYWdlL2NvbXBvbmVudHMvSW5zdXJlUmVzdWx0U2VjdGlvbi5qc3giLCJzcmMvaW5zdXJlLXBhZ2UvY29tcG9uZW50cy9JbnN1cmVQcm9kdWN0QWRkZXIuanN4Iiwic3JjL2luc3VyZS1wYWdlL2NvbXBvbmVudHMvSW5zdXJlSGVscGVyU2VjdGlvbi5qc3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0FBLENBQUMsWUFBVzs7QUFFUixLQUFJLFFBQVcsUUFBUSxPQUFSLENBQVg7S0FDTixXQUFhLFFBQVEsV0FBUixDQUFiO0tBQ0EsYUFBZSxRQUFRLDZCQUFSLENBQWY7S0FDQSxlQUFnQixRQUFRLCtCQUFSLENBQWhCO0tBQ0Esa0JBQW1CLFFBQVEsa0NBQVIsQ0FBbkI7S0FDQSxnQkFBaUIsUUFBUSxpQ0FBUixDQUFqQjtLQUNBLG9CQUFvQixRQUFRLG1DQUFSLENBQXBCO0tBQ0Esc0JBQXNCLFFBQVEsc0NBQVIsQ0FBdEI7S0FDQSxzQkFBc0IsUUFBUSxzQ0FBUixDQUF0QjtLQUNBLHFCQUFxQixRQUFRLHFDQUFSLENBQXJCLENBWFU7O0FBY1gsS0FBSSxvQkFBSjtLQUFjLG1CQUFkO0tBQXVCLHNCQUF2QjtLQUFtQyx1QkFBbkMsQ0FkVzs7QUFnQlIsS0FBRyxPQUFPLEdBQVAsRUFBWTswQkFDbUIsT0FBTyxHQUFQLENBQVcsS0FBWCxDQURuQjtBQUNmLHdDQURlO0FBQ0wsc0NBREs7QUFDSSw4Q0FESjs7QUFFakIsZUFBYSxPQUFPLEdBQVAsQ0FBVyxXQUFYLENBQXVCLFVBQXZCLENBRkk7O0FBSWpCLGFBQVcsU0FBWCxDQUFxQixPQUFyQixFQUppQjtBQUtqQixlQUFhLFNBQWIsQ0FBdUIsU0FBdkIsRUFMaUI7QUFNakIsa0JBQWdCLFNBQWhCLENBQTBCLGFBQTFCLEVBTmlCO0VBQWY7Ozs7OztBQWhCUSxLQStCUixVQUFILEVBQWU7QUFDZCxnQkFBYyxlQUFkLENBQThCLFFBQTlCLEVBQXdDLE9BQXhDLEVBRGM7RUFBZjs7QUFJQSxLQUFJLGVBQWUsb0JBQUMsa0JBQUQsT0FBZixDQW5DTzs7QUFxQ1gsS0FBSSxpQkFBaUIsa0JBQWtCLFdBQWxCLENBQWpCOztBQXJDTyxTQXdDUixDQUFTLE1BQVQsQ0FDRixvQkFBQyxtQkFBRDtBQUNDLGNBQVksWUFBWixFQURELENBREUsRUFHRixTQUFTLGNBQVQsQ0FBd0IsUUFBeEIsQ0FIRSxFQUlGLGNBSkUsRUF4Q1E7O0FBK0NYLFVBQVMsTUFBVCxDQUNDLG9CQUFDLG1CQUFELE9BREQsRUFFQyxTQUFTLGNBQVQsQ0FBd0IsU0FBeEIsQ0FGRCxFQS9DVztDQUFYLENBQUQ7Ozs7Ozs7QUNBQSxJQUFJLFFBQVcsUUFBUSxPQUFSLENBQVg7SUFDSCxjQUFnQixRQUFRLHlDQUFSLENBQWhCO0lBQ0EsbUJBQW1CLFFBQVEsMENBQVIsRUFBb0QsZ0JBQXBEOztBQUlwQixJQUFJLHNCQUFzQixNQUFNLFdBQU4sQ0FBa0I7OztBQUMzQyxTQUFRLENBQ1AsUUFBUSxvQ0FBUixDQURPLEVBRVAsUUFBUSxzQ0FBUixDQUZPLEVBR1AsUUFBUSx5Q0FBUixDQUhPLEVBSVAsUUFBUSxtQ0FBUixDQUpPLEVBS1AsUUFBUSwyQ0FBUixDQUxPLENBQVI7O0FBUUcsWUFBVyxFQUFYOztBQUlBLDZDQUFrQjtBQUNkLFNBQU8sRUFBUCxDQURjO0VBYnNCO0FBbUJ4Qyw2Q0FBa0I7QUFDZCxTQUFPLEVBQVAsQ0FEYztFQW5Cc0I7QUF5QnhDLGlEQUFvQjs7Ozs7Ozs7RUF6Qm9CO0FBbUN4Qyx1REFBdUIsRUFuQ2lCO0FBd0MzQyxtQ0FBWSxTQUFTLFlBQVk7Ozs7Ozs7OztlQU9LLEtBQUssS0FBTCxDQVBMO01BTzNCLGlDQVAyQjtNQU9kLHlDQVBjO01BUzFCLGtCQUE4RCxnQkFBOUQsZ0JBVDBCO01BU1QsYUFBNkMsZ0JBQTdDLFdBVFM7TUFTRyxrQkFBaUMsZ0JBQWpDLGdCQVRIO01BU29CLGNBQWdCLGdCQUFoQixZQVRwQjs7QUFVaEMsTUFBSSxjQUFlLGNBQWMsZUFBZCxJQUFpQyxnQkFBZ0IsTUFBaEIsR0FBeUIsQ0FBekIsQ0FWcEI7QUFXaEMsTUFBSSxlQUFlLGdCQUFnQixXQUFoQixDQUFmLENBWDRCOztBQWFoQyxNQUFJLHlCQUFKO01BQW1CLHVCQUFuQjtNQUFnQyx1QkFBaEM7TUFBNkMsdUJBQTdDO01BQTBELHNCQUExRCxDQWJnQztBQWNoQyxNQUFJLHFCQUFxQixTQUFyQixDQWQ0Qjs7QUFnQmhDLFVBQU8sUUFBUSxXQUFSLEVBQVA7O0FBRUMsUUFBSyxTQUFMO0FBQ0Msb0JBQWdCLEtBQUssZUFBTCxDQUFxQixVQUFyQixFQUFpQyxJQUFqQyxDQUFoQixDQUREO0FBRUMsb0JBQWdCLEtBQUssY0FBTCxDQUFvQixhQUFwQixDQUFoQixDQUZEOztBQUlDLGtCQUFjLEtBQUssZUFBTCxDQUFxQixVQUFyQixFQUFpQyxJQUFqQyxDQUFkLENBSkQ7O0FBTUMsa0JBQ0M7O09BQU0sV0FBVSxPQUFWLEVBQWtCLGVBQVksU0FBWixFQUFzQixhQUFVLE1BQVYsRUFBaUIsa0JBQWUsT0FBZixFQUF1QixPQUFNLDJIQUFOLEVBQXRGOztLQURELENBTkQ7O0FBV0Msa0JBQWMsWUFBWSxXQUFaLENBQXdCLEdBQXhCLENBQTZCLFVBQUMsSUFBRCxFQUFPLEdBQVAsRUFBZTtTQUV4RCxTQVlHLEtBWkgsT0FGd0Q7U0FHeEQsU0FXRyxLQVhILE9BSHdEO1NBSXhELGVBVUcsS0FWSCxhQUp3RDtTQUt4RCxjQVNHLEtBVEgsWUFMd0Q7U0FNeEQsWUFRRyxLQVJILFVBTndEO1NBT3hELE1BT0csS0FQSCxJQVB3RDtTQVF4RCxNQU1HLEtBTkgsSUFSd0Q7U0FTeEQsZ0JBS0csS0FMSCxjQVR3RDtTQVV4RCxZQUlHLEtBSkgsVUFWd0Q7U0FXeEQsYUFHRyxLQUhILFdBWHdEO1NBWXhELGNBRUcsS0FGSCxZQVp3RDtTQWF4RCxlQUNHLEtBREgsYUFid0Q7OztBQWlCekQsU0FBSSxjQUFjO0FBQ2pCLGNBQVEsTUFBUjtBQUNBLGNBQVEsTUFBUjtBQUNBLGVBQVksbUJBQVo7QUFDQSxjQUFXLGtCQUFYO01BSkcsQ0FqQnFEOztBQTBCekQsU0FBRyxXQUFILEVBQWdCO0FBQ2YsMkJBQXFCLENBQ3BCO0FBQ0MsYUFBTSxPQUFOO0FBQ0EsYUFBTSxnQkFBZ0IsYUFBYSxNQUFiO0FBQ3RCLGFBQU0sR0FBTjtPQUptQixDQUFyQixDQURlO01BQWhCOztBQVVBLFNBQUksZUFBZ0IsZ0JBQWdCLEdBQWhCLENBcENxQzs7QUFzQ3pELFlBQ0Msb0JBQUMsV0FBRDtBQUNDLHVCQUFlLFlBQVksTUFBTSxHQUFqQztBQUNBLFlBQUssZUFBTDtBQUNBLGdCQUFVLE1BQVY7QUFDQSxnQkFBVSxZQUFWO0FBQ0EsbUJBQWEsaUJBQWlCLGFBQWpCLENBQWI7QUFDQSxjQUFPLEdBQVA7QUFDQSxZQUFLLEdBQUw7QUFDQSxvQkFBYyxTQUFkO0FBQ0EsbUJBQWEsV0FBYjtBQUNBLGlCQUFXLFVBQVg7QUFDQSwyQkFBb0IscUJBQXBCO0FBQ0EsMEJBQW9CLGtCQUFwQjtBQUNBLGFBQU8sR0FBUDtBQUNBLG9CQUFjLFlBQWQ7QUFDQSwyQkFBcUIsTUFBSyxtQkFBTCxDQUF5QixJQUF6QixDQUE4QixJQUE5QixFQUFvQyxZQUFwQyxFQUFrRCxJQUFsRCxFQUF3RCxTQUF4RCxFQUFtRSxHQUFuRSxDQUFyQjtBQUNBLHFCQUFlLElBQWY7TUFoQkQsQ0FERCxDQXRDeUQ7S0FBZixDQUEzQyxDQVhEOztBQXVFQyxVQXZFRDs7QUFGRCxRQTJFTSxNQUFMO0FBQ0Msb0JBQWdCLEtBQUssZUFBTCxDQUFxQixVQUFyQixFQUFpQyxJQUFqQyxDQUFoQixDQUREO0FBRUMsb0JBQWdCLEtBQUssY0FBTCxDQUFvQixhQUFwQixDQUFoQixDQUZEOztBQUlDLGtCQUFjLEtBQUssZUFBTCxDQUFxQixVQUFyQixFQUFpQyxJQUFqQyxDQUFkLENBSkQ7O0FBT0Msa0JBQWMsWUFBWSxXQUFaLENBQXdCLEdBQXhCLENBQTZCLFVBQUMsSUFBRCxFQUFPLEdBQVAsRUFBZTtTQUV4RCxnQkFXRyxLQVhILGNBRndEO1NBR3hELGNBVUcsS0FWSCxZQUh3RDtTQUl4RCxjQVNHLEtBVEgsWUFKd0Q7U0FLeEQsS0FRRyxLQVJILEdBTHdEO1NBTXhELFNBT0csS0FQSCxPQU53RDtTQU94RCxjQU1HLEtBTkgsWUFQd0Q7U0FReEQsWUFLRyxLQUxILFVBUndEO1NBU3hELGFBSUcsS0FKSCxXQVR3RDtTQVV4RCxjQUdHLEtBSEgsWUFWd0Q7U0FXeEQsWUFFRyxLQUZILFVBWHdEO1NBWXhELGVBQ0csS0FESCxhQVp3RDs7O0FBZXpELFNBQUksY0FBYztBQUNqQixhQUFVLGlCQUFpQixLQUFLLGFBQUwsc0JBQTNCO0FBQ0EsY0FBVyxLQUFLLFdBQUwsT0FBWDtBQUNBLGdCQUFVLEtBQUssV0FBTDtNQUhQLENBZnFEOztBQXNCekQsU0FBRyxXQUFILEVBQWdCO0FBQ2YsMkJBQXFCLENBQ3BCO0FBQ0MsYUFBTSxRQUFOO0FBQ0EsYUFBTSxjQUFjLGFBQWEsV0FBYjtBQUNwQixhQUFNLEdBQU47T0FKbUIsQ0FBckIsQ0FEZTtNQUFoQjs7QUFVQSxTQUFJLGVBQWdCLGdCQUFnQixHQUFoQixDQWhDcUM7O0FBa0N6RCxZQUNDLG9CQUFDLFdBQUQ7QUFDQyxvQkFBWSxLQUFLLEVBQUw7QUFDWixZQUFLLFlBQUw7QUFDQSxnQkFBVSxLQUFLLE1BQUw7QUFDVixnQkFBVSxZQUFWO0FBQ0EsdUJBQWlCLG1PQUFqQjtBQUNBLG1CQUFhLEtBQUssV0FBTDtBQUNiLFlBQUssR0FBTDtBQUNBLG9CQUFjLEtBQUssU0FBTDtBQUNkLG1CQUFhLFdBQWI7QUFDQSxpQkFBVyxLQUFLLFVBQUw7QUFDWCwyQkFBb0IscUJBQXBCO0FBQ0EsMEJBQW9CLGtCQUFwQjtBQUNBLGFBQU8sR0FBUDtBQUNBLG9CQUFjLFlBQWQ7QUFDQSwyQkFBcUIsTUFBSyxtQkFBTCxDQUF5QixJQUF6QixDQUE4QixJQUE5QixFQUFvQyxZQUFwQyxFQUFrRCxJQUFsRCxFQUF3RCxTQUF4RCxFQUFtRSxHQUFuRSxDQUFyQjtBQUNBLHFCQUFlLElBQWY7TUFoQkQsQ0FERCxDQWxDeUQ7S0FBZixDQUEzQyxDQVBEOztBQStEQyxVQS9ERDtBQTNFRCxRQTJJTSxTQUFMO0FBQ0Msb0JBQWdCLEtBQUssZUFBTCxDQUFxQixVQUFyQixFQUFpQyxJQUFqQyxDQUFoQixDQUREO0FBRUMsb0JBQWdCLEtBQUssY0FBTCxDQUFvQixhQUFwQixDQUFoQixDQUZEOztBQUlDLGtCQUFjLEtBQUssZUFBTCxDQUFxQixVQUFyQixFQUFpQyxJQUFqQyxDQUFkOzs7Ozs7QUFKRCxlQVVDLEdBQWMsWUFBWSxXQUFaLENBQXdCLEdBQXhCLENBQTZCLFVBQUMsSUFBRCxFQUFPLEdBQVAsRUFBZTtTQUV4RCxZQWFHLEtBYkgsVUFGd0Q7U0FHeEQsUUFZRyxLQVpILE1BSHdEO1NBSXhELGdCQVdHLEtBWEgsY0FKd0Q7U0FLeEQsY0FVRyxLQVZILFlBTHdEO1NBTXhELGNBU0csS0FUSCxZQU53RDtTQU94RCxLQVFHLEtBUkgsR0FQd0Q7U0FReEQsV0FPRyxLQVBILFNBUndEO1NBU3hELGdCQU1HLEtBTkgsY0FUd0Q7U0FVeEQsYUFLRyxLQUxILFdBVndEO1NBV3hELGFBSUcsS0FKSCxXQVh3RDtTQVl4RCxjQUdHLEtBSEgsWUFad0Q7U0FheEQsWUFFRyxLQUZILFVBYndEO1NBY3hELGVBQ0csS0FESCxhQWR3RDs7O0FBa0J6RCxTQUFJLFNBQVMsS0FBQyxLQUFVLElBQVYsR0FBa0IsS0FBbkIsR0FBMkIsTUFBM0IsQ0FsQjRDOztBQW9CekQsU0FBSSxjQUFjLEVBQWQsQ0FwQnFEOztBQXNCekQsaUJBQWUsY0FBZixJQUFpQyxpQkFBaUIsS0FBSyxhQUFMLFFBQWxELENBdEJ5RDs7QUF3QnpELG1CQUFjLFNBQWMsV0FBZCxFQUEyQjs7QUFFeEMsY0FBVyxrQkFBWDtBQUNBLGdCQUFVLFdBQVY7QUFDQSxlQUNDOzs7QUFDQyxpQkFBUyxpQkFBQyxDQUFELEVBQU87QUFDZCxXQUFFLGVBQUYsR0FEYztBQUVkLGFBQUksYUFBYSxFQUFFLGVBQUYsQ0FBYixDQUZVOztBQUlkLG9CQUFXLFVBQVgsQ0FBc0IsVUFBdEIsRUFKYztBQUtkLG9CQUFXLFFBQVgsQ0FBb0IsTUFBcEIsRUFMYzs7QUFPZCxvQkFDRSxLQURGLEdBRUUsSUFGRixDQUVPLGFBRlAsRUFHRSxJQUhGLENBR08sVUFIUCxFQUdtQixVQUFDLFlBQUQsRUFBZSxVQUFmLEVBQThCOztBQUUvQyxjQUFJLGVBQWUsU0FBZixJQUE0QixlQUFlLGFBQWYsRUFBK0I7QUFDOUQsc0JBQVcsV0FBWCxDQUF1QixJQUF2QixFQUQ4RDtBQUU5RCxzQkFBVyxXQUFYLENBQXVCLE1BQXZCLEVBRjhEOztBQUk5RCxzQkFBVyxZQUFNO0FBQUUsdUJBQVcsUUFBWCxDQUFvQixJQUFwQixFQUFGO1lBQU4sRUFBc0MsRUFBakQsRUFKOEQ7V0FBL0QsTUFLTztBQUNOLDJCQUFnQixLQUFoQixDQUFzQixNQUF0QixFQURNO1dBTFA7VUFGaUIsQ0FIbkIsQ0FQYztTQUFQLEVBRFY7O09BREQ7TUFKYSxDQUFkLENBeEJ5RDs7QUF5RHpELFNBQUcsV0FBSCxFQUFnQjtBQUNmLDJCQUFxQixDQUNwQjtBQUNDLGFBQU0sU0FBTjtBQUNBLGFBQU0sZ0JBQWdCLGFBQWEsYUFBYjtBQUN0QixhQUFNLEdBQU47T0FKbUIsQ0FBckIsQ0FEZTtNQUFoQjs7QUFVQSxTQUFJLGVBQWdCLGdCQUFnQixHQUFoQixDQW5FcUM7O0FBcUV6RCxZQUNDLG9CQUFDLFdBQUQ7QUFDQyx1QkFBZSxFQUFmO0FBQ0EsWUFBSyxrQkFBTDtBQUNBLGdCQUFVLFFBQVY7QUFDQSxnQkFBVSxZQUFWO0FBQ0EsdUJBQWlCLG9NQUFqQjtBQUNBLG1CQUFhLGFBQWI7QUFDQSxZQUFLLEdBQUw7QUFDQSxvQkFBYyxTQUFkO0FBQ0EsbUJBQWEsV0FBYjtBQUNBLGlCQUFXLFVBQVg7QUFDQSwyQkFBb0IscUJBQXBCO0FBQ0EsMEJBQW9CLGtCQUFwQjtBQUNBLGFBQU8sR0FBUDtBQUNBLG9CQUFjLFlBQWQ7QUFDQSwyQkFBcUIsTUFBSyxtQkFBTCxDQUF5QixJQUF6QixDQUE4QixJQUE5QixFQUFvQyxZQUFwQyxFQUFrRCxJQUFsRCxFQUF3RCxTQUF4RCxFQUFtRSxHQUFuRSxDQUFyQjtBQUNBLHFCQUFlLEtBQWY7TUFoQkQsQ0FERCxDQXJFeUQ7S0FBZixDQUEzQyxDQVZEOztBQXFHQyxpQkFDQzs7T0FBSyxXQUFVLFlBQVYsRUFBdUIsSUFBRyxjQUFILEVBQWtCLE1BQUssUUFBTCxFQUFjLG1CQUFnQixjQUFoQixFQUErQixlQUFZLE1BQVosRUFBM0Y7S0FDQzs7UUFBSyxXQUFVLGNBQVYsRUFBTDtNQUNJOztTQUFLLFdBQVUsMkJBQVYsRUFBTDtPQUNJLGdDQUFRLFdBQVUsT0FBVixFQUFrQixnQkFBYSxPQUFiLEVBQXFCLGNBQVcsT0FBWCxFQUEvQyxDQURKO09BRUk7O1VBQUssV0FBVSxZQUFWLEVBQUw7O1FBRko7T0FESjtNQUREO0tBREQsQ0FyR0Q7O0FBa0hDLFVBbEhEO0FBM0lELFFBOFBNLEtBQUw7QUFDQyxvQkFBZ0IsS0FBSyxlQUFMLENBQXFCLFVBQXJCLEVBQWlDLElBQWpDLENBQWhCLENBREQ7QUFFQyxvQkFBZ0IsS0FBSyxjQUFMLENBQW9CLGFBQXBCLENBQWhCLENBRkQ7O0FBSUMsa0JBQWMsS0FBSyxlQUFMLENBQXFCLFVBQXJCLEVBQWlDLElBQWpDLENBQWQ7Ozs7OztBQUpELGVBVUMsR0FBYyxZQUFZLFdBQVosQ0FBd0IsR0FBeEIsQ0FBNkIsVUFBQyxJQUFELEVBQU8sR0FBUCxFQUFlO1NBRXhELGdCQVFHLEtBUkgsY0FGd0Q7U0FHeEQsYUFPRyxLQVBILFdBSHdEO1NBSXhELFNBTUcsS0FOSCxPQUp3RDtTQUt4RCxjQUtHLEtBTEgsWUFMd0Q7U0FNeEQsWUFJRyxLQUpILFVBTndEO1NBT3hELGVBR0csS0FISCxhQVB3RDtTQVF4RCxPQUVHLEtBRkgsS0FSd0Q7U0FTeEQsV0FDRyxLQURILFNBVHdEOzs7QUFZekQsU0FBRyxXQUFILEVBQWdCO0FBQ2YsMkJBQXFCLENBQ3BCO0FBQ0MsYUFBTSxPQUFOO0FBQ0EsYUFBTSxnQkFBZ0IsYUFBYSxNQUFiO0FBQ3RCLGFBQU0sR0FBTjtPQUptQixDQUFyQixDQURlO01BQWhCOztBQVVBLFNBQUksZUFBZ0IsZ0JBQWdCLEdBQWhCLENBdEJxQzs7QUF3QnpEOztBQUVDLDBCQUFDLFdBQUQ7QUFDQyxxQkFBWSxHQUFaO0FBQ0EsYUFBSyxPQUFMO0FBQ0EsaUJBQVUsTUFBVjtBQUNBLGlCQUFVLFlBQVY7QUFDQSxvQkFBYSxpQkFBaUIsS0FBSyxhQUFMLENBQTlCO0FBQ0EsZUFBTyxHQUFQO0FBQ0EsYUFBSyxHQUFMO0FBQ0EscUJBQWMsS0FBSyxVQUFMO0FBQ2QsMkJBQW9CLGtCQUFwQjtBQUNBLGNBQU8sR0FBUDtBQUNBLHFCQUFjLFlBQWQ7QUFDQSxvQkFBYSxJQUFiO0FBQ0EsNEJBQXFCLE1BQUssbUJBQUwsQ0FBeUIsSUFBekIsQ0FBOEIsSUFBOUIsRUFBb0MsWUFBcEMsRUFBa0QsSUFBbEQsRUFBd0QsU0FBeEQsRUFBbUUsR0FBbkUsQ0FBckI7QUFDQSxzQkFBZSxJQUFmO0FBQ0Esb0JBQWEsUUFBYjtPQWZELENBRkQ7T0F4QnlEO0tBQWYsQ0FBM0MsQ0FWRDtBQXVEQyxVQXZERDs7QUE5UEQ7R0FoQmdDOztBQTBVaEMsU0FBTyxFQUFDLDRCQUFELEVBQWdCLHdCQUFoQixFQUE2Qix3QkFBN0IsRUFBMEMsd0JBQTFDLEVBQXVELHNCQUF2RCxFQUFQLENBMVVnQztFQXhDVTtBQXFYM0MsNkNBQWlCLFNBQVMsVUFBVSxpQkFBaUI7QUFDcEQsTUFBSSw2QkFBSixDQURvRDs7QUFHcEQsVUFBTyxRQUFRLFdBQVIsRUFBUDtBQUNDLFFBQUssU0FBTDs7QUFFQyxVQUZEO0FBREQsUUFJTSxLQUFMO0FBQ0Msd0JBQ0M7O09BQUssV0FBVSxVQUFWLEVBQUw7S0FDQzs7UUFBTSxXQUFVLGFBQVYsRUFBTjs7TUFERDtLQUNvRjs7UUFBTSxXQUFVLFFBQVYsRUFBTjs7TUFEcEY7S0FFQzs7UUFBTyxTQUFRLG1CQUFSLEVBQVA7TUFDQzs7OztPQUREO01BRUM7OztPQUNDLDZCQUFLLE9BQU0sUUFBTixFQUFMLENBREQ7T0FFQyw2QkFBSyxPQUFNLFFBQU4sRUFBTCxDQUZEO09BRkQ7TUFNQzs7O09BQ0M7OztRQUNDOztXQUFJLE9BQU0sS0FBTixFQUFKOztTQUREO1FBRUM7O1dBQUksT0FBTSxLQUFOLEVBQUo7O1NBRkQ7UUFERDtPQU5EO01BWUM7OztPQUNDOzs7UUFDQzs7O1NBQUksNkJBQUssS0FBSSw4Q0FBSixFQUFtRCxLQUFJLE1BQUosRUFBVyxPQUFNLEtBQU4sRUFBbkUsQ0FBSjtTQUREO1FBRUM7O1dBQUksV0FBVSxVQUFWLEVBQUo7U0FDQzs7OztVQUMwQiwrQkFEMUI7O1VBRXFCLCtCQUZyQjs7VUFHZ0MsK0JBSGhDO1VBREQ7U0FGRDtRQUREO09BV0M7OztRQUNDOzs7U0FBSSw2QkFBSyxLQUFJLDhDQUFKLEVBQW1ELEtBQUksTUFBSixFQUFXLE9BQU0sS0FBTixFQUFuRSxDQUFKO1NBREQ7UUFFQzs7V0FBSSxXQUFVLFVBQVYsRUFBSjtTQUNDOzs7O1VBQ21DLCtCQURuQzs7VUFFa0MsK0JBRmxDOztVQUc4QiwrQkFIOUI7VUFERDtTQUZEO1FBWEQ7T0FxQkM7OztRQUNDOzs7U0FBSSw2QkFBSyxLQUFJLDhDQUFKLEVBQW1ELEtBQUksTUFBSixFQUFXLE9BQU0sS0FBTixFQUFuRSxDQUFKO1NBREQ7UUFFQzs7V0FBSSxXQUFVLFVBQVYsRUFBSjtTQUNDOzs7O1VBQzRCLCtCQUQ1Qjs7VUFFd0IsK0JBRnhCOztVQUdnQiwrQkFIaEI7VUFERDtTQUZEO1FBckJEO09BK0JDOzs7UUFDQzs7O1NBQUksNkJBQUssS0FBSSw4Q0FBSixFQUFtRCxLQUFJLFFBQUosRUFBYSxPQUFNLEtBQU4sRUFBckUsQ0FBSjtTQUREO1FBRUM7O1dBQUksV0FBVSxVQUFWLEVBQUo7U0FDQzs7OztVQUN5QiwrQkFEekI7O1VBRW1CLCtCQUZuQjs7VUFHNEIsK0JBSDVCO1VBREQ7U0FGRDtRQS9CRDtPQXlDQzs7O1FBQ0M7OztTQUFJLDZCQUFLLEtBQUksOENBQUosRUFBbUQsS0FBSSxRQUFKLEVBQWEsT0FBTSxLQUFOLEVBQXJFLENBQUo7U0FERDtRQUVDOztXQUFJLFdBQVUsVUFBVixFQUFKO1NBQ0M7Ozs7VUFDb0IsK0JBRHBCOztVQUVzQiwrQkFGdEI7O1VBRzBCLCtCQUgxQjtVQUREO1NBRkQ7UUF6Q0Q7T0FtREM7OztRQUNDOzs7U0FBSSw2QkFBSyxLQUFJLDhDQUFKLEVBQW1ELEtBQUksT0FBSixFQUFZLE9BQU0sS0FBTixFQUFwRSxDQUFKO1NBREQ7UUFFQzs7V0FBSSxXQUFVLFVBQVYsRUFBSjtTQUNDOzs7O1VBQzRCLCtCQUQ1Qjs7VUFFOEIsK0JBRjlCO1VBREQ7U0FGRDtRQW5ERDtPQTREQzs7O1FBQ0M7OztTQUFJLDZCQUFLLEtBQUksOENBQUosRUFBbUQsS0FBSSxNQUFKLEVBQVcsT0FBTSxLQUFOLEVBQW5FLENBQUo7U0FERDtRQUVDOztXQUFJLFdBQVUsVUFBVixFQUFKO1NBQ0M7Ozs7VUFDcUIsK0JBRHJCOztVQUVvQywrQkFGcEM7O1VBR3lCLCtCQUh6QjtVQUREO1NBRkQ7UUE1REQ7T0FzRUM7OztRQUNDOzs7U0FBSSw2QkFBSyxLQUFJLDhDQUFKLEVBQW1ELEtBQUksUUFBSixFQUFhLE9BQU0sS0FBTixFQUFyRSxDQUFKO1NBREQ7UUFFQzs7V0FBSSxXQUFVLFVBQVYsRUFBSjtTQUNDOzs7O1VBQ2lCLCtCQURqQjs7VUFFdUIsK0JBRnZCOztVQUd5QiwrQkFIekI7VUFERDtTQUZEO1FBdEVEO09BZ0ZDOzs7UUFDQzs7O1NBQUksNkJBQUssS0FBSSw4Q0FBSixFQUFtRCxLQUFJLFNBQUosRUFBYyxPQUFNLEtBQU4sRUFBdEUsQ0FBSjtTQUREO1FBRUM7O1dBQUksV0FBVSxVQUFWLEVBQUo7U0FDQzs7OztVQUNvQywrQkFEcEM7O1VBRWlDLCtCQUZqQztVQUREO1NBRkQ7UUFoRkQ7T0F5RkM7OztRQUNDOzs7U0FBSSw2QkFBSyxLQUFJLDhDQUFKLEVBQW1ELEtBQUksU0FBSixFQUFjLE9BQU0sS0FBTixFQUF0RSxDQUFKO1NBREQ7UUFFQzs7V0FBSSxXQUFVLFVBQVYsRUFBSjtTQUNDOzs7O1VBQ2lDLCtCQURqQzs7VUFFK0IsK0JBRi9CO1VBREQ7U0FGRDtRQXpGRDtPQWtHQzs7O1FBQ0M7OztTQUFJLDZCQUFLLEtBQUksOENBQUosRUFBbUQsS0FBSSxRQUFKLEVBQWEsT0FBTSxLQUFOLEVBQXJFLENBQUo7U0FERDtRQUVDOztXQUFJLFdBQVUsVUFBVixFQUFKO1NBQ0M7Ozs7VUFDMEIsK0JBRDFCOztVQUU2QiwrQkFGN0I7VUFERDtTQUZEO1FBbEdEO09BWkQ7TUFGRDtLQTJIRyxRQTNISDtLQTRIRyxlQTVISDtLQURELENBREQ7QUFrSUMsVUFsSUQ7QUFKRDs7R0FIb0Q7O0FBOElwRCxTQUFPLGlCQUFQLENBOUlvRDtFQXJYVjtDQUFsQixDQUF0Qjs7QUEwZ0JKLE9BQU8sT0FBUCxHQUFpQixtQkFBakI7Ozs7Ozs7Ozs7O0FDaGhCQSxJQUFJLFFBQVUsUUFBUSxPQUFSLENBQVY7SUFDSCxpQkFBaUIsUUFBUSw0Q0FBUixDQUFqQjtJQUNBLFlBQWEsUUFBUSx1Q0FBUixDQUFiO0lBQ0EsTUFBUyxRQUFRLHdCQUFSLENBQVQ7SUFDQSxnQkFBZ0IsUUFBUSwwQ0FBUixFQUFvRCxhQUFwRDtJQUNoQixnQkFBZ0IsUUFBUSxvQ0FBUixDQUFoQjtJQUNBLGlCQUFpQixRQUFRLG1DQUFSLENBQWpCOztJQUVLLGlCQUFtQyxlQUFuQztJQUFnQixpQkFBbUIsZUFBbkI7OztBQUV0QixJQUFNLGdCQUFnQixRQUFRLHNDQUFSLENBQWhCOztJQUlBLGFBQ0wsU0FESyxVQUNMLENBQVksS0FBWixFQUFtQixLQUFuQixFQUEwQixJQUExQixFQUFnQzt1QkFEM0IsWUFDMkI7O0FBQy9CLE1BQUssS0FBTCxHQUFhLEtBQWIsQ0FEK0I7QUFFL0IsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUYrQjtBQUcvQixNQUFLLElBQUwsR0FBWSxJQUFaLENBSCtCO0NBQWhDOztBQVFELElBQUkscUJBQXFCLE1BQU0sV0FBTixDQUFrQjs7O0FBQ3ZDLFlBQVc7QUFDYixXQUFTLE1BQU0sU0FBTixDQUFnQixNQUFoQjtBQUNULGlCQUFlLE1BQU0sU0FBTixDQUFnQixLQUFoQjtBQUNmLGNBQVksTUFBTSxTQUFOLENBQWdCLE1BQWhCO0VBSFY7O0FBTUEsNkNBQWtCO0FBQ2QsU0FBTztBQUNaLFlBQVMsRUFBVDtBQUNBLGtCQUFlLEVBQWY7QUFDQSxlQUFZLEVBQVo7R0FISyxDQURjO0VBUHFCOzs7QUFlMUMsa0JBQWlCLDJCQUFXO0FBQ3JCLFNBQU87QUFDWixtQkFBZ0IsS0FBaEI7QUFDQSxVQUFPO0FBQ04sZUFBVyxFQUFYO0lBREQ7QUFHQSxnQkFBYSxFQUFiO0FBQ0EsaUJBQWMsRUFBZDtBQUNBLGNBQVcsRUFBWDtBQUNBLFVBQU8sRUFBUDtBQUNBLHdCQUFxQixlQUFyQjtHQVRLLENBRHFCO0VBQVg7O0FBZWQsaURBQW9CO0FBQ3RCLE1BQUksVUFBVSxjQUFjLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FBeEIsQ0FEa0I7O0FBR3RCLE1BQUksSUFBSixDQUFTLDhDQUFULEVBQXlELEVBQUMsZ0JBQUQsRUFBekQsRUFDRSxJQURGLENBQ08sVUFBVSxHQUFWLEVBQWU7QUFDcEIsT0FBRyxJQUFJLE1BQUosQ0FBVyxVQUFYLEtBQTBCLFNBQTFCLEVBQXFDO0FBQ3ZDLFFBQUksYUFBYSxJQUFJLE1BQUosQ0FBVyxJQUFYLENBRHNCO0FBRXZDLFFBQUksVUFBWSxXQUFXLEdBQVgsQ0FBZSxVQUFDLENBQUQ7WUFBTyxJQUFJLFVBQUosQ0FBZSxFQUFFLFVBQUYsRUFBYyxFQUFFLFlBQUY7S0FBcEMsQ0FBM0IsQ0FGbUM7O0FBSXZDLFNBQUssUUFBTCxDQUFjO0FBQ2Isa0JBQWEsT0FBYjtLQURELEVBSnVDO0lBQXhDO0dBREssQ0FTSixJQVRJLENBU0MsSUFURCxDQURQOzs7QUFIc0IsR0FnQnRCLENBQUUsb0JBQUYsRUFBd0IsRUFBeEIsQ0FBMkIsaUJBQTNCLEVBQThDLEtBQUssVUFBTCxDQUE5QyxDQWhCc0I7RUE5Qm1CO0FBbUR2Qyx1REFBdUIsRUFuRGdCO0FBdUR2QywyQkFBUzs7O2VBQ3VFLEtBQUssS0FBTCxDQUR2RTtNQUNMLHFCQURLO01BQ0UsaUNBREY7TUFDZSxtQ0FEZjtNQUM2QixxQkFEN0I7TUFDb0MsNkJBRHBDO01BQytDLGlEQUQvQztlQUVrQyxLQUFLLEtBQUwsQ0FGbEM7TUFFTCx5QkFGSztNQUVJLHFDQUZKO01BRW1CLCtCQUZuQjs7O0FBSVgsTUFBSSxjQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDs7S0FERDtJQUVDLG9CQUFDLGNBQUQ7QUFDQyxrQkFBWSxJQUFaO0FBQ0EsY0FBUyxXQUFUO0FBQ0EsbUJBQWMsS0FBSyxpQkFBTCxDQUF1QixJQUF2QixDQUE0QixJQUE1QixFQUFrQyxjQUFsQyxFQUFrRCxJQUFsRCxDQUFkO0FBQ0EsZUFBVSxNQUFNLFlBQU47QUFDVixtQkFBYyxLQUFLLEtBQUwsQ0FBVyxjQUFYLElBQTZCLGNBQTdCO0tBTGYsQ0FGRDtJQUREO0dBREcsQ0FKTzs7QUFtQlgsTUFBSSxhQUFhLGNBQWMsR0FBZCxDQUFtQixVQUFDLEtBQUQsRUFBVztBQUM5QyxPQUFJLHFCQUFKLENBRDhDOztBQUc5QyxPQUFHLE1BQU0sRUFBTixLQUFhLGNBQWIsRUFBNkI7QUFDL0IsUUFBRyxNQUFNLEdBQU4sRUFBVztBQUNiLFNBQUksY0FBYyxXQUFXLElBQVgsRUFBaUIsSUFBakIsQ0FBc0IsVUFBQyxDQUFEO2FBQU8sRUFBRSxFQUFGLElBQVEsTUFBTSxHQUFOO01BQWYsQ0FBcEM7OztBQURTLFNBSVYsZUFBZSxZQUFZLElBQVosRUFBa0I7QUFDbkMsVUFBSSxPQUFPLEVBQVAsQ0FEK0I7QUFFbkMsVUFBSSxVQUFVLEtBQUssS0FBTCxRQUFnQixZQUFZLElBQVosT0FBaEIsQ0FBVixDQUYrQjs7QUFJbkMsV0FBSyxJQUFJLEVBQUosSUFBVSxPQUFmLEVBQXdCO0FBQ3ZCLFlBQUssSUFBTCxDQUFVO0FBQ1QsZUFBTyxRQUFRLEVBQVIsQ0FBUDtBQUNBLGVBQU8sRUFBUDtRQUZELEVBRHVCO09BQXhCO0FBTUEsWUFBTSxPQUFOLEdBQWdCLElBQWhCLENBVm1DO01BQXBDO0tBSkQ7SUFERDs7QUFvQkEsV0FBTyxNQUFNLElBQU47QUFDTixTQUFLLFFBQUwsQ0FERDtBQUVDLFNBQUssb0JBQUwsQ0FGRDtBQUdDLFNBQUssTUFBTDtBQUNDLGlCQUFZLG9CQUFDLFNBQUQ7QUFDUixZQUFNLE1BQU0sSUFBTjtBQUNOLG1CQUFhLE1BQU0sV0FBTixJQUFxQixHQUFyQjtBQUNiLGtCQUFXLG1CQUFYO0FBQ0Esb0JBQWMsTUFBSyxxQkFBTCxDQUEyQixJQUEzQixDQUFnQyxJQUFoQyxFQUFzQyxNQUFNLEVBQU4sQ0FBcEQ7QUFDQSxpQkFBVyxNQUFNLFNBQU47QUFDWCxhQUFPLE1BQU0sTUFBTSxFQUFOLENBQWI7QUFDQSxZQUFNLE1BQU0sSUFBTixJQUFjLEVBQWQ7QUFDTixvQkFBYyxNQUFLLEtBQUwsQ0FBVyxjQUFYLElBQTZCLE1BQU0sWUFBTjtNQVJuQyxDQUFaLENBREQ7QUFXQyxXQVhEO0FBSEQsU0FlTSxVQUFMO0FBQ0MsaUJBQVksb0JBQUMsU0FBRDtBQUNSLG1CQUFZLEdBQVo7QUFDQSxrQkFBVyxtQkFBWDtBQUNBLGlCQUFXLE1BQU0sU0FBTjtBQUNYLGFBQU8sSUFBUDtBQUNBLGFBQU8sTUFBTSxPQUFOO0FBQ1AsWUFBTSxNQUFNLElBQU4sSUFBYyxFQUFkLEVBTkUsQ0FBWixDQUREO0FBUUMsV0FSRDtBQWZELFNBd0JNLFFBQUw7QUFDQyxpQkFBWSxvQkFBQyxjQUFEO0FBQ1IsbUJBQWEsTUFBTSxXQUFOLElBQXFCLElBQXJCO0FBQ2IsZUFBUyxNQUFNLE9BQU47QUFDVCxvQkFBYyxNQUFLLGlCQUFMLENBQXVCLElBQXZCLENBQTRCLElBQTVCLEVBQWtDLE1BQU0sRUFBTixFQUFVLE1BQU0sWUFBTixDQUExRDtBQUNBLGlCQUFXLE1BQU0sU0FBTixJQUFtQixpQkFBbkI7QUFDWCxnQkFBVSxNQUFNLE1BQU0sRUFBTixDQUFoQjtBQUNBLG9CQUFjLE1BQUssS0FBTCxDQUFXLGNBQVgsSUFBNkIsTUFBTSxZQUFOO01BTm5DLENBQVosQ0FERDtBQVNDLFdBVEQ7QUF4QkQ7QUFtQ0UsaUJBQVksU0FBWixDQUREO0FBbENELElBdkI4Qzs7QUE2RDlDLFVBQ0M7O01BQUssV0FBVSxLQUFWLEVBQWdCLGdCQUFjLE1BQU0sS0FBTixFQUFuQztJQUNDOztPQUFLLFdBQVUsVUFBVixFQUFMO0tBQ0M7O1FBQU8sV0FBVSxjQUFWLEVBQVA7TUFBaUMsTUFBTSxLQUFOLElBQWUsRUFBZjtNQURsQztLQUVHLFNBRkg7S0FERDtJQURELENBN0Q4QztHQUFYLENBQWhDLENBbkJPOztBQTBGWCxNQUFJLGtCQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDs7S0FERDtJQUVDLG9CQUFDLGNBQUQ7QUFDQyxrQkFBYSxtQkFBYjtBQUNBLGNBQVMsWUFBVDtBQUNBLG1CQUFjLEtBQUssdUJBQUw7QUFDZCxlQUFVLFNBQVY7QUFDQSxtQkFBYyxLQUFLLEtBQUwsQ0FBVyxjQUFYLElBQTZCLGNBQTdCO0tBTGYsQ0FGRDtJQUREO0dBREcsQ0ExRk87O0FBeUdYLE1BQUksYUFBYSxRQUFRLFdBQVIsT0FBMEIsS0FBMUIsR0FBa0MsS0FBbEMsR0FBMEMsT0FBMUMsQ0F6R047QUEwR1gsTUFBSSxZQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDtLQUFpQyxVQUFqQztLQUREO0lBRUMsb0JBQUMsU0FBRDtBQUNDLFdBQUssb0JBQUw7QUFDQSxrQkFBWSxHQUFaO0FBQ0EsaUJBQVcsbUJBQVg7QUFDQSxtQkFBYyxLQUFLLGlCQUFMO0FBQ2QsWUFBTyxLQUFQO0FBQ0EsV0FBSyxHQUFMO0FBQ0EsbUJBQWMsS0FBSyxLQUFMLENBQVcsY0FBWCxJQUE2QixjQUE3QjtLQVBmLENBRkQ7SUFERDtHQURHLENBMUdPOztBQTZIWCxTQUNDOztLQUFLLDJCQUF5QixRQUFRLFdBQVIsRUFBekIsRUFBa0QsSUFBRyxtQkFBSCxFQUF2RDtHQUNDOztNQUFLLFdBQVUsY0FBVixFQUFMO0lBQ0M7O09BQUssV0FBVSwyQkFBVixFQUFMO0tBRUMsMkJBQUcsV0FBVSxPQUFWLEVBQWtCLGdCQUFhLE9BQWIsRUFBcUIsY0FBVyxPQUFYLEVBQTFDLENBRkQ7S0FJQzs7UUFBSyxXQUFVLGNBQVYsRUFBTDtNQUNDOztTQUFJLFdBQVUsYUFBVixFQUF3QixJQUFHLGNBQUgsRUFBNUI7O09BREQ7TUFKRDtLQVNDOztRQUFLLFdBQVUsWUFBVixFQUFMO01BQ0M7O1NBQUssV0FBVSxhQUFWLEVBQUw7T0FFRSxXQUZGO09BSUUsVUFKRjtPQU1FLGVBTkY7T0FRRSxTQVJGO09BV0M7O1VBQUssV0FBVSxZQUFWLEVBQUw7UUFDQzs7V0FBRyxXQUFVLGdCQUFWLEVBQTJCLE1BQUssUUFBTCxFQUFjLFNBQVMsS0FBSyxhQUFMLEVBQXJEOztTQUREO1FBWEQ7T0FERDtNQVREO0tBREQ7SUFERDtHQURELENBN0hXO0VBdkQ4QjtBQTBOMUMsK0NBQWtCLFdBQVcsY0FBYyxLQUFLO0FBQy9DLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRDhCO0FBRS9DLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGb0I7O0FBSS9DLE1BQUksTUFBTSxFQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLElBQWxCLEVBQXdCLElBQXhCLENBQTZCLFVBQTdCLEtBQTRDLE9BQU8sV0FBUCxDQUpQO0FBSy9DLE1BQUksUUFBUSxTQUFjLEVBQWQsRUFBa0IsS0FBSyxLQUFMLENBQVcsS0FBWCxzQkFBb0IsV0FBWSxJQUFsRCxDQUFSLENBTDJDOztBQU8vQyxNQUFHLGdCQUFnQixhQUFhLE1BQWIsR0FBc0IsQ0FBdEIsRUFBeUI7QUFDM0MsUUFBSSxJQUFJLElBQUksQ0FBSixFQUFPLElBQUksYUFBYSxNQUFiLEVBQXFCLElBQUksQ0FBSixFQUFPLEdBQS9DLEVBQW9EO0FBQ25ELFFBQUksYUFBYSxhQUFhLENBQWIsQ0FBYixDQUQrQztBQUVuRCxVQUFNLFVBQU4sSUFBb0IsRUFBcEIsQ0FGbUQ7SUFBcEQ7R0FERDs7QUFPQSxPQUFLLFFBQUwsQ0FBYztBQUNiLFVBQU8sS0FBUDtBQUNBLGNBQVcsRUFBWDtBQUNBLFVBQU8sRUFBUDtBQUNBLGlCQUFjLEVBQWQ7QUFDQSx3QkFBcUIsZUFBckI7R0FMRCxFQU1HLFlBQVc7QUFDYixPQUFHLEtBQUssYUFBTCxFQUFILEVBQXlCO0FBQ3hCLFNBQUssZUFBTCxDQUFxQixLQUFLLEtBQUwsQ0FBVyxPQUFYLEVBQW9CLEtBQXpDLEVBRHdCO0lBQXpCO0dBREUsQ0FJRCxJQUpDLENBSUksSUFKSixDQU5ILEVBZCtDO0VBMU5OO0FBcVAxQyx1REFBc0IsV0FBVyxLQUFLO0FBQ3JDLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRG9CO0FBRXJDLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGVTs7QUFJckMsTUFBSSxNQUFNLE9BQU8sS0FBUCxDQUoyQjtBQUtyQyxNQUFJLFFBQVEsU0FBYyxFQUFkLEVBQWtCLEtBQUssS0FBTCxDQUFXLEtBQVgsc0JBQW9CLFdBQVksSUFBbEQsQ0FBUixDQUxpQzs7QUFPckMsT0FBSyxRQUFMLENBQWM7QUFDYixVQUFPLEtBQVA7QUFDQSxjQUFXLEVBQVg7QUFDQSxVQUFPLEVBQVA7QUFDQSxpQkFBYyxFQUFkO0FBQ0Esd0JBQXFCLGVBQXJCO0dBTEQsRUFNRyxZQUFXO0FBQ2IsT0FBRyxLQUFLLGFBQUwsRUFBSCxFQUF5QjtBQUN4QixTQUFLLGVBQUwsQ0FBcUIsS0FBSyxLQUFMLENBQVcsT0FBWCxFQUFvQixLQUF6QyxFQUR3QjtJQUF6QjtHQURFLENBSUQsSUFKQyxDQUlJLElBSkosQ0FOSCxFQVBxQztFQXJQSTtBQTBRMUMsK0NBQWtCLEtBQUs7QUFDdEIsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FESztBQUV0QixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRkw7O0FBSXRCLE1BQUksTUFBTSxPQUFPLEtBQVAsQ0FKWTs7QUFNdEIsT0FBSyxRQUFMLENBQWM7QUFDYixVQUFPLEdBQVA7R0FERCxFQU5zQjtFQTFRbUI7QUFxUjFDLDJDQUFnQixTQUFTLFlBQVk7QUFDcEMsTUFBSSxVQUFVLGNBQWMsS0FBSyxLQUFMLENBQVcsT0FBWCxDQUF4QixDQURnQztBQUVwQyxNQUFJLFFBQVEsU0FBYyxFQUFkLEVBQWtCLEtBQUssS0FBTCxDQUFXLEtBQVgsRUFBa0IsRUFBQyxnQkFBRCxFQUFwQyxDQUFSLENBRmdDOztBQUlwQyxPQUFLLFFBQUwsQ0FBYztBQUNiLGlCQUFjLEVBQWQ7QUFDQSx3QkFBcUIsZUFBckI7R0FGRCxFQUpvQzs7QUFTcEMsT0FBSSxJQUFJLEdBQUosSUFBVyxLQUFmLEVBQXNCO0FBQ3JCLE9BQUksTUFBTSxNQUFNLEdBQU4sQ0FBTixDQURpQjs7QUFHckIsT0FBRyxRQUFRLG1CQUFSLEVBQTZCO0FBQy9CLFVBQU0sS0FBTixJQUFlLE9BQU8sY0FBYyxHQUFkLENBQVAsQ0FBZixDQUQrQjtBQUUvQixXQUFPLE1BQU0sR0FBTixDQUFQLENBRitCOztBQUkvQixhQUorQjtJQUFoQzs7O0FBSHFCLE9BV2xCLE9BQU8sUUFBUCxFQUFpQjs7QUFDbkIsV0FBTyxNQUFNLEdBQU4sQ0FBUCxDQURtQjtJQUFwQixNQUVPO0FBQ04sVUFBTSxHQUFOLElBQWEsT0FBTyxHQUFQLENBQWIsQ0FETTtJQUZQO0dBWEQ7O0FBbUJBLE1BQUksSUFBSixDQUFTLG9DQUFULEVBQStDLEtBQS9DLEVBQ0UsSUFERixDQUNPLFVBQVMsR0FBVCxFQUFjO0FBQ25CLE9BQUcsSUFBSSxNQUFKLENBQVcsVUFBWCxLQUEwQixTQUExQixFQUFxQztBQUN2QyxRQUFJLGFBQWEsSUFBSSxNQUFKLENBQVcsSUFBWCxDQURzQjtBQUV2QyxRQUFJLE9BQU8sV0FBVyxHQUFYLENBQWUsVUFBQyxDQUFEO1lBQU8sSUFBSSxVQUFKLENBQWUsRUFBRSxTQUFGLEVBQWEsRUFBRSxTQUFGLEVBQWEsQ0FBekM7S0FBUCxDQUF0QixDQUZtQzs7QUFJdkMsUUFBRyxLQUFLLE1BQUwsS0FBZ0IsQ0FBaEIsRUFBbUI7QUFDckIsVUFBSyxRQUFMLENBQWM7QUFDYiwyQkFBcUIsc0JBQXJCO01BREQsRUFEcUI7S0FBdEIsTUFJTztBQUNOLFVBQUssUUFBTCxDQUFjO0FBQ2IsMkJBQXFCLElBQXJCO0FBQ0Esb0JBQWMsSUFBZDtNQUZELEVBRE07S0FKUDtJQUpEO0dBREssQ0FpQkosSUFqQkksQ0FpQkMsSUFqQkQsQ0FEUCxFQTVCb0M7RUFyUks7QUF1VTFDLDJEQUF3QixLQUFLO0FBQzVCLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRFc7QUFFNUIsTUFBSSxTQUFTLElBQUksTUFBSixJQUFjLElBQUksVUFBSixDQUZDOztBQUk1QixNQUFJLE1BQU0sRUFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixJQUFsQixFQUF3QixJQUF4QixDQUE2QixVQUE3QixLQUE0QyxPQUFPLFdBQVAsQ0FKMUI7QUFLNUIsTUFBSSxRQUFRLEtBQUssS0FBTCxDQUFXLFlBQVgsQ0FBd0IsSUFBeEIsQ0FBNkIsVUFBQyxDQUFEO1VBQVEsRUFBRSxLQUFGLEtBQVksR0FBWjtHQUFSLENBQTdCLENBQXVELElBQXZELENBQTRELGFBQTVELENBTGdCOztBQU81QixPQUFLLFFBQUwsQ0FBYztBQUNiLGNBQVcsR0FBWDtBQUNBLFVBQU8sT0FBTyxLQUFQLENBQVA7R0FGRCxFQVA0QjtFQXZVYTtBQW9WMUMsdUNBQWMsS0FBSztBQUNsQixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURDO0FBRWxCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGVDs7Z0JBSTJDLEtBQUssS0FBTCxDQUozQztNQUlaLHNCQUpZO01BSUwsa0NBSks7TUFJUSxvQ0FKUjtNQUlzQixzQkFKdEI7TUFJNkIsOEJBSjdCO2dCQUtlLEtBQUssS0FBTCxDQUxmO01BS1osMEJBTFk7TUFLSCxzQ0FMRzs7QUFNbEIsTUFBSSxnQkFBSixDQU5rQjs7QUFRbEIsTUFBSSxXQUFXLEtBQUssYUFBTCxFQUFYLENBUmM7O0FBVWxCLE1BQUcsUUFBSCxFQUFhOzs7QUFFWixZQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsVUFBSyxTQUFMO0FBQ0MsYUFBTztBQUNOLHFCQUFjLE9BQU8sY0FBYyxPQUFkLENBQVAsQ0FBZDtBQUNBLGtCQUFXLE9BQU8sU0FBUCxDQUFYO0FBQ0Esa0JBQVcsT0FBTyxhQUFhLElBQWIsQ0FBa0IsVUFBQyxJQUFEO2VBQVcsS0FBSyxLQUFMLEtBQWUsU0FBZjtRQUFYLENBQWxCLENBQXdELEtBQXhELENBQWxCO0FBQ0EsZUFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLFlBQUssT0FBTyxjQUFjLE1BQU0saUJBQU4sQ0FBckIsQ0FBTDtBQUNBLFlBQUssT0FBTyxNQUFNLEdBQU4sQ0FBWjtBQUNBLHNCQUFlLE9BQU8sTUFBTSxNQUFOLENBQXRCO09BUEQsQ0FERDtBQVVDLFlBVkQ7QUFERCxVQVlNLE1BQUw7QUFDQyxhQUFPO0FBQ04scUJBQWMsT0FBTyxjQUFjLE9BQWQsQ0FBUCxDQUFkO0FBQ0Esa0JBQVcsT0FBTyxTQUFQLENBQVg7QUFDQSxrQkFBVyxPQUFPLGFBQWEsSUFBYixDQUFrQixVQUFDLElBQUQ7ZUFBVyxLQUFLLEtBQUwsS0FBZSxTQUFmO1FBQVgsQ0FBbEIsQ0FBd0QsS0FBeEQsQ0FBbEI7QUFDQSxlQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsWUFBSyxPQUFPLE1BQU0sR0FBTixDQUFaO0FBQ0EsWUFBSyxJQUFMO09BTkQsQ0FERDtBQVNDLFlBVEQ7QUFaRCxVQXNCTSxTQUFMO0FBQ0MsYUFBTztBQUNOLHFCQUFjLE9BQU8sY0FBYyxPQUFkLENBQVAsQ0FBZDtBQUNBLGtCQUFXLE9BQU8sU0FBUCxDQUFYO0FBQ0Esa0JBQVcsT0FBTyxhQUFhLElBQWIsQ0FBa0IsVUFBQyxJQUFEO2VBQVcsS0FBSyxLQUFMLEtBQWUsU0FBZjtRQUFYLENBQWxCLENBQXdELEtBQXhELENBQWxCO0FBQ0EsZUFBUSxPQUFPLEtBQVAsQ0FBUjtBQUNBLFlBQUssT0FBTyxNQUFNLEdBQU4sQ0FBWjtBQUNBLFlBQUssT0FBTyxNQUFNLEdBQU4sQ0FBWjtPQU5ELENBREQ7QUFTQyxZQVREO0FBdEJELFVBZ0NNLEtBQUw7QUFDQyxhQUFPO0FBQ04scUJBQWMsT0FBTyxjQUFjLE9BQWQsQ0FBUCxDQUFkO0FBQ0Esa0JBQVcsT0FBTyxTQUFQLENBQVg7QUFDQSxrQkFBVyxPQUFPLGFBQWEsSUFBYixDQUFrQixVQUFDLElBQUQ7ZUFBVyxLQUFLLEtBQUwsS0FBZSxTQUFmO1FBQVgsQ0FBbEIsQ0FBd0QsS0FBeEQsQ0FBbEI7QUFDQSxlQUFRLE9BQU8sS0FBUCxDQUFSO0FBQ0EsWUFBSyxPQUFPLE1BQU0sR0FBTixDQUFaO0FBQ0EsWUFBSyxPQUFPLE1BQU0sR0FBTixDQUFaO0FBQ0EscUJBQWMsT0FBTyxNQUFNLFlBQU4sQ0FBckI7QUFDQSxlQUFRLE9BQU8sTUFBTSxNQUFOLENBQWY7QUFDQSxjQUFPLE9BQU8sTUFBTSxLQUFOLENBQWQ7QUFDQSxpQkFBVSxPQUFPLE1BQU0sUUFBTixDQUFqQjtBQUNBLGlCQUFVLE9BQU8sTUFBTSxRQUFOLENBQWpCO09BWEQsQ0FERDtBQWNDLFlBZEQ7QUFoQ0Q7O0FBaURBLFFBQUksVUFBVSxFQUFFLE1BQUYsQ0FBVjtBQUNKLGtCQUFjLFlBQWQsQ0FBMkIsSUFBM0IsRUFBaUMsWUFBTTtBQUN0QyxhQUFRLE9BQVIsQ0FBZ0IsUUFBaEIsRUFBMEIsS0FBMUIsQ0FBZ0MsTUFBaEMsRUFEc0M7S0FBTixDQUFqQztRQXBEWTtHQUFiLE1Bd0RPO0FBQ04sUUFBSyxRQUFMLENBQWM7QUFDYixvQkFBZ0IsSUFBaEI7SUFERCxFQURNO0dBeERQO0VBOVZ5QztBQTZaMUMsbUNBQWE7QUFDWixPQUFLLFFBQUwsQ0FBYztBQUNiLFVBQU87QUFDTixlQUFXLEVBQVg7SUFERDtBQUdBLGlCQUFjLEVBQWQ7QUFDQSxjQUFXLEVBQVg7QUFDQSxVQUFPLEVBQVA7QUFDQSxtQkFBZ0IsS0FBaEI7R0FQRCxFQURZO0VBN1o2QjtBQXlhMUMseUNBQWdCO0FBQ2YsTUFBSSxXQUFXLElBQVgsQ0FEVztNQUVULFFBQVUsS0FBSyxLQUFMLENBQVYsTUFGUztNQUdULGdCQUFrQixLQUFLLEtBQUwsQ0FBbEIsY0FIUzs7O0FBS2YsT0FBSSxJQUFJLElBQUksQ0FBSixFQUFPLElBQUksY0FBYyxNQUFkLEVBQXNCLElBQUksQ0FBSixFQUFPLEdBQWhELEVBQXFEO0FBQ3BELE9BQUksUUFBUSxjQUFjLENBQWQsQ0FBUixDQURnRDs7QUFHcEQsT0FBRyxNQUFNLFlBQU4sRUFBb0I7QUFDdEIsZUFBVyxZQUFhLE1BQU0sWUFBTixDQUFtQixNQUFNLE1BQU0sRUFBTixDQUF6QixNQUF3QyxJQUF4QyxDQURGO0lBQXZCO0dBSEQ7O0FBUUEsU0FBTyxRQUFQLENBYmU7RUF6YTBCO0NBQWxCLENBQXJCOztBQTRiSixPQUFPLE9BQVAsR0FBaUIsa0JBQWpCOzs7OztBQ25kQSxJQUFJLFFBQVcsUUFBUSxPQUFSLENBQVg7SUFDSCxtQkFBbUIsUUFBUSwwQ0FBUixFQUFvRCxnQkFBcEQ7SUFDbkIsaUJBQWtCLFFBQVEsbUNBQVIsQ0FBbEI7O0lBRUssaUJBQXNDLGVBQXRDO0lBQWdCLG9CQUFzQixlQUF0Qjs7O0FBR3RCLElBQUksc0JBQXNCLE1BQU0sV0FBTixDQUFrQjs7O0FBQzNDLFNBQVEsQ0FDUCxRQUFRLG9DQUFSLENBRE8sRUFFUCxRQUFRLHNDQUFSLENBRk8sRUFHUCxRQUFRLHlDQUFSLENBSE8sRUFJUCxRQUFRLG1DQUFSLENBSk8sRUFLUCxRQUFRLDJDQUFSLENBTE8sQ0FBUjs7QUFTRyw2Q0FBa0I7QUFDZCxTQUFPLEVBQVAsQ0FEYztFQVZzQjtBQWdCeEMsNkNBQWtCO0FBQ2QsU0FBTyxFQUFQLENBRGM7RUFoQnNCO0FBc0J4QyxpREFBb0IsRUF0Qm9CO0FBMEJ4Qyx1REFBdUIsRUExQmlCO0FBOEIzQywrQ0FBa0IsWUFBWTtNQUN4QixVQUFvQyxXQUFwQyxRQUR3QjtNQUNmLGNBQTJCLFdBQTNCLFlBRGU7TUFDRixhQUFjLFdBQWQsV0FERTs7QUFFN0IsTUFBSSxtQkFBSixDQUY2Qjs7QUFJN0IsVUFBTyxRQUFRLFdBQVIsRUFBUDtBQUNDLFFBQUssU0FBTDtBQUNDLGNBQVUsQ0FDVDtBQUNDLFlBQU8sTUFBUDtBQUNBLFdBQU0sUUFBTjtBQUNBLGtCQUFhLFFBQWI7QUFDQSxnQkFBVyxHQUFYO0FBQ0EsU0FBSSxtQkFBSjtBQUNBLG1CQUFjLGlCQUFkO0tBUFEsRUFTVDtBQUNDLFlBQU8sSUFBUDtBQUNBLFdBQU0sUUFBTjtBQUNBLFNBQUksS0FBSjtBQUNBLGNBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFUO0FBQ0EsbUJBQWMsY0FBZDtLQWRRLEVBZ0JUO0FBQ0MsWUFBTyxNQUFQO0FBQ0EsV0FBTSxRQUFOO0FBQ0EsU0FBSSxRQUFKO0FBQ0EsY0FBUyxLQUFLLGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDLElBQWxDLENBQVQ7QUFDQSxtQkFBYyxjQUFkO0tBckJRLENBQVYsQ0FERDtBQXlCQyxVQXpCRDtBQURELFFBMkJNLE1BQUw7QUFDQyxjQUFVLENBQ1Q7QUFDQyxZQUFPLE1BQVA7QUFDQSxXQUFNLFVBQU47QUFDQSxTQUFJLEtBQUo7QUFDQSxjQUFTLElBQVQ7S0FMUSxFQU9UO0FBQ0MsWUFBTyxJQUFQO0FBQ0EsV0FBTSxRQUFOO0FBQ0EsU0FBSSxLQUFKO0FBQ0EsY0FBUyxLQUFLLGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDLElBQWxDLENBQVQ7QUFDQSxtQkFBYyxjQUFkO0tBWlEsQ0FBVixDQUREO0FBZ0JDLFVBaEJEO0FBM0JELFFBNENNLFNBQUw7QUFDQyxjQUFVLENBQ1Q7QUFDQyxZQUFPLE1BQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLEtBQUo7QUFDQSxjQUFTLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsSUFBbEMsQ0FBVDtBQUNBLG1CQUFjLGNBQWQ7S0FOUSxFQVFUO0FBQ0MsWUFBTyxJQUFQO0FBQ0EsV0FBTSxRQUFOO0FBQ0EsU0FBSSxLQUFKO0FBQ0EsY0FBUyxLQUFLLGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDLElBQWxDLENBQVQ7QUFDQSxtQkFBYyxjQUFkO0tBYlEsQ0FBVixDQUREO0FBaUJDLFVBakJEO0FBNUNELFFBOERNLEtBQUw7QUFDQyxRQUFJLGtCQUFrQjtBQUNyQixZQUFPLE1BQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLGNBQUo7QUFDQSxjQUFTLEVBQVQ7QUFDQSxtQkFBYyxjQUFkO0tBTEcsQ0FETDs7QUFTQyxRQUFHLFlBQVksR0FBWixFQUFpQjtBQUNuQixTQUFJLGNBQWMsV0FBVyxJQUFYLEVBQWlCLElBQWpCLENBQXNCLFVBQUMsQ0FBRDthQUFRLEVBQUUsRUFBRixJQUFRLFlBQVksR0FBWjtNQUFoQixDQUFwQzs7O0FBRGUsU0FJaEIsZUFBZSxZQUFZLElBQVosRUFBa0I7QUFDbkMsVUFBSSxPQUFPLEVBQVAsQ0FEK0I7QUFFbkMsVUFBSSxVQUFVLEtBQUssS0FBTCxRQUFnQixZQUFZLElBQVosT0FBaEIsQ0FBVixDQUYrQjs7QUFJbkMsV0FBSyxJQUFJLEVBQUosSUFBVSxPQUFmLEVBQXdCO0FBQ3ZCLFlBQUssSUFBTCxDQUFVO0FBQ1QsZUFBTyxRQUFRLEVBQVIsQ0FBUDtBQUNBLGVBQU8sRUFBUDtRQUZELEVBRHVCO09BQXhCO0FBTUEsc0JBQWdCLE9BQWhCLEdBQTBCLElBQTFCLENBVm1DO01BQXBDO0tBSkQ7O0FBa0JBLGNBQVUsQ0FDVDtBQUNDLFlBQU8sTUFBUDtBQUNBLFdBQU0sUUFBTjtBQUNBLFNBQUksS0FBSjtBQUNBLGNBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFUO0FBQ0EsbUJBQWMsQ0FBQyxjQUFELENBQWQ7QUFDQSxtQkFBYyxjQUFkO0tBUFEsRUFVVCxlQVZTLEVBWVQ7QUFDQyxZQUFPLElBQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLEtBQUo7QUFDQSxjQUFTLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsSUFBbEMsQ0FBVDtBQUNBLG1CQUFjLGNBQWQ7S0FqQlEsRUFtQlQ7QUFDQyxZQUFPLE9BQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLFFBQUo7QUFDQSxjQUFTLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsSUFBbEMsQ0FBVDtBQUNBLG1CQUFjLGNBQWQ7S0F4QlEsRUEyQlQ7QUFDQyxZQUFPLE9BQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLE9BQUo7QUFDQSxjQUFTLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsSUFBbEMsQ0FBVDtBQUNBLG1CQUFjLGNBQWQ7S0FoQ1EsRUFrQ1Q7QUFDQyxZQUFPLFFBQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLFVBQUo7QUFDQSxnQkFBVyxpQkFBWDtBQUNBLGNBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFUO0FBQ0EsbUJBQWMsY0FBZDtLQXhDUSxFQTBDVDtBQUNDLFlBQU8sSUFBUDtBQUNBLFdBQU0sUUFBTjtBQUNBLFNBQUksVUFBSjtBQUNBLGNBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxJQUFsQyxDQUFUO0FBQ0EsbUJBQWMsY0FBZDtLQS9DUSxDQUFWLENBM0JEO0FBOEVDLFVBOUVEO0FBOUREO0FBOElFLGNBQVUsRUFBVixDQUREO0FBN0lELEdBSjZCOztBQXFKN0IsU0FBTyxPQUFQLENBcko2QjtFQTlCYTtBQXNMM0MscURBQXFCLFNBQVMsaUJBQWlCO0FBQzlDLE1BQUksbUJBQW1CLEVBQW5CLENBRDBDOztBQUc5QyxVQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsUUFBSyxTQUFMO0FBQ0MsdUJBQW1CLGdCQUFnQixHQUFoQixDQUFvQixVQUFDLElBQUQsRUFBTyxHQUFQLEVBQWU7U0FFcEQsWUFLRyxLQUxILFVBRm9EO1NBR3BELGFBSUcsS0FKSCxXQUhvRDtTQUlwRCxTQUdHLEtBSEgsT0FKb0Q7U0FLcEQsY0FFRyxLQUZILFlBTG9EO1NBTXBELHFCQUNHLEtBREgsbUJBTm9EOzs7QUFTckQsWUFBTztBQUNOLGFBQU8sU0FBUDtBQUNBLFlBQU0sQ0FDTDtBQUNDLGFBQU0sTUFBTjtBQUNBLGNBQU8sS0FBSyxVQUFMO0FBQ1AsZ0JBQVMsQ0FBVDtBQUNBLG1CQUFZLElBQVo7T0FMSSxFQU9MO0FBQ0MsYUFBTSxLQUFOO0FBQ0EsY0FBTyxpQkFBaUIsS0FBSyxNQUFMLENBQXhCO0FBQ0EsZ0JBQVMsQ0FBVDtBQUNBLGVBQVEsR0FBUjtBQUNBLGFBQU0sR0FBTjtBQUNBLHNCQUFlLElBQWY7T0FiSSxFQWVMO0FBQ0MsYUFBTSxNQUFOO0FBQ0EsY0FBTyxLQUFLLFdBQUw7QUFDUCxnQkFBUyxDQUFUO09BbEJJLEVBb0JMO0FBQ0MsYUFBTSxNQUFOO0FBQ0EsY0FBTyxLQUFLLGtCQUFMO0FBQ1AsZ0JBQVMsQ0FBVDtPQXZCSSxDQUFOO01BRkQsQ0FUcUQ7S0FBZixDQUF2QyxDQUREO0FBd0NDLFVBeENEO0FBREQsUUEwQ00sTUFBTDtBQUNDLHVCQUFtQixnQkFBZ0IsR0FBaEIsQ0FBb0IsVUFBQyxJQUFELEVBQU8sR0FBUCxFQUFlO1NBRXBELFlBT0csS0FQSCxVQUZvRDtTQUdwRCxhQU1HLEtBTkgsV0FIb0Q7U0FJcEQsU0FLRyxLQUxILE9BSm9EO1NBS3BELGNBSUcsS0FKSCxZQUxvRDtTQU1wRCxjQUdHLEtBSEgsWUFOb0Q7U0FPcEQsY0FFRyxLQUZILFlBUG9EO1NBUXBELGNBQ0csS0FESCxZQVJvRDs7O0FBV3JELFlBQU87QUFDTixhQUFPLFNBQVA7QUFDQSxZQUFNLENBQ0w7QUFDQyxhQUFNLE1BQU47QUFDQSxjQUFPLFVBQVA7QUFDQSxnQkFBUyxDQUFUO0FBQ0EsbUJBQVksSUFBWjtPQUxJLEVBT0w7QUFDQyxhQUFNLFFBQU47QUFDQSxjQUFPLFdBQVA7QUFDQSxnQkFBUyxDQUFUO0FBQ0EsYUFBTSxHQUFOO0FBQ0Esc0JBQWUsSUFBZjtPQVpJLEVBY0w7QUFDQyxhQUFNLE1BQU47QUFDQSxjQUFPLFdBQVA7QUFDQSxnQkFBUyxDQUFUO0FBQ0EsYUFBTSxHQUFOO09BbEJJLEVBb0JMO0FBQ0MsYUFBTSxRQUFOO0FBQ0EsY0FBTyxXQUFQO0FBQ0EsZ0JBQVMsQ0FBVDtBQUNBLGFBQU0sR0FBTjtPQXhCSSxDQUFOO01BRkQsQ0FYcUQ7S0FBZixDQUF2QyxDQUREO0FBMkNDLFVBM0NEO0FBMUNELFFBc0ZNLFNBQUw7QUFDQyx1QkFBbUIsZ0JBQWdCLEdBQWhCLENBQW9CLFVBQUMsSUFBRCxFQUFPLEdBQVAsRUFBZTtTQUVwRCxZQU9HLEtBUEgsVUFGb0Q7U0FHcEQsYUFNRyxLQU5ILFdBSG9EO1NBSXBELFNBS0csS0FMSCxPQUpvRDtTQUtwRCxRQUlHLEtBSkgsTUFMb0Q7U0FNcEQsZ0JBR0csS0FISCxjQU5vRDtTQU9wRCxjQUVHLEtBRkgsWUFQb0Q7U0FRcEQsY0FDRyxLQURILFlBUm9EOzs7QUFXckQsWUFBTztBQUNOLGFBQU8sU0FBUDtBQUNBLFlBQU0sQ0FDTDtBQUNDLGFBQU0sTUFBTjtBQUNBLGNBQU8sVUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxtQkFBWSxJQUFaO09BTEksRUFPTDtBQUNDLGFBQU0sU0FBTjtBQUNBLGNBQU8sYUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxhQUFNLEdBQU47QUFDQSxzQkFBZSxJQUFmO09BWkksRUFjTDtBQUNDLGFBQU0sTUFBTjtBQUNBLGNBQU8sV0FBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxhQUFNLEdBQU47T0FsQkksRUFvQkw7QUFDQyxhQUFNLFFBQU47QUFDQSxjQUFPLFdBQVA7QUFDQSxnQkFBUyxDQUFUO0FBQ0EsYUFBTSxFQUFOO0FBQ0EsY0FBTyxFQUFFLE9BQU8sS0FBUCxFQUFjLGFBQWEsTUFBYixFQUFxQixZQUFZLG1CQUFaLEVBQTVDO09BekJJLENBQU47TUFGRCxDQVhxRDtLQUFmLENBQXZDLENBREQ7QUE0Q0MsVUE1Q0Q7QUF0RkQsUUFtSU0sS0FBTDtBQUNDLHVCQUFtQixnQkFBZ0IsR0FBaEIsQ0FBb0IsVUFBQyxJQUFELEVBQU8sR0FBUCxFQUFlO1NBRXBELFlBR0csS0FISCxVQUZvRDtTQUdwRCxhQUVHLEtBRkgsV0FIb0Q7U0FJcEQsU0FDRyxLQURILE9BSm9EOzs7QUFPckQsWUFBTztBQUNOLGFBQU8sU0FBUDtBQUNBLFlBQU0sQ0FDTDtBQUNDLGFBQU0sTUFBTjtBQUNBLGNBQU8sVUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxtQkFBWSxJQUFaO09BTEksRUFPTDtBQUNDLGFBQU0sS0FBTjtBQUNBLGNBQU8saUJBQWlCLE1BQWpCLENBQVA7QUFDQSxnQkFBUyxDQUFUO0FBQ0EsZUFBUSxHQUFSO0FBQ0EsYUFBTSxHQUFOO0FBQ0Esc0JBQWUsSUFBZjtPQWJJLENBQU47TUFGRCxDQVBxRDtLQUFmLENBQXZDLENBREQ7QUE0QkMsVUE1QkQ7O0FBbklELEdBSDhDOztBQXVLOUMsU0FBTyxnQkFBUCxDQXZLOEM7RUF0TEo7Q0FBbEIsQ0FBdEI7O0FBbVdKLE9BQU8sT0FBUCxHQUFpQixtQkFBakIiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIGxldCBSZWFjdFx0XHRcdFx0PSByZXF1aXJlKCdyZWFjdCcpLFxyXG5cdFx0UmVhY3RET01cdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0LWRvbScpLFxyXG5cdFx0UXVlcnlTdG9yZVx0XHRcdD0gcmVxdWlyZSgnLi4vY29tbW9uL3N0b3Jlcy9RdWVyeVN0b3JlJyksXHJcblx0XHRQcm9kdWN0U3RvcmVcdFx0PSByZXF1aXJlKCcuLi9jb21tb24vc3RvcmVzL1Byb2R1Y3RTdG9yZScpLFxyXG5cdFx0Q29tcGFyaXNvblN0b3JlXHRcdD0gcmVxdWlyZSgnLi4vY29tbW9uL3N0b3Jlcy9Db21wYXJpc29uU3RvcmUnKSxcdFx0XHJcblx0XHRDb21tb25BY3Rpb25zXHRcdD0gcmVxdWlyZSgnLi4vY29tbW9uL2FjdGlvbnMvQ29tbW9uQWN0aW9ucycpLFxyXG5cdFx0Z2V0U2VhcmNoQ2FsbGJhY2tcdD0gcmVxdWlyZSgnLi4vY29tbW9uL3V0aWxzL2dldFNlYXJjaENhbGxiYWNrJyksXHJcblx0XHRJbnN1cmVIZWxwZXJTZWN0aW9uID0gcmVxdWlyZSgnLi9jb21wb25lbnRzL0luc3VyZUhlbHBlclNlY3Rpb24uanN4JyksXHJcblx0XHRJbnN1cmVSZXN1bHRTZWN0aW9uID0gcmVxdWlyZSgnLi9jb21wb25lbnRzL0luc3VyZVJlc3VsdFNlY3Rpb24uanN4JyksXHJcblx0XHRJbnN1cmVQcm9kdWN0QWRkZXJcdD0gcmVxdWlyZSgnLi9jb21wb25lbnRzL0luc3VyZVByb2R1Y3RBZGRlci5qc3gnKTtcclxuXHJcblxyXG5cdGxldCBtYWlubWVudSwgc3VibWVudSwgaXNMb2dnZWRJbiwgc2VhcmNoUXVlcnk7XHJcbiAgICBcclxuICAgIGlmKHdpbmRvdy5BcHApIHtcclxuXHRcdCh7bWFpbm1lbnUsIHN1Ym1lbnUsIHNlYXJjaFF1ZXJ5fSA9IHdpbmRvdy5BcHAucXVlcnkpO1xyXG5cdFx0aXNMb2dnZWRJbiA9IHdpbmRvdy5BcHAuY29tcGFyaXNvbnMuaXNMb2dnZWRJbjtcdFx0XHJcblx0XHRcclxuXHRcdFF1ZXJ5U3RvcmUucmVoeWRyYXRlKCdxdWVyeScpO1xyXG5cdFx0UHJvZHVjdFN0b3JlLnJlaHlkcmF0ZSgncmVzdWx0cycpO1xyXG5cdFx0Q29tcGFyaXNvblN0b3JlLnJlaHlkcmF0ZSgnY29tcGFyaXNvbnMnKTtcclxuICAgIH1cclxuXHJcblxyXG4vL1x0Q29tbW9uQWN0aW9ucy5mZXRjaE9wdGlvbnMoW1xyXG4vL1x0XHQzMTAwLCAzMTAxLCAzMTAyLCAzMTAzLCAzMjAwLCAzMjAxLCAzMzAwLCAzMzAxLCAzMzAyLCAxMjM0LCAzNDAwLCAzNDAxLCAzNDAyLCAzNDAzLCAzNDA0LCAzNDA1LCAzNDA2XHJcbi8vXHRdKTtcclxuXHJcblxyXG5cdGlmKGlzTG9nZ2VkSW4pIHtcclxuXHRcdENvbW1vbkFjdGlvbnMuZmV0Y2hNeVByb2R1Y3RzKG1haW5tZW51LCBzdWJtZW51KTtcclxuXHR9XHJcblx0XHRcclxuXHRsZXQgcHJvZHVjdEFkZGVyID0gPEluc3VyZVByb2R1Y3RBZGRlciAvPjtcclxuXHJcblx0bGV0IHNlYXJjaENhbGxiYWNrID0gZ2V0U2VhcmNoQ2FsbGJhY2soc2VhcmNoUXVlcnkpOyAvLyBpZiBzZWFyY2hRdWVyeSBnaXZlbiBpbiBhZHZhbmNlLCBzZWFyY2ggcmlnaHQgYXdheVxyXG5cdFxyXG5cclxuICAgIFJlYWN0RE9NLnJlbmRlcihcclxuXHRcdDxJbnN1cmVIZWxwZXJTZWN0aW9uXHJcblx0XHRcdGFkZGVyTW9kYWw9e3Byb2R1Y3RBZGRlcn0gLz4sXHJcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaGVscGVyJyksXHJcblx0XHRzZWFyY2hDYWxsYmFja1xyXG4gICAgKTtcclxuXHJcblx0UmVhY3RET00ucmVuZGVyKFxyXG5cdFx0PEluc3VyZVJlc3VsdFNlY3Rpb24gLz4sXHJcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncmVzdWx0cycpXHJcbiAgICApO1xyXG5cclxuXHJcbn0pKCk7XHJcbiIsImxldCBSZWFjdFx0XHRcdFx0PSByZXF1aXJlKCdyZWFjdCcpLFxyXG5cdFByb2R1Y3RDYXJkXHRcdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9Qcm9kdWN0Q2FyZC5qc3gnKSxcclxuXHRudW1iZXJXaXRoQ29tbWFzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9udW1iZXJGaWx0ZXJGdW5jdGlvbnMnKS5udW1iZXJXaXRoQ29tbWFzO1xyXG5cclxuXHJcblxyXG5sZXQgSW5zdXJlUmVzdWx0U2VjdGlvbiA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcclxuXHRtaXhpbnM6IFtcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9xdWVyeVN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9wcm9kdWN0U3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2NvbXBhcmlzb25TdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvc2VuZFF1ZXJ5TWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9yZXN1bHRTZWN0aW9uTWl4aW4uanN4JylcclxuXHRdLFxyXG5cdFxyXG4gICAgcHJvcFR5cGVzOiB7XHJcblxyXG4gICAgfSxcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuXHRcdFx0XHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBcclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG5cdFx0Ly9sZXQgeyBxdWVyeVN0YXRlLCByZXN1bHRTdGF0ZSB9ID0gdGhpcy5zdGF0ZTtcclxuXHRcdFxyXG5cdFx0Ly9pZiAocmVzdWx0U3RhdGUucHJvZHVjdERhdGEubGVuZ3RoID09PSAwKSB7XHJcblx0XHQvL1x0bGV0IHttYWlubWVudSwgc3VibWVudX0gPSBxdWVyeVN0YXRlO1xyXG5cdFx0Ly9cdGxldCBxdWVyeSA9IHRoaXMuZ2V0QWNjZXB0YWJsZVF1ZXJ5KHF1ZXJ5U3RhdGUpO1xyXG5cdFx0Ly9cdENvbW1vbkFjdGlvbnMuZmV0Y2hSZXN1bHRzKG1haW5tZW51LCBzdWJtZW51LCBxdWVyeSk7XHJcblx0XHQvL31cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcblxyXG4gICAgfSxcclxuXHJcblxyXG5cdF9nZXRPcHRpb25zKHN1Ym1lbnUsIG9wdENvZGVNYXApIHtcclxuXHRcdC8vIHJldHVybnMge2ZpbHRlck9wdGlvbnMsIHNvcnRPcHRpb25zLCB0b29sdGlwRWxlbSwgcmVzdWx0c0VsZW0sIGhlbHBlckVsZW19XHJcblx0XHQvL1xyXG5cdFx0Ly8gICAgdG9vbHRpcEVsZW0gLT4gdG9vbHRpcCBlbGVtZW50IHRvIGdvIGluIHNvcnRpbmcgc2VjdGlvblxyXG5cdFx0Ly8gICAgcmVzdWx0c0VsZW0gLT4gbGlzdCBvZiBmaWxsZWQgaW4gcmVzdWx0cyBlbGVtZW50cyAoQXJyYXkgb2YgUHJvZHVjdENhcmQncyBlbGVtZW50cylcclxuXHRcdC8vICAgIGhlbHBlckVsZW0gIC0+IChpZiBuZWVkZWQpIG1vZGFsIHRvIHVzZSBvciBhbnkgb3RoZXIgZWxlbWVudFxyXG5cdFx0XHJcblx0XHRsZXQge3Jlc3VsdFN0YXRlLCBjb21wYXJpc29uU3RhdGV9ID0gdGhpcy5zdGF0ZTtcclxuXHRcdFxyXG5cdFx0bGV0IHsgaXNDbXByVGFiQWN0aXZlLCBpc0xvZ2dlZEluLCBjb21wYXJpc29uR3JvdXAsXHRjdXJyZW50Q21wciB9ID0gY29tcGFyaXNvblN0YXRlO1xyXG5cdFx0bGV0IHNob3dpbmdDbXByID0gKGlzTG9nZ2VkSW4gJiYgaXNDbXByVGFiQWN0aXZlICYmIGNvbXBhcmlzb25Hcm91cC5sZW5ndGggPiAwKTtcclxuXHRcdGxldCBzZWxlY3RlZENtcHIgPSBjb21wYXJpc29uR3JvdXBbY3VycmVudENtcHJdO1xyXG5cdFx0XHJcblx0XHRsZXQgZmlsdGVyT3B0aW9ucywgc29ydE9wdGlvbnMsIHRvb2x0aXBFbGVtLCByZXN1bHRzRWxlbSwgaGVscGVyRWxlbTtcclxuXHRcdGxldCBjb21wYXJpc29uQ29udGVudHMgPSB1bmRlZmluZWQ7XHJcblx0XHRcclxuXHRcdHN3aXRjaChzdWJtZW51LnRvTG93ZXJDYXNlKCkpIHtcclxuXHRcdFx0Ly8g7Iuk7IaQ67O07ZeYXHJcblx0XHRcdGNhc2UoJ3NoaWxzb24nKTpcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gdGhpcy5fZ2V0TGlzdE9wdGlvbnMob3B0Q29kZU1hcCwgMzEwMSk7XHJcblx0XHRcdFx0ZmlsdGVyT3B0aW9ucyA9IHRoaXMuX2FkZE51bVJlc3VsdHMoZmlsdGVyT3B0aW9ucyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c29ydE9wdGlvbnMgPSB0aGlzLl9nZXRMaXN0T3B0aW9ucyhvcHRDb2RlTWFwLCAzMTAwKTtcclxuXHJcblx0XHRcdFx0dG9vbHRpcEVsZW0gPSAoXHJcblx0XHRcdFx0XHQ8c3BhbiBjbGFzc05hbWU9XCJiYWRnZVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIGRhdGEtaHRtbD1cInRydWVcIiBkYXRhLXBsYWNlbWVudD1cInJpZ2h0XCIgdGl0bGU9XCLsp4DquInspIDruYTsnKjsnbTrnoA/KOuztO2XmO2ajOyCrOqwgMKg6rCA7J6F7J6Q7JeQ6rKMwqDrs7Ttl5jquIjsnYTCoOygnOuVjMKg7KeA6riJ7ZWg7IiYwqDsnojripTsp4DrpbzCoOuCmO2DgOuCtOuKlMKg6rKD7Jy866GcwqDrs7Ttl5jtmozsgqzsnZjCoOyerOustOqxtOyghOyEseydhMKg7YyQ64uo7ZWg7IiYwqDsnojripTCoOyngO2RnOyeheuLiOuLpC4pPGJyPuu2gOyngOq4ieuloOydtOuegD8o7LKt6rWs65CcwqDrs7Ttl5jquIjCoOykkcKg7KeA6riJ7ZWY7KeAwqDslYrsnYDCoOuztO2XmOq4iMKg67mE7Jyo7J2EwqDrgpjtg4Drg4Xri4jri6QuKVwiPj88L3NwYW4+XHJcblx0XHRcdFx0KTtcclxuXHJcblxyXG5cdFx0XHRcdHJlc3VsdHNFbGVtID0gcmVzdWx0U3RhdGUucHJvZHVjdERhdGEubWFwKCAoZWxlbSwgaWR4KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQge1xyXG5cdFx0XHRcdFx0XHRmaWx0ZXIsXHJcblx0XHRcdFx0XHRcdGRhbWJvMSxcclxuXHRcdFx0XHRcdFx0cmVzZXJ2ZV9yYXRlLFxyXG5cdFx0XHRcdFx0XHR1bnBhaWRfcmF0ZSxcclxuXHRcdFx0XHRcdFx0cHJvZF9jb2RlLFxyXG5cdFx0XHRcdFx0XHRzZXgsXHJcblx0XHRcdFx0XHRcdGFnZSxcclxuXHRcdFx0XHRcdFx0aW5zdXJhbmNlX2ZlZSxcclxuXHRcdFx0XHRcdFx0cHJvZF9uYW1lLFxyXG5cdFx0XHRcdFx0XHRkZXRhaWxfdXJsLFxyXG5cdFx0XHRcdFx0XHRpbnRlcmVzdF95bixcclxuXHRcdFx0XHRcdFx0Y29tcGFueV9jb2RlLFxyXG5cdFx0XHRcdFx0fSA9IGVsZW07XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRsZXQgbWFpbkF0dHJTZXQgPSB7XHJcblx0XHRcdFx0XHRcdCfqsIDsnoXsnKDtmJUnOiBmaWx0ZXIsXHJcblx0XHRcdFx0XHRcdCfshKDtg53ri7Trs7QnOiBkYW1ibzEsXHJcblx0XHRcdFx0XHRcdCfsp4DquInspIDruYTsnKgnOiBgJHtyZXNlcnZlX3JhdGV9ICVgLFxyXG5cdFx0XHRcdFx0XHQn67aA7KeA6riJ66WgJzogYCR7dW5wYWlkX3JhdGV9ICVgXHJcblx0XHRcdFx0XHR9O1xyXG5cclxuXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmKHNob3dpbmdDbXByKSB7XHJcblx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cyA9IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn7JuUIOuztO2XmOujjCcsXHJcblx0XHRcdFx0XHRcdFx0XHRkaWZmOiBpbnN1cmFuY2VfZmVlIC0gc2VsZWN0ZWRDbXByLmFtb3VudCxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICfsm5AnXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRsZXQgaXNJbnRlcmVzdGVkID0gKGludGVyZXN0X3luID09PSAnWScpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFx0XHQ8UHJvZHVjdENhcmRcclxuXHRcdFx0XHRcdFx0XHRrZXk9e2BzaGlsc29uJHtwcm9kX2NvZGV9JHtzZXh9JHthZ2V9YH1cclxuXHRcdFx0XHRcdFx0XHR0eXBlPSd0eXBlMiBzaGlsc29uJ1xyXG5cdFx0XHRcdFx0XHRcdGNhdGVnb3J5PXtmaWx0ZXJ9XHJcblx0XHRcdFx0XHRcdFx0cHJvdmlkZXI9e2NvbXBhbnlfY29kZX1cclxuXHRcdFx0XHRcdFx0XHRsZWFkaW5nSW5mbz17bnVtYmVyV2l0aENvbW1hcyhpbnN1cmFuY2VfZmVlKX1cclxuXHRcdFx0XHRcdFx0XHRwcmVmaXg9J+yblCdcclxuXHRcdFx0XHRcdFx0XHR1bml0PSfsm5AnXHJcblx0XHRcdFx0XHRcdFx0cHJvZHVjdFRpdGxlPXtwcm9kX25hbWV9XHJcblx0XHRcdFx0XHRcdFx0bWFpbkF0dHJTZXQ9e21haW5BdHRyU2V0fVxyXG5cdFx0XHRcdFx0XHRcdGRldGFpbFVybD17ZGV0YWlsX3VybH1cclxuXHRcdFx0XHRcdFx0XHR0YXJnZXRNb2RhbFNlbGVjdG9yPScjcHJvZHVjdF92aWV3X3BvcHVwJ1xyXG5cdFx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cz17Y29tcGFyaXNvbkNvbnRlbnRzfVxyXG5cdFx0XHRcdFx0XHRcdGluZGV4PXtpZHh9XHJcblx0XHRcdFx0XHRcdFx0aXNJbnRlcmVzdGVkPXtpc0ludGVyZXN0ZWR9XHJcblx0XHRcdFx0XHRcdFx0aGFuZGxlSW50ZXJlc3RDbGljaz17dGhpcy5oYW5kbGVJbnRlcmVzdENsaWNrLmJpbmQobnVsbCwgaXNJbnRlcmVzdGVkLCAzMTAwLCBwcm9kX2NvZGUsIGlkeCl9XHJcblx0XHRcdFx0XHRcdFx0Y2hvaWNlUmV2ZXJzZT17dHJ1ZX1cclxuXHRcdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHQvLyDsoJXquLDrs7Ttl5hcclxuXHRcdFx0Y2FzZSgnbGlmZScpOlxyXG5cdFx0XHRcdGZpbHRlck9wdGlvbnMgPSB0aGlzLl9nZXRMaXN0T3B0aW9ucyhvcHRDb2RlTWFwLCAzMjAxKTtcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gdGhpcy5fYWRkTnVtUmVzdWx0cyhmaWx0ZXJPcHRpb25zKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRzb3J0T3B0aW9ucyA9IHRoaXMuX2dldExpc3RPcHRpb25zKG9wdENvZGVNYXAsIDMyMDApO1xyXG5cclxuXHJcblx0XHRcdFx0cmVzdWx0c0VsZW0gPSByZXN1bHRTdGF0ZS5wcm9kdWN0RGF0YS5tYXAoIChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0XHRcdGxldCB7XHJcblx0XHRcdFx0XHRcdGluc3VyYW5jZV9mZWUsXHJcblx0XHRcdFx0XHRcdHB1YmxpY19yYXRlLFxyXG5cdFx0XHRcdFx0XHRsb3dlc3RfcmF0ZSxcclxuXHRcdFx0XHRcdFx0aWQsXHJcblx0XHRcdFx0XHRcdGd1YnVuMixcclxuXHRcdFx0XHRcdFx0cHJpY2VfaW5kZXgsXHJcblx0XHRcdFx0XHRcdHByb2RfbmFtZSxcclxuXHRcdFx0XHRcdFx0ZGV0YWlsX3VybCxcclxuXHRcdFx0XHRcdFx0aW50ZXJlc3RfeW4sXHJcblx0XHRcdFx0XHRcdHByb2RfY29kZSxcclxuXHRcdFx0XHRcdFx0Y29tcGFueV9jb2RlLFxyXG5cdFx0XHRcdFx0fSA9IGVsZW07XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGxldCBtYWluQXR0clNldCA9IHtcclxuXHRcdFx0XHRcdFx0J+uztO2XmOujjCc6IGAke251bWJlcldpdGhDb21tYXMoZWxlbS5pbnN1cmFuY2VfZmVlKX0g7JuQICgx6rCc7JuULCA0MOyEuCDquLDspIApYCxcclxuXHRcdFx0XHRcdFx0J+qzteyLnOydtOycqCc6IGAke2VsZW0ucHVibGljX3JhdGV9ICVgLFxyXG5cdFx0XHRcdFx0XHQn7LWc7KCA67O07Kad7J207JyoJzogZWxlbS5sb3dlc3RfcmF0ZVxyXG5cdFx0XHRcdFx0fTtcclxuXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmKHNob3dpbmdDbXByKSB7XHJcblx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cyA9IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn67O07ZeY6rCA6rKp7KeA7IiYJyxcclxuXHRcdFx0XHRcdFx0XHRcdGRpZmY6IHByaWNlX2luZGV4IC0gc2VsZWN0ZWRDbXByLnByaWNlX2luZGV4LFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJyUnXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRsZXQgaXNJbnRlcmVzdGVkID0gKGludGVyZXN0X3luID09PSAnWScpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFx0XHQ8UHJvZHVjdENhcmRcclxuXHRcdFx0XHRcdFx0XHRrZXk9e2BsaWZlJHtlbGVtLmlkfWB9XHJcblx0XHRcdFx0XHRcdFx0dHlwZT0ndHlwZTEgbGlmZSdcclxuXHRcdFx0XHRcdFx0XHRjYXRlZ29yeT17ZWxlbS5ndWJ1bjJ9XHJcblx0XHRcdFx0XHRcdFx0cHJvdmlkZXI9e2NvbXBhbnlfY29kZX1cclxuXHRcdFx0XHRcdFx0XHRsZWFkaW5nSW5mb05hbWU9eyfrs7Ttl5jqsIDqsqnsp4DsiJggPHNwYW4gY2xhc3M9XCJiYWRnZVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIGRhdGEtaHRtbD1cInRydWVcIiBkYXRhLXBsYWNlbWVudD1cInJpZ2h0XCIgdGl0bGU9XCJcIiBkYXRhLW9yaWdpbmFsLXRpdGxlPVwi67O07ZeY6rCA6rKp7KeA7IiY656APzxiciAvPu2VtOuLuSDrs7Ttl5jsgqzsnZgg67O07ZeY66OM66W8IOywuOyhsOyInCDrs7Ttl5jro4zsmYAg7JeF6rOE7Y+J6reg7IKs7JeF67mE7J2YIO2VqeycvOuhnCDrgpjriIgg6rCS7Jy866GcLCAxMDDrs7Tri6Qg64aS7Jy866m0IO2Pieq3oCDrjIDruYQg67mE7Iu46rOgIOuCruycvOuptCDsoIDroLTtlZjri6TripQg6rKD7J2EIOydmOuvuO2VqeuLiOuLpC5cIj4/PC9zcGFuPid9XHJcblx0XHRcdFx0XHRcdFx0bGVhZGluZ0luZm89e2VsZW0ucHJpY2VfaW5kZXh9XHJcblx0XHRcdFx0XHRcdFx0dW5pdD0nJSdcclxuXHRcdFx0XHRcdFx0XHRwcm9kdWN0VGl0bGU9e2VsZW0ucHJvZF9uYW1lfVxyXG5cdFx0XHRcdFx0XHRcdG1haW5BdHRyU2V0PXttYWluQXR0clNldH1cclxuXHRcdFx0XHRcdFx0XHRkZXRhaWxVcmw9e2VsZW0uZGV0YWlsX3VybH1cclxuXHRcdFx0XHRcdFx0XHR0YXJnZXRNb2RhbFNlbGVjdG9yPScjcHJvZHVjdF92aWV3X3BvcHVwJ1xyXG5cdFx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cz17Y29tcGFyaXNvbkNvbnRlbnRzfVxyXG5cdFx0XHRcdFx0XHRcdGluZGV4PXtpZHh9XHJcblx0XHRcdFx0XHRcdFx0aXNJbnRlcmVzdGVkPXtpc0ludGVyZXN0ZWR9XHJcblx0XHRcdFx0XHRcdFx0aGFuZGxlSW50ZXJlc3RDbGljaz17dGhpcy5oYW5kbGVJbnRlcmVzdENsaWNrLmJpbmQobnVsbCwgaXNJbnRlcmVzdGVkLCAzMjAwLCBwcm9kX2NvZGUsIGlkeCl9XHJcblx0XHRcdFx0XHRcdFx0Y2hvaWNlUmV2ZXJzZT17dHJ1ZX1cclxuXHRcdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2FubnVpdHknKTpcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gdGhpcy5fZ2V0TGlzdE9wdGlvbnMob3B0Q29kZU1hcCwgMzMwMSk7XHJcblx0XHRcdFx0ZmlsdGVyT3B0aW9ucyA9IHRoaXMuX2FkZE51bVJlc3VsdHMoZmlsdGVyT3B0aW9ucyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c29ydE9wdGlvbnMgPSB0aGlzLl9nZXRMaXN0T3B0aW9ucyhvcHRDb2RlTWFwLCAzMzAwKTtcclxuXHJcblx0XHRcdFx0Ly90b29sdGlwRWxlbSA9IChcclxuXHRcdFx0Ly9cdDxzcGFuIGNsYXNzTmFtZT1cImJhZGdlXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJib3R0b21cIiB0aXRsZT1cIuuztO2XmOuCmOydtCwg7ISx67OEIOuTseydhCDqs6DroKTtlZjsl6wg6rCA7J6F7ZWY7IugIOyDge2SiOuztOuLpCDrs7Ttl5jro4zqsIAg7KCA66C07ZWcLCDsp4DquInspIDruYTsnKjsnbQg64aS7J2ALCDrtoDsp4DquInrpaDsnbQg64Ku7J2AIOyDge2SiOydhCDstpTsspztlbTrk5zrpr3ri4jri6QuXCI+Pzwvc3Bhbj5cclxuXHRcdFx0XHQvLyk7XHJcblxyXG5cdFx0XHRcdHJlc3VsdHNFbGVtID0gcmVzdWx0U3RhdGUucHJvZHVjdERhdGEubWFwKCAoZWxlbSwgaWR4KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQge1xyXG5cdFx0XHRcdFx0XHRwcm9kX25hbWUsXHJcblx0XHRcdFx0XHRcdGd1YnVuLFxyXG5cdFx0XHRcdFx0XHRpbnN1cmFuY2VfZmVlLFxyXG5cdFx0XHRcdFx0XHRwdWJsaWNfcmF0ZSxcclxuXHRcdFx0XHRcdFx0bG93ZXN0X3JhdGUsXHJcblx0XHRcdFx0XHRcdGlkLFxyXG5cdFx0XHRcdFx0XHRpbnNfdHlwZSxcclxuXHRcdFx0XHRcdFx0c2F2ZWRfcmF0ZV8xMCxcclxuXHRcdFx0XHRcdFx0ZGV0YWlsX3VybCxcclxuXHRcdFx0XHRcdFx0cmVmdW5kX3VybCxcclxuXHRcdFx0XHRcdFx0aW50ZXJlc3RfeW4sXHJcblx0XHRcdFx0XHRcdHByb2RfY29kZSxcclxuXHRcdFx0XHRcdFx0Y29tcGFueV9jb2RlXHJcblx0XHRcdFx0XHR9ID0gZWxlbTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRsZXQgcGVyaW9kID0gKGd1YnVuID09PSAn7KCB66a9JykgPyAnKOyblCknIDogJyjsnbzsi5wpJztcclxuXHJcblx0XHRcdFx0XHRsZXQgbWFpbkF0dHJTZXQgPSB7fTtcclxuXHJcblx0XHRcdFx0XHRtYWluQXR0clNldFtgJHtwZXJpb2R964Kp7J6F7JWhYF0gPSBgJHtudW1iZXJXaXRoQ29tbWFzKGVsZW0uaW5zdXJhbmNlX2ZlZSl9IOybkGA7XHJcblxyXG5cdFx0XHRcdFx0bWFpbkF0dHJTZXQgPSBPYmplY3QuYXNzaWduKG1haW5BdHRyU2V0LCB7XHJcblx0XHRcdFx0XHRcdC8vJ+unpOuFhCDsl7DquIgg7IiY66C57JWhJzogYCR7aW5zdXJhbmNlX2ZlZX0g7JuQYCxcclxuXHRcdFx0XHRcdFx0J+qzteyLnOydtOycqCc6IGAke3B1YmxpY19yYXRlfSAlYCxcclxuXHRcdFx0XHRcdFx0J+y1nOyggOuztOymneydtOycqCc6IGxvd2VzdF9yYXRlLFxyXG5cdFx0XHRcdFx0XHQn7ZW07KeA7ZmY6riJ6riIJzogKFxyXG5cdFx0XHRcdFx0XHRcdDxhXHJcblx0XHRcdFx0XHRcdFx0XHRvbkNsaWNrPXsoZSkgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHRcdFx0XHRcdFx0XHRcdFx0IGxldCB0YXJnZXRFbGVtID0gJCgnI3JlZnVuZF9wb3B1cCcpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHQgXHJcblx0XHRcdFx0XHRcdFx0XHRcdCB0YXJnZXRFbGVtLnJlbW92ZURhdGEoJ2JzLm1vZGFsJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdCB0YXJnZXRFbGVtLmFkZENsYXNzKCdoaWRlJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRcdFx0IHRhcmdldEVsZW1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQgLm1vZGFsKClcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQgLmZpbmQoJy5tb2RhbC1ib2R5JylcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQgLmxvYWQocmVmdW5kX3VybCwgKHJlc3BvbnNlVGV4dCwgdGV4dFN0YXR1cykgPT4ge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0IFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0IGlmKCB0ZXh0U3RhdHVzID09PSAnc3VjY2VzcycgfHwgdGV4dFN0YXR1cyA9PT0gJ25vdG1vZGlmaWVkJyApIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0IHRhcmdldEVsZW0ucmVtb3ZlQ2xhc3MoJ2luJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCB0YXJnZXRFbGVtLnJlbW92ZUNsYXNzKCdoaWRlJyk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRcdCBcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0IHNldFRpbWVvdXQoKCkgPT4geyB0YXJnZXRFbGVtLmFkZENsYXNzKCdpbicpOyB9LCA1MCk7XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQgfSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdFx0IHRhcmdldE1vZGFsRWxlbS5tb2RhbCgnaGlkZScpO1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0IH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQgfSk7XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgfVxyXG5cdFx0XHRcdFx0XHRcdFx0fT7snpDshLjtnojrs7TquLA8L2E+XHJcblx0XHRcdFx0XHRcdClcclxuXHRcdFx0XHRcdH0pO1xyXG5cclxuXHRcdFx0XHRcdGlmKHNob3dpbmdDbXByKSB7XHJcblx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cyA9IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAnMTDrhYQg7KCB66a966WgJyxcclxuXHRcdFx0XHRcdFx0XHRcdGRpZmY6IHNhdmVkX3JhdGVfMTAgLSBzZWxlY3RlZENtcHIuc2F2ZWRfcmF0ZV8xMCxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICclJ1xyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdF07XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0bGV0IGlzSW50ZXJlc3RlZCA9IChpbnRlcmVzdF95biA9PT0gJ1knKTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0cmV0dXJuIChcclxuXHRcdFx0XHRcdFx0PFByb2R1Y3RDYXJkXHJcblx0XHRcdFx0XHRcdFx0a2V5PXtgYW5udWl0eSR7aWR9YH1cclxuXHRcdFx0XHRcdFx0XHR0eXBlPSd0eXBlMSBhbm51aXR5IHNtJ1xyXG5cdFx0XHRcdFx0XHRcdGNhdGVnb3J5PXtpbnNfdHlwZX1cclxuXHRcdFx0XHRcdFx0XHRwcm92aWRlcj17Y29tcGFueV9jb2RlfVx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0bGVhZGluZ0luZm9OYW1lPXsnMTDrhYQg7KCB66a966WgIDxzcGFuIGNsYXNzPVwiYmFkZ2VcIiBkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIiBkYXRhLWh0bWw9XCJ0cnVlXCIgZGF0YS1wbGFjZW1lbnQ9XCJyaWdodFwiIHRpdGxlPVwiXCIgZGF0YS1vcmlnaW5hbC10aXRsZT1cIjEw64WEIOyggeumveuloOydtOuegD88YnI+KOuztO2XmOyDge2SiOyXkCDqsIDsnoXtlZzsp4AgMTDrhYQg7ZuE7JeQIOuCqeu2gO2VnCDrs7Ttl5jro4wg64yA67mEIOuwm+ydhCDsiJgg7J6I64qUIO2ZmOq4ieq4iOydmCDruYTsnKjsnoXri4jri6QuKVwiPj88L3NwYW4+J31cclxuXHRcdFx0XHRcdFx0XHRsZWFkaW5nSW5mbz17c2F2ZWRfcmF0ZV8xMH1cclxuXHRcdFx0XHRcdFx0XHR1bml0PSclJ1xyXG5cdFx0XHRcdFx0XHRcdHByb2R1Y3RUaXRsZT17cHJvZF9uYW1lfVxyXG5cdFx0XHRcdFx0XHRcdG1haW5BdHRyU2V0PXttYWluQXR0clNldH1cclxuXHRcdFx0XHRcdFx0XHRkZXRhaWxVcmw9e2RldGFpbF91cmx9XHJcblx0XHRcdFx0XHRcdFx0dGFyZ2V0TW9kYWxTZWxlY3Rvcj0nI3Byb2R1Y3Rfdmlld19wb3B1cCdcclxuXHRcdFx0XHRcdFx0XHRjb21wYXJpc29uQ29udGVudHM9e2NvbXBhcmlzb25Db250ZW50c31cclxuXHRcdFx0XHRcdFx0XHRpbmRleD17aWR4fVxyXG5cdFx0XHRcdFx0XHRcdGlzSW50ZXJlc3RlZD17aXNJbnRlcmVzdGVkfVxyXG5cdFx0XHRcdFx0XHRcdGhhbmRsZUludGVyZXN0Q2xpY2s9e3RoaXMuaGFuZGxlSW50ZXJlc3RDbGljay5iaW5kKG51bGwsIGlzSW50ZXJlc3RlZCwgMzMwMCwgcHJvZF9jb2RlLCBpZHgpfVxyXG5cdFx0XHRcdFx0XHRcdGNob2ljZVJldmVyc2U9e2ZhbHNlfVxyXG5cdFx0XHRcdFx0XHQvPlxyXG5cdFx0XHRcdFx0KTtcclxuXHRcdFx0XHR9KTtcclxuXHJcblx0XHRcdFx0aGVscGVyRWxlbSA9IChcclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibW9kYWwgZmFkZVwiIGlkPVwicmVmdW5kX3BvcHVwXCIgcm9sZT1cImRpYWxvZ1wiIGFyaWEtbGFiZWxsZWRieT1cIm15TW9kYWxMYWJlbFwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiID5cclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtb2RhbC1kaWFsb2dcIj5cclxuXHRcdFx0XHRcdFx0ICAgIDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtY29udGVudCBtb2RhbC1sYXJnZVwiPlxyXG5cdFx0XHRcdFx0XHQgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiIGFyaWEtbGFiZWw9XCJDbG9zZVwiPjwvYnV0dG9uPlxyXG5cdFx0XHRcdFx0XHQgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtYm9keVwiPlxyXG5cdFx0XHRcdFx0XHQgICAgICAgICAgICDsg4HshLjrgrTsmqnsnYQg6rCA7KC47JisIOyImCDsl4bsirXri4jri6QuXHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQpO1xyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlKCdjYXInKTpcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gdGhpcy5fZ2V0TGlzdE9wdGlvbnMob3B0Q29kZU1hcCwgMzQwMSk7XHJcblx0XHRcdFx0ZmlsdGVyT3B0aW9ucyA9IHRoaXMuX2FkZE51bVJlc3VsdHMoZmlsdGVyT3B0aW9ucyk7XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0c29ydE9wdGlvbnMgPSB0aGlzLl9nZXRMaXN0T3B0aW9ucyhvcHRDb2RlTWFwLCAzNDAwKTtcclxuXHJcblx0XHRcdFx0Ly90b29sdGlwRWxlbSA9IChcclxuXHRcdFx0Ly9cdDxzcGFuIGNsYXNzTmFtZT1cImJhZGdlXCIgZGF0YS10b2dnbGU9XCJ0b29sdGlwXCIgZGF0YS1wbGFjZW1lbnQ9XCJib3R0b21cIiB0aXRsZT1cIuuztO2XmOuCmOydtCwg7ISx67OEIOuTseydhCDqs6DroKTtlZjsl6wg6rCA7J6F7ZWY7IugIOyDge2SiOuztOuLpCDrs7Ttl5jro4zqsIAg7KCA66C07ZWcLCDsp4DquInspIDruYTsnKjsnbQg64aS7J2ALCDrtoDsp4DquInrpaDsnbQg64Ku7J2AIOyDge2SiOydhCDstpTsspztlbTrk5zrpr3ri4jri6QuXCI+Pzwvc3Bhbj5cclxuXHRcdFx0XHQvLyk7XHJcblxyXG5cdFx0XHRcdHJlc3VsdHNFbGVtID0gcmVzdWx0U3RhdGUucHJvZHVjdERhdGEubWFwKCAoZWxlbSwgaWR4KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQge1xyXG5cdFx0XHRcdFx0XHRpbnN1cmFuY2VfZmVlLFxyXG5cdFx0XHRcdFx0XHRjb21wYW55X25tLFxyXG5cdFx0XHRcdFx0XHRmaWx0ZXIsXHJcblx0XHRcdFx0XHRcdGludGVyZXN0X3luLFxyXG5cdFx0XHRcdFx0XHRwcm9kX2NvZGUsXHJcblx0XHRcdFx0XHRcdGNvbXBhbnlfY29kZSxcclxuXHRcdFx0XHRcdFx0YmlnbyxcclxuXHRcdFx0XHRcdFx0aG9tZXBhZ2VcclxuXHRcdFx0XHRcdH0gPSBlbGVtO1xyXG5cclxuXHRcdFx0XHRcdGlmKHNob3dpbmdDbXByKSB7XHJcblx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cyA9IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn7JewIOuztO2XmOujjCcsXHJcblx0XHRcdFx0XHRcdFx0XHRkaWZmOiBpbnN1cmFuY2VfZmVlIC0gc2VsZWN0ZWRDbXByLmFtb3VudCxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICfsm5AnXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRsZXQgaXNJbnRlcmVzdGVkID0gKGludGVyZXN0X3luID09PSAnWScpO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFx0XHQvLyBUT0RPOiBtb2RpZnkga2V5XHJcblx0XHRcdFx0XHRcdDxQcm9kdWN0Q2FyZFxyXG5cdFx0XHRcdFx0XHRcdGtleT17YGNhcnMke2lkeH1gfVxyXG5cdFx0XHRcdFx0XHRcdHR5cGU9J3R5cGUyJ1xyXG5cdFx0XHRcdFx0XHRcdGNhdGVnb3J5PXtmaWx0ZXJ9XHJcblx0XHRcdFx0XHRcdFx0cHJvdmlkZXI9e2NvbXBhbnlfY29kZX1cclxuXHRcdFx0XHRcdFx0XHRsZWFkaW5nSW5mbz17bnVtYmVyV2l0aENvbW1hcyhlbGVtLmluc3VyYW5jZV9mZWUpfVxyXG5cdFx0XHRcdFx0XHRcdHByZWZpeD0n7JewJ1xyXG5cdFx0XHRcdFx0XHRcdHVuaXQ9J+ybkCdcclxuXHRcdFx0XHRcdFx0XHRwcm9kdWN0VGl0bGU9e2VsZW0uY29tcGFueV9ubX1cclxuXHRcdFx0XHRcdFx0XHRjb21wYXJpc29uQ29udGVudHM9e2NvbXBhcmlzb25Db250ZW50c31cclxuXHRcdFx0XHRcdFx0XHRpbmRleD17aWR4fVxyXG5cdFx0XHRcdFx0XHRcdGlzSW50ZXJlc3RlZD17aXNJbnRlcmVzdGVkfVxyXG5cdFx0XHRcdFx0XHRcdGRlc2NyaXB0aW9uPXtiaWdvfVxyXG5cdFx0XHRcdFx0XHRcdGhhbmRsZUludGVyZXN0Q2xpY2s9e3RoaXMuaGFuZGxlSW50ZXJlc3RDbGljay5iaW5kKG51bGwsIGlzSW50ZXJlc3RlZCwgMzQwMCwgcHJvZF9jb2RlLCBpZHgpfVxyXG5cdFx0XHRcdFx0XHRcdGNob2ljZVJldmVyc2U9e3RydWV9XHJcblx0XHRcdFx0XHRcdFx0Y29tcGFueUxpbms9e2hvbWVwYWdlfVxyXG5cdFx0XHRcdFx0XHQvPlxyXG5cdFx0XHRcdFx0KTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRicmVhaztcclxuXHJcblx0XHRcdGRlZmF1bHQ6XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHtmaWx0ZXJPcHRpb25zLCBzb3J0T3B0aW9ucywgdG9vbHRpcEVsZW0sIHJlc3VsdHNFbGVtLCBoZWxwZXJFbGVtfTtcclxuXHR9LFxyXG5cclxuXHRnZXRQcmVTZWFyY2hWaWV3KHN1Ym1lbnUsIG1hc2tFbGVtLCBmZXRjaGluZ01zZ0VsZW0pIHtcclxuXHRcdGxldCBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHRcdFxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRjYXNlKCdzaGlsc29uJyk6XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlKCdjYXInKTpcclxuXHRcdFx0XHRwcmVTZWFyY2hWaWV3RWxlbSA9IChcclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY2FyX2xpc3RcIj5cclxuXHRcdFx0XHRcdFx0PHNwYW4gY2xhc3NOYW1lPVwibm90aWNlIGJsdWVcIj7rs7Ttl5jrgpjsnbQgLyDsl7DroLntirnslb0gLyDshLHrs4QgLyDsmrTsoITsnpDtlZzsoJUgLyDsoITri7Trs7TqsIDsnoUgLyDtlaDsnbjtlaDspp3rk7HquIkgLyDssKjsooU8L3NwYW4+PHNwYW4gY2xhc3NOYW1lPVwibm90aWNlXCI+7J2EICDsnoXroKXtlbQg64KY7JeQ6rKMIOunnuuKlCDsnpDrj5nssKgg67O07ZeY7J2EIOywvuyVhCDrs7TshLjsmpQuPC9zcGFuPlxyXG5cdFx0XHRcdFx0XHQ8dGFibGUgc3VtbWFyeT1cIuuztO2XmOyCrOuzhCDsnpDrj5nssKgg67O07ZeYIOyDge2SiCDtirnsp5VcIj5cdFxyXG5cdFx0XHRcdFx0XHRcdDxjYXB0aW9uPuuztO2XmOyCrOuzhCDsnpDrj5nssKgg67O07ZeYIOyDge2SiCDtirnsp5U8L2NhcHRpb24+XHJcblx0XHRcdFx0XHRcdFx0PGNvbGdyb3VwPlxyXG5cdFx0XHRcdFx0XHRcdFx0PGNvbCB3aWR0aD1cIjE5NHB4O1wiIC8+XHJcblx0XHRcdFx0XHRcdFx0XHQ8Y29sIHdpZHRoPVwiNjA1cHg7XCIgLz5cclxuXHRcdFx0XHRcdFx0XHQ8L2NvbGdyb3VwPlxyXG5cdFx0XHRcdFx0XHRcdDx0aGVhZD5cclxuXHRcdFx0XHRcdFx0XHRcdDx0cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRoIHNjb3BlPVwiY29sXCI+67O07ZeY7IKsPC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRoIHNjb3BlPVwiY29sXCI+7IOB7ZKI7Yq57KeVPC90aD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0XHRcdFx0PC90aGVhZD5cclxuXHRcdFx0XHRcdFx0XHQ8dGJvZHk+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dHI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZD48aW1nIHNyYz1cIi9yZXNvdXJjZXMvaW1hZ2VzL2NvbW1vbi9pbnNfYmkvaW5zX2JpMDAucG5nXCIgYWx0PVwi64+Z67aA7ZmU7J6sXCIgd2lkdGg9XCIxMDdcIiAvPjwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZCBjbGFzc05hbWU9XCJ0eHRfbGVmdFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSA1MOuFhOyghO2GteydmCDrjIDtlZzrr7zqta0g7LWc7LSIIOyekOuPmeywqOuztO2XmCDtmozsgqw8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSAzNjXsnbwgMjTsi5zqsIQg67mg66W47Lac64+ZIOyEnOu5hOyKpDxici8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQtIEV2ZXJncmVlbu2KueyVvSwg7KO87ZaJ6rGw66asIO2KueyVveuTsSDstpTqsIDtlaDsnbgg7Zic7YOdPGJyLz4gXHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3RkPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC90cj5cclxuXHRcdFx0XHRcdFx0XHRcdDx0cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRkPjxpbWcgc3JjPVwiL3Jlc291cmNlcy9pbWFnZXMvY29tbW9uL2luc19iaS9pbnNfYmkwMS5wbmdcIiBhbHQ9XCLsgrzshLHtmZTsnqxcIiB3aWR0aD1cIjEwN1wiIC8+PC90ZD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRkIGNsYXNzTmFtZT1cInR4dF9sZWZ0XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PHA+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQtIDPrhYTrrLTsgqzqs6Ao7YOA7IKs6rCA7J6F6rK966Cl7Y+s7ZWoKeudvOuptCzrs7Ttl5jro4zsnZggNX44Je2VoOyduCDtmJztg508YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSAxOTnqsJzsnZgg67O07IOB7KCE6rWt7KCB7J24IOuztOyDgeyhsOyngSDsmrTsmIEoMTTrhYQgOeyblCDquLDspIApPGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0g7Zy07J287JeQ64+EIOyatOyYge2VmOuKlCDsmrDsiJjtmJHroKXsoJXruYTsl4XssrQg7Yq57ZmU7ISc67mE7IqkIOygnOqztTxici8+IFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90ZD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dHI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZD48aW1nIHNyYz1cIi9yZXNvdXJjZXMvaW1hZ2VzL2NvbW1vbi9pbnNfYmkvaW5zX2JpMDIucG5nXCIgYWx0PVwi7ZiE64yA7ZW07IOBXCIgd2lkdGg9XCIxMDdcIiAvPjwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZCBjbGFzc05hbWU9XCJ0eHRfbGVmdFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDquIDroZzrsowg6rOg6rCd66eM7KGx64+EIOyekOuPmeywqOuztO2XmOu2gOusuCA464WEIOyXsOyGjTHsnIQ8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDso7ztlonqsbDrpqwg7Yq57JW9IOy1nOuMgCAxMy4yJSDstpTqsIDtlaDsnbg8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDsl6zshLEg7Jq07KCE7J6QIO2KueuzhCDshJzruYTsiqQ8YnIvPiBcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQ+PGltZyBzcmM9XCIvcmVzb3VyY2VzL2ltYWdlcy9jb21tb24vaW5zX2JpL2luc19iaTAzLnBuZ1wiIGFsdD1cIktC7IaQ7ZW067O07ZeYXCIgd2lkdGg9XCIxMDdcIiAvPjwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZCBjbGFzc05hbWU9XCJ0eHRfbGVmdFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDqsIDsobHqtazshLHsm5Ag7Yq57ISx7JeQIOuUsOuluCDri6TslpHtlZwg7Yq57JW9IOq1rOyEsTxici8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQtIOyEnOuvvOydhCDsnITtlZwg67O07ZeY66OMIO2VoOyduCDtmJztg508YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSAo7Z2s66ed64KY64iE6riwLyDtnazrp53rgpjriJTsnpDrj5nssKgg7Yq567OE7JW96rSAIOqwgOyeheyLnCk8YnIvPiBcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQ+PGltZyBzcmM9XCIvcmVzb3VyY2VzL2ltYWdlcy9jb21tb24vaW5zX2JpL2luc19iaTA0LnBuZ1wiIGFsdD1cIu2VnO2ZlOyGkO2VtOuztO2XmFwiIHdpZHRoPVwiMTA3XCIgLz48L3RkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQgY2xhc3NOYW1lPVwidHh0X2xlZnRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8cD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0g7Jm47KCc7LCoIOy2qeuPjOyLnCDtmZXrjIDrs7Tsg4Eg7Yq567OE7JW96rSAPGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0gNjDrhYQg7KCE7Ya17J2YIOuToOuToO2VnCDsooXtlanshpDtlbTrs7Ttl5jsgqw8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSAx64WEIDM2NeydvCAyNOyLnOqwhCDtlajqu5jtlZjripQg67O07IOB7ISc67mE7IqkPGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQ+PGltZyBzcmM9XCIvcmVzb3VyY2VzL2ltYWdlcy9jb21tb24vaW5zX2JpL2luc19iaTA1LnBuZ1wiIGFsdD1cIuuplOumrOy4oO2ZlOyerFwiIHdpZHRoPVwiMTA3XCIgLz48L3RkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQgY2xhc3NOYW1lPVwidHh0X2xlZnRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8cD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0g7KCE6rWtIDIwMOyXrOqwnCDqsIDrp7nsoJDsnYQg7Ya17ZWcIOq4tOq4iey2nOuPmSDshJzruYTsiqQ8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDsnpDssrQg67O07IOB7KGw7KeB66ed7J2EIO2Gte2VnCDsi6Dsho3tlZjqs6Ag7IS47Ius7ZWcIOuztOyDgeyEnOu5hOyKpDxici8+IFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90ZD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dHI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZD48aW1nIHNyYz1cIi9yZXNvdXJjZXMvaW1hZ2VzL2NvbW1vbi9pbnNfYmkvaW5zX2JpMDYucG5nXCIgYWx0PVwi7Z2l6rWt7ZmU7J6sXCIgd2lkdGg9XCIxMDdcIiAvPjwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZCBjbGFzc05hbWU9XCJ0eHRfbGVmdFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDsi6Tsho3tjIwg7Jq07KCE7J6Q66W8IOychO2VnCDri6TslpHtlZwg7Yq57JW9PGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0g7LCo65+JIOyepeywqeusvCDtlaDsnbjsnYQg7Ya17ZW0IO2KueuzhOyalOycqCDsoIHsmqnsnLzroZwg67O07ZeY66OMIO2VoOyduCDtmJztg508YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDsl6zshLEsIOyjvOunkOyatOyghOyekCDrk7Hsl5Dqsowg66ee7LaYIO2KueyVvSDsoJzqs7U8YnIvPiBcclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQ+PGltZyBzcmM9XCIvcmVzb3VyY2VzL2ltYWdlcy9jb21tb24vaW5zX2JpL2luc19iaTA3LnBuZ1wiIGFsdD1cIk1H7IaQ7ZW067O07ZeYXCIgd2lkdGg9XCIxMDdcIiAvPjwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZCBjbGFzc05hbWU9XCJ0eHRfbGVmdFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDquLTquInstpzrj5nshJzruYTsiqQo7Yq57JW96rCA7J6F7IucKTxici8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQtIOu4lOuemeuwleyKpCDsnqXssKnsi5wg7LaU6rCA7ZWg7J24KO2KueyVveqwgOyeheyLnCk8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDrp4jsnbzrpqzsp4Ag7Yq57JW97Jy866GcIOy2lOqwgO2VoOyduCEo7Yq57JW96rCA7J6F7IucKTxici8+IFxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDwvcD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC90ZD5cclxuXHRcdFx0XHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0XHRcdFx0XHQ8dHI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZD48aW1nIHNyYz1cIi9yZXNvdXJjZXMvaW1hZ2VzL2NvbW1vbi9pbnNfYmkvaW5zX2JpMDgucG5nXCIgYWx0PVwiQVhB64uk7J2066CJ7Yq4XCIgd2lkdGg9XCIxMDdcIiAvPjwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDx0ZCBjbGFzc05hbWU9XCJ0eHRfbGVmdFwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxwPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDtm4TtlaDsnbgg66eI7J2866as7KeAIDE3LjQlKOyXsOqwhOyjvO2WieqxsOumrCA1MDAwa20g7J207ZWY7IucKTxici8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQtIOu4lOuemeuwleyKpCDsnqXssKnsi5wgNSUsIDPrhYQg66y07IKs6rOgIDEzLjIlIOy2lOqwgO2VoOyduDxici8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PC9wPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8L3RkPlxyXG5cdFx0XHRcdFx0XHRcdFx0PC90cj5cclxuXHRcdFx0XHRcdFx0XHRcdDx0cj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRkPjxpbWcgc3JjPVwiL3Jlc291cmNlcy9pbWFnZXMvY29tbW9uL2luc19iaS9pbnNfYmkwOS5wbmdcIiBhbHQ9XCLrjZTsvIDsnbTshpDtlbTrs7Ttl5hcIiB3aWR0aD1cIjEwN1wiIC8+PC90ZD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRkIGNsYXNzTmFtZT1cInR4dF9sZWZ0XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PHA+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHQtIDHrp4wy7LKca23snbTtlZgg7KO87ZaJ7IucIOuniOydvOumrOyngCDtirnslb0g7LWc64yAIDI0JSDtlaDsnbg8YnIvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0LSDruJTrnpnrsJXsiqQg7J6l7LCpIDUlLCAz64WEIOustOyCrOqzoOyLnCAxMCUg7LaU6rCA7ZWg7J24PGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQ+PGltZyBzcmM9XCIvcmVzb3VyY2VzL2ltYWdlcy9jb21tb24vaW5zX2JpL2luc19iaTEwLnBuZ1wiIGFsdD1cIuuhr+uNsOyGkO2VtOuztO2XmFwiIHdpZHRoPVwiMTA3XCIgLz48L3RkPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8dGQgY2xhc3NOYW1lPVwidHh0X2xlZnRcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8cD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0g64uk7J2066CJ7Yq466GcIO2VnOuyiCwg7KO87ZaJ6rGw66as66GcIOuYkCDtlZzrsogg7KCI6rCQPGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdC0g7KCc7Zy07Lm065OcIOy1nOuMgCAz66eM7JuQIOyyreq1rCDtlaDsnbggKOy5tOuTnOyCrCDsoJzqs7UpPGJyLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8L3A+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDwvdGQ+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L3RyPlxyXG5cdFx0XHRcdFx0XHRcdDwvdGJvZHk+XHJcblx0XHRcdFx0XHRcdDwvdGFibGU+XHJcblx0XHRcdFx0XHRcdHsgbWFza0VsZW0gfVxyXG5cdFx0XHRcdFx0XHR7IGZldGNoaW5nTXNnRWxlbSB9XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0KTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0ZGVmYXVsdDpcclxuXHRcdFx0XHRcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gcHJlU2VhcmNoVmlld0VsZW07XHJcblx0fVxyXG5cclxuXHJcbiAgICBcclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEluc3VyZVJlc3VsdFNlY3Rpb247XHJcbiIsImxldCBSZWFjdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcclxuXHRTZWxlY3REcm9wZG93blx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9TZWxlY3REcm9wZG93bi5qc3gnKSxcclxuXHRUZXh0SW5wdXRcdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9UZXh0SW5wdXQuanN4JyksXHJcblx0QXBpXHRcdFx0XHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9hcGknKSxcclxuXHRjYWxjSW5zdXJlQWdlXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9udW1iZXJGaWx0ZXJGdW5jdGlvbnMnKS5jYWxjSW5zdXJlQWdlLFxyXG5cdENvbW1vbkFjdGlvbnNcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2FjdGlvbnMvQ29tbW9uQWN0aW9ucycpLFxyXG5cdGZvcm1WYWxpZGF0b3JzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9mb3JtVmFsaWRhdG9ycycpO1xyXG5cclxubGV0IHsgc2VsZWN0UmVxdWlyZWQsIG51bWJlclJlcXVpcmVkIH0gPSBmb3JtVmFsaWRhdG9ycztcclxuXHJcbmNvbnN0IHN1Ym1lbnVfY29kZXMgPSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29uc3RhbnRzL3N1Ym1lbnVfY29kZXMnKTtcclxuXHJcblxyXG5cclxuY2xhc3MgU2VsZWN0T3B0cyB7XHJcblx0Y29uc3RydWN0b3IobGFiZWwsIHZhbHVlLCBkYXRhKSB7XHJcblx0XHR0aGlzLmxhYmVsID0gbGFiZWw7XHJcblx0XHR0aGlzLnZhbHVlID0gdmFsdWU7XHJcblx0XHR0aGlzLmRhdGEgPSBkYXRhO1xyXG5cdH1cclxufVxyXG5cclxuXHJcbmxldCBJbnN1cmVQcm9kdWN0QWRkZXIgPSBSZWFjdC5jcmVhdGVDbGFzcyh7XHJcbiAgICBwcm9wVHlwZXM6IHtcclxuXHRcdHN1Ym1lbnU6IFJlYWN0LlByb3BUeXBlcy5zdHJpbmcsXHJcblx0XHRzZWFyY2hFbnRyaWVzOiBSZWFjdC5Qcm9wVHlwZXMuYXJyYXksXHJcblx0XHRvcHRDb2RlTWFwOiBSZWFjdC5Qcm9wVHlwZXMub2JqZWN0XHJcbiAgICB9LFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHRzdWJtZW51OiAnJyxcclxuXHRcdFx0c2VhcmNoRW50cmllczogW10sXHJcblx0XHRcdG9wdENvZGVNYXA6IHt9LFxyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cdFxyXG5cdGdldEluaXRpYWxTdGF0ZTogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuXHRcdFx0c2hvd1ZhbGlkYXRpb246IGZhbHNlLFxyXG5cdFx0XHRxdWVyeToge1xyXG5cdFx0XHRcdHByb2RfbmFtZTogJydcclxuXHRcdFx0fSxcclxuXHRcdFx0Y29tcGFueU9wdHM6IFtdLFxyXG5cdFx0XHRwcm9kTmFtZU9wdHM6IFtdLFxyXG5cdFx0XHRwcm9kX2NvZGU6ICcnLFxyXG5cdFx0XHRwcmljZTogJycsXHJcblx0XHRcdHByb2ROYW1lUGxhY2Vob2xkZXI6ICfsobDqsbTsnYQg66qo65GQIOyeheugpe2VtOyjvOyEuOyalCcsXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIFxyXG5cclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG5cdFx0bGV0IG1lbnVfY2QgPSBzdWJtZW51X2NvZGVzW3RoaXMucHJvcHMuc3VibWVudV07XHJcblx0XHRcclxuXHRcdEFwaS5wb3N0KCcvbW9waWMvYXBpL2lucy9zZWxlY3RBbGxDb21wYW55TGlzdEJ5SW5zVHlwZScsIHttZW51X2NkfSlcclxuXHRcdFx0LnRoZW4oZnVuY3Rpb24gKHJlcykge1xyXG5cdFx0XHRcdGlmKHJlcy5yZXN1bHQucmVzdWx0Q29kZSA9PT0gJ1NVQ0NFU1MnKSB7XHJcblx0XHRcdFx0XHRsZXQgcmVzdWx0RGF0YSA9IHJlcy5yZXN1bHQuZGF0YTtcclxuXHRcdFx0XHRcdGxldCBvcHRpb25zID0gKCByZXN1bHREYXRhLm1hcCgoZSkgPT4gbmV3IFNlbGVjdE9wdHMoZS5jb21wYW55X25tLCBlLmNvbXBhbnlfY29kZSkpICk7XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRcdFx0XHRjb21wYW55T3B0czogb3B0aW9uc1xyXG5cdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LmJpbmQodGhpcykpO1xyXG5cclxuXHRcdC8vIGNsZWFyIHdoZW4gY2xvc2VkXHJcblx0XHQkKCcjcHJvZHVjdF9hZGRfcG9wdXAnKS5vbignaGlkZGVuLmJzLm1vZGFsJywgdGhpcy5jbGVhclN0YXRzKTtcclxuXHJcblxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuICAgIHJlbmRlcigpIHtcclxuXHRcdGxldCB7IHF1ZXJ5LCBjb21wYW55T3B0cywgcHJvZE5hbWVPcHRzLCBwcmljZSwgcHJvZF9jb2RlLCBwcm9kTmFtZVBsYWNlaG9sZGVyIH0gPSB0aGlzLnN0YXRlO1xyXG5cdFx0bGV0IHsgc3VibWVudSwgc2VhcmNoRW50cmllcywgb3B0Q29kZU1hcCB9ID0gdGhpcy5wcm9wcztcclxuXHJcblx0XHRsZXQgY29tcGFueUZvcm0gPSAoXHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNlwiPlxyXG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuq4sOq0gOuqhTwvbGFiZWw+XHJcblx0XHRcdFx0XHQ8U2VsZWN0RHJvcGRvd25cclxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9J+yEoO2DnSdcclxuXHRcdFx0XHRcdFx0b3B0aW9ucz17Y29tcGFueU9wdHN9XHJcblx0XHRcdFx0XHRcdGhhbmRsZVNlbGVjdD17dGhpcy5jaGFuZ2VTZWxlY3RRdWVyeS5iaW5kKG51bGwsICdjb21wYW55X2NvZGUnLCBudWxsKX1cclxuXHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e3F1ZXJ5LmNvbXBhbnlfY29kZX1cclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXt0aGlzLnN0YXRlLnNob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxyXG5cdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cdFx0XHJcblx0XHRsZXQgYmFzaWNzRm9ybSA9IHNlYXJjaEVudHJpZXMubWFwKCAoZW50cnkpID0+IHtcclxuXHRcdFx0bGV0IHF1ZXJ5RWxlbTtcclxuXHJcblx0XHRcdGlmKGVudHJ5LmlkID09PSAnYWdlX2NvbnRyYWN0Jykge1xyXG5cdFx0XHRcdGlmKHF1ZXJ5LmFnZSkge1xyXG5cdFx0XHRcdFx0bGV0IGFnZVNlbGVjdGVkID0gb3B0Q29kZU1hcFszNDA2XS5maW5kKChlKSA9PiBlLmNkID09IHF1ZXJ5LmFnZSk7XHJcblx0XHRcdFx0XHQvLyBjYXV0aW9uOiAgdXNpbmcgJz09JyBzaW5jZSBjb252ZXJ0aW5nIGludG8gc3RyaW5nXHJcblxyXG5cdFx0XHRcdFx0aWYoYWdlU2VsZWN0ZWQgJiYgYWdlU2VsZWN0ZWQucmVmMSkge1xyXG5cdFx0XHRcdFx0XHRsZXQgb3B0cyA9IFtdO1xyXG5cdFx0XHRcdFx0XHRsZXQgcmF3T3B0cyA9IEpTT04ucGFyc2UoYHsgJHthZ2VTZWxlY3RlZC5yZWYxfSB9YCk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRmb3IgKGxldCBjZCBpbiByYXdPcHRzKSB7XHJcblx0XHRcdFx0XHRcdFx0b3B0cy5wdXNoKHtcclxuXHRcdFx0XHRcdFx0XHRcdGxhYmVsOiByYXdPcHRzW2NkXSxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBjZFxyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdGVudHJ5Lm9wdGlvbnMgPSBvcHRzO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdFx0c3dpdGNoKGVudHJ5LnR5cGUpIHtcclxuXHRcdFx0XHRjYXNlKCdudW1iZXInKTpcclxuXHRcdFx0XHRjYXNlKCdudW1iZXItd2l0aC1jb21tYXMnKTpcclxuXHRcdFx0XHRjYXNlKCd0ZXh0Jyk6XHJcblx0XHRcdFx0XHRxdWVyeUVsZW0gPSA8VGV4dElucHV0XHJcblx0XHRcdFx0XHRcdFx0XHRcdHR5cGU9e2VudHJ5LnR5cGV9XHJcblx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtlbnRyeS5wbGFjZWhvbGRlciB8fCAnICd9XHJcblx0XHRcdFx0XHRcdFx0XHRcdGlucHV0Q2xhc3M9J3RleHQtaW5wdXQtbm9ybWFsJ1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRoYW5kbGVDaGFuZ2U9e3RoaXMuaGFuZGxlVGV4dElucHV0Q2hhbmdlLmJpbmQobnVsbCwgZW50cnkuaWQpfVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg9e2VudHJ5Lm1heGxlbmd0aH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0dmFsdWU9e3F1ZXJ5W2VudHJ5LmlkXX1cclxuXHRcdFx0XHRcdFx0XHRcdFx0dW5pdD17ZW50cnkudW5pdCB8fCAnJ31cclxuXHRcdFx0XHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXt0aGlzLnN0YXRlLnNob3dWYWxpZGF0aW9uICYmIGVudHJ5LnZhbGlkYXRpb25Gbn1cclxuXHRcdFx0XHRcdFx0XHRcdC8+O1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSgnbnVtRml4ZWQnKTpcclxuXHRcdFx0XHRcdHF1ZXJ5RWxlbSA9IDxUZXh0SW5wdXRcclxuXHRcdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9JyAnXHJcblx0XHRcdFx0XHRcdFx0XHRcdGlucHV0Q2xhc3M9J3RleHQtaW5wdXQtbm9ybWFsJ1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg9e2VudHJ5Lm1heGxlbmd0aH1cclxuXHRcdFx0XHRcdFx0XHRcdFx0Zml4ZWQ9e3RydWV9XHJcblx0XHRcdFx0XHRcdFx0XHRcdHZhbHVlPXtlbnRyeS5maXhlZEFzfVxyXG5cdFx0XHRcdFx0XHRcdFx0XHR1bml0PXtlbnRyeS51bml0IHx8ICcnfSAvPjtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UoJ3NlbGVjdCcpOlxyXG5cdFx0XHRcdFx0cXVlcnlFbGVtID0gPFNlbGVjdERyb3Bkb3duXHJcblx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtlbnRyeS5wbGFjZWhvbGRlciB8fCAn7ISg7YOdJ31cclxuXHRcdFx0XHRcdFx0XHRcdFx0b3B0aW9ucz17ZW50cnkub3B0aW9uc31cclxuXHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmNoYW5nZVNlbGVjdFF1ZXJ5LmJpbmQobnVsbCwgZW50cnkuaWQsIGVudHJ5LmRlcGVuZGVuY2llcyl9XHJcblx0XHRcdFx0XHRcdFx0XHRcdGNsYXNzTmFtZT17ZW50cnkuY2xhc3NOYW1lIHx8ICdzZWxlY3QtZHJvcGRvd24nfVxyXG5cdFx0XHRcdFx0XHRcdFx0XHRzZWxlY3RlZD17cXVlcnlbZW50cnkuaWRdfVxyXG5cdFx0XHRcdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3RoaXMuc3RhdGUuc2hvd1ZhbGlkYXRpb24gJiYgZW50cnkudmFsaWRhdGlvbkZufVxyXG5cdFx0XHRcdFx0XHRcdFx0Lz47XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFx0cXVlcnlFbGVtID0gdW5kZWZpbmVkO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCIga2V5PXtgYWRkZXItJHtlbnRyeS5sYWJlbH1gfT5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTZcIj5cclxuXHRcdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPntlbnRyeS5sYWJlbCB8fCAnJ308L2xhYmVsPlxyXG5cdFx0XHRcdFx0XHR7IHF1ZXJ5RWxlbSB9XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0KTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdGxldCBwcm9kdWN0TmFtZUZvcm0gPSAoXHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNlwiPlxyXG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuyDge2SiOuqhTwvbGFiZWw+XHJcblx0XHRcdFx0XHQ8U2VsZWN0RHJvcGRvd25cclxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e3Byb2ROYW1lUGxhY2Vob2xkZXJ9XHJcblx0XHRcdFx0XHRcdG9wdGlvbnM9e3Byb2ROYW1lT3B0c31cclxuXHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVByb2R1Y3ROYW1lU2VsZWN0fVxyXG5cdFx0XHRcdFx0XHRzZWxlY3RlZD17cHJvZF9jb2RlfVxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3RoaXMuc3RhdGUuc2hvd1ZhbGlkYXRpb24gJiYgc2VsZWN0UmVxdWlyZWR9XHJcblx0XHRcdFx0XHQvPlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdCk7XHJcblxyXG5cdFx0bGV0IHByaWNlTGFiZWwgPSBzdWJtZW51LnRvTG93ZXJDYXNlKCkgPT09ICdjYXInID8gJ+uztO2XmOujjCcgOiAn7JuUIOuztO2XmOujjCc7XHJcblx0XHRsZXQgcHJpY2VGb3JtID0gKFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTZcIj5cclxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9XCJzZWxlY3QtbGFiZWxcIj57cHJpY2VMYWJlbH08L2xhYmVsPlxyXG5cdFx0XHRcdFx0PFRleHRJbnB1dFxyXG5cdFx0XHRcdFx0XHR0eXBlPSdudW1iZXItd2l0aC1jb21tYXMnXHJcblx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPScgJ1xyXG5cdFx0XHRcdFx0XHRpbnB1dENsYXNzPSd0ZXh0LWlucHV0LW5vcm1hbCdcclxuXHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZVByaWNlQ2hhbmdlfVxyXG5cdFx0XHRcdFx0XHR2YWx1ZT17cHJpY2V9XHJcblx0XHRcdFx0XHRcdHVuaXQ9J+ybkCdcclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXt0aGlzLnN0YXRlLnNob3dWYWxpZGF0aW9uICYmIG51bWJlclJlcXVpcmVkfVxyXG5cdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cclxuXHRcdFxyXG5cdFx0XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT17YG1vZGFsIGZhZGUgJHtzdWJtZW51LnRvTG93ZXJDYXNlKCl9YH0gaWQ9XCJwcm9kdWN0X2FkZF9wb3B1cFwiPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtZGlhbG9nXCI+XHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWNvbnRlbnQgbW9kYWwtc21hbGxcIj5cclxuXHJcblx0XHRcdFx0XHRcdDxhIGNsYXNzTmFtZT1cImNsb3NlXCIgZGF0YS1kaXNtaXNzPVwibW9kYWxcIiBhcmlhLWxhYmVsPVwiQ2xvc2VcIiAvPlxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtb2RhbC1oZWFkZXJcIj5cclxuXHRcdFx0XHRcdFx0XHQ8aDQgY2xhc3NOYW1lPVwibW9kYWwtdGl0bGVcIiBpZD1cIm15TW9kYWxMYWJlbFwiPuqwgOyeheyDge2SiCDstpTqsIDtlZjquLA8L2g0PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWJvZHlcIj5cclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInByb2R1Y3RfYWRkXCI+XHJcblxyXG5cdFx0XHRcdFx0XHRcdFx0e2NvbXBhbnlGb3JtfVxyXG5cdFx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0XHR7YmFzaWNzRm9ybX1cclxuXHJcblx0XHRcdFx0XHRcdFx0XHR7cHJvZHVjdE5hbWVGb3JtfVxyXG5cclxuXHRcdFx0XHRcdFx0XHRcdHtwcmljZUZvcm19XHJcblx0XHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRcdHsvKjxzcGFuIGNsYXNzTmFtZT1cInR4dFwiPijquIjrpqzrpbwg7J6F66Cl7ZWY7KeAIOyViuycvOyLpCDqsr3smrAsIOuTseuhneydvCDtmITsnqwg6riw7KSAIOq4iOumrOuhnCDsoIHsmqnrkKnri4jri6QuKTwvc3Bhbj4qL31cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY2VudGVyX2J0blwiPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQ8YSBjbGFzc05hbWU9XCJidG4gYnRuLXN1Ym1pdFwiIHJvbGU9XCJidXR0b25cIiBvbkNsaWNrPXt0aGlzLnN1Ym1pdFByb2R1Y3R9Pu2ZleyduDwvYT5cclxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdDwvZGl2PlxyXG5cdFx0KTtcclxuICAgIH0sXHJcblxyXG5cclxuXHRjaGFuZ2VTZWxlY3RRdWVyeShxdWVyeUF0dHIsIGRlcGVuZGVuY2llcywgZXZ0KSB7XHJcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcclxuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xyXG5cdFx0XHRcclxuXHRcdGxldCB2YWwgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpIHx8IHRhcmdldC50ZXh0Q29udGVudDtcclxuXHRcdGxldCBxdWVyeSA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuc3RhdGUucXVlcnksIHtbcXVlcnlBdHRyXTogdmFsfSk7XHJcblxyXG5cdFx0aWYoZGVwZW5kZW5jaWVzICYmIGRlcGVuZGVuY2llcy5sZW5ndGggPiAwKSB7XHJcblx0XHRcdGZvcihsZXQgaSA9IDAsIGwgPSBkZXBlbmRlbmNpZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcblx0XHRcdFx0bGV0IGRlcGVuZGVuY3kgPSBkZXBlbmRlbmNpZXNbaV07XHJcblx0XHRcdFx0cXVlcnlbZGVwZW5kZW5jeV0gPSAnJztcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRxdWVyeTogcXVlcnksXHJcblx0XHRcdHByb2RfY29kZTogJycsXHJcblx0XHRcdHByaWNlOiAnJyxcclxuXHRcdFx0cHJvZE5hbWVPcHRzOiBbXSxcclxuXHRcdFx0cHJvZE5hbWVQbGFjZWhvbGRlcjogJ+yhsOqxtOydhCDrqqjrkZAg7J6F66Cl7ZW07KO87IS47JqUJ1xyXG5cdFx0fSwgZnVuY3Rpb24oKSB7XHJcblx0XHRcdGlmKHRoaXMuY2hlY2tWYWxpZGl0eSgpKSB7XHJcblx0XHRcdFx0dGhpcy5nZXRQcm9kdWN0TmFtZXModGhpcy5wcm9wcy5zdWJtZW51LCBxdWVyeSk7XHJcblx0XHRcdH1cclxuXHRcdH0uYmluZCh0aGlzKSk7XHJcblx0fSxcclxuXHJcblx0aGFuZGxlVGV4dElucHV0Q2hhbmdlKHF1ZXJ5QXR0ciwgZXZ0KSB7XHJcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcclxuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xyXG5cdFx0XHJcblx0XHRsZXQgdmFsID0gdGFyZ2V0LnZhbHVlO1xyXG5cdFx0bGV0IHF1ZXJ5ID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5zdGF0ZS5xdWVyeSwge1txdWVyeUF0dHJdOiB2YWx9KTtcclxuXHRcdFxyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7ICAvLyByZXNldCBwcm9kX2NvZGUgYW5kIHByaWNlIGFzIHdlbGxcclxuXHRcdFx0cXVlcnk6IHF1ZXJ5LFxyXG5cdFx0XHRwcm9kX2NvZGU6ICcnLFxyXG5cdFx0XHRwcmljZTogJycsXHJcblx0XHRcdHByb2ROYW1lT3B0czogW10sXHJcblx0XHRcdHByb2ROYW1lUGxhY2Vob2xkZXI6ICfsobDqsbTsnYQg66qo65GQIOyeheugpe2VtOyjvOyEuOyalCdcclxuXHRcdH0sIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRpZih0aGlzLmNoZWNrVmFsaWRpdHkoKSkge1xyXG5cdFx0XHRcdHRoaXMuZ2V0UHJvZHVjdE5hbWVzKHRoaXMucHJvcHMuc3VibWVudSwgcXVlcnkpO1xyXG5cdFx0XHR9XHJcblx0XHR9LmJpbmQodGhpcykpO1xyXG5cclxuXHR9LFxyXG5cclxuXHRoYW5kbGVQcmljZUNoYW5nZShldnQpIHtcclxuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xyXG5cdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XHJcblx0XHRcclxuXHRcdGxldCB2YWwgPSB0YXJnZXQudmFsdWU7XHJcblx0XHRcclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRwcmljZTogdmFsXHJcblx0XHR9KTtcclxuXHR9LFxyXG5cclxuXHRnZXRQcm9kdWN0TmFtZXMoc3VibWVudSwgcXVlcnlTdGF0ZSkge1xyXG5cdFx0bGV0IG1lbnVfY2QgPSBzdWJtZW51X2NvZGVzW3RoaXMucHJvcHMuc3VibWVudV07XHJcblx0XHRsZXQgcXVlcnkgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnN0YXRlLnF1ZXJ5LCB7bWVudV9jZH0pO1xyXG5cclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRwcm9kTmFtZU9wdHM6IFtdLFxyXG5cdFx0XHRwcm9kTmFtZVBsYWNlaG9sZGVyOiAn7KGw6rG07J2EIOuqqOuRkCDsnoXroKXtlbTso7zshLjsmpQnXHJcblx0XHR9KTtcclxuXHRcdFxyXG5cdFx0Zm9yKGxldCBrZXkgaW4gcXVlcnkpIHtcclxuXHRcdFx0bGV0IHZhbCA9IHF1ZXJ5W2tleV07XHJcblx0XHRcdFxyXG5cdFx0XHRpZihrZXkgPT09ICdiaXJ0aENvbnZlcnRUb0FnZScpIHtcclxuXHRcdFx0XHRxdWVyeVsnYWdlJ10gPSBTdHJpbmcoY2FsY0luc3VyZUFnZSh2YWwpKTtcclxuXHRcdFx0XHRkZWxldGUgcXVlcnlba2V5XTtcclxuXHJcblx0XHRcdFx0Y29udGludWU7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC8vIG51bWJlciAnOTk5OTk5OTknOiBub24tc2VuZGluZyBjb2RlXHJcblx0XHRcdGlmKHZhbCA9PSA5OTk5OTk5OSkgeyAgLy8gQ2F1dGlvbiAnPT0nOiBjaGVjayAnOTk5OTk5OTknIHN0cmluZ1xyXG5cdFx0XHRcdGRlbGV0ZSBxdWVyeVtrZXldO1xyXG5cdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdHF1ZXJ5W2tleV0gPSBTdHJpbmcodmFsKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHJcblx0XHRBcGkucG9zdCgnL21vcGljL2FwaS9pbnMvZmlsdGVyUHJvZHVjdEJ5TmFtZScsIHF1ZXJ5KVxyXG5cdFx0XHQudGhlbihmdW5jdGlvbihyZXMpIHtcclxuXHRcdFx0XHRpZihyZXMucmVzdWx0LnJlc3VsdENvZGUgPT09ICdTVUNDRVNTJykge1xyXG5cdFx0XHRcdFx0bGV0IHJlc3VsdERhdGEgPSByZXMucmVzdWx0LmRhdGE7XHJcblx0XHRcdFx0XHRsZXQgb3B0cyA9IHJlc3VsdERhdGEubWFwKChlKSA9PiBuZXcgU2VsZWN0T3B0cyhlLnByb2RfbmFtZSwgZS5wcm9kX2NvZGUsIGUpKTtcclxuXHJcblx0XHRcdFx0XHRpZihvcHRzLmxlbmd0aCA9PT0gMCkge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdFx0XHRwcm9kTmFtZVBsYWNlaG9sZGVyOiAn7KGw6rG07JeQIOunnuuKlCDsg4HtkojsnbQg7KG07J6s7ZWY7KeAIOyViuyKteuLiOuLpCdcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdFx0XHRwcm9kTmFtZVBsYWNlaG9sZGVyOiAn7ISg7YOdJyxcclxuXHRcdFx0XHRcdFx0XHRwcm9kTmFtZU9wdHM6IG9wdHNcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fS5iaW5kKHRoaXMpKTtcclxuXHJcblx0fSxcclxuXHJcblx0aGFuZGxlUHJvZHVjdE5hbWVTZWxlY3QoZXZ0KSB7XHJcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcclxuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xyXG5cdFx0XHJcblx0XHRsZXQgdmFsID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ2xpJykuYXR0cignZGF0YS12YWwnKSB8fCB0YXJnZXQudGV4dENvbnRlbnQ7XHJcblx0XHRsZXQgcHJpY2UgPSB0aGlzLnN0YXRlLnByb2ROYW1lT3B0cy5maW5kKChlKSA9PiAoZS52YWx1ZSA9PT0gdmFsKSkuZGF0YS5pbnN1cmFuY2VfZmVlOyBcclxuXHJcblx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0cHJvZF9jb2RlOiB2YWwsXHJcblx0XHRcdHByaWNlOiBTdHJpbmcocHJpY2UpXHJcblx0XHR9KTtcclxuXHR9LFxyXG5cclxuXHRzdWJtaXRQcm9kdWN0KGV2dCkge1xyXG5cdFx0ZXZ0ID0gZXZ0PyBldnQgOiB3aW5kb3cuZXZlbnQ7XHJcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcclxuXHRcdFx0XHJcblx0XHRsZXQgeyBxdWVyeSwgY29tcGFueU9wdHMsIHByb2ROYW1lT3B0cywgcHJpY2UsIHByb2RfY29kZSB9ID0gdGhpcy5zdGF0ZTtcclxuXHRcdGxldCB7IHN1Ym1lbnUsIHNlYXJjaEVudHJpZXMgfSA9IHRoaXMucHJvcHM7XHJcblx0XHRsZXQgZGF0YTtcclxuXHJcblx0XHRsZXQgYWxsVmFsaWQgPSB0aGlzLmNoZWNrVmFsaWRpdHkoKTtcclxuXHJcblx0XHRpZihhbGxWYWxpZCkge1xyXG5cdFx0XHRcclxuXHRcdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRcdGNhc2UoJ3NoaWxzb24nKTpcclxuXHRcdFx0XHRcdGRhdGEgPSB7XHJcblx0XHRcdFx0XHRcdHByb2R1Y3RfdHlwZTogU3RyaW5nKHN1Ym1lbnVfY29kZXNbc3VibWVudV0pLFxyXG5cdFx0XHRcdFx0XHRwcm9kX2NvZGU6IFN0cmluZyhwcm9kX2NvZGUpLFxyXG5cdFx0XHRcdFx0XHRwcm9kX25hbWU6IFN0cmluZyhwcm9kTmFtZU9wdHMuZmluZCgoZWxlbSkgPT4gKGVsZW0udmFsdWUgPT09IHByb2RfY29kZSkpLmxhYmVsKSxcclxuXHRcdFx0XHRcdFx0YW1vdW50OiBTdHJpbmcocHJpY2UpLFxyXG5cdFx0XHRcdFx0XHRhZ2U6IFN0cmluZyhjYWxjSW5zdXJlQWdlKHF1ZXJ5LmJpcnRoQ29udmVydFRvQWdlKSksXHJcblx0XHRcdFx0XHRcdHNleDogU3RyaW5nKHF1ZXJ5LnNleCksXHJcblx0XHRcdFx0XHRcdGRhbWJvX3NoaWxzb246IFN0cmluZyhxdWVyeS5kYW1ibzEpXHJcblx0XHRcdFx0XHR9O1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0Y2FzZSgnbGlmZScpOlxyXG5cdFx0XHRcdFx0ZGF0YSA9IHtcclxuXHRcdFx0XHRcdFx0cHJvZHVjdF90eXBlOiBTdHJpbmcoc3VibWVudV9jb2Rlc1tzdWJtZW51XSksXHJcblx0XHRcdFx0XHRcdHByb2RfY29kZTogU3RyaW5nKHByb2RfY29kZSksXHJcblx0XHRcdFx0XHRcdHByb2RfbmFtZTogU3RyaW5nKHByb2ROYW1lT3B0cy5maW5kKChlbGVtKSA9PiAoZWxlbS52YWx1ZSA9PT0gcHJvZF9jb2RlKSkubGFiZWwpLFxyXG5cdFx0XHRcdFx0XHRhbW91bnQ6IFN0cmluZyhwcmljZSksXHJcblx0XHRcdFx0XHRcdHNleDogU3RyaW5nKHF1ZXJ5LnNleCksXHJcblx0XHRcdFx0XHRcdGFnZTogXCI0MFwiLFxyXG5cdFx0XHRcdFx0fTtcclxuXHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdGNhc2UoJ2FubnVpdHknKTpcclxuXHRcdFx0XHRcdGRhdGEgPSB7XHJcblx0XHRcdFx0XHRcdHByb2R1Y3RfdHlwZTogU3RyaW5nKHN1Ym1lbnVfY29kZXNbc3VibWVudV0pLFxyXG5cdFx0XHRcdFx0XHRwcm9kX2NvZGU6IFN0cmluZyhwcm9kX2NvZGUpLFxyXG5cdFx0XHRcdFx0XHRwcm9kX25hbWU6IFN0cmluZyhwcm9kTmFtZU9wdHMuZmluZCgoZWxlbSkgPT4gKGVsZW0udmFsdWUgPT09IHByb2RfY29kZSkpLmxhYmVsKSxcclxuXHRcdFx0XHRcdFx0YW1vdW50OiBTdHJpbmcocHJpY2UpLFxyXG5cdFx0XHRcdFx0XHRhZ2U6IFN0cmluZyhxdWVyeS5hZ2UpLFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0c2V4OiBTdHJpbmcocXVlcnkuc2V4KSxcclxuXHRcdFx0XHRcdH07XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRjYXNlKCdjYXInKTpcclxuXHRcdFx0XHRcdGRhdGEgPSB7XHJcblx0XHRcdFx0XHRcdHByb2R1Y3RfdHlwZTogU3RyaW5nKHN1Ym1lbnVfY29kZXNbc3VibWVudV0pLFxyXG5cdFx0XHRcdFx0XHRwcm9kX2NvZGU6IFN0cmluZyhwcm9kX2NvZGUpLFxyXG5cdFx0XHRcdFx0XHRwcm9kX25hbWU6IFN0cmluZyhwcm9kTmFtZU9wdHMuZmluZCgoZWxlbSkgPT4gKGVsZW0udmFsdWUgPT09IHByb2RfY29kZSkpLmxhYmVsKSwgXHJcblx0XHRcdFx0XHRcdGFtb3VudDogU3RyaW5nKHByaWNlKSxcclxuXHRcdFx0XHRcdFx0YWdlOiBTdHJpbmcocXVlcnkuYWdlKSxcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdHNleDogU3RyaW5nKHF1ZXJ5LnNleCksXHJcblx0XHRcdFx0XHRcdGFnZV9jb250cmFjdDogU3RyaW5nKHF1ZXJ5LmFnZV9jb250cmFjdCksXHJcblx0XHRcdFx0XHRcdGRyaXZlcjogU3RyaW5nKHF1ZXJ5LmRyaXZlciksXHJcblx0XHRcdFx0XHRcdGRhbWJvOiBTdHJpbmcocXVlcnkuZGFtYm8pLFxyXG5cdFx0XHRcdFx0XHRkY19ncmFkZTogU3RyaW5nKHF1ZXJ5LmRjX2dyYWRlKSxcclxuXHRcdFx0XHRcdFx0Y2FyX3R5cGU6IFN0cmluZyhxdWVyeS5jYXJfdHlwZSksXHJcblx0XHRcdFx0XHR9O1xyXG5cdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdH1cclxuXHRcdFx0XHJcblx0XHRcdGxldCAkdGFyZ2V0ID0gJCh0YXJnZXQpO1x0XHRcdFxyXG5cdFx0XHRDb21tb25BY3Rpb25zLmFkZE15UHJvZHVjdChkYXRhLCAoKSA9PiB7XHJcblx0XHRcdFx0JHRhcmdldC5jbG9zZXN0KCcubW9kYWwnKS5tb2RhbCgnaGlkZScpO1xyXG5cdFx0XHR9KTtcclxuXHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRzaG93VmFsaWRhdGlvbjogdHJ1ZVxyXG5cdFx0XHR9KVxyXG5cdFx0fVxyXG5cdH0sXHJcblxyXG5cdGNsZWFyU3RhdHMoKSB7XHJcblx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0cXVlcnk6IHtcclxuXHRcdFx0XHRwcm9kX25hbWU6ICcnXHJcblx0XHRcdH0sXHJcblx0XHRcdHByb2ROYW1lT3B0czogW10sXHJcblx0XHRcdHByb2RfY29kZTogJycsXHJcblx0XHRcdHByaWNlOiAnJyxcclxuXHRcdFx0c2hvd1ZhbGlkYXRpb246IGZhbHNlXHJcblx0XHR9KTtcclxuXHR9LFxyXG5cclxuXHRjaGVja1ZhbGlkaXR5KCkge1xyXG5cdFx0bGV0IGFsbFZhbGlkID0gdHJ1ZTtcclxuXHRcdGxldCB7IHF1ZXJ5IH0gPSB0aGlzLnN0YXRlO1xyXG5cdFx0bGV0IHsgc2VhcmNoRW50cmllcyB9ID0gdGhpcy5wcm9wcztcclxuXHRcdFxyXG5cdFx0Zm9yKGxldCBpID0gMCwgbCA9IHNlYXJjaEVudHJpZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XHJcblx0XHRcdGxldCBlbnRyeSA9IHNlYXJjaEVudHJpZXNbaV07XHJcblx0XHRcdFxyXG5cdFx0XHRpZihlbnRyeS52YWxpZGF0aW9uRm4pIHtcclxuXHRcdFx0XHRhbGxWYWxpZCA9IGFsbFZhbGlkICYmIChlbnRyeS52YWxpZGF0aW9uRm4ocXVlcnlbZW50cnkuaWRdKSA9PT0gdHJ1ZSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gYWxsVmFsaWQ7XHJcblx0fVxyXG5cdFxyXG5cdFxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSW5zdXJlUHJvZHVjdEFkZGVyO1xyXG4iLCJsZXQgUmVhY3RcdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcclxuXHRudW1iZXJXaXRoQ29tbWFzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9udW1iZXJGaWx0ZXJGdW5jdGlvbnMnKS5udW1iZXJXaXRoQ29tbWFzLFxyXG5cdGZvcm1WYWxpZGF0b3JzXHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2Zvcm1WYWxpZGF0b3JzJyk7XHJcblxyXG5sZXQgeyBzZWxlY3RSZXF1aXJlZCwgcmVxdWlyZWRCaXJ0aGRhdGUgfSA9IGZvcm1WYWxpZGF0b3JzO1xyXG5cclxuXHJcbmxldCBJbnN1cmVIZWxwZXJTZWN0aW9uID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xyXG5cdG1peGluczogW1xyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3F1ZXJ5U3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3Byb2R1Y3RTdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvY29tcGFyaXNvblN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9zZW5kUXVlcnlNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2hlbHBlclNlY3Rpb25NaXhpbi5qc3gnKSxcclxuXHRdLFxyXG5cdFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHRcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgIFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBcclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG5cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcblxyXG4gICAgfSxcclxuXHJcblx0X2dldFNlYXJjaEVudHJpZXMocXVlcnlTdGF0ZSkge1xyXG5cdFx0bGV0IHtzdWJtZW51LCBzZWFyY2hRdWVyeSwgb3B0Q29kZU1hcH0gPSBxdWVyeVN0YXRlO1xyXG5cdFx0bGV0IGVudHJpZXM7XHJcblxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRjYXNlKCdzaGlsc29uJyk6XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfsg53rhYTsm5TsnbwnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnbnVtYmVyJyxcclxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI6ICdZWU1NREQnLFxyXG5cdFx0XHRcdFx0XHRtYXhsZW5ndGg6ICc2JyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdiaXJ0aENvbnZlcnRUb0FnZScsIC8vIGNvbnZlcnQgdG8gaW5zdXJlIGFnZSB3aGVuIHNlbmRpbmcgcmVxdWVzdHNcclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuOiByZXF1aXJlZEJpcnRoZGF0ZSxcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn7ISx67OEJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnc2V4JyxcclxuXHRcdFx0XHRcdFx0b3B0aW9uczogdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIDEyMzQpLFxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm46IHNlbGVjdFJlcXVpcmVkXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRsYWJlbDogJ+yEoO2DneuLtOuztCcsXHJcblx0XHRcdFx0XHRcdHR5cGU6ICdzZWxlY3QnLFxyXG5cdFx0XHRcdFx0XHRpZDogJ2RhbWJvMScsXHJcblx0XHRcdFx0XHRcdG9wdGlvbnM6IHRoaXMuZ2V0U2VsZWN0T3B0aW9ucyhvcHRDb2RlTWFwLCAzMTAyKSxcclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuOiBzZWxlY3RSZXF1aXJlZFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdF07XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2xpZmUnKTpcclxuXHRcdFx0XHRlbnRyaWVzID0gW1xyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRsYWJlbDogJ+uztO2XmOuCmOydtCcsXHJcblx0XHRcdFx0XHRcdHR5cGU6ICdudW1GaXhlZCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnYWdlJyxcclxuXHRcdFx0XHRcdFx0Zml4ZWRBczogJzQwJ1xyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfshLHrs4QnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnc2VsZWN0JyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdzZXgnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMTIzNCksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRdO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlKCdhbm51aXR5Jyk6XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfrs7Ttl5jrgpjsnbQnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnc2VsZWN0JyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdhZ2UnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMzMwMiksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn7ISx67OEJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnc2V4JyxcclxuXHRcdFx0XHRcdFx0b3B0aW9uczogdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIDEyMzQpLFxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm46IHNlbGVjdFJlcXVpcmVkXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSgnY2FyJyk6XHJcblx0XHRcdFx0bGV0IGFnZUNvbnRyYWN0T3B0cyA9IHtcclxuXHRcdFx0XHRcdGxhYmVsOiAn7Jew66C57Yq57JW9JyxcclxuXHRcdFx0XHRcdHR5cGU6ICdzZWxlY3QnLFxyXG5cdFx0XHRcdFx0aWQ6ICdhZ2VfY29udHJhY3QnLFxyXG5cdFx0XHRcdFx0b3B0aW9uczogW10sXHJcblx0XHRcdFx0XHR2YWxpZGF0aW9uRm46IHNlbGVjdFJlcXVpcmVkXHJcblx0XHRcdFx0fTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRpZihzZWFyY2hRdWVyeS5hZ2UpIHtcclxuXHRcdFx0XHRcdGxldCBhZ2VTZWxlY3RlZCA9IG9wdENvZGVNYXBbMzQwNl0uZmluZCgoZSkgPT4gKGUuY2QgPT0gc2VhcmNoUXVlcnkuYWdlKSk7XHJcblx0XHRcdFx0XHQvLyBjYXV0aW9uOiAgdXNpbmcgJz09JyBzaW5jZSBjb252ZXJ0aW5nIGludG8gc3RyaW5nXHJcblxyXG5cdFx0XHRcdFx0aWYoYWdlU2VsZWN0ZWQgJiYgYWdlU2VsZWN0ZWQucmVmMSkge1xyXG5cdFx0XHRcdFx0XHRsZXQgb3B0cyA9IFtdO1xyXG5cdFx0XHRcdFx0XHRsZXQgcmF3T3B0cyA9IEpTT04ucGFyc2UoYHsgJHthZ2VTZWxlY3RlZC5yZWYxfSB9YCk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRmb3IgKGxldCBjZCBpbiByYXdPcHRzKSB7XHJcblx0XHRcdFx0XHRcdFx0b3B0cy5wdXNoKHtcclxuXHRcdFx0XHRcdFx0XHRcdGxhYmVsOiByYXdPcHRzW2NkXSxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBjZFxyXG5cdFx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdGFnZUNvbnRyYWN0T3B0cy5vcHRpb25zID0gb3B0cztcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfrs7Ttl5jrgpjsnbQnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnc2VsZWN0JyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdhZ2UnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMzQwNiksXHJcblx0XHRcdFx0XHRcdGRlcGVuZGVuY2llczogWydhZ2VfY29udHJhY3QnXSxcclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuOiBzZWxlY3RSZXF1aXJlZFxyXG5cdFx0XHRcdFx0fSxcclxuXHJcblx0XHRcdFx0XHRhZ2VDb250cmFjdE9wdHMsXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfshLHrs4QnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnc2VsZWN0JyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdzZXgnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMTIzNCksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn7Jq07KCE7J6Q7ZWc7KCVJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnZHJpdmVyJyxcclxuXHRcdFx0XHRcdFx0b3B0aW9uczogdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIDM0MDIpLFxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm46IHNlbGVjdFJlcXVpcmVkXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn7KCE64u067O06rCA7J6FJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnZGFtYm8nLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMzQwMyksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn7ZWg7J247ZWg7Kad65Ox6riJJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnZGNfZ3JhZGUnLFxyXG5cdFx0XHRcdFx0XHRjbGFzc05hbWU6ICdzZWxlY3QtZHJvcGRvd24nLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMzQwNCksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn7LCo7KKFJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAnY2FyX3R5cGUnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMzQwNSksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XTtcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0ZGVmYXVsdDpcclxuXHRcdFx0XHRlbnRyaWVzID0gW107XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIGVudHJpZXM7XHJcblx0fSxcclxuXHJcblx0X2dldENtcHJDYXJkQ29udGVudHMoc3VibWVudSwgY29tcGFyaXNvbkdyb3VwKSB7XHJcblx0XHRsZXQgY21wckNhcmRDb250ZW50cyA9IFtdO1xyXG5cclxuXHRcdHN3aXRjaChzdWJtZW51LnRvTG93ZXJDYXNlKCkpIHtcclxuXHRcdFx0Y2FzZSgnc2hpbHNvbicpOlxyXG5cdFx0XHRcdGNtcHJDYXJkQ29udGVudHMgPSBjb21wYXJpc29uR3JvdXAubWFwKChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0XHRcdGxldCB7XHJcblx0XHRcdFx0XHRcdHByb2RfbmFtZSxcclxuXHRcdFx0XHRcdFx0Y29tcGFueV9ubSxcclxuXHRcdFx0XHRcdFx0YW1vdW50LFxyXG5cdFx0XHRcdFx0XHRmaWx0ZXJfbmFtZSxcclxuXHRcdFx0XHRcdFx0ZGFtYm9fc2hpbHNvbl9uYW1lXHJcblx0XHRcdFx0XHR9ID0gZWxlbTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0cmV0dXJuXHR7XHJcblx0XHRcdFx0XHRcdHRpdGxlOiBwcm9kX25hbWUsXHJcblx0XHRcdFx0XHRcdGluZm86IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn67O07ZeY7IKs66qFJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBlbGVtLmNvbXBhbnlfbm0sXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiAzLFxyXG5cdFx0XHRcdFx0XHRcdFx0aXNQcm92aWRlcjogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+uztO2XmOujjCcsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogbnVtYmVyV2l0aENvbW1hcyhlbGVtLmFtb3VudCksXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiAzLFxyXG5cdFx0XHRcdFx0XHRcdFx0cHJlZml4OiAn7JuUJyxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICfsm5AnLFxyXG5cdFx0XHRcdFx0XHRcdFx0aXNMZWFkaW5nSW5mbzogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+qwgOyeheycoO2YlScsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogZWxlbS5maWx0ZXJfbmFtZSxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDNcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfshKDtg53ri7Trs7QnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IGVsZW0uZGFtYm9fc2hpbHNvbl9uYW1lLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogM1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlKCdsaWZlJyk6XHJcblx0XHRcdFx0Y21wckNhcmRDb250ZW50cyA9IGNvbXBhcmlzb25Hcm91cC5tYXAoKGVsZW0sIGlkeCkgPT4ge1xyXG5cdFx0XHRcdFx0bGV0IHtcclxuXHRcdFx0XHRcdFx0cHJvZF9uYW1lLFxyXG5cdFx0XHRcdFx0XHRjb21wYW55X25tLFxyXG5cdFx0XHRcdFx0XHRhbW91bnQsXHJcblx0XHRcdFx0XHRcdGZpbHRlcl9uYW1lLFxyXG5cdFx0XHRcdFx0XHRwcmljZV9pbmRleCxcclxuXHRcdFx0XHRcdFx0cHVibGljX3JhdGUsXHJcblx0XHRcdFx0XHRcdGxvd2VzdF9yYXRlXHJcblx0XHRcdFx0XHR9ID0gZWxlbTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0cmV0dXJuXHR7XHJcblx0XHRcdFx0XHRcdHRpdGxlOiBwcm9kX25hbWUsXHJcblx0XHRcdFx0XHRcdGluZm86IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn67O07ZeY7IKs66qFJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBjb21wYW55X25tLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogMyxcclxuXHRcdFx0XHRcdFx0XHRcdGlzUHJvdmlkZXI6IHRydWVcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfrs7Ttl5jqsIDqsqnsp4DsiJgnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IHByaWNlX2luZGV4LFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogMyxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICclJyxcclxuXHRcdFx0XHRcdFx0XHRcdGlzTGVhZGluZ0luZm86IHRydWVcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfqs7Xsi5zsnbTsnKgnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IHB1YmxpY19yYXRlLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogMyxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICclJ1xyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+y1nOyggOuztOymneydtOycqCcsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogbG93ZXN0X3JhdGUsXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiAzLFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJyUnXHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRdXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2FubnVpdHknKTpcclxuXHRcdFx0XHRjbXByQ2FyZENvbnRlbnRzID0gY29tcGFyaXNvbkdyb3VwLm1hcCgoZWxlbSwgaWR4KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQge1xyXG5cdFx0XHRcdFx0XHRwcm9kX25hbWUsXHJcblx0XHRcdFx0XHRcdGNvbXBhbnlfbm0sXHJcblx0XHRcdFx0XHRcdGFtb3VudCxcclxuXHRcdFx0XHRcdFx0Z3VidW4sXHJcblx0XHRcdFx0XHRcdHNhdmVkX3JhdGVfMTAsXHJcblx0XHRcdFx0XHRcdHB1YmxpY19yYXRlLFxyXG5cdFx0XHRcdFx0XHRsb3dlc3RfcmF0ZVxyXG5cdFx0XHRcdFx0fSA9IGVsZW07XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHJldHVyblx0e1xyXG5cdFx0XHRcdFx0XHR0aXRsZTogcHJvZF9uYW1lLFxyXG5cdFx0XHRcdFx0XHRpbmZvOiBbXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+uztO2XmOyCrOuqhScsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogY29tcGFueV9ubSxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDIsXHJcblx0XHRcdFx0XHRcdFx0XHRpc1Byb3ZpZGVyOiB0cnVlXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAnMTDrhYQg7KCB66a966WgJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBzYXZlZF9yYXRlXzEwLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogMixcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICclJyxcclxuXHRcdFx0XHRcdFx0XHRcdGlzTGVhZGluZ0luZm86IHRydWVcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfqs7Xsi5zsnbTsnKgnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IHB1YmxpY19yYXRlLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogMixcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICclJ1xyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+y1nOyggOuztOymneydtOycqCcsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogbG93ZXN0X3JhdGUsXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiA0LFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJycsXHJcblx0XHRcdFx0XHRcdFx0XHRzdHlsZTogeyB3aWR0aDogJzQwJScsIGJvcmRlclJpZ2h0OiAnbm9uZScsIGJvcmRlckxlZnQ6ICcxcHggc29saWQgIzc3YmRmMSd9XHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRdXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2NhcicpOlxyXG5cdFx0XHRcdGNtcHJDYXJkQ29udGVudHMgPSBjb21wYXJpc29uR3JvdXAubWFwKChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0XHRcdGxldCB7XHJcblx0XHRcdFx0XHRcdHByb2RfbmFtZSxcclxuXHRcdFx0XHRcdFx0Y29tcGFueV9ubSxcclxuXHRcdFx0XHRcdFx0YW1vdW50XHJcblx0XHRcdFx0XHR9ID0gZWxlbTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0cmV0dXJuXHR7XHJcblx0XHRcdFx0XHRcdHRpdGxlOiBwcm9kX25hbWUsXHJcblx0XHRcdFx0XHRcdGluZm86IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn67O07ZeY7IKs66qFJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBjb21wYW55X25tLFxyXG5cdFx0XHRcdFx0XHRcdFx0Y29sU3BhbjogNixcclxuXHRcdFx0XHRcdFx0XHRcdGlzUHJvdmlkZXI6IHRydWVcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfrs7Ttl5jro4wnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IG51bWJlcldpdGhDb21tYXMoYW1vdW50KSxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDYsXHJcblx0XHRcdFx0XHRcdFx0XHRwcmVmaXg6ICfsl7AnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJ+ybkCcsXHJcblx0XHRcdFx0XHRcdFx0XHRpc0xlYWRpbmdJbmZvOiB0cnVlXHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRdXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHJcblx0XHR9XHJcblxyXG5cclxuXHRcdHJldHVybiBjbXByQ2FyZENvbnRlbnRzO1xyXG5cdH1cclxuXHRcclxuICAgIFxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gSW5zdXJlSGVscGVyU2VjdGlvbjtcclxuIl19
