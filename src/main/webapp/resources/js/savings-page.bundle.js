require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({248:[function(require,module,exports){
'use strict';

(function () {

	var React = require('react'),
	    ReactDOM = require('react-dom'),
	    QueryStore = require('../common/stores/QueryStore'),
	    ProductStore = require('../common/stores/ProductStore'),
	    ComparisonStore = require('../common/stores/ComparisonStore'),
	    CommonActions = require('../common/actions/CommonActions'),
	    getSearchCallback = require('../common/utils/getSearchCallback'),
	    SavingsHelperSection = require('./components/SavingsHelperSection.jsx'),
	    SavingsResultSection = require('./components/SavingsResultSection.jsx'),
	    SavingsProductAdder = require('./components/SavingsProductAdder.jsx');

	var mainmenu = undefined,
	    submenu = undefined,
	    isLoggedIn = undefined,
	    searchQuery = undefined;

	if (window.App) {
		var _window$App$query = window.App.query;
		mainmenu = _window$App$query.mainmenu;
		submenu = _window$App$query.submenu;
		searchQuery = _window$App$query.searchQuery;

		isLoggedIn = window.App.comparisons.isLoggedIn;

		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
	}

	//    CommonActions.fetchOptions([
	//		'A0001013', 'A0001023', 'A0001043'
	//	]);

	if (isLoggedIn) {
		CommonActions.fetchMyProducts(mainmenu, submenu);
	}

	var productAdder = React.createElement(SavingsProductAdder, null);

	var searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away

	ReactDOM.render(React.createElement(SavingsHelperSection, {
		adderModal: productAdder }), document.getElementById('helper'), searchCallback);

	ReactDOM.render(React.createElement(SavingsResultSection, null), document.getElementById('results'));
})();

},{"../common/actions/CommonActions":186,"../common/stores/ComparisonStore":208,"../common/stores/ProductStore":209,"../common/stores/QueryStore":210,"../common/utils/getSearchCallback":216,"./components/SavingsHelperSection.jsx":245,"./components/SavingsProductAdder.jsx":246,"./components/SavingsResultSection.jsx":247,"react":177,"react-dom":20}],247:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    ProductCard = require('../../common/components/ProductCard.jsx'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas;

var ListOpts = function ListOpts(label, value) {
	_classCallCheck(this, ListOpts);

	this.label = label;
	this.dataAttrs = { value: value };
};

function getSavingsFilterList() {
	return [new ListOpts('전체', '99999999'), new ListOpts('1금융권', 'FIRST'), new ListOpts('저축은행', 'SECOND'), new ListOpts('기타', 'ELSE')];
}

var SavingsResultSection = React.createClass({
	displayName: 'SavingsResultSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin'), require('../../common/utils/resultSectionMixin.jsx')],

	propTypes: {},

	getDefaultProps: function getDefaultProps() {
		return {};
	},

	getInitialState: function getInitialState() {
		return {};
	},

	componentDidMount: function componentDidMount() {},


	componentWillUnmount: function componentWillUnmount() {},

	_getOptions: function _getOptions(submenu, optCodeMap) {
		var _this = this;

		//  returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element

		var _state = this.state;
		var resultState = _state.resultState;
		var comparisonState = _state.comparisonState;
		var isCmprTabActive = comparisonState.isCmprTabActive;
		var isLoggedIn = comparisonState.isLoggedIn;
		var comparisonGroup = comparisonState.comparisonGroup;
		var currentCmpr = comparisonState.currentCmpr;

		var showingCmpr = isLoggedIn && isCmprTabActive && comparisonGroup.length > 0;
		var selectedCmpr = comparisonGroup[currentCmpr];

		var filterOptions = undefined,
		    sortOptions = undefined,
		    tooltipElem = undefined,
		    resultsElem = undefined,
		    helperElem = undefined;
		var comparisonContents = undefined;

		switch (submenu.toLowerCase()) {
			// 예금
			case 'deposit':
			// 적금
			case 'saving':
			// MMDA
			case 'mmda':
			// 주택청약
			case 'house':

				filterOptions = getSavingsFilterList();
				filterOptions = this._addNumResults(filterOptions);

				sortOptions = [new ListOpts('금리순', '1')]; // TODO: modify this to real code

				tooltipElem = undefined;

				resultsElem = resultState.productData.map(function (elem, idx) {
					var AC_CODE = //기간

					// isNew?
					elem.AC_CODE;
					var // 상품코드
					DS_FN_CODE_NM = elem.DS_FN_CODE_NM;
					var // 카테고리 (1금융권, 2금융권, ...),
					BANK_IMG = elem.BANK_IMG;
					var DS_PRE_TAX = elem.DS_PRE_TAX;
					var // 이율
					DS_NM = elem.DS_NM;
					var // 상품명
					DS_SUMMARY1 = elem.DS_SUMMARY1;
					var //상품설명 1
					DS_SUMMARY2 = elem.DS_SUMMARY2;
					var //상품설명 2
					totalAmount = elem.totalAmount;
					var // 만기수령액
					afterTax = elem.afterTax;
					var // 세후이자
					DS_INTERNET_YN = elem.DS_INTERNET_YN;
					var // 인터넷가입 여부
					DS_PHONE_YN = elem.DS_PHONE_YN;
					var // 스마트폰가입 여부
					DS_PROTECT_YN = elem.DS_PROTECT_YN;
					var // 예금자보호 여부
					DS_TAXFREE_YN = elem.DS_TAXFREE_YN;
					var // 비과세 여부
					interest_yn = elem.interest_yn;
					var //관심상품 여부

					DS_PERIOD = elem.DS_PERIOD;
					var detail_url = elem.detail_url;


					var addingInfo = undefined;
					var submenuCode = undefined;

					switch (submenu.toLowerCase()) {
						case 'deposit':
							submenuCode = 1100;
							addingInfo = '(' + DS_PERIOD + ')';
							break;
						case 'saving':
							submenuCode = 1200;
							addingInfo = '(' + DS_PERIOD + ')';
							break;
						case 'mmda':
							submenuCode = 1300;
							addingInfo = '';
							break;
						case 'house':
							submenuCode = 1400;
							addingInfo = '(' + DS_PERIOD + ')';
							break;
						default:
					}

					var mainAttrSet = {
						'만기수령액': numberWithCommas(totalAmount) + ' 원',
						'세후이자': numberWithCommas(afterTax) + ' 원'
					};

					var features = [];
					if (DS_INTERNET_YN === 'Y') {
						features.push('<i class="banking-icon internet"></i><span>인터넷가입</span>');
					}
					if (DS_PHONE_YN === 'Y') {
						features.push('<i class="banking-icon smartphone"></i><span>스마트폰 가입</span>');
					}
					if (DS_PROTECT_YN === 'Y') {
						features.push('<i class="banking-icon save"></i><span>예금자 보호</span>');
					}
					if (DS_TAXFREE_YN === 'Y') {
						features.push('<i class="banking-icon free"></i><span>비과세</span>');
					}

					if (showingCmpr) {
						comparisonContents = [{
							attr: '만기수령액',
							diff: totalAmount - selectedCmpr.totalamount,
							unit: '원'
						}, {
							attr: '세후이자',
							diff: afterTax - selectedCmpr.aftertax,
							unit: '원'
						}];
					}

					var isInterested = interest_yn === 'Y'; // 관심상품?

					return React.createElement(ProductCard, {
						key: 'savings' + AC_CODE,
						type: 'type1',
						category: DS_FN_CODE_NM,
						provider: BANK_IMG,
						leadingInfo: DS_PRE_TAX,
						prefix: '연',
						unit: '%',
						addingInfo: addingInfo,
						productTitle: DS_NM,
						features: features,
						description: DS_SUMMARY1 + ' ' + DS_SUMMARY2,
						mainAttrSet: mainAttrSet,
						targetModalSelector: '#product_view_popup',
						detailUrl: detail_url,
						comparisonContents: comparisonContents,
						isInterested: isInterested,
						index: idx,
						handleInterestClick: _this.handleInterestClick.bind(null, isInterested, submenuCode, AC_CODE, idx),
						choiceReverse: false
					});
				});

				break;

			default:
		}

		return { filterOptions: filterOptions, sortOptions: sortOptions, tooltipElem: tooltipElem, resultsElem: resultsElem, helperElem: helperElem };
	},
	getPreSearchView: function getPreSearchView(submenu) {
		var preSearchViewElem = undefined;

		switch (submenu.toLowerCase()) {
			case 'shilson':
				break;
			case 'car':
				break;
			default:

		}

		return preSearchViewElem;
	}
});

module.exports = SavingsResultSection;

},{"../../common/components/ProductCard.jsx":193,"../../common/utils/comparisonStoreMixin":214,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/resultSectionMixin.jsx":221,"../../common/utils/sendQueryMixin":222,"react":177}],246:[function(require,module,exports){
'use strict';

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    TextInput = require('../../common/components/TextInput.jsx'),
    Api = require('../../common/utils/api'),
    CommonActions = require('../../common/actions/CommonActions'),
    formValidators = require('../../common/utils/formValidators');

var selectRequired = formValidators.selectRequired;
var numberRequired = formValidators.numberRequired;


var submenu_codes = require('../../common/constants/submenu_codes');

var savingsMenuCodes = {
	deposit: 'A000101',
	saving: 'A000102',
	mmda: 'A000103',
	house: 'A000104'
};

function convertTermCode(termCd, submenu, optCodeMap) {
	var termGrpCode = undefined,
	    termProp = undefined,
	    termNum = undefined;

	if (submenu === 'mmda') {
		// 'mmda' does not have a term option
		termProp = 'DS_PRE_TAX';
		termNum = '0';
	} else {
		// else, find proper optCodeMap
		switch (submenu) {
			case 'deposit':
				termGrpCode = 'A0001013';
				break;
			case 'saving':
				termGrpCode = 'A0001023';
				break;
			case 'house':
				termGrpCode = 'A0001043';
				break;
		}

		if (optCodeMap && termGrpCode) {
			var opt = optCodeMap[termGrpCode].find(function (e) {
				return e.cd === termCd;
			});

			if (opt) {
				switch (opt.cdName.trim()) {
					case '3개월':
						termProp = 'DS_RATE3';
						termNum = '3';
						break;
					case '6개월':
						termProp = 'DS_RATE6';
						termNum = '6';
						break;
					case '12개월':
					case '1년':
						termProp = 'DS_RATE12';
						termNum = '12';
						break;
					case '24개월':
					case '2년':
					case '2년이상':
						termProp = 'DS_RATE24';
						termNum = '24';
						break;
					case '36개월':
					case '3년':
					case '3년이상':
						termProp = 'DS_RATE36';
						termNum = '36';
						break;
					default:
						throw new Error('cdName for the term code \'' + termCd + '\' is not one of followings: (3개월, 6개월, 12개월, 24개월, 36개월, 1년, 2년, 2년이상, 3년이상)');
				}
			} else {
				new Error('Cannot find \'' + termGrpCode + '\' property in optCodeMap');
			}
		} else {
			throw new Error('optCodeMap or proper grpCode for term is not provided');
		}
	}

	return { termProp: termProp, termNum: termNum };
}

var SelectOpts = function SelectOpts(label, value, data) {
	_classCallCheck(this, SelectOpts);

	this.label = label;
	this.value = value;
	this.data = data;
};

var SavingsProductAdder = React.createClass({
	displayName: 'SavingsProductAdder',

	propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
	},

	getDefaultProps: function getDefaultProps() {
		return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {}
		};
	},


	getInitialState: function getInitialState() {
		return {
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			companyOpts: [],
			prodNameOpts: [],

			prodNamePlaceholder: '기관명을 선택하세요'
		};
	},

	componentDidMount: function componentDidMount() {
		var category = savingsMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/saving/selectSavingBankList', { category: category }).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				var resultData = res.result.data;
				var options = resultData.map(function (e) {
					return new SelectOpts(e.cdName, e.cd);
				});

				this.setState({
					companyOpts: options
				});
			}
		}.bind(this));

		// clear when closed
		$('#product_add_popup').on('hidden.bs.modal', this.clearStats);
	},
	componentWillUnmount: function componentWillUnmount() {},
	render: function render() {
		var _state = this.state;
		var showValidation = _state.showValidation;
		var companyOpts = _state.companyOpts;
		var company_code = _state.company_code;
		var prodNameOpts = _state.prodNameOpts;
		var interest = _state.interest;
		var prod_code = _state.prod_code;
		var term = _state.term;
		var amount = _state.amount;
		var prodNamePlaceholder = _state.prodNamePlaceholder;
		var _props = this.props;
		var submenu = _props.submenu;
		var searchEntries = _props.searchEntries;
		var optCodeMap = _props.optCodeMap;


		var companyForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'기관명'
				),
				React.createElement(SelectDropdown, {
					placeholder: '선택',
					options: companyOpts,
					handleSelect: this.handleCompanySelect,
					selected: company_code,
					validationFn: showValidation && selectRequired
				})
			)
		);

		var productNameForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'상품명'
				),
				React.createElement(SelectDropdown, {
					placeholder: prodNamePlaceholder,
					options: prodNameOpts,
					handleSelect: this.handleProductNameSelect,
					selected: prod_code,
					validationFn: showValidation && selectRequired
				})
			)
		);

		var termEntry = searchEntries.find(function (e) {
			return e.id === 'periodCd';
		});
		var termForm = undefined;
		if (termEntry) {
			termForm = React.createElement(
				'div',
				{ className: 'row' },
				React.createElement(
					'div',
					{ className: 'col-xs-6' },
					React.createElement(
						'label',
						{ className: 'select-label' },
						'가입기간'
					),
					React.createElement(SelectDropdown, {
						placeholder: termEntry.placeholder || '선택',
						options: termEntry.options,
						handleSelect: this.handleTermSelect,
						className: termEntry.className || 'select-dropdown',
						selected: term,
						validationFn: showValidation && termEntry.validationFn
					})
				)
			);
		}

		var amountEntry = searchEntries.find(function (e) {
			return e.id === 'amount';
		});
		var amountForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					amountEntry.label
				),
				React.createElement(TextInput, {
					type: 'number-with-commas',
					placeholder: amountEntry.placeholder || ' ',
					inputClass: 'text-input-normal',
					handleChange: this.handleTextInputChange.bind(null, 'amount'),
					value: amount,
					unit: amountEntry.unit || '',
					validationFn: showValidation && amountEntry.validationFn
				})
			)
		);

		var interestForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'금리'
				),
				React.createElement(TextInput, {
					type: 'number',
					placeholder: ' ',
					inputClass: 'text-input-normal',
					handleChange: this.handleTextInputChange.bind(null, 'interest'),
					value: interest,
					unit: '%',
					validationFn: showValidation && numberRequired
				})
			)
		);

		return React.createElement(
			'div',
			{ className: 'modal fade', id: 'product_add_popup' },
			React.createElement(
				'div',
				{ className: 'modal-dialog' },
				React.createElement(
					'div',
					{ className: 'modal-content modal-small' },
					React.createElement('a', { className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' }),
					React.createElement(
						'div',
						{ className: 'modal-header' },
						React.createElement(
							'h4',
							{ className: 'modal-title', id: 'myModalLabel' },
							'가입상품 추가하기'
						)
					),
					React.createElement(
						'div',
						{ className: 'modal-body' },
						React.createElement(
							'div',
							{ className: 'product_add' },
							companyForm,
							productNameForm,
							termForm,
							amountForm,
							interestForm,
							React.createElement(
								'span',
								{ className: 'txt' },
								'(금리를 입력하지 않으실 경우, 등록일 현재 기준 금리로 적용됩니다.)'
							),
							React.createElement(
								'div',
								{ className: 'center_btn' },
								React.createElement(
									'a',
									{ className: 'btn btn-submit', role: 'button', onClick: this.submitProduct },
									'확인'
								)
							)
						)
					)
				)
			)
		);
	},
	handleCompanySelect: function handleCompanySelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var companyCode = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			company_code: companyCode,
			prod_code: '',
			prodNameOpts: [],
			interest: ''
		});

		this.fetchProductNames(companyCode);
	},
	fetchProductNames: function fetchProductNames(company_code) {
		var dsCode = savingsMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/saving/filterProductList', { dsBankingCode: company_code, dsCode: dsCode }).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				var resultData = res.result.data;

				if (resultData && resultData.length > 0) {
					var options = resultData.map(function (e) {
						return new SelectOpts(e.DS_NM, e.AC_CODE, e);
					});

					this.setState({
						prodNameOpts: options,
						prodNamePlaceholder: '선택'
					});
				} else {
					this.setState({
						prodNamePlaceholder: '해당 조건의 상품이 존재하지 않습니다'
					});
				}
			}
		}.bind(this));
	},
	handleProductNameSelect: function handleProductNameSelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var prodCd = $(target).closest('li').attr('data-val') || target.textContent;
		var term = this.state.term;


		this.setState({
			prod_code: prodCd,
			interest: ''
		});

		if (term || this.props.submenu.toLowerCase() === 'mmda') {
			this.setInterestRate(prodCd, term);
		}
	},
	handleTermSelect: function handleTermSelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var term = $(target).closest('li').attr('data-val') || target.textContent;
		var prod_code = this.state.prod_code;


		this.setState({
			term: term,
			interest: ''
		});

		if (prod_code) {
			this.setInterestRate(prod_code, term);
		}
	},
	handleTextInputChange: function handleTextInputChange(queryAttr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = target.value;

		this.setState(_defineProperty({}, queryAttr, val));
	},
	clearStats: function clearStats() {
		this.setState({
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			prodNameOpts: []
		});
	},
	checkValidity: function checkValidity() {
		var _state2 = this.state;
		var company_code = _state2.company_code;
		var prod_code = _state2.prod_code;
		var amount = _state2.amount;
		var term = _state2.term;
		var interest = _state2.interest;


		var allValid = true;

		allValid = allValid && selectRequired(company_code) === true;
		allValid = allValid && selectRequired(prod_code) === true;
		allValid = allValid && numberRequired(amount) === true;
		allValid = allValid && numberRequired(interest) === true;
		if (this.props.submenu.toLowerCase() !== 'mmda') {
			allValid = allValid && selectRequired(term) === true;
		}

		return allValid;
	},
	setInterestRate: function setInterestRate(prodCd, term) {
		var _convertTermCode = convertTermCode(term, this.props.submenu, this.props.optCodeMap);

		var termProp = _convertTermCode.termProp;


		var opt = this.state.prodNameOpts.find(function (e) {
			return e.value === prodCd;
		});
		var interest = opt.data[termProp] || opt.data['DS_PRE_TAX'];

		if (interest || interest === 0) {
			this.setState({
				interest: String(interest)
			});
		}
	},
	submitProduct: function submitProduct(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var _state3 = this.state;
		var company_code = _state3.company_code;
		var prod_code = _state3.prod_code;
		var amount = _state3.amount;
		var term = _state3.term;
		var interest = _state3.interest;
		var prodNameOpts = _state3.prodNameOpts;
		var _props2 = this.props;
		var submenu = _props2.submenu;
		var searchEntries = _props2.searchEntries;
		var optCodeMap = _props2.optCodeMap;

		var data = undefined;

		var allValid = this.checkValidity();

		if (allValid) {
			(function () {

				switch (submenu.toLowerCase()) {
					case 'deposit':
					case 'saving':
					case 'mmda':
					case 'house':
						data = {
							product_type: String(submenu_codes[submenu]),
							prod_code: String(prod_code),
							prod_name: String(prodNameOpts.find(function (elem) {
								return elem.value === prod_code;
							}).label),
							amount: String(amount),
							term_code: String(term),
							interest: String(interest),
							term: String(convertTermCode(term, submenu.toLowerCase(), optCodeMap).termNum)
						};
						break;
				}

				var $target = $(target);
				CommonActions.addMyProduct(data, function () {
					$target.closest('.modal').modal('hide');
				});
			})();
		} else {
			this.setState({
				showValidation: true
			});
		}
	}
});

module.exports = SavingsProductAdder;

},{"../../common/actions/CommonActions":186,"../../common/components/SelectDropdown.jsx":197,"../../common/components/TextInput.jsx":200,"../../common/constants/submenu_codes":204,"../../common/utils/api":211,"../../common/utils/formValidators":215,"react":177}],245:[function(require,module,exports){
'use strict';

var React = require('react'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas,
    formValidators = require('../../common/utils/formValidators');

var selectRequired = formValidators.selectRequired;
var numberRequired = formValidators.numberRequired;


var SavingsHelperSection = React.createClass({
	displayName: 'SavingsHelperSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin'), require('../../common/utils/helperSectionMixin.jsx')],

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {};
	},
	componentDidMount: function componentDidMount() {},
	componentWillUnmount: function componentWillUnmount() {},
	_getSearchEntries: function _getSearchEntries(queryState) {
		var submenu = queryState.submenu;
		var searchQuery = queryState.searchQuery;
		var optCodeMap = queryState.optCodeMap;

		var entries = undefined;

		switch (submenu.toLowerCase()) {
			case 'deposit':
				entries = [{
					label: '예치금액',
					type: 'number-with-commas',
					id: 'amount',
					unit: '만원',
					validationFn: numberRequired
				}, {
					label: '예치기간',
					type: 'select',
					id: 'periodCd',
					options: this.getSelectOptions(optCodeMap, 'A0001013'),
					validationFn: selectRequired
				}];
				break;
			case 'saving':
				entries = [{
					label: '월 저축금액',
					type: 'number-with-commas',
					id: 'amount',
					unit: '만원',
					validationFn: numberRequired
				}, {
					label: '가입기간',
					type: 'select',
					id: 'periodCd',
					options: this.getSelectOptions(optCodeMap, 'A0001023'),
					validationFn: selectRequired
				}];
				break;
			case 'mmda':
				entries = [{
					label: '예치금액',
					type: 'number-with-commas',
					id: 'amount',
					unit: '만원',
					validationFn: numberRequired
				}];
				break;
			case 'house':
				entries = [{
					label: '월 저축금액',
					type: 'number-with-commas',
					id: 'amount',
					unit: '만원',
					validationFn: numberRequired
				}, {
					label: '납입기간',
					type: 'select',
					id: 'periodCd',
					options: this.getSelectOptions(optCodeMap, 'A0001043'),
					validationFn: selectRequired
				}];
				break;
			default:
				entries = [];
		}

		return entries;
	},
	_getCmprCardContents: function _getCmprCardContents(submenu, comparisonGroup) {

		var cmprCardContents = [];

		switch (submenu.toLowerCase()) {
			case 'deposit':
			case 'saving':
			case 'mmda':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var ds_nm = elem.ds_nm;
					var //상품명
					ds_banking_nm = elem.ds_banking_nm;
					var // 은행명
					interest = elem.interest;
					var //금리
					totalamount = elem.totalamount;
					var //만기수령액
					aftertax = elem.aftertax;
					var //세후이자
					ds_period = elem.ds_period;


					var interestUnit = submenu.toLowerCase() !== 'mmda' ? '% (' + ds_period + ')' : '%';

					return {
						title: ds_nm,
						info: [{
							attr: '기관명',
							value: ds_banking_nm,
							colSpan: 3,
							isProvider: true
						}, {
							attr: '금리',
							value: interest,
							colSpan: 3,
							prefix: '연',
							unit: interestUnit,
							isLeadingInfo: true
						}, {
							attr: '만기수령액',
							value: numberWithCommas(totalamount),
							unit: '원',
							colSpan: 3
						}, {
							attr: '세후이자',
							value: numberWithCommas(aftertax),
							unit: '원',
							colSpan: 3
						}]
					};
				});
				break;
			case 'house':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var ds_nm = elem.ds_nm;
					var //상품명
					ds_banking_nm = elem.ds_banking_nm;
					var //은행명
					interest = elem.interest;
					var //금리
					amount = elem.amount;
					var //월납입액
					totalamount = elem.totalamount;
					var //만기수령액
					aftertax = elem.aftertax;
					var //세후이자
					ds_period = elem.ds_period;


					var interestUnit = submenu.toLowerCase() !== 'mmda' ? '% (' + ds_period + ')' : '%';

					return {
						title: ds_nm,
						info: [{
							attr: '은행명',
							value: ds_banking_nm,
							colSpan: 4,
							isProvider: true
						}, {
							attr: '금리',
							value: interest,
							colSpan: 4,
							prefix: '연',
							unit: interestUnit,
							isLeadingInfo: true
						}, {
							attr: '월 저축금액',
							value: numberWithCommas(amount),
							colSpan: 4,
							unit: '원'
						}]
					};
				});
				break;

		}

		return cmprCardContents;
	}
});

module.exports = SavingsHelperSection;

},{"../../common/utils/comparisonStoreMixin":214,"../../common/utils/formValidators":215,"../../common/utils/helperSectionMixin.jsx":217,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/sendQueryMixin":222,"react":177}]},{},[248])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvc2F2aW5ncy1wYWdlL3NhdmluZ3MtcGFnZS5qc3giLCJzcmMvc2F2aW5ncy1wYWdlL2NvbXBvbmVudHMvU2F2aW5nc1Jlc3VsdFNlY3Rpb24uanN4Iiwic3JjL3NhdmluZ3MtcGFnZS9jb21wb25lbnRzL1NhdmluZ3NQcm9kdWN0QWRkZXIuanN4Iiwic3JjL3NhdmluZ3MtcGFnZS9jb21wb25lbnRzL1NhdmluZ3NIZWxwZXJTZWN0aW9uLmpzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsQ0FBQyxZQUFXOztBQUVYLEtBQUksUUFBWSxRQUFRLE9BQVIsQ0FBWjtLQUNILFdBQWMsUUFBUSxXQUFSLENBQWQ7S0FDQSxhQUFnQixRQUFRLDZCQUFSLENBQWhCO0tBQ0EsZUFBaUIsUUFBUSwrQkFBUixDQUFqQjtLQUNBLGtCQUFvQixRQUFRLGtDQUFSLENBQXBCO0tBQ0EsZ0JBQWtCLFFBQVEsaUNBQVIsQ0FBbEI7S0FDQSxvQkFBcUIsUUFBUSxtQ0FBUixDQUFyQjtLQUNBLHVCQUF1QixRQUFRLHVDQUFSLENBQXZCO0tBQ0EsdUJBQXVCLFFBQVEsdUNBQVIsQ0FBdkI7S0FDQSxzQkFBdUIsUUFBUSxzQ0FBUixDQUF2QixDQVhVOztBQWNYLEtBQUksb0JBQUo7S0FBYyxtQkFBZDtLQUF1QixzQkFBdkI7S0FBbUMsdUJBQW5DLENBZFc7O0FBZ0JSLEtBQUcsT0FBTyxHQUFQLEVBQVk7MEJBQ21CLE9BQU8sR0FBUCxDQUFXLEtBQVgsQ0FEbkI7QUFDZix3Q0FEZTtBQUNMLHNDQURLO0FBQ0ksOENBREo7O0FBRWpCLGVBQWEsT0FBTyxHQUFQLENBQVcsV0FBWCxDQUF1QixVQUF2QixDQUZJOztBQUlqQixhQUFXLFNBQVgsQ0FBcUIsT0FBckIsRUFKaUI7QUFLakIsZUFBYSxTQUFiLENBQXVCLFNBQXZCLEVBTGlCO0FBTWpCLGtCQUFnQixTQUFoQixDQUEwQixhQUExQixFQU5pQjtFQUFmOzs7Ozs7QUFoQlEsS0ErQlIsVUFBSCxFQUFlO0FBQ2QsZ0JBQWMsZUFBZCxDQUE4QixRQUE5QixFQUF3QyxPQUF4QyxFQURjO0VBQWY7O0FBSUEsS0FBSSxlQUFlLG9CQUFDLG1CQUFELE9BQWYsQ0FuQ087O0FBcUNYLEtBQUksaUJBQWlCLGtCQUFrQixXQUFsQixDQUFqQjs7QUFyQ08sU0F3Q1IsQ0FBUyxNQUFULENBQ0Ysb0JBQUMsb0JBQUQ7QUFDQyxjQUFZLFlBQVosRUFERCxDQURFLEVBR0YsU0FBUyxjQUFULENBQXdCLFFBQXhCLENBSEUsRUFJRixjQUpFLEVBeENROztBQStDWCxVQUFTLE1BQVQsQ0FDQyxvQkFBQyxvQkFBRCxPQURELEVBRUMsU0FBUyxjQUFULENBQXdCLFNBQXhCLENBRkQsRUEvQ1c7Q0FBWCxDQUFEOzs7Ozs7O0FDQUEsSUFBSSxRQUFRLFFBQVEsT0FBUixDQUFSO0lBQ0gsY0FBZ0IsUUFBUSx5Q0FBUixDQUFoQjtJQUNBLG1CQUFtQixRQUFRLDBDQUFSLEVBQW9ELGdCQUFwRDs7SUFHZCxXQUNMLFNBREssUUFDTCxDQUFZLEtBQVosRUFBbUIsS0FBbkIsRUFBMEI7dUJBRHJCLFVBQ3FCOztBQUN6QixNQUFLLEtBQUwsR0FBYSxLQUFiLENBRHlCO0FBRXpCLE1BQUssU0FBTCxHQUFpQixFQUFFLE9BQU8sS0FBUCxFQUFuQixDQUZ5QjtDQUExQjs7QUFNRCxTQUFTLG9CQUFULEdBQWdDO0FBQy9CLFFBQU8sQ0FDTixJQUFJLFFBQUosQ0FBYSxJQUFiLEVBQW1CLFVBQW5CLENBRE0sRUFFTixJQUFJLFFBQUosQ0FBYSxNQUFiLEVBQXFCLE9BQXJCLENBRk0sRUFHTixJQUFJLFFBQUosQ0FBYSxNQUFiLEVBQXFCLFFBQXJCLENBSE0sRUFJTixJQUFJLFFBQUosQ0FBYSxJQUFiLEVBQW1CLE1BQW5CLENBSk0sQ0FBUCxDQUQrQjtDQUFoQzs7QUFVQSxJQUFJLHVCQUF1QixNQUFNLFdBQU4sQ0FBa0I7OztBQUM1QyxTQUFRLENBQ1AsUUFBUSxvQ0FBUixDQURPLEVBRVAsUUFBUSxzQ0FBUixDQUZPLEVBR1AsUUFBUSx5Q0FBUixDQUhPLEVBSVAsUUFBUSxtQ0FBUixDQUpPLEVBS1AsUUFBUSwyQ0FBUixDQUxPLENBQVI7O0FBUUcsWUFBVyxFQUFYOztBQUlBLGtCQUFpQiwyQkFBVztBQUN4QixTQUFPLEVBQVAsQ0FEd0I7RUFBWDs7QUFNakIsa0JBQWlCLDJCQUFXO0FBQ3hCLFNBQU8sRUFBUCxDQUR3QjtFQUFYOztBQU1wQixpREFBb0IsRUF6QndCOzs7QUE2QnpDLHVCQUFzQixnQ0FBVyxFQUFYOztBQUl6QixtQ0FBWSxTQUFTLFlBQVk7Ozs7Ozs7OztlQU9LLEtBQUssS0FBTCxDQVBMO01BTzNCLGlDQVAyQjtNQU9kLHlDQVBjO01BUzFCLGtCQUE4RCxnQkFBOUQsZ0JBVDBCO01BU1QsYUFBNkMsZ0JBQTdDLFdBVFM7TUFTRyxrQkFBaUMsZ0JBQWpDLGdCQVRIO01BU29CLGNBQWdCLGdCQUFoQixZQVRwQjs7QUFVaEMsTUFBSSxjQUFlLGNBQWMsZUFBZCxJQUFpQyxnQkFBZ0IsTUFBaEIsR0FBeUIsQ0FBekIsQ0FWcEI7QUFXaEMsTUFBSSxlQUFlLGdCQUFnQixXQUFoQixDQUFmLENBWDRCOztBQWFoQyxNQUFJLHlCQUFKO01BQW1CLHVCQUFuQjtNQUFnQyx1QkFBaEM7TUFBNkMsdUJBQTdDO01BQTBELHNCQUExRCxDQWJnQztBQWNoQyxNQUFJLHFCQUFxQixTQUFyQixDQWQ0Qjs7QUFnQmhDLFVBQU8sUUFBUSxXQUFSLEVBQVA7O0FBRUMsUUFBSyxTQUFMOztBQUZELFFBSU0sUUFBTDs7QUFKRCxRQU1NLE1BQUw7O0FBTkQsUUFRTSxPQUFMOztBQUVDLG9CQUFnQixzQkFBaEIsQ0FGRDtBQUdDLG9CQUFnQixLQUFLLGNBQUwsQ0FBb0IsYUFBcEIsQ0FBaEIsQ0FIRDs7QUFLQyxrQkFBYyxDQUNiLElBQUksUUFBSixDQUFhLEtBQWIsRUFBb0IsR0FBcEIsQ0FEYSxDQUFkOztBQUxELGVBU0MsR0FBYyxTQUFkLENBVEQ7O0FBV0Msa0JBQWMsWUFBWSxXQUFaLENBQXdCLEdBQXhCLENBQTZCLFVBQUMsSUFBRCxFQUFPLEdBQVAsRUFBZTtTQUV4RDs7O0FBa0JHLFVBbEJILFFBRndEOztBQUd4RCxxQkFpQkcsS0FqQkgsY0FId0Q7O0FBSXhELGdCQWdCRyxLQWhCSCxTQUp3RDtTQUt4RCxhQWVHLEtBZkgsV0FMd0Q7O0FBTXhELGFBY0csS0FkSCxNQU53RDs7QUFPeEQsbUJBYUcsS0FiSCxZQVB3RDs7QUFReEQsbUJBWUcsS0FaSCxZQVJ3RDs7QUFTeEQsbUJBV0csS0FYSCxZQVR3RDs7QUFVeEQsZ0JBVUcsS0FWSCxTQVZ3RDs7QUFXeEQsc0JBU0csS0FUSCxlQVh3RDs7QUFZeEQsbUJBUUcsS0FSSCxZQVp3RDs7QUFheEQscUJBT0csS0FQSCxjQWJ3RDs7QUFjeEQscUJBTUcsS0FOSCxjQWR3RDs7QUFleEQsbUJBS0csS0FMSCxZQWZ3RDs7O0FBaUJ4RCxpQkFHRyxLQUhILFVBakJ3RDtTQWtCeEQsYUFFRyxLQUZILFdBbEJ3RDs7O0FBc0J6RCxTQUFJLHNCQUFKLENBdEJ5RDtBQXVCekQsU0FBSSx1QkFBSixDQXZCeUQ7O0FBMEJ6RCxhQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsV0FBSyxTQUFMO0FBQ0MscUJBQWMsSUFBZCxDQUREO0FBRUMsMEJBQWlCLGVBQWpCLENBRkQ7QUFHQyxhQUhEO0FBREQsV0FLTSxRQUFMO0FBQ0MscUJBQWMsSUFBZCxDQUREO0FBRUMsMEJBQWlCLGVBQWpCLENBRkQ7QUFHQyxhQUhEO0FBTEQsV0FTTSxNQUFMO0FBQ0MscUJBQWMsSUFBZCxDQUREO0FBRUMsb0JBQWEsRUFBYixDQUZEO0FBR0MsYUFIRDtBQVRELFdBYU0sT0FBTDtBQUNDLHFCQUFjLElBQWQsQ0FERDtBQUVDLDBCQUFpQixlQUFqQixDQUZEO0FBR0MsYUFIRDtBQWJEO01BMUJ5RDs7QUErQ3pELFNBQUksY0FBYztBQUNqQixlQUFZLGlCQUFpQixXQUFqQixRQUFaO0FBQ0EsY0FBVyxpQkFBaUIsUUFBakIsUUFBWDtNQUZHLENBL0NxRDs7QUFvRHpELFNBQUksV0FBVyxFQUFYLENBcERxRDtBQXFEekQsU0FBRyxtQkFBbUIsR0FBbkIsRUFBd0I7QUFDMUIsZUFBUyxJQUFULENBQWMseURBQWQsRUFEMEI7TUFBM0I7QUFHQSxTQUFHLGdCQUFnQixHQUFoQixFQUFxQjtBQUN2QixlQUFTLElBQVQsQ0FBYyw2REFBZCxFQUR1QjtNQUF4QjtBQUdBLFNBQUcsa0JBQWtCLEdBQWxCLEVBQXVCO0FBQ3pCLGVBQVMsSUFBVCxDQUFjLHNEQUFkLEVBRHlCO01BQTFCO0FBR0EsU0FBRyxrQkFBa0IsR0FBbEIsRUFBdUI7QUFDekIsZUFBUyxJQUFULENBQWMsbURBQWQsRUFEeUI7TUFBMUI7O0FBS0EsU0FBRyxXQUFILEVBQWdCO0FBQ2YsMkJBQXFCLENBQ3BCO0FBQ0MsYUFBTSxPQUFOO0FBQ0EsYUFBTSxjQUFjLGFBQWEsV0FBYjtBQUNwQixhQUFNLEdBQU47T0FKbUIsRUFNcEI7QUFDQyxhQUFNLE1BQU47QUFDQSxhQUFNLFdBQVcsYUFBYSxRQUFiO0FBQ2pCLGFBQU0sR0FBTjtPQVRtQixDQUFyQixDQURlO01BQWhCOztBQWVBLFNBQUksZUFBZ0IsZ0JBQWdCLEdBQWhCOztBQWxGcUMsWUF1RnhELG9CQUFDLFdBQUQ7QUFDQyx1QkFBZSxPQUFmO0FBQ0EsWUFBSyxPQUFMO0FBQ0EsZ0JBQVUsYUFBVjtBQUNBLGdCQUFVLFFBQVY7QUFDQSxtQkFBYSxVQUFiO0FBQ0EsY0FBUSxHQUFSO0FBQ0EsWUFBSyxHQUFMO0FBQ0Esa0JBQVksVUFBWjtBQUNBLG9CQUFjLEtBQWQ7QUFDQSxnQkFBVSxRQUFWO0FBQ0EsbUJBQWEsY0FBYyxHQUFkLEdBQW9CLFdBQXBCO0FBQ2IsbUJBQWEsV0FBYjtBQUNBLDJCQUFvQixxQkFBcEI7QUFDQSxpQkFBVyxVQUFYO0FBQ0EsMEJBQW9CLGtCQUFwQjtBQUNBLG9CQUFjLFlBQWQ7QUFDQSxhQUFPLEdBQVA7QUFDQSwyQkFBcUIsTUFBSyxtQkFBTCxDQUF5QixJQUF6QixDQUE4QixJQUE5QixFQUFvQyxZQUFwQyxFQUFrRCxXQUFsRCxFQUErRCxPQUEvRCxFQUF3RSxHQUF4RSxDQUFyQjtBQUNBLHFCQUFlLEtBQWY7TUFuQkQsQ0FERCxDQXRGeUQ7S0FBZixDQUEzQyxDQVhEOztBQTBIQyxVQTFIRDs7QUFSRDtHQWhCZ0M7O0FBd0poQyxTQUFPLEVBQUMsNEJBQUQsRUFBZ0Isd0JBQWhCLEVBQTZCLHdCQUE3QixFQUEwQyx3QkFBMUMsRUFBdUQsc0JBQXZELEVBQVAsQ0F4SmdDO0VBakNXO0FBNEw1Qyw2Q0FBaUIsU0FBUztBQUN6QixNQUFJLDZCQUFKLENBRHlCOztBQUd6QixVQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsUUFBSyxTQUFMO0FBQ0MsVUFERDtBQURELFFBR00sS0FBTDtBQUNDLFVBREQ7QUFIRDs7R0FIeUI7O0FBWXpCLFNBQU8saUJBQVAsQ0FaeUI7RUE1TGtCO0NBQWxCLENBQXZCOztBQStNSixPQUFPLE9BQVAsR0FBaUIsb0JBQWpCOzs7Ozs7Ozs7QUNyT0EsSUFBSSxRQUFVLFFBQVEsT0FBUixDQUFWO0lBQ0gsaUJBQWlCLFFBQVEsNENBQVIsQ0FBakI7SUFDQSxZQUFhLFFBQVEsdUNBQVIsQ0FBYjtJQUNBLE1BQVMsUUFBUSx3QkFBUixDQUFUO0lBQ0EsZ0JBQWdCLFFBQVEsb0NBQVIsQ0FBaEI7SUFDQSxpQkFBa0IsUUFBUSxtQ0FBUixDQUFsQjs7SUFFSyxpQkFBbUMsZUFBbkM7SUFBZ0IsaUJBQW1CLGVBQW5COzs7QUFHdEIsSUFBTSxnQkFBZ0IsUUFBUSxzQ0FBUixDQUFoQjs7QUFFTixJQUFNLG1CQUFtQjtBQUN4QixVQUFTLFNBQVQ7QUFDQSxTQUFRLFNBQVI7QUFDQSxPQUFNLFNBQU47QUFDQSxRQUFPLFNBQVA7Q0FKSzs7QUFRTixTQUFTLGVBQVQsQ0FBeUIsTUFBekIsRUFBaUMsT0FBakMsRUFBMEMsVUFBMUMsRUFBc0Q7QUFDckQsS0FBSSx1QkFBSjtLQUFpQixvQkFBakI7S0FBMkIsbUJBQTNCLENBRHFEOztBQUlyRCxLQUFHLFlBQVksTUFBWixFQUFvQjs7QUFFdEIsYUFBVyxZQUFYLENBRnNCO0FBR3RCLFlBQVUsR0FBVixDQUhzQjtFQUF2QixNQUlPOztBQUVOLFVBQU8sT0FBUDtBQUNDLFFBQUssU0FBTDtBQUNDLGtCQUFjLFVBQWQsQ0FERDtBQUVDLFVBRkQ7QUFERCxRQUlNLFFBQUw7QUFDQyxrQkFBYyxVQUFkLENBREQ7QUFFQyxVQUZEO0FBSkQsUUFPTSxPQUFMO0FBQ0Msa0JBQWMsVUFBZCxDQUREO0FBRUMsVUFGRDtBQVBELEdBRk07O0FBY04sTUFBRyxjQUFjLFdBQWQsRUFBMkI7QUFDN0IsT0FBSSxNQUFNLFdBQVcsV0FBWCxFQUF3QixJQUF4QixDQUE2QixVQUFDLENBQUQ7V0FBTyxFQUFFLEVBQUYsS0FBUyxNQUFUO0lBQVAsQ0FBbkMsQ0FEeUI7O0FBRzdCLE9BQUcsR0FBSCxFQUFRO0FBQ1AsWUFBTyxJQUFJLE1BQUosQ0FBVyxJQUFYLEVBQVA7QUFDQyxVQUFLLEtBQUw7QUFDQyxpQkFBVyxVQUFYLENBREQ7QUFFQyxnQkFBVSxHQUFWLENBRkQ7QUFHQyxZQUhEO0FBREQsVUFLTSxLQUFMO0FBQ0MsaUJBQVcsVUFBWCxDQUREO0FBRUMsZ0JBQVUsR0FBVixDQUZEO0FBR0MsWUFIRDtBQUxELFVBU00sTUFBTCxDQVREO0FBVUMsVUFBSyxJQUFMO0FBQ0MsaUJBQVcsV0FBWCxDQUREO0FBRUMsZ0JBQVUsSUFBVixDQUZEO0FBR0MsWUFIRDtBQVZELFVBY00sTUFBTCxDQWREO0FBZUMsVUFBSyxJQUFMLENBZkQ7QUFnQkMsVUFBSyxNQUFMO0FBQ0MsaUJBQVcsV0FBWCxDQUREO0FBRUMsZ0JBQVUsSUFBVixDQUZEO0FBR0MsWUFIRDtBQWhCRCxVQW9CTSxNQUFMLENBcEJEO0FBcUJDLFVBQUssSUFBTCxDQXJCRDtBQXNCQyxVQUFLLE1BQUw7QUFDQyxpQkFBVyxXQUFYLENBREQ7QUFFQyxnQkFBVSxJQUFWLENBRkQ7QUFHQyxZQUhEO0FBdEJEO0FBMkJFLFlBQU0sSUFBSSxLQUFKLGlDQUF1Qyx3RkFBdkMsQ0FBTixDQUREO0FBMUJELEtBRE87SUFBUixNQThCTztBQUNOLFFBQUksS0FBSixvQkFBMEIseUNBQTFCLEVBRE07SUE5QlA7R0FIRCxNQXFDTztBQUNOLFNBQU0sSUFBSSxLQUFKLENBQVUsdURBQVYsQ0FBTixDQURNO0dBckNQO0VBbEJEOztBQTZEQSxRQUFPLEVBQUUsa0JBQUYsRUFBWSxnQkFBWixFQUFQLENBakVxRDtDQUF0RDs7SUFzRU0sYUFDTCxTQURLLFVBQ0wsQ0FBWSxLQUFaLEVBQW1CLEtBQW5CLEVBQTBCLElBQTFCLEVBQWdDO3VCQUQzQixZQUMyQjs7QUFDL0IsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUQrQjtBQUUvQixNQUFLLEtBQUwsR0FBYSxLQUFiLENBRitCO0FBRy9CLE1BQUssSUFBTCxHQUFZLElBQVosQ0FIK0I7Q0FBaEM7O0FBUUQsSUFBSSxzQkFBc0IsTUFBTSxXQUFOLENBQWtCOzs7QUFDeEMsWUFBVztBQUNiLFdBQVMsTUFBTSxTQUFOLENBQWdCLE1BQWhCO0FBQ1QsaUJBQWUsTUFBTSxTQUFOLENBQWdCLEtBQWhCO0FBQ2YsY0FBWSxNQUFNLFNBQU4sQ0FBZ0IsTUFBaEI7RUFIVjs7QUFNQSw2Q0FBa0I7QUFDZCxTQUFPO0FBQ1osWUFBUyxFQUFUO0FBQ0Esa0JBQWUsRUFBZjtBQUNBLGVBQVksRUFBWjtHQUhLLENBRGM7RUFQc0I7OztBQWUzQyxrQkFBaUIsMkJBQVc7QUFDckIsU0FBTztBQUNaLG1CQUFnQixLQUFoQjtBQUNBLGlCQUFjLEVBQWQ7QUFDQSxjQUFXLEVBQVg7QUFDQSxjQUFXLEVBQVg7QUFDQSxXQUFRLEVBQVI7QUFDQSxTQUFNLEVBQU47QUFDQSxhQUFVLEVBQVY7QUFDQSxnQkFBYSxFQUFiO0FBQ0EsaUJBQWMsRUFBZDs7QUFFQSx3QkFBcUIsWUFBckI7R0FYSyxDQURxQjtFQUFYOztBQWlCZCxpREFBb0I7QUFDdEIsTUFBSSxXQUFXLGlCQUFpQixLQUFLLEtBQUwsQ0FBVyxPQUFYLENBQW1CLFdBQW5CLEVBQWpCLENBQVgsQ0FEa0I7O0FBR3RCLE1BQUksSUFBSixDQUFTLHdDQUFULEVBQW1ELEVBQUMsa0JBQUQsRUFBbkQsRUFDRSxJQURGLENBQ08sVUFBVSxHQUFWLEVBQWU7QUFDcEIsT0FBRyxJQUFJLE1BQUosQ0FBVyxVQUFYLEtBQTBCLFNBQTFCLEVBQXFDO0FBQ3ZDLFFBQUksYUFBYSxJQUFJLE1BQUosQ0FBVyxJQUFYLENBRHNCO0FBRXZDLFFBQUksVUFBWSxXQUFXLEdBQVgsQ0FBZSxVQUFDLENBQUQ7WUFBTyxJQUFJLFVBQUosQ0FBZSxFQUFFLE1BQUYsRUFBVSxFQUFFLEVBQUY7S0FBaEMsQ0FBM0IsQ0FGbUM7O0FBSXZDLFNBQUssUUFBTCxDQUFjO0FBQ2Isa0JBQWEsT0FBYjtLQURELEVBSnVDO0lBQXhDO0dBREssQ0FTSixJQVRJLENBU0MsSUFURCxDQURQOzs7QUFIc0IsR0FpQnRCLENBQUUsb0JBQUYsRUFBd0IsRUFBeEIsQ0FBMkIsaUJBQTNCLEVBQThDLEtBQUssVUFBTCxDQUE5QyxDQWpCc0I7RUFoQ29CO0FBcUR4Qyx1REFBdUIsRUFyRGlCO0FBeUR4QywyQkFBUztlQVdQLEtBQUssS0FBTCxDQVhPO01BRVYsdUNBRlU7TUFHVixpQ0FIVTtNQUlWLG1DQUpVO01BS1YsbUNBTFU7TUFNViwyQkFOVTtNQU9WLDZCQVBVO01BUVYsbUJBUlU7TUFTVix1QkFUVTtNQVVWLGlEQVZVO2VBWWtDLEtBQUssS0FBTCxDQVpsQztNQVlMLHlCQVpLO01BWUkscUNBWko7TUFZbUIsK0JBWm5COzs7QUFjWCxNQUFJLGNBQ0g7O0tBQUssV0FBVSxLQUFWLEVBQUw7R0FDQzs7TUFBSyxXQUFVLFVBQVYsRUFBTDtJQUNDOztPQUFPLFdBQVUsY0FBVixFQUFQOztLQUREO0lBRUMsb0JBQUMsY0FBRDtBQUNDLGtCQUFZLElBQVo7QUFDQSxjQUFTLFdBQVQ7QUFDQSxtQkFBYyxLQUFLLG1CQUFMO0FBQ2QsZUFBVSxZQUFWO0FBQ0EsbUJBQWMsa0JBQWtCLGNBQWxCO0tBTGYsQ0FGRDtJQUREO0dBREcsQ0FkTzs7QUE2QlgsTUFBSSxrQkFDSDs7S0FBSyxXQUFVLEtBQVYsRUFBTDtHQUNDOztNQUFLLFdBQVUsVUFBVixFQUFMO0lBQ0M7O09BQU8sV0FBVSxjQUFWLEVBQVA7O0tBREQ7SUFFQyxvQkFBQyxjQUFEO0FBQ0Msa0JBQWEsbUJBQWI7QUFDQSxjQUFTLFlBQVQ7QUFDQSxtQkFBYyxLQUFLLHVCQUFMO0FBQ2QsZUFBVSxTQUFWO0FBQ0EsbUJBQWMsa0JBQWtCLGNBQWxCO0tBTGYsQ0FGRDtJQUREO0dBREcsQ0E3Qk87O0FBOENYLE1BQUksWUFBWSxjQUFjLElBQWQsQ0FBbUIsVUFBQyxDQUFEO1VBQVEsRUFBRSxFQUFGLEtBQVMsVUFBVDtHQUFSLENBQS9CLENBOUNPO0FBK0NYLE1BQUksV0FBVyxTQUFYLENBL0NPO0FBZ0RYLE1BQUcsU0FBSCxFQUFjO0FBQ2IsY0FDQzs7TUFBSyxXQUFVLEtBQVYsRUFBTDtJQUNDOztPQUFLLFdBQVUsVUFBVixFQUFMO0tBQ0M7O1FBQU8sV0FBVSxjQUFWLEVBQVA7O01BREQ7S0FFQyxvQkFBQyxjQUFEO0FBQ0MsbUJBQWEsVUFBVSxXQUFWLElBQXlCLElBQXpCO0FBQ2IsZUFBUyxVQUFVLE9BQVY7QUFDVCxvQkFBYyxLQUFLLGdCQUFMO0FBQ2QsaUJBQVcsVUFBVSxTQUFWLElBQXVCLGlCQUF2QjtBQUNYLGdCQUFVLElBQVY7QUFDQSxvQkFBYyxrQkFBa0IsVUFBVSxZQUFWO01BTmpDLENBRkQ7S0FERDtJQURELENBRGE7R0FBZDs7QUFrQkEsTUFBSSxjQUFjLGNBQWMsSUFBZCxDQUFtQixVQUFDLENBQUQ7VUFBUSxFQUFFLEVBQUYsS0FBUyxRQUFUO0dBQVIsQ0FBakMsQ0FsRU87QUFtRVgsTUFBSSxhQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDtLQUFpQyxZQUFZLEtBQVo7S0FEbEM7SUFFQyxvQkFBQyxTQUFEO0FBQ0MsV0FBSyxvQkFBTDtBQUNBLGtCQUFhLFlBQVksV0FBWixJQUEyQixHQUEzQjtBQUNiLGlCQUFXLG1CQUFYO0FBQ0EsbUJBQWMsS0FBSyxxQkFBTCxDQUEyQixJQUEzQixDQUFnQyxJQUFoQyxFQUFzQyxRQUF0QyxDQUFkO0FBQ0EsWUFBTyxNQUFQO0FBQ0EsV0FBTSxZQUFZLElBQVosSUFBb0IsRUFBcEI7QUFDTixtQkFBYyxrQkFBa0IsWUFBWSxZQUFaO0tBUGpDLENBRkQ7SUFERDtHQURHLENBbkVPOztBQXNGWCxNQUFJLGVBQ0g7O0tBQUssV0FBVSxLQUFWLEVBQUw7R0FDQzs7TUFBSyxXQUFVLFVBQVYsRUFBTDtJQUNDOztPQUFPLFdBQVUsY0FBVixFQUFQOztLQUREO0lBRUMsb0JBQUMsU0FBRDtBQUNDLFdBQUssUUFBTDtBQUNBLGtCQUFZLEdBQVo7QUFDQSxpQkFBVyxtQkFBWDtBQUNBLG1CQUFjLEtBQUsscUJBQUwsQ0FBMkIsSUFBM0IsQ0FBZ0MsSUFBaEMsRUFBc0MsVUFBdEMsQ0FBZDtBQUNBLFlBQU8sUUFBUDtBQUNBLFdBQUssR0FBTDtBQUNBLG1CQUFjLGtCQUFrQixjQUFsQjtLQVBmLENBRkQ7SUFERDtHQURHLENBdEZPOztBQXlHWCxTQUNDOztLQUFLLFdBQVUsWUFBVixFQUF1QixJQUFHLG1CQUFILEVBQTVCO0dBQ0M7O01BQUssV0FBVSxjQUFWLEVBQUw7SUFDQzs7T0FBSyxXQUFVLDJCQUFWLEVBQUw7S0FFQywyQkFBRyxXQUFVLE9BQVYsRUFBa0IsZ0JBQWEsT0FBYixFQUFxQixjQUFXLE9BQVgsRUFBMUMsQ0FGRDtLQUlDOztRQUFLLFdBQVUsY0FBVixFQUFMO01BQ0M7O1NBQUksV0FBVSxhQUFWLEVBQXdCLElBQUcsY0FBSCxFQUE1Qjs7T0FERDtNQUpEO0tBU0M7O1FBQUssV0FBVSxZQUFWLEVBQUw7TUFDQzs7U0FBSyxXQUFVLGFBQVYsRUFBTDtPQUVFLFdBRkY7T0FHRSxlQUhGO09BSUUsUUFKRjtPQUtFLFVBTEY7T0FNRSxZQU5GO09BUUM7O1VBQU0sV0FBVSxLQUFWLEVBQU47O1FBUkQ7T0FTQzs7VUFBSyxXQUFVLFlBQVYsRUFBTDtRQUNDOztXQUFHLFdBQVUsZ0JBQVYsRUFBMkIsTUFBSyxRQUFMLEVBQWMsU0FBUyxLQUFLLGFBQUwsRUFBckQ7O1NBREQ7UUFURDtPQUREO01BVEQ7S0FERDtJQUREO0dBREQsQ0F6R1c7RUF6RCtCO0FBc00zQyxtREFBb0IsS0FBSztBQUN4QixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURPO0FBRXhCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGSDs7QUFJeEIsTUFBSSxjQUFjLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBN0IsS0FBNEMsT0FBTyxXQUFQLENBSnRDOztBQU14QixPQUFLLFFBQUwsQ0FBYztBQUNiLGlCQUFjLFdBQWQ7QUFDQSxjQUFXLEVBQVg7QUFDQSxpQkFBYyxFQUFkO0FBQ0EsYUFBVSxFQUFWO0dBSkQsRUFOd0I7O0FBYXhCLE9BQUssaUJBQUwsQ0FBdUIsV0FBdkIsRUFid0I7RUF0TWtCO0FBc04zQywrQ0FBa0IsY0FBYztBQUMvQixNQUFJLFNBQVMsaUJBQWlCLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FBbUIsV0FBbkIsRUFBakIsQ0FBVCxDQUQyQjs7QUFHL0IsTUFBSSxJQUFKLENBQVMscUNBQVQsRUFBZ0QsRUFBRSxlQUFlLFlBQWYsRUFBNkIsY0FBL0IsRUFBaEQsRUFDRSxJQURGLENBQ08sVUFBVSxHQUFWLEVBQWU7QUFDcEIsT0FBRyxJQUFJLE1BQUosQ0FBVyxVQUFYLEtBQTBCLFNBQTFCLEVBQXFDO0FBQ3ZDLFFBQUksYUFBYSxJQUFJLE1BQUosQ0FBVyxJQUFYLENBRHNCOztBQUd2QyxRQUFHLGNBQWMsV0FBVyxNQUFYLEdBQW9CLENBQXBCLEVBQXVCO0FBQ3ZDLFNBQUksVUFBWSxXQUFXLEdBQVgsQ0FBZSxVQUFDLENBQUQ7YUFBTyxJQUFJLFVBQUosQ0FBZSxFQUFFLEtBQUYsRUFBUyxFQUFFLE9BQUYsRUFBVyxDQUFuQztNQUFQLENBQTNCLENBRG1DOztBQUd2QyxVQUFLLFFBQUwsQ0FBYztBQUNiLG9CQUFjLE9BQWQ7QUFDQSwyQkFBcUIsSUFBckI7TUFGRCxFQUh1QztLQUF4QyxNQU9PO0FBQ04sVUFBSyxRQUFMLENBQWM7QUFDYiwyQkFBcUIsc0JBQXJCO01BREQsRUFETTtLQVBQO0lBSEQ7R0FESyxDQWlCSixJQWpCSSxDQWlCQyxJQWpCRCxDQURQLEVBSCtCO0VBdE5XO0FBK08zQywyREFBd0IsS0FBSztBQUM1QixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURXO0FBRTVCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGQzs7QUFJNUIsTUFBSSxTQUFTLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBN0IsS0FBNEMsT0FBTyxXQUFQLENBSjdCO01BS3RCLE9BQVMsS0FBSyxLQUFMLENBQVQsS0FMc0I7OztBQU81QixPQUFLLFFBQUwsQ0FBYztBQUNiLGNBQVcsTUFBWDtBQUNBLGFBQVUsRUFBVjtHQUZELEVBUDRCOztBQVk1QixNQUFHLFFBQVEsS0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQixXQUFuQixPQUFxQyxNQUFyQyxFQUE2QztBQUN2RCxRQUFLLGVBQUwsQ0FBcUIsTUFBckIsRUFBNkIsSUFBN0IsRUFEdUQ7R0FBeEQ7RUEzUDBDO0FBaVEzQyw2Q0FBaUIsS0FBSztBQUNyQixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURJO0FBRXJCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGTjs7QUFJckIsTUFBSSxPQUFPLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBN0IsS0FBNEMsT0FBTyxXQUFQLENBSmxDO01BS2YsWUFBYyxLQUFLLEtBQUwsQ0FBZCxVQUxlOzs7QUFPckIsT0FBSyxRQUFMLENBQWM7QUFDYixTQUFNLElBQU47QUFDQSxhQUFVLEVBQVY7R0FGRCxFQVBxQjs7QUFZckIsTUFBRyxTQUFILEVBQWM7QUFDYixRQUFLLGVBQUwsQ0FBcUIsU0FBckIsRUFBZ0MsSUFBaEMsRUFEYTtHQUFkO0VBN1EwQztBQW1SM0MsdURBQXNCLFdBQVcsS0FBSztBQUNyQyxRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURvQjtBQUVyQyxNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRlU7O0FBSXJDLE1BQUksTUFBTSxPQUFPLEtBQVAsQ0FKMkI7O0FBTXJDLE9BQUssUUFBTCxxQkFDRSxXQUFZLElBRGQsRUFOcUM7RUFuUks7QUErUjNDLG1DQUFhO0FBQ1osT0FBSyxRQUFMLENBQWM7QUFDYixtQkFBZ0IsS0FBaEI7QUFDQSxpQkFBYyxFQUFkO0FBQ0EsY0FBVyxFQUFYO0FBQ0EsY0FBVyxFQUFYO0FBQ0EsV0FBUSxFQUFSO0FBQ0EsU0FBTSxFQUFOO0FBQ0EsYUFBVSxFQUFWO0FBQ0EsaUJBQWMsRUFBZDtHQVJELEVBRFk7RUEvUjhCO0FBNFMzQyx5Q0FBZ0I7Z0JBT1gsS0FBSyxLQUFMLENBUFc7TUFFZCxvQ0FGYztNQUdkLDhCQUhjO01BSWQsd0JBSmM7TUFLZCxvQkFMYztNQU1kLDRCQU5jOzs7QUFTZixNQUFJLFdBQVcsSUFBWCxDQVRXOztBQVdmLGFBQVcsWUFBYSxlQUFlLFlBQWYsTUFBaUMsSUFBakMsQ0FYVDtBQVlmLGFBQVcsWUFBYSxlQUFlLFNBQWYsTUFBOEIsSUFBOUIsQ0FaVDtBQWFmLGFBQVcsWUFBYSxlQUFlLE1BQWYsTUFBMkIsSUFBM0IsQ0FiVDtBQWNmLGFBQVcsWUFBYSxlQUFlLFFBQWYsTUFBNkIsSUFBN0IsQ0FkVDtBQWVmLE1BQUcsS0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQixXQUFuQixPQUFxQyxNQUFyQyxFQUE2QztBQUMvQyxjQUFXLFlBQWEsZUFBZSxJQUFmLE1BQXlCLElBQXpCLENBRHVCO0dBQWhEOztBQUlBLFNBQU8sUUFBUCxDQW5CZTtFQTVTMkI7QUFrVTNDLDJDQUFnQixRQUFRLE1BQU07eUJBQ1YsZ0JBQWdCLElBQWhCLEVBQXNCLEtBQUssS0FBTCxDQUFXLE9BQVgsRUFBb0IsS0FBSyxLQUFMLENBQVcsVUFBWCxFQURoQzs7TUFDdkIscUNBRHVCOzs7QUFHN0IsTUFBSSxNQUFNLEtBQUssS0FBTCxDQUFXLFlBQVgsQ0FBd0IsSUFBeEIsQ0FBNkIsVUFBQyxDQUFEO1VBQVEsRUFBRSxLQUFGLEtBQVksTUFBWjtHQUFSLENBQW5DLENBSHlCO0FBSTdCLE1BQUksV0FBWSxJQUFJLElBQUosQ0FBUyxRQUFULEtBQXNCLElBQUksSUFBSixDQUFTLFlBQVQsQ0FBdEIsQ0FKYTs7QUFNN0IsTUFBRyxZQUFZLGFBQWEsQ0FBYixFQUFnQjtBQUM5QixRQUFLLFFBQUwsQ0FBYztBQUNiLGNBQVUsT0FBTyxRQUFQLENBQVY7SUFERCxFQUQ4QjtHQUEvQjtFQXhVMEM7QUFnVjNDLHVDQUFjLEtBQUs7QUFDbEIsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FEQztBQUVsQixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRlQ7O2dCQVdkLEtBQUssS0FBTCxDQVhjO01BS2pCLG9DQUxpQjtNQU1qQiw4QkFOaUI7TUFPakIsd0JBUGlCO01BUWpCLG9CQVJpQjtNQVNqQiw0QkFUaUI7TUFVakIsb0NBVmlCO2dCQVkyQixLQUFLLEtBQUwsQ0FaM0I7TUFZWiwwQkFaWTtNQVlILHNDQVpHO01BWVksZ0NBWlo7O0FBYWxCLE1BQUksZ0JBQUosQ0Fia0I7O0FBZWxCLE1BQUksV0FBVyxLQUFLLGFBQUwsRUFBWCxDQWZjOztBQWlCbEIsTUFBRyxRQUFILEVBQWE7OztBQUVaLFlBQU8sUUFBUSxXQUFSLEVBQVA7QUFDQyxVQUFLLFNBQUwsQ0FERDtBQUVDLFVBQUssUUFBTCxDQUZEO0FBR0MsVUFBSyxNQUFMLENBSEQ7QUFJQyxVQUFLLE9BQUw7QUFDQyxhQUFPO0FBQ04scUJBQWMsT0FBTyxjQUFjLE9BQWQsQ0FBUCxDQUFkO0FBQ0Esa0JBQVcsT0FBTyxTQUFQLENBQVg7QUFDQSxrQkFBVyxPQUFPLGFBQWEsSUFBYixDQUFrQixVQUFDLElBQUQ7ZUFBVyxLQUFLLEtBQUwsS0FBZSxTQUFmO1FBQVgsQ0FBbEIsQ0FBd0QsS0FBeEQsQ0FBbEI7QUFDQSxlQUFRLE9BQU8sTUFBUCxDQUFSO0FBQ0Esa0JBQVcsT0FBTyxJQUFQLENBQVg7QUFDQSxpQkFBVSxPQUFPLFFBQVAsQ0FBVjtBQUNBLGFBQU0sT0FBTyxnQkFBZ0IsSUFBaEIsRUFBc0IsUUFBUSxXQUFSLEVBQXRCLEVBQTZDLFVBQTdDLEVBQXlELE9BQXpELENBQWI7T0FQRCxDQUREO0FBVUMsWUFWRDtBQUpEOztBQWlCQSxRQUFJLFVBQVUsRUFBRSxNQUFGLENBQVY7QUFDSixrQkFBYyxZQUFkLENBQTJCLElBQTNCLEVBQWlDLFlBQU07QUFDdEMsYUFBUSxPQUFSLENBQWdCLFFBQWhCLEVBQTBCLEtBQTFCLENBQWdDLE1BQWhDLEVBRHNDO0tBQU4sQ0FBakM7UUFwQlk7R0FBYixNQXdCTztBQUNOLFFBQUssUUFBTCxDQUFjO0FBQ2Isb0JBQWdCLElBQWhCO0lBREQsRUFETTtHQXhCUDtFQWpXMEM7Q0FBbEIsQ0FBdEI7O0FBbVlKLE9BQU8sT0FBUCxHQUFpQixtQkFBakI7Ozs7O0FDdGVBLElBQUksUUFBVyxRQUFRLE9BQVIsQ0FBWDtJQUNILG1CQUFtQixRQUFRLDBDQUFSLEVBQW9ELGdCQUFwRDtJQUNuQixpQkFBa0IsUUFBUSxtQ0FBUixDQUFsQjs7SUFFSyxpQkFBbUMsZUFBbkM7SUFBZ0IsaUJBQW1CLGVBQW5COzs7QUFHdEIsSUFBSSx1QkFBdUIsTUFBTSxXQUFOLENBQWtCOzs7QUFDNUMsU0FBUSxDQUNQLFFBQVEsb0NBQVIsQ0FETyxFQUVQLFFBQVEsc0NBQVIsQ0FGTyxFQUdQLFFBQVEseUNBQVIsQ0FITyxFQUlQLFFBQVEsbUNBQVIsQ0FKTyxFQUtQLFFBQVEsMkNBQVIsQ0FMTyxDQUFSOztBQVNHLDZDQUFrQjtBQUNkLFNBQU8sRUFBUCxDQURjO0VBVnVCO0FBZ0J6Qyw2Q0FBa0I7QUFDZCxTQUFPLEVBQVAsQ0FEYztFQWhCdUI7QUFzQnpDLGlEQUFvQixFQXRCcUI7QUEwQnpDLHVEQUF1QixFQTFCa0I7QUE4QjVDLCtDQUFrQixZQUFZO01BQ3ZCLFVBQXFDLFdBQXJDLFFBRHVCO01BQ2QsY0FBNEIsV0FBNUIsWUFEYztNQUNELGFBQWUsV0FBZixXQURDOztBQUU3QixNQUFJLG1CQUFKLENBRjZCOztBQUk3QixVQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsUUFBSyxTQUFMO0FBQ0MsY0FBVSxDQUNUO0FBQ0MsWUFBTyxNQUFQO0FBQ0EsV0FBTSxvQkFBTjtBQUNBLFNBQUksUUFBSjtBQUNBLFdBQU0sSUFBTjtBQUNBLG1CQUFjLGNBQWQ7S0FOUSxFQVFUO0FBQ0MsWUFBTyxNQUFQO0FBQ0EsV0FBTSxRQUFOO0FBQ0EsU0FBSSxVQUFKO0FBQ0EsY0FBUyxLQUFLLGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDLFVBQWxDLENBQVQ7QUFDQSxtQkFBYyxjQUFkO0tBYlEsQ0FBVixDQUREO0FBaUJDLFVBakJEO0FBREQsUUFtQk0sUUFBTDtBQUNDLGNBQVUsQ0FDVDtBQUNDLFlBQU8sUUFBUDtBQUNBLFdBQU0sb0JBQU47QUFDQSxTQUFJLFFBQUo7QUFDQSxXQUFNLElBQU47QUFDQSxtQkFBYyxjQUFkO0tBTlEsRUFRVDtBQUNDLFlBQU8sTUFBUDtBQUNBLFdBQU0sUUFBTjtBQUNBLFNBQUksVUFBSjtBQUNBLGNBQVMsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxVQUFsQyxDQUFUO0FBQ0EsbUJBQWMsY0FBZDtLQWJRLENBQVYsQ0FERDtBQWlCQyxVQWpCRDtBQW5CRCxRQXFDTSxNQUFMO0FBQ0MsY0FBVSxDQUNUO0FBQ0MsWUFBTyxNQUFQO0FBQ0EsV0FBTSxvQkFBTjtBQUNBLFNBQUksUUFBSjtBQUNBLFdBQU0sSUFBTjtBQUNBLG1CQUFjLGNBQWQ7S0FOUSxDQUFWLENBREQ7QUFVQyxVQVZEO0FBckNELFFBZ0RNLE9BQUw7QUFDQyxjQUFVLENBQ1Q7QUFDQyxZQUFPLFFBQVA7QUFDQSxXQUFNLG9CQUFOO0FBQ0EsU0FBSSxRQUFKO0FBQ0EsV0FBTSxJQUFOO0FBQ0EsbUJBQWMsY0FBZDtLQU5RLEVBUVQ7QUFDQyxZQUFPLE1BQVA7QUFDQSxXQUFNLFFBQU47QUFDQSxTQUFJLFVBQUo7QUFDQSxjQUFTLEtBQUssZ0JBQUwsQ0FBc0IsVUFBdEIsRUFBa0MsVUFBbEMsQ0FBVDtBQUNBLG1CQUFjLGNBQWQ7S0FiUSxDQUFWLENBREQ7QUFpQkMsVUFqQkQ7QUFoREQ7QUFtRUUsY0FBVSxFQUFWLENBREQ7QUFsRUQsR0FKNkI7O0FBMEU3QixTQUFPLE9BQVAsQ0ExRTZCO0VBOUJjO0FBMkc1QyxxREFBcUIsU0FBUyxpQkFBaUI7O0FBRTlDLE1BQUksbUJBQW1CLEVBQW5CLENBRjBDOztBQUk5QyxVQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsUUFBSyxTQUFMLENBREQ7QUFFQyxRQUFLLFFBQUwsQ0FGRDtBQUdDLFFBQUssTUFBTDtBQUNDLHVCQUFtQixnQkFBZ0IsR0FBaEIsQ0FBb0IsVUFBQyxJQUFELEVBQU8sR0FBUCxFQUFlO1NBRXBELFFBTUcsS0FOSCxNQUZvRDs7QUFHcEQscUJBS0csS0FMSCxjQUhvRDs7QUFJcEQsZ0JBSUcsS0FKSCxTQUpvRDs7QUFLcEQsbUJBR0csS0FISCxZQUxvRDs7QUFNcEQsZ0JBRUcsS0FGSCxTQU5vRDs7QUFPcEQsaUJBQ0csS0FESCxVQVBvRDs7O0FBVXJELFNBQUksZUFBZSxPQUFDLENBQVEsV0FBUixPQUEwQixNQUExQixXQUEwQyxlQUEzQyxHQUEwRCxHQUExRCxDQVZrQzs7QUFZckQsWUFBTztBQUNOLGFBQU8sS0FBUDtBQUNBLFlBQU0sQ0FDTDtBQUNDLGFBQU0sS0FBTjtBQUNBLGNBQU8sYUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxtQkFBWSxJQUFaO09BTEksRUFPTDtBQUNDLGFBQU0sSUFBTjtBQUNBLGNBQU8sUUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxlQUFRLEdBQVI7QUFDQSxhQUFNLFlBQU47QUFDQSxzQkFBZSxJQUFmO09BYkksRUFlTDtBQUNDLGFBQU0sT0FBTjtBQUNBLGNBQU8saUJBQWlCLFdBQWpCLENBQVA7QUFDQSxhQUFNLEdBQU47QUFDQSxnQkFBUyxDQUFUO09BbkJJLEVBcUJMO0FBQ0MsYUFBTSxNQUFOO0FBQ0EsY0FBTyxpQkFBaUIsUUFBakIsQ0FBUDtBQUNBLGFBQU0sR0FBTjtBQUNBLGdCQUFTLENBQVQ7T0F6QkksQ0FBTjtNQUZELENBWnFEO0tBQWYsQ0FBdkMsQ0FERDtBQTZDQyxVQTdDRDtBQUhELFFBaURNLE9BQUw7QUFDQyx1QkFBbUIsZ0JBQWdCLEdBQWhCLENBQW9CLFVBQUMsSUFBRCxFQUFPLEdBQVAsRUFBZTtTQUVwRCxRQU9HLEtBUEgsTUFGb0Q7O0FBR3BELHFCQU1HLEtBTkgsY0FIb0Q7O0FBSXBELGdCQUtHLEtBTEgsU0FKb0Q7O0FBS3BELGNBSUcsS0FKSCxPQUxvRDs7QUFNcEQsbUJBR0csS0FISCxZQU5vRDs7QUFPcEQsZ0JBRUcsS0FGSCxTQVBvRDs7QUFRcEQsaUJBQ0csS0FESCxVQVJvRDs7O0FBV3JELFNBQUksZUFBZSxPQUFDLENBQVEsV0FBUixPQUEwQixNQUExQixXQUEwQyxlQUEzQyxHQUEwRCxHQUExRCxDQVhrQzs7QUFhckQsWUFBTztBQUNOLGFBQU8sS0FBUDtBQUNBLFlBQU0sQ0FDTDtBQUNDLGFBQU0sS0FBTjtBQUNBLGNBQU8sYUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxtQkFBWSxJQUFaO09BTEksRUFPTDtBQUNDLGFBQU0sSUFBTjtBQUNBLGNBQU8sUUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxlQUFRLEdBQVI7QUFDQSxhQUFNLFlBQU47QUFDQSxzQkFBZSxJQUFmO09BYkksRUFlTDtBQUNDLGFBQU0sUUFBTjtBQUNBLGNBQU8saUJBQWlCLE1BQWpCLENBQVA7QUFDQSxnQkFBUyxDQUFUO0FBQ0EsYUFBTSxHQUFOO09BbkJJLENBQU47TUFGRCxDQWJxRDtLQUFmLENBQXZDLENBREQ7QUF3Q0MsVUF4Q0Q7O0FBakRELEdBSjhDOztBQWlHOUMsU0FBTyxnQkFBUCxDQWpHOEM7RUEzR0g7Q0FBbEIsQ0FBdkI7O0FBbU5KLE9BQU8sT0FBUCxHQUFpQixvQkFBakIiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiKGZ1bmN0aW9uKCkge1xuXG5cdGxldCBSZWFjdFx0XHRcdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0JyksXG5cdFx0UmVhY3RET01cdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QtZG9tJyksXG5cdFx0UXVlcnlTdG9yZVx0XHRcdFx0PSByZXF1aXJlKCcuLi9jb21tb24vc3RvcmVzL1F1ZXJ5U3RvcmUnKSxcblx0XHRQcm9kdWN0U3RvcmVcdFx0XHQ9IHJlcXVpcmUoJy4uL2NvbW1vbi9zdG9yZXMvUHJvZHVjdFN0b3JlJyksXG5cdFx0Q29tcGFyaXNvblN0b3JlXHRcdFx0PSByZXF1aXJlKCcuLi9jb21tb24vc3RvcmVzL0NvbXBhcmlzb25TdG9yZScpLFx0XHRcblx0XHRDb21tb25BY3Rpb25zXHRcdFx0PSByZXF1aXJlKCcuLi9jb21tb24vYWN0aW9ucy9Db21tb25BY3Rpb25zJyksXG5cdFx0Z2V0U2VhcmNoQ2FsbGJhY2tcdFx0PSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMvZ2V0U2VhcmNoQ2FsbGJhY2snKSxcblx0XHRTYXZpbmdzSGVscGVyU2VjdGlvblx0PSByZXF1aXJlKCcuL2NvbXBvbmVudHMvU2F2aW5nc0hlbHBlclNlY3Rpb24uanN4JyksXG5cdFx0U2F2aW5nc1Jlc3VsdFNlY3Rpb25cdD0gcmVxdWlyZSgnLi9jb21wb25lbnRzL1NhdmluZ3NSZXN1bHRTZWN0aW9uLmpzeCcpLFxuXHRcdFNhdmluZ3NQcm9kdWN0QWRkZXJcdFx0PSByZXF1aXJlKCcuL2NvbXBvbmVudHMvU2F2aW5nc1Byb2R1Y3RBZGRlci5qc3gnKTtcblxuXG5cdGxldCBtYWlubWVudSwgc3VibWVudSwgaXNMb2dnZWRJbiwgc2VhcmNoUXVlcnk7XG4gICAgXG4gICAgaWYod2luZG93LkFwcCkge1xuXHRcdCh7bWFpbm1lbnUsIHN1Ym1lbnUsIHNlYXJjaFF1ZXJ5fSA9IHdpbmRvdy5BcHAucXVlcnkpO1xuXHRcdGlzTG9nZ2VkSW4gPSB3aW5kb3cuQXBwLmNvbXBhcmlzb25zLmlzTG9nZ2VkSW47XG5cdFx0XG5cdFx0UXVlcnlTdG9yZS5yZWh5ZHJhdGUoJ3F1ZXJ5Jyk7XG5cdFx0UHJvZHVjdFN0b3JlLnJlaHlkcmF0ZSgncmVzdWx0cycpO1xuXHRcdENvbXBhcmlzb25TdG9yZS5yZWh5ZHJhdGUoJ2NvbXBhcmlzb25zJyk7XG4gICAgfVxuXG5cbi8vICAgIENvbW1vbkFjdGlvbnMuZmV0Y2hPcHRpb25zKFtcbi8vXHRcdCdBMDAwMTAxMycsICdBMDAwMTAyMycsICdBMDAwMTA0Mydcbi8vXHRdKTtcblxuXG5cdGlmKGlzTG9nZ2VkSW4pIHtcblx0XHRDb21tb25BY3Rpb25zLmZldGNoTXlQcm9kdWN0cyhtYWlubWVudSwgc3VibWVudSk7XG5cdH1cblxuXHRsZXQgcHJvZHVjdEFkZGVyID0gPFNhdmluZ3NQcm9kdWN0QWRkZXIgLz47XG5cblx0bGV0IHNlYXJjaENhbGxiYWNrID0gZ2V0U2VhcmNoQ2FsbGJhY2soc2VhcmNoUXVlcnkpOyAvLyBpZiBzZWFyY2hRdWVyeSBnaXZlbiBpbiBhZHZhbmNlLCBzZWFyY2ggcmlnaHQgYXdheVxuXHRcblxuICAgIFJlYWN0RE9NLnJlbmRlcihcblx0XHQ8U2F2aW5nc0hlbHBlclNlY3Rpb25cblx0XHRcdGFkZGVyTW9kYWw9e3Byb2R1Y3RBZGRlcn0gLz4sXG5cdFx0ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2hlbHBlcicpLFxuXHRcdHNlYXJjaENhbGxiYWNrXG4gICAgKTtcblxuXHRSZWFjdERPTS5yZW5kZXIoXG5cdFx0PFNhdmluZ3NSZXN1bHRTZWN0aW9uIC8+LFxuXHRcdGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyZXN1bHRzJylcbiAgICApO1xuXG5cbn0pKCk7XG4iLCJsZXQgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpLFxyXG5cdFByb2R1Y3RDYXJkXHRcdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9Qcm9kdWN0Q2FyZC5qc3gnKSxcclxuXHRudW1iZXJXaXRoQ29tbWFzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9udW1iZXJGaWx0ZXJGdW5jdGlvbnMnKS5udW1iZXJXaXRoQ29tbWFzO1xyXG5cclxuXHJcbmNsYXNzIExpc3RPcHRzIHtcclxuXHRjb25zdHJ1Y3RvcihsYWJlbCwgdmFsdWUpIHtcclxuXHRcdHRoaXMubGFiZWwgPSBsYWJlbDtcclxuXHRcdHRoaXMuZGF0YUF0dHJzID0geyB2YWx1ZTogdmFsdWUgfTtcclxuXHR9XHJcbn1cclxuXHJcbmZ1bmN0aW9uIGdldFNhdmluZ3NGaWx0ZXJMaXN0KCkge1xyXG5cdHJldHVybiBbXHJcblx0XHRuZXcgTGlzdE9wdHMoJ+yghOyytCcsICc5OTk5OTk5OScpLFxyXG5cdFx0bmV3IExpc3RPcHRzKCcx6riI7Jy16raMJywgJ0ZJUlNUJyksXHJcblx0XHRuZXcgTGlzdE9wdHMoJ+yggOy2leydgO2WiScsICdTRUNPTkQnKSxcclxuXHRcdG5ldyBMaXN0T3B0cygn6riw7YOAJywgJ0VMU0UnKVxyXG5cdF07XHJcbn1cclxuXHJcblxyXG5sZXQgU2F2aW5nc1Jlc3VsdFNlY3Rpb24gPSBSZWFjdC5jcmVhdGVDbGFzcyh7XHJcblx0bWl4aW5zOiBbXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcXVlcnlTdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcHJvZHVjdFN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9jb21wYXJpc29uU3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3NlbmRRdWVyeU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcmVzdWx0U2VjdGlvbk1peGluLmpzeCcpXHJcblx0XSxcclxuXHRcclxuICAgIHByb3BUeXBlczoge1xyXG5cclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHRcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG5cdGNvbXBvbmVudERpZE1vdW50KCkge1xyXG5cclxuXHR9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuXHRfZ2V0T3B0aW9ucyhzdWJtZW51LCBvcHRDb2RlTWFwKSB7XHJcblx0XHQvLyAgcmV0dXJucyB7ZmlsdGVyT3B0aW9ucywgc29ydE9wdGlvbnMsIHRvb2x0aXBFbGVtLCByZXN1bHRzRWxlbSwgaGVscGVyRWxlbX1cclxuXHRcdC8vXHJcblx0XHQvLyAgICB0b29sdGlwRWxlbSAtPiB0b29sdGlwIGVsZW1lbnQgdG8gZ28gaW4gc29ydGluZyBzZWN0aW9uXHJcblx0XHQvLyAgICByZXN1bHRzRWxlbSAtPiBsaXN0IG9mIGZpbGxlZCBpbiByZXN1bHRzIGVsZW1lbnRzIChQcm9kdWN0Q2FyZCdzKVxyXG5cdFx0Ly8gICAgaGVscGVyRWxlbSAgLT4gKGlmIG5lZWRlZCkgbW9kYWwgdG8gdXNlIG9yIGFueSBvdGhlciBlbGVtZW50XHJcblx0XHRcclxuXHRcdGxldCB7cmVzdWx0U3RhdGUsIGNvbXBhcmlzb25TdGF0ZX0gPSB0aGlzLnN0YXRlO1xyXG5cdFx0XHJcblx0XHRsZXQgeyBpc0NtcHJUYWJBY3RpdmUsIGlzTG9nZ2VkSW4sIGNvbXBhcmlzb25Hcm91cCxcdGN1cnJlbnRDbXByIH0gPSBjb21wYXJpc29uU3RhdGU7XHJcblx0XHRsZXQgc2hvd2luZ0NtcHIgPSAoaXNMb2dnZWRJbiAmJiBpc0NtcHJUYWJBY3RpdmUgJiYgY29tcGFyaXNvbkdyb3VwLmxlbmd0aCA+IDApO1xyXG5cdFx0bGV0IHNlbGVjdGVkQ21wciA9IGNvbXBhcmlzb25Hcm91cFtjdXJyZW50Q21wcl07XHJcblx0XHRcclxuXHRcdGxldCBmaWx0ZXJPcHRpb25zLCBzb3J0T3B0aW9ucywgdG9vbHRpcEVsZW0sIHJlc3VsdHNFbGVtLCBoZWxwZXJFbGVtO1xyXG5cdFx0bGV0IGNvbXBhcmlzb25Db250ZW50cyA9IHVuZGVmaW5lZDtcclxuXHRcdFxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHQvLyDsmIjquIhcclxuXHRcdFx0Y2FzZSgnZGVwb3NpdCcpOlxyXG5cdFx0XHRcdC8vIOyggeq4iFxyXG5cdFx0XHRjYXNlKCdzYXZpbmcnKTpcclxuXHRcdFx0XHQvLyBNTURBXHJcblx0XHRcdGNhc2UoJ21tZGEnKTpcclxuXHRcdFx0XHQvLyDso7ztg53ssq3slb1cclxuXHRcdFx0Y2FzZSgnaG91c2UnKTpcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gZ2V0U2F2aW5nc0ZpbHRlckxpc3QoKTtcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gdGhpcy5fYWRkTnVtUmVzdWx0cyhmaWx0ZXJPcHRpb25zKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRzb3J0T3B0aW9ucyA9IFtcclxuXHRcdFx0XHRcdG5ldyBMaXN0T3B0cygn6riI66as7IicJywgJzEnKVxyXG5cdFx0XHRcdF07IC8vIFRPRE86IG1vZGlmeSB0aGlzIHRvIHJlYWwgY29kZVxyXG5cclxuXHRcdFx0XHR0b29sdGlwRWxlbSA9IHVuZGVmaW5lZDtcclxuXHJcblx0XHRcdFx0cmVzdWx0c0VsZW0gPSByZXN1bHRTdGF0ZS5wcm9kdWN0RGF0YS5tYXAoIChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0XHRcdGxldCB7XHJcblx0XHRcdFx0XHRcdEFDX0NPREUsIC8vIOyDge2SiOy9lOuTnFxyXG5cdFx0XHRcdFx0XHREU19GTl9DT0RFX05NLCAvLyDsubTthYzqs6DrpqwgKDHquIjsnLXqtowsIDLquIjsnLXqtowsIC4uLiksXHJcblx0XHRcdFx0XHRcdEJBTktfSU1HLFxyXG5cdFx0XHRcdFx0XHREU19QUkVfVEFYLCAvLyDsnbTsnKhcclxuXHRcdFx0XHRcdFx0RFNfTk0sIC8vIOyDge2SiOuqhVxyXG5cdFx0XHRcdFx0XHREU19TVU1NQVJZMSwgLy/sg4HtkojshKTrqoUgMVxyXG5cdFx0XHRcdFx0XHREU19TVU1NQVJZMiwgLy/sg4HtkojshKTrqoUgMlxyXG5cdFx0XHRcdFx0XHR0b3RhbEFtb3VudCwgLy8g66eM6riw7IiY66C57JWhXHJcblx0XHRcdFx0XHRcdGFmdGVyVGF4LCAvLyDshLjtm4TsnbTsnpBcclxuXHRcdFx0XHRcdFx0RFNfSU5URVJORVRfWU4sIC8vIOyduO2EsOuEt+qwgOyehSDsl6zrtoBcclxuXHRcdFx0XHRcdFx0RFNfUEhPTkVfWU4sIC8vIOyKpOuniO2KuO2PsOqwgOyehSDsl6zrtoBcclxuXHRcdFx0XHRcdFx0RFNfUFJPVEVDVF9ZTiwgLy8g7JiI6riI7J6Q67O07Zi4IOyXrOu2gFxyXG5cdFx0XHRcdFx0XHREU19UQVhGUkVFX1lOLCAvLyDruYTqs7zshLgg7Jes67aAXHJcblx0XHRcdFx0XHRcdGludGVyZXN0X3luLCAvL+q0gOyLrOyDge2SiCDsl6zrtoBcclxuXHJcblx0XHRcdFx0XHRcdERTX1BFUklPRCwgLy/quLDqsIRcclxuXHRcdFx0XHRcdFx0ZGV0YWlsX3VybFxyXG5cdFx0XHRcdFx0XHQvLyBpc05ldz9cclxuXHRcdFx0XHRcdH0gPSBlbGVtO1xyXG5cclxuXHRcdFx0XHRcdGxldCBhZGRpbmdJbmZvO1xyXG5cdFx0XHRcdFx0bGV0IHN1Ym1lbnVDb2RlO1xyXG5cclxuXHJcblx0XHRcdFx0XHRzd2l0Y2goc3VibWVudS50b0xvd2VyQ2FzZSgpKSB7XHJcblx0XHRcdFx0XHRcdGNhc2UoJ2RlcG9zaXQnKTpcclxuXHRcdFx0XHRcdFx0XHRzdWJtZW51Q29kZSA9IDExMDA7XHJcblx0XHRcdFx0XHRcdFx0YWRkaW5nSW5mbyA9IGAoJHtEU19QRVJJT0R9KWA7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdGNhc2UoJ3NhdmluZycpOlxyXG5cdFx0XHRcdFx0XHRcdHN1Ym1lbnVDb2RlID0gMTIwMDtcclxuXHRcdFx0XHRcdFx0XHRhZGRpbmdJbmZvID0gYCgke0RTX1BFUklPRH0pYDtcclxuXHRcdFx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcdFx0Y2FzZSgnbW1kYScpOlxyXG5cdFx0XHRcdFx0XHRcdHN1Ym1lbnVDb2RlID0gMTMwMDtcclxuXHRcdFx0XHRcdFx0XHRhZGRpbmdJbmZvID0gJyc7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdGNhc2UoJ2hvdXNlJyk6XHJcblx0XHRcdFx0XHRcdFx0c3VibWVudUNvZGUgPSAxNDAwO1xyXG5cdFx0XHRcdFx0XHRcdGFkZGluZ0luZm8gPSBgKCR7RFNfUEVSSU9EfSlgO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGxldCBtYWluQXR0clNldCA9IHtcclxuXHRcdFx0XHRcdFx0J+unjOq4sOyImOugueyVoSc6IGAke251bWJlcldpdGhDb21tYXModG90YWxBbW91bnQpfSDsm5BgLFxyXG5cdFx0XHRcdFx0XHQn7IS47ZuE7J207J6QJzogYCR7bnVtYmVyV2l0aENvbW1hcyhhZnRlclRheCl9IOybkGBcclxuXHRcdFx0XHRcdH07XHJcblxyXG5cdFx0XHRcdFx0bGV0IGZlYXR1cmVzID0gW107XHJcblx0XHRcdFx0XHRpZihEU19JTlRFUk5FVF9ZTiA9PT0gJ1knKSB7XHJcblx0XHRcdFx0XHRcdGZlYXR1cmVzLnB1c2goJzxpIGNsYXNzPVwiYmFua2luZy1pY29uIGludGVybmV0XCI+PC9pPjxzcGFuPuyduO2EsOuEt+qwgOyehTwvc3Bhbj4nKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmKERTX1BIT05FX1lOID09PSAnWScpIHtcclxuXHRcdFx0XHRcdFx0ZmVhdHVyZXMucHVzaCgnPGkgY2xhc3M9XCJiYW5raW5nLWljb24gc21hcnRwaG9uZVwiPjwvaT48c3Bhbj7siqTrp4jtirjtj7Ag6rCA7J6FPC9zcGFuPicpO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0aWYoRFNfUFJPVEVDVF9ZTiA9PT0gJ1knKSB7XHJcblx0XHRcdFx0XHRcdGZlYXR1cmVzLnB1c2goJzxpIGNsYXNzPVwiYmFua2luZy1pY29uIHNhdmVcIj48L2k+PHNwYW4+7JiI6riI7J6QIOuztO2YuDwvc3Bhbj4nKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGlmKERTX1RBWEZSRUVfWU4gPT09ICdZJykge1xyXG5cdFx0XHRcdFx0XHRmZWF0dXJlcy5wdXNoKCc8aSBjbGFzcz1cImJhbmtpbmctaWNvbiBmcmVlXCI+PC9pPjxzcGFuPuu5hOqzvOyEuDwvc3Bhbj4nKTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdGlmKHNob3dpbmdDbXByKSB7XHJcblx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cyA9IFtcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn66eM6riw7IiY66C57JWhJyxcclxuXHRcdFx0XHRcdFx0XHRcdGRpZmY6IHRvdGFsQW1vdW50IC0gc2VsZWN0ZWRDbXByLnRvdGFsYW1vdW50LFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJ+ybkCdcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfshLjtm4TsnbTsnpAnLFxyXG5cdFx0XHRcdFx0XHRcdFx0ZGlmZjogYWZ0ZXJUYXggLSBzZWxlY3RlZENtcHIuYWZ0ZXJ0YXgsXHJcblx0XHRcdFx0XHRcdFx0XHR1bml0OiAn7JuQJ1xyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0XTtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRsZXQgaXNJbnRlcmVzdGVkID0gKGludGVyZXN0X3luID09PSAnWScpOyAvLyDqtIDsi6zsg4Htkog/XHJcblxyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdHJldHVybiAoXHJcblx0XHRcdFx0XHRcdDxQcm9kdWN0Q2FyZFxyXG5cdFx0XHRcdFx0XHRcdGtleT17YHNhdmluZ3Mke0FDX0NPREV9YH1cclxuXHRcdFx0XHRcdFx0XHR0eXBlPSd0eXBlMSdcclxuXHRcdFx0XHRcdFx0XHRjYXRlZ29yeT17RFNfRk5fQ09ERV9OTX1cclxuXHRcdFx0XHRcdFx0XHRwcm92aWRlcj17QkFOS19JTUd9XHJcblx0XHRcdFx0XHRcdFx0bGVhZGluZ0luZm89e0RTX1BSRV9UQVh9XHJcblx0XHRcdFx0XHRcdFx0cHJlZml4PXsn7JewJ31cclxuXHRcdFx0XHRcdFx0XHR1bml0PSclJ1xyXG5cdFx0XHRcdFx0XHRcdGFkZGluZ0luZm89e2FkZGluZ0luZm99XHJcblx0XHRcdFx0XHRcdFx0cHJvZHVjdFRpdGxlPXtEU19OTX1cclxuXHRcdFx0XHRcdFx0XHRmZWF0dXJlcz17ZmVhdHVyZXN9XHJcblx0XHRcdFx0XHRcdFx0ZGVzY3JpcHRpb249e0RTX1NVTU1BUlkxICsgJyAnICsgRFNfU1VNTUFSWTJ9XHJcblx0XHRcdFx0XHRcdFx0bWFpbkF0dHJTZXQ9e21haW5BdHRyU2V0fVxyXG5cdFx0XHRcdFx0XHRcdHRhcmdldE1vZGFsU2VsZWN0b3I9JyNwcm9kdWN0X3ZpZXdfcG9wdXAnXHJcblx0XHRcdFx0XHRcdFx0ZGV0YWlsVXJsPXtkZXRhaWxfdXJsfVxyXG5cdFx0XHRcdFx0XHRcdGNvbXBhcmlzb25Db250ZW50cz17Y29tcGFyaXNvbkNvbnRlbnRzfVxyXG5cdFx0XHRcdFx0XHRcdGlzSW50ZXJlc3RlZD17aXNJbnRlcmVzdGVkfVxyXG5cdFx0XHRcdFx0XHRcdGluZGV4PXtpZHh9XHJcblx0XHRcdFx0XHRcdFx0aGFuZGxlSW50ZXJlc3RDbGljaz17dGhpcy5oYW5kbGVJbnRlcmVzdENsaWNrLmJpbmQobnVsbCwgaXNJbnRlcmVzdGVkLCBzdWJtZW51Q29kZSwgQUNfQ09ERSwgaWR4KX1cclxuXHRcdFx0XHRcdFx0XHRjaG9pY2VSZXZlcnNlPXtmYWxzZX1cclxuXHRcdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cclxuXHJcblx0XHRcdGRlZmF1bHQ6XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHtmaWx0ZXJPcHRpb25zLCBzb3J0T3B0aW9ucywgdG9vbHRpcEVsZW0sIHJlc3VsdHNFbGVtLCBoZWxwZXJFbGVtfTtcclxuXHR9LFxyXG5cclxuXHRnZXRQcmVTZWFyY2hWaWV3KHN1Ym1lbnUpIHtcclxuXHRcdGxldCBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHRcdFxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRjYXNlKCdzaGlsc29uJyk6XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2NhcicpOlxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFxyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHJcblx0fVxyXG5cclxuXHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBTYXZpbmdzUmVzdWx0U2VjdGlvbjtcclxuIiwibGV0IFJlYWN0XHRcdFx0PSByZXF1aXJlKCdyZWFjdCcpLFxyXG5cdFNlbGVjdERyb3Bkb3duXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi9jb21wb25lbnRzL1NlbGVjdERyb3Bkb3duLmpzeCcpLFxyXG5cdFRleHRJbnB1dFx0XHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi9jb21wb25lbnRzL1RleHRJbnB1dC5qc3gnKSxcclxuXHRBcGlcdFx0XHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2FwaScpLFxyXG5cdENvbW1vbkFjdGlvbnNcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2FjdGlvbnMvQ29tbW9uQWN0aW9ucycpLFxyXG5cdGZvcm1WYWxpZGF0b3JzXHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2Zvcm1WYWxpZGF0b3JzJyk7XHJcblxyXG5sZXQgeyBzZWxlY3RSZXF1aXJlZCwgbnVtYmVyUmVxdWlyZWQgfSA9IGZvcm1WYWxpZGF0b3JzO1xyXG5cclxuXHJcbmNvbnN0IHN1Ym1lbnVfY29kZXMgPSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29uc3RhbnRzL3N1Ym1lbnVfY29kZXMnKTtcclxuXHJcbmNvbnN0IHNhdmluZ3NNZW51Q29kZXMgPSB7XHJcblx0ZGVwb3NpdDogJ0EwMDAxMDEnLFxyXG5cdHNhdmluZzogJ0EwMDAxMDInLFxyXG5cdG1tZGE6ICdBMDAwMTAzJyxcclxuXHRob3VzZTogJ0EwMDAxMDQnXHJcbn07XHJcblxyXG5cclxuZnVuY3Rpb24gY29udmVydFRlcm1Db2RlKHRlcm1DZCwgc3VibWVudSwgb3B0Q29kZU1hcCkge1xyXG5cdGxldCB0ZXJtR3JwQ29kZSwgdGVybVByb3AsIHRlcm1OdW07XHJcblxyXG5cclxuXHRpZihzdWJtZW51ID09PSAnbW1kYScpIHtcclxuXHRcdC8vICdtbWRhJyBkb2VzIG5vdCBoYXZlIGEgdGVybSBvcHRpb25cclxuXHRcdHRlcm1Qcm9wID0gJ0RTX1BSRV9UQVgnO1xyXG5cdFx0dGVybU51bSA9ICcwJztcclxuXHR9IGVsc2Uge1xyXG5cdFx0Ly8gZWxzZSwgZmluZCBwcm9wZXIgb3B0Q29kZU1hcFxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUpIHsgXHJcblx0XHRcdGNhc2UoJ2RlcG9zaXQnKTpcclxuXHRcdFx0XHR0ZXJtR3JwQ29kZSA9ICdBMDAwMTAxMyc7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ3NhdmluZycpOlxyXG5cdFx0XHRcdHRlcm1HcnBDb2RlID0gJ0EwMDAxMDIzJztcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSgnaG91c2UnKTpcclxuXHRcdFx0XHR0ZXJtR3JwQ29kZSA9ICdBMDAwMTA0Myc7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYob3B0Q29kZU1hcCAmJiB0ZXJtR3JwQ29kZSkge1xyXG5cdFx0XHRsZXQgb3B0ID0gb3B0Q29kZU1hcFt0ZXJtR3JwQ29kZV0uZmluZCgoZSkgPT4gZS5jZCA9PT0gdGVybUNkKTtcclxuXHJcblx0XHRcdGlmKG9wdCkge1xyXG5cdFx0XHRcdHN3aXRjaChvcHQuY2ROYW1lLnRyaW0oKSkge1xyXG5cdFx0XHRcdFx0Y2FzZSgnM+qwnOyblCcpOlxyXG5cdFx0XHRcdFx0XHR0ZXJtUHJvcCA9ICdEU19SQVRFMyc7XHJcblx0XHRcdFx0XHRcdHRlcm1OdW0gPSAnMyc7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSgnNuqwnOyblCcpOlxyXG5cdFx0XHRcdFx0XHR0ZXJtUHJvcCA9ICdEU19SQVRFNic7XHJcblx0XHRcdFx0XHRcdHRlcm1OdW0gPSAnNic7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSgnMTLqsJzsm5QnKTpcclxuXHRcdFx0XHRcdGNhc2UoJzHrhYQnKTpcclxuXHRcdFx0XHRcdFx0dGVybVByb3AgPSAnRFNfUkFURTEyJztcclxuXHRcdFx0XHRcdFx0dGVybU51bSA9ICcxMic7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSgnMjTqsJzsm5QnKTpcclxuXHRcdFx0XHRcdGNhc2UoJzLrhYQnKTpcclxuXHRcdFx0XHRcdGNhc2UoJzLrhYTsnbTsg4EnKTpcclxuXHRcdFx0XHRcdFx0dGVybVByb3AgPSAnRFNfUkFURTI0JztcclxuXHRcdFx0XHRcdFx0dGVybU51bSA9ICcyNCc7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0Y2FzZSgnMzbqsJzsm5QnKTpcclxuXHRcdFx0XHRcdGNhc2UoJzPrhYQnKTpcclxuXHRcdFx0XHRcdGNhc2UoJzPrhYTsnbTsg4EnKTpcclxuXHRcdFx0XHRcdFx0dGVybVByb3AgPSAnRFNfUkFURTM2JztcclxuXHRcdFx0XHRcdFx0dGVybU51bSA9ICczNic7XHJcblx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0ZGVmYXVsdDpcclxuXHRcdFx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKGBjZE5hbWUgZm9yIHRoZSB0ZXJtIGNvZGUgJyR7dGVybUNkfScgaXMgbm90IG9uZSBvZiBmb2xsb3dpbmdzOiAoM+qwnOyblCwgNuqwnOyblCwgMTLqsJzsm5QsIDI06rCc7JuULCAzNuqwnOyblCwgMeuFhCwgMuuFhCwgMuuFhOydtOyDgSwgM+uFhOydtOyDgSlgKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0bmV3IEVycm9yKGBDYW5ub3QgZmluZCAnJHt0ZXJtR3JwQ29kZX0nIHByb3BlcnR5IGluIG9wdENvZGVNYXBgKTtcclxuXHRcdFx0fVxyXG5cdFx0XHRcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHRocm93IG5ldyBFcnJvcignb3B0Q29kZU1hcCBvciBwcm9wZXIgZ3JwQ29kZSBmb3IgdGVybSBpcyBub3QgcHJvdmlkZWQnKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cclxuXHRyZXR1cm4geyB0ZXJtUHJvcCwgdGVybU51bSB9O1xyXG59XHJcblxyXG5cclxuXHJcbmNsYXNzIFNlbGVjdE9wdHMge1xyXG5cdGNvbnN0cnVjdG9yKGxhYmVsLCB2YWx1ZSwgZGF0YSkge1xyXG5cdFx0dGhpcy5sYWJlbCA9IGxhYmVsO1xyXG5cdFx0dGhpcy52YWx1ZSA9IHZhbHVlO1xyXG5cdFx0dGhpcy5kYXRhID0gZGF0YTtcclxuXHR9XHJcbn1cclxuXHJcblxyXG5sZXQgU2F2aW5nc1Byb2R1Y3RBZGRlciA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcclxuICAgIHByb3BUeXBlczoge1xyXG5cdFx0c3VibWVudTogUmVhY3QuUHJvcFR5cGVzLnN0cmluZyxcclxuXHRcdHNlYXJjaEVudHJpZXM6IFJlYWN0LlByb3BUeXBlcy5hcnJheSxcclxuXHRcdG9wdENvZGVNYXA6IFJlYWN0LlByb3BUeXBlcy5vYmplY3RcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblx0XHRcdHN1Ym1lbnU6ICcnLFxyXG5cdFx0XHRzZWFyY2hFbnRyaWVzOiBbXSxcclxuXHRcdFx0b3B0Q29kZU1hcDoge31cclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHRcclxuXHRnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblx0XHRcdHNob3dWYWxpZGF0aW9uOiBmYWxzZSxcclxuXHRcdFx0Y29tcGFueV9jb2RlOiAnJyxcclxuXHRcdFx0cHJvZF9jb2RlOiAnJyxcclxuXHRcdFx0cHJvZF9uYW1lOiAnJyxcclxuXHRcdFx0YW1vdW50OiAnJyxcclxuXHRcdFx0dGVybTogJycsXHJcblx0XHRcdGludGVyZXN0OiAnJyxcclxuXHRcdFx0Y29tcGFueU9wdHM6IFtdLFxyXG5cdFx0XHRwcm9kTmFtZU9wdHM6IFtdLFxyXG5cclxuXHRcdFx0cHJvZE5hbWVQbGFjZWhvbGRlcjogJ+q4sOq0gOuqheydhCDshKDtg53tlZjshLjsmpQnXHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcbiAgICBcclxuXHJcbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcclxuXHRcdGxldCBjYXRlZ29yeSA9IHNhdmluZ3NNZW51Q29kZXNbdGhpcy5wcm9wcy5zdWJtZW51LnRvTG93ZXJDYXNlKCldO1xyXG5cclxuXHRcdEFwaS5wb3N0KCcvbW9waWMvYXBpL3NhdmluZy9zZWxlY3RTYXZpbmdCYW5rTGlzdCcsIHtjYXRlZ29yeX0pXHJcblx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXMpIHtcclxuXHRcdFx0XHRpZihyZXMucmVzdWx0LnJlc3VsdENvZGUgPT09ICdTVUNDRVNTJykge1xyXG5cdFx0XHRcdFx0bGV0IHJlc3VsdERhdGEgPSByZXMucmVzdWx0LmRhdGE7XHJcblx0XHRcdFx0XHRsZXQgb3B0aW9ucyA9ICggcmVzdWx0RGF0YS5tYXAoKGUpID0+IG5ldyBTZWxlY3RPcHRzKGUuY2ROYW1lLCBlLmNkKSkgKTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdFx0XHRcdGNvbXBhbnlPcHRzOiBvcHRpb25zXHJcblx0XHRcdFx0XHR9KTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0uYmluZCh0aGlzKSk7XHJcblxyXG5cdFx0XHJcblx0XHQvLyBjbGVhciB3aGVuIGNsb3NlZFxyXG5cdFx0JCgnI3Byb2R1Y3RfYWRkX3BvcHVwJykub24oJ2hpZGRlbi5icy5tb2RhbCcsIHRoaXMuY2xlYXJTdGF0cyk7XHJcblxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuXHRcdFxyXG4gICAgfSxcclxuXHJcbiAgICByZW5kZXIoKSB7XHJcblx0XHRsZXQge1xyXG5cdFx0XHRzaG93VmFsaWRhdGlvbixcclxuXHRcdFx0Y29tcGFueU9wdHMsXHJcblx0XHRcdGNvbXBhbnlfY29kZSxcclxuXHRcdFx0cHJvZE5hbWVPcHRzLFxyXG5cdFx0XHRpbnRlcmVzdCxcclxuXHRcdFx0cHJvZF9jb2RlLFxyXG5cdFx0XHR0ZXJtLFxyXG5cdFx0XHRhbW91bnQsXHJcblx0XHRcdHByb2ROYW1lUGxhY2Vob2xkZXJcclxuXHRcdH0gPSB0aGlzLnN0YXRlO1xyXG5cdFx0bGV0IHsgc3VibWVudSwgc2VhcmNoRW50cmllcywgb3B0Q29kZU1hcCB9ID0gdGhpcy5wcm9wcztcclxuXHJcblx0XHRsZXQgY29tcGFueUZvcm0gPSAoXHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNlwiPlxyXG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuq4sOq0gOuqhTwvbGFiZWw+XHJcblx0XHRcdFx0XHQ8U2VsZWN0RHJvcGRvd25cclxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9J+yEoO2DnSdcclxuXHRcdFx0XHRcdFx0b3B0aW9ucz17Y29tcGFueU9wdHN9XHJcblx0XHRcdFx0XHRcdGhhbmRsZVNlbGVjdD17dGhpcy5oYW5kbGVDb21wYW55U2VsZWN0fVxyXG5cdFx0XHRcdFx0XHRzZWxlY3RlZD17Y29tcGFueV9jb2RlfVxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3Nob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxyXG5cdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cclxuXHRcdGxldCBwcm9kdWN0TmFtZUZvcm0gPSAoXHJcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNlwiPlxyXG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuyDge2SiOuqhTwvbGFiZWw+XHJcblx0XHRcdFx0XHQ8U2VsZWN0RHJvcGRvd25cclxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e3Byb2ROYW1lUGxhY2Vob2xkZXJ9XHJcblx0XHRcdFx0XHRcdG9wdGlvbnM9e3Byb2ROYW1lT3B0c31cclxuXHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVByb2R1Y3ROYW1lU2VsZWN0fVxyXG5cdFx0XHRcdFx0XHRzZWxlY3RlZD17cHJvZF9jb2RlfVxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3Nob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxyXG5cdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cclxuXHJcblxyXG5cdFx0bGV0IHRlcm1FbnRyeSA9IHNlYXJjaEVudHJpZXMuZmluZCgoZSkgPT4gKGUuaWQgPT09ICdwZXJpb2RDZCcpKTtcclxuXHRcdGxldCB0ZXJtRm9ybSA9IHVuZGVmaW5lZDtcclxuXHRcdGlmKHRlcm1FbnRyeSkge1xyXG5cdFx0XHR0ZXJtRm9ybSA9IChcclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNlwiPlxyXG5cdFx0XHRcdFx0XHQ8bGFiZWwgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+6rCA7J6F6riw6rCEPC9sYWJlbD5cclxuXHRcdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXHJcblx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e3Rlcm1FbnRyeS5wbGFjZWhvbGRlciB8fCAn7ISg7YOdJ31cclxuXHRcdFx0XHRcdFx0XHRvcHRpb25zPXt0ZXJtRW50cnkub3B0aW9uc31cclxuXHRcdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlVGVybVNlbGVjdH1cclxuXHRcdFx0XHRcdFx0XHRjbGFzc05hbWU9e3Rlcm1FbnRyeS5jbGFzc05hbWUgfHwgJ3NlbGVjdC1kcm9wZG93bid9XHJcblx0XHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e3Rlcm19XHJcblx0XHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtzaG93VmFsaWRhdGlvbiAmJiB0ZXJtRW50cnkudmFsaWRhdGlvbkZufVxyXG5cdFx0XHRcdFx0XHQvPlxyXG5cdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdCk7XHJcblx0XHR9XHJcblxyXG5cdFx0bGV0IGFtb3VudEVudHJ5ID0gc2VhcmNoRW50cmllcy5maW5kKChlKSA9PiAoZS5pZCA9PT0gJ2Ftb3VudCcpKTtcclxuXHRcdGxldCBhbW91bnRGb3JtID0gKFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTZcIj5cclxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9XCJzZWxlY3QtbGFiZWxcIj57YW1vdW50RW50cnkubGFiZWx9PC9sYWJlbD5cclxuXHRcdFx0XHRcdDxUZXh0SW5wdXRcclxuXHRcdFx0XHRcdFx0dHlwZT0nbnVtYmVyLXdpdGgtY29tbWFzJ1xyXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj17YW1vdW50RW50cnkucGxhY2Vob2xkZXIgfHwgJyAnfVxyXG5cdFx0XHRcdFx0XHRpbnB1dENsYXNzPSd0ZXh0LWlucHV0LW5vcm1hbCdcclxuXHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZVRleHRJbnB1dENoYW5nZS5iaW5kKG51bGwsICdhbW91bnQnKX1cclxuXHRcdFx0XHRcdFx0dmFsdWU9e2Ftb3VudH1cclxuXHRcdFx0XHRcdFx0dW5pdD17YW1vdW50RW50cnkudW5pdCB8fCAnJ31cclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtzaG93VmFsaWRhdGlvbiAmJiBhbW91bnRFbnRyeS52YWxpZGF0aW9uRm59XHJcblx0XHRcdFx0XHQvPlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdCk7XHJcblxyXG5cclxuXHRcdFxyXG5cdFx0bGV0IGludGVyZXN0Rm9ybSA9IChcclxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cclxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02XCI+XHJcblx0XHRcdFx0XHQ8bGFiZWwgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+6riI66asPC9sYWJlbD5cclxuXHRcdFx0XHRcdDxUZXh0SW5wdXRcclxuXHRcdFx0XHRcdFx0dHlwZT0nbnVtYmVyJ1xyXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0nICdcclxuXHRcdFx0XHRcdFx0aW5wdXRDbGFzcz0ndGV4dC1pbnB1dC1ub3JtYWwnXHJcblx0XHRcdFx0XHRcdGhhbmRsZUNoYW5nZT17dGhpcy5oYW5kbGVUZXh0SW5wdXRDaGFuZ2UuYmluZChudWxsLCAnaW50ZXJlc3QnKX1cclxuXHRcdFx0XHRcdFx0dmFsdWU9e2ludGVyZXN0fVxyXG5cdFx0XHRcdFx0XHR1bml0PSclJ1xyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3Nob3dWYWxpZGF0aW9uICYmIG51bWJlclJlcXVpcmVkfVxyXG5cdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0PC9kaXY+XHJcblx0XHQpO1xyXG5cclxuXHRcdFxyXG5cdFx0XHJcblx0XHRyZXR1cm4gKFxyXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsIGZhZGVcIiBpZD1cInByb2R1Y3RfYWRkX3BvcHVwXCI+XHJcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtb2RhbC1kaWFsb2dcIj5cclxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtY29udGVudCBtb2RhbC1zbWFsbFwiPlxyXG5cclxuXHRcdFx0XHRcdFx0PGEgY2xhc3NOYW1lPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiIGFyaWEtbGFiZWw9XCJDbG9zZVwiIC8+XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWhlYWRlclwiPlxyXG5cdFx0XHRcdFx0XHRcdDxoNCBjbGFzc05hbWU9XCJtb2RhbC10aXRsZVwiIGlkPVwibXlNb2RhbExhYmVsXCI+6rCA7J6F7IOB7ZKIIOy2lOqwgO2VmOq4sDwvaDQ+XHJcblx0XHRcdFx0XHRcdDwvZGl2PlxyXG5cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtYm9keVwiPlxyXG5cdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicHJvZHVjdF9hZGRcIj5cclxuXHJcblx0XHRcdFx0XHRcdFx0XHR7Y29tcGFueUZvcm19XHJcblx0XHRcdFx0XHRcdFx0XHR7cHJvZHVjdE5hbWVGb3JtfVxyXG5cdFx0XHRcdFx0XHRcdFx0e3Rlcm1Gb3JtfVxyXG5cdFx0XHRcdFx0XHRcdFx0e2Ftb3VudEZvcm19XHJcblx0XHRcdFx0XHRcdFx0XHR7aW50ZXJlc3RGb3JtfVxyXG5cdFx0XHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzc05hbWU9XCJ0eHRcIj4o6riI66as66W8IOyeheugpe2VmOyngCDslYrsnLzsi6Qg6rK97JqwLCDrk7HroZ3snbwg7ZiE7J6sIOq4sOykgCDquIjrpqzroZwg7KCB7Jqp65Cp64uI64ukLik8L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNlbnRlcl9idG5cIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGEgY2xhc3NOYW1lPVwiYnRuIGJ0bi1zdWJtaXRcIiByb2xlPVwiYnV0dG9uXCIgb25DbGljaz17dGhpcy5zdWJtaXRQcm9kdWN0fT7tmZXsnbg8L2E+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblxyXG5cdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHQ8L2Rpdj5cclxuXHRcdCk7XHJcbiAgICB9LFxyXG5cclxuXHJcblx0aGFuZGxlQ29tcGFueVNlbGVjdChldnQpIHtcclxuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xyXG5cdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XHJcblx0XHRcclxuXHRcdGxldCBjb21wYW55Q29kZSA9ICQodGFyZ2V0KS5jbG9zZXN0KCdsaScpLmF0dHIoJ2RhdGEtdmFsJykgfHwgdGFyZ2V0LnRleHRDb250ZW50O1xyXG5cclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRjb21wYW55X2NvZGU6IGNvbXBhbnlDb2RlLFxyXG5cdFx0XHRwcm9kX2NvZGU6ICcnLFxyXG5cdFx0XHRwcm9kTmFtZU9wdHM6IFtdLFxyXG5cdFx0XHRpbnRlcmVzdDogJydcclxuXHRcdH0pO1xyXG5cclxuXHRcdHRoaXMuZmV0Y2hQcm9kdWN0TmFtZXMoY29tcGFueUNvZGUpO1xyXG5cdH0sXHJcblxyXG5cdGZldGNoUHJvZHVjdE5hbWVzKGNvbXBhbnlfY29kZSkge1xyXG5cdFx0bGV0IGRzQ29kZSA9IHNhdmluZ3NNZW51Q29kZXNbdGhpcy5wcm9wcy5zdWJtZW51LnRvTG93ZXJDYXNlKCldO1xyXG5cclxuXHRcdEFwaS5wb3N0KCcvbW9waWMvYXBpL3NhdmluZy9maWx0ZXJQcm9kdWN0TGlzdCcsIHsgZHNCYW5raW5nQ29kZTogY29tcGFueV9jb2RlLCBkc0NvZGUgfSlcclxuXHRcdFx0LnRoZW4oZnVuY3Rpb24gKHJlcykge1xyXG5cdFx0XHRcdGlmKHJlcy5yZXN1bHQucmVzdWx0Q29kZSA9PT0gJ1NVQ0NFU1MnKSB7XHJcblx0XHRcdFx0XHRsZXQgcmVzdWx0RGF0YSA9IHJlcy5yZXN1bHQuZGF0YTtcclxuXHJcblx0XHRcdFx0XHRpZihyZXN1bHREYXRhICYmIHJlc3VsdERhdGEubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdFx0XHRsZXQgb3B0aW9ucyA9ICggcmVzdWx0RGF0YS5tYXAoKGUpID0+IG5ldyBTZWxlY3RPcHRzKGUuRFNfTk0sIGUuQUNfQ09ERSwgZSkpICk7XHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdFx0XHRwcm9kTmFtZU9wdHM6IG9wdGlvbnMsXHJcblx0XHRcdFx0XHRcdFx0cHJvZE5hbWVQbGFjZWhvbGRlcjogJ+yEoO2DnSdcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnNldFN0YXRlKHtcclxuXHRcdFx0XHRcdFx0XHRwcm9kTmFtZVBsYWNlaG9sZGVyOiAn7ZW064u5IOyhsOqxtOydmCDsg4HtkojsnbQg7KG07J6s7ZWY7KeAIOyViuyKteuLiOuLpCdcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9LmJpbmQodGhpcykpO1xyXG5cclxuXHR9LFxyXG5cclxuXHRoYW5kbGVQcm9kdWN0TmFtZVNlbGVjdChldnQpIHtcclxuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xyXG5cdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XHJcblx0XHRcclxuXHRcdGxldCBwcm9kQ2QgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpIHx8IHRhcmdldC50ZXh0Q29udGVudDtcclxuXHRcdGxldCB7IHRlcm0gfSA9IHRoaXMuc3RhdGU7XHJcblxyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdHByb2RfY29kZTogcHJvZENkLFxyXG5cdFx0XHRpbnRlcmVzdDogJydcclxuXHRcdH0pO1xyXG5cclxuXHRcdGlmKHRlcm0gfHwgdGhpcy5wcm9wcy5zdWJtZW51LnRvTG93ZXJDYXNlKCkgPT09ICdtbWRhJykge1xyXG5cdFx0XHR0aGlzLnNldEludGVyZXN0UmF0ZShwcm9kQ2QsIHRlcm0pO1xyXG5cdFx0fVxyXG5cdH0sXHJcblxyXG5cdFxyXG5cdGhhbmRsZVRlcm1TZWxlY3QoZXZ0KSB7XHJcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcclxuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xyXG5cdFx0XHRcclxuXHRcdGxldCB0ZXJtID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ2xpJykuYXR0cignZGF0YS12YWwnKSB8fCB0YXJnZXQudGV4dENvbnRlbnQ7XHJcblx0XHRsZXQgeyBwcm9kX2NvZGUgfSA9IHRoaXMuc3RhdGU7XHJcblx0XHRcclxuXHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHR0ZXJtOiB0ZXJtLFxyXG5cdFx0XHRpbnRlcmVzdDogJydcclxuXHRcdH0pO1xyXG5cclxuXHRcdGlmKHByb2RfY29kZSkge1xyXG5cdFx0XHR0aGlzLnNldEludGVyZXN0UmF0ZShwcm9kX2NvZGUsIHRlcm0pO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0fSxcclxuXHJcblx0aGFuZGxlVGV4dElucHV0Q2hhbmdlKHF1ZXJ5QXR0ciwgZXZ0KSB7XHJcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcclxuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xyXG5cdFx0XHRcclxuXHRcdGxldCB2YWwgPSB0YXJnZXQudmFsdWU7XHJcblx0XHRcclxuXHRcdHRoaXMuc2V0U3RhdGUoeyAgLy8gcmVzZXQgcHJvZF9jb2RlIGFuZCBpbnRlcmVzdCBhcyB3ZWxsXHJcblx0XHRcdFtxdWVyeUF0dHJdOiB2YWxcclxuXHRcdH0pO1xyXG5cdH0sXHJcblxyXG5cclxuXHRjbGVhclN0YXRzKCkge1xyXG5cdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdHNob3dWYWxpZGF0aW9uOiBmYWxzZSxcclxuXHRcdFx0Y29tcGFueV9jb2RlOiAnJyxcclxuXHRcdFx0cHJvZF9jb2RlOiAnJyxcclxuXHRcdFx0cHJvZF9uYW1lOiAnJyxcclxuXHRcdFx0YW1vdW50OiAnJyxcclxuXHRcdFx0dGVybTogJycsXHJcblx0XHRcdGludGVyZXN0OiAnJyxcclxuXHRcdFx0cHJvZE5hbWVPcHRzOiBbXVxyXG5cdFx0fSk7XHJcblx0fSxcclxuXHJcblx0Y2hlY2tWYWxpZGl0eSgpIHtcclxuXHRcdGxldCB7XHJcblx0XHRcdGNvbXBhbnlfY29kZSxcclxuXHRcdFx0cHJvZF9jb2RlLFxyXG5cdFx0XHRhbW91bnQsXHJcblx0XHRcdHRlcm0sXHJcblx0XHRcdGludGVyZXN0XHJcblx0XHR9ID0gdGhpcy5zdGF0ZTtcclxuXHRcdFxyXG5cdFx0bGV0IGFsbFZhbGlkID0gdHJ1ZTtcclxuXHRcdFxyXG5cdFx0YWxsVmFsaWQgPSBhbGxWYWxpZCAmJiAoc2VsZWN0UmVxdWlyZWQoY29tcGFueV9jb2RlKSA9PT0gdHJ1ZSk7XHJcblx0XHRhbGxWYWxpZCA9IGFsbFZhbGlkICYmIChzZWxlY3RSZXF1aXJlZChwcm9kX2NvZGUpID09PSB0cnVlKTtcclxuXHRcdGFsbFZhbGlkID0gYWxsVmFsaWQgJiYgKG51bWJlclJlcXVpcmVkKGFtb3VudCkgPT09IHRydWUpO1xyXG5cdFx0YWxsVmFsaWQgPSBhbGxWYWxpZCAmJiAobnVtYmVyUmVxdWlyZWQoaW50ZXJlc3QpID09PSB0cnVlKTtcclxuXHRcdGlmKHRoaXMucHJvcHMuc3VibWVudS50b0xvd2VyQ2FzZSgpICE9PSAnbW1kYScpIHtcclxuXHRcdFx0YWxsVmFsaWQgPSBhbGxWYWxpZCAmJiAoc2VsZWN0UmVxdWlyZWQodGVybSkgPT09IHRydWUpO1xyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiBhbGxWYWxpZDtcclxuXHR9LFxyXG5cclxuXHRzZXRJbnRlcmVzdFJhdGUocHJvZENkLCB0ZXJtKSB7XHJcblx0XHRsZXQgeyB0ZXJtUHJvcCB9ID0gY29udmVydFRlcm1Db2RlKHRlcm0sIHRoaXMucHJvcHMuc3VibWVudSwgdGhpcy5wcm9wcy5vcHRDb2RlTWFwKTtcclxuXHRcdFxyXG5cdFx0bGV0IG9wdCA9IHRoaXMuc3RhdGUucHJvZE5hbWVPcHRzLmZpbmQoKGUpID0+IChlLnZhbHVlID09PSBwcm9kQ2QpKTtcclxuXHRcdGxldCBpbnRlcmVzdCA9IFx0b3B0LmRhdGFbdGVybVByb3BdIHx8IG9wdC5kYXRhWydEU19QUkVfVEFYJ107XHJcblxyXG5cdFx0aWYoaW50ZXJlc3QgfHwgaW50ZXJlc3QgPT09IDApIHtcclxuXHRcdFx0dGhpcy5zZXRTdGF0ZSh7XHJcblx0XHRcdFx0aW50ZXJlc3Q6IFN0cmluZyhpbnRlcmVzdClcclxuXHRcdFx0fSk7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0XHJcblx0c3VibWl0UHJvZHVjdChldnQpIHtcclxuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xyXG5cdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XHJcblx0XHRcdFxyXG5cdFx0bGV0IHsgXHRcdFx0XHJcblx0XHRcdGNvbXBhbnlfY29kZSxcclxuXHRcdFx0cHJvZF9jb2RlLFxyXG5cdFx0XHRhbW91bnQsXHJcblx0XHRcdHRlcm0sXHJcblx0XHRcdGludGVyZXN0LFxyXG5cdFx0XHRwcm9kTmFtZU9wdHMsXHJcblx0XHR9ID0gdGhpcy5zdGF0ZTtcclxuXHRcdGxldCB7IHN1Ym1lbnUsIHNlYXJjaEVudHJpZXMsIG9wdENvZGVNYXAgfSA9IHRoaXMucHJvcHM7XHJcblx0XHRsZXQgZGF0YTtcclxuXHJcblx0XHRsZXQgYWxsVmFsaWQgPSB0aGlzLmNoZWNrVmFsaWRpdHkoKTtcclxuXHJcblx0XHRpZihhbGxWYWxpZCkge1xyXG5cdFx0XHRcclxuXHRcdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRcdGNhc2UoJ2RlcG9zaXQnKTpcclxuXHRcdFx0XHRjYXNlKCdzYXZpbmcnKTpcclxuXHRcdFx0XHRjYXNlKCdtbWRhJyk6XHJcblx0XHRcdFx0Y2FzZSgnaG91c2UnKTpcclxuXHRcdFx0XHRcdGRhdGEgPSB7XHJcblx0XHRcdFx0XHRcdHByb2R1Y3RfdHlwZTogU3RyaW5nKHN1Ym1lbnVfY29kZXNbc3VibWVudV0pLFxyXG5cdFx0XHRcdFx0XHRwcm9kX2NvZGU6IFN0cmluZyhwcm9kX2NvZGUpLFxyXG5cdFx0XHRcdFx0XHRwcm9kX25hbWU6IFN0cmluZyhwcm9kTmFtZU9wdHMuZmluZCgoZWxlbSkgPT4gKGVsZW0udmFsdWUgPT09IHByb2RfY29kZSkpLmxhYmVsKSxcclxuXHRcdFx0XHRcdFx0YW1vdW50OiBTdHJpbmcoYW1vdW50KSxcclxuXHRcdFx0XHRcdFx0dGVybV9jb2RlOiBTdHJpbmcodGVybSksXHJcblx0XHRcdFx0XHRcdGludGVyZXN0OiBTdHJpbmcoaW50ZXJlc3QpLFxyXG5cdFx0XHRcdFx0XHR0ZXJtOiBTdHJpbmcoY29udmVydFRlcm1Db2RlKHRlcm0sIHN1Ym1lbnUudG9Mb3dlckNhc2UoKSwgb3B0Q29kZU1hcCkudGVybU51bSlcclxuXHRcdFx0XHRcdH07XHJcblx0XHRcdFx0XHRicmVhaztcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0bGV0ICR0YXJnZXQgPSAkKHRhcmdldCk7XHJcblx0XHRcdENvbW1vbkFjdGlvbnMuYWRkTXlQcm9kdWN0KGRhdGEsICgpID0+IHtcclxuXHRcdFx0XHQkdGFyZ2V0LmNsb3Nlc3QoJy5tb2RhbCcpLm1vZGFsKCdoaWRlJyk7XHJcblx0XHRcdH0pO1xyXG5cclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHRoaXMuc2V0U3RhdGUoe1xyXG5cdFx0XHRcdHNob3dWYWxpZGF0aW9uOiB0cnVlXHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdH1cclxuXHRcclxuXHRcclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFNhdmluZ3NQcm9kdWN0QWRkZXI7XHJcbiIsImxldCBSZWFjdFx0XHRcdFx0PSByZXF1aXJlKCdyZWFjdCcpLFxyXG5cdG51bWJlcldpdGhDb21tYXNcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL251bWJlckZpbHRlckZ1bmN0aW9ucycpLm51bWJlcldpdGhDb21tYXMsXHJcblx0Zm9ybVZhbGlkYXRvcnNcdFx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvZm9ybVZhbGlkYXRvcnMnKTtcclxuXHJcbmxldCB7IHNlbGVjdFJlcXVpcmVkLCBudW1iZXJSZXF1aXJlZCB9ID0gZm9ybVZhbGlkYXRvcnM7XHJcblxyXG5cclxubGV0IFNhdmluZ3NIZWxwZXJTZWN0aW9uID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xyXG5cdG1peGluczogW1xyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3F1ZXJ5U3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3Byb2R1Y3RTdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvY29tcGFyaXNvblN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9zZW5kUXVlcnlNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2hlbHBlclNlY3Rpb25NaXhpbi5qc3gnKSxcclxuXHRdLFxyXG5cdFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHRcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuXHRcdFx0XHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIFxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcblxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuXHRfZ2V0U2VhcmNoRW50cmllcyhxdWVyeVN0YXRlKSB7XHJcblx0XHRsZXQgeyBzdWJtZW51LCBzZWFyY2hRdWVyeSwgb3B0Q29kZU1hcCB9ID0gcXVlcnlTdGF0ZTtcclxuXHRcdGxldCBlbnRyaWVzO1xyXG5cdFx0XHJcblx0XHRzd2l0Y2goc3VibWVudS50b0xvd2VyQ2FzZSgpKSB7XHJcblx0XHRcdGNhc2UoJ2RlcG9zaXQnKTpcclxuXHRcdFx0XHRlbnRyaWVzID0gW1xyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRsYWJlbDogJ+yYiOy5mOq4iOyVoScsXHJcblx0XHRcdFx0XHRcdHR5cGU6ICdudW1iZXItd2l0aC1jb21tYXMnLFxyXG5cdFx0XHRcdFx0XHRpZDogJ2Ftb3VudCcsXHJcblx0XHRcdFx0XHRcdHVuaXQ6ICfrp4zsm5AnLFxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm46IG51bWJlclJlcXVpcmVkXHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRsYWJlbDogJ+yYiOy5mOq4sOqwhCcsXHJcblx0XHRcdFx0XHRcdHR5cGU6ICdzZWxlY3QnLFxyXG5cdFx0XHRcdFx0XHRpZDogJ3BlcmlvZENkJyxcclxuXHRcdFx0XHRcdFx0b3B0aW9uczogdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsICdBMDAwMTAxMycpLFxyXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm46IHNlbGVjdFJlcXVpcmVkXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSgnc2F2aW5nJyk6XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfsm5Qg7KCA7LaV6riI7JWhJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ251bWJlci13aXRoLWNvbW1hcycsXHJcblx0XHRcdFx0XHRcdGlkOiAnYW1vdW50JyxcclxuXHRcdFx0XHRcdFx0dW5pdDogJ+unjOybkCcsXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25GbjogbnVtYmVyUmVxdWlyZWRcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn6rCA7J6F6riw6rCEJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAncGVyaW9kQ2QnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgJ0EwMDAxMDIzJyksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRdO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRjYXNlKCdtbWRhJyk6XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfsmIjsuZjquIjslaEnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnbnVtYmVyLXdpdGgtY29tbWFzJyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdhbW91bnQnLFxyXG5cdFx0XHRcdFx0XHR1bml0OiAn66eM7JuQJyxcclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuOiBudW1iZXJSZXF1aXJlZFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdF07XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2hvdXNlJyk6XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfsm5Qg7KCA7LaV6riI7JWhJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ251bWJlci13aXRoLWNvbW1hcycsXHJcblx0XHRcdFx0XHRcdGlkOiAnYW1vdW50JyxcclxuXHRcdFx0XHRcdFx0dW5pdDogJ+unjOybkCcsXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25GbjogbnVtYmVyUmVxdWlyZWRcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdGxhYmVsOiAn64Kp7J6F6riw6rCEJyxcclxuXHRcdFx0XHRcdFx0dHlwZTogJ3NlbGVjdCcsXHJcblx0XHRcdFx0XHRcdGlkOiAncGVyaW9kQ2QnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgJ0EwMDAxMDQzJyksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRdO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdGVudHJpZXMgPSBbXTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZW50cmllcztcclxuXHR9LFxyXG5cclxuXHRfZ2V0Q21wckNhcmRDb250ZW50cyhzdWJtZW51LCBjb21wYXJpc29uR3JvdXApIHtcclxuXHJcblx0XHRsZXQgY21wckNhcmRDb250ZW50cyA9IFtdO1xyXG5cclxuXHRcdHN3aXRjaChzdWJtZW51LnRvTG93ZXJDYXNlKCkpIHtcclxuXHRcdFx0Y2FzZSgnZGVwb3NpdCcpOlxyXG5cdFx0XHRjYXNlKCdzYXZpbmcnKTpcclxuXHRcdFx0Y2FzZSgnbW1kYScpOlxyXG5cdFx0XHRcdGNtcHJDYXJkQ29udGVudHMgPSBjb21wYXJpc29uR3JvdXAubWFwKChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0XHRcdGxldCB7XHJcblx0XHRcdFx0XHRcdGRzX25tLCAvL+yDge2SiOuqhVxyXG5cdFx0XHRcdFx0XHRkc19iYW5raW5nX25tLCAvLyDsnYDtlonrqoVcclxuXHRcdFx0XHRcdFx0aW50ZXJlc3QsIC8v6riI66asXHJcblx0XHRcdFx0XHRcdHRvdGFsYW1vdW50LCAvL+unjOq4sOyImOugueyVoVxyXG5cdFx0XHRcdFx0XHRhZnRlcnRheCwgLy/shLjtm4TsnbTsnpBcclxuXHRcdFx0XHRcdFx0ZHNfcGVyaW9kXHJcblx0XHRcdFx0XHR9ID0gZWxlbTtcclxuXHJcblx0XHRcdFx0XHRsZXQgaW50ZXJlc3RVbml0ID0gKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSAhPT0gJ21tZGEnKSA/IGAlICgke2RzX3BlcmlvZH0pYCA6ICclJztcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0cmV0dXJuXHR7XHJcblx0XHRcdFx0XHRcdHRpdGxlOiBkc19ubSxcclxuXHRcdFx0XHRcdFx0aW5mbzogW1xyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfquLDqtIDrqoUnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IGRzX2Jhbmtpbmdfbm0sXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiAzLFxyXG5cdFx0XHRcdFx0XHRcdFx0aXNQcm92aWRlcjogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+q4iOumrCcsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogaW50ZXJlc3QsXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiAzLFxyXG5cdFx0XHRcdFx0XHRcdFx0cHJlZml4OiAn7JewJyxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6IGludGVyZXN0VW5pdCxcclxuXHRcdFx0XHRcdFx0XHRcdGlzTGVhZGluZ0luZm86IHRydWVcclxuXHRcdFx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfrp4zquLDsiJjroLnslaEnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IG51bWJlcldpdGhDb21tYXModG90YWxhbW91bnQpLFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJ+ybkCcsXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiAzXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn7IS47ZuE7J207J6QJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBudW1iZXJXaXRoQ29tbWFzKGFmdGVydGF4KSxcclxuXHRcdFx0XHRcdFx0XHRcdHVuaXQ6ICfsm5AnLFx0XHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDNcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdF1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0Y2FzZSgnaG91c2UnKTpcclxuXHRcdFx0XHRjbXByQ2FyZENvbnRlbnRzID0gY29tcGFyaXNvbkdyb3VwLm1hcCgoZWxlbSwgaWR4KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQge1xyXG5cdFx0XHRcdFx0XHRkc19ubSwgLy/sg4HtkojrqoVcclxuXHRcdFx0XHRcdFx0ZHNfYmFua2luZ19ubSwgLy/snYDtlonrqoVcclxuXHRcdFx0XHRcdFx0aW50ZXJlc3QsIC8v6riI66asXHJcblx0XHRcdFx0XHRcdGFtb3VudCwgLy/sm5TrgqnsnoXslaFcclxuXHRcdFx0XHRcdFx0dG90YWxhbW91bnQsIC8v66eM6riw7IiY66C57JWhXHJcblx0XHRcdFx0XHRcdGFmdGVydGF4LCAvL+yEuO2bhOydtOyekFxyXG5cdFx0XHRcdFx0XHRkc19wZXJpb2RcclxuXHRcdFx0XHRcdH0gPSBlbGVtO1xyXG5cclxuXHRcdFx0XHRcdGxldCBpbnRlcmVzdFVuaXQgPSAoc3VibWVudS50b0xvd2VyQ2FzZSgpICE9PSAnbW1kYScpID8gYCUgKCR7ZHNfcGVyaW9kfSlgIDogJyUnO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRyZXR1cm5cdHtcclxuXHRcdFx0XHRcdFx0dGl0bGU6IGRzX25tLFxyXG5cdFx0XHRcdFx0XHRpbmZvOiBbXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+ydgO2WieuqhScsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogZHNfYmFua2luZ19ubSxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDQsXHJcblx0XHRcdFx0XHRcdFx0XHRpc1Byb3ZpZGVyOiB0cnVlXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn6riI66asJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBpbnRlcmVzdCxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDQsXHJcblx0XHRcdFx0XHRcdFx0XHRwcmVmaXg6ICfsl7AnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogaW50ZXJlc3RVbml0LFxyXG5cdFx0XHRcdFx0XHRcdFx0aXNMZWFkaW5nSW5mbzogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+yblCDsoIDstpXquIjslaEnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dmFsdWU6IG51bWJlcldpdGhDb21tYXMoYW1vdW50KSxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDQsXHJcblx0XHRcdFx0XHRcdFx0XHR1bml0OiAn7JuQJ1xyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdF1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHRicmVhaztcclxuXHRcdFx0XHRcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gY21wckNhcmRDb250ZW50cztcclxuXHJcblx0fVxyXG5cdFxyXG4gICAgXHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSBTYXZpbmdzSGVscGVyU2VjdGlvbjtcclxuIl19
