function fn_parseComma(num) {
 /*
  *  * fn_parseComma(123456789)	  =  123,456,789
  * fn_parseComma("123456789")	=  123,456,789
  * fn_parseComma(+123456789)	 = +123,456,789
  * fn_parseComma(-123456789)	 = -123,456,789
  * fn_parseComma(123456789.052)  =  123,456,789.052
  * fn_parseComma(-123456789.052) = -123,456,789.052
  * */

	var regix = /(^[+-]?\d+)(\d{3})/;
	var result = num.toString();

	while (true) {
		var noTested = !(regix.test(result));

		if ( noTested ) {
			break;
		}

		result = result.replace(regix, '$1' + ',' + '$2');
	}

  return result;
}

function setPageNavigation(obj, totalCnt, pageNum, returnUri, naviType, rowCount, naviSize) {
	if(naviType == null || naviType == "")
		naviType = 1;
	if(rowCount == null || rowCount == "")
		rowCount		= 10;
	if(naviSize == null || naviSize == "")
		naviSize		= 10;
	
	var totalPage		= totalCnt % rowCount > 0 ? Math.floor(totalCnt / rowCount) + 1 : totalCnt / rowCount;
	var pageGroup		= Math.floor(pageNum / naviSize) + 1;
	var pageGroupStart	= pageGroup == 1 ? 1 : ((pageGroup - 1) * naviSize) + 1;
	var pageGroupEnd	= (pageGroupStart + naviSize) - 1 > totalPage ? totalPage : (pageGroupStart + naviSize) - 1;
	var pagePrev		= pageGroupStart - 1;
	var pageNext		= pageGroupEnd + 1;
	var naviHTML		= "";
	
	if(naviType == 1) {
		naviHTML +="<ul class='pagination pagination-sm'>";
		
		if(pagePrev == 0)
			naviHTML +="	<li><a href='#' class='prev' title='이전 페이지'></a></li>";
		else
			naviHTML +="	<li><a href='" + returnUri + "?pageNum=" + pagePrev + "' class='prev' title='이전 페이지'></a></li>";
		
		for(var i = pageGroupStart; i <= pageGroupEnd; i++) {
			if(i == pageNum)
				naviHTML +="	<li class='active'>" + i + "</li>";
			else
				naviHTML +="	<li><a href='" + returnUri + "?pageNum=" + i + "' title='" + i + "페이지'>" + i + "</a></li>";
		}
		
		if(pageNext > totalPage)
			naviHTML +="	<li><a href='#' class='next' title='다음 페이지'></a></li>";
		else
			naviHTML +="	<li><a href='" + returnUri + "?pageNum=" + pageNext + "' class='next' title='다음 페이지'></a></li>";
		
		naviHTML +="</ul>";
	}

	obj.append(naviHTML);
}

function fn_clickTodayProduct(detailUrl){

	if ( detailUrl.toLowerCase().indexOf('http://') > -1 || detailUrl.toLowerCase().indexOf('https://') > -1 ) {
		window.open(detailUrl,'_blank');
	}else{
		(function () {
					
			var targetModalElem = $('#product_view_popup');
			
			if ( detailUrl.indexOf('card') > -1 ){
				$('body').attr("id","card");
				
				targetModalElem.on('hidden.bs.modal', function () {
					$('body').attr("id","");
				})
			}
			
			targetModalElem.data('idx', 1);
			targetModalElem.removeData('bs.modal');

			targetModalElem.addClass('hide');

			targetModalElem.modal().find('.modal-body').load(detailUrl, function (responseText, textStatus) {

				if (textStatus === 'success' || textStatus === 'notmodified') {
					targetModalElem.removeClass('in');
					targetModalElem.removeClass('hide');

					$.ajax({
						dataType: "script",
						cache: true,
						url: '/resources/js/detail-modal-actions.js'
					});

					setTimeout(function () {
						targetModalElem.addClass('in');
					}, 50);
				} else {
					targetModalElem.modal('hide');
				}
			});
		})();
	}
}