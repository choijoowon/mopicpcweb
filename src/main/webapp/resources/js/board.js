$(function() {
	// 전역 프로퍼티
	var boardProperties = {};
	
	// 공지사항 프로퍼티
	var noticeProperties = {
		bltnType	: "1000001"
		, rowCount	: "10"
		, naviSize	: "10"
		, listUrl	: "/mopic/api/selectBoardList"
		, detailUrl	: "/mopic/api/selectBoardDetail"
		, returnUrl	: "/information/noticeList"
	}
	
	// 공지사항 프로퍼티
	var faqProperties = {
		bltnType	: "1000002"
		, rowCount	: "10"
		, naviSize	: "10"
		, listUrl	: "/mopic/api/selectBoardList"
		, detailUrl	: "/mopic/api/selectBoardDetail"
		, returnUrl	: "/information/faqList"
	}
	
	// 1:1문의 프로퍼티
	var inquireProperties = {
		bltnType	: "1000003"
		, rowCount	: "10"
		, naviSize	: "10"
		, listUrl	: "/mopic/api/selectBoardList"
		, detailUrl	: "/mopic/api/selectBoardDetailList"
		, returnUrl	: "/mypage/inquireList"
		, deleteUrl	: "/mopic/api/deleteBoardData"
	}
	
	// 전역프로퍼티 세팅
	function setBoardProperty(opt) {
		$.extend(boardProperties, opt[0]);
		
		if(boardProperties["bltnType"] == "1000001")
			$.extend(boardProperties, noticeProperties);
		else if(boardProperties["bltnType"] == "1000002")
			$.extend(boardProperties, faqProperties);
		else if(boardProperties["bltnType"] == "1000003")
			$.extend(boardProperties, inquireProperties);
	}
	
	function completeLocation() {
		location.href=boardProperties["returnUrl"];
	}
	
	// 공지사항 리스트 출력
	function drawNoticeList(obj, result) {
		var iHTML			= "";
		var totalCnt		= result.totalCnt;
		var listLen 		= result.data.length;
		var data			= null;
		var detailObj		= null;
		
		if(listLen > 0) {
			for(var i = 0; i < listLen; i++) {
				data = result.data[i];
				var num = data.topNoticeYn == "Y" ? "공지" : data.rnum;
				
				iHTML += "<div class='panel panel-default'>";
				iHTML += "	<div class='panel-heading' role='tab' id='heading01'>";
				iHTML += "		<h4 class='panel-title notice'>";
				iHTML += "			<a data-toggle='collapse' data-parent='#accordion' href='#collapse" + i + "' aria-expanded='false' aria-controls='collapse01'>";
				iHTML += "				<span class='num'>" + num + "</span>";
				iHTML += "				<span class='title'>" + data.title + "</span>";
				iHTML += "				<span class='date'>" + data.regDate + "</span>";
				iHTML += "			</a>";
				iHTML += "		</h4>";
				iHTML += "	</div>";
				iHTML += "	<div id='collapse" + i + "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading01'>";
				iHTML += "		<div class='panel-body'>";
				iHTML += "			<div class='carat'></div>";
				iHTML += "			<div class='event_out'>" + data.cts + "</div>";
				iHTML += "		</div>";
				iHTML += "	</div>";
				iHTML += "</div>";
			}
		} else {
			iHTML += "<div class='panel panel-default'>";
			iHTML += "	<div class='panel-heading' role='tab' id='heading01'>";
			iHTML += "		<h4 class='panel-title notice'>조회된 컨텐츠가 없습니다.</h4>";
			iHTML += "	</div>";
			iHTML += "</div>";
		}
		
		obj.append(iHTML);
	}
	
	// faq 리스트 출력
	function drawFaqList(obj, result) {
		var iHTML			= "";
		var totalCnt		= result.totalCnt;
		var listLen 		= result.data.length;
		var data			= null;
		var detailObj		= null;
		
		if(listLen > 0) {
			for(var i = 0; i < listLen; i++) {
				data = result.data[i];
				
				iHTML += "<div class='panel panel-default'>";
				iHTML += "	<div class='panel-heading' role='tab' id='heading01'>";
				iHTML += "		<h4 class='panel-title notice'>";
				iHTML += "			<a data-toggle='collapse' data-parent='#accordion' href='#collapse" + i + "' aria-expanded='false' aria-controls='collapse01'>";
				iHTML += "				<span class='title'>Q. " + data.title + "</span>";
				iHTML += "			</a>";
				iHTML += "		</h4>";
				iHTML += "	</div>";
				iHTML += "	<div id='collapse" + i + "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='heading01'>";
				iHTML += "		<div class='panel-body'>";
				iHTML += "			<div class='carat'></div>";
				iHTML += "			<div class='event_out'>" + data.cts + "</div>";
				iHTML += "		</div>";
				iHTML += "	</div>";
				iHTML += "</div>";
			}
		} else {
			iHTML += "<div class='panel panel-default'>";
			iHTML += "	<div class='panel-heading' role='tab' id='heading01'>";
			iHTML += "		<h4 class='panel-title notice'>조회된 컨텐츠가 없습니다.</h4>";
			iHTML += "	</div>";
			iHTML += "</div>";
		}
		
		obj.append(iHTML);
	}
	
	// 1:1문의 리스트 출력
	function drawInquireList(obj, result) {
		var iHTML			= "";
		var totalCnt		= result.totalCnt;
		var listLen 		= result.data.length;
		var data			= null;
		var detailObj		= null;
		
		if(listLen > 0) {
			for(var i = 0; i < listLen; i++) {
				data = result.data[i];

				iHTML += "<tr data-id='" + data.bltnCd + "'>";
				iHTML += "	<td class='txt-center'>" + data.categoryName + "</td>";
				iHTML += "	<td><a href='/mypage/inquireView?bltnCd=" + data.bltnCd + "&pageNum=" + data.pageNumber + "'>" + data.title + "</a></td>";
				iHTML += "	<td class='txt-center'>" + data.regDate + "</td>";
				iHTML += "	<td class='txt-center'>" + (data.ansCnt == 0 ? "답변미완료" : "답변완료") + "</td>";
				iHTML += "	<td class='txt-center'><a href='javascript:deleteBoardData(\"" + data.bltnCd + "\");' name='btnDelete' class='btn_close'></a></td>";
				iHTML += "</tr>";
			}
		} else {
			iHTML +="<tr><td colspan='5' class='list_none'>등록된 글이 없습니다.</td></tr>";
		}
		
		obj.append(iHTML);
	}
	
	function setPageNavigation(naviType, totalCnt) {
		console.log('setPageNavigation');
		if(naviType == null || naviType == "")
			naviType = 1;
		
		var pageNum			= boardProperties["pageNum"];
		var rowCount		= boardProperties["rowCount"];
		var naviSize		= boardProperties["naviSize"];
		var totalPage		= totalCnt % rowCount > 0 ? Math.floor(totalCnt / rowCount) + 1 : totalCnt / rowCount;
		var pageGroup		= Math.floor(pageNum / naviSize) + 1;
		var pageGroupStart	= pageGroup == 1 ? 1 : ((pageGroup - 1) * naviSize) + 1;
		var pageGroupEnd	= (pageGroupStart + naviSize) - 1 > totalPage ? totalPage : (pageGroupStart + naviSize) - 1;
		var pagePrev		= pageGroupStart - 1;
		var pageNext		= pageGroupEnd + 1;
		var naviHTML		= "";
		
		if(naviType == 1) {
			naviHTML +="<ul class='pagination pagination-sm'>";
			
			if(pagePrev == 0)
				naviHTML +="	<li><a href='#' class='prev' title='이전 페이지'></a></li>";
			else
				naviHTML +="	<li><a href='" + boardProperties["returnUrl"] + "?pageNum=" + pagePrev + "' class='prev' title='이전 페이지'></a></li>";
			
			for(var i = pageGroupStart; i <= pageGroupEnd; i++) {
				if(i == pageNum)
					naviHTML +="	<li class='active'>" + i + "</li>";
				else
					naviHTML +="	<li><a href='" + boardProperties["returnUrl"] + "?pageNum=" + i + "' title='" + i + "페이지'>" + i + "</a></li>";
			}
			
			if(pageNext > totalPage)
				naviHTML +="	<li><a href='#' class='next' title='다음 페이지'></a></li>";
			else
				naviHTML +="	<li><a href='" + boardProperties["returnUrl"] + "?pageNum=" + pageNext + "' class='next' title='다음 페이지'></a></li>";
			
			naviHTML +="</ul>";
		}
		
		$("#pageNavigation").append(naviHTML);
	}
	
	// 리스트 호출
	var boardList = {
		init : function(options) {
			var obj	= $(this);
			
			var request = {
				bltnType	: boardProperties["bltnType"]
				, pageNum	: boardProperties["pageNum"]
				, rowCount	: boardProperties["rowCount"]
			};
			
			$.ajax({
				type : 'POST'
				, url : boardProperties["listUrl"]
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						if(boardProperties["bltnType"] == "1000001")
							drawNoticeList(obj, result);
						else if(boardProperties["bltnType"] == "1000002")
							drawFaqList(obj, result);
						else if(boardProperties["bltnType"] == "1000003")
							drawInquireList(obj, result);
						
						if(result.totalCnt > 0)
							setPageNavigation(1, result.totalCnt);
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
		}
	}
	
	// 리스트 플러그인
	$.fn.callBoardList = function(opt) {
		setBoardProperty(arguments);
		return boardList.init.apply(this, arguments);
	}
	
	function drawView(result, obj) {
		var iHTML	= "";
		var bltnCd	= result.data[0].bltnCd;
		iHTML += "<table>";
		
		for(var i = 0; i < result.data.length; i++) {
			var data	= result.data[i];
			
			if(i == 0) {
				iHTML += "	<tr>";
				iHTML += "		<td class='title2 td_w130'>문의 분류</td>";
				iHTML += "		<td class='ti15'>" + data.categoryName + "</td>";
				iHTML += "	</tr>";
				iHTML += "	<tr>";
				iHTML += "		<td class='title2 td_w130'>제목</td>";
				iHTML += "		<td class='ti15'>" + data.title + "</td>";
				iHTML += "	</tr>";
				iHTML += "	<tr>";
				iHTML += "		<td class='title2 td_w130'>내용</td>";
				iHTML += "		<td class='ti15'><pre>" + data.cts + "</pre></td>";
				iHTML += "	</tr>";
			} else {
				iHTML += "	<tr>";
				iHTML += "		<td class='title2 td_w130'>답변</td>";
				iHTML += "		<td class='ti15'><pre>" + data.cts + "</pre></td>";
				iHTML += "	</tr>";
			}
			
			if(data.fileCnt > 0) {
				iHTML += "	<tr>";
				iHTML += "		<td class='title2 td_w130'>첨부파일</td>";
				iHTML += "		<td class='ti15'>";
				
				for(var j = 0; j < data.fileVO.length; j++) {
					var file	= data.fileVO[j];
					
					iHTML += "			<ul class='file'>";
					iHTML += "				<a href='/common/download?idx=" + file.fileNo + "'>" + file.fileNm + "." + file.fileExt + "</a>";	
					iHTML += "			</ul>";
				}
				iHTML += "		</td>";
				iHTML += "	</tr>";
			}
			
		}
		iHTML += "</table>";
		
		iHTML += "<div class='right_btn'>";
		if(result.data[0].ansCnt == 0) {
			iHTML += "	<a class='btn btn-submit' href='/mypage/inquireModify?bltnCd=" + bltnCd + "' role='button'>수정</a> "; 
		}
		iHTML += "	<a class='btn btn-submit' href='" + boardProperties["returnUrl"] + "?pageNum=" + boardProperties["pageNum"] + "' role='button'>목록</a>";
		iHTML += "</div>";;
		
		obj.append(iHTML);
	}
	
	function drawModify(result, obj) {
		var iHTML	= "";
		
		iHTML +="<tr>";
		iHTML +="	<th class='title2'><label for='category'>문의 분류</label></th>";
		iHTML +="	<td>";
		iHTML +="		<select name='category' id='category' tabindex='4' class='dropdown-sy' >";
		iHTML +="		</select>";
		iHTML +="	</td>";
		iHTML +="</tr>";
		iHTML +="<tr>";
		iHTML +="	<th class='title2'><label for='title'>제목</label></th>";
		iHTML +="	<td>";
		iHTML +="		<input type='text' name='title' id='title' class='text-input-normal' value='" + result.data[0].title + "' placeholder='제목을 입력하세요.' required autofocus onfocus='if (this.placeholder == '제목을 입력하세요.') {this.placeholder = '';}' onblur='if (this.placeholder == '') {this.placeholder = '제목을 입력하세요.';}'/>";
		iHTML +="	</td>";
		iHTML +="</tr>";
		iHTML +="<tr>";
		iHTML +="	<th class='title2'><label for='cts'>내용</label></th>";
		iHTML +="	<td>";
		iHTML +="		<textarea name='cts' id='cts' cols='45' rows='10' class='text-area' placeholder='내용을 입력하세요.' required autofocus onfocus='if (this.placeholder == '내용을 입력하세요.') {this.placeholder = '';}' onblur='if (this.placeholder == '') {this.placeholder = '내용을 입력하세요.';}'>" + result.data[0].cts + "</textarea>";
		iHTML +="	</td>";
		iHTML +="</tr>";
		
		if(result.data[0].fileCnt > 0) {
			iHTML +="<tr>";
			iHTML +="	<th class='title2'><label for='file1'>파일첨부</label></th>";
			iHTML +="	<td>";
			iHTML +="		<ul class='file'>";
			
			for(var j = 0; j < result.data[0].fileVO.length; j++) {
				var file	= result.data[0].fileVO[j];
				
				iHTML +="			<li>";
				iHTML +="			" + file.fileNm + "." + file.fileExt;
				iHTML +="				<!--input type='file' id='file1' name='file' /-->";
				iHTML +="			</li>";
			}
			
			iHTML +="		</ul>";
			iHTML +="	</td>";
			iHTML +="</tr>";
		}
		
		obj.append(iHTML);
		
		var request2 = {
			grpCd	: "1020"
			, delYn : "N"
		};
		
		$.ajax({
			type : 'POST'
			, url : "/mopic/api/selectCmnCdList"
			, contentType : 'application/json;charset=UTF-8'
			, data : JSON.stringify(request2)
			, dataType : 'json'
			, success : function(response) {
				var result2 = response.result;
				
				if(result2.resultCode == "SUCCESS") {
					var sHTML = "";
					for( var i = 0; i < result2.data.length; i++) {
						sHTML += "<option value='" + result2.data[i].cd + "'>" + result2.data[i].cdName + "</option>";
					}
					$("#category").append(sHTML);
					$("#category").val(result.data[0].category);
				} else {
					alert(result2.resultMessage);
				}
			}
			, error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + "Error");
			}
		});
	}
	
	// 상세데이터 호출
	var boardData = {
		init : function(options) {
			var obj	= $(this);
			
			var request = {
				bltnType	: boardProperties["bltnType"]
				, bltnCd	: boardProperties["bltnCd"]
			};
			
			$.ajax({
				type : 'POST'
				, url : boardProperties["detailUrl"]
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(boardProperties.pageType == "view")
						drawView(result, obj);
					if(boardProperties.pageType == "modify")
						drawModify(result, obj);
					
					if(result.resultCode == "SUCCESS") {
						
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
		}
	}
	
	// 상세 플러그인
	$.fn.callBoardData = function(opt) {
		setBoardProperty(arguments);
		return boardData.init.apply(this, arguments);
	}
	
	// 상세데이터 호출
	var boardWrite = {
		init : function(options) {
			var obj	= $(this);
			
			var request = {
				bltnType	: boardProperties["bltnType"]
				, bltnCd	: boardProperties["bltnCd"]
			};
			
			$.ajax({
				type : 'POST'
				, url : boardProperties["detailUrl"]
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(boardProperties.pageType == "view")
						drawView(result, obj);
					
					if(result.resultCode == "SUCCESS") {
						
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
		}
	}
	
	// 상세 플러그인
	$.fn.callBoardWrite = function(opt) {
		setBoardProperty(arguments);
		return boardWrite.init.apply(this, arguments);
	}
});

function deleteBoardData(bltnCd) {
	var request = {
		bltnCd : bltnCd
	};
	
	$.ajax({
		type : 'POST'
		, url : "/mopic/api/deleteBoardData"
		, contentType : 'application/json;charset=UTF-8'
		, data : JSON.stringify(request)
		, dataType : 'json'
		, success : function(response) {
			var result = response.result;
			
			if(result.resultCode == "SUCCESS") {
				alert("삭제 되었습니다.");
				location.reload();
			} else {
				alert(result.resultMessage);
			}
		}
	});
}
