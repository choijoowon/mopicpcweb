require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({225:[function(require,module,exports){
'use strict';

(function () {
	var React = require('react'),
	    ReactDOM = require('react-dom'),
	    QueryStore = require('../common/stores/QueryStore'),
	    ProductStore = require('../common/stores/ProductStore'),
	    ComparisonStore = require('../common/stores/ComparisonStore'),
	    getSearchCallback = require('../common/utils/getSearchCallback'),
	    CommonActions = require('../common/actions/CommonActions'),
	    ExchHelperSection = require('./components/ExchHelperSection.jsx'),
	    ExchResultSection = require('./components/ExchResultSection.jsx');

	var searchQuery = undefined;

	if (window.App) {
		searchQuery = window.App.query.searchQuery;


		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons'); // just for login state
	}

	//    CommonActions.fetchOptions([
	//		'6100', 'EX002', 'EX003'
	//	]);

	var searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away

	ReactDOM.render(React.createElement(ExchHelperSection, null), document.getElementById('helper'), searchCallback);

	ReactDOM.render(React.createElement(ExchResultSection, null), document.getElementById('results'));
})();

},{"../common/actions/CommonActions":186,"../common/stores/ComparisonStore":208,"../common/stores/ProductStore":209,"../common/stores/QueryStore":210,"../common/utils/getSearchCallback":216,"./components/ExchHelperSection.jsx":223,"./components/ExchResultSection.jsx":224,"react":177,"react-dom":20}],224:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    ProductCard = require('../../common/components/ProductCard.jsx'),
    SelectList = require('../../common/components/SelectList.jsx'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas,
    Spinner = require('../../common/components/spinner.jsx');

var ListOpts = function ListOpts(label, value) {
	_classCallCheck(this, ListOpts);

	this.label = label;
	this.dataAttrs = { value: value };
};

var InsureResultSection = React.createClass({
	displayName: 'InsureResultSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin')],

	propTypes: {},

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {};
	},
	componentDidMount: function componentDidMount() {
		//let { queryState, resultState } = this.state;

		//if (resultState.productData.length === 0) {
		//	let {mainmenu, submenu} = queryState;
		//	let query = this.getAcceptableQuery(queryState);
		//	CommonActions.fetchResults(mainmenu, submenu, query);
		//}
	},
	componentWillUnmount: function componentWillUnmount() {},
	render: function render() {
		var _state = this.state;
		var queryState = _state.queryState;
		var resultState = _state.resultState;

		var optCodeMap = queryState.optCodeMap;

		var _getOptions = this._getOptions(queryState.submenu, optCodeMap, resultState);

		var filterOptions = _getOptions.filterOptions;
		var sortOptions = _getOptions.sortOptions;
		var tooltipElem = _getOptions.tooltipElem;
		var resultsElem = _getOptions.resultsElem;
		var helperElem = _getOptions.helperElem;


		if (resultState.receivedOnce) {
			if (resultState.productData.length > 0) {
				var unit = queryState.searchQuery.toKRW === 'Y' ? '원' : resultState.productData[0].unit || '-';

				var tableHeaderElem = React.createElement(
					'tr',
					null,
					React.createElement(
						'th',
						null,
						'은행명'
					),
					React.createElement(
						'th',
						null,
						'환전금액(' + unit + ')'
					),
					React.createElement(
						'th',
						null,
						'절약금액(' + unit + ')'
					),
					React.createElement(
						'th',
						null,
						'매매기준율(%)'
					),
					React.createElement(
						'th',
						null,
						'고지기준일자'
					)
				);

				return React.createElement(
					'section',
					{ className: 'down_result' },
					React.createElement(
						SelectList,
						{
							className: 'top_sort',
							options: sortOptions,
							value: queryState.sorting || sortOptions && sortOptions[0] && sortOptions[0].dataAttrs.value || '',
							handleSelect: function handleSelect() {} },
						tooltipElem
					),
					React.createElement(
						'section',
						{ className: 'results' },
						React.createElement(
							'div',
							{ className: 'car_list' },
							React.createElement(
								'table',
								{ summary: '은형별 환율 정보' },
								React.createElement(
									'caption',
									null,
									'은형별 환율 정보'
								),
								React.createElement(
									'colgroup',
									null,
									React.createElement('col', { width: '190px' }),
									React.createElement('col', { width: '140px' }),
									React.createElement('col', { width: '140px' }),
									React.createElement('col', { width: '140px' }),
									React.createElement('col', { width: '*' })
								),
								React.createElement(
									'thead',
									null,
									tableHeaderElem
								),
								React.createElement(
									'tbody',
									null,
									resultsElem
								)
							),
							resultState.isFetching ? React.createElement(
								'div',
								{ className: resultState.isFetching ? 'text-center' : 'hide', style: { padding: '25px 0' } },
								React.createElement(Spinner, { width: '40px', height: '40px' })
							) : undefined
						)
					)
				);
			} else {
				return React.createElement(
					'div',
					{ className: 'no-result-msg' },
					React.createElement(
						'p',
						{ className: 'text-center' },
						'검색결과가 없습니다.'
					)
				);
			}
		} else {

			var fetchingMsgElem = undefined,
			    maskElem = undefined;

			if (resultState.isFetching) {
				maskElem = React.createElement('div', { className: 'mask' });

				fetchingMsgElem = React.createElement(
					'div',
					{ className: 'fetching-modal' },
					React.createElement(
						'div',
						{ className: 'fetching-dialog' },
						React.createElement(
							'div',
							{ className: 'fetching-msg text-center' },
							React.createElement(
								'div',
								null,
								React.createElement(Spinner, { width: '80px', height: '80px' })
							),
							React.createElement(
								'div',
								{ className: 'message-box' },
								React.createElement(
									'span',
									null,
									'데이터를 불러오는 중입니다.',
									React.createElement('br', null),
									'잠시만 기다려주세요.'
								)
							)
						)
					)
				);
			}

			return React.createElement(
				'div',
				{ className: 'pre-search text-center' },
				React.createElement(
					'p',
					null,
					'나에게 맞는 상품을 찾아 보세요.'
				),
				maskElem,
				fetchingMsgElem
			);
		}
	},
	_getListOptions: function _getListOptions(optCodeMap, code) {
		if (optCodeMap[code]) {
			return optCodeMap[code].map(function (e) {
				return new ListOpts(e.cdName, e.cd);
			});
		}
		return [];
	},
	_getOptions: function _getOptions(submenu, optCodeMap) {
		// returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element

		var _state2 = this.state;
		var resultState = _state2.resultState;
		var comparisonState = _state2.comparisonState;
		var isCmprTabActive = comparisonState.isCmprTabActive;
		var isLoggedIn = comparisonState.isLoggedIn;
		var comparisonGroup = comparisonState.comparisonGroup;
		var currentCmpr = comparisonState.currentCmpr;

		var showingCmpr = isLoggedIn && isCmprTabActive && comparisonGroup.length > 0;
		var selectedCmpr = comparisonGroup[currentCmpr];

		var filterOptions = undefined,
		    sortOptions = undefined,
		    tooltipElem = undefined,
		    resultsElem = undefined,
		    helperElem = undefined;
		var comparisonContents = undefined;

		sortOptions = this._getListOptions(optCodeMap, 6100);

		//tooltipElem = (
		//	<span className="badge" data-toggle="tooltip" data-placement="right" title="보험나이, 성별 등을 고려하여 가입하신 상품보다 보험료가 저렴한, 지급준비율이 높은, 부지급률이 낮은 상품을 추천해드립니다.">?</span>
		//);

		resultsElem = resultState.productData.map(function (elem, idx) {
			var bi = elem.bi;
			var //은행BI
			exchange_money = elem.exchange_money;
			var //환전금액
			saved_money = elem.saved_money;
			var //절약금액
			tr_rate = elem.tr_rate;
			var //매매기준율
			tr_date = elem.tr_date;
			var //고시기준일자
			tr_url = elem.tr_url;


			return React.createElement(
				'tr',
				{ key: 'exch' + bi + exchange_money + tr_rate },
				React.createElement(
					'td',
					null,
					React.createElement(
						'a',
						{ href: tr_url, target: '_blank' },
						React.createElement('i', { className: 'bi bi-' + bi })
					)
				),
				React.createElement(
					'td',
					null,
					exchange_money
				),
				React.createElement(
					'td',
					null,
					saved_money
				),
				React.createElement(
					'td',
					null,
					tr_rate
				),
				React.createElement(
					'td',
					null,
					tr_date
				)
			);
		});

		return { filterOptions: filterOptions, sortOptions: sortOptions, tooltipElem: tooltipElem, resultsElem: resultsElem, helperElem: helperElem };
	},
	getPreSearchView: function getPreSearchView(submenu) {
		var preSearchViewElem = undefined;

		switch (submenu.toLowerCase()) {
			case 'shilson':
				break;
			case 'car':
				break;
			default:

		}

		return preSearchViewElem;
	}
});

module.exports = InsureResultSection;

},{"../../common/components/ProductCard.jsx":193,"../../common/components/SelectList.jsx":198,"../../common/components/spinner.jsx":202,"../../common/utils/comparisonStoreMixin":214,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/sendQueryMixin":222,"react":177}],223:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    CommonActions = require('../../common/actions/CommonActions'),
    formValidators = require('../../common/utils/formValidators'),
    SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    TextInput = require('../../common/components/TextInput.jsx'),
    QuerySection = require('../../common/components/QuerySection.jsx');

var selectRequired = formValidators.selectRequired;
var numberRequired = formValidators.numberRequired;


var _tradeOptCache = {};
var _nationOptCache = {};

var initNationOpts = [{ code_nm: 'USD(미국 달러)', code_id: 'USD', nationlabel: '미국' }, { code_nm: 'JPY(일본 엔)', code_id: 'JPY', nationlabel: '일본' }]; // initial options if not fetched yet

var SelectOpts = function SelectOpts(label, value) {
	_classCallCheck(this, SelectOpts);

	this.label = label;
	this.value = value;
};

function setDefaultIfAvailable(searchQuery, optCodeMap) {
	var _this = this;

	var tradeOpts = optCodeMap['EX003'];
	var nationOpts = optCodeMap['nationOpts'];

	// update nation state when fetched
	if (!searchQuery.tr_nation && nationOpts && nationOpts.length > 0) {
		(function () {
			var firstOpt = nationOpts[0];

			if (firstOpt) {
				setTimeout(function () {
					CommonActions.addOptions({ nationOpts: nationOpts });
					CommonActions.changeSearchQuery('tr_nation', firstOpt.code_id);
				}, 50);
			}
		})();
	}

	// update trade opt state when fetched
	if (!searchQuery.exchange_type && tradeOpts && tradeOpts.length > 0) {
		(function () {
			var firstOpt = tradeOpts[0];
			if (firstOpt) {
				setTimeout(function () {
					CommonActions.changeSearchQuery('exchange_type', firstOpt.cd);
					_this.setState({
						toKRW: firstOpt.cdDesc === 'Y'
					});
				}, 50);
			}
		})();
	}
}

var ExchHelperSection = React.createClass({
	displayName: 'ExchHelperSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/sendQueryMixin')],

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {
			currentNationIdx: 0,
			toKRW: false
		};
	},
	componentWillMount: function componentWillMount() {},
	componentDidMount: function componentDidMount() {
		/* set default options */
		var _state$queryState = this.state.queryState;
		var searchQuery = _state$queryState.searchQuery;
		var optCodeMap = _state$queryState.optCodeMap;

		//cache initial trade opt state

		_tradeOptCache = optCodeMap['EX003'];
		_nationOptCache = optCodeMap['nationOpts'];

		if (searchQuery.tr_nation) {
			this.setState({
				currentNationIdx: optCodeMap['nationOpts'].findIndex(function (e) {
					return e.code_id === searchQuery.tr_nation;
				})
			});
		}

		setDefaultIfAvailable.bind(this)(searchQuery, optCodeMap);
	},
	componentWillUnmount: function componentWillUnmount() {},
	componentDidUpdate: function componentDidUpdate(prevProps, prevState) {
		// set default trade option when optCode fetched
		var _state$queryState2 = this.state.queryState;
		var searchQuery = _state$queryState2.searchQuery;
		var optCodeMap = _state$queryState2.optCodeMap;

		var tradeOpts = optCodeMap['EX003'];
		var nationOpts = optCodeMap['nationOpts'];

		if (_tradeOptCache !== tradeOpts || _nationOptCache !== nationOpts) {
			_tradeOptCache = tradeOpts;
			_nationOptCache = nationOpts;

			setDefaultIfAvailable.bind(this)(searchQuery, optCodeMap);
		}
	},
	render: function render() {
		var _state = this.state;
		var queryState = _state.queryState;
		var resultState = _state.resultState;
		var currentNationIdx = _state.currentNationIdx;
		var toKRW = _state.toKRW;
		var optCodeMap = queryState.optCodeMap;
		var submenu = queryState.submenu;
		var searchQuery = queryState.searchQuery;


		var nationOpts = optCodeMap.nationOpts || initNationOpts;
		nationOpts = nationOpts.length > 0 ? nationOpts : initNationOpts;

		var tradeTypeOpts = this.getSelectOptions(optCodeMap, 'EX003');

		var nationForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-4' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'환전할 국가'
				),
				React.createElement(SelectDropdown, {
					tabIndex: 0,
					placeholder: '선택',
					options: nationOpts.map(function (e) {
						return new SelectOpts(e.code_nm, e.code_id);
					}),
					handleSelect: this.handleSelectChange.bind(null, 'tr_nation', ['money', 'coupon']),
					className: 'select-dropdown',
					selected: queryState.searchQuery['tr_nation'],
					validationFn: queryState.showValidation && selectRequired
				})
			)
		);

		var rateTable = undefined;
		if (nationOpts[currentNationIdx].tr_rate) {
			var _nationOpts$currentNa = //송금 보낼때
			//송금 받을때
			nationOpts[currentNationIdx];
			var tr_date = _nationOpts$currentNa.tr_date;
			var //기준일
			tr_rate = _nationOpts$currentNa.tr_rate;
			var //매매기준율
			tr_buy_cash = _nationOpts$currentNa.tr_buy_cash;
			var //현찰 살때
			tr_sell_cash = _nationOpts$currentNa.tr_sell_cash;
			var //현찰 팔때
			tr_send_money = _nationOpts$currentNa.tr_send_money;
			var tr_receive_money = _nationOpts$currentNa.tr_receive_money;


			rateTable = React.createElement(
				'div',
				{ className: 'row', style: { marginTop: '-18px' } },
				React.createElement(
					'div',
					{ className: 'notice-info' },
					React.createElement(
						'span',
						null,
						'기준일: ',
						tr_date
					),
					React.createElement(
						'span',
						{ className: 'devide' },
						'|'
					),
					React.createElement(
						'span',
						null,
						'제공: KEB하나은행'
					)
				),
				React.createElement(
					'div',
					{ className: 'col-xs-12' },
					React.createElement(
						'ul',
						{ className: 'currency-box' },
						React.createElement(
							'li',
							{ className: 'first' },
							React.createElement(
								'span',
								null,
								'매매기준율'
							),
							React.createElement(
								'span',
								{ className: 'val' },
								tr_rate || '-'
							)
						),
						React.createElement(
							'li',
							null,
							React.createElement(
								'span',
								null,
								'현찰(살 때)'
							),
							React.createElement(
								'span',
								{ className: 'val' },
								tr_buy_cash || '-'
							)
						),
						React.createElement(
							'li',
							null,
							React.createElement(
								'span',
								null,
								'현찰(팔 때)'
							),
							React.createElement(
								'span',
								{ className: 'val' },
								tr_sell_cash || '-'
							)
						),
						React.createElement(
							'li',
							null,
							React.createElement(
								'span',
								null,
								'송금(보낼 때)'
							),
							React.createElement(
								'span',
								{ className: 'val' },
								tr_send_money || '-'
							)
						),
						React.createElement(
							'li',
							null,
							React.createElement(
								'span',
								null,
								'송금(받을 때)'
							),
							React.createElement(
								'span',
								{ className: 'val' },
								tr_receive_money || '-'
							)
						)
					)
				)
			);
		}

		var tradeTypeForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-4' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'거래종류'
				),
				React.createElement(SelectDropdown, {
					tabIndex: 0,
					placeholder: '선택',
					options: tradeTypeOpts,
					handleSelect: this.handleSelectChange.bind(null, 'exchange_type', ['coupon']),
					className: 'select-dropdown',
					selected: queryState.searchQuery['exchange_type'],
					validationFn: queryState.showValidation && selectRequired
				})
			)
		);

		var baseNationLabel = undefined,
		    baseMsg = undefined,
		    quoteMsg = undefined;

		if (toKRW === 'Y') {
			baseNationLabel = nationOpts[currentNationIdx].nationlabel;
			baseMsg = nationOpts[currentNationIdx].code_nm + '를';
			quoteMsg = 'KRW(대한민국 원)으로 환전합니다.';
		} else {
			baseNationLabel = '대한민국';
			baseMsg = 'KRW(대한민국 원)를';
			quoteMsg = nationOpts[currentNationIdx].code_nm + '으로 환전합니다.';
		}

		var amountForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-4' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					baseNationLabel
				),
				React.createElement(TextInput, {
					type: 'number-with-commas',
					placeholder: '금액입력',
					inputClass: 'text-input-normal',
					handleChange: this.handleTextInputChange.bind(null, 'money'),
					value: queryState.searchQuery['money'],
					validationFn: queryState.showValidation && numberRequired
				})
			),
			React.createElement(
				'div',
				{ className: 'currency-right' },
				React.createElement(
					'ul',
					null,
					React.createElement(
						'li',
						null,
						React.createElement(
							'span',
							null,
							baseMsg
						)
					),
					React.createElement(
						'li',
						null,
						React.createElement(
							'span',
							{ className: 'quote' },
							quoteMsg
						)
					)
				)
			)
		);

		var couponElem = undefined;
		if (queryState.searchQuery.exchange_type === 'TR_BUY_CASH') {
			couponElem = React.createElement(
				'div',
				{ className: 'row' },
				React.createElement(
					'div',
					{ className: 'col-xs-4' },
					React.createElement(
						'label',
						{ htmlFor: 'select', className: 'select-label coupon' },
						'우대쿠폰'
					),
					React.createElement(TextInput, {
						type: 'number-with-commas',
						placeholder: '쿠폰할인율',
						inputClass: 'text-input-normal',
						handleChange: this.handleTextInputChange.bind(null, 'coupon'),
						value: queryState.searchQuery['coupon'],
						disabled: queryState.searchQuery.exchange_type !== 'TR_BUY_CASH',
						unit: '%',
						maxlength: 3,
						maximum: 100
					})
				)
			);
		}

		return React.createElement(
			'div',
			{ className: 'top_search' },
			nationForm,
			rateTable,
			tradeTypeForm,
			amountForm,
			couponElem,
			React.createElement(
				'div',
				{ className: 'right_btn' },
				React.createElement(
					'a',
					{ id: 'search-btn', className: 'btn btn-submit', role: 'button', onClick: this.handleSearchBtnClick },
					'검색'
				)
			)
		);
	},
	handleSelectChange: function handleSelectChange(id, dependencies, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var liElem = $(target).closest('li');
		var val = liElem.attr('data-val');

		val = val === 0 ? 0 : val || target.textContent;

		CommonActions.changeSearchQuery(id, val);

		if (dependencies && dependencies.length > 0) {
			for (var i = 0, l = dependencies.length; i < l; i++) {
				var dependency = dependencies[i];
				CommonActions.changeSearchQuery(dependency, '');
			}
		}

		if (id === 'tr_nation') {
			this.setState({
				currentNationIdx: liElem.index()
			});
		}

		if (id === 'exchange_type') {
			var optCodeMap = this.state.queryState.optCodeMap;

			this.setState({
				toKRW: optCodeMap['EX003'].find(function (e) {
					return e.cd === val;
				}).cdDesc
			});
		}
	},
	handleTextInputChange: function handleTextInputChange(id, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		CommonActions.changeSearchQuery(id, target.value);
	},
	getSelectOptions: function getSelectOptions(optCodeMap, code, iconCssPrefix) {
		if (optCodeMap[code]) {
			return optCodeMap[code].map(function (e) {
				return new SelectOpts(e.cdName, e.cd);
			});
		}

		// TODO: when no map for the code?
		return [];
	},
	handleSearchBtnClick: function handleSearchBtnClick() {
		var _state2 = this.state;
		var queryState = _state2.queryState;
		var resultState = _state2.resultState;
		var searchQuery = queryState.searchQuery;


		var allValid = this.checkValidity();

		if (allValid) {
			CommonActions.setReceivedOnce(false);

			CommonActions.changeFilter(99999999);
			CommonActions.changeSorting('');

			$('body').addClass('masked'); // disable scrolling
			this.sendQueryWithCount(0, queryState, resultState, false);
		} else {
			CommonActions.validationToShow(true);
		}
	},
	checkValidity: function checkValidity() {
		var allValid = true;
		var searchQuery = this.state.queryState.searchQuery;


		allValid = allValid && selectRequired(searchQuery['tr_nation']) === true;
		allValid = allValid && selectRequired(searchQuery['exchange_type']) === true;
		allValid = allValid && numberRequired(searchQuery['money']) === true;

		return allValid;
	}
});

module.exports = ExchHelperSection;

},{"../../common/actions/CommonActions":186,"../../common/components/QuerySection.jsx":195,"../../common/components/SelectDropdown.jsx":197,"../../common/components/TextInput.jsx":200,"../../common/utils/formValidators":215,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/sendQueryMixin":222,"react":177}],202:[function(require,module,exports){
'use strict';

var React = require('react');

var refreshIntervalId = '';

var Spinner = React.createClass({
	displayName: 'Spinner',

	propTypes: {
		width: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number]),
		height: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.number])
	},

	getDefaultProps: function getDefaultProps() {
		return {
			width: '50px',
			height: '50px'
		};
	},
	render: function render() {
		return React.createElement('img', {
			src: '/resources/images/common/spinner.gif',
			style: { width: this.props.width, height: this.props.height }
		});
	}
});

module.exports = Spinner;

},{"react":177}]},{},[225])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvZXhjaGFuZ2UtcGFnZS9leGNoYW5nZS1wYWdlLmpzeCIsInNyYy9leGNoYW5nZS1wYWdlL2NvbXBvbmVudHMvRXhjaFJlc3VsdFNlY3Rpb24uanN4Iiwic3JjL2V4Y2hhbmdlLXBhZ2UvY29tcG9uZW50cy9FeGNoSGVscGVyU2VjdGlvbi5qc3giLCJzcmMvY29tbW9uL2NvbXBvbmVudHMvc3Bpbm5lci5qc3giXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7OztBQ0FBLENBQUMsWUFBVztBQUNYLEtBQUksUUFBVyxRQUFRLE9BQVIsQ0FBWDtLQUNILFdBQWEsUUFBUSxXQUFSLENBQWI7S0FDQSxhQUFlLFFBQVEsNkJBQVIsQ0FBZjtLQUNBLGVBQWdCLFFBQVEsK0JBQVIsQ0FBaEI7S0FDQSxrQkFBbUIsUUFBUSxrQ0FBUixDQUFuQjtLQUNBLG9CQUFvQixRQUFRLG1DQUFSLENBQXBCO0tBQ0EsZ0JBQWlCLFFBQVEsaUNBQVIsQ0FBakI7S0FDQSxvQkFBb0IsUUFBUSxvQ0FBUixDQUFwQjtLQUNBLG9CQUFvQixRQUFRLG9DQUFSLENBQXBCLENBVFU7O0FBWVgsS0FBSSx1QkFBSixDQVpXOztBQWNSLEtBQUcsT0FBTyxHQUFQLEVBQVk7QUFDZixnQkFBZSxPQUFPLEdBQVAsQ0FBVyxLQUFYLENBQWYsWUFEZTs7O0FBR2pCLGFBQVcsU0FBWCxDQUFxQixPQUFyQixFQUhpQjtBQUlqQixlQUFhLFNBQWIsQ0FBdUIsU0FBdkIsRUFKaUI7QUFLakIsa0JBQWdCLFNBQWhCLENBQTBCLGFBQTFCO0FBTGlCLEVBQWY7Ozs7OztBQWRRLEtBNEJQLGlCQUFpQixrQkFBa0IsV0FBbEIsQ0FBakI7O0FBNUJPLFNBK0JSLENBQVMsTUFBVCxDQUNGLG9CQUFDLGlCQUFELE9BREUsRUFFRixTQUFTLGNBQVQsQ0FBd0IsUUFBeEIsQ0FGRSxFQUdGLGNBSEUsRUEvQlE7O0FBcUNYLFVBQVMsTUFBVCxDQUNDLG9CQUFDLGlCQUFELE9BREQsRUFFQyxTQUFTLGNBQVQsQ0FBd0IsU0FBeEIsQ0FGRCxFQXJDVztDQUFYLENBQUQ7Ozs7Ozs7QUNBQSxJQUFJLFFBQVcsUUFBUSxPQUFSLENBQVg7SUFDSCxjQUFnQixRQUFRLHlDQUFSLENBQWhCO0lBQ0EsYUFBZSxRQUFRLHdDQUFSLENBQWY7SUFDQSxtQkFBbUIsUUFBUSwwQ0FBUixFQUFvRCxnQkFBcEQ7SUFDbkIsVUFBYSxRQUFRLHFDQUFSLENBQWI7O0lBR0ssV0FDTCxTQURLLFFBQ0wsQ0FBWSxLQUFaLEVBQW1CLEtBQW5CLEVBQTBCO3VCQURyQixVQUNxQjs7QUFDekIsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUR5QjtBQUV6QixNQUFLLFNBQUwsR0FBaUIsRUFBRSxPQUFPLEtBQVAsRUFBbkIsQ0FGeUI7Q0FBMUI7O0FBT0QsSUFBSSxzQkFBc0IsTUFBTSxXQUFOLENBQWtCOzs7QUFDM0MsU0FBUSxDQUNQLFFBQVEsb0NBQVIsQ0FETyxFQUVQLFFBQVEsc0NBQVIsQ0FGTyxFQUdQLFFBQVEseUNBQVIsQ0FITyxFQUlQLFFBQVEsbUNBQVIsQ0FKTyxDQUFSOztBQU9HLFlBQVcsRUFBWDs7QUFLQSw2Q0FBa0I7QUFDZCxTQUFPLEVBQVAsQ0FEYztFQWJzQjtBQW1CeEMsNkNBQWtCO0FBQ2QsU0FBTyxFQUFQLENBRGM7RUFuQnNCO0FBeUJ4QyxpREFBb0I7Ozs7Ozs7O0VBekJvQjtBQW1DeEMsdURBQXVCLEVBbkNpQjtBQXVDM0MsMkJBQVM7ZUFDd0IsS0FBSyxLQUFMLENBRHhCO01BQ0gsK0JBREc7TUFDUyxpQ0FEVDs7QUFFUixNQUFJLGFBQWEsV0FBVyxVQUFYLENBRlQ7O29CQUlpRSxLQUFLLFdBQUwsQ0FBaUIsV0FBVyxPQUFYLEVBQW9CLFVBQXJDLEVBQWlELFdBQWpELEVBSmpFOztNQUlILDBDQUpHO01BSVksc0NBSlo7TUFJeUIsc0NBSnpCO01BSXNDLHNDQUp0QztNQUltRCxvQ0FKbkQ7OztBQU9SLE1BQUcsWUFBWSxZQUFaLEVBQTBCO0FBQzVCLE9BQUcsWUFBWSxXQUFaLENBQXdCLE1BQXhCLEdBQWlDLENBQWpDLEVBQW9DO0FBQ3RDLFFBQUksT0FBTyxVQUFDLENBQVcsV0FBWCxDQUF1QixLQUF2QixLQUFpQyxHQUFqQyxHQUF3QyxHQUF6QyxHQUFnRCxZQUFZLFdBQVosQ0FBd0IsQ0FBeEIsRUFBMkIsSUFBM0IsSUFBbUMsR0FBbkMsQ0FEckI7O0FBR3RDLFFBQUksa0JBQ0g7OztLQUNDOzs7O01BREQ7S0FFQzs7O2dCQUFhLFVBQWI7TUFGRDtLQUdDOzs7Z0JBQWEsVUFBYjtNQUhEO0tBSUM7Ozs7TUFKRDtLQUtDOzs7O01BTEQ7S0FERyxDQUhrQzs7QUFhdEMsV0FDQzs7T0FBUyxXQUFVLGFBQVYsRUFBVDtLQUVDO0FBQUMsZ0JBQUQ7O0FBQ0Msa0JBQVUsVUFBVjtBQUNBLGdCQUFTLFdBQVQ7QUFDQSxjQUFRLFdBQVcsT0FBWCxJQUF1QixlQUFlLFlBQVksQ0FBWixDQUFmLElBQWlDLFlBQVksQ0FBWixFQUFlLFNBQWYsQ0FBeUIsS0FBekIsSUFBbUMsRUFBM0Y7QUFDUixxQkFBYyx3QkFBSSxFQUFKLEVBSmY7TUFNRSxXQU5GO01BRkQ7S0FXQzs7UUFBUyxXQUFVLFNBQVYsRUFBVDtNQUVDOztTQUFLLFdBQVUsVUFBVixFQUFMO09BRUM7O1VBQU8sU0FBUSxXQUFSLEVBQVA7UUFDQzs7OztTQUREO1FBRUM7OztTQUNDLDZCQUFLLE9BQU0sT0FBTixFQUFMLENBREQ7U0FFQyw2QkFBSyxPQUFNLE9BQU4sRUFBTCxDQUZEO1NBR0MsNkJBQUssT0FBTSxPQUFOLEVBQUwsQ0FIRDtTQUlDLDZCQUFLLE9BQU0sT0FBTixFQUFMLENBSkQ7U0FLQyw2QkFBSyxPQUFNLEdBQU4sRUFBTCxDQUxEO1NBRkQ7UUFTQzs7O1NBQVEsZUFBUjtTQVREO1FBVUM7OztTQUFRLFdBQVI7U0FWRDtRQUZEO09BZUcsWUFBWSxVQUFaLEdBQ0Q7O1VBQUssV0FBVyxZQUFZLFVBQVosR0FBeUIsYUFBekIsR0FBeUMsTUFBekMsRUFBaUQsT0FBTyxFQUFDLFNBQVMsUUFBVCxFQUFSLEVBQWpFO1FBQ0ksb0JBQUMsT0FBRCxJQUFTLE9BQU0sTUFBTixFQUFhLFFBQU8sTUFBUCxFQUF0QixDQURKO1FBREMsR0FJRyxTQUpIO09BakJKO01BWEQ7S0FERCxDQWJzQztJQUF2QyxNQXVETztBQUNOLFdBQ0M7O09BQUssV0FBVSxlQUFWLEVBQUw7S0FDQzs7UUFBRyxXQUFVLGFBQVYsRUFBSDs7TUFERDtLQURELENBRE07SUF2RFA7R0FERCxNQWdFTzs7QUFFTixPQUFJLDJCQUFKO09BQXFCLG9CQUFyQixDQUZNOztBQUlOLE9BQUcsWUFBWSxVQUFaLEVBQXdCO0FBQzFCLGVBQVcsNkJBQUssV0FBVSxNQUFWLEVBQUwsQ0FBWCxDQUQwQjs7QUFHMUIsc0JBQ0M7O09BQUssV0FBVSxnQkFBVixFQUFMO0tBQ0M7O1FBQUssV0FBVSxpQkFBVixFQUFMO01BQ0M7O1NBQUssV0FBVSwwQkFBVixFQUFMO09BQ0M7OztRQUNDLG9CQUFDLE9BQUQsSUFBUyxPQUFNLE1BQU4sRUFBYSxRQUFPLE1BQVAsRUFBdEIsQ0FERDtRQUREO09BSUM7O1VBQUssV0FBVSxhQUFWLEVBQUw7UUFDQzs7OztTQUFxQiwrQkFBckI7O1NBREQ7UUFKRDtPQUREO01BREQ7S0FERCxDQUgwQjtJQUEzQjs7QUFtQkEsVUFDQzs7TUFBSyxXQUFVLHdCQUFWLEVBQUw7SUFDQzs7OztLQUREO0lBRUcsUUFGSDtJQUdHLGVBSEg7SUFERCxDQXZCTTtHQWhFUDtFQTlDMEM7QUFrSjNDLDJDQUFnQixZQUFZLE1BQU07QUFDakMsTUFBRyxXQUFXLElBQVgsQ0FBSCxFQUFxQjtBQUNwQixVQUFTLFdBQVcsSUFBWCxFQUFpQixHQUFqQixDQUFxQixVQUFDLENBQUQ7V0FBTyxJQUFJLFFBQUosQ0FBYSxFQUFFLE1BQUYsRUFBVSxFQUFFLEVBQUY7SUFBOUIsQ0FBOUIsQ0FEb0I7R0FBckI7QUFHQSxTQUFPLEVBQVAsQ0FKaUM7RUFsSlM7QUEwSjNDLG1DQUFZLFNBQVMsWUFBWTs7Ozs7OztnQkFPTyxLQUFLLEtBQUwsQ0FQUDtNQU8xQixrQ0FQMEI7TUFPYiwwQ0FQYTtNQVMxQixrQkFBOEQsZ0JBQTlELGdCQVQwQjtNQVNULGFBQTZDLGdCQUE3QyxXQVRTO01BU0csa0JBQWlDLGdCQUFqQyxnQkFUSDtNQVNvQixjQUFnQixnQkFBaEIsWUFUcEI7O0FBVWhDLE1BQUksY0FBZSxjQUFjLGVBQWQsSUFBaUMsZ0JBQWdCLE1BQWhCLEdBQXlCLENBQXpCLENBVnBCO0FBV2hDLE1BQUksZUFBZSxnQkFBZ0IsV0FBaEIsQ0FBZixDQVg0Qjs7QUFhaEMsTUFBSSx5QkFBSjtNQUFtQix1QkFBbkI7TUFBZ0MsdUJBQWhDO01BQTZDLHVCQUE3QztNQUEwRCxzQkFBMUQsQ0FiZ0M7QUFjaEMsTUFBSSxxQkFBcUIsU0FBckIsQ0FkNEI7O0FBaUJoQyxnQkFBYyxLQUFLLGVBQUwsQ0FBcUIsVUFBckIsRUFBaUMsSUFBakMsQ0FBZDs7Ozs7O0FBakJnQyxhQXdCaEMsR0FBYyxZQUFZLFdBQVosQ0FBd0IsR0FBeEIsQ0FBNkIsVUFBQyxJQUFELEVBQU8sR0FBUCxFQUFlO09BRXhELEtBTUcsS0FOSCxHQUZ3RDs7QUFHeEQsb0JBS0csS0FMSCxlQUh3RDs7QUFJeEQsaUJBSUcsS0FKSCxZQUp3RDs7QUFLeEQsYUFHRyxLQUhILFFBTHdEOztBQU14RCxhQUVHLEtBRkgsUUFOd0Q7O0FBT3hELFlBQ0csS0FESCxPQVB3RDs7O0FBV3pELFVBQ0M7O01BQUksY0FBWSxLQUFLLGlCQUFpQixPQUFsQyxFQUFKO0lBQ0M7OztLQUFJOztRQUFHLE1BQU0sTUFBTixFQUFjLFFBQU8sUUFBUCxFQUFqQjtNQUFpQywyQkFBRyxzQkFBb0IsRUFBcEIsRUFBSCxDQUFqQztNQUFKO0tBREQ7SUFFQzs7O0tBQUssY0FBTDtLQUZEO0lBR0M7OztLQUFLLFdBQUw7S0FIRDtJQUlDOzs7S0FBSyxPQUFMO0tBSkQ7SUFLQzs7O0tBQUssT0FBTDtLQUxEO0lBREQsQ0FYeUQ7R0FBZixDQUEzQyxDQXhCZ0M7O0FBZ0RoQyxTQUFPLEVBQUMsNEJBQUQsRUFBZ0Isd0JBQWhCLEVBQTZCLHdCQUE3QixFQUEwQyx3QkFBMUMsRUFBdUQsc0JBQXZELEVBQVAsQ0FoRGdDO0VBMUpVO0FBNk0zQyw2Q0FBaUIsU0FBUztBQUN6QixNQUFJLDZCQUFKLENBRHlCOztBQUd6QixVQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsUUFBSyxTQUFMO0FBQ0MsVUFERDtBQURELFFBR00sS0FBTDtBQUNDLFVBREQ7QUFIRDs7R0FIeUI7O0FBWXpCLFNBQU8saUJBQVAsQ0FaeUI7RUE3TWlCO0NBQWxCLENBQXRCOztBQWlPSixPQUFPLE9BQVAsR0FBaUIsbUJBQWpCOzs7Ozs7O0FDaFBBLElBQUksUUFBVSxRQUFRLE9BQVIsQ0FBVjtJQUNILGdCQUFnQixRQUFRLG9DQUFSLENBQWhCO0lBQ0EsaUJBQWlCLFFBQVEsbUNBQVIsQ0FBakI7SUFDQSxpQkFBaUIsUUFBUSw0Q0FBUixDQUFqQjtJQUNBLFlBQWEsUUFBUSx1Q0FBUixDQUFiO0lBQ0EsZUFBZSxRQUFRLDBDQUFSLENBQWY7O0lBSUssaUJBQW1DLGVBQW5DO0lBQWdCLGlCQUFtQixlQUFuQjs7O0FBRXRCLElBQUksaUJBQWlCLEVBQWpCO0FBQ0osSUFBSSxrQkFBa0IsRUFBbEI7O0FBRUosSUFBTSxpQkFBaUIsQ0FDdEIsRUFBRSxTQUFTLFlBQVQsRUFBdUIsU0FBUyxLQUFULEVBQWdCLGFBQWEsSUFBYixFQURuQixFQUV0QixFQUFFLFNBQVMsV0FBVCxFQUFzQixTQUFTLEtBQVQsRUFBZ0IsYUFBYSxJQUFiLEVBRmxCLENBQWpCOztJQU1BLGFBQ0wsU0FESyxVQUNMLENBQVksS0FBWixFQUFtQixLQUFuQixFQUEwQjt1QkFEckIsWUFDcUI7O0FBQ3pCLE1BQUssS0FBTCxHQUFhLEtBQWIsQ0FEeUI7QUFFekIsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUZ5QjtDQUExQjs7QUFPRCxTQUFTLHFCQUFULENBQStCLFdBQS9CLEVBQTRDLFVBQTVDLEVBQXdEOzs7QUFDdkQsS0FBSSxZQUFZLFdBQVcsT0FBWCxDQUFaLENBRG1EO0FBRXZELEtBQUksYUFBYSxXQUFXLFlBQVgsQ0FBYjs7O0FBRm1ELEtBS25ELENBQUMsWUFBWSxTQUFaLElBQXlCLFVBQTFCLElBQXdDLFdBQVcsTUFBWCxHQUFvQixDQUFwQixFQUF3Qjs7QUFDbkUsT0FBSSxXQUFXLFdBQVcsQ0FBWCxDQUFYOztBQUVKLE9BQUcsUUFBSCxFQUFhO0FBQ1osZUFBVyxZQUFNO0FBQ2hCLG1CQUFjLFVBQWQsQ0FBeUIsRUFBRSxzQkFBRixFQUF6QixFQURnQjtBQUVoQixtQkFBYyxpQkFBZCxDQUFnQyxXQUFoQyxFQUE2QyxTQUFTLE9BQVQsQ0FBN0MsQ0FGZ0I7S0FBTixFQUdSLEVBSEgsRUFEWTtJQUFiO09BSG1FO0VBQXBFOzs7QUFMdUQsS0FpQm5ELENBQUMsWUFBWSxhQUFaLElBQTZCLFNBQTlCLElBQTJDLFVBQVUsTUFBVixHQUFtQixDQUFuQixFQUF1Qjs7QUFDckUsT0FBSSxXQUFXLFVBQVUsQ0FBVixDQUFYO0FBQ0osT0FBRyxRQUFILEVBQWE7QUFDWixlQUFXLFlBQU07QUFDaEIsbUJBQWMsaUJBQWQsQ0FBZ0MsZUFBaEMsRUFBaUQsU0FBUyxFQUFULENBQWpELENBRGdCO0FBRWhCLFdBQUssUUFBTCxDQUFjO0FBQ2IsYUFBTyxTQUFTLE1BQVQsS0FBb0IsR0FBcEI7TUFEUixFQUZnQjtLQUFOLEVBS1IsRUFMSCxFQURZO0lBQWI7T0FGcUU7RUFBdEU7Q0FqQkQ7O0FBaUNBLElBQUksb0JBQW9CLE1BQU0sV0FBTixDQUFrQjs7O0FBQ3pDLFNBQVEsQ0FDUCxRQUFRLG9DQUFSLENBRE8sRUFFUCxRQUFRLHNDQUFSLENBRk8sRUFHUCxRQUFRLG1DQUFSLENBSE8sQ0FBUjs7QUFNRyw2Q0FBa0I7QUFDZCxTQUFPLEVBQVAsQ0FEYztFQVBvQjtBQWF0Qyw2Q0FBa0I7QUFDZCxTQUFPO0FBQ1oscUJBQWtCLENBQWxCO0FBQ0EsVUFBTyxLQUFQO0dBRkssQ0FEYztFQWJvQjtBQW9CdEMsbURBQXFCLEVBcEJpQjtBQXdCekMsaURBQW9COzswQkFFZSxLQUFLLEtBQUwsQ0FBVyxVQUFYLENBRmY7TUFFYiw0Q0FGYTtNQUVBOzs7QUFGQTtBQUtuQixtQkFBaUIsV0FBVyxPQUFYLENBQWpCLENBTG1CO0FBTW5CLG9CQUFrQixXQUFXLFlBQVgsQ0FBbEIsQ0FObUI7O0FBUW5CLE1BQUcsWUFBWSxTQUFaLEVBQXVCO0FBQ3pCLFFBQUssUUFBTCxDQUFjO0FBQ2Isc0JBQWtCLFdBQVcsWUFBWCxFQUF5QixTQUF6QixDQUFtQyxVQUFDLENBQUQ7WUFBTyxFQUFFLE9BQUYsS0FBYyxZQUFZLFNBQVo7S0FBckIsQ0FBckQ7SUFERCxFQUR5QjtHQUExQjs7QUFNQSx3QkFBc0IsSUFBdEIsQ0FBMkIsSUFBM0IsRUFBaUMsV0FBakMsRUFBOEMsVUFBOUMsRUFkbUI7RUF4QnFCO0FBeUN0Qyx1REFBdUIsRUF6Q2U7QUE2Q3pDLGlEQUFtQixXQUFXLFdBQVc7OzJCQUVOLEtBQUssS0FBTCxDQUFXLFVBQVgsQ0FGTTtNQUVsQyw2Q0FGa0M7TUFFckIsMkNBRnFCOztBQUd4QyxNQUFJLFlBQVksV0FBVyxPQUFYLENBQVosQ0FIb0M7QUFJeEMsTUFBSSxhQUFhLFdBQVcsWUFBWCxDQUFiLENBSm9DOztBQU14QyxNQUFHLG1CQUFtQixTQUFuQixJQUFnQyxvQkFBb0IsVUFBcEIsRUFBZ0M7QUFDbEUsb0JBQWlCLFNBQWpCLENBRGtFO0FBRWxFLHFCQUFrQixVQUFsQixDQUZrRTs7QUFJbEUseUJBQXNCLElBQXRCLENBQTJCLElBQTNCLEVBQWlDLFdBQWpDLEVBQThDLFVBQTlDLEVBSmtFO0dBQW5FO0VBbkR3QztBQTJEekMsMkJBQVM7ZUFDbUQsS0FBSyxLQUFMLENBRG5EO01BQ0YsK0JBREU7TUFDVSxpQ0FEVjtNQUN1QiwyQ0FEdkI7TUFDeUMscUJBRHpDO01BRUYsYUFBcUMsV0FBckMsV0FGRTtNQUVVLFVBQXlCLFdBQXpCLFFBRlY7TUFFbUIsY0FBZ0IsV0FBaEIsWUFGbkI7OztBQUlSLE1BQUksYUFBYSxXQUFXLFVBQVgsSUFBeUIsY0FBekIsQ0FKVDtBQUtSLGVBQWEsV0FBVyxNQUFYLEdBQW9CLENBQXBCLEdBQXdCLFVBQXhCLEdBQXFDLGNBQXJDLENBTEw7O0FBT1IsTUFBSSxnQkFBZ0IsS0FBSyxnQkFBTCxDQUFzQixVQUF0QixFQUFrQyxPQUFsQyxDQUFoQixDQVBJOztBQVNSLE1BQUksYUFDSDs7S0FBSyxXQUFVLEtBQVYsRUFBTDtHQUNDOztNQUFLLFdBQVUsVUFBVixFQUFMO0lBQ0M7O09BQU8sV0FBVSxjQUFWLEVBQVA7O0tBREQ7SUFFQyxvQkFBQyxjQUFEO0FBQ0MsZUFBVSxDQUFWO0FBQ0Esa0JBQVksSUFBWjtBQUNBLGNBQVMsV0FBVyxHQUFYLENBQWUsVUFBQyxDQUFEO2FBQU8sSUFBSSxVQUFKLENBQWUsRUFBRSxPQUFGLEVBQVcsRUFBRSxPQUFGO01BQWpDLENBQXhCO0FBQ0EsbUJBQWMsS0FBSyxrQkFBTCxDQUF3QixJQUF4QixDQUE2QixJQUE3QixFQUFtQyxXQUFuQyxFQUFnRCxDQUFDLE9BQUQsRUFBVSxRQUFWLENBQWhELENBQWQ7QUFDQSxnQkFBVSxpQkFBVjtBQUNBLGVBQVUsV0FBVyxXQUFYLENBQXVCLFdBQXZCLENBQVY7QUFDQSxtQkFBYyxXQUFXLGNBQVgsSUFBNkIsY0FBN0I7S0FQZixDQUZEO0lBREQ7R0FERyxDQVRJOztBQTJCUixNQUFJLHFCQUFKLENBM0JRO0FBNEJSLE1BQUcsV0FBVyxnQkFBWCxFQUE2QixPQUE3QixFQUFzQzs7O0FBUXBDLGNBQVcsZ0JBQVgsRUFSb0M7T0FFdkMsd0NBRnVDOztBQUd2QywyQ0FIdUM7O0FBSXZDLG1EQUp1Qzs7QUFLdkMscURBTHVDOztBQU12Qyx1REFOdUM7T0FPdkMsMERBUHVDOzs7QUFVeEMsZUFDQzs7TUFBSyxXQUFVLEtBQVYsRUFBZ0IsT0FBTyxFQUFDLFdBQVcsT0FBWCxFQUFSLEVBQXJCO0lBQ0M7O09BQUssV0FBVSxhQUFWLEVBQUw7S0FBNkI7Ozs7TUFBWSxPQUFaO01BQTdCO0tBQXdEOztRQUFNLFdBQVUsUUFBVixFQUFOOztNQUF4RDtLQUF5Rjs7OztNQUF6RjtLQUREO0lBRUM7O09BQUssV0FBVSxXQUFWLEVBQUw7S0FDQzs7UUFBSSxXQUFVLGNBQVYsRUFBSjtNQUNDOztTQUFJLFdBQVUsT0FBVixFQUFKO09BQXNCOzs7O1FBQXRCO09BQXdDOztVQUFNLFdBQVUsS0FBVixFQUFOO1FBQXVCLFdBQVcsR0FBWDtRQUEvRDtPQUREO01BRUM7OztPQUFJOzs7O1FBQUo7T0FBd0I7O1VBQU0sV0FBVSxLQUFWLEVBQU47UUFBdUIsZUFBZSxHQUFmO1FBQS9DO09BRkQ7TUFHQzs7O09BQUk7Ozs7UUFBSjtPQUF3Qjs7VUFBTSxXQUFVLEtBQVYsRUFBTjtRQUF1QixnQkFBZ0IsR0FBaEI7UUFBL0M7T0FIRDtNQUlDOzs7T0FBSTs7OztRQUFKO09BQXlCOztVQUFNLFdBQVUsS0FBVixFQUFOO1FBQXVCLGlCQUFpQixHQUFqQjtRQUFoRDtPQUpEO01BS0M7OztPQUFJOzs7O1FBQUo7T0FBeUI7O1VBQU0sV0FBVSxLQUFWLEVBQU47UUFBdUIsb0JBQW9CLEdBQXBCO1FBQWhEO09BTEQ7TUFERDtLQUZEO0lBREQsQ0FWd0M7R0FBekM7O0FBMkJBLE1BQUksZ0JBQ0g7O0tBQUssV0FBVSxLQUFWLEVBQUw7R0FDQzs7TUFBSyxXQUFVLFVBQVYsRUFBTDtJQUNDOztPQUFPLFdBQVUsY0FBVixFQUFQOztLQUREO0lBRUMsb0JBQUMsY0FBRDtBQUNDLGVBQVUsQ0FBVjtBQUNBLGtCQUFZLElBQVo7QUFDQSxjQUFTLGFBQVQ7QUFDQSxtQkFBYyxLQUFLLGtCQUFMLENBQXdCLElBQXhCLENBQTZCLElBQTdCLEVBQW1DLGVBQW5DLEVBQW9ELENBQUMsUUFBRCxDQUFwRCxDQUFkO0FBQ0EsZ0JBQVUsaUJBQVY7QUFDQSxlQUFVLFdBQVcsV0FBWCxDQUF1QixlQUF2QixDQUFWO0FBQ0EsbUJBQWMsV0FBVyxjQUFYLElBQTZCLGNBQTdCO0tBUGYsQ0FGRDtJQUREO0dBREcsQ0F2REk7O0FBMEVSLE1BQUksMkJBQUo7TUFBcUIsbUJBQXJCO01BQThCLG9CQUE5QixDQTFFUTs7QUE0RVIsTUFBRyxVQUFVLEdBQVYsRUFBZTtBQUNqQixxQkFBa0IsV0FBVyxnQkFBWCxFQUE2QixXQUE3QixDQUREO0FBRWpCLGFBQWEsV0FBVyxnQkFBWCxFQUE2QixPQUE3QixNQUFiLENBRmlCO0FBR2pCLGNBQVcsc0JBQVgsQ0FIaUI7R0FBbEIsTUFJTztBQUNOLHFCQUFrQixNQUFsQixDQURNO0FBRU4sYUFBVSxjQUFWLENBRk07QUFHTixjQUFjLFdBQVcsZ0JBQVgsRUFBNkIsT0FBN0IsY0FBZCxDQUhNO0dBSlA7O0FBVUEsTUFBSSxhQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDtLQUFpQyxlQUFqQztLQUREO0lBRUMsb0JBQUMsU0FBRDtBQUNDLFdBQUssb0JBQUw7QUFDQSxrQkFBWSxNQUFaO0FBQ0EsaUJBQVcsbUJBQVg7QUFDQSxtQkFBYyxLQUFLLHFCQUFMLENBQTJCLElBQTNCLENBQWdDLElBQWhDLEVBQXNDLE9BQXRDLENBQWQ7QUFDQSxZQUFPLFdBQVcsV0FBWCxDQUF1QixPQUF2QixDQUFQO0FBQ0EsbUJBQWMsV0FBVyxjQUFYLElBQTZCLGNBQTdCO0tBTmYsQ0FGRDtJQUREO0dBWUM7O01BQUssV0FBVSxnQkFBVixFQUFMO0lBQ0M7OztLQUNDOzs7TUFBSTs7O09BQU8sT0FBUDtPQUFKO01BREQ7S0FFQzs7O01BQUk7O1NBQU0sV0FBVSxPQUFWLEVBQU47T0FBeUIsUUFBekI7T0FBSjtNQUZEO0tBREQ7SUFaRDtHQURHLENBdEZJOztBQTRHUixNQUFJLHNCQUFKLENBNUdRO0FBNkdSLE1BQUcsV0FBVyxXQUFYLENBQXVCLGFBQXZCLEtBQXlDLGFBQXpDLEVBQXdEO0FBQzFELGdCQUNDOztNQUFLLFdBQVUsS0FBVixFQUFMO0lBQ0M7O09BQUssV0FBVSxVQUFWLEVBQUw7S0FDQzs7UUFBTyxTQUFRLFFBQVIsRUFBaUIsV0FBVSxxQkFBVixFQUF4Qjs7TUFERDtLQUVDLG9CQUFDLFNBQUQ7QUFDQyxZQUFLLG9CQUFMO0FBQ0EsbUJBQVksT0FBWjtBQUNBLGtCQUFXLG1CQUFYO0FBQ0Esb0JBQWMsS0FBSyxxQkFBTCxDQUEyQixJQUEzQixDQUFnQyxJQUFoQyxFQUFzQyxRQUF0QyxDQUFkO0FBQ0EsYUFBTyxXQUFXLFdBQVgsQ0FBdUIsUUFBdkIsQ0FBUDtBQUNBLGdCQUFVLFdBQVcsV0FBWCxDQUF1QixhQUF2QixLQUF5QyxhQUF6QztBQUNWLFlBQUssR0FBTDtBQUNBLGlCQUFXLENBQVg7QUFDQSxlQUFTLEdBQVQ7TUFURCxDQUZEO0tBREQ7SUFERCxDQUQwRDtHQUEzRDs7QUFzQkEsU0FDQzs7S0FBSyxXQUFVLFlBQVYsRUFBTDtHQUVFLFVBRkY7R0FHRSxTQUhGO0dBS0UsYUFMRjtHQU9FLFVBUEY7R0FTRSxVQVRGO0dBWUM7O01BQUssV0FBVSxXQUFWLEVBQUw7SUFDQzs7T0FBRyxJQUFHLFlBQUgsRUFBZ0IsV0FBVSxnQkFBVixFQUEyQixNQUFLLFFBQUwsRUFBYyxTQUFTLEtBQUssb0JBQUwsRUFBckU7O0tBREQ7SUFaRDtHQURELENBbklRO0VBM0RnQztBQXFOekMsaURBQW1CLElBQUksY0FBYyxLQUFLO0FBQ3pDLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRHdCO0FBRXpDLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGYzs7QUFJekMsTUFBSSxTQUFTLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsQ0FBVCxDQUpxQztBQUt6QyxNQUFJLE1BQU0sT0FBTyxJQUFQLENBQVksVUFBWixDQUFOLENBTHFDOztBQU96QyxRQUFNLEdBQUMsS0FBUSxDQUFSLEdBQWEsQ0FBZCxHQUFtQixPQUFPLE9BQU8sV0FBUCxDQVBTOztBQVN6QyxnQkFBYyxpQkFBZCxDQUFnQyxFQUFoQyxFQUFvQyxHQUFwQyxFQVR5Qzs7QUFXekMsTUFBRyxnQkFBZ0IsYUFBYSxNQUFiLEdBQXNCLENBQXRCLEVBQXlCO0FBQzNDLFFBQUksSUFBSSxJQUFJLENBQUosRUFBTyxJQUFJLGFBQWEsTUFBYixFQUFxQixJQUFJLENBQUosRUFBTyxHQUEvQyxFQUFvRDtBQUNuRCxRQUFJLGFBQWEsYUFBYSxDQUFiLENBQWIsQ0FEK0M7QUFFbkQsa0JBQWMsaUJBQWQsQ0FBZ0MsVUFBaEMsRUFBNEMsRUFBNUMsRUFGbUQ7SUFBcEQ7R0FERDs7QUFPQSxNQUFHLE9BQU8sV0FBUCxFQUFvQjtBQUN0QixRQUFLLFFBQUwsQ0FBYztBQUNiLHNCQUFrQixPQUFPLEtBQVAsRUFBbEI7SUFERCxFQURzQjtHQUF2Qjs7QUFNQSxNQUFHLE9BQU8sZUFBUCxFQUF3QjtPQUNwQixhQUFlLEtBQUssS0FBTCxDQUFXLFVBQVgsQ0FBZixXQURvQjs7QUFFMUIsUUFBSyxRQUFMLENBQWM7QUFDYixXQUFPLFdBQVcsT0FBWCxFQUFvQixJQUFwQixDQUF5QixVQUFDLENBQUQ7WUFBTyxFQUFFLEVBQUYsS0FBUyxHQUFUO0tBQVAsQ0FBekIsQ0FBOEMsTUFBOUM7SUFEUixFQUYwQjtHQUEzQjtFQTdPd0M7QUFzUHpDLHVEQUFzQixJQUFJLEtBQUs7QUFDOUIsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FEYTtBQUU5QixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRkc7O0FBSTlCLGdCQUFjLGlCQUFkLENBQWdDLEVBQWhDLEVBQW9DLE9BQU8sS0FBUCxDQUFwQyxDQUo4QjtFQXRQVTtBQTZQekMsNkNBQWlCLFlBQVksTUFBTSxlQUFlO0FBQ2pELE1BQUcsV0FBVyxJQUFYLENBQUgsRUFBcUI7QUFDcEIsVUFBUyxXQUFXLElBQVgsRUFBaUIsR0FBakIsQ0FBcUIsVUFBQyxDQUFEO1dBQU8sSUFBSSxVQUFKLENBQWUsRUFBRSxNQUFGLEVBQVUsRUFBRSxFQUFGO0lBQWhDLENBQTlCLENBRG9CO0dBQXJCOzs7QUFEaUQsU0FNMUMsRUFBUCxDQU5pRDtFQTdQVDtBQXNRekMsdURBQXVCO2dCQUNZLEtBQUssS0FBTCxDQURaO01BQ2hCLGdDQURnQjtNQUNKLGtDQURJO01BRWhCLGNBQWdCLFdBQWhCLFlBRmdCOzs7QUFJdEIsTUFBSSxXQUFXLEtBQUssYUFBTCxFQUFYLENBSmtCOztBQU10QixNQUFHLFFBQUgsRUFBYTtBQUNaLGlCQUFjLGVBQWQsQ0FBOEIsS0FBOUIsRUFEWTs7QUFHWixpQkFBYyxZQUFkLENBQTJCLFFBQTNCLEVBSFk7QUFJWixpQkFBYyxhQUFkLENBQTRCLEVBQTVCLEVBSlk7O0FBTVosS0FBRSxNQUFGLEVBQVUsUUFBVixDQUFtQixRQUFuQjtBQU5ZLE9BT1osQ0FBSyxrQkFBTCxDQUF3QixDQUF4QixFQUEyQixVQUEzQixFQUF1QyxXQUF2QyxFQUFvRCxLQUFwRCxFQVBZO0dBQWIsTUFRTztBQUNOLGlCQUFjLGdCQUFkLENBQStCLElBQS9CLEVBRE07R0FSUDtFQTVRd0M7QUF5UnpDLHlDQUFnQjtBQUNmLE1BQUksV0FBVyxJQUFYLENBRFc7TUFFVCxjQUFnQixLQUFLLEtBQUwsQ0FBVyxVQUFYLENBQWhCLFlBRlM7OztBQUlmLGFBQVcsWUFBYSxlQUFlLFlBQVksV0FBWixDQUFmLE1BQTZDLElBQTdDLENBSlQ7QUFLZixhQUFXLFlBQWEsZUFBZSxZQUFZLGVBQVosQ0FBZixNQUFpRCxJQUFqRCxDQUxUO0FBTWYsYUFBVyxZQUFhLGVBQWUsWUFBWSxPQUFaLENBQWYsTUFBeUMsSUFBekMsQ0FOVDs7QUFRZixTQUFPLFFBQVAsQ0FSZTtFQXpSeUI7Q0FBbEIsQ0FBcEI7O0FBc1NKLE9BQU8sT0FBUCxHQUFpQixpQkFBakI7Ozs7O0FDbldBLElBQUksUUFBUSxRQUFRLE9BQVIsQ0FBUjs7QUFFSixJQUFJLG9CQUFvQixFQUFwQjs7QUFFSixJQUFJLFVBQVUsTUFBTSxXQUFOLENBQWtCOzs7QUFDL0IsWUFBVztBQUNWLFNBQU8sTUFBTSxTQUFOLENBQWdCLFNBQWhCLENBQTBCLENBQ2hDLE1BQU0sU0FBTixDQUFnQixNQUFoQixFQUNBLE1BQU0sU0FBTixDQUFnQixNQUFoQixDQUZNLENBQVA7QUFJQSxVQUFRLE1BQU0sU0FBTixDQUFnQixTQUFoQixDQUEwQixDQUNqQyxNQUFNLFNBQU4sQ0FBZ0IsTUFBaEIsRUFDQSxNQUFNLFNBQU4sQ0FBZ0IsTUFBaEIsQ0FGTyxDQUFSO0VBTEQ7O0FBV0csNkNBQWtCO0FBQ2QsU0FBTztBQUNaLFVBQU8sTUFBUDtBQUNBLFdBQVEsTUFBUjtHQUZLLENBRGM7RUFaVTtBQW1CL0IsMkJBQVE7QUFDUCxTQUNDO0FBQ0MsUUFBSSxzQ0FBSjtBQUNBLFVBQU8sRUFBQyxPQUFPLEtBQUssS0FBTCxDQUFXLEtBQVgsRUFBa0IsUUFBUSxLQUFLLEtBQUwsQ0FBVyxNQUFYLEVBQXpDO0dBRkQsQ0FERCxDQURPO0VBbkJ1QjtDQUFsQixDQUFWOztBQTZCSixPQUFPLE9BQVAsR0FBaUIsT0FBakIiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiKGZ1bmN0aW9uKCkge1xyXG5cdGxldCBSZWFjdFx0XHRcdFx0PSByZXF1aXJlKCdyZWFjdCcpLFxyXG5cdFx0UmVhY3RET01cdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0LWRvbScpLFxyXG5cdFx0UXVlcnlTdG9yZVx0XHRcdD0gcmVxdWlyZSgnLi4vY29tbW9uL3N0b3Jlcy9RdWVyeVN0b3JlJyksXHJcblx0XHRQcm9kdWN0U3RvcmVcdFx0PSByZXF1aXJlKCcuLi9jb21tb24vc3RvcmVzL1Byb2R1Y3RTdG9yZScpLFxyXG5cdFx0Q29tcGFyaXNvblN0b3JlXHRcdD0gcmVxdWlyZSgnLi4vY29tbW9uL3N0b3Jlcy9Db21wYXJpc29uU3RvcmUnKSxcclxuXHRcdGdldFNlYXJjaENhbGxiYWNrXHQ9IHJlcXVpcmUoJy4uL2NvbW1vbi91dGlscy9nZXRTZWFyY2hDYWxsYmFjaycpLFxyXG5cdFx0Q29tbW9uQWN0aW9uc1x0XHQ9IHJlcXVpcmUoJy4uL2NvbW1vbi9hY3Rpb25zL0NvbW1vbkFjdGlvbnMnKSxcdFx0XHJcblx0XHRFeGNoSGVscGVyU2VjdGlvblx0PSByZXF1aXJlKCcuL2NvbXBvbmVudHMvRXhjaEhlbHBlclNlY3Rpb24uanN4JyksXHJcblx0XHRFeGNoUmVzdWx0U2VjdGlvblx0PSByZXF1aXJlKCcuL2NvbXBvbmVudHMvRXhjaFJlc3VsdFNlY3Rpb24uanN4Jyk7XHJcblxyXG5cdFxyXG5cdGxldCBzZWFyY2hRdWVyeTtcclxuICAgIFxyXG4gICAgaWYod2luZG93LkFwcCkge1xyXG5cdFx0KHtzZWFyY2hRdWVyeX0gPSB3aW5kb3cuQXBwLnF1ZXJ5KTtcclxuXHRcdFxyXG5cdFx0UXVlcnlTdG9yZS5yZWh5ZHJhdGUoJ3F1ZXJ5Jyk7XHJcblx0XHRQcm9kdWN0U3RvcmUucmVoeWRyYXRlKCdyZXN1bHRzJyk7XHJcblx0XHRDb21wYXJpc29uU3RvcmUucmVoeWRyYXRlKCdjb21wYXJpc29ucycpOyAvLyBqdXN0IGZvciBsb2dpbiBzdGF0ZVxyXG4gICAgfVxyXG5cclxuXHJcbi8vICAgIENvbW1vbkFjdGlvbnMuZmV0Y2hPcHRpb25zKFtcclxuLy9cdFx0JzYxMDAnLCAnRVgwMDInLCAnRVgwMDMnXHJcbi8vXHRdKTtcclxuXHJcblxyXG5cdGxldCBzZWFyY2hDYWxsYmFjayA9IGdldFNlYXJjaENhbGxiYWNrKHNlYXJjaFF1ZXJ5KTsgLy8gaWYgc2VhcmNoUXVlcnkgZ2l2ZW4gaW4gYWR2YW5jZSwgc2VhcmNoIHJpZ2h0IGF3YXlcclxuXHRcclxuXHJcbiAgICBSZWFjdERPTS5yZW5kZXIoXHJcblx0XHQ8RXhjaEhlbHBlclNlY3Rpb24gLz4sXHJcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaGVscGVyJyksXHJcblx0XHRzZWFyY2hDYWxsYmFja1xyXG4gICAgKTtcclxuXHJcblx0UmVhY3RET00ucmVuZGVyKFxyXG5cdFx0PEV4Y2hSZXN1bHRTZWN0aW9uIC8+LFxyXG5cdFx0ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jlc3VsdHMnKVxyXG4gICAgKTtcclxuXHJcblxyXG59KSgpO1xyXG4iLCJsZXQgUmVhY3RcdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcclxuXHRQcm9kdWN0Q2FyZFx0XHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvUHJvZHVjdENhcmQuanN4JyksXHJcblx0U2VsZWN0TGlzdFx0XHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvU2VsZWN0TGlzdC5qc3gnKSxcclxuXHRudW1iZXJXaXRoQ29tbWFzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9udW1iZXJGaWx0ZXJGdW5jdGlvbnMnKS5udW1iZXJXaXRoQ29tbWFzLFxyXG5cdFNwaW5uZXJcdFx0XHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvc3Bpbm5lci5qc3gnKTtcclxuXHJcblxyXG5jbGFzcyBMaXN0T3B0cyB7XHJcblx0Y29uc3RydWN0b3IobGFiZWwsIHZhbHVlKSB7XHJcblx0XHR0aGlzLmxhYmVsID0gbGFiZWw7XHJcblx0XHR0aGlzLmRhdGFBdHRycyA9IHsgdmFsdWU6IHZhbHVlIH07XHJcblx0fVxyXG59XHJcblxyXG5cclxubGV0IEluc3VyZVJlc3VsdFNlY3Rpb24gPSBSZWFjdC5jcmVhdGVDbGFzcyh7XHJcblx0bWl4aW5zOiBbXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcXVlcnlTdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcHJvZHVjdFN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9jb21wYXJpc29uU3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3NlbmRRdWVyeU1peGluJyksXHJcblx0XSxcclxuXHRcclxuICAgIHByb3BUeXBlczoge1xyXG5cclxuICAgIH0sXHJcblxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHRcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfSxcclxuICAgIFxyXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XHJcblx0XHQvL2xldCB7IHF1ZXJ5U3RhdGUsIHJlc3VsdFN0YXRlIH0gPSB0aGlzLnN0YXRlO1xyXG5cdFx0XHJcblx0XHQvL2lmIChyZXN1bHRTdGF0ZS5wcm9kdWN0RGF0YS5sZW5ndGggPT09IDApIHtcclxuXHRcdC8vXHRsZXQge21haW5tZW51LCBzdWJtZW51fSA9IHF1ZXJ5U3RhdGU7XHJcblx0XHQvL1x0bGV0IHF1ZXJ5ID0gdGhpcy5nZXRBY2NlcHRhYmxlUXVlcnkocXVlcnlTdGF0ZSk7XHJcblx0XHQvL1x0Q29tbW9uQWN0aW9ucy5mZXRjaFJlc3VsdHMobWFpbm1lbnUsIHN1Ym1lbnUsIHF1ZXJ5KTtcclxuXHRcdC8vfVxyXG4gICAgfSxcclxuXHJcbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuXHRyZW5kZXIoKSB7XHJcblx0XHRsZXQge3F1ZXJ5U3RhdGUsIHJlc3VsdFN0YXRlfSA9IHRoaXMuc3RhdGU7XHJcblx0XHRsZXQgb3B0Q29kZU1hcCA9IHF1ZXJ5U3RhdGUub3B0Q29kZU1hcDtcclxuXHRcdFxyXG5cdFx0bGV0IHtmaWx0ZXJPcHRpb25zLCBzb3J0T3B0aW9ucywgdG9vbHRpcEVsZW0sIHJlc3VsdHNFbGVtLCBoZWxwZXJFbGVtfSA9IHRoaXMuX2dldE9wdGlvbnMocXVlcnlTdGF0ZS5zdWJtZW51LCBvcHRDb2RlTWFwLCByZXN1bHRTdGF0ZSk7XHJcblxyXG5cclxuXHRcdGlmKHJlc3VsdFN0YXRlLnJlY2VpdmVkT25jZSkge1xyXG5cdFx0XHRpZihyZXN1bHRTdGF0ZS5wcm9kdWN0RGF0YS5sZW5ndGggPiAwKSB7XHJcblx0XHRcdFx0bGV0IHVuaXQgPSAocXVlcnlTdGF0ZS5zZWFyY2hRdWVyeS50b0tSVyA9PT0gJ1knKSA/ICfsm5AnIDogKHJlc3VsdFN0YXRlLnByb2R1Y3REYXRhWzBdLnVuaXQgfHwgJy0nKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRsZXQgdGFibGVIZWFkZXJFbGVtID0gKFxyXG5cdFx0XHRcdFx0PHRyPlxyXG5cdFx0XHRcdFx0XHQ8dGg+7J2A7ZaJ66qFPC90aD5cclxuXHRcdFx0XHRcdFx0PHRoPntg7ZmY7KCE6riI7JWhKCR7dW5pdH0pYH08L3RoPlxyXG5cdFx0XHRcdFx0XHQ8dGg+e2DsoIjslb3quIjslaEoJHt1bml0fSlgfTwvdGg+XHJcblx0XHRcdFx0XHRcdDx0aD7rp6Trp6TquLDspIDsnKgoJSk8L3RoPlxyXG5cdFx0XHRcdFx0XHQ8dGg+6rOg7KeA6riw7KSA7J287J6QPC90aD5cclxuXHRcdFx0XHRcdDwvdHI+XHJcblx0XHRcdFx0KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFx0PHNlY3Rpb24gY2xhc3NOYW1lPSdkb3duX3Jlc3VsdCc+XHJcblxyXG5cdFx0XHRcdFx0XHQ8U2VsZWN0TGlzdFxyXG5cdFx0XHRcdFx0XHRcdGNsYXNzTmFtZT0ndG9wX3NvcnQnXHJcblx0XHRcdFx0XHRcdFx0b3B0aW9ucz17c29ydE9wdGlvbnN9XHJcblx0XHRcdFx0XHRcdFx0dmFsdWU9eyBxdWVyeVN0YXRlLnNvcnRpbmcgfHwgKHNvcnRPcHRpb25zICYmIHNvcnRPcHRpb25zWzBdICYmIHNvcnRPcHRpb25zWzBdLmRhdGFBdHRycy52YWx1ZSkgfHwgJyd9XHJcblx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXsoKT0+e319PlxyXG5cdFx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRcdHt0b29sdGlwRWxlbX1cclxuXHRcdFx0XHRcdFx0PC9TZWxlY3RMaXN0PlxyXG5cclxuXHRcdFx0XHRcdFx0PHNlY3Rpb24gY2xhc3NOYW1lPSdyZXN1bHRzJz5cclxuXHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNhcl9saXN0XCI+XHJcblx0XHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHRcdDx0YWJsZSBzdW1tYXJ5PVwi7J2A7ZiV67OEIO2ZmOycqCDsoJXrs7RcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PGNhcHRpb24+7J2A7ZiV67OEIO2ZmOycqCDsoJXrs7Q8L2NhcHRpb24+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxjb2xncm91cD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8Y29sIHdpZHRoPVwiMTkwcHhcIiAvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxjb2wgd2lkdGg9XCIxNDBweFwiIC8+XHJcblx0XHRcdFx0XHRcdFx0XHRcdFx0PGNvbCB3aWR0aD1cIjE0MHB4XCIgLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8Y29sIHdpZHRoPVwiMTQwcHhcIiAvPlxyXG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxjb2wgd2lkdGg9XCIqXCIgLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9jb2xncm91cD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRoZWFkPnt0YWJsZUhlYWRlckVsZW19PC90aGVhZD5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHRib2R5PntyZXN1bHRzRWxlbX08L3Rib2R5PlxyXG5cdFx0XHRcdFx0XHRcdFx0PC90YWJsZT5cclxuXHJcblx0XHRcdFx0XHRcdFx0XHR7IHJlc3VsdFN0YXRlLmlzRmV0Y2hpbmcgPyAoXHJcblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPXtyZXN1bHRTdGF0ZS5pc0ZldGNoaW5nID8gJ3RleHQtY2VudGVyJyA6ICdoaWRlJ30gc3R5bGU9e3twYWRkaW5nOiAnMjVweCAwJ319PlxyXG5cdFx0XHRcdFx0XHRcdFx0XHQgICAgPFNwaW5uZXIgd2lkdGg9JzQwcHgnIGhlaWdodD0nNDBweCcgLz5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHRcdFx0XHQgKSA6IHVuZGVmaW5lZCB9XHJcblx0XHRcdFx0XHRcdFx0XHRcclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHJcblx0XHRcdFx0XHRcdDwvc2VjdGlvbj5cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHQ8L3NlY3Rpb24+XHJcblx0XHRcdFx0KTtcclxuXHRcdFx0XHRcclxuXHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9J25vLXJlc3VsdC1tc2cnPlxyXG5cdFx0XHRcdFx0XHQ8cCBjbGFzc05hbWU9J3RleHQtY2VudGVyJz7qsoDsg4nqsrDqs7zqsIAg7JeG7Iq164uI64ukLjwvcD5cclxuXHRcdFx0XHRcdDwvZGl2PlxyXG5cdFx0XHRcdCk7XHJcblxyXG5cdFx0XHR9XHJcblx0XHR9IGVsc2Uge1xyXG5cclxuXHRcdFx0bGV0IGZldGNoaW5nTXNnRWxlbSwgbWFza0VsZW07XHJcblx0XHRcdFxyXG5cdFx0XHRpZihyZXN1bHRTdGF0ZS5pc0ZldGNoaW5nKSB7XHJcblx0XHRcdFx0bWFza0VsZW0gPSA8ZGl2IGNsYXNzTmFtZT1cIm1hc2tcIiAvPjtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRmZXRjaGluZ01zZ0VsZW0gPSAoXHJcblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImZldGNoaW5nLW1vZGFsXCI+XHJcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiZmV0Y2hpbmctZGlhbG9nXCI+XHJcblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJmZXRjaGluZy1tc2cgdGV4dC1jZW50ZXJcIj5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXY+XHJcblx0XHRcdFx0XHRcdFx0XHRcdDxTcGlubmVyIHdpZHRoPSc4MHB4JyBoZWlnaHQ9JzgwcHgnIC8+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibWVzc2FnZS1ib3hcIj5cclxuXHRcdFx0XHRcdFx0XHRcdFx0PHNwYW4+642w7J207YSw66W8IOu2iOufrOyYpOuKlCDspJHsnoXri4jri6QuPGJyIC8+7J6g7Iuc66eMIOq4sOuLpOugpOyjvOyEuOyalC48L3NwYW4+XHJcblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHRcdFx0PC9kaXY+XHJcblx0XHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0XHQpO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicHJlLXNlYXJjaCB0ZXh0LWNlbnRlclwiPlxyXG5cdFx0XHRcdFx0PHA+64KY7JeQ6rKMIOunnuuKlCDsg4HtkojsnYQg7LC+7JWEIOuztOyEuOyalC48L3A+XHJcblx0XHRcdFx0XHR7IG1hc2tFbGVtIH1cclxuXHRcdFx0XHRcdHsgZmV0Y2hpbmdNc2dFbGVtIH1cclxuXHRcdFx0XHQ8L2Rpdj5cclxuXHRcdFx0KTtcclxuXHJcblx0XHR9XHJcblxyXG5cclxuXHR9LFxyXG5cclxuXHRfZ2V0TGlzdE9wdGlvbnMob3B0Q29kZU1hcCwgY29kZSkge1xyXG5cdFx0aWYob3B0Q29kZU1hcFtjb2RlXSkge1xyXG5cdFx0XHRyZXR1cm4gKCBvcHRDb2RlTWFwW2NvZGVdLm1hcCgoZSkgPT4gbmV3IExpc3RPcHRzKGUuY2ROYW1lLCBlLmNkKSkgKTtcclxuXHRcdH1cclxuXHRcdHJldHVybiBbXTtcclxuXHR9LFxyXG5cclxuXHJcblx0X2dldE9wdGlvbnMoc3VibWVudSwgb3B0Q29kZU1hcCkge1xyXG5cdFx0Ly8gcmV0dXJucyB7ZmlsdGVyT3B0aW9ucywgc29ydE9wdGlvbnMsIHRvb2x0aXBFbGVtLCByZXN1bHRzRWxlbSwgaGVscGVyRWxlbX1cclxuXHRcdC8vXHJcblx0XHQvLyAgICB0b29sdGlwRWxlbSAtPiB0b29sdGlwIGVsZW1lbnQgdG8gZ28gaW4gc29ydGluZyBzZWN0aW9uXHJcblx0XHQvLyAgICByZXN1bHRzRWxlbSAtPiBsaXN0IG9mIGZpbGxlZCBpbiByZXN1bHRzIGVsZW1lbnRzIChQcm9kdWN0Q2FyZCdzKVxyXG5cdFx0Ly8gICAgaGVscGVyRWxlbSAgLT4gKGlmIG5lZWRlZCkgbW9kYWwgdG8gdXNlIG9yIGFueSBvdGhlciBlbGVtZW50XHJcblx0XHRcclxuXHRcdGxldCB7IHJlc3VsdFN0YXRlLCBjb21wYXJpc29uU3RhdGUgfSA9IHRoaXMuc3RhdGU7XHJcblx0XHRcclxuXHRcdGxldCB7IGlzQ21wclRhYkFjdGl2ZSwgaXNMb2dnZWRJbiwgY29tcGFyaXNvbkdyb3VwLFx0Y3VycmVudENtcHIgfSA9IGNvbXBhcmlzb25TdGF0ZTtcclxuXHRcdGxldCBzaG93aW5nQ21wciA9IChpc0xvZ2dlZEluICYmIGlzQ21wclRhYkFjdGl2ZSAmJiBjb21wYXJpc29uR3JvdXAubGVuZ3RoID4gMCk7XHJcblx0XHRsZXQgc2VsZWN0ZWRDbXByID0gY29tcGFyaXNvbkdyb3VwW2N1cnJlbnRDbXByXTtcclxuXHRcdFxyXG5cdFx0bGV0IGZpbHRlck9wdGlvbnMsIHNvcnRPcHRpb25zLCB0b29sdGlwRWxlbSwgcmVzdWx0c0VsZW0sIGhlbHBlckVsZW07XHJcblx0XHRsZXQgY29tcGFyaXNvbkNvbnRlbnRzID0gdW5kZWZpbmVkO1xyXG5cclxuXHRcdFxyXG5cdFx0c29ydE9wdGlvbnMgPSB0aGlzLl9nZXRMaXN0T3B0aW9ucyhvcHRDb2RlTWFwLCA2MTAwKTtcclxuXHRcdFxyXG5cdFx0Ly90b29sdGlwRWxlbSA9IChcclxuXHRcdC8vXHQ8c3BhbiBjbGFzc05hbWU9XCJiYWRnZVwiIGRhdGEtdG9nZ2xlPVwidG9vbHRpcFwiIGRhdGEtcGxhY2VtZW50PVwicmlnaHRcIiB0aXRsZT1cIuuztO2XmOuCmOydtCwg7ISx67OEIOuTseydhCDqs6DroKTtlZjsl6wg6rCA7J6F7ZWY7IugIOyDge2SiOuztOuLpCDrs7Ttl5jro4zqsIAg7KCA66C07ZWcLCDsp4DquInspIDruYTsnKjsnbQg64aS7J2ALCDrtoDsp4DquInrpaDsnbQg64Ku7J2AIOyDge2SiOydhCDstpTsspztlbTrk5zrpr3ri4jri6QuXCI+Pzwvc3Bhbj5cclxuXHRcdC8vKTtcclxuXHJcblxyXG5cdFx0cmVzdWx0c0VsZW0gPSByZXN1bHRTdGF0ZS5wcm9kdWN0RGF0YS5tYXAoIChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0bGV0IHtcclxuXHRcdFx0XHRiaSwgLy/snYDtlolCSVxyXG5cdFx0XHRcdGV4Y2hhbmdlX21vbmV5LCAvL+2ZmOyghOq4iOyVoVxyXG5cdFx0XHRcdHNhdmVkX21vbmV5LCAvL+ygiOyVveq4iOyVoVxyXG5cdFx0XHRcdHRyX3JhdGUsIC8v66ek66ek6riw7KSA7JyoXHJcblx0XHRcdFx0dHJfZGF0ZSwgLy/qs6Dsi5zquLDspIDsnbzsnpBcclxuXHRcdFx0XHR0cl91cmxcclxuXHRcdFx0fSA9IGVsZW07XHJcblxyXG5cdFx0XHRcclxuXHRcdFx0cmV0dXJuIChcclxuXHRcdFx0XHQ8dHIga2V5PXtgZXhjaCR7Yml9JHtleGNoYW5nZV9tb25leX0ke3RyX3JhdGV9YH0+XHJcblx0XHRcdFx0XHQ8dGQ+PGEgaHJlZj17dHJfdXJsfSB0YXJnZXQ9XCJfYmxhbmtcIj48aSBjbGFzc05hbWU9e2BiaSBiaS0ke2JpfWB9PjwvaT48L2E+PC90ZD5cclxuXHRcdFx0XHRcdDx0ZD57ZXhjaGFuZ2VfbW9uZXl9PC90ZD5cclxuXHRcdFx0XHRcdDx0ZD57c2F2ZWRfbW9uZXl9PC90ZD5cclxuXHRcdFx0XHRcdDx0ZD57dHJfcmF0ZX08L3RkPlxyXG5cdFx0XHRcdFx0PHRkPnt0cl9kYXRlfTwvdGQ+XHJcblx0XHRcdFx0PC90cj5cclxuXHRcdFx0KTtcclxuXHRcdH0pO1xyXG5cclxuXHJcblxyXG5cdFx0cmV0dXJuIHtmaWx0ZXJPcHRpb25zLCBzb3J0T3B0aW9ucywgdG9vbHRpcEVsZW0sIHJlc3VsdHNFbGVtLCBoZWxwZXJFbGVtfTtcclxuXHR9LFxyXG5cclxuXHRnZXRQcmVTZWFyY2hWaWV3KHN1Ym1lbnUpIHtcclxuXHRcdGxldCBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHRcdFxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRjYXNlKCdzaGlsc29uJyk6XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2NhcicpOlxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFxyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHJcblx0fVxyXG5cclxuXHJcbiAgICBcclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IEluc3VyZVJlc3VsdFNlY3Rpb247XHJcbiIsImxldCBSZWFjdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcblx0Q29tbW9uQWN0aW9uc1x0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vYWN0aW9ucy9Db21tb25BY3Rpb25zJyksXG5cdGZvcm1WYWxpZGF0b3JzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9mb3JtVmFsaWRhdG9ycycpLFxuXHRTZWxlY3REcm9wZG93blx0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29tcG9uZW50cy9TZWxlY3REcm9wZG93bi5qc3gnKSxcblx0VGV4dElucHV0XHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvVGV4dElucHV0LmpzeCcpLFxuXHRRdWVyeVNlY3Rpb25cdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvUXVlcnlTZWN0aW9uLmpzeCcpO1xuXHRcblxuXG5sZXQgeyBzZWxlY3RSZXF1aXJlZCwgbnVtYmVyUmVxdWlyZWQgfSA9IGZvcm1WYWxpZGF0b3JzO1xuXG5sZXQgX3RyYWRlT3B0Q2FjaGUgPSB7fTtcbmxldCBfbmF0aW9uT3B0Q2FjaGUgPSB7fTtcblxuY29uc3QgaW5pdE5hdGlvbk9wdHMgPSBbXG5cdHsgY29kZV9ubTogJ1VTRCjrr7jqta0g64us65+sKScsIGNvZGVfaWQ6ICdVU0QnLCBuYXRpb25sYWJlbDogJ+uvuOq1rScgfSxcblx0eyBjb2RlX25tOiAnSlBZKOydvOuzuCDsl5QpJywgY29kZV9pZDogJ0pQWScsIG5hdGlvbmxhYmVsOiAn7J2867O4JyB9XG5dOyAvLyBpbml0aWFsIG9wdGlvbnMgaWYgbm90IGZldGNoZWQgeWV0XG5cblxuY2xhc3MgU2VsZWN0T3B0cyB7XG5cdGNvbnN0cnVjdG9yKGxhYmVsLCB2YWx1ZSkge1xuXHRcdHRoaXMubGFiZWwgPSBsYWJlbDtcblx0XHR0aGlzLnZhbHVlID0gdmFsdWU7XG5cdH1cbn1cblxuXG5mdW5jdGlvbiBzZXREZWZhdWx0SWZBdmFpbGFibGUoc2VhcmNoUXVlcnksIG9wdENvZGVNYXApIHtcblx0bGV0IHRyYWRlT3B0cyA9IG9wdENvZGVNYXBbJ0VYMDAzJ107XG5cdGxldCBuYXRpb25PcHRzID0gb3B0Q29kZU1hcFsnbmF0aW9uT3B0cyddO1xuXG5cdC8vIHVwZGF0ZSBuYXRpb24gc3RhdGUgd2hlbiBmZXRjaGVkXG5cdGlmKCAhc2VhcmNoUXVlcnkudHJfbmF0aW9uICYmIG5hdGlvbk9wdHMgJiYgbmF0aW9uT3B0cy5sZW5ndGggPiAwICkge1xuXHRcdGxldCBmaXJzdE9wdCA9IG5hdGlvbk9wdHNbMF07XG5cdFx0XG5cdFx0aWYoZmlyc3RPcHQpIHtcblx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdFx0XHRDb21tb25BY3Rpb25zLmFkZE9wdGlvbnMoeyBuYXRpb25PcHRzIH0pO1xuXHRcdFx0XHRDb21tb25BY3Rpb25zLmNoYW5nZVNlYXJjaFF1ZXJ5KCd0cl9uYXRpb24nLCBmaXJzdE9wdC5jb2RlX2lkKTtcblx0XHRcdH0sIDUwKTtcblx0XHR9XG5cdH1cblx0XG5cdC8vIHVwZGF0ZSB0cmFkZSBvcHQgc3RhdGUgd2hlbiBmZXRjaGVkXG5cdGlmKCAhc2VhcmNoUXVlcnkuZXhjaGFuZ2VfdHlwZSAmJiB0cmFkZU9wdHMgJiYgdHJhZGVPcHRzLmxlbmd0aCA+IDAgKSB7XG5cdFx0bGV0IGZpcnN0T3B0ID0gdHJhZGVPcHRzWzBdO1xuXHRcdGlmKGZpcnN0T3B0KSB7XG5cdFx0XHRzZXRUaW1lb3V0KCgpID0+IHtcblx0XHRcdFx0Q29tbW9uQWN0aW9ucy5jaGFuZ2VTZWFyY2hRdWVyeSgnZXhjaGFuZ2VfdHlwZScsIGZpcnN0T3B0LmNkKTtcblx0XHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdFx0dG9LUlc6IGZpcnN0T3B0LmNkRGVzYyA9PT0gJ1knXG5cdFx0XHRcdH0pO1xuXHRcdFx0fSwgNTApO1xuXHRcdH1cblx0fVxuXG59XG5cblxuXG5sZXQgRXhjaEhlbHBlclNlY3Rpb24gPSBSZWFjdC5jcmVhdGVDbGFzcyh7XG5cdG1peGluczogW1xuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9xdWVyeVN0b3JlTWl4aW4nKSxcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcHJvZHVjdFN0b3JlTWl4aW4nKSxcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvc2VuZFF1ZXJ5TWl4aW4nKSxcblx0XSxcblxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcbiAgICAgICAgcmV0dXJuIHtcblx0XHRcdFxuICAgICAgICB9O1xuICAgIH0sXG5cbiAgICBnZXRJbml0aWFsU3RhdGUoKSB7XG4gICAgICAgIHJldHVybiB7XG5cdFx0XHRjdXJyZW50TmF0aW9uSWR4OiAwLFxuXHRcdFx0dG9LUlc6IGZhbHNlXG4gICAgICAgIH07XG4gICAgfSxcbiAgICBcbiAgICBjb21wb25lbnRXaWxsTW91bnQoKSB7XG5cbiAgICB9LFxuXG5cdGNvbXBvbmVudERpZE1vdW50KCkge1xuXHRcdC8qIHNldCBkZWZhdWx0IG9wdGlvbnMgKi9cblx0XHRsZXQgeyBzZWFyY2hRdWVyeSwgb3B0Q29kZU1hcCB9ID0gdGhpcy5zdGF0ZS5xdWVyeVN0YXRlO1xuXG5cdFx0Ly9jYWNoZSBpbml0aWFsIHRyYWRlIG9wdCBzdGF0ZVxuXHRcdF90cmFkZU9wdENhY2hlID0gb3B0Q29kZU1hcFsnRVgwMDMnXTtcblx0XHRfbmF0aW9uT3B0Q2FjaGUgPSBvcHRDb2RlTWFwWyduYXRpb25PcHRzJ107XG5cblx0XHRpZihzZWFyY2hRdWVyeS50cl9uYXRpb24pIHtcblx0XHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0XHRjdXJyZW50TmF0aW9uSWR4OiBvcHRDb2RlTWFwWyduYXRpb25PcHRzJ10uZmluZEluZGV4KChlKSA9PiBlLmNvZGVfaWQgPT09IHNlYXJjaFF1ZXJ5LnRyX25hdGlvbilcblx0XHRcdH0pO1xuXHRcdH1cblx0XHRcblx0XHRzZXREZWZhdWx0SWZBdmFpbGFibGUuYmluZCh0aGlzKShzZWFyY2hRdWVyeSwgb3B0Q29kZU1hcCk7XG5cdH0sXG5cbiAgICBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcblx0XHRcbiAgICB9LFxuXG5cdGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xuXHRcdC8vIHNldCBkZWZhdWx0IHRyYWRlIG9wdGlvbiB3aGVuIG9wdENvZGUgZmV0Y2hlZFxuXHRcdGxldCB7IHNlYXJjaFF1ZXJ5LCBvcHRDb2RlTWFwIH0gPSB0aGlzLnN0YXRlLnF1ZXJ5U3RhdGU7XG5cdFx0bGV0IHRyYWRlT3B0cyA9IG9wdENvZGVNYXBbJ0VYMDAzJ107XG5cdFx0bGV0IG5hdGlvbk9wdHMgPSBvcHRDb2RlTWFwWyduYXRpb25PcHRzJ107XG5cdFx0XG5cdFx0aWYoX3RyYWRlT3B0Q2FjaGUgIT09IHRyYWRlT3B0cyB8fCBfbmF0aW9uT3B0Q2FjaGUgIT09IG5hdGlvbk9wdHMpIHtcblx0XHRcdF90cmFkZU9wdENhY2hlID0gdHJhZGVPcHRzO1xuXHRcdFx0X25hdGlvbk9wdENhY2hlID0gbmF0aW9uT3B0cztcblxuXHRcdFx0c2V0RGVmYXVsdElmQXZhaWxhYmxlLmJpbmQodGhpcykoc2VhcmNoUXVlcnksIG9wdENvZGVNYXApO1xuXHRcdH1cblx0fSxcblxuXHRyZW5kZXIoKSB7XG5cdFx0bGV0IHsgcXVlcnlTdGF0ZSwgcmVzdWx0U3RhdGUsIGN1cnJlbnROYXRpb25JZHgsIHRvS1JXIH0gPSB0aGlzLnN0YXRlO1xuXHRcdGxldCB7IG9wdENvZGVNYXAsIHN1Ym1lbnUsIHNlYXJjaFF1ZXJ5IH0gPSBxdWVyeVN0YXRlO1xuXG5cdFx0bGV0IG5hdGlvbk9wdHMgPSBvcHRDb2RlTWFwLm5hdGlvbk9wdHMgfHwgaW5pdE5hdGlvbk9wdHM7XG5cdFx0bmF0aW9uT3B0cyA9IG5hdGlvbk9wdHMubGVuZ3RoID4gMCA/IG5hdGlvbk9wdHMgOiBpbml0TmF0aW9uT3B0cztcblxuXHRcdGxldCB0cmFkZVR5cGVPcHRzID0gdGhpcy5nZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsICdFWDAwMycpO1xuXG5cdFx0bGV0IG5hdGlvbkZvcm0gPSAoXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy00XCI+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPu2ZmOyghO2VoCDqta3qsIA8L2xhYmVsPlxuXHRcdFx0XHRcdDxTZWxlY3REcm9wZG93blxuXHRcdFx0XHRcdFx0dGFiSW5kZXg9ezB9XG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0n7ISg7YOdJ1xuXHRcdFx0XHRcdFx0b3B0aW9ucz17bmF0aW9uT3B0cy5tYXAoKGUpID0+IG5ldyBTZWxlY3RPcHRzKGUuY29kZV9ubSwgZS5jb2RlX2lkKSApfVxuXHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlbGVjdENoYW5nZS5iaW5kKG51bGwsICd0cl9uYXRpb24nLCBbJ21vbmV5JywgJ2NvdXBvbiddKX1cblx0XHRcdFx0XHRcdGNsYXNzTmFtZT0nc2VsZWN0LWRyb3Bkb3duJ1xuXHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e3F1ZXJ5U3RhdGUuc2VhcmNoUXVlcnlbJ3RyX25hdGlvbiddfVxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtxdWVyeVN0YXRlLnNob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxuXHRcdFx0XHRcdC8+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcblx0XHRcblxuXHRcdGxldCByYXRlVGFibGU7XG5cdFx0aWYobmF0aW9uT3B0c1tjdXJyZW50TmF0aW9uSWR4XS50cl9yYXRlKSB7XG5cdFx0XHRsZXQge1xuXHRcdFx0XHR0cl9kYXRlLCAvL+q4sOykgOydvFxuXHRcdFx0XHR0cl9yYXRlLCAvL+unpOunpOq4sOykgOycqFxuXHRcdFx0XHR0cl9idXlfY2FzaCwgLy/tmITssLAg7IK065WMXG5cdFx0XHRcdHRyX3NlbGxfY2FzaCwgLy/tmITssLAg7YyU65WMXG5cdFx0XHRcdHRyX3NlbmRfbW9uZXksIC8v7Iah6riIIOuztOuCvOuVjFxuXHRcdFx0XHR0cl9yZWNlaXZlX21vbmV5IC8v7Iah6riIIOuwm+ydhOuVjFxuXHRcdFx0fSA9IG5hdGlvbk9wdHNbY3VycmVudE5hdGlvbklkeF07XG5cdFx0XHRcblx0XHRcdHJhdGVUYWJsZSA9IChcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJyb3dcIiBzdHlsZT17e21hcmdpblRvcDogJy0xOHB4J319PlxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPSdub3RpY2UtaW5mbyc+PHNwYW4+6riw7KSA7J28OiB7dHJfZGF0ZX08L3NwYW4+PHNwYW4gY2xhc3NOYW1lPVwiZGV2aWRlXCI+fDwvc3Bhbj48c3Bhbj7soJzqs7U6IEtFQu2VmOuCmOydgO2WiTwvc3Bhbj48L2Rpdj5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0xMlwiPlxuXHRcdFx0XHRcdFx0PHVsIGNsYXNzTmFtZT1cImN1cnJlbmN5LWJveFwiPlxuXHRcdFx0XHRcdFx0XHQ8bGkgY2xhc3NOYW1lPVwiZmlyc3RcIj48c3Bhbj7rp6Trp6TquLDspIDsnKg8L3NwYW4+PHNwYW4gY2xhc3NOYW1lPVwidmFsXCI+e3RyX3JhdGUgfHwgJy0nfTwvc3Bhbj48L2xpPlxuXHRcdFx0XHRcdFx0XHQ8bGk+PHNwYW4+7ZiE7LCwKOyCtCDrlYwpPC9zcGFuPjxzcGFuIGNsYXNzTmFtZT1cInZhbFwiPnt0cl9idXlfY2FzaCB8fCAnLSd9PC9zcGFuPjwvbGk+XG5cdFx0XHRcdFx0XHRcdDxsaT48c3Bhbj7tmITssLAo7YyUIOuVjCk8L3NwYW4+PHNwYW4gY2xhc3NOYW1lPVwidmFsXCI+e3RyX3NlbGxfY2FzaCB8fCAnLSd9PC9zcGFuPjwvbGk+XG5cdFx0XHRcdFx0XHRcdDxsaT48c3Bhbj7shqHquIgo67O064K8IOuVjCk8L3NwYW4+PHNwYW4gY2xhc3NOYW1lPVwidmFsXCI+e3RyX3NlbmRfbW9uZXkgfHwgJy0nfTwvc3Bhbj48L2xpPlxuXHRcdFx0XHRcdFx0XHQ8bGk+PHNwYW4+7Iah6riIKOuwm+ydhCDrlYwpPC9zcGFuPjxzcGFuIGNsYXNzTmFtZT1cInZhbFwiPnt0cl9yZWNlaXZlX21vbmV5IHx8ICctJ308L3NwYW4+PC9saT5cblx0XHRcdFx0XHRcdDwvdWw+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0KTtcblx0XHR9XG5cblxuXHRcdGxldCB0cmFkZVR5cGVGb3JtID0gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNFwiPlxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9XCJzZWxlY3QtbGFiZWxcIj7qsbDrnpjsooXrpZg8L2xhYmVsPlxuXHRcdFx0XHRcdDxTZWxlY3REcm9wZG93blxuXHRcdFx0XHRcdFx0dGFiSW5kZXg9ezB9XG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0n7ISg7YOdJ1xuXHRcdFx0XHRcdFx0b3B0aW9ucz17dHJhZGVUeXBlT3B0c31cblx0XHRcdFx0XHRcdGhhbmRsZVNlbGVjdD17dGhpcy5oYW5kbGVTZWxlY3RDaGFuZ2UuYmluZChudWxsLCAnZXhjaGFuZ2VfdHlwZScsIFsnY291cG9uJ10pfVxuXHRcdFx0XHRcdFx0Y2xhc3NOYW1lPSdzZWxlY3QtZHJvcGRvd24nXG5cdFx0XHRcdFx0XHRzZWxlY3RlZD17cXVlcnlTdGF0ZS5zZWFyY2hRdWVyeVsnZXhjaGFuZ2VfdHlwZSddfVxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtxdWVyeVN0YXRlLnNob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxuXHRcdFx0XHRcdC8+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcblxuXHRcdFxuXG5cdFx0bGV0IGJhc2VOYXRpb25MYWJlbCwgYmFzZU1zZywgcXVvdGVNc2c7XG5cblx0XHRpZih0b0tSVyA9PT0gJ1knKSB7XG5cdFx0XHRiYXNlTmF0aW9uTGFiZWwgPSBuYXRpb25PcHRzW2N1cnJlbnROYXRpb25JZHhdLm5hdGlvbmxhYmVsO1xuXHRcdFx0YmFzZU1zZyA9IGAke25hdGlvbk9wdHNbY3VycmVudE5hdGlvbklkeF0uY29kZV9ubX3rpbxgO1xuXHRcdFx0cXVvdGVNc2cgPSAnS1JXKOuMgO2VnOuvvOq1rSDsm5Ap7Jy866GcIO2ZmOyghO2VqeuLiOuLpC4nO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRiYXNlTmF0aW9uTGFiZWwgPSAn64yA7ZWc66+86rWtJztcblx0XHRcdGJhc2VNc2cgPSAnS1JXKOuMgO2VnOuvvOq1rSDsm5Ap66W8Jztcblx0XHRcdHF1b3RlTXNnID0gYCR7bmF0aW9uT3B0c1tjdXJyZW50TmF0aW9uSWR4XS5jb2RlX25tfeycvOuhnCDtmZjsoITtlanri4jri6QuYDtcblx0XHR9XG5cdFx0XG5cdFx0bGV0IGFtb3VudEZvcm0gPSAoXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy00XCI+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPntiYXNlTmF0aW9uTGFiZWx9PC9sYWJlbD5cblx0XHRcdFx0XHQ8VGV4dElucHV0XG5cdFx0XHRcdFx0XHR0eXBlPSdudW1iZXItd2l0aC1jb21tYXMnXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0n6riI7JWh7J6F66ClJ1xuXHRcdFx0XHRcdFx0aW5wdXRDbGFzcz0ndGV4dC1pbnB1dC1ub3JtYWwnXG5cdFx0XHRcdFx0XHRoYW5kbGVDaGFuZ2U9e3RoaXMuaGFuZGxlVGV4dElucHV0Q2hhbmdlLmJpbmQobnVsbCwgJ21vbmV5Jyl9XG5cdFx0XHRcdFx0XHR2YWx1ZT17cXVlcnlTdGF0ZS5zZWFyY2hRdWVyeVsnbW9uZXknXX1cblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbj17cXVlcnlTdGF0ZS5zaG93VmFsaWRhdGlvbiAmJiBudW1iZXJSZXF1aXJlZH1cblx0XHRcdFx0XHQvPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjdXJyZW5jeS1yaWdodFwiPlxuXHRcdFx0XHRcdDx1bD5cblx0XHRcdFx0XHRcdDxsaT48c3Bhbj57YmFzZU1zZ308L3NwYW4+PC9saT5cblx0XHRcdFx0XHRcdDxsaT48c3BhbiBjbGFzc05hbWU9XCJxdW90ZVwiPntxdW90ZU1zZ308L3NwYW4+PC9saT5cblx0XHRcdFx0XHQ8L3VsPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdCk7XG5cblx0XHRsZXQgY291cG9uRWxlbTtcblx0XHRpZihxdWVyeVN0YXRlLnNlYXJjaFF1ZXJ5LmV4Y2hhbmdlX3R5cGUgPT09ICdUUl9CVVlfQ0FTSCcpIHtcblx0XHRcdGNvdXBvbkVsZW0gPSAoXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNFwiPlxuXHRcdFx0XHRcdFx0PGxhYmVsIGh0bWxGb3I9XCJzZWxlY3RcIiBjbGFzc05hbWU9XCJzZWxlY3QtbGFiZWwgY291cG9uXCI+7Jqw64yA7L+g7Y+wPC9sYWJlbD5cblx0XHRcdFx0XHRcdDxUZXh0SW5wdXRcblx0XHRcdFx0XHRcdFx0dHlwZT0nbnVtYmVyLXdpdGgtY29tbWFzJ1xuXHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0n7L+g7Y+w7ZWg7J247JyoJ1xuXHRcdFx0XHRcdFx0XHRpbnB1dENsYXNzPSd0ZXh0LWlucHV0LW5vcm1hbCdcblx0XHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZVRleHRJbnB1dENoYW5nZS5iaW5kKG51bGwsICdjb3Vwb24nKX1cblx0XHRcdFx0XHRcdFx0dmFsdWU9e3F1ZXJ5U3RhdGUuc2VhcmNoUXVlcnlbJ2NvdXBvbiddfVxuXHRcdFx0XHRcdFx0XHRkaXNhYmxlZD17cXVlcnlTdGF0ZS5zZWFyY2hRdWVyeS5leGNoYW5nZV90eXBlICE9PSAnVFJfQlVZX0NBU0gnfVxuXHRcdFx0XHRcdFx0XHR1bml0PSclJ1xuXHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg9ezN9XG5cdFx0XHRcdFx0XHRcdG1heGltdW09ezEwMH1cblx0XHRcdFx0XHRcdC8+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0KTtcblx0XHR9XG5cdFx0XG5cblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJ0b3Bfc2VhcmNoXCI+XG5cblx0XHRcdFx0e25hdGlvbkZvcm19XG5cdFx0XHRcdHtyYXRlVGFibGV9XG5cblx0XHRcdFx0e3RyYWRlVHlwZUZvcm19XG5cdFx0XHRcdFxuXHRcdFx0XHR7YW1vdW50Rm9ybX1cblx0XHRcdFx0XG5cdFx0XHRcdHtjb3Vwb25FbGVtfVxuXG5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9J3JpZ2h0X2J0bic+XG5cdFx0XHRcdFx0PGEgaWQ9XCJzZWFyY2gtYnRuXCIgY2xhc3NOYW1lPSdidG4gYnRuLXN1Ym1pdCcgcm9sZT0nYnV0dG9uJyBvbkNsaWNrPXt0aGlzLmhhbmRsZVNlYXJjaEJ0bkNsaWNrfT7qsoDsg4k8L2E+XG5cdFx0XHRcdDwvZGl2PlxuXG5cdFx0XHQ8L2Rpdj5cblx0XHQpO1xuXG5cbiAgICB9LFxuXG5cdGhhbmRsZVNlbGVjdENoYW5nZShpZCwgZGVwZW5kZW5jaWVzLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblx0XHRcblx0XHRsZXQgbGlFbGVtID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ2xpJyk7XG5cdFx0bGV0IHZhbCA9IGxpRWxlbS5hdHRyKCdkYXRhLXZhbCcpO1xuXHRcdFxuXHRcdHZhbCA9ICh2YWwgPT09IDApID8gMCA6ICh2YWwgfHwgdGFyZ2V0LnRleHRDb250ZW50KTtcblxuXHRcdENvbW1vbkFjdGlvbnMuY2hhbmdlU2VhcmNoUXVlcnkoaWQsIHZhbCk7XG5cblx0XHRpZihkZXBlbmRlbmNpZXMgJiYgZGVwZW5kZW5jaWVzLmxlbmd0aCA+IDApIHtcblx0XHRcdGZvcihsZXQgaSA9IDAsIGwgPSBkZXBlbmRlbmNpZXMubGVuZ3RoOyBpIDwgbDsgaSsrKSB7XG5cdFx0XHRcdGxldCBkZXBlbmRlbmN5ID0gZGVwZW5kZW5jaWVzW2ldO1xuXHRcdFx0XHRDb21tb25BY3Rpb25zLmNoYW5nZVNlYXJjaFF1ZXJ5KGRlcGVuZGVuY3ksICcnKTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRpZihpZCA9PT0gJ3RyX25hdGlvbicpIHtcblx0XHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0XHRjdXJyZW50TmF0aW9uSWR4OiBsaUVsZW0uaW5kZXgoKVxuXHRcdFx0fSk7XG5cdFx0fVxuXG5cdFx0aWYoaWQgPT09ICdleGNoYW5nZV90eXBlJykge1xuXHRcdFx0bGV0IHsgb3B0Q29kZU1hcCB9ID0gdGhpcy5zdGF0ZS5xdWVyeVN0YXRlO1xuXHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdHRvS1JXOiBvcHRDb2RlTWFwWydFWDAwMyddLmZpbmQoKGUpID0+IGUuY2QgPT09IHZhbCkuY2REZXNjXG5cdFx0XHR9KTtcblx0XHR9XG5cblx0fSxcblxuXHRoYW5kbGVUZXh0SW5wdXRDaGFuZ2UoaWQsIGV2dCkge1xuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xuXHRcdFxuXHRcdENvbW1vbkFjdGlvbnMuY2hhbmdlU2VhcmNoUXVlcnkoaWQsIHRhcmdldC52YWx1ZSk7XG5cdH0sXG5cblx0Z2V0U2VsZWN0T3B0aW9ucyhvcHRDb2RlTWFwLCBjb2RlLCBpY29uQ3NzUHJlZml4KSB7XG5cdFx0aWYob3B0Q29kZU1hcFtjb2RlXSkge1xuXHRcdFx0cmV0dXJuICggb3B0Q29kZU1hcFtjb2RlXS5tYXAoKGUpID0+IG5ldyBTZWxlY3RPcHRzKGUuY2ROYW1lLCBlLmNkKSkgKTtcblx0XHR9XG5cdFx0XG5cdFx0Ly8gVE9ETzogd2hlbiBubyBtYXAgZm9yIHRoZSBjb2RlP1xuXHRcdHJldHVybiBbXTtcblx0fSxcblxuXHRoYW5kbGVTZWFyY2hCdG5DbGljaygpIHtcblx0XHRsZXQgeyBxdWVyeVN0YXRlLCByZXN1bHRTdGF0ZSB9ID0gdGhpcy5zdGF0ZTtcblx0XHRsZXQgeyBzZWFyY2hRdWVyeSB9ID0gcXVlcnlTdGF0ZTtcblxuXHRcdGxldCBhbGxWYWxpZCA9IHRoaXMuY2hlY2tWYWxpZGl0eSgpO1xuXG5cdFx0aWYoYWxsVmFsaWQpIHtcblx0XHRcdENvbW1vbkFjdGlvbnMuc2V0UmVjZWl2ZWRPbmNlKGZhbHNlKTtcblx0XHRcdFxuXHRcdFx0Q29tbW9uQWN0aW9ucy5jaGFuZ2VGaWx0ZXIoOTk5OTk5OTkpO1xuXHRcdFx0Q29tbW9uQWN0aW9ucy5jaGFuZ2VTb3J0aW5nKCcnKTtcblxuXHRcdFx0JCgnYm9keScpLmFkZENsYXNzKCdtYXNrZWQnKTsgLy8gZGlzYWJsZSBzY3JvbGxpbmdcblx0XHRcdHRoaXMuc2VuZFF1ZXJ5V2l0aENvdW50KDAsIHF1ZXJ5U3RhdGUsIHJlc3VsdFN0YXRlLCBmYWxzZSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdENvbW1vbkFjdGlvbnMudmFsaWRhdGlvblRvU2hvdyh0cnVlKTtcblx0XHR9XG5cdH0sXG5cblx0Y2hlY2tWYWxpZGl0eSgpIHtcblx0XHRsZXQgYWxsVmFsaWQgPSB0cnVlO1xuXHRcdGxldCB7IHNlYXJjaFF1ZXJ5IH0gPSB0aGlzLnN0YXRlLnF1ZXJ5U3RhdGU7XG5cblx0XHRhbGxWYWxpZCA9IGFsbFZhbGlkICYmIChzZWxlY3RSZXF1aXJlZChzZWFyY2hRdWVyeVsndHJfbmF0aW9uJ10pID09PSB0cnVlKTtcblx0XHRhbGxWYWxpZCA9IGFsbFZhbGlkICYmIChzZWxlY3RSZXF1aXJlZChzZWFyY2hRdWVyeVsnZXhjaGFuZ2VfdHlwZSddKSA9PT0gdHJ1ZSk7XG5cdFx0YWxsVmFsaWQgPSBhbGxWYWxpZCAmJiAobnVtYmVyUmVxdWlyZWQoc2VhcmNoUXVlcnlbJ21vbmV5J10pID09PSB0cnVlKTtcblxuXHRcdHJldHVybiBhbGxWYWxpZDtcblx0fVxuXG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBFeGNoSGVscGVyU2VjdGlvbjtcbiIsImxldCBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XHJcblxyXG5sZXQgcmVmcmVzaEludGVydmFsSWQgPSAnJztcclxuXHJcbmxldCBTcGlubmVyID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xyXG5cdHByb3BUeXBlczoge1xyXG5cdFx0d2lkdGg6IFJlYWN0LlByb3BUeXBlcy5vbmVPZlR5cGUoW1xyXG5cdFx0XHRSZWFjdC5Qcm9wVHlwZXMuc3RyaW5nLFxyXG5cdFx0XHRSZWFjdC5Qcm9wVHlwZXMubnVtYmVyXHJcblx0XHRdKSxcclxuXHRcdGhlaWdodDogUmVhY3QuUHJvcFR5cGVzLm9uZU9mVHlwZShbXHJcblx0XHRcdFJlYWN0LlByb3BUeXBlcy5zdHJpbmcsXHJcblx0XHRcdFJlYWN0LlByb3BUeXBlcy5udW1iZXJcclxuXHRcdF0pXHJcbiAgICB9LFxyXG5cclxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHR3aWR0aDogJzUwcHgnLFxyXG5cdFx0XHRoZWlnaHQ6ICc1MHB4J1xyXG4gICAgICAgIH07XHJcbiAgICB9LFxyXG5cdFxyXG5cdHJlbmRlcigpe1xyXG5cdFx0cmV0dXJuIChcclxuXHRcdFx0PGltZ1xyXG5cdFx0XHRcdHNyYz1cIi9yZXNvdXJjZXMvaW1hZ2VzL2NvbW1vbi9zcGlubmVyLmdpZlwiXHJcblx0XHRcdFx0c3R5bGU9e3t3aWR0aDogdGhpcy5wcm9wcy53aWR0aCwgaGVpZ2h0OiB0aGlzLnByb3BzLmhlaWdodH19XHJcblx0XHRcdC8+XHJcblx0XHQpO1xyXG5cdH1cclxufSk7XHJcblxyXG5tb2R1bGUuZXhwb3J0cyA9IFNwaW5uZXI7XHJcbiJdfQ==
