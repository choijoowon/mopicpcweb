$(function() {
	// 전역 프로퍼티
	var boardProperties = {};
	
	// 공지사항 프로퍼티
	var noticeProperties = {
		bltnType	: "1000001"
		, rowCount	: "10"
		, naviSize	: "10"
		, listUrl	: "/mopic/api/selectBoardList"
		, detailUrl	: "/mopic/api/selectBoardDetail"
		, returnUrl	: "/admin/noticeList"
	}
	
	// faq 프로퍼티
	var faqProperties = {
		bltnType	: "1000002"
		, rowCount	: "10"
		, naviSize	: "10"
		, listUrl	: "/mopic/api/selectBoardList"
		, detailUrl	: "/mopic/api/selectBoardDetail"
		, returnUrl	: "/admin/faqList"
	}
	
	// 1:1문의 프로퍼티
	var inquireProperties = {
		bltnType	: "1000003"
		, rowCount	: "10"
		, naviSize	: "10"
		, listUrl	: "/mopic/api/selectBoardList"
		, detailUrl	: "/mopic/api/selectBoardDetailList"
		, returnUrl	: "/admin/inquireList"
	}
	
	// 전역프로퍼티 세팅
	function setBoardProperty(opt) {
		$.extend(boardProperties, opt[0]);
		
		if(boardProperties["bltnType"] == "1000001")
			$.extend(boardProperties, noticeProperties);
		else if(boardProperties["bltnType"] == "1000002")
			$.extend(boardProperties, faqProperties);
		else if(boardProperties["bltnType"] == "1000003")
			$.extend(boardProperties, inquireProperties);
	}
	
	function completeLocation() {
		location.href=boardProperties["returnUrl"];
	}
	
	// 게시글 상세
	function setBoardDetail(obj, bltnCd) {
		var request = {
			bltnType : boardProperties["bltnType"]
			, bltnCd : bltnCd
		};
		
		$.ajax({
			type : 'POST'
			, url : boardProperties["detailUrl"]
			, contentType : 'application/json;charset=UTF-8'
			, data : JSON.stringify(request)
			, dataType : 'json'
			, success : function(response) {
				var bltnCd	= "";
				var result	= response.result;
				
				if(result.resultCode == "SUCCESS") {
					var iHTML = "";

					iHTML += "		<div class='cont-box' data-id=>";
					
					for(var i = 0; i < result.data.length; i++) {
						var detailObj = result.data[i];
						if(i == 0)
							bltnCd = detailObj.bltnCd;
						iHTML += (i == 0 ? "" : "			<div class='devide-line'></div>");
						iHTML += "			<div class='" + (i == 0 ? "user-attach" : "admin-attach") + "'>";
						
						if(detailObj.fileCnt > 0) {
							iHTML += "				<div class='add-file'>";
							iHTML += "					<span class='txt'>첨부파일 " + detailObj.fileCnt + "개(95KB)</span>";
							iHTML += "				</div>";
							iHTML += "				<ol class='img-list'>";
							
							for(var j = 0; j < detailObj.fileVO.length; j++) {
								var file	= detailObj.fileVO[j];
								
								iHTML += "					<li><a href='/common/download?idx=" + file.fileNo + "' title='" + file.fileNm + "'>" + file.fileNm + "." + file.fileExt + "<span class='size'>40KB</span></a></li>";
							}
							
							iHTML += "				</ol>";
							/*
							iHTML += "				<div class='img-preview'>";
							iHTML += "					<p>이미지 미리보기</p>";
							iHTML += "					<ol class='thumb-img clearfix'>";
							iHTML += "						<li>";
							iHTML += "							<p class='img'>";
							iHTML += "								<img src='/resources/images/admin/dummy/faq_img00.png' alt='샘플 이미지' />";
							iHTML += "							</p>";
							iHTML += "							<span class='name'>01.jpg</span>";
							iHTML += "						</li>";
							iHTML += "					</ol>";
							iHTML += "				</div>";
							*/
							iHTML += "			</div>";
							iHTML += "			<!-- 첨부파일끝 -->";
						}
						
						iHTML += "			<p class='" + (i == 0 ? "question" : "response") + "'><pre>" + detailObj.cts + "</pre></p>";
					}
					
					iHTML += "			</div>";
					iHTML += "			<div class='modify' data-id='" + (result.data.length == 1 ? "write" : "modify") + "'>";
					iHTML += "				<button type='button' data-id='" + bltnCd + "'>" + (result.data.length == 1 ? "답변" : "수정") + "하기</button>";
					iHTML += "			</div>";
					iHTML += "		</div>";
					
					obj.html(iHTML);
					
					$('.ques-cont button').click( function(e) {
						var _txtID	= $(this).attr('data-id');
						var _dataID	= $(this).parent().attr('data-id');
						
						if ( _dataID == 'modify'){
							location.href="/admin/inquireModify?bltnCd=" + _txtID;
						} else {
							location.href="/admin/inquireWrite?upperBltnCd=" + _txtID;
						}
					} );
				}
			}
		});
	}
	
	// 공지사항 리스트 출력
	function drawNoticeList(obj, result) {
		var iHTML			= "";
		var totalCnt		= result.totalCnt;
		var listLen 		= result.data.length;
		var data			= null;
		var detailObj		= null;
		
		if(listLen > 0) {
			for(var i = 0; i < listLen; i++) {
				data = result.data[i];
				
				iHTML +="<tr data-id='" + data.bltnCd + "'>";
				iHTML +="	<td>" + ((totalCnt - data.rnum) + 1) + "</td>";
				iHTML +="	<td class='title'><span class='tit'>" + data.title + "</span></td>";
				iHTML +="	<td>" + data.regDate + "</td>";
				iHTML +="	<td><span class='glyphicon glyphicon-chevron-down' aria-hidden='true'></span></td>";
				iHTML +="</tr>";
				iHTML +="<tr class='notice-cont'>";
				iHTML +="	<td colspan='4' id='detail_" + data.bltnCd + "'>";
				iHTML +="		<div class='cont-box' data-id='" + data.bltnCd + "'>";
				iHTML +="			<div class='notice-box'>";
				iHTML +="				<!--";
				iHTML +="				<span class='img'>이미지</span>";
				iHTML +="				-->";
				iHTML +="				<p>" + data.cts + "</p>";
				iHTML +="			</div>";
				iHTML +="			<div class='apply' id='" + data.bltnCd + "'>";
				iHTML +="				<button type='button'>삭제하기</button><button type='button'>수정하기</button>";
				iHTML +="			</div>";
				iHTML +="		</div>";
				iHTML +="	</td>";
				iHTML +="</tr>";
			}
		} else {
			iHTML +="<tr><td colspan='4' align='center'>조회된 컨텐츠가 없습니다.</td></td>";
		}
		
		obj.append(iHTML);
		
		$('.faq-table tbody tr:not(.notice-cont)').bind("click", function(e) {
			e.preventDefault();
			var _txtID			= $(this).attr('data-id');
			var _currentTarget	= $(e.currentTarget);
			var _currentId		= _currentTarget.next().children().attr("id");

			_currentTarget.toggleClass('select');
			_currentTarget.find('td .glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up')
			_currentTarget.next().toggle();
			
			$('.notice-cont .apply button').bind("click", function(e) {	
				if( $(this).text() == '삭제하기' ) {
					if(window.confirm("삭제하시겠습니까?")) { 
						$.deleteBoardData(boardProperties["bltnType"], _txtID);
					}
				} else {
					location.href="/admin/noticeModify?bltnCd=" + _txtID;
				}
			});
		});
	}
	
	function drawFaqList(obj, result) {
		var iHTML			= "";
		var totalCnt		= result.totalCnt;
		var listLen 		= result.data.length;
		var data			= null;
		var detailObj		= null;
		
		if(listLen > 0) {
			for(var i = 0; i < listLen; i++) {
				data = result.data[i];
				
				iHTML +="<tr data-id='" + data.bltnCd + "'>";
				iHTML +="	<td class='title'><span class='tit'>Q. " + data.title + "</span></td>";
				iHTML +="	<td><span class='glyphicon glyphicon-chevron-down' aria-hidden='true'></span></td>";
				iHTML +="</tr>";
				iHTML +="<tr class='notice-cont'>";
				iHTML +="	<td colspan='2' id='detail_" + data.bltnCd + "'>";
				iHTML +="		<div class='cont-box' data-id='" + data.bltnCd + "'>";
				iHTML +="			<div class='notice-box'>";
				iHTML +="				<!--";
				iHTML +="				<span class='img'>이미지</span>";
				iHTML +="				-->";
				iHTML +="				<p>" + data.cts + "</p>";
				iHTML +="			</div>";
				iHTML +="			<div class='apply' id='" + data.bltnCd + "'>";
				iHTML +="				<button type='button'>삭제하기</button><button type='button'>수정하기</button>";
				iHTML +="			</div>";
				iHTML +="		</div>";
				iHTML +="	</td>";
				iHTML +="</tr>";
			}
		} else {
			iHTML +="<tr><td colspan='6' align='center'>조회된 컨텐츠가 없습니다.</td></td>";
		}
		
		obj.append(iHTML);
		
		$('.faq-table tbody tr:not(.notice-cont)').bind("click", function(e) {
			e.preventDefault();
			var _txtID			= $(this).attr('data-id');
			var _currentTarget	= $(e.currentTarget);
			var _currentId		= _currentTarget.next().children().attr("id");

			_currentTarget.toggleClass('select');
			_currentTarget.find('td .glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up')
			_currentTarget.next().toggle();
			
			$('.notice-cont .apply button').bind("click", function(e) {	
				if( $(this).text() == '삭제하기' ) {
					if(window.confirm("삭제하시겠습니까?")) { 
						$.deleteBoardData(boardProperties["bltnType"], _txtID);
					}
				} else {
					location.href="/admin/faqModify?bltnCd=" + _txtID;
				}
			});
		});
	}
	
	// 1:1문의 리스트 출력
	function drawInquireList(obj, result) {
		var iHTML			= "";
		var totalCnt		= result.totalCnt;
		var listLen 		= result.data.length;
		var data			= null;
		var detailObj		= null;
		
		if(listLen > 0) {
			for(var i = 0; i < listLen; i++) {
				data = result.data[i];
				
				iHTML += "<tr data-id='" + data.bltnCd + "'>";
				iHTML += "	<td class='reply com'>" + (data.ansCnt == 0 ? "답변미완료" : "답변완료") + "</td>";
				iHTML += "	<td>" + data.categoryName + "</td>";
				iHTML += "	<td class='title'><span class='tit'>" + data.title + "</span></td>";
				iHTML += "	<td>" + data.regId + "</td>";
				iHTML += "	<td>" + data.regDate + "</td>";
				iHTML += "	<td><span class='glyphicon glyphicon-chevron-down' aria-hidden='true'></span></td>";
				iHTML += "</tr>";
				iHTML += "<tr class='ques-cont'>";
				iHTML += "	<td colspan='6' id='detail_" + data.bltnCd + "'></td>";
				iHTML += "</tr>";
			}
		} else {
			iHTML +="<tr><td colspan='6' align='center'>조회된 컨텐츠가 없습니다.</td></td>";
		}
		
		obj.append(iHTML);
		
		$('.faq-table tbody tr:not(.ques-cont)').bind("click", function(e) {
			e.preventDefault();
			var _txtID			= $(this).attr('data-id');
			var _currentTarget	= $(e.currentTarget);
			var _currentId		= _currentTarget.next().children().attr("id");
			
			if($("#" + _currentId).html().trim() == "")
				setBoardDetail($("#" + _currentId), _txtID);

			_currentTarget.toggleClass('select');
			_currentTarget.find('td .glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up')
			_currentTarget.next().toggle();
			
			$('.notice-cont .apply button').bind("click", function(e) {	
				if( $(this).text() == '삭제하기' ) {
					if(window.confirm("삭제하시겠습니까?")) { 
						$.deleteBoardData(boardProperties["bltnType"], _txtID);
					}
				} else {
					location.href="/admin/noticeModify?bltnCd=" + _txtID;
				}
			});
		});
	}
	
	function setPageNavigation(naviType, totalCnt) {
		if(naviType == null || naviType == "")
			naviType = 1;
		
		var pageNum			= boardProperties["pageNum"];
		var rowCount		= boardProperties["rowCount"];
		var naviSize		= boardProperties["naviSize"];
		var totalPage		= totalCnt % rowCount > 0 ? Math.floor(totalCnt / rowCount) + 1 : totalCnt / rowCount;
		var pageGroup		= Math.floor(pageNum / naviSize) + 1;
		var pageGroupStart	= pageGroup == 1 ? 1 : ((pageGroup - 1) * naviSize) + 1;
		var pageGroupEnd	= (pageGroupStart + naviSize) - 1 > totalPage ? totalPage : (pageGroupStart + naviSize) - 1;
		var pagePrev		= pageGroupStart - 1;
		var pageNext		= pageGroupEnd + 1;
		var naviHTML		= "";
		
		if(naviType == 1) {
			naviHTML +="<div class='board-paging'>";
			naviHTML +="	<ol>";
			
			if(pagePrev == 0)
				naviHTML +="		<li class='prev'>이전페이지</li>";
			else
				naviHTML +="		<li class='prev'><a href='" + boardProperties["returnUrl"] + "?pageNum=" + pagePrev + "' title='이전페이지'>이전페이지</a></li>";
			
			for(var i = pageGroupStart; i <= pageGroupEnd; i++) {
				if(i == pageNum)
					naviHTML +="		<li class='active'>" + i + "</li>";
				else
					naviHTML +="		<li><a href='" + boardProperties["returnUrl"] + "?pageNum=" + i + "' title='" + i + "페이지'>" + i + "</a></li>";
			}
			
			if(pageNext > totalPage)
				naviHTML +="		<li class='next'>다음페이지</li>";
			else
				naviHTML +="		<li class='next'><a href='" + boardProperties["returnUrl"] + "?pageNum=" + pageNext + "' title='이전페이지'>다음페이지</a></li>";
			
			naviHTML +="	</ol>";
			naviHTML +="</div>";
		}
		
		$("#pageNavigation").append(naviHTML);
	}
	
	// 리스트 호출
	var boardList = {
		init : function(options) {
			var obj			= $(this);
			
			var request = {
				bltnType	: boardProperties["bltnType"]
				, pageNum	: boardProperties["pageNum"]
				, rowCount	: boardProperties["rowCount"]
			};
			
			$.ajax({
				type : 'POST'
				, url : boardProperties["listUrl"]
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(result.resultCode == "SUCCESS") {
						if(boardProperties["bltnType"] == "1000001")
							drawNoticeList(obj, result);
						else if(boardProperties["bltnType"] == "1000002")
							drawFaqList(obj, result);
						else if(boardProperties["bltnType"] == "1000003")
							drawInquireList(obj, result);
						
						if(result.totalCnt > 0)
							setPageNavigation(1, result.totalCnt);
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
		}
	}
	
	// 리스트 플러그인
	$.fn.callBoardList = function(opt) {
		setBoardProperty(arguments);
		return boardList.init.apply(this, arguments);
	}
	
	function drawModify(result, obj) {
		if(boardProperties["bltnType"] == "1000003") {
			var data = result.data;
			
			for(var i = 0; i < data.length; i++) {
				if(i == 0) {
					$('#txt_title').text(result.data[i].title);
					$('#txt_cts').text(result.data[i].cts);
				} else {
					$('#bltnCd').val(result.data[i].bltnCd);
					$('.text-editor').summernote('code', result.data[i].cts);
				}
			}
		} else {
			$('#title').val(result.data.title);
			$('.text-editor').summernote('code', result.data.cts);
		}
	}
	
	// 상세데이터 호출
	var boardData = {
		init : function(options) {
			var obj	= $(this);
			
			var request = {
				bltnType	: boardProperties["bltnType"]
				, bltnCd	: boardProperties["bltnCd"]
			};
			
			$.ajax({
				type : 'POST'
				, url : boardProperties["detailUrl"]
				, contentType : 'application/json;charset=UTF-8'
				, data : JSON.stringify(request)
				, dataType : 'json'
				, success : function(response) {
					var result = response.result;
					
					if(boardProperties.pageType == "view")
						drawView(result, obj);
					if(boardProperties.pageType == "modify")
						drawModify(result, obj);
					if(boardProperties.pageType == "write")
						drawModify(result, obj);
					
					if(result.resultCode == "SUCCESS") {
						
					} else {
						alert(result.resultMessage);
					}
				}
				, error : function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.status + "Error");
				}
			});
		}
	}
	
	// 상세 플러그인
	$.fn.callBoardData = function(opt) {
		setBoardProperty(arguments);
		return boardData.init.apply(this, arguments);
	}
});
