/**
 * 문자열 byte계산
 * 
 * @param obj
 * @returns {Number}
 */
function fn_checkByte(obj) {
	var len = obj.val().length;
	var temp = obj.val();
	var bytes = 0;

	for(var k = 0; k < len; k++) {
		var onechar = escape(temp.charAt(k));
		
		if(onechar.length == 1) {
			bytes++;
		} else if(onechar.indexOf("%u") != -1) {
			bytes += 3;
		} else if(onechar.indexOf("%") != -1) {
			bytes += onechar.length/3;
		}
	}

	return bytes;
};

function fn_checkLen(obj, num) {
	var len = obj.val().length;
	var temp = obj.val();
	
	if(len > num) {
		return false;
	}

	return true;
};