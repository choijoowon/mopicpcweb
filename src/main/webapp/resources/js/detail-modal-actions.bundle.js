require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({205:[function(require,module,exports){
'use strict';

var CommonActions = require('./actions/CommonActions'),
    checkIE = require('./utils/checkIE');

var IE_version = checkIE();

if (!IE_version || IE_version >= 9) {
	$("#gray-scroll .gray-scroll-in").mCustomScrollbar({
		setHeight: 410,
		theme: "dark-3"
	});
}

$('#btn-interest').click(function () {
	var $this = $(this);

	var resultIdx = $this.closest('.modal').data('idx');
	var product_type = $this.data('product_type');
	var prod_code = $this.data('prod_code');

	$this.remove();

	CommonActions.addInterested(product_type, prod_code, resultIdx, function () {
		// TODO : 추가되었습니다 + $this.remove
		$('#concern-msg').text('관심상품으로 추가 되었습니다.').addClass('slideIn');
		setTimeout(function () {
			$('#concern-msg').removeClass('slideIn');
		}, 2000);
	});
});

},{"./actions/CommonActions":186,"./utils/checkIE":212}]},{},[205])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvY29tbW9uL2RldGFpbC1tb2RhbC1hY3Rpb25zLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7QUNBQSxJQUFJLGdCQUFnQixRQUFRLHlCQUFSLENBQWhCO0lBQ0gsVUFBWSxRQUFRLGlCQUFSLENBQVo7O0FBRUQsSUFBSSxhQUFhLFNBQWI7O0FBRUosSUFBRyxDQUFDLFVBQUQsSUFBZSxjQUFjLENBQWQsRUFBaUI7QUFDbEMsR0FBRSw4QkFBRixFQUFrQyxnQkFBbEMsQ0FBbUQ7QUFDbEQsYUFBVSxHQUFWO0FBQ0EsU0FBTSxRQUFOO0VBRkQsRUFEa0M7Q0FBbkM7O0FBT0EsRUFBRSxlQUFGLEVBQW1CLEtBQW5CLENBQXlCLFlBQVc7QUFDbkMsS0FBSSxRQUFRLEVBQUUsSUFBRixDQUFSLENBRCtCOztBQUduQyxLQUFJLFlBQVksTUFBTSxPQUFOLENBQWMsUUFBZCxFQUF3QixJQUF4QixDQUE2QixLQUE3QixDQUFaLENBSCtCO0FBSW5DLEtBQUksZUFBZSxNQUFNLElBQU4sQ0FBVyxjQUFYLENBQWYsQ0FKK0I7QUFLbkMsS0FBSSxZQUFZLE1BQU0sSUFBTixDQUFXLFdBQVgsQ0FBWixDQUwrQjs7QUFPbkMsT0FBTSxNQUFOLEdBUG1DOztBQVNuQyxlQUFjLGFBQWQsQ0FBNEIsWUFBNUIsRUFBMEMsU0FBMUMsRUFBcUQsU0FBckQsRUFBZ0UsWUFBVzs7QUFFMUUsSUFBRSxjQUFGLEVBQWtCLElBQWxCLENBQXVCLGtCQUF2QixFQUEyQyxRQUEzQyxDQUFvRCxTQUFwRCxFQUYwRTtBQUdwRSxhQUFXLFlBQVc7QUFBRSxLQUFFLGNBQUYsRUFBa0IsV0FBbEIsQ0FBOEIsU0FBOUIsRUFBRjtHQUFYLEVBQTBELElBQXJFLEVBSG9FO0VBQVgsQ0FBaEUsQ0FUbUM7Q0FBWCxDQUF6QiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJsZXQgQ29tbW9uQWN0aW9uc1x0PSByZXF1aXJlKCcuL2FjdGlvbnMvQ29tbW9uQWN0aW9ucycpLFxuXHRjaGVja0lFXHRcdFx0PSByZXF1aXJlKCcuL3V0aWxzL2NoZWNrSUUnKTtcblxubGV0IElFX3ZlcnNpb24gPSBjaGVja0lFKCk7XG5cbmlmKCFJRV92ZXJzaW9uIHx8IElFX3ZlcnNpb24gPj0gOSkge1xuXHQkKFwiI2dyYXktc2Nyb2xsIC5ncmF5LXNjcm9sbC1pblwiKS5tQ3VzdG9tU2Nyb2xsYmFyKHtcblx0XHRzZXRIZWlnaHQ6NDEwLFxuXHRcdHRoZW1lOlwiZGFyay0zXCJcblx0fSk7XG59XG5cbiQoJyNidG4taW50ZXJlc3QnKS5jbGljayhmdW5jdGlvbigpIHtcblx0bGV0ICR0aGlzID0gJCh0aGlzKTtcblx0XG5cdGxldCByZXN1bHRJZHggPSAkdGhpcy5jbG9zZXN0KCcubW9kYWwnKS5kYXRhKCdpZHgnKTtcblx0bGV0IHByb2R1Y3RfdHlwZSA9ICR0aGlzLmRhdGEoJ3Byb2R1Y3RfdHlwZScpO1xuXHRsZXQgcHJvZF9jb2RlID0gJHRoaXMuZGF0YSgncHJvZF9jb2RlJyk7XG5cdFxuXHQkdGhpcy5yZW1vdmUoKTtcblxuXHRDb21tb25BY3Rpb25zLmFkZEludGVyZXN0ZWQocHJvZHVjdF90eXBlLCBwcm9kX2NvZGUsIHJlc3VsdElkeCwgZnVuY3Rpb24oKSB7XG5cdFx0Ly8gVE9ETyA6IOy2lOqwgOuQmOyXiOyKteuLiOuLpCArICR0aGlzLnJlbW92ZVxuXHRcdCQoJyNjb25jZXJuLW1zZycpLnRleHQoJ+q0gOyLrOyDge2SiOycvOuhnCDstpTqsIAg65CY7JeI7Iq164uI64ukLicpLmFkZENsYXNzKCdzbGlkZUluJyk7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7ICQoJyNjb25jZXJuLW1zZycpLnJlbW92ZUNsYXNzKCdzbGlkZUluJyk7IH0sIDIwMDApO1xuXHR9KTtcbn0pO1xuIl19
