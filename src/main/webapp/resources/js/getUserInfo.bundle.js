require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({207:[function(require,module,exports){
'use strict';

(function () {
	if (window.App.isFirstLogin && window.App.isFirstLogin === 'Y') {
		var React = require('react'),
		    ReactDOM = require('react-dom'),
		    UserInfoAdder = require('./components/UserInfoAdder.jsx');

		ReactDOM.render(React.createElement(UserInfoAdder, null), document.getElementById('first-time-container'), function () {
			//setCookie('isFirstLogin', 'N');
			$('#infoAdder').modal('show');
		});
	}
})();

},{"./components/UserInfoAdder.jsx":201,"react":177,"react-dom":20}],201:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    ReactDOM = require('react-dom'),
    CommonActions = require('../actions/CommonActions'),
    SelectDropdown = require('./SelectDropdown.jsx'),
    TextInput = require('./TextInput.jsx'),
    CheckboxGroup = require('./CheckboxGroup.jsx'),
    Api = require('../utils/api'),
    birthdate = require('../utils/formValidators').birthdate;

var SelectOpts = function SelectOpts(label, value, data) {
	_classCallCheck(this, SelectOpts);

	this.label = label;
	this.value = value;
	this.data = data;
};

function getSelectOptions(optCodeMap, code) {
	if (optCodeMap[code]) {
		return optCodeMap[code].map(function (e) {
			return new SelectOpts(e.cdName, e.cd);
		});
	}
	// TODO: when no map for the code?
	return [];
}

var UserInfoAdder = React.createClass({
	displayName: 'UserInfoAdder',

	mixins: [require('../../common/utils/queryStoreMixin')],

	propTypes: {},

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {
			occupation: '',
			sex: '',
			mainBank: '',
			bankCode: '',
			dob: '',
			income: '',
			productInterest: [],
			homeCity: '',
			homeTown: '',
			homeTownOpts: [],
			homeTownPlacholder: '시/군/도'
		};
	},
	componentDidMount: function componentDidMount() {
		CommonActions.fetchOptions([7001, 7002, 7003]);

		Api.post('/mopic/api/mem/selectSigunguInfo', {}).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				CommonActions.addOptions({ sidoOpts: res.result.data });
			}
		}.bind(this));
	},
	componentWillUnmount: function componentWillUnmount() {},
	render: function render() {
		var _state = this.state;
		var queryState = _state.queryState;
		var occupation = _state.occupation;
		var sex = _state.sex;
		var mainBank = _state.mainBank;
		var bankCode = _state.bankCode;
		var dob = _state.dob;
		var income = _state.income;
		var productInterest = _state.productInterest;
		var homeCity = _state.homeCity;
		var homeTown = _state.homeTown;
		var homeTownOpts = _state.homeTownOpts;
		var homeTownPlacholder = _state.homeTownPlacholder;
		var optCodeMap = queryState.optCodeMap;


		var sidoOpts = [];
		if (optCodeMap['sidoOpts']) {
			sidoOpts = optCodeMap['sidoOpts'].map(function (el) {
				return new SelectOpts(el.sido_name, el.sido, el);
			});
		}

		return React.createElement(
			'div',
			{ className: 'modal fade', id: 'infoAdder', tabIndex: -1, role: 'dialog', 'aria-labelledby': 'myModalLabel', 'aria-hidden': 'true' },
			React.createElement(
				'div',
				{ className: 'modal-dialog' },
				React.createElement(
					'div',
					{ className: 'modal-content modal-small' },
					React.createElement('a', { href: true, className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' }),
					React.createElement(
						'div',
						{ className: 'modal-header' },
						React.createElement(
							'h4',
							{ className: 'modal-title', id: 'myModalLabel' },
							React.createElement('img', { src: '/resources/images/common/logo_blue.png', alt: 'mopic', width: '110' })
						)
					),
					React.createElement(
						'div',
						{ className: 'modal-body' },
						React.createElement(
							'div',
							{ className: 'product_add' },
							React.createElement(
								'p',
								{ className: 'txt' },
								'추가정보를 입력하시면, 입력하신 정보를 기반으로 더 좋은 상품을 추천 해드립니다.'
							),
							React.createElement(
								'div',
								{ className: 'row' },
								React.createElement(
									'div',
									{ className: 'col-xs-5' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'직업'
									),
									React.createElement(SelectDropdown, {
										placeholder: '선택',
										options: getSelectOptions(optCodeMap, 7001),
										handleSelect: this.handleSelect.bind(null, 'occupation'),
										selected: occupation,
										validationFn: false
									})
								),
								React.createElement(
									'div',
									{ className: 'col-xs-5 col-xs-offset-1' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'생년월일'
									),
									React.createElement(TextInput, {
										type: 'number',
										placeholder: 'YYMMDD',
										inputClass: 'text-input-normal',
										maxlength: '6',
										handleChange: this.handleTextChange.bind(null, 'dob'),
										value: dob,
										validationFn: birthdate
									})
								)
							),
							React.createElement(
								'div',
								{ className: 'row' },
								React.createElement(
									'div',
									{ className: 'col-xs-5' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'성별'
									),
									React.createElement(SelectDropdown, {
										placeholder: '선택',
										options: getSelectOptions(optCodeMap, 1234),
										handleSelect: this.handleSelect.bind(null, 'sex'),
										selected: sex,
										validationFn: false
									})
								),
								React.createElement(
									'div',
									{ className: 'col-xs-5 col-xs-offset-1 income' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'가처분소득(월)'
									),
									React.createElement(TextInput, {
										type: 'number-with-commas',
										placeholder: ' ',
										inputClass: 'text-input-normal',
										handleChange: this.handleTextChange.bind(null, 'income'),
										value: income,
										unit: '만원',
										validationFn: false
									})
								)
							),
							React.createElement(
								'div',
								{ className: 'row' },
								React.createElement(
									'div',
									{ className: 'col-xs-5' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'주거래은행'
									),
									React.createElement(SelectDropdown, {
										placeholder: '선택',
										options: getSelectOptions(optCodeMap, 7002),
										handleSelect: this.handleSelect.bind(null, 'bankCode'),
										selected: bankCode,
										validationFn: false
									})
								),
								React.createElement(
									'div',
									{ className: 'col-xs-5 col-xs-offset-1' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'거주지역'
									),
									React.createElement(SelectDropdown, {
										placeholder: '시/도',
										options: sidoOpts,
										handleSelect: this.handleSidoSelect.bind(null, sidoOpts),
										selected: homeCity,
										validationFn: false
									})
								)
							),
							React.createElement(
								'div',
								{ className: 'row' },
								React.createElement(
									'div',
									{ className: 'col-xs-2' },
									React.createElement(
										'label',
										{ htmlFor: 'select', className: 'select-label' },
										'관심분야'
									)
								),
								React.createElement(
									'div',
									{ className: 'col-xs-10' },
									React.createElement(CheckboxGroup, {
										options: getSelectOptions(optCodeMap, 7003),
										selected: productInterest,
										handleSelect: this.handleCheckboxToggle.bind(null, 'productInterest')
									})
								)
							),
							React.createElement(
								'div',
								{ className: 'center_btn', onClick: this.submitForm },
								React.createElement(
									'a',
									{ className: 'btn btn-submit', role: 'button' },
									'확인'
								)
							)
						)
					)
				)
			)
		);
	},
	handleTextChange: function handleTextChange(attr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = target.value;

		this.setState(_defineProperty({}, attr, val));
	},
	handleSelect: function handleSelect(attr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState(_defineProperty({}, attr, val));
	},
	handleSidoSelect: function handleSidoSelect(sidoOpts, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('li').attr('data-val') || target.textContent;

		var sigundoOpts = sidoOpts.find(function (e) {
			return e.value === val;
		}).data.sigungu_name;
		sigundoOpts = JSON.parse('{' + sigundoOpts + '}');

		var homeTownOpts = [];
		for (var key in sigundoOpts) {
			homeTownOpts.push(new SelectOpts(sigundoOpts[key], key));
		}

		this.setState({
			homeCity: val,
			homeTown: '',
			homeTownPlacholder: '선택',
			homeTownOpts: homeTownOpts
		});
	},
	handleCheckboxToggle: function handleCheckboxToggle(attr, evt) {
		var selected = this.state[attr];

		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('[data-val]').attr('data-val');
		var idx = selected.indexOf(val);

		if (idx > -1) {
			selected.splice(idx, 1);
		} else {
			selected.push(val);
		}

		this.setState(_defineProperty({}, attr, selected));
	},
	submitForm: function submitForm(evt) {
		var _this = this;

		if (birthdate(this.state.dob) === true) {
			(function () {
				evt = evt ? evt : window.event;
				var target = evt.target || evt.srcElement;

				var data = _extends({}, _this.state, {
					productInterest: _this.state.productInterest.join(',')
				});

				delete data.homeTownOpts;
				delete data.homeTownPlacholder;

				Api.post('/mopic/api/updateUserInfo', data).then(function (res) {
					if (res.result.resultCode === 'SUCCESS') {
						$(target).closest('.modal').modal('hide');
					}
				}.bind(_this));
			})();
		}
	}
});

module.exports = UserInfoAdder;

},{"../../common/utils/queryStoreMixin":220,"../actions/CommonActions":186,"../utils/api":211,"../utils/formValidators":215,"./CheckboxGroup.jsx":189,"./SelectDropdown.jsx":197,"./TextInput.jsx":200,"react":177,"react-dom":20}],189:[function(require,module,exports){
'use strict';

var React = require('react'),
    ReactDOM = require('react-dom');

var CheckboxGroup = React.createClass({
	displayName: 'CheckboxGroup',

	propTypes: {
		handleSelect: React.PropTypes.func,
		options: React.PropTypes.array,
		selected: React.PropTypes.array
	},

	//disabled: React.PropTypes.bool,
	//className: React.PropTypes.string,
	//tabIndex: React.PropTypes.number
	getDefaultProps: function getDefaultProps() {
		return {
			options: ['-'],
			selected: []
		};
	},
	//disabled: false,
	//className: 'radio-group',
	//tabIndex: 0
	getInitialState: function getInitialState() {
		return {};
	},
	componentDidMount: function componentDidMount() {},
	componentWillUnmount: function componentWillUnmount() {},
	render: function render() {
		var _this = this;

		var _props =
		//tabIndex
		this.props;
		var options = _props.options;
		var selected = _props.selected;
		var
		//disabled,
		className = _props.className;


		var optionsElem = options.map(function (el, idx) {
			var label = el.label;
			var value = el.value;


			return React.createElement(
				'div',
				{ className: 'check-bottom',
					key: 'check' + idx,
					'data-val': el.value === 0 ? 0 : value || '' },
				React.createElement(
					'span',
					null,
					React.createElement('input', { type: 'checkbox', id: value, checked: _this.isChecked(value), onChange: _this.handleOptionClick }),
					React.createElement(
						'label',
						{ htmlFor: value },
						React.createElement('i', null),
						label
					)
				)
			);
		});

		return React.createElement(
			'div',
			{ className: 'check-top' },
			React.createElement(
				'div',
				{ className: 'check-top1' },
				optionsElem
			)
		);
	},
	handleOptionClick: function handleOptionClick(e) {
		this.props.handleSelect(e);
		//this.setState({
		//	tabIndex: null
		//});
	},
	isChecked: function isChecked(val) {
		return this.props.selected.indexOf(val) > -1;
	}
});

module.exports = CheckboxGroup;

},{"react":177,"react-dom":20}]},{},[207])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvY29tbW9uL2dldFVzZXJJbmZvLmpzeCIsInNyYy9jb21tb24vY29tcG9uZW50cy9Vc2VySW5mb0FkZGVyLmpzeCIsInNyYy9jb21tb24vY29tcG9uZW50cy9DaGVja2JveEdyb3VwLmpzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsQ0FBQyxZQUFXO0FBQ1gsS0FBRyxPQUFPLEdBQVAsQ0FBVyxZQUFYLElBQTJCLE9BQU8sR0FBUCxDQUFXLFlBQVgsS0FBNEIsR0FBNUIsRUFBaUM7QUFDOUQsTUFBSSxRQUFRLFFBQVEsT0FBUixDQUFSO01BQ0gsV0FBVyxRQUFRLFdBQVIsQ0FBWDtNQUNBLGdCQUFnQixRQUFRLGdDQUFSLENBQWhCLENBSDZEOztBQUs5RCxXQUFTLE1BQVQsQ0FDQyxvQkFBQyxhQUFELE9BREQsRUFFQyxTQUFTLGNBQVQsQ0FBd0Isc0JBQXhCLENBRkQsRUFHQyxZQUFNOztBQUVMLEtBQUUsWUFBRixFQUFnQixLQUFoQixDQUFzQixNQUF0QixFQUZLO0dBQU4sQ0FIRCxDQUw4RDtFQUEvRDtDQURBLENBQUQ7Ozs7Ozs7Ozs7O0FDQUEsSUFBSSxRQUFVLFFBQVEsT0FBUixDQUFWO0lBQ0gsV0FBWSxRQUFRLFdBQVIsQ0FBWjtJQUNBLGdCQUFnQixRQUFRLDBCQUFSLENBQWhCO0lBQ0EsaUJBQWlCLFFBQVEsc0JBQVIsQ0FBakI7SUFDQSxZQUFhLFFBQVEsaUJBQVIsQ0FBYjtJQUNBLGdCQUFnQixRQUFRLHFCQUFSLENBQWhCO0lBQ0EsTUFBUyxRQUFRLGNBQVIsQ0FBVDtJQUNBLFlBQWEsUUFBUSx5QkFBUixFQUFtQyxTQUFuQzs7SUFHUixhQUNMLFNBREssVUFDTCxDQUFZLEtBQVosRUFBbUIsS0FBbkIsRUFBMEIsSUFBMUIsRUFBZ0M7dUJBRDNCLFlBQzJCOztBQUMvQixNQUFLLEtBQUwsR0FBYSxLQUFiLENBRCtCO0FBRS9CLE1BQUssS0FBTCxHQUFhLEtBQWIsQ0FGK0I7QUFHL0IsTUFBSyxJQUFMLEdBQVksSUFBWixDQUgrQjtDQUFoQzs7QUFPRCxTQUFTLGdCQUFULENBQTBCLFVBQTFCLEVBQXNDLElBQXRDLEVBQTRDO0FBQzNDLEtBQUcsV0FBVyxJQUFYLENBQUgsRUFBcUI7QUFDcEIsU0FBUyxXQUFXLElBQVgsRUFBaUIsR0FBakIsQ0FBcUIsVUFBQyxDQUFEO1VBQU8sSUFBSSxVQUFKLENBQWUsRUFBRSxNQUFGLEVBQVUsRUFBRSxFQUFGO0dBQWhDLENBQTlCLENBRG9CO0VBQXJCOztBQUQyQyxRQUtwQyxFQUFQLENBTDJDO0NBQTVDOztBQVNBLElBQUksZ0JBQWdCLE1BQU0sV0FBTixDQUFrQjs7O0FBQ3JDLFNBQVEsQ0FDUCxRQUFRLG9DQUFSLENBRE8sQ0FBUjs7QUFJRyxZQUFXLEVBQVg7O0FBSUEsNkNBQWtCO0FBQ2QsU0FBTyxFQUFQLENBRGM7RUFUZ0I7QUFlckMsNkNBQWtCO0FBQ2pCLFNBQU87QUFDTixlQUFZLEVBQVo7QUFDQSxRQUFLLEVBQUw7QUFDQSxhQUFVLEVBQVY7QUFDQSxhQUFVLEVBQVY7QUFDQSxRQUFLLEVBQUw7QUFDQSxXQUFRLEVBQVI7QUFDQSxvQkFBaUIsRUFBakI7QUFDQSxhQUFVLEVBQVY7QUFDQSxhQUFVLEVBQVY7QUFDQSxpQkFBYyxFQUFkO0FBQ0EsdUJBQW9CLE9BQXBCO0dBWEQsQ0FEaUI7RUFmbUI7QUFnQ2xDLGlEQUFvQjtBQUN0QixnQkFBYyxZQUFkLENBQTJCLENBQzFCLElBRDBCLEVBQ3BCLElBRG9CLEVBQ2QsSUFEYyxDQUEzQixFQURzQjs7QUFLdEIsTUFBSSxJQUFKLENBQVMsa0NBQVQsRUFBNkMsRUFBN0MsRUFDRSxJQURGLENBQ08sVUFBVSxHQUFWLEVBQWU7QUFDcEIsT0FBRyxJQUFJLE1BQUosQ0FBVyxVQUFYLEtBQTBCLFNBQTFCLEVBQXFDO0FBQ3ZDLGtCQUFjLFVBQWQsQ0FBeUIsRUFBRSxVQUFVLElBQUksTUFBSixDQUFXLElBQVgsRUFBckMsRUFEdUM7SUFBeEM7R0FESyxDQUlKLElBSkksQ0FJQyxJQUpELENBRFAsRUFMc0I7RUFoQ2M7QUE2Q2xDLHVEQUF1QixFQTdDVztBQWlEbEMsMkJBQVM7ZUFjUCxLQUFLLEtBQUwsQ0FkTztNQUVWLCtCQUZVO01BR1YsK0JBSFU7TUFJVixpQkFKVTtNQUtWLDJCQUxVO01BTVYsMkJBTlU7TUFPVixpQkFQVTtNQVFWLHVCQVJVO01BU1YseUNBVFU7TUFVViwyQkFWVTtNQVdWLDJCQVhVO01BWVYsbUNBWlU7TUFhViwrQ0FiVTtNQWdCTCxhQUFlLFdBQWYsV0FoQks7OztBQW1CWCxNQUFJLFdBQVcsRUFBWCxDQW5CTztBQW9CWCxNQUFHLFdBQVcsVUFBWCxDQUFILEVBQTJCO0FBQzFCLGNBQVcsV0FBVyxVQUFYLEVBQXVCLEdBQXZCLENBQTJCLFVBQUMsRUFBRDtXQUFRLElBQUksVUFBSixDQUFlLEdBQUcsU0FBSCxFQUFjLEdBQUcsSUFBSCxFQUFTLEVBQXRDO0lBQVIsQ0FBdEMsQ0FEMEI7R0FBM0I7O0FBS0EsU0FDQzs7S0FBSyxXQUFVLFlBQVYsRUFBdUIsSUFBRyxXQUFILEVBQWUsVUFBVSxDQUFDLENBQUQsRUFBSSxNQUFLLFFBQUwsRUFBYyxtQkFBZ0IsY0FBaEIsRUFBK0IsZUFBWSxNQUFaLEVBQXRHO0dBQ0M7O01BQUssV0FBVSxjQUFWLEVBQUw7SUFDQzs7T0FBSyxXQUFVLDJCQUFWLEVBQUw7S0FDQywyQkFBRyxZQUFLLFdBQVUsT0FBVixFQUFrQixnQkFBYSxPQUFiLEVBQXFCLGNBQVcsT0FBWCxFQUEvQyxDQUREO0tBRUM7O1FBQUssV0FBVSxjQUFWLEVBQUw7TUFDQzs7U0FBSSxXQUFVLGFBQVYsRUFBd0IsSUFBRyxjQUFILEVBQTVCO09BQThDLDZCQUFLLEtBQUksd0NBQUosRUFBNkMsS0FBSSxPQUFKLEVBQVksT0FBTSxLQUFOLEVBQTlELENBQTlDO09BREQ7TUFGRDtLQUtDOztRQUFLLFdBQVUsWUFBVixFQUFMO01BQ0M7O1NBQUssV0FBVSxhQUFWLEVBQUw7T0FDQzs7VUFBRyxXQUFVLEtBQVYsRUFBSDs7UUFERDtPQUVDOztVQUFLLFdBQVUsS0FBVixFQUFMO1FBQ0M7O1dBQUssV0FBVSxVQUFWLEVBQUw7U0FDQzs7WUFBTyxTQUFRLFFBQVIsRUFBaUIsV0FBVSxjQUFWLEVBQXhCOztVQUREO1NBRUMsb0JBQUMsY0FBRDtBQUNDLHVCQUFhLElBQWI7QUFDQSxtQkFBUyxpQkFBaUIsVUFBakIsRUFBNkIsSUFBN0IsQ0FBVDtBQUNBLHdCQUFjLEtBQUssWUFBTCxDQUFrQixJQUFsQixDQUF1QixJQUF2QixFQUE2QixZQUE3QixDQUFkO0FBQ0Esb0JBQVUsVUFBVjtBQUNBLHdCQUFjLEtBQWQ7VUFMRCxDQUZEO1NBREQ7UUFXQzs7V0FBSyxXQUFVLDBCQUFWLEVBQUw7U0FDQzs7WUFBTyxTQUFRLFFBQVIsRUFBaUIsV0FBVSxjQUFWLEVBQXhCOztVQUREO1NBRUMsb0JBQUMsU0FBRDtBQUNDLGdCQUFLLFFBQUw7QUFDQSx1QkFBYSxRQUFiO0FBQ0Esc0JBQVcsbUJBQVg7QUFDQSxxQkFBVSxHQUFWO0FBQ0Esd0JBQWMsS0FBSyxnQkFBTCxDQUFzQixJQUF0QixDQUEyQixJQUEzQixFQUFpQyxLQUFqQyxDQUFkO0FBQ0EsaUJBQU8sR0FBUDtBQUNBLHdCQUFjLFNBQWQ7VUFQRCxDQUZEO1NBWEQ7UUFGRDtPQTBCQzs7VUFBSyxXQUFVLEtBQVYsRUFBTDtRQUNDOztXQUFLLFdBQVUsVUFBVixFQUFMO1NBQ0M7O1lBQU8sU0FBUSxRQUFSLEVBQWlCLFdBQVUsY0FBVixFQUF4Qjs7VUFERDtTQUVDLG9CQUFDLGNBQUQ7QUFDQyx1QkFBYSxJQUFiO0FBQ0EsbUJBQVMsaUJBQWlCLFVBQWpCLEVBQTZCLElBQTdCLENBQVQ7QUFDQSx3QkFBYyxLQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUIsSUFBdkIsRUFBNkIsS0FBN0IsQ0FBZDtBQUNBLG9CQUFVLEdBQVY7QUFDQSx3QkFBYyxLQUFkO1VBTEQsQ0FGRDtTQUREO1FBV0M7O1dBQUssV0FBVSxpQ0FBVixFQUFMO1NBQ0M7O1lBQU8sU0FBUSxRQUFSLEVBQWlCLFdBQVUsY0FBVixFQUF4Qjs7VUFERDtTQUVDLG9CQUFDLFNBQUQ7QUFDQyxnQkFBSyxvQkFBTDtBQUNBLHVCQUFhLEdBQWI7QUFDQSxzQkFBVyxtQkFBWDtBQUNBLHdCQUFjLEtBQUssZ0JBQUwsQ0FBc0IsSUFBdEIsQ0FBMkIsSUFBM0IsRUFBaUMsUUFBakMsQ0FBZDtBQUNBLGlCQUFPLE1BQVA7QUFDQSxnQkFBTSxJQUFOO0FBQ0Esd0JBQWMsS0FBZDtVQVBELENBRkQ7U0FYRDtRQTFCRDtPQWtEQzs7VUFBSyxXQUFVLEtBQVYsRUFBTDtRQUNDOztXQUFLLFdBQVUsVUFBVixFQUFMO1NBQ0M7O1lBQU8sU0FBUSxRQUFSLEVBQWlCLFdBQVUsY0FBVixFQUF4Qjs7VUFERDtTQUVDLG9CQUFDLGNBQUQ7QUFDQyx1QkFBYSxJQUFiO0FBQ0EsbUJBQVMsaUJBQWlCLFVBQWpCLEVBQTZCLElBQTdCLENBQVQ7QUFDQSx3QkFBYyxLQUFLLFlBQUwsQ0FBa0IsSUFBbEIsQ0FBdUIsSUFBdkIsRUFBNkIsVUFBN0IsQ0FBZDtBQUNBLG9CQUFVLFFBQVY7QUFDQSx3QkFBYyxLQUFkO1VBTEQsQ0FGRDtTQUREO1FBV0M7O1dBQUssV0FBVSwwQkFBVixFQUFMO1NBQ0M7O1lBQU8sU0FBUSxRQUFSLEVBQWlCLFdBQVUsY0FBVixFQUF4Qjs7VUFERDtTQUVDLG9CQUFDLGNBQUQ7QUFDQyx1QkFBYSxLQUFiO0FBQ0EsbUJBQVMsUUFBVDtBQUNBLHdCQUFjLEtBQUssZ0JBQUwsQ0FBc0IsSUFBdEIsQ0FBMkIsSUFBM0IsRUFBaUMsUUFBakMsQ0FBZDtBQUNBLG9CQUFVLFFBQVY7QUFDQSx3QkFBYyxLQUFkO1VBTEQsQ0FGRDtTQVhEO1FBbEREO09BcUZDOztVQUFLLFdBQVUsS0FBVixFQUFMO1FBQ0M7O1dBQUssV0FBVSxVQUFWLEVBQUw7U0FDQzs7WUFBTyxTQUFRLFFBQVIsRUFBaUIsV0FBVSxjQUFWLEVBQXhCOztVQUREO1NBREQ7UUFJQzs7V0FBSyxXQUFVLFdBQVYsRUFBTDtTQUNDLG9CQUFDLGFBQUQ7QUFDQyxtQkFBUyxpQkFBaUIsVUFBakIsRUFBNkIsSUFBN0IsQ0FBVDtBQUNBLG9CQUFVLGVBQVY7QUFDQSx3QkFBYyxLQUFLLG9CQUFMLENBQTBCLElBQTFCLENBQStCLElBQS9CLEVBQXFDLGlCQUFyQyxDQUFkO1VBSEQsQ0FERDtTQUpEO1FBckZEO09BaUdDOztVQUFLLFdBQVUsWUFBVixFQUF1QixTQUFTLEtBQUssVUFBTCxFQUFyQztRQUFzRDs7V0FBRyxXQUFVLGdCQUFWLEVBQTJCLE1BQUssUUFBTCxFQUE5Qjs7U0FBdEQ7UUFqR0Q7T0FERDtNQUxEO0tBREQ7SUFERDtHQURELENBekJXO0VBakR5QjtBQTZMckMsNkNBQWlCLE1BQU0sS0FBSztBQUMzQixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURVO0FBRTNCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGQTs7QUFJM0IsTUFBSSxNQUFNLE9BQU8sS0FBUCxDQUppQjs7QUFNM0IsT0FBSyxRQUFMLHFCQUNFLE1BQU8sSUFEVCxFQU4yQjtFQTdMUztBQXdNckMscUNBQWEsTUFBTSxLQUFLO0FBQ3ZCLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBRE07QUFFdkIsTUFBSSxTQUFTLElBQUksTUFBSixJQUFjLElBQUksVUFBSixDQUZKOztBQUl2QixNQUFJLE1BQU0sRUFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixJQUFsQixFQUF3QixJQUF4QixDQUE2QixVQUE3QixLQUE0QyxPQUFPLFdBQVAsQ0FKL0I7O0FBTXZCLE9BQUssUUFBTCxxQkFDRSxNQUFPLElBRFQsRUFOdUI7RUF4TWE7QUFtTnJDLDZDQUFpQixVQUFVLEtBQUs7QUFDL0IsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FEYztBQUUvQixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRkk7O0FBSS9CLE1BQUksTUFBTSxFQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLElBQWxCLEVBQXdCLElBQXhCLENBQTZCLFVBQTdCLEtBQTRDLE9BQU8sV0FBUCxDQUp2Qjs7QUFNL0IsTUFBSSxjQUFjLFNBQVMsSUFBVCxDQUFjLFVBQUMsQ0FBRDtVQUFPLEVBQUUsS0FBRixLQUFZLEdBQVo7R0FBUCxDQUFkLENBQXNDLElBQXRDLENBQTJDLFlBQTNDLENBTmE7QUFPL0IsZ0JBQWMsS0FBSyxLQUFMLE9BQWUsaUJBQWYsQ0FBZCxDQVArQjs7QUFTL0IsTUFBSSxlQUFlLEVBQWYsQ0FUMkI7QUFVL0IsT0FBSSxJQUFJLEdBQUosSUFBVyxXQUFmLEVBQTRCO0FBQzNCLGdCQUFhLElBQWIsQ0FBa0IsSUFBSSxVQUFKLENBQWUsWUFBWSxHQUFaLENBQWYsRUFBaUMsR0FBakMsQ0FBbEIsRUFEMkI7R0FBNUI7O0FBSUEsT0FBSyxRQUFMLENBQWM7QUFDYixhQUFVLEdBQVY7QUFDQSxhQUFVLEVBQVY7QUFDQSx1QkFBb0IsSUFBcEI7QUFDQSw2QkFKYTtHQUFkLEVBZCtCO0VBbk5LO0FBeU9yQyxxREFBcUIsTUFBTSxLQUFLO0FBQy9CLE1BQUksV0FBVyxLQUFLLEtBQUwsQ0FBVyxJQUFYLENBQVgsQ0FEMkI7O0FBRy9CLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBSGM7QUFJL0IsTUFBSSxTQUFTLElBQUksTUFBSixJQUFjLElBQUksVUFBSixDQUpJOztBQU0vQixNQUFJLE1BQU0sRUFBRSxNQUFGLEVBQVUsT0FBVixDQUFrQixZQUFsQixFQUFnQyxJQUFoQyxDQUFxQyxVQUFyQyxDQUFOLENBTjJCO0FBTy9CLE1BQUksTUFBTSxTQUFTLE9BQVQsQ0FBaUIsR0FBakIsQ0FBTixDQVAyQjs7QUFTL0IsTUFBRyxNQUFNLENBQUMsQ0FBRCxFQUFJO0FBQ1osWUFBUyxNQUFULENBQWdCLEdBQWhCLEVBQXFCLENBQXJCLEVBRFk7R0FBYixNQUVPO0FBQ04sWUFBUyxJQUFULENBQWMsR0FBZCxFQURNO0dBRlA7O0FBT0EsT0FBSyxRQUFMLHFCQUNFLE1BQU8sU0FEVCxFQWhCK0I7RUF6T0s7QUE4UHJDLGlDQUFXLEtBQUs7OztBQUNmLE1BQUcsVUFBVSxLQUFLLEtBQUwsQ0FBVyxHQUFYLENBQVYsS0FBOEIsSUFBOUIsRUFBb0M7O0FBQ3RDLFVBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQO0FBQ2pCLFFBQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUo7O0FBRTNCLFFBQUksT0FBTyxTQUFjLEVBQWQsRUFBa0IsTUFBSyxLQUFMLEVBQVk7QUFDeEMsc0JBQWlCLE1BQUssS0FBTCxDQUFXLGVBQVgsQ0FBMkIsSUFBM0IsQ0FBZ0MsR0FBaEMsQ0FBakI7S0FEVSxDQUFQOztBQUlKLFdBQU8sS0FBSyxZQUFMO0FBQ1AsV0FBTyxLQUFLLGtCQUFMOztBQUVQLFFBQUksSUFBSixDQUFTLDJCQUFULEVBQXNDLElBQXRDLEVBQ0UsSUFERixDQUNPLFVBQVUsR0FBVixFQUFlO0FBQ3BCLFNBQUcsSUFBSSxNQUFKLENBQVcsVUFBWCxLQUEwQixTQUExQixFQUFxQztBQUN2QyxRQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLFFBQWxCLEVBQTRCLEtBQTVCLENBQWtDLE1BQWxDLEVBRHVDO01BQXhDO0tBREssQ0FJSixJQUpJLE9BRFA7UUFYc0M7R0FBdkM7RUEvUG9DO0NBQWxCLENBQWhCOztBQXVSSixPQUFPLE9BQVAsR0FBaUIsYUFBakI7Ozs7O0FDbFRBLElBQUksUUFBUyxRQUFRLE9BQVIsQ0FBVDtJQUNILFdBQVcsUUFBUSxXQUFSLENBQVg7O0FBRUQsSUFBSSxnQkFBZ0IsTUFBTSxXQUFOLENBQWtCOzs7QUFDckMsWUFBVztBQUNWLGdCQUFjLE1BQU0sU0FBTixDQUFnQixJQUFoQjtBQUNkLFdBQVMsTUFBTSxTQUFOLENBQWdCLEtBQWhCO0FBQ1QsWUFBVSxNQUFNLFNBQU4sQ0FBZ0IsS0FBaEI7RUFIWDs7Ozs7QUFTQSw2Q0FBa0I7QUFDakIsU0FBTztBQUNOLFlBQVMsQ0FBQyxHQUFELENBQVQ7QUFDQSxhQUFVLEVBQVY7R0FGRCxDQURpQjtFQVZtQjs7OztBQW9CckMsNkNBQWtCO0FBQ2pCLFNBQU8sRUFBUCxDQURpQjtFQXBCbUI7QUEwQnJDLGlEQUFvQixFQTFCaUI7QUE4QnJDLHVEQUF1QixFQTlCYztBQWtDckMsMkJBQVM7Ozs7O0FBT0osT0FBSyxLQUFMLENBUEk7TUFFUCx5QkFGTztNQUdQLDJCQUhPOzs7QUFLUCwrQkFMTzs7O0FBVVIsTUFBSSxjQUFjLFFBQVEsR0FBUixDQUFZLFVBQUMsRUFBRCxFQUFLLEdBQUwsRUFBYTtPQUNwQyxRQUFpQixHQUFqQixNQURvQztPQUM3QixRQUFVLEdBQVYsTUFENkI7OztBQUcxQyxVQUNDOztNQUFLLFdBQVUsY0FBVjtBQUNKLG9CQUFhLEdBQWI7QUFDQSxpQkFBVyxHQUFHLEtBQUgsS0FBYSxDQUFiLEdBQWlCLENBQWpCLEdBQXNCLFNBQVMsRUFBVCxFQUZsQztJQUdDOzs7S0FDQywrQkFBTyxNQUFLLFVBQUwsRUFBZ0IsSUFBSSxLQUFKLEVBQVcsU0FBUyxNQUFLLFNBQUwsQ0FBZSxLQUFmLENBQVQsRUFBZ0MsVUFBVSxNQUFLLGlCQUFMLEVBQTVFLENBREQ7S0FFQzs7UUFBTyxTQUFTLEtBQVQsRUFBUDtNQUF1Qiw4QkFBdkI7TUFBNkIsS0FBN0I7TUFGRDtLQUhEO0lBREQsQ0FIMEM7R0FBYixDQUExQixDQVZJOztBQTBCUixTQUNDOztLQUFLLFdBQVUsV0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxZQUFWLEVBQUw7SUFDRSxXQURGO0lBREQ7R0FERCxDQTFCUTtFQWxDNEI7QUFxRXJDLCtDQUFrQixHQUFHO0FBQ3BCLE9BQUssS0FBTCxDQUFXLFlBQVgsQ0FBd0IsQ0FBeEI7Ozs7QUFEb0IsRUFyRWdCO0FBNEVyQywrQkFBVSxLQUFLO0FBQ2QsU0FBTyxLQUFLLEtBQUwsQ0FBVyxRQUFYLENBQW9CLE9BQXBCLENBQTRCLEdBQTVCLElBQW1DLENBQUMsQ0FBRCxDQUQ1QjtFQTVFc0I7Q0FBbEIsQ0FBaEI7O0FBbUZKLE9BQU8sT0FBUCxHQUFpQixhQUFqQiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIoZnVuY3Rpb24oKSB7XG5cdGlmKHdpbmRvdy5BcHAuaXNGaXJzdExvZ2luICYmIHdpbmRvdy5BcHAuaXNGaXJzdExvZ2luID09PSAnWScpIHtcblx0XHRsZXQgUmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpLFxuXHRcdFx0UmVhY3RET00gPSByZXF1aXJlKCdyZWFjdC1kb20nKSxcblx0XHRcdFVzZXJJbmZvQWRkZXIgPSByZXF1aXJlKCcuL2NvbXBvbmVudHMvVXNlckluZm9BZGRlci5qc3gnKTtcblx0XHRcblx0XHRSZWFjdERPTS5yZW5kZXIoXG5cdFx0XHQ8VXNlckluZm9BZGRlciAvPixcblx0XHRcdGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdmaXJzdC10aW1lLWNvbnRhaW5lcicpLFxuXHRcdFx0KCkgPT4ge1xuXHRcdFx0XHQvL3NldENvb2tpZSgnaXNGaXJzdExvZ2luJywgJ04nKTtcblx0XHRcdFx0JCgnI2luZm9BZGRlcicpLm1vZGFsKCdzaG93Jyk7XG5cdFx0XHR9XG5cdFx0KTtcblx0fVxufSkoKTtcbiIsImxldCBSZWFjdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcblx0UmVhY3RET01cdFx0PSByZXF1aXJlKCdyZWFjdC1kb20nKSxcblx0Q29tbW9uQWN0aW9uc1x0PSByZXF1aXJlKCcuLi9hY3Rpb25zL0NvbW1vbkFjdGlvbnMnKSxcblx0U2VsZWN0RHJvcGRvd25cdD0gcmVxdWlyZSgnLi9TZWxlY3REcm9wZG93bi5qc3gnKSxcblx0VGV4dElucHV0XHRcdD0gcmVxdWlyZSgnLi9UZXh0SW5wdXQuanN4JyksXG5cdENoZWNrYm94R3JvdXBcdD0gcmVxdWlyZSgnLi9DaGVja2JveEdyb3VwLmpzeCcpLFxuXHRBcGlcdFx0XHRcdD0gcmVxdWlyZSgnLi4vdXRpbHMvYXBpJyksXG5cdGJpcnRoZGF0ZVx0XHQ9IHJlcXVpcmUoJy4uL3V0aWxzL2Zvcm1WYWxpZGF0b3JzJykuYmlydGhkYXRlO1xuXG5cbmNsYXNzIFNlbGVjdE9wdHMge1xuXHRjb25zdHJ1Y3RvcihsYWJlbCwgdmFsdWUsIGRhdGEpIHtcblx0XHR0aGlzLmxhYmVsID0gbGFiZWw7XG5cdFx0dGhpcy52YWx1ZSA9IHZhbHVlO1xuXHRcdHRoaXMuZGF0YSA9IGRhdGE7XG5cdH1cbn1cblxuZnVuY3Rpb24gZ2V0U2VsZWN0T3B0aW9ucyhvcHRDb2RlTWFwLCBjb2RlKSB7XG5cdGlmKG9wdENvZGVNYXBbY29kZV0pIHtcblx0XHRyZXR1cm4gKCBvcHRDb2RlTWFwW2NvZGVdLm1hcCgoZSkgPT4gbmV3IFNlbGVjdE9wdHMoZS5jZE5hbWUsIGUuY2QpKSApO1xuXHR9XG5cdC8vIFRPRE86IHdoZW4gbm8gbWFwIGZvciB0aGUgY29kZT9cblx0cmV0dXJuIFtdO1xufVxuXG5cbmxldCBVc2VySW5mb0FkZGVyID0gUmVhY3QuY3JlYXRlQ2xhc3Moe1xuXHRtaXhpbnM6IFtcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcXVlcnlTdG9yZU1peGluJyksXG5cdF0sXG5cdFxuICAgIHByb3BUeXBlczoge1xuXHRcdFxuICAgIH0sXG5cbiAgICBnZXREZWZhdWx0UHJvcHMoKSB7XG4gICAgICAgIHJldHVybiB7XG5cdFx0XHRcbiAgICAgICAgfTtcbiAgICB9LFxuXHRcblx0Z2V0SW5pdGlhbFN0YXRlKCkge1xuXHRcdHJldHVybiB7XG5cdFx0XHRvY2N1cGF0aW9uOiAnJyxcblx0XHRcdHNleDogJycsXG5cdFx0XHRtYWluQmFuazogJycsXG5cdFx0XHRiYW5rQ29kZTogJycsXG5cdFx0XHRkb2I6ICcnLFxuXHRcdFx0aW5jb21lOiAnJyxcblx0XHRcdHByb2R1Y3RJbnRlcmVzdDogW10sXG5cdFx0XHRob21lQ2l0eTogJycsXG5cdFx0XHRob21lVG93bjogJycsXG5cdFx0XHRob21lVG93bk9wdHM6IFtdLFxuXHRcdFx0aG9tZVRvd25QbGFjaG9sZGVyOiAn7IucL+q1sC/rj4QnXG5cdFx0fTtcbiAgICB9LFxuICAgIFxuXG4gICAgY29tcG9uZW50RGlkTW91bnQoKSB7XG5cdFx0Q29tbW9uQWN0aW9ucy5mZXRjaE9wdGlvbnMoW1xuXHRcdFx0NzAwMSwgNzAwMiwgNzAwM1xuXHRcdF0pO1xuXG5cdFx0QXBpLnBvc3QoJy9tb3BpYy9hcGkvbWVtL3NlbGVjdFNpZ3VuZ3VJbmZvJywge30pXG5cdFx0XHQudGhlbihmdW5jdGlvbiAocmVzKSB7XG5cdFx0XHRcdGlmKHJlcy5yZXN1bHQucmVzdWx0Q29kZSA9PT0gJ1NVQ0NFU1MnKSB7XG5cdFx0XHRcdFx0Q29tbW9uQWN0aW9ucy5hZGRPcHRpb25zKHsgc2lkb09wdHM6IHJlcy5yZXN1bHQuZGF0YSB9KTtcblx0XHRcdFx0fVxuXHRcdFx0fS5iaW5kKHRoaXMpKTtcbiAgICB9LFxuXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG5cdFx0XG4gICAgfSxcblxuICAgIHJlbmRlcigpIHtcblx0XHRsZXQge1xuXHRcdFx0cXVlcnlTdGF0ZSxcblx0XHRcdG9jY3VwYXRpb24sXG5cdFx0XHRzZXgsXG5cdFx0XHRtYWluQmFuayxcblx0XHRcdGJhbmtDb2RlLFxuXHRcdFx0ZG9iLFxuXHRcdFx0aW5jb21lLFxuXHRcdFx0cHJvZHVjdEludGVyZXN0LFxuXHRcdFx0aG9tZUNpdHksXG5cdFx0XHRob21lVG93bixcblx0XHRcdGhvbWVUb3duT3B0cyxcblx0XHRcdGhvbWVUb3duUGxhY2hvbGRlclxuXHRcdH0gPSB0aGlzLnN0YXRlO1xuXG5cdFx0bGV0IHsgb3B0Q29kZU1hcCB9ID0gcXVlcnlTdGF0ZTtcblxuXG5cdFx0bGV0IHNpZG9PcHRzID0gW107XG5cdFx0aWYob3B0Q29kZU1hcFsnc2lkb09wdHMnXSkge1xuXHRcdFx0c2lkb09wdHMgPSBvcHRDb2RlTWFwWydzaWRvT3B0cyddLm1hcCgoZWwpID0+IG5ldyBTZWxlY3RPcHRzKGVsLnNpZG9fbmFtZSwgZWwuc2lkbywgZWwpKTtcblx0XHR9XG5cblx0XHRcblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtb2RhbCBmYWRlXCIgaWQ9XCJpbmZvQWRkZXJcIiB0YWJJbmRleD17LTF9IHJvbGU9XCJkaWFsb2dcIiBhcmlhLWxhYmVsbGVkYnk9XCJteU1vZGFsTGFiZWxcIiBhcmlhLWhpZGRlbj1cInRydWVcIj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtb2RhbC1kaWFsb2dcIj5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWNvbnRlbnQgbW9kYWwtc21hbGxcIj5cblx0XHRcdFx0XHRcdDxhIGhyZWYgY2xhc3NOYW1lPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiIGFyaWEtbGFiZWw9XCJDbG9zZVwiIC8+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWhlYWRlclwiPlxuXHRcdFx0XHRcdFx0XHQ8aDQgY2xhc3NOYW1lPVwibW9kYWwtdGl0bGVcIiBpZD1cIm15TW9kYWxMYWJlbFwiPjxpbWcgc3JjPVwiL3Jlc291cmNlcy9pbWFnZXMvY29tbW9uL2xvZ29fYmx1ZS5wbmdcIiBhbHQ9XCJtb3BpY1wiIHdpZHRoPVwiMTEwXCIgLz48L2g0PlxuXHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWJvZHlcIj5cblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0X2FkZFwiPlxuXHRcdFx0XHRcdFx0XHRcdDxwIGNsYXNzTmFtZT1cInR4dFwiPuy2lOqwgOygleuztOulvCDsnoXroKXtlZjsi5zrqbQsIOyeheugpe2VmOyLoCDsoJXrs7Trpbwg6riw67CY7Jy866GcIOuNlCDsoovsnYAg7IOB7ZKI7J2EIOy2lOyynCDtlbTrk5zrpr3ri4jri6QuPC9wPlxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy01XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxsYWJlbCBodG1sRm9yPVwic2VsZWN0XCIgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+7KeB7JeFPC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9eyfshKDtg50nfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdG9wdGlvbnM9e2dldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgNzAwMSl9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlbGVjdC5iaW5kKG51bGwsICdvY2N1cGF0aW9uJyl9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e29jY3VwYXRpb259XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtmYWxzZX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0Lz5cblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNSBjb2wteHMtb2Zmc2V0LTFcIj5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PGxhYmVsIGh0bWxGb3I9XCJzZWxlY3RcIiBjbGFzc05hbWU9XCJzZWxlY3QtbGFiZWxcIj7sg53rhYTsm5Tsnbw8L2xhYmVsPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8VGV4dElucHV0XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dHlwZT0nbnVtYmVyJ1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXsnWVlNTUREJ31cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRpbnB1dENsYXNzPSd0ZXh0LWlucHV0LW5vcm1hbCdcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRtYXhsZW5ndGg9JzYnXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZVRleHRDaGFuZ2UuYmluZChudWxsLCAnZG9iJyl9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dmFsdWU9e2RvYn1cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e2JpcnRoZGF0ZX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0Lz5cblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy01XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxsYWJlbCBodG1sRm9yPVwic2VsZWN0XCIgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+7ISx67OEPC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9eyfshKDtg50nfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdG9wdGlvbnM9e2dldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgMTIzNCl9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlbGVjdC5iaW5kKG51bGwsICdzZXgnKX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzZWxlY3RlZD17c2V4fVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbj17ZmFsc2V9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdC8+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTUgY29sLXhzLW9mZnNldC0xIGluY29tZVwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGFiZWwgaHRtbEZvcj1cInNlbGVjdFwiIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuqwgOyymOu2hOyGjOuTnSjsm5QpPC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PFRleHRJbnB1dFxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHR5cGU9J251bWJlci13aXRoLWNvbW1hcydcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj17JyAnfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGlucHV0Q2xhc3M9J3RleHQtaW5wdXQtbm9ybWFsJ1xuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGhhbmRsZUNoYW5nZT17dGhpcy5oYW5kbGVUZXh0Q2hhbmdlLmJpbmQobnVsbCwgJ2luY29tZScpfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZhbHVlPXtpbmNvbWV9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dW5pdD17J+unjOybkCd9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtmYWxzZX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0Lz5cblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy01XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxsYWJlbCBodG1sRm9yPVwic2VsZWN0XCIgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+7KO86rGw656Y7J2A7ZaJPC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9eyfshKDtg50nfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdG9wdGlvbnM9e2dldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgNzAwMil9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlbGVjdC5iaW5kKG51bGwsICdiYW5rQ29kZScpfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHNlbGVjdGVkPXtiYW5rQ29kZX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e2ZhbHNlfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHQvPlxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy01IGNvbC14cy1vZmZzZXQtMVwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGFiZWwgaHRtbEZvcj1cInNlbGVjdFwiIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuqxsOyjvOyngOyXrTwvbGFiZWw+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxTZWxlY3REcm9wZG93blxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXsn7IucL+uPhCd9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0b3B0aW9ucz17c2lkb09wdHN9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNpZG9TZWxlY3QuYmluZChudWxsLCBzaWRvT3B0cyl9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0c2VsZWN0ZWQ9e2hvbWVDaXR5fVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbj17ZmFsc2V9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdC8+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdFx0XHR7Lyo8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNVwiIC8+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy01IGNvbC14cy1vZmZzZXQtMVwiPlxuXHRcdFx0XHRcdFx0XHRcdFx0XHQ8bGFiZWwgaHRtbEZvcj1cInNlbGVjdFwiIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiIC8+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxTZWxlY3REcm9wZG93blxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHBsYWNlaG9sZGVyPXtob21lVG93blBsYWNob2xkZXJ9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0b3B0aW9ucz17aG9tZVRvd25PcHRzfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdGhhbmRsZVNlbGVjdD17dGhpcy5oYW5kbGVTZWxlY3QuYmluZChudWxsLCAnaG9tZVRvd24nKX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRzZWxlY3RlZD17aG9tZVRvd259XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtmYWxzZX1cblx0XHRcdFx0XHRcdFx0XHRcdFx0Lz5cblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdDwvZGl2PiovfVxuXHRcdFx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy0yXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHRcdDxsYWJlbCBodG1sRm9yPVwic2VsZWN0XCIgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+6rSA7Ius67aE7JW8PC9sYWJlbD5cblx0XHRcdFx0XHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtMTBcIj5cblx0XHRcdFx0XHRcdFx0XHRcdFx0PENoZWNrYm94R3JvdXBcblx0XHRcdFx0XHRcdFx0XHRcdFx0XHRvcHRpb25zPXtnZXRTZWxlY3RPcHRpb25zKG9wdENvZGVNYXAsIDcwMDMpfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHRcdHNlbGVjdGVkPXtwcm9kdWN0SW50ZXJlc3R9XG5cdFx0XHRcdFx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZUNoZWNrYm94VG9nZ2xlLmJpbmQobnVsbCwgJ3Byb2R1Y3RJbnRlcmVzdCcpfVxuXHRcdFx0XHRcdFx0XHRcdFx0XHQvPlxuXHRcdFx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjZW50ZXJfYnRuXCIgb25DbGljaz17dGhpcy5zdWJtaXRGb3JtfT48YSBjbGFzc05hbWU9XCJidG4gYnRuLXN1Ym1pdFwiIHJvbGU9XCJidXR0b25cIj7tmZXsnbg8L2E+PC9kaXY+XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQpO1xuICAgIH0sXG5cblx0aGFuZGxlVGV4dENoYW5nZShhdHRyLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblx0XHRcblx0XHRsZXQgdmFsID0gdGFyZ2V0LnZhbHVlO1xuXG5cdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRbYXR0cl06IHZhbFxuXHRcdH0pO1xuXHR9LFxuXG5cdGhhbmRsZVNlbGVjdChhdHRyLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblxuXHRcdGxldCB2YWwgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpIHx8IHRhcmdldC50ZXh0Q29udGVudDtcblxuXHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0W2F0dHJdOiB2YWxcblx0XHR9KTtcblx0fSxcblxuXHRoYW5kbGVTaWRvU2VsZWN0KHNpZG9PcHRzLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblxuXHRcdGxldCB2YWwgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpIHx8IHRhcmdldC50ZXh0Q29udGVudDtcblxuXHRcdGxldCBzaWd1bmRvT3B0cyA9IHNpZG9PcHRzLmZpbmQoKGUpID0+IGUudmFsdWUgPT09IHZhbCkuZGF0YS5zaWd1bmd1X25hbWU7XG5cdFx0c2lndW5kb09wdHMgPSBKU09OLnBhcnNlKGB7JHtzaWd1bmRvT3B0c319YCk7XG5cblx0XHRsZXQgaG9tZVRvd25PcHRzID0gW107XG5cdFx0Zm9yKGxldCBrZXkgaW4gc2lndW5kb09wdHMpIHtcblx0XHRcdGhvbWVUb3duT3B0cy5wdXNoKG5ldyBTZWxlY3RPcHRzKHNpZ3VuZG9PcHRzW2tleV0sIGtleSkpO1xuXHRcdH1cblxuXHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0aG9tZUNpdHk6IHZhbCxcblx0XHRcdGhvbWVUb3duOiAnJyxcblx0XHRcdGhvbWVUb3duUGxhY2hvbGRlcjogJ+yEoO2DnScsXG5cdFx0XHRob21lVG93bk9wdHNcblx0XHR9KTtcblx0fSxcblxuXHRoYW5kbGVDaGVja2JveFRvZ2dsZShhdHRyLCBldnQpIHtcblx0XHRsZXQgc2VsZWN0ZWQgPSB0aGlzLnN0YXRlW2F0dHJdO1xuXG5cdFx0ZXZ0ID0gZXZ0PyBldnQgOiB3aW5kb3cuZXZlbnQ7XG5cdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XG5cblx0XHRsZXQgdmFsID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ1tkYXRhLXZhbF0nKS5hdHRyKCdkYXRhLXZhbCcpO1xuXHRcdGxldCBpZHggPSBzZWxlY3RlZC5pbmRleE9mKHZhbCk7XG5cblx0XHRpZihpZHggPiAtMSkge1xuXHRcdFx0c2VsZWN0ZWQuc3BsaWNlKGlkeCwgMSk7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHNlbGVjdGVkLnB1c2godmFsKTtcblx0XHR9XG5cblx0XHRcblx0XHR0aGlzLnNldFN0YXRlKHtcblx0XHRcdFthdHRyXTogc2VsZWN0ZWRcblx0XHR9KTtcblx0fSxcblxuXHRzdWJtaXRGb3JtKGV2dCkge1xuXHRcdGlmKGJpcnRoZGF0ZSh0aGlzLnN0YXRlLmRvYikgPT09IHRydWUpIHtcblx0XHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdFx0bGV0IHRhcmdldCA9IGV2dC50YXJnZXQgfHwgZXZ0LnNyY0VsZW1lbnQ7XG5cblx0XHRcdGxldCBkYXRhID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5zdGF0ZSwge1xuXHRcdFx0XHRwcm9kdWN0SW50ZXJlc3Q6IHRoaXMuc3RhdGUucHJvZHVjdEludGVyZXN0LmpvaW4oJywnKVxuXHRcdFx0fSk7XG5cblx0XHRcdGRlbGV0ZSBkYXRhLmhvbWVUb3duT3B0cztcblx0XHRcdGRlbGV0ZSBkYXRhLmhvbWVUb3duUGxhY2hvbGRlcjtcblxuXHRcdFx0QXBpLnBvc3QoJy9tb3BpYy9hcGkvdXBkYXRlVXNlckluZm8nLCBkYXRhKVxuXHRcdFx0XHQudGhlbihmdW5jdGlvbiAocmVzKSB7XG5cdFx0XHRcdFx0aWYocmVzLnJlc3VsdC5yZXN1bHRDb2RlID09PSAnU1VDQ0VTUycpIHtcblx0XHRcdFx0XHRcdCQodGFyZ2V0KS5jbG9zZXN0KCcubW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fS5iaW5kKHRoaXMpKTtcblx0XHR9XG5cdH1cblxuXHRcbn0pO1xuXG5cbm1vZHVsZS5leHBvcnRzID0gVXNlckluZm9BZGRlcjtcbiIsImxldCBSZWFjdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0JyksXG5cdFJlYWN0RE9NXHQ9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG52YXIgQ2hlY2tib3hHcm91cCA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcblx0cHJvcFR5cGVzOiB7XG5cdFx0aGFuZGxlU2VsZWN0OiBSZWFjdC5Qcm9wVHlwZXMuZnVuYyxcblx0XHRvcHRpb25zOiBSZWFjdC5Qcm9wVHlwZXMuYXJyYXksXG5cdFx0c2VsZWN0ZWQ6IFJlYWN0LlByb3BUeXBlcy5hcnJheSxcblx0XHQvL2Rpc2FibGVkOiBSZWFjdC5Qcm9wVHlwZXMuYm9vbCxcblx0XHQvL2NsYXNzTmFtZTogUmVhY3QuUHJvcFR5cGVzLnN0cmluZyxcblx0XHQvL3RhYkluZGV4OiBSZWFjdC5Qcm9wVHlwZXMubnVtYmVyXG5cdH0sXG5cblx0Z2V0RGVmYXVsdFByb3BzKCkge1xuXHRcdHJldHVybiB7XG5cdFx0XHRvcHRpb25zOiBbJy0nXSxcblx0XHRcdHNlbGVjdGVkOiBbXSxcblx0XHRcdC8vZGlzYWJsZWQ6IGZhbHNlLFxuXHRcdFx0Ly9jbGFzc05hbWU6ICdyYWRpby1ncm91cCcsXG5cdFx0XHQvL3RhYkluZGV4OiAwXG5cdFx0fTtcblx0fSxcblxuXHRnZXRJbml0aWFsU3RhdGUoKSB7XG5cdFx0cmV0dXJuIHtcblxuXHRcdH07XG5cdH0sXG5cdFxuXHRjb21wb25lbnREaWRNb3VudCgpIHtcblxuXHR9LFxuXG5cdGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuXG5cdH0sXG5cblx0cmVuZGVyKCkge1xuXHRcdGxldCB7XG5cdFx0XHRvcHRpb25zLFxuXHRcdFx0c2VsZWN0ZWQsXG5cdFx0XHQvL2Rpc2FibGVkLFxuXHRcdFx0Y2xhc3NOYW1lLFxuXHRcdFx0Ly90YWJJbmRleFxuXHRcdH0gPSB0aGlzLnByb3BzO1xuXG5cblx0XHRsZXQgb3B0aW9uc0VsZW0gPSBvcHRpb25zLm1hcCgoZWwsIGlkeCkgPT4ge1xuXHRcdFx0bGV0IHsgbGFiZWwsIHZhbHVlIH0gPSBlbDtcblx0XHRcdFxuXHRcdFx0cmV0dXJuIChcblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjaGVjay1ib3R0b21cIlxuXHRcdFx0XHRcdGtleT17YGNoZWNrJHtpZHh9YH1cblx0XHRcdFx0XHRkYXRhLXZhbD17KGVsLnZhbHVlID09PSAwID8gMCA6ICh2YWx1ZSB8fCAnJykpfT5cblx0XHRcdFx0XHQ8c3Bhbj5cblx0XHRcdFx0XHRcdDxpbnB1dCB0eXBlPVwiY2hlY2tib3hcIiBpZD17dmFsdWV9IGNoZWNrZWQ9e3RoaXMuaXNDaGVja2VkKHZhbHVlKX0gb25DaGFuZ2U9e3RoaXMuaGFuZGxlT3B0aW9uQ2xpY2t9Lz5cblx0XHRcdFx0XHRcdDxsYWJlbCBodG1sRm9yPXt2YWx1ZX0+PGkgLz57bGFiZWx9PC9sYWJlbD5cblx0XHRcdFx0XHQ8L3NwYW4+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0KTtcblx0XHR9KTtcblx0XHRcblx0XHRcblx0XHRyZXR1cm4gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjaGVjay10b3BcIj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjaGVjay10b3AxXCI+XG5cdFx0XHRcdFx0e29wdGlvbnNFbGVtfVxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdCk7XG5cdH0sXG5cblx0aGFuZGxlT3B0aW9uQ2xpY2soZSkge1xuXHRcdHRoaXMucHJvcHMuaGFuZGxlU2VsZWN0KGUpO1xuXHRcdC8vdGhpcy5zZXRTdGF0ZSh7XG5cdFx0Ly9cdHRhYkluZGV4OiBudWxsXG5cdFx0Ly99KTtcblx0fSxcblxuXHRpc0NoZWNrZWQodmFsKSB7XG5cdFx0cmV0dXJuIHRoaXMucHJvcHMuc2VsZWN0ZWQuaW5kZXhPZih2YWwpID4gLTE7XG5cdH1cblxuXHRcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IENoZWNrYm94R3JvdXA7XG4iXX0=
