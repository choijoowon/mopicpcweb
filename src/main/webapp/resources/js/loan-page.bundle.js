require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({233:[function(require,module,exports){
'use strict';

(function () {

	var React = require('react'),
	    ReactDOM = require('react-dom'),
	    QueryStore = require('../common/stores/QueryStore'),
	    ProductStore = require('../common/stores/ProductStore'),
	    ComparisonStore = require('../common/stores/ComparisonStore'),
	    CommonActions = require('../common/actions/CommonActions'),
	    getSearchCallback = require('../common/utils/getSearchCallback'),
	    LoanHelperSection = require('./components/LoanHelperSection.jsx'),
	    LoanResultSection = require('./components/LoanResultSection.jsx'),
	    LoanProductAdder = require('./components/LoanProductAdder.jsx');

	var mainmenu = undefined,
	    submenu = undefined,
	    isLoggedIn = undefined,
	    searchQuery = undefined;

	if (window.App) {
		var _window$App$query = window.App.query;
		mainmenu = _window$App$query.mainmenu;
		submenu = _window$App$query.submenu;
		searchQuery = _window$App$query.searchQuery;

		isLoggedIn = window.App.comparisons.isLoggedIn;

		QueryStore.rehydrate('query');
		ProductStore.rehydrate('results');
		ComparisonStore.rehydrate('comparisons');
	}

	//    CommonActions.fetchOptions([
	//		'B0001041'
	//	]);

	if (isLoggedIn) {
		CommonActions.fetchMyProducts(mainmenu, submenu);
	}

	var productAdder = React.createElement(LoanProductAdder, null);

	var searchCallback = getSearchCallback(searchQuery); // if searchQuery given in advance, search right away

	ReactDOM.render(React.createElement(LoanHelperSection, {
		adderModal: productAdder
	}), document.getElementById('helper'), searchCallback);

	ReactDOM.render(React.createElement(LoanResultSection, null), document.getElementById('results'));
})();

},{"../common/actions/CommonActions":186,"../common/stores/ComparisonStore":208,"../common/stores/ProductStore":209,"../common/stores/QueryStore":210,"../common/utils/getSearchCallback":216,"./components/LoanHelperSection.jsx":230,"./components/LoanProductAdder.jsx":231,"./components/LoanResultSection.jsx":232,"react":177,"react-dom":20}],232:[function(require,module,exports){
'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    ProductCard = require('../../common/components/ProductCard.jsx'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas;

var ListOpts = function ListOpts(label, value) {
	_classCallCheck(this, ListOpts);

	this.label = label;
	this.dataAttrs = { value: value };
};

function getLoanFilterList() {
	return [new ListOpts('전체', '99999999'), new ListOpts('1금융권', 'FIRST'), new ListOpts('저축은행', 'SECOND'), new ListOpts('기타', 'ELSE')];
}

function amountWithUnit(amount) {
	var parsed = '';
	if (amount >= 10000) {
		parsed += parseInt(amount / 10000) + '억';
	}
	if (amount % 10000 !== 0) {
		parsed += amount % 10000 + '만';
	}
	parsed += '원';

	return parsed;
}

var LoanResultSection = React.createClass({
	displayName: 'LoanResultSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin'), require('../../common/utils/resultSectionMixin.jsx')],

	propTypes: {},

	getDefaultProps: function getDefaultProps() {
		return {};
	},

	getInitialState: function getInitialState() {
		return {};
	},

	componentDidMount: function componentDidMount() {},


	componentWillUnmount: function componentWillUnmount() {},

	_getOptions: function _getOptions(submenu, optCodeMap) {
		var _this = this;

		//  returns {filterOptions, sortOptions, tooltipElem, resultsElem, helperElem}
		//
		//    tooltipElem -> tooltip element to go in sorting section
		//    resultsElem -> list of filled in results elements (ProductCard's)
		//    helperElem  -> (if needed) modal to use or any other element

		var _state = this.state;
		var resultState = _state.resultState;
		var comparisonState = _state.comparisonState;
		var isCmprTabActive = comparisonState.isCmprTabActive;
		var isLoggedIn = comparisonState.isLoggedIn;
		var comparisonGroup = comparisonState.comparisonGroup;
		var currentCmpr = comparisonState.currentCmpr;

		var showingCmpr = isLoggedIn && isCmprTabActive && comparisonGroup.length > 0;
		var selectedCmpr = comparisonGroup[currentCmpr];

		var filterOptions = undefined,
		    sortOptions = undefined,
		    tooltipElem = undefined,
		    resultsElem = undefined,
		    helperElem = undefined;
		var comparisonContents = undefined;

		switch (submenu.toLowerCase()) {
			// 신용대출
			case 'signature':
			// 담보대출
			case 'secured':
				filterOptions = getLoanFilterList();
				filterOptions = this._addNumResults(filterOptions);

				sortOptions = [new ListOpts('저금리순', 'RATE'), new ListOpts('최고한도순', 'AMOUNT'), new ListOpts('최장기간순', 'TERM')]; // TODO: modify this to real code
				if (submenu.toLowerCase() === 'secured') {
					sortOptions.push(new ListOpts('담보인정비율순', 'LN_RATE'));
				}

				tooltipElem = undefined;

				resultsElem = resultState.productData.map(function (elem, idx) {
					var LA_CODE = //관심상품 여부

					// isNew?
					elem.LA_CODE;
					var // 상품코드
					LN_FN_CODE_NM = elem.LN_FN_CODE_NM;
					var // 카테고리 (1금융권, 2금융권, ...),
					BANK_IMG = elem.BANK_IMG;
					var LN_RATE_LOW = elem.LN_RATE_LOW;
					var //최저연이율

					LN_NM = elem.LN_NM;
					var // 상품명
					LN_SUMMARY1 = elem.LN_SUMMARY1;
					var //상품설명 1
					LN_SUMMARY2 = elem.LN_SUMMARY2;
					var //상품설명 2
					LN_MINIMUM = elem.LN_MINIMUM;
					var //대출금액 최소
					LN_MAXIMUM = elem.LN_MAXIMUM;
					var //대출금액 최대
					LN_NOMAL_DATE_NM = elem.LN_NOMAL_DATE_NM;
					var //(최대)대출기간

					LN_CLASS1_NM = elem.LN_CLASS1_NM;
					var //신용등급1
					LN_CLASS2_NM = elem.LN_CLASS2_NM;
					var //신용등급2
					LN_RATE = elem.LN_RATE;
					var //담보인정비율

					LN_IS_YN = elem.LN_IS_YN;
					var // 온라인가입 여부

					interest_yn = elem.interest_yn;
					var detail_url = elem.detail_url;


					var submenuCode = undefined;
					var mainAttrSet = {
						'금액': LN_MINIMUM !== 0 ? amountWithUnit(LN_MINIMUM) + ' ~ ' + amountWithUnit(LN_MAXIMUM) : LN_MAXIMUM !== 0 ? '최고 ' + amountWithUnit(LN_MAXIMUM) : '자세히보기',
						'기간': LN_NOMAL_DATE_NM ? '최대 ' + LN_NOMAL_DATE_NM : '-'
					};

					switch (submenu.toLowerCase()) {
						case 'signature':
							submenuCode = 2100;
							mainAttrSet['신용등급'] = LN_CLASS1_NM + ' ~ ' + LN_CLASS2_NM;
							break;
						case 'secured':
							submenuCode = 2200;
							mainAttrSet['담보인정비율'] = LN_RATE ? LN_RATE + ' %' : '-';
							break;
						default:
					}

					var features = [];
					if (LN_IS_YN === 'Y') {
						features.push('<i class="banking-icon internet"></i><span>인터넷가입</span>');
					}

					if (showingCmpr) {
						comparisonContents = [{
							attr: '최저금리기준 연',
							diff: LN_RATE_LOW - selectedCmpr.ln_rate_low,
							unit: '%'
						}];
					}

					var isInterested = interest_yn === 'Y'; // 관심상품?

					return React.createElement(ProductCard, {
						key: 'loan' + LA_CODE,
						type: 'type1',
						category: LN_FN_CODE_NM,
						provider: BANK_IMG,
						leadingInfo: LN_RATE_LOW,
						prefix: '연',
						unit: '%~',
						productTitle: LN_NM,
						features: features,
						description: LN_SUMMARY1 + '<br />' + LN_SUMMARY2,
						mainAttrSet: mainAttrSet,
						targetModalSelector: '#product_view_popup',
						detailUrl: detail_url,
						comparisonContents: comparisonContents,
						isInterested: isInterested,
						index: idx,
						handleInterestClick: _this.handleInterestClick.bind(null, isInterested, submenuCode, LA_CODE, idx),
						choiceReverse: true
					});
				});

				break;

			default:
		}

		return { filterOptions: filterOptions, sortOptions: sortOptions, tooltipElem: tooltipElem, resultsElem: resultsElem, helperElem: helperElem };
	},
	getPreSearchView: function getPreSearchView(submenu) {
		var preSearchViewElem = undefined;

		switch (submenu.toLowerCase()) {
			case 'shilson':
				break;
			case 'car':
				break;
			default:

		}

		return preSearchViewElem;
	}
});

module.exports = LoanResultSection;

},{"../../common/components/ProductCard.jsx":193,"../../common/utils/comparisonStoreMixin":214,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/resultSectionMixin.jsx":221,"../../common/utils/sendQueryMixin":222,"react":177}],231:[function(require,module,exports){
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var React = require('react'),
    SelectDropdown = require('../../common/components/SelectDropdown.jsx'),
    TextInput = require('../../common/components/TextInput.jsx'),
    Api = require('../../common/utils/api'),
    CommonActions = require('../../common/actions/CommonActions'),
    formValidators = require('../../common/utils/formValidators');

var selectRequired = formValidators.selectRequired;
var numberRequired = formValidators.numberRequired;


var submenu_codes = require('../../common/constants/submenu_codes');

var loanMenuCodes = {
	signature: 'B000101',
	secured: 'B000102'
};

var termOpts = [{
	value: 'B00010901',
	label: '6개월'
}, {
	value: 'B00010902',
	label: '1년'
}, {
	value: 'B00010903',
	label: '3년'
}, {
	value: 'B00010904',
	label: '5년'
}, {
	value: 'B00010905',
	label: '7년'
}, {
	value: 'B00010906',
	label: '10년'
}, {
	value: 'B00010907',
	label: '15년'
}, {
	value: 'B00010908',
	label: '20년'
}, {
	value: 'B00010909',
	label: '25년'
}, {
	value: 'B00010910',
	label: '30년'
}];

function convertTermCode(termCd) {
	var termKey = undefined,
	    termNum = undefined;

	switch (termCd) {
		case 'A0001010101':
			termKey = 'DS_RATE3';
			termNum = '3';
			break;
		case 'A0001010102':
			termKey = 'DS_RATE6';
			termNum = '6';
			break;
		case 'A0001010103':
			termKey = 'DS_RATE12';
			termNum = '12';
			break;
		case 'A0001010104':
			termKey = 'DS_RATE24';
			termNum = '24';
			break;
		case 'A0001010105':
			termKey = 'DS_RATE36';
			termNum = '36';
			break;
		default:
			termKey = 'DS_PRE_TAX';
			termNum = '0';
	}

	return { termKey: termKey, termNum: termNum };
}

var SelectOpts = function SelectOpts(label, value, data) {
	_classCallCheck(this, SelectOpts);

	this.label = label;
	this.value = value;
	this.data = data;
};

var LoanProductAdder = React.createClass({
	displayName: 'LoanProductAdder',

	propTypes: {
		submenu: React.PropTypes.string,
		searchEntries: React.PropTypes.array,
		optCodeMap: React.PropTypes.object
	},

	getDefaultProps: function getDefaultProps() {
		return {
			submenu: '',
			searchEntries: [],
			optCodeMap: {}
		};
	},


	getInitialState: function getInitialState() {
		var placeholder = undefined;

		if (this.props.submenu.toLowerCase() === 'secured') {
			placeholder = '담보유형과 기관명을 선택해주세요.';
		} else {
			placeholder = '기관명을 선택해주세요.';
		}

		return {
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			secureType: '',
			companyOpts: [],
			prodNameOpts: [],
			repayType: '',

			prodNamePlaceholder: placeholder
		};
	},

	componentDidMount: function componentDidMount() {
		var category = loanMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/loan/selectLoanBankList', { category: category }).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				var resultData = res.result.data;
				var options = resultData.map(function (e) {
					return new SelectOpts(e.cdName, e.cd);
				});

				this.setState({
					companyOpts: options
				});
			}
		}.bind(this));

		// clear when closed
		$('#product_add_popup').on('hidden.bs.modal', this.clearStats);
	},
	componentWillUnmount: function componentWillUnmount() {},
	render: function render() {
		var _state = this.state;
		var showValidation = _state.showValidation;
		var companyOpts = _state.companyOpts;
		var company_code = _state.company_code;
		var prodNameOpts = _state.prodNameOpts;
		var interest = _state.interest;
		var secureType = _state.secureType;
		var prod_code = _state.prod_code;
		var term = _state.term;
		var amount = _state.amount;
		var prodNamePlaceholder = _state.prodNamePlaceholder;
		var _props = this.props;
		var submenu = _props.submenu;
		var searchEntries = _props.searchEntries;
		var optCodeMap = _props.optCodeMap;


		var securityForm = undefined;
		if (submenu.toLowerCase() === 'secured') {
			var securityEntry = this.props.searchEntries.find(function (e) {
				return e.id === 'secureType';
			});

			securityForm = React.createElement(
				'div',
				{ className: 'row' },
				React.createElement(
					'div',
					{ className: 'col-xs-6' },
					React.createElement(
						'label',
						{ className: 'select-label' },
						securityEntry.label || ''
					),
					React.createElement(SelectDropdown, {
						tabIndex: 0,
						placeholder: securityEntry.placeholder || '선택',
						options: securityEntry.options,
						handleSelect: this.handleSecuritySelect,
						className: securityEntry.className || 'select-dropdown',
						selected: secureType,
						validationFn: showValidation && securityEntry.validationFn
					})
				)
			);
		}

		var companyForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'기관명'
				),
				React.createElement(SelectDropdown, {
					placeholder: '선택',
					options: companyOpts,
					handleSelect: this.handleCompanySelect,
					selected: company_code,
					validationFn: showValidation && selectRequired
				})
			)
		);

		var productNameForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'상품명'
				),
				React.createElement(SelectDropdown, {
					placeholder: prodNamePlaceholder,
					options: prodNameOpts,
					handleSelect: this.handleProductNameSelect,
					selected: prod_code,
					validationFn: showValidation && selectRequired
				})
			)
		);

		var termForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'대출기간'
				),
				React.createElement(SelectDropdown, {
					placeholder: '선택',
					options: termOpts,
					handleSelect: this.handleSelect.bind(null, 'term'),
					className: 'select-dropdown',
					selected: term,
					validationFn: showValidation && selectRequired
				})
			)
		);

		var amountForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'대출금액'
				),
				React.createElement(TextInput, {
					type: 'number-with-commas',
					placeholder: ' ',
					inputClass: 'text-input-normal',
					handleChange: this.handleTextInputChange.bind(null, 'amount'),
					value: amount,
					unit: '만원',
					validationFn: showValidation && numberRequired
				})
			)
		);

		var interestForm = React.createElement(
			'div',
			{ className: 'row' },
			React.createElement(
				'div',
				{ className: 'col-xs-6' },
				React.createElement(
					'label',
					{ className: 'select-label' },
					'금리'
				),
				React.createElement(TextInput, {
					type: 'number',
					placeholder: ' ',
					inputClass: 'text-input-normal',
					handleChange: this.handleTextInputChange.bind(null, 'interest'),
					value: interest,
					unit: '%',
					validationFn: showValidation && numberRequired
				})
			)
		);

		return React.createElement(
			'div',
			{ className: 'modal fade', id: 'product_add_popup' },
			React.createElement(
				'div',
				{ className: 'modal-dialog' },
				React.createElement(
					'div',
					{ className: 'modal-content modal-small' },
					React.createElement('a', { className: 'close', 'data-dismiss': 'modal', 'aria-label': 'Close' }),
					React.createElement(
						'div',
						{ className: 'modal-header' },
						React.createElement(
							'h4',
							{ className: 'modal-title', id: 'myModalLabel' },
							'가입상품 추가하기'
						)
					),
					React.createElement(
						'div',
						{ className: 'modal-body' },
						React.createElement(
							'div',
							{ className: 'product_add' },
							securityForm,
							companyForm,
							productNameForm,
							termForm,
							amountForm,
							interestForm,
							React.createElement(
								'span',
								{ className: 'txt' },
								'(금리를 입력하지 않으실 경우, 등록일 현재 기준 금리로 적용됩니다.)'
							),
							React.createElement(
								'div',
								{ className: 'center_btn' },
								React.createElement(
									'a',
									{ className: 'btn btn-submit', role: 'button', onClick: this.submitProduct },
									'확인'
								)
							)
						)
					)
				)
			)
		);
	},
	handleSelect: function handleSelect(attr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState(_defineProperty({}, attr, val));
	},
	handleTextInputChange: function handleTextInputChange(attr, evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var val = target.value;

		this.setState(_defineProperty({}, attr, val));
	},
	handleProductNameSelect: function handleProductNameSelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var prodCd = $(target).closest('li').attr('data-val') || target.textContent;
		var interest = this.state.prodNameOpts.find(function (e) {
			return e.value === prodCd;
		}).data.LN_RATE_LOW;

		this.setState({
			prod_code: prodCd,
			interest: (interest || interest === 0) && String(interest) || ''
		});
	},
	handleSecuritySelect: function handleSecuritySelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var secureType = $(target).closest('li').attr('data-val') || target.textContent;
		var company_code = this.state.company_code;


		this.setState({
			secureType: secureType,
			prod_code: ''
		});

		if (company_code) {
			this.fetchProductNames(company_code, secureType);
		}
	},
	handleCompanySelect: function handleCompanySelect(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var lnBankingCode = $(target).closest('li').attr('data-val') || target.textContent;

		this.setState({
			company_code: lnBankingCode,
			prod_code: '',
			prodNameOpts: [],
			interest: ''
		});

		var secureType = this.state.secureType;


		if (secureType || this.props.submenu.toLowerCase() !== 'secured') {
			this.fetchProductNames(lnBankingCode, secureType);
		}
	},
	fetchProductNames: function fetchProductNames(company_code, secureType) {
		var lnCode = loanMenuCodes[this.props.submenu.toLowerCase()];

		Api.post('/mopic/api/loan/filterProductList', { lnBankingCode: company_code, lnCode: lnCode, secureType: secureType }).then(function (res) {
			if (res.result.resultCode === 'SUCCESS') {
				var resultData = res.result.data;

				if (resultData && resultData.length > 0) {
					var options = resultData.map(function (e) {
						return new SelectOpts(e.LN_NM, e.LA_CODE, e);
					});

					this.setState({
						prodNameOpts: options,
						prodNamePlaceholder: '선택'
					});
				} else {
					this.setState({
						prodNamePlaceholder: '해당 조건의 상품이 존재하지 않습니다'
					});
				}
			}
		}.bind(this));
	},
	clearStats: function clearStats() {
		// clear every state but company options
		this.setState({
			showValidation: false,
			company_code: '',
			prod_code: '',
			prod_name: '',
			amount: '',
			term: '',
			interest: '',
			secureType: '',
			prodNameOpts: [],
			prodNamePlaceholder: '담보유형과 기관명을 선택하세요.'
		});
	},
	checkValidity: function checkValidity() {
		var _state2 = this.state;
		var company_code = _state2.company_code;
		var prod_code = _state2.prod_code;
		var amount = _state2.amount;
		var term = _state2.term;
		var interest = _state2.interest;
		var secureType = _state2.secureType;


		var allValid = true;

		allValid = allValid && selectRequired(company_code) === true;
		allValid = allValid && selectRequired(prod_code) === true;
		allValid = allValid && selectRequired(term) === true;
		allValid = allValid && numberRequired(amount) === true;
		allValid = allValid && numberRequired(interest) === true;

		if (this.props.submenu.toLowerCase() === 'secured') {
			allValid = allValid && selectRequired(secureType) === true;
		}

		return allValid;
	},
	submitProduct: function submitProduct(evt) {
		evt = evt ? evt : window.event;
		var target = evt.target || evt.srcElement;

		var _state3 = this.state;
		var company_code = _state3.company_code;
		var prod_code = _state3.prod_code;
		var amount = _state3.amount;
		var term = _state3.term;
		var interest = _state3.interest;
		var secureType = _state3.secureType;
		var prodNameOpts = _state3.prodNameOpts;
		var _props2 = this.props;
		var submenu = _props2.submenu;
		var searchEntries = _props2.searchEntries;

		var data = undefined;

		var allValid = this.checkValidity();

		if (allValid) {
			(function () {
				var data = {};

				switch (submenu.toLowerCase()) {
					case 'secured':
						data.secureType = secureType;
					case 'signature':
						data = _extends({}, data, {
							product_type: String(submenu_codes[submenu]),
							prod_code: String(prod_code),
							prod_name: String(prodNameOpts.find(function (elem) {
								return elem.value === prod_code;
							}).label),
							amount: String(amount),
							term_code: String(term),
							interest: String(interest)
						});
						break;
				}

				var $target = $(target);
				CommonActions.addMyProduct(data, function () {
					$target.closest('.modal').modal('hide');
				});
			})();
		} else {
			this.setState({
				showValidation: true
			});
		}
	}
});

module.exports = LoanProductAdder;

},{"../../common/actions/CommonActions":186,"../../common/components/SelectDropdown.jsx":197,"../../common/components/TextInput.jsx":200,"../../common/constants/submenu_codes":204,"../../common/utils/api":211,"../../common/utils/formValidators":215,"react":177}],230:[function(require,module,exports){
'use strict';

var React = require('react'),
    numberWithCommas = require('../../common/utils/numberFilterFunctions').numberWithCommas,
    formValidators = require('../../common/utils/formValidators');

var selectRequired = formValidators.selectRequired;
var numberRequired = formValidators.numberRequired;


var LoanHelperSection = React.createClass({
	displayName: 'LoanHelperSection',

	mixins: [require('../../common/utils/queryStoreMixin'), require('../../common/utils/productStoreMixin'), require('../../common/utils/comparisonStoreMixin'), require('../../common/utils/sendQueryMixin'), require('../../common/utils/helperSectionMixin.jsx')],

	getDefaultProps: function getDefaultProps() {
		return {};
	},
	getInitialState: function getInitialState() {
		return {};
	},
	componentDidMount: function componentDidMount() {},
	componentWillUnmount: function componentWillUnmount() {},
	_getSearchEntries: function _getSearchEntries(queryState) {
		var submenu = queryState.submenu;
		var searchQuery = queryState.searchQuery;
		var optCodeMap = queryState.optCodeMap;

		var entries = undefined;

		switch (submenu.toLowerCase()) {
			case 'signature':
				entries = [{
					label: '대출금액',
					type: 'number-with-commas',
					id: 'amount',
					unit: '만원',
					validationFn: numberRequired
				}];
				break;
			case 'secured':
				entries = [{
					label: '담보유형',
					type: 'select',
					id: 'secureType',
					options: this.getSelectOptions(optCodeMap, 'B0001081'),
					validationFn: selectRequired
				}];
				break;
			default:
				entries = [];
		}

		return entries;
	},
	_getCmprCardContents: function _getCmprCardContents(submenu, comparisonGroup) {

		var cmprCardContents = [];

		switch (submenu.toLowerCase()) {
			case 'signature':
			case 'secured':
				cmprCardContents = comparisonGroup.map(function (elem, idx) {
					var ln_nm = //대출금액
					//이자
					elem.ln_nm;
					var //상품명
					ln_banking_nm = elem.ln_banking_nm;
					var //은행명
					interest = elem.interest;
					var //금리
					amount = elem.amount;


					return {
						title: ln_nm,
						info: [{
							attr: '기관명',
							value: ln_banking_nm,
							colSpan: 4,
							isProvider: true
						}, {
							attr: '금리',
							value: interest,
							colSpan: 4,
							prefix: '연',
							unit: '%',
							isLeadingInfo: true
						}, {
							attr: '대출금액',
							value: numberWithCommas(amount) + '만원',
							colSpan: 4
						}]
					};
				});

				// {
				// 	attr: '이자',
				// 	value: elem.dambo_shilson_name,
				// 	colSpan: 3
				// }
				break;

		}

		return cmprCardContents;
	}
});

module.exports = LoanHelperSection;

},{"../../common/utils/comparisonStoreMixin":214,"../../common/utils/formValidators":215,"../../common/utils/helperSectionMixin.jsx":217,"../../common/utils/numberFilterFunctions":218,"../../common/utils/productStoreMixin":219,"../../common/utils/queryStoreMixin":220,"../../common/utils/sendQueryMixin":222,"react":177}]},{},[233])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvbG9hbi1wYWdlL2xvYW4tcGFnZS5qc3giLCJzcmMvbG9hbi1wYWdlL2NvbXBvbmVudHMvTG9hblJlc3VsdFNlY3Rpb24uanN4Iiwic3JjL2xvYW4tcGFnZS9jb21wb25lbnRzL0xvYW5Qcm9kdWN0QWRkZXIuanN4Iiwic3JjL2xvYW4tcGFnZS9jb21wb25lbnRzL0xvYW5IZWxwZXJTZWN0aW9uLmpzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7O0FDQUEsQ0FBQyxZQUFXOztBQUVYLEtBQUksUUFBVyxRQUFRLE9BQVIsQ0FBWDtLQUNILFdBQWEsUUFBUSxXQUFSLENBQWI7S0FDQSxhQUFlLFFBQVEsNkJBQVIsQ0FBZjtLQUNBLGVBQWdCLFFBQVEsK0JBQVIsQ0FBaEI7S0FDQSxrQkFBbUIsUUFBUSxrQ0FBUixDQUFuQjtLQUNBLGdCQUFpQixRQUFRLGlDQUFSLENBQWpCO0tBQ0Esb0JBQW9CLFFBQVEsbUNBQVIsQ0FBcEI7S0FDQSxvQkFBb0IsUUFBUSxvQ0FBUixDQUFwQjtLQUNBLG9CQUFvQixRQUFRLG9DQUFSLENBQXBCO0tBQ0EsbUJBQW1CLFFBQVEsbUNBQVIsQ0FBbkIsQ0FYVTs7QUFjWCxLQUFJLG9CQUFKO0tBQWMsbUJBQWQ7S0FBdUIsc0JBQXZCO0tBQW1DLHVCQUFuQyxDQWRXOztBQWdCUixLQUFHLE9BQU8sR0FBUCxFQUFZOzBCQUNtQixPQUFPLEdBQVAsQ0FBVyxLQUFYLENBRG5CO0FBQ2Ysd0NBRGU7QUFDTCxzQ0FESztBQUNJLDhDQURKOztBQUVqQixlQUFhLE9BQU8sR0FBUCxDQUFXLFdBQVgsQ0FBdUIsVUFBdkIsQ0FGSTs7QUFJakIsYUFBVyxTQUFYLENBQXFCLE9BQXJCLEVBSmlCO0FBS2pCLGVBQWEsU0FBYixDQUF1QixTQUF2QixFQUxpQjtBQU1qQixrQkFBZ0IsU0FBaEIsQ0FBMEIsYUFBMUIsRUFOaUI7RUFBZjs7Ozs7O0FBaEJRLEtBK0JSLFVBQUgsRUFBZTtBQUNkLGdCQUFjLGVBQWQsQ0FBOEIsUUFBOUIsRUFBd0MsT0FBeEMsRUFEYztFQUFmOztBQUlBLEtBQUksZUFBZSxvQkFBQyxnQkFBRCxPQUFmLENBbkNPOztBQXFDWCxLQUFJLGlCQUFpQixrQkFBa0IsV0FBbEIsQ0FBakI7O0FBckNPLFNBd0NSLENBQVMsTUFBVCxDQUNGLG9CQUFDLGlCQUFEO0FBQ0MsY0FBWSxZQUFaO0VBREQsQ0FERSxFQUlGLFNBQVMsY0FBVCxDQUF3QixRQUF4QixDQUpFLEVBS0YsY0FMRSxFQXhDUTs7QUFnRFgsVUFBUyxNQUFULENBQ0Msb0JBQUMsaUJBQUQsT0FERCxFQUVDLFNBQVMsY0FBVCxDQUF3QixTQUF4QixDQUZELEVBaERXO0NBQVgsQ0FBRDs7Ozs7OztBQ0FBLElBQUksUUFBUSxRQUFRLE9BQVIsQ0FBUjtJQUNILGNBQWdCLFFBQVEseUNBQVIsQ0FBaEI7SUFDQSxtQkFBbUIsUUFBUSwwQ0FBUixFQUFvRCxnQkFBcEQ7O0lBR2QsV0FDTCxTQURLLFFBQ0wsQ0FBWSxLQUFaLEVBQW1CLEtBQW5CLEVBQTBCO3VCQURyQixVQUNxQjs7QUFDekIsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUR5QjtBQUV6QixNQUFLLFNBQUwsR0FBaUIsRUFBRSxPQUFPLEtBQVAsRUFBbkIsQ0FGeUI7Q0FBMUI7O0FBTUQsU0FBUyxpQkFBVCxHQUE2QjtBQUM1QixRQUFPLENBQ04sSUFBSSxRQUFKLENBQWEsSUFBYixFQUFtQixVQUFuQixDQURNLEVBRU4sSUFBSSxRQUFKLENBQWEsTUFBYixFQUFxQixPQUFyQixDQUZNLEVBR04sSUFBSSxRQUFKLENBQWEsTUFBYixFQUFxQixRQUFyQixDQUhNLEVBSU4sSUFBSSxRQUFKLENBQWEsSUFBYixFQUFtQixNQUFuQixDQUpNLENBQVAsQ0FENEI7Q0FBN0I7O0FBU0EsU0FBUyxjQUFULENBQXdCLE1BQXhCLEVBQWdDO0FBQy9CLEtBQUksU0FBUyxFQUFULENBRDJCO0FBRS9CLEtBQUcsVUFBVSxLQUFWLEVBQWlCO0FBQ25CLFlBQVcsU0FBUyxTQUFTLEtBQVQsQ0FBVCxHQUEyQixHQUEzQixDQURRO0VBQXBCO0FBR0EsS0FBRyxTQUFTLEtBQVQsS0FBbUIsQ0FBbkIsRUFBc0I7QUFDeEIsWUFBVyxNQUFDLEdBQVMsS0FBVCxHQUFrQixHQUFuQixDQURhO0VBQXpCO0FBR0EsV0FBVSxHQUFWLENBUitCOztBQVUvQixRQUFPLE1BQVAsQ0FWK0I7Q0FBaEM7O0FBZUEsSUFBSSxvQkFBb0IsTUFBTSxXQUFOLENBQWtCOzs7QUFDekMsU0FBUSxDQUNQLFFBQVEsb0NBQVIsQ0FETyxFQUVQLFFBQVEsc0NBQVIsQ0FGTyxFQUdQLFFBQVEseUNBQVIsQ0FITyxFQUlQLFFBQVEsbUNBQVIsQ0FKTyxFQUtQLFFBQVEsMkNBQVIsQ0FMTyxDQUFSOztBQVFHLFlBQVcsRUFBWDs7QUFJQSxrQkFBaUIsMkJBQVc7QUFDeEIsU0FBTyxFQUFQLENBRHdCO0VBQVg7O0FBTWpCLGtCQUFpQiwyQkFBVztBQUN4QixTQUFPLEVBQVAsQ0FEd0I7RUFBWDs7QUFNcEIsaURBQW9CLEVBekJxQjs7O0FBNkJ0Qyx1QkFBc0IsZ0NBQVcsRUFBWDs7QUFJekIsbUNBQVksU0FBUyxZQUFZOzs7Ozs7Ozs7ZUFPTyxLQUFLLEtBQUwsQ0FQUDtNQU8xQixpQ0FQMEI7TUFPYix5Q0FQYTtNQVMxQixrQkFBOEQsZ0JBQTlELGdCQVQwQjtNQVNULGFBQTZDLGdCQUE3QyxXQVRTO01BU0csa0JBQWlDLGdCQUFqQyxnQkFUSDtNQVNvQixjQUFnQixnQkFBaEIsWUFUcEI7O0FBVWhDLE1BQUksY0FBZSxjQUFjLGVBQWQsSUFBaUMsZ0JBQWdCLE1BQWhCLEdBQXlCLENBQXpCLENBVnBCO0FBV2hDLE1BQUksZUFBZSxnQkFBZ0IsV0FBaEIsQ0FBZixDQVg0Qjs7QUFhaEMsTUFBSSx5QkFBSjtNQUFtQix1QkFBbkI7TUFBZ0MsdUJBQWhDO01BQTZDLHVCQUE3QztNQUEwRCxzQkFBMUQsQ0FiZ0M7QUFjaEMsTUFBSSxxQkFBcUIsU0FBckIsQ0FkNEI7O0FBZ0JoQyxVQUFPLFFBQVEsV0FBUixFQUFQOztBQUVDLFFBQUssV0FBTDs7QUFGRCxRQUlNLFNBQUw7QUFDQyxvQkFBZ0IsbUJBQWhCLENBREQ7QUFFQyxvQkFBZ0IsS0FBSyxjQUFMLENBQW9CLGFBQXBCLENBQWhCLENBRkQ7O0FBSUMsa0JBQWMsQ0FDYixJQUFJLFFBQUosQ0FBYSxNQUFiLEVBQXFCLE1BQXJCLENBRGEsRUFFYixJQUFJLFFBQUosQ0FBYSxPQUFiLEVBQXNCLFFBQXRCLENBRmEsRUFHYixJQUFJLFFBQUosQ0FBYSxPQUFiLEVBQXNCLE1BQXRCLENBSGEsQ0FBZDtBQUpELFFBU0ksUUFBUSxXQUFSLE9BQTBCLFNBQTFCLEVBQXFDO0FBQ3ZDLGlCQUFZLElBQVosQ0FBaUIsSUFBSSxRQUFKLENBQWEsU0FBYixFQUF3QixTQUF4QixDQUFqQixFQUR1QztLQUF4Qzs7QUFNQSxrQkFBYyxTQUFkLENBZkQ7O0FBaUJDLGtCQUFjLFlBQVksV0FBWixDQUF3QixHQUF4QixDQUE2QixVQUFDLElBQUQsRUFBTyxHQUFQLEVBQWU7U0FFeEQ7OztBQXFCRyxVQXJCSCxRQUZ3RDs7QUFHeEQscUJBb0JHLEtBcEJILGNBSHdEOztBQUl4RCxnQkFtQkcsS0FuQkgsU0FKd0Q7U0FLeEQsY0FrQkcsS0FsQkgsWUFMd0Q7OztBQU94RCxhQWdCRyxLQWhCSCxNQVB3RDs7QUFReEQsbUJBZUcsS0FmSCxZQVJ3RDs7QUFTeEQsbUJBY0csS0FkSCxZQVR3RDs7QUFVeEQsa0JBYUcsS0FiSCxXQVZ3RDs7QUFXeEQsa0JBWUcsS0FaSCxXQVh3RDs7QUFZeEQsd0JBV0csS0FYSCxpQkFad0Q7OztBQWN4RCxvQkFTRyxLQVRILGFBZHdEOztBQWV4RCxvQkFRRyxLQVJILGFBZndEOztBQWdCeEQsZUFPRyxLQVBILFFBaEJ3RDs7O0FBa0J4RCxnQkFLRyxLQUxILFNBbEJ3RDs7O0FBb0J4RCxtQkFHRyxLQUhILFlBcEJ3RDtTQXFCeEQsYUFFRyxLQUZILFdBckJ3RDs7O0FBMEJ6RCxTQUFJLHVCQUFKLENBMUJ5RDtBQTJCekQsU0FBSSxjQUFjO0FBQ2pCLFlBQU0sVUFBQyxLQUFlLENBQWYsR0FBdUIsZUFBZSxVQUFmLFlBQWdDLGVBQWUsVUFBZixDQUF4RCxHQUF3RixlQUFlLENBQWYsV0FBeUIsZUFBZSxVQUFmLENBQXpCLEdBQXdELE9BQXhEO0FBQzlGLFlBQU0sMkJBQXlCLGdCQUF6QixHQUE4QyxHQUE5QztNQUZILENBM0JxRDs7QUFnQ3pELGFBQU8sUUFBUSxXQUFSLEVBQVA7QUFDQyxXQUFLLFdBQUw7QUFDQyxxQkFBYyxJQUFkLENBREQ7QUFFQyxtQkFBWSxNQUFaLElBQXlCLHVCQUFrQixZQUEzQyxDQUZEO0FBR0MsYUFIRDtBQURELFdBS00sU0FBTDtBQUNDLHFCQUFjLElBQWQsQ0FERDtBQUVDLG1CQUFZLFFBQVosSUFBd0IsVUFBYSxjQUFiLEdBQTJCLEdBQTNCLENBRnpCO0FBR0MsYUFIRDtBQUxEO01BaEN5RDs7QUE4Q3pELFNBQUksV0FBVyxFQUFYLENBOUNxRDtBQStDekQsU0FBRyxhQUFhLEdBQWIsRUFBa0I7QUFDcEIsZUFBUyxJQUFULENBQWMseURBQWQsRUFEb0I7TUFBckI7O0FBSUEsU0FBRyxXQUFILEVBQWdCO0FBQ2YsMkJBQXFCLENBQ3BCO0FBQ0MsYUFBTSxVQUFOO0FBQ0EsYUFBTSxjQUFjLGFBQWEsV0FBYjtBQUNwQixhQUFNLEdBQU47T0FKbUIsQ0FBckIsQ0FEZTtNQUFoQjs7QUFVQSxTQUFJLGVBQWdCLGdCQUFnQixHQUFoQjs7QUE3RHFDLFlBZ0V4RCxvQkFBQyxXQUFEO0FBQ0Msb0JBQVksT0FBWjtBQUNBLFlBQUssT0FBTDtBQUNBLGdCQUFVLGFBQVY7QUFDQSxnQkFBVSxRQUFWO0FBQ0EsbUJBQWEsV0FBYjtBQUNBLGNBQU8sR0FBUDtBQUNBLFlBQUssSUFBTDtBQUNBLG9CQUFjLEtBQWQ7QUFDQSxnQkFBVSxRQUFWO0FBQ0EsbUJBQWEsY0FBYyxRQUFkLEdBQXlCLFdBQXpCO0FBQ2IsbUJBQWEsV0FBYjtBQUNBLDJCQUFvQixxQkFBcEI7QUFDQSxpQkFBVyxVQUFYO0FBQ0EsMEJBQW9CLGtCQUFwQjtBQUNBLG9CQUFjLFlBQWQ7QUFDQSxhQUFPLEdBQVA7QUFDQSwyQkFBcUIsTUFBSyxtQkFBTCxDQUF5QixJQUF6QixDQUE4QixJQUE5QixFQUFvQyxZQUFwQyxFQUFrRCxXQUFsRCxFQUErRCxPQUEvRCxFQUF3RSxHQUF4RSxDQUFyQjtBQUNBLHFCQUFlLElBQWY7TUFsQkQsQ0FERCxDQS9EeUQ7S0FBZixDQUEzQyxDQWpCRDs7QUF3R0MsVUF4R0Q7O0FBSkQ7R0FoQmdDOztBQWtJaEMsU0FBTyxFQUFDLDRCQUFELEVBQWdCLHdCQUFoQixFQUE2Qix3QkFBN0IsRUFBMEMsd0JBQTFDLEVBQXVELHNCQUF2RCxFQUFQLENBbElnQztFQWpDUTtBQXNLekMsNkNBQWlCLFNBQVM7QUFDekIsTUFBSSw2QkFBSixDQUR5Qjs7QUFHekIsVUFBTyxRQUFRLFdBQVIsRUFBUDtBQUNDLFFBQUssU0FBTDtBQUNDLFVBREQ7QUFERCxRQUdNLEtBQUw7QUFDQyxVQUREO0FBSEQ7O0dBSHlCOztBQVl6QixTQUFPLGlCQUFQLENBWnlCO0VBdEtlO0NBQWxCLENBQXBCOztBQXlMSixPQUFPLE9BQVAsR0FBaUIsaUJBQWpCOzs7Ozs7Ozs7OztBQzdOQSxJQUFJLFFBQVUsUUFBUSxPQUFSLENBQVY7SUFDSCxpQkFBaUIsUUFBUSw0Q0FBUixDQUFqQjtJQUNBLFlBQWEsUUFBUSx1Q0FBUixDQUFiO0lBQ0EsTUFBUyxRQUFRLHdCQUFSLENBQVQ7SUFDQSxnQkFBZ0IsUUFBUSxvQ0FBUixDQUFoQjtJQUNBLGlCQUFrQixRQUFRLG1DQUFSLENBQWxCOztJQUVLLGlCQUFtQyxlQUFuQztJQUFnQixpQkFBbUIsZUFBbkI7OztBQUV0QixJQUFNLGdCQUFnQixRQUFRLHNDQUFSLENBQWhCOztBQUVOLElBQU0sZ0JBQWdCO0FBQ3JCLFlBQVcsU0FBWDtBQUNBLFVBQVMsU0FBVDtDQUZLOztBQUtOLElBQU0sV0FBVyxDQUNoQjtBQUNDLFFBQU8sV0FBUDtBQUNBLFFBQU8sS0FBUDtDQUhlLEVBS2hCO0FBQ0MsUUFBTyxXQUFQO0FBQ0EsUUFBTyxJQUFQO0NBUGUsRUFTaEI7QUFDQyxRQUFPLFdBQVA7QUFDQSxRQUFPLElBQVA7Q0FYZSxFQWFoQjtBQUNDLFFBQU8sV0FBUDtBQUNBLFFBQU8sSUFBUDtDQWZlLEVBaUJoQjtBQUNDLFFBQU8sV0FBUDtBQUNBLFFBQU8sSUFBUDtDQW5CZSxFQXFCaEI7QUFDQyxRQUFPLFdBQVA7QUFDQSxRQUFPLEtBQVA7Q0F2QmUsRUF5QmhCO0FBQ0MsUUFBTyxXQUFQO0FBQ0EsUUFBTyxLQUFQO0NBM0JlLEVBNkJoQjtBQUNDLFFBQU8sV0FBUDtBQUNBLFFBQU8sS0FBUDtDQS9CZSxFQWlDaEI7QUFDQyxRQUFPLFdBQVA7QUFDQSxRQUFPLEtBQVA7Q0FuQ2UsRUFxQ2hCO0FBQ0MsUUFBTyxXQUFQO0FBQ0EsUUFBTyxLQUFQO0NBdkNlLENBQVg7O0FBNENOLFNBQVMsZUFBVCxDQUF5QixNQUF6QixFQUFpQztBQUNoQyxLQUFJLG1CQUFKO0tBQWEsbUJBQWIsQ0FEZ0M7O0FBR2hDLFNBQU8sTUFBUDtBQUNDLE9BQUssYUFBTDtBQUNDLGFBQVUsVUFBVixDQUREO0FBRUMsYUFBVSxHQUFWLENBRkQ7QUFHQyxTQUhEO0FBREQsT0FLTSxhQUFMO0FBQ0MsYUFBVSxVQUFWLENBREQ7QUFFQyxhQUFVLEdBQVYsQ0FGRDtBQUdDLFNBSEQ7QUFMRCxPQVNNLGFBQUw7QUFDQyxhQUFVLFdBQVYsQ0FERDtBQUVDLGFBQVUsSUFBVixDQUZEO0FBR0MsU0FIRDtBQVRELE9BYU0sYUFBTDtBQUNDLGFBQVUsV0FBVixDQUREO0FBRUMsYUFBVSxJQUFWLENBRkQ7QUFHQyxTQUhEO0FBYkQsT0FpQk0sYUFBTDtBQUNDLGFBQVUsV0FBVixDQUREO0FBRUMsYUFBVSxJQUFWLENBRkQ7QUFHQyxTQUhEO0FBakJEO0FBc0JFLGFBQVUsWUFBVixDQUREO0FBRUMsYUFBVSxHQUFWLENBRkQ7QUFyQkQsRUFIZ0M7O0FBNkJoQyxRQUFPLEVBQUUsZ0JBQUYsRUFBVyxnQkFBWCxFQUFQLENBN0JnQztDQUFqQzs7SUFrQ00sYUFDTCxTQURLLFVBQ0wsQ0FBWSxLQUFaLEVBQW1CLEtBQW5CLEVBQTBCLElBQTFCLEVBQWdDO3VCQUQzQixZQUMyQjs7QUFDL0IsTUFBSyxLQUFMLEdBQWEsS0FBYixDQUQrQjtBQUUvQixNQUFLLEtBQUwsR0FBYSxLQUFiLENBRitCO0FBRy9CLE1BQUssSUFBTCxHQUFZLElBQVosQ0FIK0I7Q0FBaEM7O0FBUUQsSUFBSSxtQkFBbUIsTUFBTSxXQUFOLENBQWtCOzs7QUFDckMsWUFBVztBQUNiLFdBQVMsTUFBTSxTQUFOLENBQWdCLE1BQWhCO0FBQ1QsaUJBQWUsTUFBTSxTQUFOLENBQWdCLEtBQWhCO0FBQ2YsY0FBWSxNQUFNLFNBQU4sQ0FBZ0IsTUFBaEI7RUFIVjs7QUFNQSw2Q0FBa0I7QUFDZCxTQUFPO0FBQ1osWUFBUyxFQUFUO0FBQ0Esa0JBQWUsRUFBZjtBQUNBLGVBQVksRUFBWjtHQUhLLENBRGM7RUFQbUI7OztBQWV4QyxrQkFBaUIsMkJBQVc7QUFDM0IsTUFBSSx1QkFBSixDQUQyQjs7QUFHM0IsTUFBRyxLQUFLLEtBQUwsQ0FBVyxPQUFYLENBQW1CLFdBQW5CLE9BQXFDLFNBQXJDLEVBQWdEO0FBQ2xELGlCQUFjLG9CQUFkLENBRGtEO0dBQW5ELE1BRU87QUFDTixpQkFBYyxjQUFkLENBRE07R0FGUDs7QUFNTSxTQUFPO0FBQ1osbUJBQWdCLEtBQWhCO0FBQ0EsaUJBQWMsRUFBZDtBQUNBLGNBQVcsRUFBWDtBQUNBLGNBQVcsRUFBWDtBQUNBLFdBQVEsRUFBUjtBQUNBLFNBQU0sRUFBTjtBQUNBLGFBQVUsRUFBVjtBQUNBLGVBQVksRUFBWjtBQUNBLGdCQUFhLEVBQWI7QUFDQSxpQkFBYyxFQUFkO0FBQ0EsY0FBVyxFQUFYOztBQUVBLHdCQUFxQixXQUFyQjtHQWJLLENBVHFCO0VBQVg7O0FBMkJkLGlEQUFvQjtBQUN0QixNQUFJLFdBQVcsY0FBYyxLQUFLLEtBQUwsQ0FBVyxPQUFYLENBQW1CLFdBQW5CLEVBQWQsQ0FBWCxDQURrQjs7QUFHdEIsTUFBSSxJQUFKLENBQVMsb0NBQVQsRUFBK0MsRUFBQyxrQkFBRCxFQUEvQyxFQUNFLElBREYsQ0FDTyxVQUFVLEdBQVYsRUFBZTtBQUNwQixPQUFHLElBQUksTUFBSixDQUFXLFVBQVgsS0FBMEIsU0FBMUIsRUFBcUM7QUFDdkMsUUFBSSxhQUFhLElBQUksTUFBSixDQUFXLElBQVgsQ0FEc0I7QUFFdkMsUUFBSSxVQUFZLFdBQVcsR0FBWCxDQUFlLFVBQUMsQ0FBRDtZQUFPLElBQUksVUFBSixDQUFlLEVBQUUsTUFBRixFQUFVLEVBQUUsRUFBRjtLQUFoQyxDQUEzQixDQUZtQzs7QUFJdkMsU0FBSyxRQUFMLENBQWM7QUFDYixrQkFBYSxPQUFiO0tBREQsRUFKdUM7SUFBeEM7R0FESyxDQVNKLElBVEksQ0FTQyxJQVRELENBRFA7OztBQUhzQixHQWlCdEIsQ0FBRSxvQkFBRixFQUF3QixFQUF4QixDQUEyQixpQkFBM0IsRUFBOEMsS0FBSyxVQUFMLENBQTlDLENBakJzQjtFQTFDaUI7QUErRHJDLHVEQUF1QixFQS9EYztBQW1FckMsMkJBQVM7ZUFZUCxLQUFLLEtBQUwsQ0FaTztNQUVWLHVDQUZVO01BR1YsaUNBSFU7TUFJVixtQ0FKVTtNQUtWLG1DQUxVO01BTVYsMkJBTlU7TUFPViwrQkFQVTtNQVFWLDZCQVJVO01BU1YsbUJBVFU7TUFVVix1QkFWVTtNQVdWLGlEQVhVO2VBYWtDLEtBQUssS0FBTCxDQWJsQztNQWFMLHlCQWJLO01BYUkscUNBYko7TUFhbUIsK0JBYm5COzs7QUFlWCxNQUFJLHdCQUFKLENBZlc7QUFnQlgsTUFBRyxRQUFRLFdBQVIsT0FBMEIsU0FBMUIsRUFBcUM7QUFDdkMsT0FBSSxnQkFBZ0IsS0FBSyxLQUFMLENBQVcsYUFBWCxDQUF5QixJQUF6QixDQUE4QixVQUFDLENBQUQ7V0FBTyxFQUFFLEVBQUYsS0FBUyxZQUFUO0lBQVAsQ0FBOUMsQ0FEbUM7O0FBR3ZDLGtCQUNDOztNQUFLLFdBQVUsS0FBVixFQUFMO0lBQ0M7O09BQUssV0FBVSxVQUFWLEVBQUw7S0FDQzs7UUFBTyxXQUFVLGNBQVYsRUFBUDtNQUFpQyxjQUFjLEtBQWQsSUFBdUIsRUFBdkI7TUFEbEM7S0FFQyxvQkFBQyxjQUFEO0FBQ0MsZ0JBQVUsQ0FBVjtBQUNBLG1CQUFhLGNBQWMsV0FBZCxJQUE2QixJQUE3QjtBQUNiLGVBQVMsY0FBYyxPQUFkO0FBQ1Qsb0JBQWMsS0FBSyxvQkFBTDtBQUNkLGlCQUFXLGNBQWMsU0FBZCxJQUEyQixpQkFBM0I7QUFDWCxnQkFBVSxVQUFWO0FBQ0Esb0JBQWMsa0JBQWtCLGNBQWMsWUFBZDtNQVBqQyxDQUZEO0tBREQ7SUFERCxDQUh1QztHQUF4Qzs7QUFxQkEsTUFBSSxjQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDs7S0FERDtJQUVDLG9CQUFDLGNBQUQ7QUFDQyxrQkFBWSxJQUFaO0FBQ0EsY0FBUyxXQUFUO0FBQ0EsbUJBQWMsS0FBSyxtQkFBTDtBQUNkLGVBQVUsWUFBVjtBQUNBLG1CQUFjLGtCQUFrQixjQUFsQjtLQUxmLENBRkQ7SUFERDtHQURHLENBckNPOztBQW9EWCxNQUFJLGtCQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDs7S0FERDtJQUVDLG9CQUFDLGNBQUQ7QUFDQyxrQkFBYSxtQkFBYjtBQUNBLGNBQVMsWUFBVDtBQUNBLG1CQUFjLEtBQUssdUJBQUw7QUFDZCxlQUFVLFNBQVY7QUFDQSxtQkFBYyxrQkFBa0IsY0FBbEI7S0FMZixDQUZEO0lBREQ7R0FERyxDQXBETzs7QUFxRVgsTUFBSSxXQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDs7S0FERDtJQUVDLG9CQUFDLGNBQUQ7QUFDQyxrQkFBWSxJQUFaO0FBQ0EsY0FBUyxRQUFUO0FBQ0EsbUJBQWMsS0FBSyxZQUFMLENBQWtCLElBQWxCLENBQXVCLElBQXZCLEVBQTZCLE1BQTdCLENBQWQ7QUFDQSxnQkFBVSxpQkFBVjtBQUNBLGVBQVUsSUFBVjtBQUNBLG1CQUFjLGtCQUFrQixjQUFsQjtLQU5mLENBRkQ7SUFERDtHQURHLENBckVPOztBQXFGWCxNQUFJLGFBQ0g7O0tBQUssV0FBVSxLQUFWLEVBQUw7R0FDQzs7TUFBSyxXQUFVLFVBQVYsRUFBTDtJQUNDOztPQUFPLFdBQVUsY0FBVixFQUFQOztLQUREO0lBRUMsb0JBQUMsU0FBRDtBQUNDLFdBQUssb0JBQUw7QUFDQSxrQkFBWSxHQUFaO0FBQ0EsaUJBQVcsbUJBQVg7QUFDQSxtQkFBYyxLQUFLLHFCQUFMLENBQTJCLElBQTNCLENBQWdDLElBQWhDLEVBQXNDLFFBQXRDLENBQWQ7QUFDQSxZQUFPLE1BQVA7QUFDQSxXQUFLLElBQUw7QUFDQSxtQkFBYyxrQkFBa0IsY0FBbEI7S0FQZixDQUZEO0lBREQ7R0FERyxDQXJGTzs7QUF3R1gsTUFBSSxlQUNIOztLQUFLLFdBQVUsS0FBVixFQUFMO0dBQ0M7O01BQUssV0FBVSxVQUFWLEVBQUw7SUFDQzs7T0FBTyxXQUFVLGNBQVYsRUFBUDs7S0FERDtJQUVDLG9CQUFDLFNBQUQ7QUFDQyxXQUFLLFFBQUw7QUFDQSxrQkFBWSxHQUFaO0FBQ0EsaUJBQVcsbUJBQVg7QUFDQSxtQkFBYyxLQUFLLHFCQUFMLENBQTJCLElBQTNCLENBQWdDLElBQWhDLEVBQXNDLFVBQXRDLENBQWQ7QUFDQSxZQUFPLFFBQVA7QUFDQSxXQUFLLEdBQUw7QUFDQSxtQkFBYyxrQkFBa0IsY0FBbEI7S0FQZixDQUZEO0lBREQ7R0FERyxDQXhHTzs7QUEySFgsU0FDQzs7S0FBSyxXQUFVLFlBQVYsRUFBdUIsSUFBRyxtQkFBSCxFQUE1QjtHQUNDOztNQUFLLFdBQVUsY0FBVixFQUFMO0lBQ0M7O09BQUssV0FBVSwyQkFBVixFQUFMO0tBRUMsMkJBQUcsV0FBVSxPQUFWLEVBQWtCLGdCQUFhLE9BQWIsRUFBcUIsY0FBVyxPQUFYLEVBQTFDLENBRkQ7S0FJQzs7UUFBSyxXQUFVLGNBQVYsRUFBTDtNQUNDOztTQUFJLFdBQVUsYUFBVixFQUF3QixJQUFHLGNBQUgsRUFBNUI7O09BREQ7TUFKRDtLQVNDOztRQUFLLFdBQVUsWUFBVixFQUFMO01BQ0M7O1NBQUssV0FBVSxhQUFWLEVBQUw7T0FFRSxZQUZGO09BR0UsV0FIRjtPQUlFLGVBSkY7T0FLRSxRQUxGO09BTUUsVUFORjtPQU9FLFlBUEY7T0FTQzs7VUFBTSxXQUFVLEtBQVYsRUFBTjs7UUFURDtPQVVDOztVQUFLLFdBQVUsWUFBVixFQUFMO1FBQ0M7O1dBQUcsV0FBVSxnQkFBVixFQUEyQixNQUFLLFFBQUwsRUFBYyxTQUFTLEtBQUssYUFBTCxFQUFyRDs7U0FERDtRQVZEO09BREQ7TUFURDtLQUREO0lBREQ7R0FERCxDQTNIVztFQW5FNEI7QUFrT3hDLHFDQUFhLE1BQU0sS0FBSztBQUN2QixRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURNO0FBRXZCLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGSjs7QUFJdkIsTUFBSSxNQUFNLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBN0IsS0FBNEMsT0FBTyxXQUFQLENBSi9COztBQU12QixPQUFLLFFBQUwscUJBQ0UsTUFBTyxJQURULEVBTnVCO0VBbE9nQjtBQThPeEMsdURBQXNCLE1BQU0sS0FBSztBQUNoQyxRQUFNLE1BQUssR0FBTCxHQUFXLE9BQU8sS0FBUCxDQURlO0FBRWhDLE1BQUksU0FBUyxJQUFJLE1BQUosSUFBYyxJQUFJLFVBQUosQ0FGSzs7QUFJaEMsTUFBSSxNQUFNLE9BQU8sS0FBUCxDQUpzQjs7QUFNaEMsT0FBSyxRQUFMLHFCQUNFLE1BQU8sSUFEVCxFQU5nQztFQTlPTztBQTRQeEMsMkRBQXdCLEtBQUs7QUFDNUIsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FEVztBQUU1QixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRkM7O0FBSTVCLE1BQUksU0FBUyxFQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLElBQWxCLEVBQXdCLElBQXhCLENBQTZCLFVBQTdCLEtBQTRDLE9BQU8sV0FBUCxDQUo3QjtBQUs1QixNQUFJLFdBQVcsS0FBSyxLQUFMLENBQVcsWUFBWCxDQUF3QixJQUF4QixDQUE2QixVQUFDLENBQUQ7VUFBUSxFQUFFLEtBQUYsS0FBWSxNQUFaO0dBQVIsQ0FBN0IsQ0FBMEQsSUFBMUQsQ0FBK0QsV0FBL0QsQ0FMYTs7QUFPNUIsT0FBSyxRQUFMLENBQWM7QUFDYixjQUFXLE1BQVg7QUFDQSxhQUFVLENBQUcsWUFBWSxhQUFhLENBQWIsQ0FBYixJQUFnQyxPQUFPLFFBQVAsQ0FBaEMsSUFBc0QsRUFBeEQ7R0FGWCxFQVA0QjtFQTVQVztBQTBReEMscURBQXFCLEtBQUs7QUFDekIsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FEUTtBQUV6QixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRkY7O0FBSXpCLE1BQUksYUFBYSxFQUFFLE1BQUYsRUFBVSxPQUFWLENBQWtCLElBQWxCLEVBQXdCLElBQXhCLENBQTZCLFVBQTdCLEtBQTRDLE9BQU8sV0FBUCxDQUpwQztNQUtuQixlQUFpQixLQUFLLEtBQUwsQ0FBakIsYUFMbUI7OztBQU96QixPQUFLLFFBQUwsQ0FBYztBQUNiLHlCQURhO0FBRWIsY0FBVyxFQUFYO0dBRkQsRUFQeUI7O0FBWXpCLE1BQUcsWUFBSCxFQUFpQjtBQUNoQixRQUFLLGlCQUFMLENBQXVCLFlBQXZCLEVBQXFDLFVBQXJDLEVBRGdCO0dBQWpCO0VBdFJ1QztBQTRSeEMsbURBQW9CLEtBQUs7QUFDeEIsUUFBTSxNQUFLLEdBQUwsR0FBVyxPQUFPLEtBQVAsQ0FETztBQUV4QixNQUFJLFNBQVMsSUFBSSxNQUFKLElBQWMsSUFBSSxVQUFKLENBRkg7O0FBSXhCLE1BQUksZ0JBQWdCLEVBQUUsTUFBRixFQUFVLE9BQVYsQ0FBa0IsSUFBbEIsRUFBd0IsSUFBeEIsQ0FBNkIsVUFBN0IsS0FBNEMsT0FBTyxXQUFQLENBSnhDOztBQU14QixPQUFLLFFBQUwsQ0FBYztBQUNiLGlCQUFjLGFBQWQ7QUFDQSxjQUFXLEVBQVg7QUFDQSxpQkFBYyxFQUFkO0FBQ0EsYUFBVSxFQUFWO0dBSkQsRUFOd0I7O01BYWxCLGFBQWUsS0FBSyxLQUFMLENBQWYsV0Fia0I7OztBQWV4QixNQUFHLGNBQWMsS0FBSyxLQUFMLENBQVcsT0FBWCxDQUFtQixXQUFuQixPQUFxQyxTQUFyQyxFQUFnRDtBQUNoRSxRQUFLLGlCQUFMLENBQXVCLGFBQXZCLEVBQXNDLFVBQXRDLEVBRGdFO0dBQWpFO0VBM1N1QztBQWdUeEMsK0NBQWtCLGNBQWMsWUFBWTtBQUMzQyxNQUFJLFNBQVMsY0FBYyxLQUFLLEtBQUwsQ0FBVyxPQUFYLENBQW1CLFdBQW5CLEVBQWQsQ0FBVCxDQUR1Qzs7QUFHM0MsTUFBSSxJQUFKLENBQVMsbUNBQVQsRUFBOEMsRUFBRSxlQUFlLFlBQWYsRUFBNkIsY0FBL0IsRUFBdUMsc0JBQXZDLEVBQTlDLEVBQ0UsSUFERixDQUNPLFVBQVUsR0FBVixFQUFlO0FBQ3BCLE9BQUcsSUFBSSxNQUFKLENBQVcsVUFBWCxLQUEwQixTQUExQixFQUFxQztBQUN2QyxRQUFJLGFBQWEsSUFBSSxNQUFKLENBQVcsSUFBWCxDQURzQjs7QUFHdkMsUUFBRyxjQUFjLFdBQVcsTUFBWCxHQUFvQixDQUFwQixFQUF1QjtBQUN2QyxTQUFJLFVBQVksV0FBVyxHQUFYLENBQWUsVUFBQyxDQUFEO2FBQU8sSUFBSSxVQUFKLENBQWUsRUFBRSxLQUFGLEVBQVMsRUFBRSxPQUFGLEVBQVcsQ0FBbkM7TUFBUCxDQUEzQixDQURtQzs7QUFHdkMsVUFBSyxRQUFMLENBQWM7QUFDYixvQkFBYyxPQUFkO0FBQ0EsMkJBQXFCLElBQXJCO01BRkQsRUFIdUM7S0FBeEMsTUFPTztBQUNOLFVBQUssUUFBTCxDQUFjO0FBQ2IsMkJBQXFCLHNCQUFyQjtNQURELEVBRE07S0FQUDtJQUhEO0dBREssQ0FpQkosSUFqQkksQ0FpQkMsSUFqQkQsQ0FEUCxFQUgyQztFQWhUSjtBQXlVeEMsbUNBQWE7O0FBRVosT0FBSyxRQUFMLENBQWM7QUFDYixtQkFBZ0IsS0FBaEI7QUFDQSxpQkFBYyxFQUFkO0FBQ0EsY0FBVyxFQUFYO0FBQ0EsY0FBVyxFQUFYO0FBQ0EsV0FBUSxFQUFSO0FBQ0EsU0FBTSxFQUFOO0FBQ0EsYUFBVSxFQUFWO0FBQ0EsZUFBWSxFQUFaO0FBQ0EsaUJBQWMsRUFBZDtBQUNBLHdCQUFxQixtQkFBckI7R0FWRCxFQUZZO0VBelUyQjtBQXlWeEMseUNBQWdCO2dCQVFYLEtBQUssS0FBTCxDQVJXO01BRWQsb0NBRmM7TUFHZCw4QkFIYztNQUlkLHdCQUpjO01BS2Qsb0JBTGM7TUFNZCw0QkFOYztNQU9kLGdDQVBjOzs7QUFVZixNQUFJLFdBQVcsSUFBWCxDQVZXOztBQVlmLGFBQVcsWUFBYSxlQUFlLFlBQWYsTUFBaUMsSUFBakMsQ0FaVDtBQWFmLGFBQVcsWUFBYSxlQUFlLFNBQWYsTUFBOEIsSUFBOUIsQ0FiVDtBQWNmLGFBQVcsWUFBYSxlQUFlLElBQWYsTUFBeUIsSUFBekIsQ0FkVDtBQWVmLGFBQVcsWUFBYSxlQUFlLE1BQWYsTUFBMkIsSUFBM0IsQ0FmVDtBQWdCZixhQUFXLFlBQWEsZUFBZSxRQUFmLE1BQTZCLElBQTdCLENBaEJUOztBQWtCZixNQUFHLEtBQUssS0FBTCxDQUFXLE9BQVgsQ0FBbUIsV0FBbkIsT0FBcUMsU0FBckMsRUFBZ0Q7QUFDbEQsY0FBVyxZQUFhLGVBQWUsVUFBZixNQUErQixJQUEvQixDQUQwQjtHQUFuRDs7QUFJQSxTQUFPLFFBQVAsQ0F0QmU7RUF6VndCO0FBa1h4Qyx1Q0FBYyxLQUFLO0FBQ2xCLFFBQU0sTUFBSyxHQUFMLEdBQVcsT0FBTyxLQUFQLENBREM7QUFFbEIsTUFBSSxTQUFTLElBQUksTUFBSixJQUFjLElBQUksVUFBSixDQUZUOztnQkFZZCxLQUFLLEtBQUwsQ0FaYztNQUtqQixvQ0FMaUI7TUFNakIsOEJBTmlCO01BT2pCLHdCQVBpQjtNQVFqQixvQkFSaUI7TUFTakIsNEJBVGlCO01BVWpCLGdDQVZpQjtNQVdqQixvQ0FYaUI7Z0JBYWUsS0FBSyxLQUFMLENBYmY7TUFhWiwwQkFiWTtNQWFILHNDQWJHOztBQWNsQixNQUFJLGdCQUFKLENBZGtCOztBQWdCbEIsTUFBSSxXQUFXLEtBQUssYUFBTCxFQUFYLENBaEJjOztBQWtCbEIsTUFBRyxRQUFILEVBQWE7O0FBQ1osUUFBSSxPQUFPLEVBQVA7O0FBRUosWUFBTyxRQUFRLFdBQVIsRUFBUDtBQUNDLFVBQUssU0FBTDtBQUNDLFdBQUssVUFBTCxHQUFrQixVQUFsQixDQUREO0FBREQsVUFHTSxXQUFMO0FBQ0MsYUFBTyxTQUFjLEVBQWQsRUFBa0IsSUFBbEIsRUFBd0I7QUFDOUIscUJBQWMsT0FBTyxjQUFjLE9BQWQsQ0FBUCxDQUFkO0FBQ0Esa0JBQVcsT0FBTyxTQUFQLENBQVg7QUFDQSxrQkFBVyxPQUFPLGFBQWEsSUFBYixDQUFrQixVQUFDLElBQUQ7ZUFBVyxLQUFLLEtBQUwsS0FBZSxTQUFmO1FBQVgsQ0FBbEIsQ0FBd0QsS0FBeEQsQ0FBbEI7QUFDQSxlQUFRLE9BQU8sTUFBUCxDQUFSO0FBQ0Esa0JBQVcsT0FBTyxJQUFQLENBQVg7QUFDQSxpQkFBVSxPQUFPLFFBQVAsQ0FBVjtPQU5NLENBQVAsQ0FERDtBQVNDLFlBVEQ7QUFIRDs7QUFlQSxRQUFJLFVBQVUsRUFBRSxNQUFGLENBQVY7QUFDSixrQkFBYyxZQUFkLENBQTJCLElBQTNCLEVBQWlDLFlBQU07QUFDdEMsYUFBUSxPQUFSLENBQWdCLFFBQWhCLEVBQTBCLEtBQTFCLENBQWdDLE1BQWhDLEVBRHNDO0tBQU4sQ0FBakM7UUFuQlk7R0FBYixNQXVCTztBQUNOLFFBQUssUUFBTCxDQUFjO0FBQ2Isb0JBQWdCLElBQWhCO0lBREQsRUFETTtHQXZCUDtFQXBZdUM7Q0FBbEIsQ0FBbkI7O0FBcWFKLE9BQU8sT0FBUCxHQUFpQixnQkFBakI7Ozs7O0FDNWdCQSxJQUFJLFFBQVcsUUFBUSxPQUFSLENBQVg7SUFDSCxtQkFBbUIsUUFBUSwwQ0FBUixFQUFvRCxnQkFBcEQ7SUFDbkIsaUJBQWtCLFFBQVEsbUNBQVIsQ0FBbEI7O0lBRUssaUJBQW1DLGVBQW5DO0lBQWdCLGlCQUFtQixlQUFuQjs7O0FBR3RCLElBQUksb0JBQW9CLE1BQU0sV0FBTixDQUFrQjs7O0FBQ3pDLFNBQVEsQ0FDUCxRQUFRLG9DQUFSLENBRE8sRUFFUCxRQUFRLHNDQUFSLENBRk8sRUFHUCxRQUFRLHlDQUFSLENBSE8sRUFJUCxRQUFRLG1DQUFSLENBSk8sRUFLUCxRQUFRLDJDQUFSLENBTE8sQ0FBUjs7QUFTRyw2Q0FBa0I7QUFDZCxTQUFPLEVBQVAsQ0FEYztFQVZvQjtBQWdCdEMsNkNBQWtCO0FBQ2QsU0FBTyxFQUFQLENBRGM7RUFoQm9CO0FBc0J0QyxpREFBb0IsRUF0QmtCO0FBMEJ0Qyx1REFBdUIsRUExQmU7QUE4QnpDLCtDQUFrQixZQUFZO01BQ3hCLFVBQW9DLFdBQXBDLFFBRHdCO01BQ2YsY0FBMkIsV0FBM0IsWUFEZTtNQUNGLGFBQWMsV0FBZCxXQURFOztBQUU3QixNQUFJLG1CQUFKLENBRjZCOztBQUk3QixVQUFPLFFBQVEsV0FBUixFQUFQO0FBQ0MsUUFBSyxXQUFMO0FBQ0MsY0FBVSxDQUNUO0FBQ0MsWUFBTyxNQUFQO0FBQ0EsV0FBTSxvQkFBTjtBQUNBLFNBQUksUUFBSjtBQUNBLFdBQU0sSUFBTjtBQUNBLG1CQUFjLGNBQWQ7S0FOUSxDQUFWLENBREQ7QUFVQyxVQVZEO0FBREQsUUFZTSxTQUFMO0FBQ0MsY0FBVSxDQUNUO0FBQ0MsWUFBTyxNQUFQO0FBQ0EsV0FBTSxRQUFOO0FBQ0EsU0FBSSxZQUFKO0FBQ0EsY0FBUyxLQUFLLGdCQUFMLENBQXNCLFVBQXRCLEVBQWtDLFVBQWxDLENBQVQ7QUFDQSxtQkFBYyxjQUFkO0tBTlEsQ0FBVixDQUREO0FBVUMsVUFWRDtBQVpEO0FBd0JFLGNBQVUsRUFBVixDQUREO0FBdkJELEdBSjZCOztBQStCN0IsU0FBTyxPQUFQLENBL0I2QjtFQTlCVztBQWdFekMscURBQXFCLFNBQVMsaUJBQWlCOztBQUU5QyxNQUFJLG1CQUFtQixFQUFuQixDQUYwQzs7QUFJOUMsVUFBTyxRQUFRLFdBQVIsRUFBUDtBQUNDLFFBQUssV0FBTCxDQUREO0FBRUMsUUFBSyxTQUFMO0FBQ0MsdUJBQW1CLGdCQUFnQixHQUFoQixDQUFvQixVQUFDLElBQUQsRUFBTyxHQUFQLEVBQWU7U0FFcEQ7O0FBS0csVUFMSCxNQUZvRDs7QUFHcEQscUJBSUcsS0FKSCxjQUhvRDs7QUFJcEQsZ0JBR0csS0FISCxTQUpvRDs7QUFLcEQsY0FFRyxLQUZILE9BTG9EOzs7QUFTckQsWUFBTztBQUNOLGFBQU8sS0FBUDtBQUNBLFlBQU0sQ0FDTDtBQUNDLGFBQU0sS0FBTjtBQUNBLGNBQU8sYUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxtQkFBWSxJQUFaO09BTEksRUFPTDtBQUNDLGFBQU0sSUFBTjtBQUNBLGNBQU8sUUFBUDtBQUNBLGdCQUFTLENBQVQ7QUFDQSxlQUFRLEdBQVI7QUFDQSxhQUFNLEdBQU47QUFDQSxzQkFBZSxJQUFmO09BYkksRUFlTDtBQUNDLGFBQU0sTUFBTjtBQUNBLGNBQVUsaUJBQWlCLE1BQWpCLFFBQVY7QUFDQSxnQkFBUyxDQUFUO09BbEJJLENBQU47TUFGRCxDQVRxRDtLQUFmLENBQXZDLENBREQ7Ozs7Ozs7QUF3Q0MsVUF4Q0Q7O0FBRkQsR0FKOEM7O0FBa0Q5QyxTQUFPLGdCQUFQLENBbEQ4QztFQWhFTjtDQUFsQixDQUFwQjs7QUF5SEosT0FBTyxPQUFQLEdBQWlCLGlCQUFqQiIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIoZnVuY3Rpb24oKSB7XG5cblx0bGV0IFJlYWN0XHRcdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0JyksXG5cdFx0UmVhY3RET01cdFx0XHQ9IHJlcXVpcmUoJ3JlYWN0LWRvbScpLFxuXHRcdFF1ZXJ5U3RvcmVcdFx0XHQ9IHJlcXVpcmUoJy4uL2NvbW1vbi9zdG9yZXMvUXVlcnlTdG9yZScpLFxuXHRcdFByb2R1Y3RTdG9yZVx0XHQ9IHJlcXVpcmUoJy4uL2NvbW1vbi9zdG9yZXMvUHJvZHVjdFN0b3JlJyksXG5cdFx0Q29tcGFyaXNvblN0b3JlXHRcdD0gcmVxdWlyZSgnLi4vY29tbW9uL3N0b3Jlcy9Db21wYXJpc29uU3RvcmUnKSxcdFx0XG5cdFx0Q29tbW9uQWN0aW9uc1x0XHQ9IHJlcXVpcmUoJy4uL2NvbW1vbi9hY3Rpb25zL0NvbW1vbkFjdGlvbnMnKSxcblx0XHRnZXRTZWFyY2hDYWxsYmFja1x0PSByZXF1aXJlKCcuLi9jb21tb24vdXRpbHMvZ2V0U2VhcmNoQ2FsbGJhY2snKSxcblx0XHRMb2FuSGVscGVyU2VjdGlvblx0PSByZXF1aXJlKCcuL2NvbXBvbmVudHMvTG9hbkhlbHBlclNlY3Rpb24uanN4JyksXG5cdFx0TG9hblJlc3VsdFNlY3Rpb25cdD0gcmVxdWlyZSgnLi9jb21wb25lbnRzL0xvYW5SZXN1bHRTZWN0aW9uLmpzeCcpLFxuXHRcdExvYW5Qcm9kdWN0QWRkZXJcdD0gcmVxdWlyZSgnLi9jb21wb25lbnRzL0xvYW5Qcm9kdWN0QWRkZXIuanN4Jyk7XG5cblx0XG5cdGxldCBtYWlubWVudSwgc3VibWVudSwgaXNMb2dnZWRJbiwgc2VhcmNoUXVlcnk7XG4gICAgXG4gICAgaWYod2luZG93LkFwcCkge1xuXHRcdCh7bWFpbm1lbnUsIHN1Ym1lbnUsIHNlYXJjaFF1ZXJ5fSA9IHdpbmRvdy5BcHAucXVlcnkpO1xuXHRcdGlzTG9nZ2VkSW4gPSB3aW5kb3cuQXBwLmNvbXBhcmlzb25zLmlzTG9nZ2VkSW47XG5cdFx0XG5cdFx0UXVlcnlTdG9yZS5yZWh5ZHJhdGUoJ3F1ZXJ5Jyk7XG5cdFx0UHJvZHVjdFN0b3JlLnJlaHlkcmF0ZSgncmVzdWx0cycpO1xuXHRcdENvbXBhcmlzb25TdG9yZS5yZWh5ZHJhdGUoJ2NvbXBhcmlzb25zJyk7XG4gICAgfVxuXG5cbi8vICAgIENvbW1vbkFjdGlvbnMuZmV0Y2hPcHRpb25zKFtcbi8vXHRcdCdCMDAwMTA0MSdcbi8vXHRdKTtcblxuXG5cdGlmKGlzTG9nZ2VkSW4pIHtcblx0XHRDb21tb25BY3Rpb25zLmZldGNoTXlQcm9kdWN0cyhtYWlubWVudSwgc3VibWVudSk7XG5cdH1cblxuXHRsZXQgcHJvZHVjdEFkZGVyID0gPExvYW5Qcm9kdWN0QWRkZXIgLz47XG5cblx0bGV0IHNlYXJjaENhbGxiYWNrID0gZ2V0U2VhcmNoQ2FsbGJhY2soc2VhcmNoUXVlcnkpOyAvLyBpZiBzZWFyY2hRdWVyeSBnaXZlbiBpbiBhZHZhbmNlLCBzZWFyY2ggcmlnaHQgYXdheVxuXHRcblxuICAgIFJlYWN0RE9NLnJlbmRlcihcblx0XHQ8TG9hbkhlbHBlclNlY3Rpb25cblx0XHRcdGFkZGVyTW9kYWw9e3Byb2R1Y3RBZGRlcn1cblx0XHQvPixcblx0XHRkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaGVscGVyJyksXG5cdFx0c2VhcmNoQ2FsbGJhY2tcbiAgICApO1xuXG5cdFJlYWN0RE9NLnJlbmRlcihcblx0XHQ8TG9hblJlc3VsdFNlY3Rpb24gLz4sXG5cdFx0ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Jlc3VsdHMnKVxuICAgICk7XG5cblxufSkoKTtcbiIsImxldCBSZWFjdCA9IHJlcXVpcmUoJ3JlYWN0JyksXHJcblx0UHJvZHVjdENhcmRcdFx0XHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi9jb21wb25lbnRzL1Byb2R1Y3RDYXJkLmpzeCcpLFxyXG5cdG51bWJlcldpdGhDb21tYXNcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL251bWJlckZpbHRlckZ1bmN0aW9ucycpLm51bWJlcldpdGhDb21tYXM7XHJcblxyXG5cclxuY2xhc3MgTGlzdE9wdHMge1xyXG5cdGNvbnN0cnVjdG9yKGxhYmVsLCB2YWx1ZSkge1xyXG5cdFx0dGhpcy5sYWJlbCA9IGxhYmVsO1xyXG5cdFx0dGhpcy5kYXRhQXR0cnMgPSB7IHZhbHVlOiB2YWx1ZSB9O1xyXG5cdH1cclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0TG9hbkZpbHRlckxpc3QoKSB7XHJcblx0cmV0dXJuIFtcclxuXHRcdG5ldyBMaXN0T3B0cygn7KCE7LK0JywgJzk5OTk5OTk5JyksXHJcblx0XHRuZXcgTGlzdE9wdHMoJzHquIjsnLXqtownLCAnRklSU1QnKSxcclxuXHRcdG5ldyBMaXN0T3B0cygn7KCA7LaV7J2A7ZaJJywgJ1NFQ09ORCcpLFxyXG5cdFx0bmV3IExpc3RPcHRzKCfquLDtg4AnLCAnRUxTRScpXHJcblx0XTtcclxufVxyXG5cclxuZnVuY3Rpb24gYW1vdW50V2l0aFVuaXQoYW1vdW50KSB7XHJcblx0bGV0IHBhcnNlZCA9ICcnO1xyXG5cdGlmKGFtb3VudCA+PSAxMDAwMCkge1xyXG5cdFx0cGFyc2VkICs9IChwYXJzZUludChhbW91bnQgLyAxMDAwMCkgKyAn7Ja1Jyk7XHJcblx0fVxyXG5cdGlmKGFtb3VudCAlIDEwMDAwICE9PSAwKSB7XHJcblx0XHRwYXJzZWQgKz0gKChhbW91bnQgJSAxMDAwMCkgKyAn66eMJyk7XHJcblx0fVxyXG5cdHBhcnNlZCArPSAn7JuQJztcclxuXHJcblx0cmV0dXJuIHBhcnNlZDtcclxufVxyXG5cclxuXHJcblxyXG5sZXQgTG9hblJlc3VsdFNlY3Rpb24gPSBSZWFjdC5jcmVhdGVDbGFzcyh7XHJcblx0bWl4aW5zOiBbXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcXVlcnlTdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcHJvZHVjdFN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9jb21wYXJpc29uU3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL3NlbmRRdWVyeU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvcmVzdWx0U2VjdGlvbk1peGluLmpzeCcpXHJcblx0XSxcclxuXHRcclxuICAgIHByb3BUeXBlczoge1xyXG5cclxuICAgIH0sXHJcblxyXG4gICAgZ2V0RGVmYXVsdFByb3BzOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG5cdFx0XHRcclxuICAgICAgICB9O1xyXG4gICAgfSxcclxuXHJcbiAgICBnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcblxyXG5cdGNvbXBvbmVudERpZE1vdW50KCkge1xyXG5cclxuXHR9LFxyXG5cclxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50OiBmdW5jdGlvbigpIHtcclxuXHJcbiAgICB9LFxyXG5cclxuXHRfZ2V0T3B0aW9ucyhzdWJtZW51LCBvcHRDb2RlTWFwKSB7XHJcblx0XHQvLyAgcmV0dXJucyB7ZmlsdGVyT3B0aW9ucywgc29ydE9wdGlvbnMsIHRvb2x0aXBFbGVtLCByZXN1bHRzRWxlbSwgaGVscGVyRWxlbX1cclxuXHRcdC8vXHJcblx0XHQvLyAgICB0b29sdGlwRWxlbSAtPiB0b29sdGlwIGVsZW1lbnQgdG8gZ28gaW4gc29ydGluZyBzZWN0aW9uXHJcblx0XHQvLyAgICByZXN1bHRzRWxlbSAtPiBsaXN0IG9mIGZpbGxlZCBpbiByZXN1bHRzIGVsZW1lbnRzIChQcm9kdWN0Q2FyZCdzKVxyXG5cdFx0Ly8gICAgaGVscGVyRWxlbSAgLT4gKGlmIG5lZWRlZCkgbW9kYWwgdG8gdXNlIG9yIGFueSBvdGhlciBlbGVtZW50XHJcblx0XHRcclxuXHRcdGxldCB7IHJlc3VsdFN0YXRlLCBjb21wYXJpc29uU3RhdGUgfSA9IHRoaXMuc3RhdGU7XHJcblx0XHRcclxuXHRcdGxldCB7IGlzQ21wclRhYkFjdGl2ZSwgaXNMb2dnZWRJbiwgY29tcGFyaXNvbkdyb3VwLFx0Y3VycmVudENtcHIgfSA9IGNvbXBhcmlzb25TdGF0ZTtcclxuXHRcdGxldCBzaG93aW5nQ21wciA9IChpc0xvZ2dlZEluICYmIGlzQ21wclRhYkFjdGl2ZSAmJiBjb21wYXJpc29uR3JvdXAubGVuZ3RoID4gMCk7XHJcblx0XHRsZXQgc2VsZWN0ZWRDbXByID0gY29tcGFyaXNvbkdyb3VwW2N1cnJlbnRDbXByXTtcclxuXHRcdFxyXG5cdFx0bGV0IGZpbHRlck9wdGlvbnMsIHNvcnRPcHRpb25zLCB0b29sdGlwRWxlbSwgcmVzdWx0c0VsZW0sIGhlbHBlckVsZW07XHJcblx0XHRsZXQgY29tcGFyaXNvbkNvbnRlbnRzID0gdW5kZWZpbmVkO1xyXG5cdFx0XHJcblx0XHRzd2l0Y2goc3VibWVudS50b0xvd2VyQ2FzZSgpKSB7XHJcblx0XHRcdC8vIOyLoOyaqeuMgOy2nFxyXG5cdFx0XHRjYXNlKCdzaWduYXR1cmUnKTpcclxuXHRcdFx0Ly8g64u067O064yA7LacXHJcblx0XHRcdGNhc2UoJ3NlY3VyZWQnKTpcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gZ2V0TG9hbkZpbHRlckxpc3QoKTtcclxuXHRcdFx0XHRmaWx0ZXJPcHRpb25zID0gdGhpcy5fYWRkTnVtUmVzdWx0cyhmaWx0ZXJPcHRpb25zKTtcclxuXHRcdFx0XHRcclxuXHRcdFx0XHRzb3J0T3B0aW9ucyA9IFtcclxuXHRcdFx0XHRcdG5ldyBMaXN0T3B0cygn7KCA6riI66as7IicJywgJ1JBVEUnKSxcclxuXHRcdFx0XHRcdG5ldyBMaXN0T3B0cygn7LWc6rOg7ZWc64+E7IicJywgJ0FNT1VOVCcpLFxyXG5cdFx0XHRcdFx0bmV3IExpc3RPcHRzKCfstZzsnqXquLDqsITsiJwnLCAnVEVSTScpLFxyXG5cdFx0XHRcdF07IC8vIFRPRE86IG1vZGlmeSB0aGlzIHRvIHJlYWwgY29kZVxyXG5cdFx0XHRcdGlmKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSA9PT0gJ3NlY3VyZWQnKSB7XHJcblx0XHRcdFx0XHRzb3J0T3B0aW9ucy5wdXNoKG5ldyBMaXN0T3B0cygn64u067O07J247KCV67mE7Jyo7IicJywgJ0xOX1JBVEUnKSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdFxyXG5cdFx0XHRcdFxyXG5cclxuXHRcdFx0XHR0b29sdGlwRWxlbSA9IHVuZGVmaW5lZDtcclxuXHJcblx0XHRcdFx0cmVzdWx0c0VsZW0gPSByZXN1bHRTdGF0ZS5wcm9kdWN0RGF0YS5tYXAoIChlbGVtLCBpZHgpID0+IHtcclxuXHRcdFx0XHRcdGxldCB7XHJcblx0XHRcdFx0XHRcdExBX0NPREUsIC8vIOyDge2SiOy9lOuTnFxyXG5cdFx0XHRcdFx0XHRMTl9GTl9DT0RFX05NLCAvLyDsubTthYzqs6DrpqwgKDHquIjsnLXqtowsIDLquIjsnLXqtowsIC4uLiksXHJcblx0XHRcdFx0XHRcdEJBTktfSU1HLFxyXG5cdFx0XHRcdFx0XHRMTl9SQVRFX0xPVywgLy/stZzsoIDsl7DsnbTsnKhcclxuXHJcblx0XHRcdFx0XHRcdExOX05NLCAvLyDsg4HtkojrqoVcclxuXHRcdFx0XHRcdFx0TE5fU1VNTUFSWTEsIC8v7IOB7ZKI7ISk66qFIDFcclxuXHRcdFx0XHRcdFx0TE5fU1VNTUFSWTIsIC8v7IOB7ZKI7ISk66qFIDJcclxuXHRcdFx0XHRcdFx0TE5fTUlOSU1VTSwgLy/rjIDstpzquIjslaEg7LWc7IaMXHJcblx0XHRcdFx0XHRcdExOX01BWElNVU0sIC8v64yA7Lac6riI7JWhIOy1nOuMgFxyXG5cdFx0XHRcdFx0XHRMTl9OT01BTF9EQVRFX05NLCAvLyjstZzrjIAp64yA7Lac6riw6rCEXHJcblx0XHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0XHRMTl9DTEFTUzFfTk0sIC8v7Iug7Jqp65Ox6riJMVxyXG5cdFx0XHRcdFx0XHRMTl9DTEFTUzJfTk0sIC8v7Iug7Jqp65Ox6riJMlxyXG5cdFx0XHRcdFx0XHRMTl9SQVRFLCAvL+uLtOuztOyduOygleu5hOycqFxyXG5cclxuXHRcdFx0XHRcdFx0TE5fSVNfWU4sIC8vIOyYqOudvOyduOqwgOyehSDsl6zrtoBcclxuXHJcblx0XHRcdFx0XHRcdGludGVyZXN0X3luLCAvL+q0gOyLrOyDge2SiCDsl6zrtoBcclxuXHRcdFx0XHRcdFx0ZGV0YWlsX3VybFxyXG5cdFx0XHRcdFx0XHQvLyBpc05ldz9cclxuXHRcdFx0XHRcdH0gPSBlbGVtO1xyXG5cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0bGV0IHN1Ym1lbnVDb2RlO1xyXG5cdFx0XHRcdFx0bGV0IG1haW5BdHRyU2V0ID0ge1xyXG5cdFx0XHRcdFx0XHQn6riI7JWhJzogKExOX01JTklNVU0gIT09IDApID8gYCR7YW1vdW50V2l0aFVuaXQoTE5fTUlOSU1VTSl9IH4gJHthbW91bnRXaXRoVW5pdChMTl9NQVhJTVVNKX1gIDogKExOX01BWElNVU0gIT09IDAgPyBg7LWc6rOgICR7YW1vdW50V2l0aFVuaXQoTE5fTUFYSU1VTSl9YCA6ICfsnpDshLjtnojrs7TquLAnKSxcclxuXHRcdFx0XHRcdFx0J+q4sOqwhCc6IExOX05PTUFMX0RBVEVfTk0gPyBg7LWc64yAICR7TE5fTk9NQUxfREFURV9OTX1gIDogJy0nLFxyXG5cdFx0XHRcdFx0fTtcclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRcdFx0XHRjYXNlKCdzaWduYXR1cmUnKTpcclxuXHRcdFx0XHRcdFx0XHRzdWJtZW51Q29kZSA9IDIxMDA7XHJcblx0XHRcdFx0XHRcdFx0bWFpbkF0dHJTZXRbJ+yLoOyaqeuTseq4iSddID0gYCR7TE5fQ0xBU1MxX05NfSB+ICR7TE5fQ0xBU1MyX05NfWA7XHJcblx0XHRcdFx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHRcdGNhc2UoJ3NlY3VyZWQnKTpcclxuXHRcdFx0XHRcdFx0XHRzdWJtZW51Q29kZSA9IDIyMDA7XHJcblx0XHRcdFx0XHRcdFx0bWFpbkF0dHJTZXRbJ+uLtOuztOyduOygleu5hOycqCddID0gTE5fUkFURSA/IGAke0xOX1JBVEV9ICVgIDogJy0nO1xyXG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRcdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHJcblxyXG5cdFx0XHRcdFx0bGV0IGZlYXR1cmVzID0gW107XHJcblx0XHRcdFx0XHRpZihMTl9JU19ZTiA9PT0gJ1knKSB7XHJcblx0XHRcdFx0XHRcdGZlYXR1cmVzLnB1c2goJzxpIGNsYXNzPVwiYmFua2luZy1pY29uIGludGVybmV0XCI+PC9pPjxzcGFuPuyduO2EsOuEt+qwgOyehTwvc3Bhbj4nKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0XHRcdFx0aWYoc2hvd2luZ0NtcHIpIHtcclxuXHRcdFx0XHRcdFx0Y29tcGFyaXNvbkNvbnRlbnRzID0gW1xyXG5cdFx0XHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0XHRcdGF0dHI6ICfstZzsoIDquIjrpqzquLDspIAg7JewJyxcclxuXHRcdFx0XHRcdFx0XHRcdGRpZmY6IExOX1JBVEVfTE9XIC0gc2VsZWN0ZWRDbXByLmxuX3JhdGVfbG93LFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJyUnXHJcblx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRdO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGxldCBpc0ludGVyZXN0ZWQgPSAoaW50ZXJlc3RfeW4gPT09ICdZJyk7IC8vIOq0gOyLrOyDge2SiD9cclxuXHRcdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRyZXR1cm4gKFxyXG5cdFx0XHRcdFx0XHQ8UHJvZHVjdENhcmRcclxuXHRcdFx0XHRcdFx0XHRrZXk9e2Bsb2FuJHtMQV9DT0RFfWB9XHJcblx0XHRcdFx0XHRcdFx0dHlwZT0ndHlwZTEnXHJcblx0XHRcdFx0XHRcdFx0Y2F0ZWdvcnk9e0xOX0ZOX0NPREVfTk19XHJcblx0XHRcdFx0XHRcdFx0cHJvdmlkZXI9e0JBTktfSU1HfVxyXG5cdFx0XHRcdFx0XHRcdGxlYWRpbmdJbmZvPXtMTl9SQVRFX0xPV31cclxuXHRcdFx0XHRcdFx0XHRwcmVmaXg9J+yXsCdcclxuXHRcdFx0XHRcdFx0XHR1bml0PSclfidcclxuXHRcdFx0XHRcdFx0XHRwcm9kdWN0VGl0bGU9e0xOX05NfVxyXG5cdFx0XHRcdFx0XHRcdGZlYXR1cmVzPXtmZWF0dXJlc31cclxuXHRcdFx0XHRcdFx0XHRkZXNjcmlwdGlvbj17TE5fU1VNTUFSWTEgKyAnPGJyIC8+JyArIExOX1NVTU1BUlkyfVxyXG5cdFx0XHRcdFx0XHRcdG1haW5BdHRyU2V0PXttYWluQXR0clNldH1cclxuXHRcdFx0XHRcdFx0XHR0YXJnZXRNb2RhbFNlbGVjdG9yPScjcHJvZHVjdF92aWV3X3BvcHVwJ1xyXG5cdFx0XHRcdFx0XHRcdGRldGFpbFVybD17ZGV0YWlsX3VybH1cclxuXHRcdFx0XHRcdFx0XHRjb21wYXJpc29uQ29udGVudHM9e2NvbXBhcmlzb25Db250ZW50c31cclxuXHRcdFx0XHRcdFx0XHRpc0ludGVyZXN0ZWQ9e2lzSW50ZXJlc3RlZH1cclxuXHRcdFx0XHRcdFx0XHRpbmRleD17aWR4fVxyXG5cdFx0XHRcdFx0XHRcdGhhbmRsZUludGVyZXN0Q2xpY2s9e3RoaXMuaGFuZGxlSW50ZXJlc3RDbGljay5iaW5kKG51bGwsIGlzSW50ZXJlc3RlZCwgc3VibWVudUNvZGUsIExBX0NPREUsIGlkeCl9XHJcblx0XHRcdFx0XHRcdFx0Y2hvaWNlUmV2ZXJzZT17dHJ1ZX1cclxuXHRcdFx0XHRcdFx0Lz5cclxuXHRcdFx0XHRcdCk7XHJcblx0XHRcdFx0fSk7XHJcblxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cclxuXHJcblx0XHRcdGRlZmF1bHQ6XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHtmaWx0ZXJPcHRpb25zLCBzb3J0T3B0aW9ucywgdG9vbHRpcEVsZW0sIHJlc3VsdHNFbGVtLCBoZWxwZXJFbGVtfTtcclxuXHR9LFxyXG5cclxuXHRnZXRQcmVTZWFyY2hWaWV3KHN1Ym1lbnUpIHtcclxuXHRcdGxldCBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHRcdFxyXG5cdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xyXG5cdFx0XHRjYXNlKCdzaGlsc29uJyk6XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ2NhcicpOlxyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdFxyXG5cdFx0fVxyXG5cclxuXHRcdHJldHVybiBwcmVTZWFyY2hWaWV3RWxlbTtcclxuXHJcblx0fVxyXG5cclxuICAgIFxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gTG9hblJlc3VsdFNlY3Rpb247XHJcbiIsImxldCBSZWFjdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcblx0U2VsZWN0RHJvcGRvd25cdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL2NvbXBvbmVudHMvU2VsZWN0RHJvcGRvd24uanN4JyksXG5cdFRleHRJbnB1dFx0XHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi9jb21wb25lbnRzL1RleHRJbnB1dC5qc3gnKSxcblx0QXBpXHRcdFx0XHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9hcGknKSxcblx0Q29tbW9uQWN0aW9uc1x0PSByZXF1aXJlKCcuLi8uLi9jb21tb24vYWN0aW9ucy9Db21tb25BY3Rpb25zJyksXG5cdGZvcm1WYWxpZGF0b3JzXHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2Zvcm1WYWxpZGF0b3JzJyk7XG5cbmxldCB7IHNlbGVjdFJlcXVpcmVkLCBudW1iZXJSZXF1aXJlZCB9ID0gZm9ybVZhbGlkYXRvcnM7XG5cbmNvbnN0IHN1Ym1lbnVfY29kZXMgPSByZXF1aXJlKCcuLi8uLi9jb21tb24vY29uc3RhbnRzL3N1Ym1lbnVfY29kZXMnKTtcblxuY29uc3QgbG9hbk1lbnVDb2RlcyA9IHtcblx0c2lnbmF0dXJlOiAnQjAwMDEwMScsXG5cdHNlY3VyZWQ6ICdCMDAwMTAyJ1xufTtcblxuY29uc3QgdGVybU9wdHMgPSBbXG5cdHtcblx0XHR2YWx1ZTogJ0IwMDAxMDkwMScsXG5cdFx0bGFiZWw6ICc26rCc7JuUJ1xuXHR9LFxuXHR7XG5cdFx0dmFsdWU6ICdCMDAwMTA5MDInLFxuXHRcdGxhYmVsOiAnMeuFhCdcblx0fSxcblx0e1xuXHRcdHZhbHVlOiAnQjAwMDEwOTAzJyxcblx0XHRsYWJlbDogJzPrhYQnXG5cdH0sXG5cdHtcblx0XHR2YWx1ZTogJ0IwMDAxMDkwNCcsXG5cdFx0bGFiZWw6ICc164WEJ1xuXHR9LFxuXHR7XG5cdFx0dmFsdWU6ICdCMDAwMTA5MDUnLFxuXHRcdGxhYmVsOiAnN+uFhCdcblx0fSxcblx0e1xuXHRcdHZhbHVlOiAnQjAwMDEwOTA2Jyxcblx0XHRsYWJlbDogJzEw64WEJ1xuXHR9LFxuXHR7XG5cdFx0dmFsdWU6ICdCMDAwMTA5MDcnLFxuXHRcdGxhYmVsOiAnMTXrhYQnXG5cdH0sXG5cdHtcblx0XHR2YWx1ZTogJ0IwMDAxMDkwOCcsXG5cdFx0bGFiZWw6ICcyMOuFhCdcblx0fSxcblx0e1xuXHRcdHZhbHVlOiAnQjAwMDEwOTA5Jyxcblx0XHRsYWJlbDogJzI164WEJ1xuXHR9LFxuXHR7XG5cdFx0dmFsdWU6ICdCMDAwMTA5MTAnLFxuXHRcdGxhYmVsOiAnMzDrhYQnXG5cdH0sXG5dO1xuXG5cbmZ1bmN0aW9uIGNvbnZlcnRUZXJtQ29kZSh0ZXJtQ2QpIHtcblx0bGV0IHRlcm1LZXksIHRlcm1OdW07XG5cdFxuXHRzd2l0Y2godGVybUNkKSB7XG5cdFx0Y2FzZSgnQTAwMDEwMTAxMDEnKTpcblx0XHRcdHRlcm1LZXkgPSAnRFNfUkFURTMnO1xuXHRcdFx0dGVybU51bSA9ICczJztcblx0XHRcdGJyZWFrO1xuXHRcdGNhc2UoJ0EwMDAxMDEwMTAyJyk6XG5cdFx0XHR0ZXJtS2V5ID0gJ0RTX1JBVEU2Jztcblx0XHRcdHRlcm1OdW0gPSAnNic7XG5cdFx0XHRicmVhaztcblx0XHRjYXNlKCdBMDAwMTAxMDEwMycpOlxuXHRcdFx0dGVybUtleSA9ICdEU19SQVRFMTInO1xuXHRcdFx0dGVybU51bSA9ICcxMic7XG5cdFx0XHRicmVhaztcblx0XHRjYXNlKCdBMDAwMTAxMDEwNCcpOlxuXHRcdFx0dGVybUtleSA9ICdEU19SQVRFMjQnO1xuXHRcdFx0dGVybU51bSA9ICcyNCc7XG5cdFx0XHRicmVhaztcblx0XHRjYXNlKCdBMDAwMTAxMDEwNScpOlxuXHRcdFx0dGVybUtleSA9ICdEU19SQVRFMzYnO1xuXHRcdFx0dGVybU51bSA9ICczNic7XG5cdFx0XHRicmVhaztcblx0XHRkZWZhdWx0OlxuXHRcdFx0dGVybUtleSA9ICdEU19QUkVfVEFYJ1xuXHRcdFx0dGVybU51bSA9ICcwJztcblx0fVxuXG5cdHJldHVybiB7IHRlcm1LZXksIHRlcm1OdW0gfTtcbn1cblxuXG5cbmNsYXNzIFNlbGVjdE9wdHMge1xuXHRjb25zdHJ1Y3RvcihsYWJlbCwgdmFsdWUsIGRhdGEpIHtcblx0XHR0aGlzLmxhYmVsID0gbGFiZWw7XG5cdFx0dGhpcy52YWx1ZSA9IHZhbHVlO1xuXHRcdHRoaXMuZGF0YSA9IGRhdGE7XG5cdH1cbn1cblxuXG5sZXQgTG9hblByb2R1Y3RBZGRlciA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcbiAgICBwcm9wVHlwZXM6IHtcblx0XHRzdWJtZW51OiBSZWFjdC5Qcm9wVHlwZXMuc3RyaW5nLFxuXHRcdHNlYXJjaEVudHJpZXM6IFJlYWN0LlByb3BUeXBlcy5hcnJheSxcblx0XHRvcHRDb2RlTWFwOiBSZWFjdC5Qcm9wVHlwZXMub2JqZWN0XG4gICAgfSxcblxuICAgIGdldERlZmF1bHRQcm9wcygpIHtcbiAgICAgICAgcmV0dXJuIHtcblx0XHRcdHN1Ym1lbnU6ICcnLFxuXHRcdFx0c2VhcmNoRW50cmllczogW10sXG5cdFx0XHRvcHRDb2RlTWFwOiB7fVxuICAgICAgICB9O1xuICAgIH0sXG5cdFxuXHRnZXRJbml0aWFsU3RhdGU6IGZ1bmN0aW9uKCkge1xuXHRcdGxldCBwbGFjZWhvbGRlcjtcblxuXHRcdGlmKHRoaXMucHJvcHMuc3VibWVudS50b0xvd2VyQ2FzZSgpID09PSAnc2VjdXJlZCcpIHtcblx0XHRcdHBsYWNlaG9sZGVyID0gJ+uLtOuztOycoO2YleqzvCDquLDqtIDrqoXsnYQg7ISg7YOd7ZW07KO87IS47JqULic7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHBsYWNlaG9sZGVyID0gJ+q4sOq0gOuqheydhCDshKDtg53tlbTso7zshLjsmpQuJztcblx0XHR9XG5cdFx0XG4gICAgICAgIHJldHVybiB7XG5cdFx0XHRzaG93VmFsaWRhdGlvbjogZmFsc2UsXG5cdFx0XHRjb21wYW55X2NvZGU6ICcnLFxuXHRcdFx0cHJvZF9jb2RlOiAnJyxcblx0XHRcdHByb2RfbmFtZTogJycsXG5cdFx0XHRhbW91bnQ6ICcnLFxuXHRcdFx0dGVybTogJycsXG5cdFx0XHRpbnRlcmVzdDogJycsXG5cdFx0XHRzZWN1cmVUeXBlOiAnJyxcblx0XHRcdGNvbXBhbnlPcHRzOiBbXSxcblx0XHRcdHByb2ROYW1lT3B0czogW10sXG5cdFx0XHRyZXBheVR5cGU6ICcnLFxuXG5cdFx0XHRwcm9kTmFtZVBsYWNlaG9sZGVyOiBwbGFjZWhvbGRlclxuICAgICAgICB9O1xuICAgIH0sXG4gICAgXG5cbiAgICBjb21wb25lbnREaWRNb3VudCgpIHtcblx0XHRsZXQgY2F0ZWdvcnkgPSBsb2FuTWVudUNvZGVzW3RoaXMucHJvcHMuc3VibWVudS50b0xvd2VyQ2FzZSgpXTtcblxuXHRcdEFwaS5wb3N0KCcvbW9waWMvYXBpL2xvYW4vc2VsZWN0TG9hbkJhbmtMaXN0Jywge2NhdGVnb3J5fSlcblx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXMpIHtcblx0XHRcdFx0aWYocmVzLnJlc3VsdC5yZXN1bHRDb2RlID09PSAnU1VDQ0VTUycpIHtcblx0XHRcdFx0XHRsZXQgcmVzdWx0RGF0YSA9IHJlcy5yZXN1bHQuZGF0YTtcblx0XHRcdFx0XHRsZXQgb3B0aW9ucyA9ICggcmVzdWx0RGF0YS5tYXAoKGUpID0+IG5ldyBTZWxlY3RPcHRzKGUuY2ROYW1lLCBlLmNkKSkgKTtcblx0XHRcdFx0XHRcblx0XHRcdFx0XHR0aGlzLnNldFN0YXRlKHtcblx0XHRcdFx0XHRcdGNvbXBhbnlPcHRzOiBvcHRpb25zXG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdH1cblx0XHRcdH0uYmluZCh0aGlzKSk7XG5cblx0XHRcblx0XHQvLyBjbGVhciB3aGVuIGNsb3NlZFxuXHRcdCQoJyNwcm9kdWN0X2FkZF9wb3B1cCcpLm9uKCdoaWRkZW4uYnMubW9kYWwnLCB0aGlzLmNsZWFyU3RhdHMpO1xuXG4gICAgfSxcblxuICAgIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuXHRcdFxuICAgIH0sXG5cbiAgICByZW5kZXIoKSB7XG5cdFx0bGV0IHtcblx0XHRcdHNob3dWYWxpZGF0aW9uLFxuXHRcdFx0Y29tcGFueU9wdHMsXG5cdFx0XHRjb21wYW55X2NvZGUsXG5cdFx0XHRwcm9kTmFtZU9wdHMsXG5cdFx0XHRpbnRlcmVzdCxcblx0XHRcdHNlY3VyZVR5cGUsXG5cdFx0XHRwcm9kX2NvZGUsXG5cdFx0XHR0ZXJtLFxuXHRcdFx0YW1vdW50LFxuXHRcdFx0cHJvZE5hbWVQbGFjZWhvbGRlclxuXHRcdH0gPSB0aGlzLnN0YXRlO1xuXHRcdGxldCB7IHN1Ym1lbnUsIHNlYXJjaEVudHJpZXMsIG9wdENvZGVNYXAgfSA9IHRoaXMucHJvcHM7XG5cblx0XHRsZXQgc2VjdXJpdHlGb3JtO1xuXHRcdGlmKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSA9PT0gJ3NlY3VyZWQnKSB7XG5cdFx0XHRsZXQgc2VjdXJpdHlFbnRyeSA9IHRoaXMucHJvcHMuc2VhcmNoRW50cmllcy5maW5kKChlKSA9PiBlLmlkID09PSAnc2VjdXJlVHlwZScpO1xuXHRcdFx0XG5cdFx0XHRzZWN1cml0eUZvcm0gPSAoXG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9J2NvbC14cy02Jz5cblx0XHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9J3NlbGVjdC1sYWJlbCc+e3NlY3VyaXR5RW50cnkubGFiZWwgfHwgJyd9PC9sYWJlbD5cblx0XHRcdFx0XHRcdDxTZWxlY3REcm9wZG93blxuXHRcdFx0XHRcdFx0XHR0YWJJbmRleD17MH1cblx0XHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9e3NlY3VyaXR5RW50cnkucGxhY2Vob2xkZXIgfHwgJ+yEoO2DnSd9XG5cdFx0XHRcdFx0XHRcdG9wdGlvbnM9e3NlY3VyaXR5RW50cnkub3B0aW9uc31cblx0XHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlY3VyaXR5U2VsZWN0fVxuXHRcdFx0XHRcdFx0XHRjbGFzc05hbWU9e3NlY3VyaXR5RW50cnkuY2xhc3NOYW1lIHx8ICdzZWxlY3QtZHJvcGRvd24nfVxuXHRcdFx0XHRcdFx0XHRzZWxlY3RlZD17c2VjdXJlVHlwZX1cblx0XHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtzaG93VmFsaWRhdGlvbiAmJiBzZWN1cml0eUVudHJ5LnZhbGlkYXRpb25Gbn1cblx0XHRcdFx0XHRcdC8+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0KTtcblx0XHR9XG5cblx0XHRsZXQgY29tcGFueUZvcm0gPSAoXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02XCI+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuq4sOq0gOuqhTwvbGFiZWw+XG5cdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0n7ISg7YOdJ1xuXHRcdFx0XHRcdFx0b3B0aW9ucz17Y29tcGFueU9wdHN9XG5cdFx0XHRcdFx0XHRoYW5kbGVTZWxlY3Q9e3RoaXMuaGFuZGxlQ29tcGFueVNlbGVjdH1cblx0XHRcdFx0XHRcdHNlbGVjdGVkPXtjb21wYW55X2NvZGV9XG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3Nob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxuXHRcdFx0XHRcdC8+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcblxuXHRcdGxldCBwcm9kdWN0TmFtZUZvcm0gPSAoXG5cdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cInJvd1wiPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cImNvbC14cy02XCI+XG5cdFx0XHRcdFx0PGxhYmVsIGNsYXNzTmFtZT1cInNlbGVjdC1sYWJlbFwiPuyDge2SiOuqhTwvbGFiZWw+XG5cdFx0XHRcdFx0PFNlbGVjdERyb3Bkb3duXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj17cHJvZE5hbWVQbGFjZWhvbGRlcn1cblx0XHRcdFx0XHRcdG9wdGlvbnM9e3Byb2ROYW1lT3B0c31cblx0XHRcdFx0XHRcdGhhbmRsZVNlbGVjdD17dGhpcy5oYW5kbGVQcm9kdWN0TmFtZVNlbGVjdH1cblx0XHRcdFx0XHRcdHNlbGVjdGVkPXtwcm9kX2NvZGV9XG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3Nob3dWYWxpZGF0aW9uICYmIHNlbGVjdFJlcXVpcmVkfVxuXHRcdFx0XHRcdC8+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcblxuXG5cdFx0XG5cdFx0bGV0IHRlcm1Gb3JtID0gKFxuXHRcdFx0PGRpdiBjbGFzc05hbWU9XCJyb3dcIj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjb2wteHMtNlwiPlxuXHRcdFx0XHRcdDxsYWJlbCBjbGFzc05hbWU9XCJzZWxlY3QtbGFiZWxcIj7rjIDstpzquLDqsIQ8L2xhYmVsPlxuXHRcdFx0XHRcdDxTZWxlY3REcm9wZG93blxuXHRcdFx0XHRcdFx0cGxhY2Vob2xkZXI9J+yEoO2DnSdcblx0XHRcdFx0XHRcdG9wdGlvbnM9e3Rlcm1PcHRzfVxuXHRcdFx0XHRcdFx0aGFuZGxlU2VsZWN0PXt0aGlzLmhhbmRsZVNlbGVjdC5iaW5kKG51bGwsICd0ZXJtJyl9XG5cdFx0XHRcdFx0XHRjbGFzc05hbWU9J3NlbGVjdC1kcm9wZG93bidcblx0XHRcdFx0XHRcdHNlbGVjdGVkPXt0ZXJtfVxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuPXtzaG93VmFsaWRhdGlvbiAmJiBzZWxlY3RSZXF1aXJlZH1cblx0XHRcdFx0XHQvPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdCk7XG5cblx0XHRsZXQgYW1vdW50Rm9ybSA9IChcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTZcIj5cblx0XHRcdFx0XHQ8bGFiZWwgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+64yA7Lac6riI7JWhPC9sYWJlbD5cblx0XHRcdFx0XHQ8VGV4dElucHV0XG5cdFx0XHRcdFx0XHR0eXBlPSdudW1iZXItd2l0aC1jb21tYXMnXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0nICdcblx0XHRcdFx0XHRcdGlucHV0Q2xhc3M9J3RleHQtaW5wdXQtbm9ybWFsJ1xuXHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZVRleHRJbnB1dENoYW5nZS5iaW5kKG51bGwsICdhbW91bnQnKX1cblx0XHRcdFx0XHRcdHZhbHVlPXthbW91bnR9XG5cdFx0XHRcdFx0XHR1bml0PSfrp4zsm5AnXG5cdFx0XHRcdFx0XHR2YWxpZGF0aW9uRm49e3Nob3dWYWxpZGF0aW9uICYmIG51bWJlclJlcXVpcmVkfVxuXHRcdFx0XHRcdC8+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcblxuXG5cdFx0XG5cdFx0bGV0IGludGVyZXN0Rm9ybSA9IChcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwicm93XCI+XG5cdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwiY29sLXhzLTZcIj5cblx0XHRcdFx0XHQ8bGFiZWwgY2xhc3NOYW1lPVwic2VsZWN0LWxhYmVsXCI+6riI66asPC9sYWJlbD5cblx0XHRcdFx0XHQ8VGV4dElucHV0XG5cdFx0XHRcdFx0XHR0eXBlPSdudW1iZXInXG5cdFx0XHRcdFx0XHRwbGFjZWhvbGRlcj0nICdcblx0XHRcdFx0XHRcdGlucHV0Q2xhc3M9J3RleHQtaW5wdXQtbm9ybWFsJ1xuXHRcdFx0XHRcdFx0aGFuZGxlQ2hhbmdlPXt0aGlzLmhhbmRsZVRleHRJbnB1dENoYW5nZS5iaW5kKG51bGwsICdpbnRlcmVzdCcpfVxuXHRcdFx0XHRcdFx0dmFsdWU9e2ludGVyZXN0fVxuXHRcdFx0XHRcdFx0dW5pdD0nJSdcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbj17c2hvd1ZhbGlkYXRpb24gJiYgbnVtYmVyUmVxdWlyZWR9XHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0Lz5cblx0XHRcdFx0PC9kaXY+XG5cdFx0XHQ8L2Rpdj5cblx0XHQpO1xuXG5cdFx0XG5cdFx0XG5cdFx0cmV0dXJuIChcblx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibW9kYWwgZmFkZVwiIGlkPVwicHJvZHVjdF9hZGRfcG9wdXBcIj5cblx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJtb2RhbC1kaWFsb2dcIj5cblx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWNvbnRlbnQgbW9kYWwtc21hbGxcIj5cblxuXHRcdFx0XHRcdFx0PGEgY2xhc3NOYW1lPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJtb2RhbFwiIGFyaWEtbGFiZWw9XCJDbG9zZVwiIC8+XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtaGVhZGVyXCI+XG5cdFx0XHRcdFx0XHRcdDxoNCBjbGFzc05hbWU9XCJtb2RhbC10aXRsZVwiIGlkPVwibXlNb2RhbExhYmVsXCI+6rCA7J6F7IOB7ZKIIOy2lOqwgO2VmOq4sDwvaDQ+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT1cIm1vZGFsLWJvZHlcIj5cblx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJwcm9kdWN0X2FkZFwiPlxuXG5cdFx0XHRcdFx0XHRcdFx0e3NlY3VyaXR5Rm9ybX1cblx0XHRcdFx0XHRcdFx0XHR7Y29tcGFueUZvcm19XG5cdFx0XHRcdFx0XHRcdFx0e3Byb2R1Y3ROYW1lRm9ybX1cblx0XHRcdFx0XHRcdFx0XHR7dGVybUZvcm19XG5cdFx0XHRcdFx0XHRcdFx0e2Ftb3VudEZvcm19XG5cdFx0XHRcdFx0XHRcdFx0e2ludGVyZXN0Rm9ybX1cblx0XHRcdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdFx0XHQ8c3BhbiBjbGFzc05hbWU9XCJ0eHRcIj4o6riI66as66W8IOyeheugpe2VmOyngCDslYrsnLzsi6Qg6rK97JqwLCDrk7HroZ3snbwg7ZiE7J6sIOq4sOykgCDquIjrpqzroZwg7KCB7Jqp65Cp64uI64ukLik8L3NwYW4+XG5cdFx0XHRcdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9XCJjZW50ZXJfYnRuXCI+XG5cdFx0XHRcdFx0XHRcdFx0XHQ8YSBjbGFzc05hbWU9XCJidG4gYnRuLXN1Ym1pdFwiIHJvbGU9XCJidXR0b25cIiBvbkNsaWNrPXt0aGlzLnN1Ym1pdFByb2R1Y3R9Pu2ZleyduDwvYT5cblx0XHRcdFx0XHRcdFx0XHQ8L2Rpdj5cblx0XHRcdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdFx0XHQ8L2Rpdj5cblxuXHRcdFx0XHRcdFx0XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0PC9kaXY+XG5cdFx0KTtcbiAgICB9LFxuXG5cdGhhbmRsZVNlbGVjdChhdHRyLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblx0XHRcblx0XHRsZXQgdmFsID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ2xpJykuYXR0cignZGF0YS12YWwnKSB8fCB0YXJnZXQudGV4dENvbnRlbnQ7XG5cdFx0XG5cdFx0dGhpcy5zZXRTdGF0ZSh7ICAvLyByZXNldCBwcm9kX2NvZGUgYW5kIGludGVyZXN0IGFzIHdlbGxcblx0XHRcdFthdHRyXTogdmFsXG5cdFx0fSk7XG5cdH0sXG5cblx0XG5cdGhhbmRsZVRleHRJbnB1dENoYW5nZShhdHRyLCBldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblx0XHRcblx0XHRsZXQgdmFsID0gdGFyZ2V0LnZhbHVlO1xuXHRcdFxuXHRcdHRoaXMuc2V0U3RhdGUoeyAgLy8gcmVzZXQgcHJvZF9jb2RlIGFuZCBpbnRlcmVzdCBhcyB3ZWxsXG5cdFx0XHRbYXR0cl06IHZhbFxuXHRcdH0pO1xuXHR9LFxuXG5cblxuXG5cdGhhbmRsZVByb2R1Y3ROYW1lU2VsZWN0KGV2dCkge1xuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xuXHRcdFxuXHRcdGxldCBwcm9kQ2QgPSAkKHRhcmdldCkuY2xvc2VzdCgnbGknKS5hdHRyKCdkYXRhLXZhbCcpIHx8IHRhcmdldC50ZXh0Q29udGVudDtcblx0XHRsZXQgaW50ZXJlc3QgPSB0aGlzLnN0YXRlLnByb2ROYW1lT3B0cy5maW5kKChlKSA9PiAoZS52YWx1ZSA9PT0gcHJvZENkKSkuZGF0YS5MTl9SQVRFX0xPVztcblxuXHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0cHJvZF9jb2RlOiBwcm9kQ2QsXG5cdFx0XHRpbnRlcmVzdDogKCAoaW50ZXJlc3QgfHwgaW50ZXJlc3QgPT09IDApICYmIFN0cmluZyhpbnRlcmVzdCkgKSB8fCAnJ1xuXHRcdH0pO1xuXHR9LFxuXG5cdFxuXHRoYW5kbGVTZWN1cml0eVNlbGVjdChldnQpIHtcblx0XHRldnQgPSBldnQ/IGV2dCA6IHdpbmRvdy5ldmVudDtcblx0XHRsZXQgdGFyZ2V0ID0gZXZ0LnRhcmdldCB8fCBldnQuc3JjRWxlbWVudDtcblx0XHRcdFxuXHRcdGxldCBzZWN1cmVUeXBlID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ2xpJykuYXR0cignZGF0YS12YWwnKSB8fCB0YXJnZXQudGV4dENvbnRlbnQ7XG5cdFx0bGV0IHsgY29tcGFueV9jb2RlIH0gPSB0aGlzLnN0YXRlO1xuXHRcdFxuXHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0c2VjdXJlVHlwZSxcblx0XHRcdHByb2RfY29kZTogJydcblx0XHR9KTtcblxuXHRcdGlmKGNvbXBhbnlfY29kZSkge1xuXHRcdFx0dGhpcy5mZXRjaFByb2R1Y3ROYW1lcyhjb21wYW55X2NvZGUsIHNlY3VyZVR5cGUpO1xuXHRcdH1cblx0XHRcblx0fSxcblxuXHRoYW5kbGVDb21wYW55U2VsZWN0KGV2dCkge1xuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xuXHRcdFxuXHRcdGxldCBsbkJhbmtpbmdDb2RlID0gJCh0YXJnZXQpLmNsb3Nlc3QoJ2xpJykuYXR0cignZGF0YS12YWwnKSB8fCB0YXJnZXQudGV4dENvbnRlbnQ7XG5cblx0XHR0aGlzLnNldFN0YXRlKHtcblx0XHRcdGNvbXBhbnlfY29kZTogbG5CYW5raW5nQ29kZSxcblx0XHRcdHByb2RfY29kZTogJycsXG5cdFx0XHRwcm9kTmFtZU9wdHM6IFtdLFxuXHRcdFx0aW50ZXJlc3Q6ICcnXG5cdFx0fSk7XG5cblx0XHRsZXQgeyBzZWN1cmVUeXBlIH0gPSB0aGlzLnN0YXRlO1xuXG5cdFx0aWYoc2VjdXJlVHlwZSB8fCB0aGlzLnByb3BzLnN1Ym1lbnUudG9Mb3dlckNhc2UoKSAhPT0gJ3NlY3VyZWQnKSB7XG5cdFx0XHR0aGlzLmZldGNoUHJvZHVjdE5hbWVzKGxuQmFua2luZ0NvZGUsIHNlY3VyZVR5cGUpO1xuXHRcdH1cblx0fSxcblxuXHRmZXRjaFByb2R1Y3ROYW1lcyhjb21wYW55X2NvZGUsIHNlY3VyZVR5cGUpIHtcblx0XHRsZXQgbG5Db2RlID0gbG9hbk1lbnVDb2Rlc1t0aGlzLnByb3BzLnN1Ym1lbnUudG9Mb3dlckNhc2UoKV07XG5cdFx0XG5cdFx0QXBpLnBvc3QoJy9tb3BpYy9hcGkvbG9hbi9maWx0ZXJQcm9kdWN0TGlzdCcsIHsgbG5CYW5raW5nQ29kZTogY29tcGFueV9jb2RlLCBsbkNvZGUsIHNlY3VyZVR5cGUgfSlcblx0XHRcdC50aGVuKGZ1bmN0aW9uIChyZXMpIHtcblx0XHRcdFx0aWYocmVzLnJlc3VsdC5yZXN1bHRDb2RlID09PSAnU1VDQ0VTUycpIHtcblx0XHRcdFx0XHRsZXQgcmVzdWx0RGF0YSA9IHJlcy5yZXN1bHQuZGF0YTtcblxuXHRcdFx0XHRcdGlmKHJlc3VsdERhdGEgJiYgcmVzdWx0RGF0YS5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdFx0XHRsZXQgb3B0aW9ucyA9ICggcmVzdWx0RGF0YS5tYXAoKGUpID0+IG5ldyBTZWxlY3RPcHRzKGUuTE5fTk0sIGUuTEFfQ09ERSwgZSkpICk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0XHRcdFx0XHRwcm9kTmFtZU9wdHM6IG9wdGlvbnMsXG5cdFx0XHRcdFx0XHRcdHByb2ROYW1lUGxhY2Vob2xkZXI6ICfshKDtg50nXG5cdFx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRcdFx0XHRcdHByb2ROYW1lUGxhY2Vob2xkZXI6ICftlbTri7kg7KGw6rG07J2YIOyDge2SiOydtCDsobTsnqztlZjsp4Ag7JWK7Iq164uI64ukJ1xuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9LmJpbmQodGhpcykpO1xuXHR9LFxuXG5cblx0Y2xlYXJTdGF0cygpIHtcblx0XHQvLyBjbGVhciBldmVyeSBzdGF0ZSBidXQgY29tcGFueSBvcHRpb25zXG5cdFx0dGhpcy5zZXRTdGF0ZSh7XG5cdFx0XHRzaG93VmFsaWRhdGlvbjogZmFsc2UsXG5cdFx0XHRjb21wYW55X2NvZGU6ICcnLFxuXHRcdFx0cHJvZF9jb2RlOiAnJyxcblx0XHRcdHByb2RfbmFtZTogJycsXG5cdFx0XHRhbW91bnQ6ICcnLFxuXHRcdFx0dGVybTogJycsXG5cdFx0XHRpbnRlcmVzdDogJycsXG5cdFx0XHRzZWN1cmVUeXBlOiAnJyxcblx0XHRcdHByb2ROYW1lT3B0czogW10sXG5cdFx0XHRwcm9kTmFtZVBsYWNlaG9sZGVyOiAn64u067O07Jyg7ZiV6rO8IOq4sOq0gOuqheydhCDshKDtg53tlZjshLjsmpQuJ1xuXHRcdH0pO1xuXHR9LFxuXG5cdGNoZWNrVmFsaWRpdHkoKSB7XG5cdFx0bGV0IHtcblx0XHRcdGNvbXBhbnlfY29kZSxcblx0XHRcdHByb2RfY29kZSxcblx0XHRcdGFtb3VudCxcblx0XHRcdHRlcm0sXG5cdFx0XHRpbnRlcmVzdCxcblx0XHRcdHNlY3VyZVR5cGVcblx0XHR9ID0gdGhpcy5zdGF0ZTtcblx0XHRcblx0XHRsZXQgYWxsVmFsaWQgPSB0cnVlO1xuXHRcdFxuXHRcdGFsbFZhbGlkID0gYWxsVmFsaWQgJiYgKHNlbGVjdFJlcXVpcmVkKGNvbXBhbnlfY29kZSkgPT09IHRydWUpO1xuXHRcdGFsbFZhbGlkID0gYWxsVmFsaWQgJiYgKHNlbGVjdFJlcXVpcmVkKHByb2RfY29kZSkgPT09IHRydWUpO1xuXHRcdGFsbFZhbGlkID0gYWxsVmFsaWQgJiYgKHNlbGVjdFJlcXVpcmVkKHRlcm0pID09PSB0cnVlKTtcblx0XHRhbGxWYWxpZCA9IGFsbFZhbGlkICYmIChudW1iZXJSZXF1aXJlZChhbW91bnQpID09PSB0cnVlKTtcblx0XHRhbGxWYWxpZCA9IGFsbFZhbGlkICYmIChudW1iZXJSZXF1aXJlZChpbnRlcmVzdCkgPT09IHRydWUpO1xuXG5cdFx0aWYodGhpcy5wcm9wcy5zdWJtZW51LnRvTG93ZXJDYXNlKCkgPT09ICdzZWN1cmVkJykge1xuXHRcdFx0YWxsVmFsaWQgPSBhbGxWYWxpZCAmJiAoc2VsZWN0UmVxdWlyZWQoc2VjdXJlVHlwZSkgPT09IHRydWUpO1xuXHRcdH1cblx0XHRcblx0XHRyZXR1cm4gYWxsVmFsaWQ7XG5cdH0sXG5cdFxuXHRzdWJtaXRQcm9kdWN0KGV2dCkge1xuXHRcdGV2dCA9IGV2dD8gZXZ0IDogd2luZG93LmV2ZW50O1xuXHRcdGxldCB0YXJnZXQgPSBldnQudGFyZ2V0IHx8IGV2dC5zcmNFbGVtZW50O1xuXHRcdFx0XG5cdFx0bGV0IHtcblx0XHRcdGNvbXBhbnlfY29kZSxcblx0XHRcdHByb2RfY29kZSxcblx0XHRcdGFtb3VudCxcblx0XHRcdHRlcm0sXG5cdFx0XHRpbnRlcmVzdCxcblx0XHRcdHNlY3VyZVR5cGUsXG5cdFx0XHRwcm9kTmFtZU9wdHNcblx0XHR9ID0gdGhpcy5zdGF0ZTtcblx0XHRsZXQgeyBzdWJtZW51LCBzZWFyY2hFbnRyaWVzIH0gPSB0aGlzLnByb3BzO1xuXHRcdGxldCBkYXRhO1xuXG5cdFx0bGV0IGFsbFZhbGlkID0gdGhpcy5jaGVja1ZhbGlkaXR5KCk7XG5cblx0XHRpZihhbGxWYWxpZCkge1xuXHRcdFx0bGV0IGRhdGEgPSB7fTtcblx0XHRcdFxuXHRcdFx0c3dpdGNoKHN1Ym1lbnUudG9Mb3dlckNhc2UoKSkge1xuXHRcdFx0XHRjYXNlKCdzZWN1cmVkJyk6XG5cdFx0XHRcdFx0ZGF0YS5zZWN1cmVUeXBlID0gc2VjdXJlVHlwZTtcblx0XHRcdFx0Y2FzZSgnc2lnbmF0dXJlJyk6XG5cdFx0XHRcdFx0ZGF0YSA9IE9iamVjdC5hc3NpZ24oe30sIGRhdGEsIHtcblx0XHRcdFx0XHRcdHByb2R1Y3RfdHlwZTogU3RyaW5nKHN1Ym1lbnVfY29kZXNbc3VibWVudV0pLFxuXHRcdFx0XHRcdFx0cHJvZF9jb2RlOiBTdHJpbmcocHJvZF9jb2RlKSxcblx0XHRcdFx0XHRcdHByb2RfbmFtZTogU3RyaW5nKHByb2ROYW1lT3B0cy5maW5kKChlbGVtKSA9PiAoZWxlbS52YWx1ZSA9PT0gcHJvZF9jb2RlKSkubGFiZWwpLFxuXHRcdFx0XHRcdFx0YW1vdW50OiBTdHJpbmcoYW1vdW50KSxcblx0XHRcdFx0XHRcdHRlcm1fY29kZTogU3RyaW5nKHRlcm0pLFxuXHRcdFx0XHRcdFx0aW50ZXJlc3Q6IFN0cmluZyhpbnRlcmVzdClcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdH1cblxuXHRcdFx0bGV0ICR0YXJnZXQgPSAkKHRhcmdldCk7XG5cdFx0XHRDb21tb25BY3Rpb25zLmFkZE15UHJvZHVjdChkYXRhLCAoKSA9PiB7XG5cdFx0XHRcdCR0YXJnZXQuY2xvc2VzdCgnLm1vZGFsJykubW9kYWwoJ2hpZGUnKTtcblx0XHRcdH0pO1xuXG5cdFx0fSBlbHNlIHtcblx0XHRcdHRoaXMuc2V0U3RhdGUoe1xuXHRcdFx0XHRzaG93VmFsaWRhdGlvbjogdHJ1ZVxuXHRcdFx0fSk7XG5cdFx0fVxuXHR9XG5cdFxuXHRcbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IExvYW5Qcm9kdWN0QWRkZXI7XG4iLCJsZXQgUmVhY3RcdFx0XHRcdD0gcmVxdWlyZSgncmVhY3QnKSxcclxuXHRudW1iZXJXaXRoQ29tbWFzXHQ9IHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9udW1iZXJGaWx0ZXJGdW5jdGlvbnMnKS5udW1iZXJXaXRoQ29tbWFzLFxyXG5cdGZvcm1WYWxpZGF0b3JzXHRcdD0gcmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2Zvcm1WYWxpZGF0b3JzJyk7XHJcblxyXG5sZXQgeyBzZWxlY3RSZXF1aXJlZCwgbnVtYmVyUmVxdWlyZWQgfSA9IGZvcm1WYWxpZGF0b3JzO1xyXG5cclxuXHJcbmxldCBMb2FuSGVscGVyU2VjdGlvbiA9IFJlYWN0LmNyZWF0ZUNsYXNzKHtcclxuXHRtaXhpbnM6IFtcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9xdWVyeVN0b3JlTWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9wcm9kdWN0U3RvcmVNaXhpbicpLFxyXG5cdFx0cmVxdWlyZSgnLi4vLi4vY29tbW9uL3V0aWxzL2NvbXBhcmlzb25TdG9yZU1peGluJyksXHJcblx0XHRyZXF1aXJlKCcuLi8uLi9jb21tb24vdXRpbHMvc2VuZFF1ZXJ5TWl4aW4nKSxcclxuXHRcdHJlcXVpcmUoJy4uLy4uL2NvbW1vbi91dGlscy9oZWxwZXJTZWN0aW9uTWl4aW4uanN4JyksXHJcblx0XSxcclxuXHRcclxuXHJcbiAgICBnZXREZWZhdWx0UHJvcHMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuXHRcdFx0XHJcbiAgICAgICAgfTtcclxuICAgIH0sXHJcblxyXG4gICAgZ2V0SW5pdGlhbFN0YXRlKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcblx0XHRcdFxyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICBcclxuICAgIGNvbXBvbmVudERpZE1vdW50KCkge1xyXG5cclxuICAgIH0sXHJcblxyXG4gICAgY29tcG9uZW50V2lsbFVubW91bnQoKSB7XHJcblxyXG4gICAgfSxcclxuXHJcblx0X2dldFNlYXJjaEVudHJpZXMocXVlcnlTdGF0ZSkge1xyXG5cdFx0bGV0IHtzdWJtZW51LCBzZWFyY2hRdWVyeSwgb3B0Q29kZU1hcH0gPSBxdWVyeVN0YXRlO1xyXG5cdFx0bGV0IGVudHJpZXM7XHJcblx0XHRcclxuXHRcdHN3aXRjaChzdWJtZW51LnRvTG93ZXJDYXNlKCkpIHtcclxuXHRcdFx0Y2FzZSgnc2lnbmF0dXJlJyk6XHJcblx0XHRcdFx0ZW50cmllcyA9IFtcclxuXHRcdFx0XHRcdHtcclxuXHRcdFx0XHRcdFx0bGFiZWw6ICfrjIDstpzquIjslaEnLFxyXG5cdFx0XHRcdFx0XHR0eXBlOiAnbnVtYmVyLXdpdGgtY29tbWFzJyxcclxuXHRcdFx0XHRcdFx0aWQ6ICdhbW91bnQnLFxyXG5cdFx0XHRcdFx0XHR1bml0OiAn66eM7JuQJyxcclxuXHRcdFx0XHRcdFx0dmFsaWRhdGlvbkZuOiBudW1iZXJSZXF1aXJlZFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdF07XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdGNhc2UoJ3NlY3VyZWQnKTpcclxuXHRcdFx0XHRlbnRyaWVzID0gW1xyXG5cdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRsYWJlbDogJ+uLtOuztOycoO2YlScsXHJcblx0XHRcdFx0XHRcdHR5cGU6ICdzZWxlY3QnLFxyXG5cdFx0XHRcdFx0XHRpZDogJ3NlY3VyZVR5cGUnLFxyXG5cdFx0XHRcdFx0XHRvcHRpb25zOiB0aGlzLmdldFNlbGVjdE9wdGlvbnMob3B0Q29kZU1hcCwgJ0IwMDAxMDgxJyksXHJcblx0XHRcdFx0XHRcdHZhbGlkYXRpb25Gbjogc2VsZWN0UmVxdWlyZWRcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRdO1xyXG5cdFx0XHRcdGJyZWFrO1xyXG5cdFx0XHRkZWZhdWx0OlxyXG5cdFx0XHRcdGVudHJpZXMgPSBbXTtcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZW50cmllcztcclxuXHR9LFxyXG5cclxuXHRfZ2V0Q21wckNhcmRDb250ZW50cyhzdWJtZW51LCBjb21wYXJpc29uR3JvdXApIHtcclxuXHJcblx0XHRsZXQgY21wckNhcmRDb250ZW50cyA9IFtdO1xyXG5cclxuXHRcdHN3aXRjaChzdWJtZW51LnRvTG93ZXJDYXNlKCkpIHtcclxuXHRcdFx0Y2FzZSgnc2lnbmF0dXJlJyk6XHJcblx0XHRcdGNhc2UoJ3NlY3VyZWQnKTpcclxuXHRcdFx0XHRjbXByQ2FyZENvbnRlbnRzID0gY29tcGFyaXNvbkdyb3VwLm1hcCgoZWxlbSwgaWR4KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQge1xyXG5cdFx0XHRcdFx0XHRsbl9ubSwgLy/sg4HtkojrqoVcclxuXHRcdFx0XHRcdFx0bG5fYmFua2luZ19ubSwgLy/snYDtlonrqoVcclxuXHRcdFx0XHRcdFx0aW50ZXJlc3QsIC8v6riI66asXHJcblx0XHRcdFx0XHRcdGFtb3VudCwgLy/rjIDstpzquIjslaFcclxuXHRcdFx0XHRcdFx0Ly/snbTsnpBcclxuXHRcdFx0XHRcdH0gPSBlbGVtO1xyXG5cdFx0XHRcdFx0XHJcblx0XHRcdFx0XHRyZXR1cm5cdHtcclxuXHRcdFx0XHRcdFx0dGl0bGU6IGxuX25tLFxyXG5cdFx0XHRcdFx0XHRpbmZvOiBbXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+q4sOq0gOuqhScsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogbG5fYmFua2luZ19ubSxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDQsXHJcblx0XHRcdFx0XHRcdFx0XHRpc1Byb3ZpZGVyOiB0cnVlXHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XHR7XHJcblx0XHRcdFx0XHRcdFx0XHRhdHRyOiAn6riI66asJyxcclxuXHRcdFx0XHRcdFx0XHRcdHZhbHVlOiBpbnRlcmVzdCxcclxuXHRcdFx0XHRcdFx0XHRcdGNvbFNwYW46IDQsXHJcblx0XHRcdFx0XHRcdFx0XHRwcmVmaXg6ICfsl7AnLFxyXG5cdFx0XHRcdFx0XHRcdFx0dW5pdDogJyUnLFxyXG5cdFx0XHRcdFx0XHRcdFx0aXNMZWFkaW5nSW5mbzogdHJ1ZVxyXG5cdFx0XHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRcdFx0e1xyXG5cdFx0XHRcdFx0XHRcdFx0YXR0cjogJ+uMgOy2nOq4iOyVoScsXHJcblx0XHRcdFx0XHRcdFx0XHR2YWx1ZTogYCR7bnVtYmVyV2l0aENvbW1hcyhhbW91bnQpfeunjOybkGAsXHJcblx0XHRcdFx0XHRcdFx0XHRjb2xTcGFuOiA0XHJcblx0XHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0XHQvLyB7XHJcblx0XHRcdFx0XHRcdFx0Ly8gXHRhdHRyOiAn7J207J6QJyxcclxuXHRcdFx0XHRcdFx0XHQvLyBcdHZhbHVlOiBlbGVtLmRhbWJvX3NoaWxzb25fbmFtZSxcclxuXHRcdFx0XHRcdFx0XHQvLyBcdGNvbFNwYW46IDNcclxuXHRcdFx0XHRcdFx0XHQvLyB9XHJcblx0XHRcdFx0XHRcdF1cclxuXHRcdFx0XHRcdH07XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdFx0YnJlYWs7XHJcblx0XHRcdFx0XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIGNtcHJDYXJkQ29udGVudHM7XHJcblxyXG5cdH1cclxuXHRcclxuICAgIFxyXG59KTtcclxuXHJcbm1vZHVsZS5leHBvcnRzID0gTG9hbkhlbHBlclNlY3Rpb247XHJcbiJdfQ==
