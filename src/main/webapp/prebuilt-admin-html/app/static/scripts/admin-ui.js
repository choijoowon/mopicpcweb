


var leftMenuSet = (function() {
    var _initSet = function(){
        $('.left-navi li').bind({
            'click': leftClick
        });
    }  

    function leftClick(e){
    	var _select = $(e.currentTarget).hasClass('select');
        if ( !_select ){
            if ( $(e.currentTarget).find('.sub-list').length > 0){
                e.preventDefault();
                $(e.currentTarget).find('.glyphicon').toggleClass('glyphicon-menu-down glyphicon-menu-up')
                $(e.currentTarget).find('.sub-list').toggle();
            } else {
                var linkStr = $(e.currentTarget).find('a').attr('href');
                window.open(linkStr, '_self');
            }
        } else {
            e.preventDefault();
        }
    }

    return {
        'initSet' : _initSet
    }

})();

var layerPopUp = {
    THIS: "",
    pop : "",
    init : function($pop){
        THIS = this;
        THIS.pop = $pop;
        THIS.bindEvent();
    },
    open : function(){
        $('.pop-bg').show();
        THIS.pop.show();
        $('body').addClass('pop');
    },
    close : function() {
        THIS.pop.hide();
        $('.pop-bg').hide();
        $('body').toggleClass('pop');
    },
    bindEvent : function() {
        THIS.pop.find('.close').click( function(e) {
            e.preventDefault();
            THIS.close();
        } );
    }
};

var recommend_pro_list = function($table_body){
    var t_body = $table_body;
    var pro_data;
    var _initSet = function($data){
        pro_data = $data;
        $('.table-list-header h3').text('상품목록 총(' + pro_data.length + ')개');
        $(pro_data).each(function(i) {
            t_body.append( renderList(pro_data[i]) );
        });
    }

    var renderList = function(renderData){
        var returnNode;
        for (var prop in renderData) {
            //console.log(prop + " is " + renderData[prop]);
            var _id = '<tr data-id=' + renderData['id'] + '>';
            var _check = '<td class="cnt"><div class="checkbox"><label for="select-chk-' + renderData['id'] + '"><input type="checkbox" id="select-chk-' + renderData['id'] + '" name="select-chk-' + renderData['id'] + '" /></label></div></td>';
            var _sort = '<td>' + renderData['big-sort'] +  '&gt;' + renderData['big-sort'] + '</td>';
            var _name = '<td>' + '<a href="' + renderData['link'] + '" title="' + renderData['name'] + '">' + renderData['name'] + '</a></td>';
            var _des = '<td>' + renderData['description'] + '</td>';
            var _date = '<td class="date">' + renderData['date'] + '</td>';
            var _status = '<td class="status">' + renderData['status'] + '</td>';
            returnNode = _id + _check+ _sort + _name + _des + _date + _status + '</tr>';
        }
        return returnNode;
    }


    return {
        'initSet' : _initSet
    };

}

var DropDownSingle = function($drop) {
    var drop = $drop,
        select, currentText;
    var currentIndex = 0;

    var _initSet = function(){
        select = drop.find('button');
        _bindEvent();
    };
    var _bindEvent = function(){
        drop.find('li a').click(function(e) {
            e.preventDefault(); 
            currentText = $(e.currentTarget).text();
            select.text(currentText);
            currentIndex = $(e.currentTarget).parent().index();
        });
    };
    var _getText = function(){
        return currentText;
    };
    var _getIndex = function(){
        return currentIndex;
    };
    
    return {
        'initSet' : _initSet,
        'getText' : _getText,
        'getIndex' : _getIndex
    };
};
var DropDownSet = function($drop) {
    var drop = $drop,
        data, select, currentText;
    var currentIndex = 0;

    var _initSet = function(){
        select = drop.find('button');
        $(data).each(function(i) {
            drop.find('.dropdown-menu').append(renderNode(data[i]['text']));
        });
    };
    
    var renderNode = function(renderData){
        var returnNode;
        return returnNode = '<li><a href="/">' + renderData + '</a></li>';
    };
    var _bindEvent = function(callback){
        drop.find('li a').click(function(e) {
            e.preventDefault(); 
            currentText = $(e.currentTarget).text();
            select.text(currentText);
            currentIndex = $(e.currentTarget).parent().index();
            if ( callback !== undefined ){
                callback(currentIndex);
            }    
        });
    };
    var _dispatchEvent = function(num){
        drop.find('li:eq(' + num + ') a').trigger('click');
    };
    var _disableSet = function(bool){
        select.attr('disabled', bool);
    };
    var _getText = function(){
        return currentText;
    };
    var _getIndex = function(){
        return currentIndex;
    };
    var _getID = function(){
        return data[currentIndex]['id'];
    };
    var _visible = function(str){
        if ( str == 'show' ){
            drop.show();
        } else {
            drop.hide();
        }
    };
    var _setData = function($data){
        data = $data;
    };
    var _update = function(num){
        if ( data[num].child.length == 0 ){
            _visible('hide');
        } else {
            var dm = drop.find('.dropdown-menu');
                dm.find('li').remove()
                select.text(data[num].child[0]);
            $(data[num].child).each(function(i) {
                dm.append(renderNode(data[num].child[i]));
            });
            _bindEvent();
            _visible('show');
        }
    }

    return {
        'initSet' : _initSet,
        'setData' : _setData,
        'bindEvent' : _bindEvent,
        'update' : _update,
        'dispatchEvent' : _dispatchEvent,
        'disableSet' : _disableSet,
        'visible' : _visible,
        'getText' : _getText,
        'getIndex' : _getIndex,
        'getID' : _getID
    };
};

function inputText($input){
    var max = $input.find('input[type="text"]').attr('maxlength');
    if ( max !== undefined ){
        $input.find('input[type="text"]').bind('input', function() {
            var cLength = $(this).val().length;
            if ( cLength > Number(max) ){ cLength = max; }
            $input.find('.input-group-addon').text(cLength + '/' + max);
        });
    }
    
}



(function() {
	leftMenuSet.initSet();
    if ( $('.layer-pop').length > 0 ){
        layerPopUp.init($('.layer-pop'));
    };
    if ( $('.input-group').length > 0 ){
        $('.input-group').each( function() {
            inputText( $(this));    
        } );
    };

    
})();


