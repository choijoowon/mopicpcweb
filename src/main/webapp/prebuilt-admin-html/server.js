/*
 * For initial front-end testing of mo-pic
 */

var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var basePath = '/WEB-INF/views/admin/admin_page';


app
    .set('port', (process.env.PORT || 3000))
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({extended: true}));

//관리자 페이지 리스트
app.get(/list/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '_list.html'));
});


//상품정보 조회
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '00.html'));
});
//상품등록
app.get(/01/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '01.html'));
});

//이벤트 조회
app.get(/02/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '02.html'));
});
//이벤트 등록
app.get(/03/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '03.html'));
});

//메뉴 관리
app.get(/04/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '04.html'));
});
//배경 이미지 설정
app.get(/05/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '05.html'));
});
//1:1 문의 답변
app.get(/06/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '06.html'));
});
//통계 - DashBoard
app.get(/07/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '07.html'));
});
//통계 - 방문현황
app.get(/08/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '08.html'));
});
//통계 - 회원분석
app.get(/09/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '09.html'));
});

//공지사항 등록
app.get(/10/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '10.html'));
});
//공지사항 수정, 확인
app.get(/11/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '11.html'));
});
//공통코드
app.get(/12/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '12.html'));
});

app.get(/13/, function(req, res) {
    res.sendFile(path.join(__dirname + basePath + '13.html'));
});

app.get('/json/:filename', function(req, res) {
    var filename = req.params.filename;
    var fileDir = __dirname + '/WEB-INF/views/admin/' + filename;
    //res.download(fileDir);
    res.sendFile(path.join(fileDir));
});


//killall node 



app.use('/resources/css/admin', express.static(path.join(__dirname, 'resources', 'css', 'admin')));
app.use('/resources/js/admin', express.static(path.join(__dirname, 'resources', 'js', 'admin')));
app.use('/resources/images/admin', express.static(path.join(__dirname, 'resources', 'images', 'admin')));
app.use('/resources/lib/admin', express.static(path.join(__dirname, 'resources', 'lib', 'admin')));
app.use('/json/', express.static(path.join(__dirname, 'WEB-INF', 'views', 'admin')));


app.listen(app.get('port'), function() {
    console.log('Server started: http://localhost:' + app.get('port') + '/');
});
